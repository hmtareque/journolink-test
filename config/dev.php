<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authenticated Developer Emails
    |--------------------------------------------------------------------------
    |
    | These are developers emails 
    | users logged in with these emails will get all permissions 
    | and app configuration options as well
    |
    */

    'emails' => [
        'admin@exetie.com',
        'hasan@exetie.com',
        'hmtareque@gmail.com'
    ],

    

];
