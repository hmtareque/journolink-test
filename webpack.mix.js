const { mix } = require('laravel-mix');
        /*
         |--------------------------------------------------------------------------
         | Mix Asset Management
         |--------------------------------------------------------------------------
         |
         | Mix provides a clean, fluent API for defining some Webpack build steps
         | for your Laravel application. By default, we are compiling the Sass
         | file for the application as well as bundling up all the JS files.
         |
         */

mix.sass('resources/assets/sass/web/web.scss', 'public/css');
mix.sass('resources/assets/sass/app/app.scss', 'public/css');
mix.sass('resources/assets/sass/app/login.scss', 'public/css');

// web
mix.combine([
    'resources/assets/css/font-awesome/css/font-awesome.css',
    'resources/assets/js/jquery-ui/jquery-ui.css',
    'resources/assets/js/jquery-ui/jquery-ui.theme.css',
    'resources/assets/bootstrap/css/bootstrap.css',
    'resources/assets/bootstrap/css/bootstrap-theme.css',
    'resources/assets/js/plugins/master-slider/masterslider/style/masterslider.css',
    'resources/assets/js/plugins/master-slider/masterslider/skins/default/style.css',
    'resources/assets/js/plugins/master-slider/masterslider/style/style.css',
    'resources/assets/js/plugins/owl-carousel/assets/owl.carousel.css',
    'resources/assets/js/plugins/owl-carousel/assets/owl.theme.default.css',
], 'public/css/web-requisite.css');

mix.combine([
    'resources/assets/js/plugins/jquery/jquery.js',
    'resources/assets/bootstrap/js/bootstrap.js',
    'resources/assets/js/plugins/jquery/jquery-ui.min.js',
    'resources/assets/js/plugins/jquery-form/jquery-form.js',
    'resources/assets/js/plugins/master-slider/masterslider/jquery.easing.min.js',
    'resources/assets/js/plugins/master-slider/masterslider/masterslider.min.js',
    'resources/assets/js/plugins/owl-carousel/owl.carousel.js',
    'resources/assets/bootstrap/js/ie-10-viewport-bug-workaround.js',
], 'public/js/web-requisite.js');

// app
mix.combine([
    'resources/assets/css/font-awesome/css/font-awesome.css',
    'resources/assets/js/plugins/jquery-ui/jquery-ui.css',
    'resources/assets/js/plugins/jquery-ui/jquery-ui.theme.css',
    'resources/assets/bootstrap/css/bootstrap.css',
    'resources/assets/bootstrap/css/bootstrap-theme.css',
    'resources/assets/css/datatables/datatables.bootstrap.min.css',
], 'public/css/app-requisite.css');

mix.combine([
    'resources/assets/js/plugins/jquery/jquery.js',
    'resources/assets/js/plugins/jquery-ui/jquery-ui.min.js',
    'resources/assets/js/plugins/datatables/jquery.datatables.min.js',
    'resources/assets/js/plugins/datatables/datatables.bootstrap.min.js',
    'resources/assets/bootstrap/js/bootstrap.js',
    'resources/assets/js/plugins/jquery-form/jquery.form.js',
    'resources/assets/bootstrap/js/ie-10-viewport-bug-workaround.js',
], 'public/js/app-requisite.js');


mix.combine([
    'resources/assets/js/scripts/sidebar.js',
    'resources/assets/js/scripts/form.js',
    'resources/assets/js/scripts/students.js',
], 'public/js/app.js');

