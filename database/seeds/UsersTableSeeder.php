<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {


        for ($i = 0; $i < 25; $i++) {
            $fake = Faker\Factory::create();

            $staff_data = array(
                'first_name' => $fake->firstName,
                'last_name' => $fake->lastName,
                'date_of_birth' => date('Y-m-d', strtotime('-1 year')),
                'gender' => 'male',
                'ni_number' => str_random(8),
                'address_line_1' => $fake->address,
                'address_line_2' => NULL,
                'street' => $fake->streetName,
                'city' => $fake->city,
                'postcode' => $fake->postcode,
                'phone' => $fake->phoneNumber,
                'mobile' => $fake->phoneNumber,
                'designation' => 'Designation',
                'immigration_status' => 'Work Visa',
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => 1
            );

            $staff_id = DB::table('staffs')->insertGetId($staff_data);

            if ($staff_id == 1) {
                DB::table('users')->insert([
                    'id' => 1,
                    'staff_id' => 1,
                    'role_id' => 1,
                    'email' => 'admin@exetie.com',
                    'password' => bcrypt('01001100'),
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => 1
                ]);
            } else {
                $user_data = array(
                    'staff_id' => $staff_id,
                    'role_id' => $fake->numberBetween(1, 25),
                    'email' => $fake->email,
                    'password' => bcrypt($fake->text(10)),
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => 1
                );

                DB::table('users')->insert($user_data);
            }
        }
    }

}
