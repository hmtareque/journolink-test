<?php


use Illuminate\Database\Seeder;

class PermissionsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $modules = DB::table('modules')->get();
        
        if($modules->count()>0){
            
            foreach($modules as $module){
               
                $permission = array(
                    'role_id' => 1,
                    'module_id' => $module->id,
                    'permission' => 4,
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => 1,
                );
                
                DB::table('permissions')->insert($permission);
            }
        }
    }
}
