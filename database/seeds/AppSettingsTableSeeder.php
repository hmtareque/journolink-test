<?php

use Illuminate\Database\Seeder;

class AppSettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = array(
            array(
                'param' => 'name',
                'type' => 'text',
                'value' => 'App Name',
                'removable' => 0
            ),
            array(
                'param' => 'dev_name',
                'type' => 'text',
                'value' => 'Hasan Tareque',
                'removable' => 0
            ),
            array(
                'param' => 'dev_email',
                'type' => 'text',
                'value' => 'hasan@exetie.com',
                'removable' => 0
            )
        );

        foreach ($settings as $setting) {
             DB::table('app_settings')->insert($setting);
        }
    }
}