

<?php

use Illuminate\Database\Seeder;

class ContactsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $contacts = array(
            array(
                'param' => 'business_name',
                'value' => 'Business Name',
            ),
            array(
                'param' => 'address_line_1',
                'value' => 'Address Line 1',
            ),
            array(
                'param' => 'address_line_2',
                'value' => 'Address Line 2',
            ),
            array(
                'param' => 'street',
                'value' => 'Street',
            ),
            array(
                'param' => 'city',
                'value' => 'City',
            ),
            array(
                'param' => 'postcode',
                'value' => 'Postcode',
            ),
            array(
                'param' => 'county',
                'value' => 'County',
            ),
            array(
                'param' => 'country',
                'value' => 'Country',
            ),
            array(
                'param' => 'email',
                'value' => 'email@exetie.com',
            ),
            array(
                'param' => 'phone',
                'value' => '0203 153 1076',
            ),
            array(
                'param' => 'fax',
                'value' => '0203 153 1076',
            ),
            array(
                'param' => 'mobile',
                'value' => '075 3535 1616',
            )
        );

        foreach ($contacts as $contact) {
             DB::table('contacts')->insert($contact);
        }
    }
}

