<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class AppModulesTableSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {

        $modules = array(
            ['id' => 1, 'name' => 'Authentication - Users'],
            ['id' => 2, 'name' => 'Authoraization - Roles'],
            ['id' => 3, 'name' => 'Manage Clients'],
            ['id' => 4, 'name' => 'Education - Schools'],
            ['id' => 5, 'name' => 'Education - Classes'],
            ['id' => 6, 'name' => 'Education - Students'],
            ['id' => 7, 'name' => 'Education - Teachers'],
            ['id' => 8, 'name' => 'Education - Reports'],
            ['id' => 9, 'name' => 'Education - Administration'],
        //  ['id' => 3, 'name' => 'Website - Configuration'],
        //  ['id' => 4, 'name' => 'Website - Contents'],
        );

        DB::table('modules')->insert($modules);
    }

}
