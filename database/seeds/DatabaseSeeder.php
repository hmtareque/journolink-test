<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
         $this->call(AppSettingsTableSeeder::class);
         $this->call(ServiceSettingsDataSeeder::class);
         $this->call(AppModulesTableSeeder::class);
         $this->call(RolesTableSeeder::class);
         $this->call(PermissionsTableSeeder::class);
         $this->call(UsersTableSeeder::class);
         $this->call(ContactsTableSeeder::class);
         $this->call(EducationSettingsDataSeeder::class);
         $this->call(CommonDataSeeder::class);
         $this->call(ClientTableSeeder::class);

    }
}
