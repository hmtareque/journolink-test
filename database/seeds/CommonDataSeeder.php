<?php

use Illuminate\Database\Seeder;

class CommonDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $heard_about_us_by = array(
            ['id' => 1, 'name' => 'Google'],
            ['id' => 2, 'name' => 'Word of Mouth'],
            ['id' => 3, 'name' => 'Press'],
            ['id' => 4, 'name' => 'Advertisement'],
            ['id' => 5, 'name' => 'TV'],
            ['id' => 6, 'name' => 'Article'],
            ['id' => 7, 'name' => 'Blog Post'],
            ['id' => 8, 'name' => 'Social Media']
        );
        
        DB::table('heard_about_us_by')->insert($heard_about_us_by);
        
        
        $immigration_statuses = array(
            ['id' => 1, 'name' => 'UK Nationals'],
            ['id' => 2, 'name' => 'EEA National receiving welfare benefits'],
            ['id' => 3, 'name' => 'EEA National financially self-supporting'],
            ['id' => 4, 'name' => 'EEA National in UK studying'],
            ['id' => 5, 'name' => 'EEA National currently working'],
            ['id' => 6, 'name' => 'Discretionary leave to remain'],
            ['id' => 7, 'name' => 'Indefinite leave to remain'],
            ['id' => 8, 'name' => 'Study visa'],
            ['id' => 9, 'name' => 'Work visa'],
            ['id' => 10, 'name' => 'Husband/Wife sponsorship'],
            ['id' => 11, 'name' => 'Asylum seekers awaiting decision'],
            ['id' => 12, 'name' => 'Refugee'],
            ['id' => 13, 'name' => 'Humanitarian Protection']
        );
        
        DB::table('immigration_statuses')->insert($immigration_statuses);
        
        $boroughes = array(
            ['id' => 1, 'name' => 'Barnet'],
            ['id' => 2, 'name' => 'Brent'],
            ['id' => 3, 'name' => 'Harrow'],
            ['id' => 4, 'name' => 'Enfield'],
            ['id' => 5, 'name' => 'Hillingdon'],
            ['id' => 6, 'name' => 'Ealing'],
            ['id' => 7, 'name' => 'Hayes'],
            ['id' => 8, 'name' => 'Camden'],
            ['id' => 9, 'name' => 'Willesden'],
        );
        
        DB::table('boroughes')->insert($boroughes);
    }
}













