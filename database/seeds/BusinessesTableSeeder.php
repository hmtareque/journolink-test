<?php

use Illuminate\Database\Seeder;

class BusinessesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $businesses = array('Business One', 'Business Two');
        
        foreach($businesses as $business){
            $business_data = array(
                'name' => $business
            );
            
            DB::table('businesses')->insert($business_data);
        }
    }
}
