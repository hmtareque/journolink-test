<?php

use Illuminate\Database\Seeder;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        $roles = array(
            ['id' => 1, 'name' => 'Super Admin', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1],
            ['id' => 2, 'name' => 'Director', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1],
            ['id' => 3, 'name' => 'Account Manager', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1],
            ['id' => 4, 'name' => 'Office Admin', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1],
            ['id' => 5, 'name' => 'Front Office Desk', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1],
            
            ['id' => 6, 'name' => 'Education Services Manager', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1],
            ['id' => 7, 'name' => 'School Admin', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1],
            ['id' => 8, 'name' => 'Teacher', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1],
            ['id' => 9, 'name' => 'Teaching Assistant', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1],
            ['id' => 10, 'name' => 'Volunteer - Education', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1],
            ['id' => 11, 'name' => 'Mentor - Education', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1],
            
            ['id' => 12, 'name' => 'Family Services Manager', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1],
            ['id' => 13, 'name' => 'Housing Manager', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1],
            ['id' => 14, 'name' => 'Youth Manager', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1],
            ['id' => 15, 'name' => 'Employability Manager', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1],
            ['id' => 16, 'name' => 'Parenting Manager', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1],
            ['id' => 17, 'name' => 'Volunteer - Family', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1],
            ['id' => 18, 'name' => 'Mentor - Family', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1],
            
            ['id' => 19, 'name' => 'Advocacy Services Manager', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1],
            ['id' => 20, 'name' => 'Community Advocate', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1],
            ['id' => 21, 'name' => 'Immigration Advocate', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1],
            ['id' => 22, 'name' => 'Mental Health Advocate', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1],
            ['id' => 23, 'name' => 'Volunteer - Advocacy', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1],
            ['id' => 24, 'name' => 'Mentor - Advocacy', 'created_at' => date('Y-m-d H:i:s'), 'created_by' => 1],
        );
          
        DB::table('roles')->insert($roles);
      
    }
}
