<?php

use Illuminate\Database\Seeder;

class ServiceSettingsDataSeeder extends Seeder {

    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run() {
        $service_areas = array(
            ['area' => 'education', 'name' => 'Education'],
            ['area' => 'family', 'name' => 'Family'],
            ['area' => 'advocacy', 'name' => 'Advocacy'],
        );

        DB::table('service_areas')->insert($service_areas);

        $services = array(
            ['id' => 1, 'service_area' => 'family', 'name' => 'Youth'],
            ['id' => 2, 'service_area' => 'family', 'name' => 'Housing'],
            ['id' => 3, 'service_area' => 'family', 'name' => 'Emploiability'],
            ['id' => 4, 'service_area' => 'family', 'name' => 'Parenting'],
            ['id' => 5, 'service_area' => 'advocacy', 'name' => 'Community Advocacy'],
            ['id' => 6, 'service_area' => 'advocacy', 'name' => 'Immigration Advice'],
            ['id' => 7, 'service_area' => 'advocacy', 'name' => 'Mental Health'],
        );

        DB::table('services')->insert($services);
        
        $service_categories = array(
            ['service_area' => 'family', 'service_id' => 2, 'name' => 'Money', 'private' => 0, 'sort' => 1],
            ['service_area' => 'family', 'service_id' => 2, 'name' => 'Health', 'private' => 0, 'sort' => 2],
            ['service_area' => 'family', 'service_id' => 2, 'name' => 'Education, Employment and Training', 'private' => 0, 'sort' => 3],
            ['service_area' => 'family', 'service_id' => 2, 'name' => 'Family and Relationship', 'private' => 0, 'sort' => 4],
            ['service_area' => 'family', 'service_id' => 2, 'name' => 'Life Skills', 'private' => 0, 'sort' => 5],
            ['service_area' => 'family', 'service_id' => 3, 'name' => 'Future Preparation', 'private' => 0, 'sort' => 1],
            ['service_area' => 'family', 'service_id' => 3, 'name' => 'Future Enhanced', 'private' => 0, 'sort' => 2],
            ['service_area' => 'family', 'service_id' => 3, 'name' => 'Future Unleashed', 'private' => 0, 'sort' => 3],
            ['service_area' => 'advocacy', 'service_id' => 7, 'name' => 'Counselling', 'private' => 0,  'sort' => 1],
            ['service_area' => 'advocacy', 'service_id' => 7, 'name' => 'Advocacy', 'private' => 1, 'sort' => 2],
        );

        DB::table('service_categories')->insert($service_categories);
    }

}
