<?php

use Illuminate\Database\Seeder;

class EducationSettingsDataSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        

        $academic_years = array(
            ['id' => 1, 'year' => '2016-17', 'sort' => 1],
            ['id' => 2, 'year' => '2017-18', 'sort' => 2],
        );
        
        DB::table('academic_years')->insert($academic_years);
        
        
        
        $student_groups = array(
            ['id' => 1, 'group' => 'Reception', 'sort' => 1],
            ['id' => 2, 'group' => 'Year 1', 'sort' => 2],
            ['id' => 3, 'group' => 'Year 2', 'sort' => 3],
            ['id' => 4, 'group' => 'Year 3', 'sort' => 4],
            ['id' => 5, 'group' => 'Year 4', 'sort' => 5],
            ['id' => 6, 'group' => 'Year 5', 'sort' => 6],
            ['id' => 7, 'group' => 'Year 6', 'sort' => 7],
            ['id' => 8, 'group' => 'GCSE', 'sort' => 8],
            ['id' => 9, 'group' => 'ESOL - Entry 1', 'sort' => 9],
            ['id' => 10, 'group' => 'ESOL - Entry 2', 'sort' => 10],
            ); 
        
        DB::table('student_groups')->insert($student_groups);
        
        
        $courses = array(
            ['id' => 1, 'title' => 'English', 'sort' => 1],
            ['id' => 2, 'title' => 'Math', 'sort' => 2],
            ['id' => 3, 'title' => 'Pashtu', 'sort' => 3],
            ['id' => 4, 'title' => 'Dari', 'sort' => 4],
            ['id' => 5, 'title' => 'Farsi', 'sort' => 5],
            ['id' => 6, 'title' => 'ESOL', 'sort' => 6],
        ); 
        
        DB::table('courses')->insert($courses);
        
        
        $terms = array(
            ['academic_year_id' => 1, 'term' => 'Summer', 'dates' => '1 September - 31 December', 'sort' => 1],
            ['academic_year_id' => 1, 'term' => 'Spring', 'dates' => '1 January - 31 March', 'sort' => 2],
            ['academic_year_id' => 1, 'term' => 'Autumn', 'dates' => '1 April - 31 August', 'sort' => 3],
        );
        
        DB::table('school_terms')->insert($terms);
        
        $schools = array(
            
            ['type' => 'saturday', 'name' => 'Norbury Primary', 'primary_contact_person' => 'Primary Contact Person Name', 'address_line_1' => 'Address Line 1', 'address_line_2' => 'Address Line 2', 'street' => 'Street', 'postcode' => 'Postcode', 'phone' => '0207 777 7777', 'mobile' => '07985 666 666', 'email' => 'email@domain.com'],
            ['type' => 'saturday', 'name' => 'Edgware Primary', 'primary_contact_person' => 'Primary Contact Person Name', 'address_line_1' => 'Address Line 1', 'address_line_2' => 'Address Line 2', 'street' => 'Street', 'postcode' => 'Postcode', 'phone' => '0207 777 7777', 'mobile' => '07985 666 666', 'email' => 'email@domain.com'],
            ['type' => 'saturday', 'name' => 'Stag Lane Junior', 'primary_contact_person' => 'Primary Contact Person Name', 'address_line_1' => 'Address Line 1', 'address_line_2' => 'Address Line 2', 'street' => 'Street', 'postcode' => 'Postcode', 'phone' => '0207 777 7777', 'mobile' => '07985 666 666', 'email' => 'email@domain.com'],
            ['type' => 'saturday', 'name' => 'Childs Hill Primary', 'primary_contact_person' => 'Primary Contact Person Name', 'address_line_1' => 'Address Line 1', 'address_line_2' => 'Address Line 2', 'street' => 'Street', 'postcode' => 'Postcode', 'phone' => '0207 777 7777', 'mobile' => '07985 666 666', 'email' => 'email@domain.com'],
            ['type' => 'general', 'name' => 'Paiwand Office', 'primary_contact_person' => 'Primary Contact Person Name', 'address_line_1' => 'Address Line 1', 'address_line_2' => 'Address Line 2', 'street' => 'Street', 'postcode' => 'Postcode', 'phone' => '0207 777 7777', 'mobile' => '07985 666 666', 'email' => 'email@domain.com'],
            ['type' => 'referral', 'name' => 'All Saints Primary', 'primary_contact_person' => 'Primary Contact Person Name', 'address_line_1' => 'Address Line 1', 'address_line_2' => 'Address Line 2', 'street' => 'Street', 'postcode' => 'Postcode', 'phone' => '0207 777 7777', 'mobile' => '07985 666 666', 'email' => 'email@domain.com'],
            ['type' => 'referral', 'name' => 'St Mary and St John\'s', 'primary_contact_person' => 'Primary Contact Person Name', 'address_line_1' => 'Address Line 1', 'address_line_2' => 'Address Line 2', 'street' => 'Street', 'postcode' => 'Postcode', 'phone' => '0207 777 7777', 'mobile' => '07985 666 666', 'email' => 'email@domain.com'],
            ['type' => 'referral', 'name' => 'Wessex Gardens Primary', 'primary_contact_person' => 'Primary Contact Person Name', 'address_line_1' => 'Address Line 1', 'address_line_2' => 'Address Line 2', 'street' => 'Street', 'postcode' => 'Postcode', 'phone' => '0207 777 7777', 'mobile' => '07985 666 666', 'email' => 'email@domain.com'],
            ['type' => 'referral', 'name' => 'Grange Primary', 'primary_contact_person' => 'Primary Contact Person Name', 'address_line_1' => 'Address Line 1', 'address_line_2' => 'Address Line 2', 'street' => 'Street', 'postcode' => 'Postcode', 'phone' => '0207 777 7777', 'mobile' => '07985 666 666', 'email' => 'email@domain.com'],
            ['type' => 'referral', 'name' => 'Vaughan Primary', 'primary_contact_person' => 'Primary Contact Person Name', 'address_line_1' => 'Address Line 1', 'address_line_2' => 'Address Line 2', 'street' => 'Street', 'postcode' => 'Postcode', 'phone' => '0207 777 7777', 'mobile' => '07985 666 666', 'email' => 'email@domain.com'],
            ['type' => 'referral', 'name' => 'Cedars Manor', 'primary_contact_person' => 'Primary Contact Person Name', 'address_line_1' => 'Address Line 1', 'address_line_2' => 'Address Line 2', 'street' => 'Street', 'postcode' => 'Postcode', 'phone' => '0207 777 7777', 'mobile' => '07985 666 666', 'email' => 'email@domain.com'],
            ['type' => 'referral', 'name' => 'Weald Rise Primary', 'primary_contact_person' => 'Primary Contact Person Name', 'address_line_1' => 'Address Line 1', 'address_line_2' => 'Address Line 2', 'street' => 'Street', 'postcode' => 'Postcode', 'phone' => '0207 777 7777', 'mobile' => '07985 666 666', 'email' => 'email@domain.com'],
            ['type' => 'referral', 'name' => 'Stag Lane Infant', 'primary_contact_person' => 'Primary Contact Person Name', 'address_line_1' => 'Address Line 1', 'address_line_2' => 'Address Line 2', 'street' => 'Street', 'postcode' => 'Postcode', 'phone' => '0207 777 7777', 'mobile' => '07985 666 666', 'email' => 'email@domain.com'],
            ['type' => 'referral', 'name' => 'Glebe Primary', 'primary_contact_person' => 'Primary Contact Person Name', 'address_line_1' => 'Address Line 1', 'address_line_2' => 'Address Line 2', 'street' => 'Street', 'postcode' => 'Postcode', 'phone' => '0207 777 7777', 'mobile' => '07985 666 666', 'email' => 'email@domain.com'],
            ['type' => 'referral', 'name' => 'Kenmore Park Junior', 'primary_contact_person' => 'Primary Contact Person Name', 'address_line_1' => 'Address Line 1', 'address_line_2' => 'Address Line 2', 'street' => 'Street', 'postcode' => 'Postcode', 'phone' => '0207 777 7777', 'mobile' => '07985 666 666', 'email' => 'email@domain.com'],
            ['type' => 'referral', 'name' => 'Colindale Primary', 'primary_contact_person' => 'Primary Contact Person Name', 'address_line_1' => 'Address Line 1', 'address_line_2' => 'Address Line 2', 'street' => 'Street', 'postcode' => 'Postcode', 'phone' => '0207 777 7777', 'mobile' => '07985 666 666', 'email' => 'email@domain.com'],
            ['type' => 'referral', 'name' => 'Woodcroft Primary', 'primary_contact_person' => 'Primary Contact Person Name', 'address_line_1' => 'Address Line 1', 'address_line_2' => 'Address Line 2', 'street' => 'Street', 'postcode' => 'Postcode', 'phone' => '0207 777 7777', 'mobile' => '07985 666 666', 'email' => 'email@domain.com'],
            
        );
        
        DB::table('schools')->insert($schools);
        
        
    }
}

