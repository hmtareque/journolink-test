<?php

use Illuminate\Database\Seeder;

class ClientTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
        for($i=0; $i<100; $i++){
            $fake = Faker\Factory::create();
            
            $data = array(
                'ref_no' => str_random(8),
                'first_name' => $fake->firstName,
                'last_name' => $fake->lastName,
                'date_of_birth' => date('Y-m-d', strtotime('-1 year')),
                'gender' => 'male',
                'ni_number' => $fake->numberBetween(999, 99999),
                'address_line_1' => $fake->address,
                'address_line_2' => NULL,
                'street' => $fake->streetName,
                'city' => $fake->city,
                'postcode' => $fake->postcode,
                'phone' => $fake->phoneNumber,
                'email' => $fake->email,
                'mobile' => $fake->phoneNumber,
                'next_of_kin' => $fake->name,
                'next_of_kin_relationship' => $fake->text(10),
                'next_of_kin_address_same_as_main' => 1,
                'next_of_kin_address_line_1' => NULL,
                'next_of_kin_address_line_2' => NULL,
                'next_of_kin_street' => NULL,
                'next_of_kin_city' => NULL,
                'next_of_kin_postcode' => NULL,
                'next_of_kin_phone' => $fake->phoneNumber,
                'next_of_kin_email' => $fake->email,
                'next_of_kin_mobile' => NULL,
                'born_in_the_uk' => 0,
                'arrived_in_the_uk' => date('Y-m-d'),
                'immigration_status' => $fake->numberBetween(1,9),
                'other_immigration_status' => NULL,
                'first_language' => $fake->languageCode,
                'ethnicity' => $fake->text(5),
                'country_of_origin' => $fake->countryCode,
                'heard_about_us_by' => $fake->text(6),
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => 1
            );

            $client_id = DB::table('clients')->insertGetId($data);
            
            $services = array('education', 'family', 'advocacy');
            
            $engagement_data = array(
                'client_id' => $client_id,
                'service' => $services[rand(0, 2)],
            );
            
            DB::table('engagements')->insert($engagement_data);
        }
    }
}
