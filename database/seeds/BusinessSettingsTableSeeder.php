<?php

use Illuminate\Database\Seeder;

class BusinessSettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $settings = array(
            array(
                'param' => 'name',
                'value' => 'Business Name',
                'removable' => 0
            ),
            array(
                'param' => 'currency',
                'value' => 'gbp',
                'removable' => 0
            ),
            array(
                'param' => 'timezone',
                'value' => 'Europe/London',
                'removable' => 0
            )
        );

        foreach ($settings as $setting) {
             DB::table('business_settings')->insert($setting);
        }
    }
}
