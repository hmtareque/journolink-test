<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

function countries($code = "") {

    $countries = array(
        "AF" => "Afghanistan",
        "AX" => "Aland Islands",
        "AL" => "Albania",
        "DZ" => "Algeria",
        "AS" => "American Samoa",
        "AD" => "Andorra",
        "AO" => "Angola",
        "AI" => "Anguilla",
        "AQ" => "Antarctica",
        "AG" => "Antigua and Barbuda",
        "AR" => "Argentina",
        "AM" => "Armenia",
        "AW" => "Aruba",
        "AC" => "Ascension Island",
        "AU" => "Australia",
        "AT" => "Austria",
        "AZ" => "Azerbaijan",
        "BD" => "Bangladesh",
        "BS" => "Bahamas",
        "BH" => "Bahrain",
        "BB" => "Barbados",
        "BY" => "Belarus",
        "BE" => "Belgium",
        "BZ" => "Belize",
        "BJ" => "Benin",
        "BM" => "Bermuda",
        "BT" => "Bhutan",
        "BO" => "Bolivia",
        "BA" => "Bosnia and Herzegovina",
        "BW" => "Botswana",
        "BV" => "Bouvet Island",
        "BR" => "Brazil",
        "IO" => "British Indian Ocean Territory",
        "VG" => "British Virgin Islands",
        "BN" => "Brunei",
        "BG" => "Bulgaria",
        "BF" => "Burkina Faso",
        "BI" => "Burundi",
        "KH" => "Cambodia",
        "CM" => "Cameroon",
        "CA" => "Canada",
        "IC" => "Canary Islands",
        "CV" => "Cape Verde",
        "BQ" => "Caribbean Netherlands",
        "KY" => "Cayman Islands",
        "CF" => "Central African Republic",
        "EA" => "Ceuta and Melilla",
        "TD" => "Chad",
        "CL" => "Chile",
        "CN" => "China",
        "CX" => "Christmas Island",
        "CP" => "Clipperton Island",
        "CC" => "Cocos",
        "CO" => "Colombia",
        "KM" => "Comoros",
        "CD" => "Congo",
        "CG" => "Congo",
        "CK" => "Cook Islands",
        "CR" => "Costa Rica",
        "CI" => "Côte d’Ivoire",
        "HR" => "Croatia",
        "CU" => "Cuba",
        "CW" => "Curaçao",
        "CY" => "Cyprus",
        "CZ" => "Czech Republic",
        "DK" => "Denmark",
        "DG" => "Diego Garcia",
        "DJ" => "Djibouti",
        "DM" => "Dominica",
        "DO" => "Dominican Republic",
        "EC" => "Ecuador",
        "EG" => "Egypt",
        "SV" => "El Salvador",
        "GQ" => "Equatorial Guinea",
        "ER" => "Eritrea",
        "EE" => "Estonia",
        "ET" => "Ethiopia",
        "FK" => "Falkland Islands",
        "FO" => "Faroe Islands",
        "FJ" => "Fiji",
        "FI" => "Finland",
        "FR" => "France",
        "GF" => "French Guiana",
        "PF" => "French Polynesia",
        "TF" => "French Southern Territories",
        "GA" => "Gabon",
        "GM" => "Gambia",
        "GE" => "Georgia",
        "DE" => "Germany",
        "GH" => "Ghana",
        "GI" => "Gibraltar",
        "GR" => "Greece",
        "GL" => "Greenland",
        "GD" => "Grenada",
        "GP" => "Guadeloupe",
        "GU" => "Guam",
        "GT" => "Guatemala",
        "GG" => "Guernsey",
        "GN" => "Guinea",
        "GW" => "Guinea-Bissau",
        "GY" => "Guyana",
        "HT" => "Haiti",
        "HM" => "Heard & McDonald Islands",
        "HN" => "Honduras",
        "HK" => "Hong Kong",
        "HU" => "Hungary",
        "IS" => "Iceland",
        "IN" => "India",
        "ID" => "Indonesia",
        "IR" => "Iran",
        "IQ" => "Iraq",
        "IE" => "Republic of Ireland",
        "IM" => "Isle of Man",
        "IL" => "Israel",
        "IT" => "Italy",
        "JM" => "Jamaica",
        "JP" => "Japan",
        "JE" => "Jersey",
        "JO" => "Jordan",
        "KZ" => "Kazakhstan",
        "KE" => "Kenya",
        "KI" => "Kiribati",
        "XK" => "Kosovo",
        "KW" => "Kuwait",
        "KG" => "Kyrgyzstan",
        "LA" => "Laos",
        "LV" => "Latvia",
        "LB" => "Lebanon",
        "LS" => "Lesotho",
        "LR" => "Liberia",
        "LY" => "Libya",
        "LI" => "Liechtenstein",
        "LT" => "Lithuania",
        "LU" => "Luxembourg",
        "MO" => "Macau",
        "MK" => "Macedonia",
        "MG" => "Madagascar",
        "MW" => "Malawi",
        "MY" => "Malaysia",
        "MV" => "Maldives",
        "ML" => "Mali",
        "MT" => "Malta",
        "MH" => "Marshall Islands",
        "MQ" => "Martinique",
        "MR" => "Mauritania",
        "MU" => "Mauritius",
        "YT" => "Mayotte",
        "MX" => "Mexico",
        "FM" => "Micronesia",
        "MD" => "Moldova",
        "MC" => "Monaco",
        "MN" => "Mongolia",
        "ME" => "Montenegro",
        "MS" => "Montserrat",
        "MA" => "Morocco",
        "MZ" => "Mozambique",
        "MM" => "Myanmar",
        "NA" => "Namibia",
        "NR" => "Nauru",
        "NP" => "Nepal",
        "NL" => "Netherlands",
        "NC" => "New Caledonia",
        "NZ" => "New Zealand",
        "NI" => "Nicaragua",
        "NE" => "Niger",
        "NG" => "Nigeria",
        "NU" => "Niue",
        "NF" => "Norfolk Island",
        "MP" => "Northern Mariana Islands",
        "KP" => "North Korea",
        "NO" => "Norway",
        "OM" => "Oman",
        "PK" => "Pakistan",
        "PW" => "Palau",
        "PS" => "Palestine",
        "PA" => "Panama",
        "PG" => "Papua New Guinea",
        "PY" => "Paraguay",
        "PE" => "Peru",
        "PH" => "Philippines",
        "PN" => "Pitcairn Islands",
        "PL" => "Poland",
        "PT" => "Portugal",
        "PR" => "Puerto Rico",
        "QA" => "Qatar",
        "RE" => "Réunion",
        "RO" => "Romania",
        "RU" => "Russia",
        "RW" => "Rwanda",
        "BL" => "Saint Barthélemy",
        "SH" => "Saint Helena",
        "KN" => "Saint Kitts and Nevis",
        "LC" => "Saint Lucia",
        "MF" => "Saint Martin",
        "PM" => "Saint Pierre and Miquelon",
        "WS" => "Samoa",
        "SM" => "San Marino",
        "ST" => "São Tomé and Príncipe",
        "SA" => "Saudi Arabia",
        "SN" => "Senegal",
        "RS" => "Serbia",
        "SC" => "Seychelles",
        "SL" => "Sierra Leone",
        "SG" => "Singapore",
        "SX" => "Sint Maarten",
        "SK" => "Slovakia",
        "SI" => "Slovenia",
        "SB" => "Solomon Islands",
        "SO" => "Somalia",
        "ZA" => "South Africa",
        "GS" => "South Georgia & South Sandwich Islands",
        "KR" => "South Korea",
        "SS" => "South Sudan",
        "ES" => "Spain",
        "LK" => "Sri Lanka",
        "VC" => "St. Vincent & Grenadines",
        "SD" => "Sudan",
        "SR" => "Suriname",
        "SJ" => "Svalbard and Jan Mayen",
        "SZ" => "Swaziland",
        "SE" => "Sweden",
        "CH" => "Switzerland",
        "SY" => "Syria",
        "TW" => "Taiwan",
        "TJ" => "Tajikistan",
        "TZ" => "Tanzania",
        "TH" => "Thailand",
        "TL" => "Timor-Leste",
        "TG" => "Togo",
        "TK" => "Tokelau",
        "TO" => "Tonga",
        "TT" => "Trinidad and Tobago",
        "TA" => "Tristan da Cunha",
        "TN" => "Tunisia",
        "TR" => "Turkey",
        "TM" => "Turkmenistan",
        "TC" => "Turks and Caicos Islands",
        "TV" => "Tuvalu",
        "UM" => "U.S. Outlying Islands",
        "VI" => "U.S. Virgin Islands",
        "UG" => "Uganda",
        "UA" => "Ukraine",
        "AE" => "United Arab Emirates",
        "GB" => "United Kingdom",
        "US" => "United States",
        "UY" => "Uruguay",
        "UZ" => "Uzbekistan",
        "VU" => "Vanuatu",
        "VA" => "Vatican City",
        "VE" => "Venezuela",
        "VN" => "Vietnam",
        "WF" => "Wallis and Futuna",
        "EH" => "Western Sahara",
        "YE" => "Yemen",
        "ZM" => "Zambia",
        "ZW" => "Zimbabwe",
    );

    if ($code != "") {
        return (isset($countries[$code])) ? $countries[$code] : false;
    }

    return $countries;
}

function nationalities() {
    $nationals = array(
        'Afghan',
        'Albanian',
        'Algerian',
        'American',
        'Andorran',
        'Angolan',
        'Antiguans',
        'Argentinean',
        'Armenian',
        'Australian',
        'Austrian',
        'Azerbaijani',
        'Bahamian',
        'Bahraini',
        'Bangladeshi',
        'Barbadian',
        'Barbudans',
        'Batswana',
        'Belarusian',
        'Belgian',
        'Belizean',
        'Beninese',
        'Bhutanese',
        'Bolivian',
        'Bosnian',
        'Brazilian',
        'British',
        'Bruneian',
        'Bulgarian',
        'Burkinabe',
        'Burmese',
        'Burundian',
        'Cambodian',
        'Cameroonian',
        'Canadian',
        'Cape Verdean',
        'Central African',
        'Chadian',
        'Chilean',
        'Chinese',
        'Colombian',
        'Comoran',
        'Congolese',
        'Costa Rican',
        'Croatian',
        'Cuban',
        'Cypriot',
        'Czech',
        'Danish',
        'Djibouti',
        'Dominican',
        'Dutch',
        'East Timorese',
        'Ecuadorean',
        'Egyptian',
        'Emirian',
        'Equatorial Guinean',
        'Eritrean',
        'Estonian',
        'Ethiopian',
        'Fijian',
        'Filipino',
        'Finnish',
        'French',
        'Gabonese',
        'Gambian',
        'Georgian',
        'German',
        'Ghanaian',
        'Greek',
        'Grenadian',
        'Guatemalan',
        'Guinea-Bissauan',
        'Guinean',
        'Guyanese',
        'Haitian',
        'Herzegovinian',
        'Honduran',
        'Hungarian',
        'I-Kiribati',
        'Icelander',
        'Indian',
        'Indonesian',
        'Iranian',
        'Iraqi',
        'Irish',
        'Israeli',
        'Italian',
        'Ivorian',
        'Jamaican',
        'Japanese',
        'Jordanian',
        'Kazakhstani',
        'Kenyan',
        'Kittian and Nevisian',
        'Kuwaiti',
        'Kyrgyz',
        'Laotian',
        'Latvian',
        'Lebanese',
        'Liberian',
        'Libyan',
        'Liechtensteiner',
        'Lithuanian',
        'Luxembourger',
        'Macedonian',
        'Malagasy',
        'Malawian',
        'Malaysian',
        'Maldivan',
        'Malian',
        'Maltese',
        'Marshallese',
        'Mauritanian',
        'Mauritian',
        'Mexican',
        'Micronesian',
        'Moldovan',
        'Monacan',
        'Mongolian',
        'Moroccan',
        'Mosotho',
        'Motswana',
        'Mozambican',
        'Namibian',
        'Nauruan',
        'Nepalese',
        'New Zealander',
        'Nicaraguan',
        'Nigerian',
        'Nigerien',
        'North Korean',
        'Northern Irish',
        'Norwegian',
        'Omani',
        'Pakistani',
        'Palauan',
        'Panamanian',
        'Papua New Guinean',
        'Paraguayan',
        'Peruvian',
        'Polish',
        'Portuguese',
        'Qatari',
        'Romanian',
        'Russian',
        'Rwandan',
        'Saint Lucian',
        'Salvadoran',
        'Samoan',
        'San Marinese',
        'Sao Tomean',
        'Saudi',
        'Scottish',
        'Senegalese',
        'Serbian',
        'Seychellois',
        'Sierra Leonean',
        'Singaporean',
        'Slovakian',
        'Slovenian',
        'Solomon Islander',
        'Somali',
        'South African',
        'South Korean',
        'Spanish',
        'Sri Lankan',
        'Sudanese',
        'Surinamer',
        'Swazi',
        'Swedish',
        'Swiss',
        'Syrian',
        'Taiwanese',
        'Tajik',
        'Tanzanian',
        'Thai',
        'Togolese',
        'Tongan',
        'Trinidadian',
        'Tobagonian',
        'Tunisian',
        'Turkish',
        'Tuvaluan',
        'Ugandan',
        'Ukrainian',
        'Uruguayan',
        'Uzbekistani',
        'Venezuelan',
        'Vietnamese',
        'Welsh',
        'Yemenite',
        'Zambian',
        'Zimbabwean',
    );

    return $nationals;
}

function languages($code="") {
    
    // put in database
    
    
    $languages = array(
        'DR' => 'Dari',
        'FR' => 'Farsi',
        'AA' => 'Afar',
        'AB' => 'Abkhazian',
        'AF' => 'Afrikaans',
        'AM' => 'Amharic',
        'AR' => 'Arabic',
        'AS' => 'Assamese',
        'AY' => 'Aymara',
        'AZ' => 'Azerbaijani',
        'BA' => 'Bashkir',
        'BE' => 'Byelorussian',
        'BG' => 'Bulgarian',
        'BH' => 'Bihari',
        'BI' => 'Bislama',
        'BN' => 'Bangla',
        'BO' => 'Tibetan',
        'BR' => 'Breton',
        'CA' => 'Catalan',
        'CO' => 'Corsican',
        'CS' => 'Czech',
        'CY' => 'Welsh',
        'DA' => 'Danish',
        'DE' => 'German',
        'DZ' => 'Bhutani',
        'EN' => 'English',
        'EL' => 'Greek',
        'EO' => 'Esperanto',
        'ES' => 'Spanish',
        'ET' => 'Estonian',
        'EU' => 'Basque',
        'FA' => 'Persian',
        'FI' => 'Finnish',
        'FJ' => 'Fiji',
        'FO' => 'Faeroese',
        'FR' => 'French',
        'FY' => 'Frisian',
        'GA' => 'Irish',
        'GD' => 'Scots/Gaelic',
        'GL' => 'Galician',
        'GN' => 'Guarani',
        'GU' => 'Gujarati',
        'HA' => 'Hausa',
        'HI' => 'Hindi',
        'HR' => 'Croatian',
        'HU' => 'Hungarian',
        'HY' => 'Armenian',
        'IA' => 'Interlingua',
        'IE' => 'Interlingue',
        'IK' => 'Inupiak',
        'IN' => 'Indonesian',
        'IS' => 'Icelandic',
        'IT' => 'Italian',
        'IW' => 'Hebrew',
        'JA' => 'Japanese',
        'JI' => 'Yiddish',
        'JW' => 'Javanese',
        'KA' => 'Georgian',
        'KK' => 'Kazakh',
        'KL' => 'Greenlandic',
        'KM' => 'Cambodian',
        'KN' => 'Kannada',
        'KO' => 'Korean',
        'KS' => 'Kashmiri',
        'KU' => 'Kurdish',
        'KY' => 'Kirghiz',
        'LA' => 'Latin',
        'LN' => 'Lingala',
        'LO' => 'Laothian',
        'LT' => 'Lithuanian',
        'LV' => 'Latvian/Lettish',
        'MG' => 'Malagasy',
        'MI' => 'Maori',
        'MK' => 'Macedonian',
        'ML' => 'Malayalam',
        'MN' => 'Mongolian',
        'MO' => 'Moldavian',
        'MR' => 'Marathi',
        'MS' => 'Malay',
        'MT' => 'Maltese',
        'MY' => 'Burmese',
        'NA' => 'Nauru',
        'NE' => 'Nepali',
        'NL' => 'Dutch',
        'NO' => 'Norwegian',
        'OC' => 'Occitan',
        'OM' => '(Afan)/Oromoor/Oriya',
        'PA' => 'Punjabi',
        'PL' => 'Polish',
        'PS' => 'Pashto/Pushto',
        'PT' => 'Portuguese',
        'QU' => 'Quechua',
        'RM' => 'Rhaeto-Romance',
        'RN' => 'Kirundi',
        'RO' => 'Romanian',
        'RU' => 'Russian',
        'RW' => 'Kinyarwanda',
        'SA' => 'Sanskrit',
        'SD' => 'Sindhi',
        'SG' => 'Sangro',
        'SH' => 'Serbo-Croatian',
        'SI' => 'Singhalese',
        'SK' => 'Slovak',
        'SL' => 'Slovenian',
        'SM' => 'Samoan',
        'SN' => 'Shona',
        'SO' => 'Somali',
        'SQ' => 'Albanian',
        'SR' => 'Serbian',
        'SS' => 'Siswati',
        'ST' => 'Sesotho',
        'SU' => 'Sundanese',
        'SV' => 'Swedish',
        'SW' => 'Swahili',
        'TA' => 'Tamil',
        'TE' => 'Tegulu',
        'TG' => 'Tajik',
        'TH' => 'Thai',
        'TI' => 'Tigrinya',
        'TK' => 'Turkmen',
        'TL' => 'Tagalog',
        'TN' => 'Setswana',
        'TO' => 'Tonga',
        'TR' => 'Turkish',
        'TS' => 'Tsonga',
        'TT' => 'Tatar',
        'TW' => 'Twi',
        'UK' => 'Ukrainian',
        'UR' => 'Urdu',
        'UZ' => 'Uzbek',
        'VI' => 'Vietnamese',
        'VO' => 'Volapuk',
        'WO' => 'Wolof',
        'XH' => 'Xhosa',
        'YO' => 'Yoruba',
        'ZH' => 'Chinese',
        'ZU' => 'Zulu',
    );

    if ($code != "") {
        return (isset($languages[$code])) ? $languages[$code] : false;
    }

    return $languages;
}


function ethnicities($code=""){
    
    $ethnicity_with_group = array(
        'Asian or Asian British' => array(
            'A1' => 'Indian',
            'A2' => 'Pakistani',
            'A3' => 'Bangladeshi',
            'A4' => 'Chinese',
            'A9' => 'Other Asian background',
        ),
        'Black or Black British' => array(
            'B1' => 'Caribbean',
            'B2' => 'African',
            'B9' => 'Other Black background',
        ),
        'Mixed' => array(
            'M1' => 'White and Black Caribbean',
            'M2' => 'White and Black African',
            'M3' => 'White and Asian',
            'M9' => 'Other Mixed background',
        ),
        'White' => array(
            'W1' => 'British',
            'W2' => 'Irish',
            'W3' => 'Gypsy or Irish traveller',
            'W9' => 'Other White background',
        ),
        'Other ethnic group' => array(
            'O1' => 'Arab',
            'O9' => 'Other Ethnic group',
        ),
    );
    
    $ethnicities = array(
        'A1' => 'Indian',
        'A2' => 'Pakistani',
        'A3' => 'Bangladeshi',
        'A4' => 'Chinese',
        'A9' => 'Other Asian background',
        'B1' => 'Caribbean',
        'B2' => 'African',
        'B9' => 'Other Black background',
        'M1' => 'White and Black Caribbean',
        'M2' => 'White and Black African',
        'M3' => 'White and Asian',
        'M9' => 'Other Mixed background',
        'W1' => 'British',
        'W2' => 'Irish',
        'W3' => 'Gypsy or Irish traveller',
        'W9' => 'Other White background',
        'O1' => 'Arab',
        'O9' => 'Other Ethnic group',
    );

    if ($code != "") {
        return (isset($ethnicities[$code])) ? $ethnicities[$code] : false;
    }

    return $ethnicity_with_group;
    
}



function religion(){
    
    $religion = array(
        'Christianity',
        'Islam',
        'Hinduism',
        'Judaism',
        'Sikhism',
        'Buddhism',
        'Religion not stated'
    );

    return $religion;
}


function php_date($date){
    $date_portion = explode('/', $date);
    return $date_portion[2] . '-' . $date_portion[1] . '-' . $date_portion[0];
}



function user(){
    
    $info = array(
        'users.id', 'users.role_id', 'users.email',
        'staffs.id as staff_id', 'staffs.first_name', 'staffs.middle_name', 'staffs.last_name', 'staffs.designation'
    );
    
    return App\Models\User::join('staffs', 'users.staff_id', 'staffs.id')
            ->where('users.id', auth()->id())->first($info);
}

function avatar(){
    $s3 = new App\Modules\S3Module;
    $picture = $s3->picture('staff_profile_picture', user()->staff_id);
    
    //print_r($picture);
    
    return ($picture)?  $picture : asset('img/profile.png');
}


function username($user_id){
    $user = App\Models\User::join('staffs', 'users.staff_id', 'staffs.id')
            ->where('users.id', $user_id)->first(array('users.id', 'staffs.first_name', 'staffs.middle_name', 'staffs.last_name'));
    return ($user)? $user->first_name.' '.$user->middle_name.' '.$user->last_name : false;
}




function engaged($client_id, $service){
    $engaged = App\Models\Engagement::where('client_id', $client_id)->where('service', $service)->count();
    return ($engaged)? true : false;
}

function engage($client_id, $service) {

        DB::beginTransaction();
        try {
            $engaged = App\Models\Engagement::where('client_id', $client_id)->where('service', $service)->count();
            if ($engaged <= 0) {

                $engagement_data = array(
                    'client_id' => $client_id,
                    'service' => $service
                );
                App\Models\Engagement::create($engagement_data);
            }
        } catch (Exception $ex) {
            DB::rollBack();
            return false;
        }
        DB::commit();
        return true;
    }

function fee_services($client_id, $service){

    $services = array();
    if($service == 'education'){
        $data = App\Models\Student::join('schools', 'students.school_id', 'schools.id')
                ->where('client_id', $client_id)
                ->get(array('schools.id as id', 'schools.name as name'));
        
        if($data->count()>0){
            foreach($data as $item){
                $services[] = $item;
            }
        }
    }else {
        $data = App\Models\Engagement::join('services', 'services.service_area', 'engagements.service')
                ->where('services.service_area', $service)
                ->where('client_id', $client_id)
                ->get(array('services.id as id', 'services.name as name'));
        
         if($data->count()>0){
            foreach($data as $item){
                $services[] = $item;
            }
        }
    }

    return $services;

}
