<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

use App\Models\ServiceAccess;
use App\Models\DataAccess;

/**
 * Description of access
 *
 * @author hasan
 */
class access {

    public static function services($area) {
        $access = ServiceAccess::where('user_id', auth()->id())->where('area', $area)->first();

        if ($access) {

            $service = array(
                'type' => $access->access,
                'data' => $access->data,
                'access' => array()
            );

            $services = array();
            if ($access->access == 'custom') {
                //echo $access->id;
                $service_data = DataAccess::where('access_id', $access->id)->get();

                if ($service_data->count() > 0) {

                    foreach ($service_data as $data) {
                        $services[] = $data->service_id;
                    }

                    $service['access'] = $services;
                }
            }



            return $service;
        }

        return false;
    }

    public static function categories($area, $service) {
        $access = ServiceAccess::where('user_id', auth()->id())->where('area', $area)->first();
        if ($access) {

            $category = array(
                'type' => $access->access,
                'data' => $access->data,
                'access' => array()
            );

            if ($access->access == 'custom') {
                $data = DataAccess::where('access_id', $access->id)->where('service_id', $service)->first();

                if ($data) {

                    if ($data->access != '') {
                        $category['access'] = ($data->access != '') ? json_decode($data->access, true) : $access->type;
                    }
                }
            }

            return $category;
        }
        return false;
    }

    public static function data($area) {
        $access = ServiceAccess::where('user_id', auth()->id())->where('service_area', $area)->first();
        return ($access) ? $access->data : 'none';
    }

}
