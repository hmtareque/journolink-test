<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceAccess extends Model {
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_service_accesses';

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

}
