<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;


class Event extends Model {

    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'events';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
       'type', 'event', 'data', 'created_by', 'created_at'
    ];
    
    /**
     * Indicates if the model should be timestamped.
     *
     * @var bool
     */
    public $timestamps = false;

    
    

}
