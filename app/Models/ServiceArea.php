<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ServiceArea extends Model {


    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'service_areas';

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

}
