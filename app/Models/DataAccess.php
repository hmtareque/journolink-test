<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class DataAccess extends Model {
    
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'user_data_accesses';

    /**
     * The attributes that are not mass assignable.
     *
     * @var array
     */
    protected $guarded = [];

}
