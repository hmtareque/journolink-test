<?php

namespace App\Providers;

use App\Models\Permission;


use Illuminate\Support\Facades\Gate;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array
     */
    protected $policies = [
        'App\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('create', function ($user, $module) {
            
            $permission = Permission::where('role_id', $user->role_id)
                    ->where('module_id', $module)
                    ->where('permission', 1)
                    ->first();
            
            return ($permission) ? true : false;
        });
        
        Gate::define('read', function ($user, $module) {
            
            $permission = Permission::where('role_id', $user->role_id)
                    ->where('module_id', $module)
                    ->where('permission', 2)
                    ->first();
            
            return ($permission) ? true : false;
        });
        
        Gate::define('update', function ($user, $module) {
            
            $permission = Permission::where('role_id', $user->role_id)
                    ->where('module_id', $module)
                    ->where('permission', 3)
                    ->first();

            return ($permission) ? true : false;
        });
        
        Gate::define('delete', function ($user, $module) {
            
            $permission = Permission::where('role_id', $user->role_id)
                    ->where('module_id', $module)
                    ->where('permission', 4)
                    ->first();
            
            return ($permission) ? true : false;
        });
    }
}
