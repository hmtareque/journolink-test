<?php

namespace App\Providers;

use Illuminate\Support\Facades\Validator;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        Validator::extend('uk_date', function($attribute, $value, $parameters, $validator) {
            $date = $value;
            if (strpos($date, '/') > 0) {
                $date_portion = explode('/', $date);
              
                if (isset($date_portion[0]) && isset($date_portion[1]) && isset($date_portion[2])) {
                    if(strlen($date_portion[0]) != 2 || strlen($date_portion[1]) != 2 || strlen($date_portion[2]) != 4){
                        return false;
                    }

                    $date = $date_portion[1] . '/' . $date_portion[0] . '/' . $date_portion[2];
                } else {
                    return false;
                }
                
            }
            
            $parsed = date_parse($date);
            // if value matches given format return true=validation succeeded 
            if ($parsed['error_count'] === 0 && $parsed['warning_count'] === 0) {
                return true;
            }
            return false;
        });
        
        Validator::extend('alpha_spaces', function($attribute, $value) {
            return preg_match('/^[\pL\s]+$/u', $value);
        });
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
