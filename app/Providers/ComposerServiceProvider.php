<?php

namespace App\Providers;

use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ComposerServiceProvider extends ServiceProvider
{
    /**
     * Register bindings in the container.
     *
     * @return void
     */
    public function boot()
    {
        // Using class based composers...
        View::composer(['*'], 'App\Http\ViewComposers\AccessComposer');
        View::composer(['auth.roles.create', 'auth.users.create', 'auth.users.login-details'], 'App\Http\ViewComposers\RoleComposer');
        View::composer(['web.posts.create', 'web.posts.edit', 'web.navs.links.create', 'web.navs.links.edit'], 'App\Http\ViewComposers\PostComposer');
        View::composer(['web.navs.links.create', 'web.navs.links.edit'], 'App\Http\ViewComposers\NavComposer');
        
        View::composer(
                ['app.education.classes.create', 
                    'app.education.classes.edit', 
                    'app.education.students.classes.create',
                    'app.education.students.assessments.create',
                    'app.education.reports.student',
                    ], 
                'App\Http\ViewComposers\AcademicYearComposer');
        View::composer(
                ['app.education.classes.create', 
                    'app.education.classes.edit', 
                    'app.education.students.classes.create', 
                    'app.education.students.assessments.create', 
                    'app.education.reports.referral-school',
                    'app.education.reports.service-manager'
                    ], 
                'App\Http\ViewComposers\SchoolComposer'
                );
        View::composer(['app.education.classes.create', 'app.education.classes.edit'], 'App\Http\ViewComposers\CourseComposer');
        View::composer(['app.education.classes.create', 'app.education.classes.edit'], 'App\Http\ViewComposers\StudentGroupComposer');
        View::composer(
                [
                    'app.education.students.create',
                    'app.education.students.edit',
                    'auth.users.create',
                    'auth.users.edit',
                    'app.*.cases.create',
                    'app.*.cases.edit',
                    'app.*.clients.create',
                    'app.*.clients.edit'
                    ],
                'App\Http\ViewComposers\ImmigrationStatusComposer'
                );
        View::composer(
                [
                    'app.education.students.create',
                    'app.education.students.edit',
                    'app.*.cases.create',
                    'app.*.cases.edit',
                    'app.*.clients.create',
                    'app.*.clients.edit'
                    ],
                'App\Http\ViewComposers\HeardAboutUsByComposer'
                );
        
        View::composer(
                [
                    'app.education.students.create',
                    'app.education.students.edit',
                    'app.education.schools.create',
                    'app.education.schools.edit',
                    'app.*.cases.create',
                    'app.*.cases.edit',
                    'app.*.activities.create',
                    'app.*.activities.edit',
                    'app.*.clients.create',
                    'app.*.clients.edit'],
                'App\Http\ViewComposers\BoroughComposer'
                );

    }

    /**
     * Register the service provider.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}