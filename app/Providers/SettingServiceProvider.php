<?php

namespace App\Providers;

use Illuminate\Support\Facades\Schema;
use Illuminate\Support\ServiceProvider;
use Illuminate\Contracts\Cache\Factory;
use App\Models\AppSettings;
use App\Models\WebSettings;

class SettingServiceProvider extends ServiceProvider {

    /**
     * Bootstrap the application services.
     *
     * @return void 
     */
    public function boot(Factory $cache, AppSettings $app, WebSettings $web) {

        if (Schema::hasTable('app_settings') && Schema::hasTable('web_settings')) {
            $app_settings = $cache->remember('settings', 60, function() use ($app) {
                return $app->pluck('value', 'param')->all();
            });

            $web_settings = $cache->remember('app', 60, function() use ($web) {
                return $web->pluck('value', 'param')->all();
            });

            config()->set('settings', $app_settings);
            config()->set('web', $web_settings);
        }
    }

}
