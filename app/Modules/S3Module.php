<?php

namespace App\Modules;

use Illuminate\Support\Facades\Storage;
use Aws\S3\S3Client;
use File;
use App\Models\Doc;

/**
 * Description of DocModule
 *
 * @author hasan
 */
class S3Module {

    //put your code here

    protected $storage;

    public function __construct() {

        $this->storage = 's3';

        $this->host = 'https://s3-' . config('filesystems.disks.s3.region') . '.amazonaws.com/' . config('filesystems.disks.s3.bucket');

        $this->s3 = new S3Client([
            'version' => 'latest',
            'region' => config('filesystems.disks.s3.region'),
            'credentials' => [
                'key' => config('filesystems.disks.s3.key'),
                'secret' => config('filesystems.disks.s3.secret')
            ]
        ]);
    }
    
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function docs($object_type, $object_id) {
        $data = Doc::where('object_type', $object_type)
                ->where('object_id', $object_id)
                ->get();

        if ($data->count() > 0) {
            $files = array();
            foreach ($data as $doc) {

                if ($doc) {
                    $files = (array) json_decode($doc->files, true);
                    $items = array();
                    if (count($files) > 0) {
                        foreach ($files as $key => $file) {
                            $file_name = (isset($file['name'])) ? $file['name'] : '';
                            $items[$key] = array(
                                'name' => $file_name,
                                'link' => $this->host . '/' . $doc->folder . '/' . $file_name
                            );
                        }
                    }

                    $docs[] = (object) array(
                                'id' => $doc->id,
                                'name' => $doc->name,
                                'files' => $items
                    );
                }
            }

            return $docs;
        }
        return false;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function doc($object_type, $object_id) {
        $doc = Doc::where('object_type', $object_type)
                ->where('object_id', $object_id)
                ->first();

        if ($doc) {
            
            $files = (array)json_decode($doc->files, true);
            $items = array();
            if(count($files)>0){
                foreach($files as $key => $file){
                    $items[$key] = array(
                        'name' => $file['name'],
                        'link' => $this->host.'/'.$doc->folder.'/'.$file['name']
                    );
                }
            }

            $docs = array(
                'id' => $doc->id,
                'name' => $doc->name,
                'files' => $items
            );
            return (object) $docs;
        }
        
        return false;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function add($doc_id, $files, $private = false) {

        $doc = Doc::where('id', $doc_id)->first();

        $doc_files = (array) json_decode($doc->files, true);

        if (count($files) > 0) {
            foreach ($files as $file) {
                $file_name = $file->getClientOriginalName();
                $name = strtolower(date('Y_m_d_His') . '_' . str_replace(' ', '_', trim($file_name)));
                $security = ($private) ? 'private' : 'public';
                Storage::disk('s3')->put($doc->folder . '/' . $name, file_get_contents($file), $security);

                $doc_files[uniqid()] = array(
                    'name' => $name,
                    'ext' => File::extension($name),
                );
            }

            $update_data = array(
                'files' => json_encode($doc_files),
                'private' => $private,
                'updated_at' => date('Y-m-d H:i:s'),
                'updated_by' => auth()->id()
            );

            return Doc::where('id', $doc_id)->update($update_data);
        }
        return false;
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($object_type, $object_id, $folder, $doc_name, $files, $private = false) {
        if (count($files) > 0) {
            $file_to_upload = array();
            foreach ($files as $file) {
                $file_name = $file->getClientOriginalName();
                $name = strtolower(date('Y_m_d_His') . '_' . str_replace(' ', '_', trim($file_name)));
                $security = ($private) ? 'private' : 'public';
                Storage::disk('s3')->put($folder . '/' . $name, file_get_contents($file), $security);
                $file_to_upload[uniqid()] = array(
                    'name' => $name,
                    'ext' => File::extension($name),
                );
            }

            $doc_data = array(
                'storage' => $this->storage,
                'object_type' => $object_type,
                'object_id' => $object_id,
                'folder' => $folder,
                'name' => $doc_name,
                'files' => json_encode($file_to_upload),
                'private' => $private,
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => auth()->id()
            );

            return Doc::create($doc_data);
        }

        return false;
    }

    public function storePicture($object_type, $object_id, $folder, $doc_name, $file, $private = false) {

        $file_name = $file->getClientOriginalName();
        $name = strtolower(date('Y_m_d_His') . '_' . str_replace(' ', '_', trim($file_name)));
        Storage::disk('s3')->put($folder . '/' . $name, file_get_contents($file), 'public');
        
        $file_to_upload = array(
            'name' => $name,
            'ext' => File::extension($name),
        );
        
        $has_file = Doc::where('object_type', $object_type)->where('object_id', $object_id)->first();
         
        if($has_file){
            Storage::disk('s3')->delete($has_file->folder . '/' . $has_file->name);
            
            $doc_data = array(
           
            'files' => json_encode($file_to_upload),
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => auth()->id()
        );
            
            return Doc::where('id', $has_file->id)->update($doc_data);
            
        }

        $doc_data = array(
            'storage' => $this->storage,
            'object_type' => $object_type,
            'object_id' => $object_id,
            'folder' => $folder,
            'name' => $doc_name,
            'files' => json_encode($file_to_upload),
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => auth()->id()
        );

        return Doc::create($doc_data);
    }
    
    public function picture($object_type, $object_id){
        $picture = Doc::where('object_type', $object_type)->where('object_id', $object_id)->first();
        
        if($picture){
            $file = json_decode($picture->files, true);
            return $this->host.'/'.$picture->folder.'/'.$file['name'];
        }
        
        return false;
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function getSignedUrl($file_path) {
        $object = array(
            'Bucket' => config('filesystems.disks.s3.bucket'),
            'Key' => $file_path
        );

        $cmd = $this->s3->getCommand('GetObject', $object);
        $signed_url = $this->s3->createPresignedRequest($cmd, '+60 minutes');

        return (string) $signed_url->getUri();
    }

    /**
     * remove single file
     * 
     * @param type $object_type
     * @param type $object_id
     * @param type $doc_id
     * @param type $file_id
     * @return type
     */
    public function remove($doc_id, $file_id) {

        $doc = Doc::where('id', $doc_id)->first();
        $files = json_decode($doc->files);
        $file = $files->$file_id;
  
        Storage::disk('s3')->delete($doc->folder . '/' . $file->name);

        $doc_files = (array) $files;
        unset($doc_files[$file_id]);

        $update_data = array(
            'files' => json_encode($doc_files),
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => auth()->id()
        );

        return Doc::where('id', $doc_id)->update($update_data);
    }

    /**
     * Remove all files of a doc
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($doc_id) {
        $doc = Doc::where('id', $doc_id)->first();
        Storage::disk('s3')->delete($doc->folder);
        return Doc::where('id', $doc_id)->forceDelete();
    }

}
