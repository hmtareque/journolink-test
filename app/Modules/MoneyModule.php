<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modules;

use DB;
use App\Models\Fee;
use App\Models\Payment;

/**
 * Description of MoneyModule
 *
 * @author Hasan
 */
class MoneyModule {
    
    //fee 
    
    public function serviceAccountSummary($client_id, $service) {
        $fees = Fee::where('client_id', $client_id)->where('service_area', $service)->get();

        $exempt = 0;
        $due = 0;

        $paid_fees = array();

        if ($fees->count() > 0) {
            foreach ($fees as $fee) {
                $paid_fees[] = $fee->id;
                $fee->exempt_amount = ($fee->exempt == 1) ? $fee->fee : 0;
                $fee->amount = ($fee->exempt == 0) ? $fee->fee - $fee->discount : 0;
                $fee->paid = Payment::where('fee_id', $fee->id)->sum('amount');
                $fee->balance = ($fee->exempt == 0) ? $fee->amount - $fee->paid : 0;

                $exempt += $fee->exempt_amount;
                $due += $fee->balance;
            }
        }

        $recent_payments = Payment::whereIn('fee_id', $paid_fees)->orderBy('created_at', 'desc')->take(5)->get();

        $account = array(
            'client_id' => $client_id,
            'service' => $service,
            'recent_payments' => $recent_payments,
            'exempt' => $exempt,
            'due' => $due
        );

        return (object) $account;
    }

    public function fees($client_id, $service) {
        $fees = Fee::where('client_id', $client_id)->where('service_area', $service)->get();

        $total_fee = 0;
        $total_exempt = 0;
        $total_paid = 0;
        $total_discount = 0;
        $balance = 0;

        foreach ($fees as $fee) {
            $fee->exempt_amount = ($fee->exempt == 1) ? $fee->fee : 0;
            $fee->amount = ($fee->exempt == 0) ? $fee->fee - $fee->discount : 0;
            $fee->paid = Payment::where('fee_id', $fee->id)->sum('amount');
            $fee->balance = ($fee->exempt == 0) ? $fee->amount - $fee->paid : 0;

            $total_fee += $fee->fee;
            $total_exempt += $fee->exempt_amount;
            $total_paid += $fee->paid;
            $total_discount += $fee->discount;
            $balance += $fee->balance;
        }

        $fees->total_fee = $total_fee;
        $fees->total_exempt = $total_exempt;
        $fees->total_paid = $total_paid;
        $fees->total_discount = $total_discount;
        $fees->balance = $balance;


        return $fees;
    }

    public function storeFee($request){
        
        $exempt = $request->get('exempt_from_fee');
        
        $fee_data = array(
            'client_id' => $request->get('client'),
            'service_area' => $request->get('service'),
             'service_id' => $request->get('fee_for_service'),
            'description' => $request->get('description'),
            'fee' => $request->get('fee'),
            'exempt' => $exempt,
            'discount' => ($exempt == 0 && trim($request->get('discount')) != '')? $request->get('discount') : 0,
            'reason_for_exemption' => ($exempt == 1)? $request->get('exemption_reason') : NULL,
            'created_by' => auth()->id()
        );
        
        return Fee::create($fee_data);
    }
    
    public function updateFee($fee_id, $request){
        
        $exempt = $request->get('exempt_from_fee');
        
        $fee_data = array(
            'service_id' => $request->get('fee_for_service'),
            'description' => $request->get('description'),
            'fee' => $request->get('fee'),
            'discount' => ($exempt != 1 && trim($request->get('discount')) != '')? $request->get('discount') : 0,
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => auth()->id()
        );
        
        if($request->has('exempt_from_fee')){
            $fee_data['exempt'] = $exempt;
            $fee_data['reason_for_exemption'] = ($exempt == 1)? $request->get('exemption_reason') : NULL;
        }

        return Fee::where('id', $fee_id)->update($fee_data);
    }
    
    
     public function fee($fee_id){
        $fee = Fee::where('id', $fee_id)->first();
        $fee->payments = Payment::where('fee_id', $fee_id)->get();
        $fee->paid = Payment::where('fee_id', $fee_id)->sum('amount');
        $fee->due = ($fee->fee - $fee->discount) - $fee->paid;
        
        return $fee;
    }
    
    public function removeFee($fee_id){
        $fee = Payment::where('id', $fee_id)->first();
        
        # todo check type 
        
        DB::beginTransaction();
        try {
            
            Fee::where('id', $fee_id)->update(array('deleted_by' => auth()->id()));
            Fee::where('id', $fee_id)->delete();
            
        } catch (Exception $ex) {
            DB::rollBack();
            return false;
        }
        
        DB::commit();
        return true;
        
        
    }
    
    
    public function storePayment($fee_id, $request){
        
        
        $payment_data = array(
            'fee_id' => $fee_id,
            'description' => $request->get('description'),
            'payment_method' => $request->get('payment_method'),
            'amount' => $request->get('amount'),
            'has_donation' => $request->get('donate'),
            'donation_amount' => ($request->get('donate') == 1)? $request->get('donation_amount') : 0.00,
            'created_by' => auth()->id()
        );
        
        return Payment::create($payment_data);
        
        
    }
    
    public function payment($payment_id){
        return Payment::where('id', $payment_id)->first();
    }
    
    
    public function updatePayment($payment_id, $request){
        
        $payment_data = array(
            'description' => $request->get('description'),
            'payment_method' => $request->get('payment_method'),
            'amount' => $request->get('amount'),
            'has_donation' => $request->get('donate'),
            'donation_amount' => ($request->get('donate') == 1)? $request->get('donation_amount') : 0.00,
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => auth()->id()
        );
        
        return Payment::where('id', $payment_id)->update($payment_data);

    }
    
    
    
    public function removePayment($payment_id){
        $payment = Payment::where('id', $payment_id)->first();
        
        # todo check type 
        
        DB::beginTransaction();
        try {
            
            Payment::where('id', $payment_id)->update(array('deleted_by' => auth()->id()));
            Payment::where('id', $payment_id)->delete();
            
        } catch (Exception $ex) {
            DB::rollBack();
            return false;
        }
        
        DB::commit();
        return true;
        
        
    }
    
    
    
}