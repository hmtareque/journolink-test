<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modules;

use DB;
use Illuminate\Support\Facades\Auth;

use App\Models\WebSettings;
use App\Models\PostCategory;
use App\Models\Post;
use App\Models\Nav;
use App\Models\Link;
use App\Models\WebComponent;
use App\Models\WebComponentItem;
use App\Models\WebContact;
use App\Models\Contact;

use Illuminate\Support\Facades\Storage;


/**
 * Description of WebsiteModule
 *
 * @author hasan
 */
class WebsiteModule {

    public function settings(){
        return WebSettings::all();
    }
    
    public function storeSetting(array $data) {
        
        $setting_data = array(
            'param' => strtolower(str_replace(" ", "_", trim($data['parameter']))),
            'type' => $data['type']
        );

        $setting_data['value'] = NULL;
        if ($data['type'] === "image") {
            $setting_data['value'] = Storage::disk('public')->put('images/web/settings', $data['setting_image']);
        } elseif (isset($data['setting_value'])) {
            $setting_data['value'] = trim($data['setting_value']);
        }

        if (isset($data['setting_options']) && trim($data['setting_options']) != "") {
            $setting_data['options'] = trim($data['setting_options']);
        }

        return WebSettings::create($setting_data);
    }

    public function updateSettings(array $data){
        
        DB::beginTransaction();
        try {
            foreach($data as $param => $value){
                
                $setting = WebSettings::where('param', $param)->first();
                
                if($setting){
                
                if($setting->type === "text" || $setting->type === "option"){
                    WebSettings::where('param', $param)->update(array('value' => trim($value)));
                }
                
                if($setting->type === "image"){
                       if(is_object($value)){
                        $image = Storage::disk('public')->put('images/web/settings', $value);
                        WebSettings::where('param', $param)->update(array('value' => trim($image)));
                       }
                    }
                }
            }
        } catch (Exception $ex) {
            DB::rollBack();
            return false;
        }
        
        DB::commit();
        return true;
    }
    
     public function deleteSetting($id){
        return WebSettings::where('id', $id)->forceDelete();
    }
    
    /////////////// post category /////////////
    public function postCategories(){
        
        $categories = PostCategory::orderBy('name', 'asc')->get(array('id', 'name', 'visible'));
        
        if($categories->count() > 0){
            foreach($categories as $category){
                $category->no_of_posts = Post::where('category_id', $category->id)->count();
            }
        }
        
        return $categories;
    }
    
    public function postsOfCategory($category_id){
        return Post::where('category_id', $category_id)->get();
    }
    
    public function postCategory($id){
        return PostCategory::find($id);
    }
    
    public function storePostCategory(array $data){
        
        $post_category_data = array(
            'name' => $data['name'],
            'route' => str_slug(trim($data['name']), '-'),
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => Auth::user()->id
         );
        
        return PostCategory::create($post_category_data);
    }
    
    public function updatePostCategory(array $data, $id){
        
        $post_category_data = array(
            'name' => $data['name'],
            'route' => str_slug(trim($data['name']), '-'),
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => Auth::user()->id
         );
        
        PostCategory::where('id', $id)->update($post_category_data);
        
        return PostCategory::find($id);
    }
    
    
    public function makeVisiblePostCategory($id){
        $post_category_data = array(
            'visible' => 1,
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => Auth::user()->id
         );
        
        PostCategory::where('id', $id)->update($post_category_data);
        
        return PostCategory::find($id);
    }
    
    public function hidePostCategory($id){
       $post_category_data = array(
            'visible' => 0,
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => Auth::user()->id
         );
        
        PostCategory::where('id', $id)->update($post_category_data);
        
        return PostCategory::find($id);
    }
    
    public function deletePostCategory($id){
        
        $post_category_data = array(
            'deleted_by' => Auth::user()->id
         );
        
        PostCategory::where('id', $id)->update($post_category_data);
        
        return PostCategory::where('id', $id)->delete();
    }
    
    
    
    
    ////////////// post ///////////////
    
    public function posts(){
        $info = array('posts.id', 'posts.title', 'posts.route', 'post_categories.name as category', 'posts.published');
        return Post::leftJoin('post_categories', 'posts.category_id', '=', 'post_categories.id')
                ->orderBy('posts.created_at', 'desc')
                ->get($info);
    }
    
    public function post($id){
        return Post::find($id);
    }
    
    public function postBySlug($route){
        return Post::where('route', $route)->where('published', 1)->first();
    }
    
    public function storePost(array $data){
        $post_data = array(
            'category_id' => $data['post_category'],
            'title' => $data['title'],
            'route' => str_slug(trim($data['title']).'-'.date('Y-m-d-H-i-s'), '-'),
            'meta_keywords' => $data['meta_keywords'],
            'meta_description' => $data['meta_description'],
            'content' => $data['content'],
            'published' => (isset($data['do_not_publish']))? 0 : 1,
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => Auth::user()->id
        );
        
        return Post::create($post_data);
    }
    
    public function updatePost(array $data, $id){
        $post_data = array(
            'category_id' => $data['post_category'],
            'title' => $data['title'],
            'meta_keywords' => $data['meta_keywords'],
            'meta_description' => $data['meta_description'],
            'content' => $data['content'],
            'published' => (isset($data['do_not_publish']))? 0 : 1,
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => Auth::user()->id
        );
        
        Post::where('id', $id)->update($post_data);
        
        return Post::find($id);
    }
    
    public function publishPost($id){
        
        $publish_data = array(
            'published' => 1, 
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => Auth::user()->id
        );
        
        Post::where('id', $id)->update($publish_data);
        
        return Post::find($id);
    }
    
    public function unpublishPost($id){
        
        $publish_data = array(
            'published' => 0, 
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => Auth::user()->id
        );
        
        Post::where('id', $id)->update($publish_data);
        
        return Post::find($id);
    }
    
    public function deletePost($id){
        $post_data = array(
            'deleted_by' => Auth::user()->id
         );
        
        Post::where('id', $id)->update($post_data);
        
        return Post::where('id', $id)->delete();
    }
    
    
    ////////////// menu //////////////
    
    public function navs(){
        
        $navs = Nav::orderBy('name')->get(array('id', 'name', 'route', 'visible'));
        
        if($navs->count()){
            foreach($navs as $nav){
                $nav->no_of_links = Link::where('nav_id', $nav->id)->count();
            }
        }
        
        return $navs;
    }
    
    public function navLinks($nav_id){
        return Link::where('nav_id', $nav_id)->get();
    }
    
    public function nav($id){
        return Nav::find($id);
    }
    
    public function storeNav(array $data){
        $nav_data = array(
            'name' => $data['name'],
            'route' => str_slug(trim($data['name']), '-'),
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => Auth::user()->id
         );
        
        return Nav::create($nav_data);
    }
    
    public function updateNav(array $data, $id){
        $nav_data = array(
            'name' => $data['name'],
            'route' => str_slug(trim($data['name']), '-'),
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => Auth::user()->id
         );
        
        Nav::where('id', $id)->update($nav_data);
        
        return Nav::find($id);
    }
    
    public function makeNavVisible($id){
        $nav_data = array(
            'visible' => 1, 
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => Auth::user()->id
        );
        
        Nav::where('id', $id)->update($nav_data);
        
        return Post::find($id);
    }
    
    public function hideNav($id){
        $nav_data = array(
            'visible' => 0, 
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => Auth::user()->id
        );
        
        Nav::where('id', $id)->update($nav_data);
        
        return Nav::find($id);
    }
    
    public function deleteNav($id){
        $nav_data = array(
            'deleted_by' => Auth::user()->id
        );
        
        Nav::where('id', $id)->update($nav_data);
        
        return Nav::where('id', $id)->delete();
    }
    
    
    //////////////// links ///////////////
    
    public function links(){
        return Link::all();
    }
    
    public function linksOfNav($nav_id){
        return Link::where('nav_id', $nav_id)
                ->orderBy('sort', 'asc')
                ->get();
    }
    
    public function link($id){
        return Link::find($id);
    }
    
    public function storeLink(array $data){
        
        $link = "";
        if($data['type'] == "post"){
            $link = $data['post_link'];
        }
        
        if($data['type'] == "internal"){
            $link = $data['internal_link'];
        }
        
        if($data['type'] == "external"){
            $link = $data['external_link'];
        }
        
        if($data['type'] == "nav"){
            $link = $data['nav_link'];
        }

        $link_data = array(
            'nav_id' => $data['nav'],
            'name' => $data['name'],
            'type' => $data['type'],
            'link' => $link,
            'target' => (isset($data['open_in_new_window']))? 1 : 0,
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => Auth::user()->id
         );
        
        return Link::create($link_data);
    }
    
    public function updateLink(array $data, $id){

        $link = "";
        if($data['type'] == "post"){
            $link = $data['post_link'];
        }
        
        if($data['type'] == "internal"){
            $link = $data['internal_link'];
        }
        
        if($data['type'] == "external"){
            $link = $data['external_link'];
        }
        
        if($data['type'] == "nav"){
            $link = $data['nav_link'];
        }

        $link_data = array(
            'nav_id' => $data['nav'],
            'name' => $data['name'],
            'type' => $data['type'],
            'link' => $link,
            'target' => (isset($data['open_in_new_window']))? 1 : 0,
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => Auth::user()->id
         );
        
        Link::where('id', $id)->update($link_data);
        
        return Link::find($id);
    }
    
    
    public function makeLinkVisible($id){
        $nav_data = array(
            'visible' => 1, 
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => Auth::user()->id
        );
        
        Link::where('id', $id)->update($nav_data);
        
        return Link::find($id);
    }
    
    public function hideLink($id){
        $nav_data = array(
            'visible' => 0, 
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => Auth::user()->id
        );
        
        Link::where('id', $id)->update($nav_data);
        
        return Link::find($id);
    }
    
    public function deleteLink($id){
        return Link::where('id', $id)->forceDelete();
    }
    
    
    //components 
    
    public function allComponents(){
        $components = WebComponent::orderBy('name', 'asc')->get(array('id', 'name', 'route', 'visible'));
        
        if($components->count()>0){
        foreach($components as $component){
            $component->no_of_items = WebComponentItem::where('component_id', $component->id)->count();
        }
        }
        return $components;
    }
    
    public function component($id){
        $component = WebComponent::find($id);
        
        if($component){
            $component_data = array(
                'id' => $component->id,
                'name' => $component->name,
                'properties' => json_decode($component->properties),
            );
            
            return (object)$component_data;
        }
        
        return false;
    }
    
    public function storeComponent(array $data){
        
        $properties = array();
        if(count($data['property'])>0){
            foreach($data['property'] as $key => $property){
                $properties[] = array(
                    'id' => $key,
                    'name' => $property['name'],
                    'type' => $property['type'],
                    'options' => $property['options'],
                    'required' => (isset($property['required']))? 1 : 0,
                    'visible' => 1 
                );
            }
        }

        $component_data = array(
            'name' => $data['name'],
            'route' => str_slug(trim($data['name']), '-'),
            'properties' => json_encode($properties),
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => Auth::user()->id
        );
        
        return WebComponent::create($component_data);
    }
    
    
    public function updateComponent(array $data, $id){
        
        $properties = array();
        if(count($data['property'])>0){
            foreach($data['property'] as $key => $property){
                $properties[] = array(
                    'id' => $key,
                    'name' => $property['name'],
                    'type' => $property['type'],
                    'options' => $property['options'],
                    'required' => (isset($property['required']))? 1 : 0,
                    'visible' => $property['visible'] 
                );
            }
        }

        $component_data = array(
            'name' => $data['name'],
            'route' => str_slug(trim($data['name']), '-'),
            'properties' => json_encode($properties),
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => Auth::user()->id
        );
        
        WebComponent::where('id', $id)->update($component_data);
        
        return WebComponent::find($id);
    }
    
    public function makeComponentVisible($id){
        $component_data = array(
            'visible' => 1, 
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => Auth::user()->id
        );
        
        WebComponent::where('id', $id)->update($component_data);
        
        return WebComponent::find($id);
    }
    
    public function hideComponent($id){
        $component_data = array(
            'visible' => 0, 
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => Auth::user()->id
        );
        
        WebComponent::where('id', $id)->update($component_data);
        
        return WebComponent::find($id);
    }
    
    public function deleteComponent($id){
        
        $component_data = array(
            'deleted_by' => Auth::user()->id
        );
        
        WebComponent::where('id', $id)->update($component_data);
        
        return WebComponent::where('id', $id)->delete();
    }
    
    
    ////// component items 
    
     public function allComponentItems($component_id){
        return WebComponentItem::where('component_id', $component_id)->get();
    }
    
    public function componentItems($component_id){
        return WebComponentItem::where('component_id', $component_id)->get();
    }
    
    public function componentItem($id){
        $item = WebComponentItem::find($id);
        
        $component = WebComponent::where('id', $item->component_id)->first();
        
        if($component){
            $item_values = (array)json_decode($item->item, true);
            $properties = json_decode($component->properties);
            
            ksort($item_values);
            
            $item_properties = array();
            foreach($properties as $property){
                if ($property->visible == 1) {
                    $value = (isset($item_values[$property->id])) ? $item_values[$property->id] : '';
                    $item_properties[$property->id] = (object) array(
                                'id' => $property->id,
                                'name' => $property->name,
                                'type' => $property->type,
                                'options' => $property->options,
                                'value' => $value     
                    );
                }
            }
        }
        
        ksort($item_properties);
        
        if($item){
            $item_data = array(
                'id' => $item->id,
                'component_id' => $item->component_id,
                'component_name' => $component->name,
                'identifier' => $item->identifier,
                'properties' => $item_properties,
            );
            
            return (object)$item_data;
        }
        
        return false;

    }
    
    public function storeComponentItem(array $data, $component_id){
        
        $item = array();
        if(count($data['item'])>0){
            foreach($data['item'] as $id => $value){
                if(is_object($value)){
                   $item[$id] = Storage::disk('public')->put('images/components', $value);
                } else {
                   $item[$id] = $value; 
                }  
            }
        }
        
        ksort($item);
        
        $item_data = array(
            'component_id' => $component_id,
            'identifier' => $data['identifier'],
            'route' => str_slug(trim($data['identifier']), '-'),
            'item' => json_encode($item),
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => Auth::user()->id
        );
        
        return WebComponentItem::create($item_data);
    }
    
    
    public function updateComponentItem(array $data, $item_id){

        $item = array();
        if(count($data['item'])>0){
            foreach($data['item'] as $id => $value){
                if(is_object($value)){
                   $item[$id] = Storage::disk('public')->put('images/components', $value);
                } else {
                   $item[$id] = $value; 
                }  
            }
        }
        
        ksort($item);
        
        $item_data = array(
            'identifier' => $data['identifier'],
            'route' => str_slug(trim($data['identifier']), '-'), 
            'item' => json_encode($item),
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => Auth::user()->id
        );
        
        return WebComponentItem::where('id', $item_id)->update($item_data);
    }
    
    public function makeComponentItemVisible($id){
        $item_data = array(
            'visible' => 1, 
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => Auth::user()->id
        );
        
        WebComponentItem::where('id', $id)->update($item_data);
        
        return WebComponentItem::find($id);
    }
    
    public function hideComponentItem($id){
        $item_data = array(
            'visible' => 0, 
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => Auth::user()->id
        );
        
        WebComponentItem::where('id', $id)->update($item_data);
        
        return WebComponentItem::find($id);
    }
    
    public function deleteComponentItem($id){
        
        $item_data = array(
            'deleted_by' => Auth::user()->id
        );
        
        WebComponentItem::where('id', $id)->update($item_data);
        
        return WebComponentItem::where('id', $id)->delete();
    }
    

    ///////// contacts ///////////
    
    public function updateContacts($data){
        
        unset($data['_token']);
        
        DB::beginTransaction();
        try {
            foreach ($data as $param => $value){
            Contact::where('param', $param)->update(array('value' => $value, 'updated_at' => date('Y-m-d H:i:s'), 'updated_by' => Auth::user()->id));
        }
        } catch (Exception $ex) {
            DB::rollBack();
            return false;
        }
        
        DB::commit();
        return true; 
    }
    
    public function contacts(){
        $contacts = Contact::get(array('param', 'value'));
        
        $data = array();
        foreach($contacts as $contact){
            $data[$contact->param] = $contact->value;
        }
        return (object)$data;
    }
    
    public function webContacts(){
        return WebContact::take(100)
                ->orderBy('created_at', 'desc')
                ->get();
    }

}
