<?php

namespace App\Modules;

use DB;
use Illuminate\Support\Facades\Auth;
use App\Models\Module;
use App\Models\User;
use App\Models\Role;
use App\Models\Permission;
use App\Models\Staff;

use App\Models\ServiceAccess;
use App\Models\DataAccess;


/**
 * Description of UserModule
 *
 * @author hasan
 */
class AuthModule {
    
    public function __construct(S3Module $s3) {
        $this->s3 = $s3;
    }

    public function modules() {
        return Module::all();
    }
    
    
    public function profile(){
        $info = array(
            'users.id as id',
            'staffs.id as staff_id',
            'staffs.*',
            'users.*',
        );

        $user = User::join('staffs', 'users.staff_id', '=', 'staffs.id')
                        ->where('users.id', auth()->id())
                        ->first($info);
        
        $user->profile_picture = $this->s3->picture('staff_profile_picture', $user->staff_id);
        
        return $user;
    }
    
    

    public function allStaff() {

        $info = array(
            'users.id as id',
            'staffs.id as staff_id',
            'staffs.first_name',
            'staffs.last_name',
            'staffs.status as status',
            'staffs.designation',
            'staffs.phone',
            'staffs.mobile',
            'staffs.email',
            'last_logged_in_at',
            'staffs.status',
            'users.active',
        );

        return User::join('staffs', 'users.staff_id', '=', 'staffs.id')
                        ->leftJoin('roles', 'roles.id', '=', 'users.role_id')
                        ->orderBy('staffs.first_name')
                        ->orderBy('staffs.status', 'asc')
                        ->orderBy('users.active', 'desc')
                        ->get($info);
    }
    
    public function activeUsers() {

        $info = array(
            'users.id as id',
            'staffs.id as staff_id',
            'staffs.first_name',
            'staffs.last_name',
            'staffs.designation'
        );

        return User::join('staffs', 'users.staff_id', '=', 'staffs.id')
                        ->join('roles', 'roles.id', '=', 'users.role_id')
                        ->where('staffs.status', 'active')
                        ->where('users.active', 1)
                        ->orderBy('first_name', 'asc')
                        ->get($info);
    }

    public function user($id) {

        $info = array(
            'users.id as id',
            'staffs.id as staff_id',
            'staffs.*',
            'users.*',
        );

        return User::join('staffs', 'users.staff_id', '=', 'staffs.id')
                        ->where('users.id', $id)
                        ->first($info);
    }

    public function storeUser($data) {

       
        
        DB::beginTransaction();

        try {
            $staff_data = array(
                'first_name' => $data->get('first_name'),
                'middle_name' => $data->get('middle_name'),
                'last_name' => $data->get('last_name'),
                'ni_number' => $data->get('ni_number'),
                'date_of_birth' => date('Y-m-d', strtotime(php_date($data->get('date_of_birth')))),
                'gender' => $data->get('gender'),
                'other_gender' => ($data->get('gender') === "other") ? $data->get('other_gender') : NULL,
                'address_line_1' => $data->get('address_line_1'),
                'address_line_2' => $data->get('address_line_2'),
                'street' => $data->get('street'),
                'city' => $data->get('city'),
                'postcode' => $data->get('postcode'),
                'phone' => $data->get('phone'),
                'mobile' => $data->get('mobile'),
                'email' => $data->get('email_address'),
                'immigration_status' => $data->get('immigration_status'),
                'other_immigration_status' => ($data->get('immigration_status') === "other") ? $data->get('other_immigration_status') : NULL,
                'passport_no' => $data->get('passport_no'),
                'visa_expiry_date' => ($data->get('visa_expiry_date') != '') ? date('Y-m-d', strtotime(php_date($data->get('visa_expiry_date')))) : NULL,
                'immigration_note' => $data->get('immigration_note'),
                'joined_at' => ($data->get('joined_at') != '') ? date('Y-m-d', strtotime(php_date($data->get('joined_at')))) : NULL,
                'designation' => $data->get('designation'),
                'has_paid_role' => $data->get('paid_role'),
                'salary_type' => $data->get('salary_type'),
                'salary_amount' => ($data->get('paid_role') == 1)? $data->get('salary_amount') : 0,
                'salary_description' => $data->get('salary_description'),
                'dbs_number' => $data->get('dbs_number'),
                'dbs_issue_date' => ($data->get('dbs_issue_date') != '') ? date('Y-m-d', strtotime(php_date($data->get('dbs_issue_date')))) : NULL,
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => Auth::user()->id
            );

            $staff = Staff::create($staff_data);
            
            $user_data = array(
                'staff_id' => $staff->id,
                'role_id' => ($data->get('email') != "")? $data->get('role') : NULL,
                'email' => ($data->get('email') != "")? $data->get('email') : NULL,
                'password' => ($data->get('email') != "")? bcrypt($data->get('password')) : NULL,
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => Auth::user()->id
            );
            
            $user = User::create($user_data);

            // access
            $accesses = $data->get('access');
            foreach ($accesses as $area => $access) {
                $service_access_data = array(
                    'user_id' => $user->id,
                    'area' => $area,
                    'access' => $access['type'],
                    'data' => $access['data']
                );
                $new_access = ServiceAccess::create($service_access_data);

                if ($access['type'] == 'custom' && isset($access['access']) && is_array($access['access'])) {

                        foreach ($access['access'] as $service_id => $service_access) {
                            $data_access_data = array(
                                'access_id' => $new_access->id,
                                'service_id' => $service_id,
                                'access' => (is_array($service_access)) ? json_encode($service_access) : NULL,
                                
                            );
                            
                            DataAccess::create($data_access_data);
                        }
                }
            }
            
           } catch (Exception $ex) {
            DB::rollBack();
            return false;
        }

        DB::commit();
        return $user;
    }
    
    
    public function updateUser($user_id, $data) {

        DB::beginTransaction();

        try {

            $user = User::find($user_id);
            if ($user) {
                $staff_data = array(
                    'first_name' => $data->get('first_name'),
                    'middle_name' => $data->get('middle_name'),
                    'last_name' => $data->get('last_name'),
                    'ni_number' => $data->get('ni_number'),
                    'date_of_birth' => date('Y-m-d', strtotime(php_date($data->get('date_of_birth')))),
                    'gender' => $data->get('gender'),
                    'other_gender' => ($data->get('gender') === "other") ? $data->get('other_gender') : NULL,
                    'address_line_1' => $data->get('address_line_1'),
                    'address_line_2' => $data->get('address_line_2'),
                    'street' => $data->get('street'),
                    'city' => $data->get('city'),
                    'postcode' => $data->get('postcode'),
                    'phone' => $data->get('phone'),
                    'mobile' => $data->get('mobile'),
                    'immigration_status' => $data->get('immigration_status'),
                    'other_immigration_status' => ($data->get('immigration_status') === "other") ? $data->get('other_immigration_status') : NULL,
                    'passport_no' => $data->get('passport_no'),
                    'visa_expiry_date' => ($data->get('visa_expiry_date') != '') ? date('Y-m-d', strtotime(php_date($data->get('visa_expiry_date')))) : NULL,
                    'immigration_note' => $data->get('immigration_note'),
                    'joined_at' => ($data->get('joined_at') != '') ? date('Y-m-d', strtotime(php_date($data->get('joined_at')))) : NULL,
                    'designation' => $data->get('designation'),
                    'has_paid_role' => $data->get('has_paid_role'),
                    'salary_type' => $data->get('salary_type'),
                    'salary_amount' => $data->get('salary_amount'),
                    'salary_description' => $data->get('salary_description'),
                    'dbs_number' => $data->get('dbs_number'),
                    'dbs_issue_date' => ($data->get('dbs_issue_date') != '') ? date('Y-m-d', strtotime(php_date($data->get('dbs_issue_date')))) : NULL,
                    'updated_by' => Auth::user()->id
                );

                Staff::where('id', $user->staff_id)->update($staff_data);
            }
        } catch (Exception $ex) {
            DB::rollBack();
            return false;
        }

        DB::commit();
        return $user;
    }

    public function userLoginDetails($user_id) {

        $info = array(
            'users.id as id',
            'staffs.id as staff_id',
            'staffs.first_name',
            'staffs.middle_name',
            'staffs.last_name',
            'users.email',
            'users.role_id',
            'users.active'
        );

        return User::join('staffs', 'users.staff_id', '=', 'staffs.id')
                        ->where('users.id', $user_id)
                        ->first($info);
    }
    
    public function updateLoginDetails($id, array $data) {

        DB::beginTransaction();

        try {

            $user_data = array(
                'role_id' => $data['role'],
                'email' => $data['email'],
                'password' => bcrypt($data['password']),
                'updated_by' => Auth::user()->id
            );

            User::where('id', $id)->update($user_data);

        } catch (Exception $ex) {
            DB::rollBack();
            return false;
        }

        DB::commit();
        return true;
    }
    
    public function blockUser($user_id) {

        $user_data = array(
            'active' => 0,
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => Auth::user()->id
        );

        return User::where('id', $user_id)
                ->update($user_data);
    }

    public function unblockUser($user_id) {
        $user_data = array(
            'active' => 1,
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => Auth::user()->id
        );

        return User::where('id', $user_id)
                ->update($user_data);
    }

    ///////////////// role ////////////////
    
    /**
     * Delivering all roles in storage 
     * 
     * @return object
     */
    public function allRoles() {
        $data = Role::all(array('id', 'name'));
        
        $roles = array();
        if(count($data)>0){
            foreach($data as $role){
                $roles[] = (object)array(
                    'id' => $role->id,
                    'name' => $role->name,
                    'no_of_user' => count($this->usersOfRole($role->id))
                );
            }
        }
        
        return $roles;
    }

    /**
     * Store role in the storage 
     * 
     * @param array $input
     * @return boolean
     */
    public function storeRole($permission) {

        DB::beginTransaction();

        try {
            $role_data = array(
                'name' => $permission['role'],
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => Auth::user()->staff_id
            );

            $role = Role::create($role_data);

            if ($role) {
                foreach ($permission['permission'] as $module => $permissions) {
                    foreach ($permissions as $operation) {
                        $permission_data = array(
                            'role_id' => $role->id,
                            'module_id' => $module,
                            'permission' => $operation,
                            'created_at' => date('Y-m-d H:i:s'),
                            'created_by' => Auth::user()->id
                        );
                        Permission::create($permission_data);
                    }
                }
            }
        } catch (Exception $ex) {
            DB::rollBack();
            return false;
        }

        DB::commit();
        return $role;
    }

    /**
     * Delivering the role with permission 
     * 
     * @param int $id
     * @return array
     */
    public function showRoleInfo($id) {

        $role = Role::find($id);
        
        if($role){
            $modules = $this->modules();
            $operations = array(1,2,3,4);
            $permissions = array();
            foreach($modules as $module){
                $permissions[$module->id]['name'] = $module->name;
                foreach($operations as $operation){
                    $permission = Permission::where('role_id', $role->id)
                            ->where('module_id', $module->id)
                            ->where('permission', $operation)
                            ->orderBy('module_id')
                            ->first();
                    if($permission){
                        $permissions[$module->id][$operation] = $permission->permission;
                    } else {
                        $permissions[$module->id][$operation] = 0;
                    }
                }
            }

            $role->permissions = $permissions;
            
            return $role;
        }
        
        return false;
    }

    /**
     * Update the role with permissions 
     * 
     * @param int $id
     * @param array $input
     * @return boolean
     */
    public function updateRole($role_id, $permission) {

        DB::beginTransaction();

        try {
            $role_data = array(
                'name' => $permission['role'],
                'updated_at' => date('Y-m-d H:i:s'),
                'updated_by' => Auth::user()->id
            );

            $role = Role::where('id', $role_id)->update($role_data);

            if ($role) {
                
                Permission::where('role_id', $role_id)->forceDelete();
                
                foreach ($permission['permission'] as $module => $permissions) {
                    foreach ($permissions as $operation) {
                        $permission_data = array(
                            'role_id' => $role_id,
                            'module_id' => $module,
                            'permission' => $operation,
                            'created_at' => date('Y-m-d H:i:s'),
                            'created_by' => Auth::user()->id
                        );
                        Permission::create($permission_data);
                    }
                }
            }
        } catch (Exception $ex) {
            DB::rollBack();
            return false;
        }

        DB::commit();
        return $role;
    }

    /**
     * Delivers the all users associated with specific role 
     * 
     * @param int $role_id
     * @return object 
     */
    public function usersOfRole($role_id) {
        return User::join('staffs', 'users.staff_id', '=', 'staffs.id')
                ->where('users.role_id', $role_id)
                ->get();
    }

    
    /**
     * Deletes the specific role with associated permissions
     * 
     * @param type $role_id
     * @return boolean
     */
    public function deleteRole($role_id) {

        try {

            DB::transaction(function() use ($role_id) {
                Role::where('id', $role_id)->delete();
                Permission::where('role_id', $role_id)->forceDelete();
            });

            return true;
        } catch (Exception $ex) {
            return false;
        }
    }
    
    /////////////// role end //////////////
    
    
    
    
    /////////////// data access /////////////
    
    public static function access($user_id){
        
        $areas = ServiceAccess::where('user_id', $user_id)->get();
        
        $data = array();
        if ($areas->count() > 0) {

            foreach ($areas as $area) {
                
                $data[$area->area]['type'] = $area->access;
                $data[$area->area]['data'] = $area->data;
                $data[$area->area]['access'] = array();
                
                if ($area->access == 'custom') {
                    $accesses = DataAccess::where('access_id', $area->id)->get();
                   
                 
                    if ($accesses->count() > 0) {

                        $custom_access = array();
                        foreach ($accesses as $access) {

                            if($access->access != ""){
                                $custom_access[$access->service_id]  = json_decode($access->access, true);
                            } else {
                                $custom_access[$access->service_id]  = '';
                            }

                            
                        }
                     
                        $data[$area->area]['access'] = $custom_access;
                    }

                    
                }
            }
        }

        return $data;
    }
    
    
    public function updateAccess($user_id, $data){
        
        DB::beginTransaction();
        try {
            
            $previous_accesses = ServiceAccess::where('user_id', $user_id)->get();
            
            if($previous_accesses->count()>0){
                foreach($previous_accesses as $previous_access){
                    DataAccess::where('access_id', $previous_access->id)->forceDelete();
                }
            }
            
            ServiceAccess::where('user_id', $user_id)->forceDelete();

            // access
            $accesses = $data->get('access');
            foreach ($accesses as $area => $access) {
                $service_access_data = array(
                    'user_id' => $user_id,
                    'area' => $area,
                    'access' => $access['type'],
                    'data' => $access['data']
                );
                
                
                $new_access = ServiceAccess::create($service_access_data);

                if ($access['type'] == 'custom' && isset($access['access']) && is_array($access['access'])) {

                        foreach ($access['access'] as $service_id => $service_access) {
                            $data_access_data = array(
                                'access_id' => $new_access->id,
                                'service_id' => $service_id,
                                'access' => (is_array($service_access)) ? json_encode($service_access) : NULL,
                                
                            );

                            DataAccess::create($data_access_data);
                        }
                }
            }
        } catch (Exception $ex) {
            DB::rollBack();
            return false;
        }
        
        DB::commit();
        return true;
        
    }
    
    
    
}


