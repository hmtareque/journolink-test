<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modules;

use App\Models\ServiceArea;
use App\Models\Service;
use App\Models\ServiceCategory;

/**
 * Description of SettingModule
 *
 * @author hasan
 */
class SettingModule {
    //put your code here
    
    
    public function services($area){
        
        $services = Service::where('service_area', $area)->get();
        
        if($services->count()>0){
            foreach ($services as $service){
                $service->categories = ServiceCategory::where('service_area', $area)
                        ->where('service_id', $service->id)
                        ->orderBy('sort')
                        ->get();
            }
        }
        
        return $services;
    }
    
    
    
    
}
