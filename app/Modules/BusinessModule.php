<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modules;

use DB;
use App\Models\BusinessSettings;

/**
 * Description of AppModule
 *
 * @author hasan
 */
class BusinessModule {
    //put your code here
    
    public function settings(){
        return BusinessSettings::all();
    }
    
    public function storeSetting(array $data){
        
        $setting_data = array(
            'param' => strtolower(str_replace(" ", "_", trim($data['parameter']))),
            'value' => trim($data['value'])
        );
        
        return BusinessSettings::create($setting_data);
        
    }
    
    public function updateSettings(array $data) {
        DB::beginTransaction();
        try {
            foreach ($data as $param => $value) {
                BusinessSettings::where('param', $param)->update(array('value' => $value));
            }
        } catch (Exception $ex) {
            DB::rollBack();
            return false;
        }
        DB::commit();
        return true;
    }
    
    public function deleteSetting($id){
        
       
        
        return BusinessSettings::where('id', $id)->forceDelete();
        
    }

}
