<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modules;

use DB;
use App\Models\Client;
use App\Models\Engagement;
use App\Models\Student;
use App\Models\StudentClass;
use App\Models\ClassAttendance;
use App\Models\StudentAttendance;
use App\Models\ClassAssessment;
use App\Models\StudentAssessment;
use App\Models\Fee;
use App\Models\Payment;


/**
 * Description of StudentModule
 *
 * @author hasan
 */
class StudentModule {

    public function __construct(S3Module $s3, NoteModule $note) {
        $this->s3 = $s3;
        $this->note = $note;
    }

    public function search($keyword) {

        $info = array('clients.id as client_id', 'clients.ref_no', 'clients.first_name', 'clients.middle_name',
            'clients.last_name', 'clients.date_of_birth', 'clients.phone', 'clients.mobile', 'clients.email',
            'clients.next_of_kin', 'clients.next_of_kin_relationship'
        );

        return Engagement::join('clients', 'clients.id', 'engagements.client_id')
                        ->where(function($query) use ($keyword) {
                            $query->where('clients.ref_no', $keyword)
                            ->orWhere('postcode', 'like', $keyword . '%')
                            ->orWhere('last_name', $keyword)
                            ->orWhere('date_of_birth', $keyword)
                            ->orWhere('phone', $keyword)
                            ->orWhere('mobile', $keyword);
                        })->take(100)->get($info);
    }

    public function info($client_id) {

        $info = array('students.id as id', 'clients.id as client_id', 'clients.ref_no', 'students.id as student_id',
            'students.date_of_enrolment', 'students.status', 'clients.first_name', 'clients.middle_name',
            'clients.last_name', 'clients.date_of_birth', 'clients.phone', 'clients.mobile', 'clients.email',
            'clients.next_of_kin', 'clients.next_of_kin_relationship', 'clients.next_of_kin_phone', 'clients.next_of_kin_mobile', 'clients.next_of_kin_email'
        );

        return Student::join('clients', 'students.client_id', 'clients.id')
                        ->where('clients.id', $client_id)
                        ->first($info);
    }

    public function enrolledStudent($client_id, $class_id) {

        $info = array('students.id as id', 'clients.id as client_id', 'clients.ref_no',
            'students.date_of_enrolment', 'students.status', 'clients.first_name', 'clients.middle_name',
            'clients.last_name', 'clients.date_of_birth', 'clients.phone', 'clients.mobile', 'clients.email',
            'clients.next_of_kin', 'clients.next_of_kin_relationship', 'clients.next_of_kin_phone', 'clients.next_of_kin_mobile', 'clients.next_of_kin_email'
        );

        return Student::join('clients', 'students.client_id', 'clients.id')
                        ->where('clients.id', $client_id)
                        ->where('students.class_id', $class_id)
                        ->first($info);
    }

    public function enrol($client_id, $data) {

        DB::beginTransaction();

        try {

            engage($client_id, 'education');

            $all = $data->all();
            $classes = $all['classes'];


            if (count($classes) > 0) {
                foreach ($classes as $class) {
                    if (isset($class['class']) && $class['class'] != '') {

                        $student_data = array(
                            'type' => $data->get('type'),
                            'date_of_enrolment' => date('Y-m-d', strtotime(php_date($data->get('enrolment_date')))),
                            'client_id' => $client_id,
                            'school_id' => $data->get('school'),
                            'class_id' => $class['class'],
                            'referred_by' => $data->get('referred_by'),
                            'leave_on_own' => $data->get('can_leave_on_own'),
                            'have_to_collect_by' => ($data->get('can_leave_on_own') == 0) ? $data->get('have_to_collect_by') : NULL,
                            'created_at' => date('Y-m-d H:i:s'),
                            'created_by' => auth()->id()
                        );

                        $student = Student::create($student_data);

                        // get class assessment 
                        $class_assessment = ClassAssessment::where('term_id', $data->get('term'))
                                ->where('class_id', $class['class'])
                                ->first();

                        if (!$class_assessment) {
                            // create class assessment 
                            $class_assessment_data = array(
                                'term_id' => $data->get('term'),
                                'class_id' => $class['class'],
                                'created_at' => date('Y-m-d H:i:s'),
                                'created_by' => auth()->id()
                            );

                            $class_assessment = ClassAssessment::create($class_assessment_data);
                        }


                        $student_assessment_data = array(
                            'class_assessment_id' => $class_assessment->id,
                            'type' => 'external',
                            'student_id' => $student->id,
                            'level' => $class['level'],
                            'note' => $class['note'],
                            'created_at' => date('Y-m-d H:i:s'),
                            'created_by' => auth()->id()
                        );

                        StudentAssessment::create($student_assessment_data);
                    }
                }
            }
        } catch (Exception $ex) {

            DB::rollBack();
            return false;
        }

        DB::commit();
        return $student;
    }

    public function pay() {

        $fee_data = array(
            'beneficiary_id' => $beneficiary->id,
            'service_area' => 'education',
            'service_id' => $data->get('school'),
            'description' => $data->get('payment_description'),
            'fee' => $data->get('fee'),
            'exempt' => $data->get('exempt_from_fee'),
            'discount' => ($data->get('exempt_from_fee')) ? $data->get('discount') : 0,
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => auth()->id()
        );

        $fee = Fee::create($fee_data);


        if ($data->get('paid') != "") {

            $payment_data = array(
                'fee_id' => $fee->id,
                'description' => 'Payment with enrolment',
                'amount' => $data->get('paid'),
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => auth()->id()
            );


            Payment::create($payment_data);
        }
    }

    /* student classes start */

    public function storeClass($class) {
        
    }

    public function updateClass($id, $class) {
        
    }

    /* student classes end */

    public function recent($take = 100) {

        $students = Client::take($take)->get();

        return $students;
    }

    public function all() {

        $students = Client::join('engagements', 'engagements.client_id', '=', 'clients.id')
                ->where('engagements.service', 'education')
                ->orderBy('clients.created_at', 'desc')
                ->get();

        return $students;
    }

    public function active() {

        $info = array('clients.id as id', 'clients.ref_no', 'clients.first_name', 'clients.middle_name',
            'clients.last_name', 'clients.phone', 'clients.mobile', 'clients.email',
            DB::raw('count(students.id) as no_of_class'));

        $students = Student::join('clients', 'clients.id', 'students.client_id')
                ->where('students.status', 'studying')
                ->groupBy('clients.id', 'clients.ref_no', 'clients.first_name', 'clients.middle_name', 'clients.last_name', 'clients.phone', 'clients.mobile', 'clients.email')
                ->get($info);

        return $students;
    }

    public function details($client_id) {

        $student = Client::where('id', $client_id)->first();
        $student->profile_picture = $this->s3->picture('client_profile_picture', $client_id);

        $student->immigration_docs = $this->s3->doc('client_immigration', $client_id);

        if ($student->has_medical_condition == 1) {
            $student->medical_condition_docs = $this->s3->doc('client_medical_condition', $client_id);
        }

        if ($student->has_allergy == 1) {
            $student->allergy_docs = $this->s3->doc('client_allergy_docs', $client_id);
        }

        if ($student->has_disability == 1) {
            $student->disability_docs = $this->s3->doc('client_disability_docs', $client_id);
        }

        if ($student->need_additional_support == 1) {
            $student->additional_support_docs = $this->s3->doc('client_additional_support_docs', $client_id);
        }

        $student->classes = $this->enrolledClasses($client_id);

        $student->notes = $this->note->notes('education_client', $client_id);
        $student->docs = $this->s3->docs('education_client', $client_id);

        return $student;
    }

    public function enrolledClasses($client_id) {

        $info = array('student_classes.id', 'schools.id as school_id', 'schools.name as school', 'courses.title as course', 'student_groups.group as group', 'academic_years.year as year', 'students.date_of_enrolment', 'students.status');

        $classes = Student::join('student_classes', 'student_classes.id', '=', 'students.class_id')
                ->join('schools', 'student_classes.school_id', '=', 'schools.id')
                ->join('student_groups', 'student_groups.id', '=', 'student_classes.student_group_id')
                ->join('courses', 'courses.id', '=', 'student_classes.course_id')
                ->join('academic_years', 'academic_years.id', '=', 'student_classes.academic_year_id')
                ->where('students.client_id', $client_id)
                ->orderBy('schools.type')
                ->orderBy('schools.name')
                ->orderBy('student_classes.academic_year_id')
                ->orderBy('student_classes.course_id')
                ->orderBy('student_classes.student_group_id')
                ->get($info);

        return $classes;
    }

    public function enrolledClass($client_id, $class_id) {

        $info = array('student_classes.id', 'students.id as student_id', 'schools.name as school', 'courses.title as course', 'student_classes.status as class_status', 'student_groups.group as group', 'academic_years.year as year', 'students.date_of_enrolment', 'students.status');

        $class = Student::join('student_classes', 'student_classes.id', '=', 'students.class_id')
                ->join('schools', 'student_classes.school_id', '=', 'schools.id')
                ->join('student_groups', 'student_groups.id', '=', 'student_classes.student_group_id')
                ->join('courses', 'courses.id', '=', 'student_classes.course_id')
                ->join('academic_years', 'academic_years.id', '=', 'student_classes.academic_year_id')
                ->where('students.client_id', $client_id)
                ->where('students.class_id', $class_id)
                ->first($info);

        $class->attendances = StudentAttendance::join('class_attendances', 'class_attendances.id', 'student_attendances.class_attendance_id')
                ->where('class_attendances.class_id', $class->id)
                ->where('student_attendances.student_id', $class->student_id)
                ->get(array('student_attendances.id', 'class_attendances.date', 'student_attendances.present', 'student_attendances.reason_for_absence'));


//        $assessments = StudentAssessment::join('class_assessments', 'class_assessments.id', 'student_assessments.class_assessment_id')
//                ->join('school_terms', 'school_terms.id', 'class_assessments.term_id')
//                ->join('academic_years', 'school_terms.academic_year_id', 'academic_years.id')
//                ->where('class_assessments.class_id', $class->id)
//                ->where('student_assessments.student_id', $class->student_id)
//                ->get(array('student_assessments.id', 'class_assessments.id as class_assessment_id', 'student_assessments.type', 'student_assessments.level', 'class_assessments.term_id', 
//                    'school_terms.term', 'school_terms.dates', 'academic_years.year',
//                    ));
//        

        $assessments = StudentAssessment::join('class_assessments', 'class_assessments.id', 'student_assessments.class_assessment_id')
                ->join('school_terms', 'school_terms.id', 'class_assessments.term_id')
                ->join('academic_years', 'school_terms.academic_year_id', 'academic_years.id')
                ->where('class_assessments.class_id', $class_id)
                ->where('student_assessments.student_id', $class->student_id)
                ->get(array('student_assessments.id', 'class_assessments.id as class_assessment_id', 'student_assessments.type', 'student_assessments.level', 'student_assessments.note', 'class_assessments.term_id',
            'school_terms.term', 'school_terms.dates', 'academic_years.year',
        ));

        // print_r($assessments);

        $data = array();
        foreach ($assessments as $assessment) {
            $data[$assessment->term_id]['year'] = $assessment->year;
            $data[$assessment->term_id]['term'] = $assessment->term;
            $data[$assessment->term_id]['dates'] = $assessment->dates;

            $data[$assessment->term_id]['assessments'][$assessment->type] = array(
                'id' => $assessment->id,
                'class_assessment_id' => $assessment->class_assessment_id,
                'level' => $assessment->level,
                'note' => $assessment->note,
                'docs' => $this->s3->doc('student_assessment_docs', $assessment->id)
            );
        }

        $class->assessments = $data;
        $class->assessments2 = $class;

        return $class;
    }

    // assessment 

    public function storeAssessment($class_id, $request) {

        DB::beginTransaction();

        try {
            $term_id = $request->get('term');
            $student_id = $request->get('student_id');

            // get class assessment 
            $class_assessment = ClassAssessment::where('term_id', $term_id)
                    ->where('class_id', $class_id)
                    ->first();

            if (!$class_assessment) {
                // create class assessment 
                $class_assessment_data = array(
                    'term_id' => $term_id,
                    'class_id' => $class_id,
                    'created_at' => date('Y-m-d H:i:s'),
                    'created_by' => auth()->id()
                );

                $class_assessment = ClassAssessment::create($class_assessment_data);
            }

            $student_assessment_data = array(
                'class_assessment_id' => $class_assessment->id,
                'type' => $request->get('type'),
                'student_id' => $student_id,
                'level' => $request->get('level'),
                'note' => $request->get('note'),
                'created_at' => date('Y-m-d H:i:s'),
                'created_by' => auth()->id()
            );

            $assessment = StudentAssessment::create($student_assessment_data);
        } catch (Exception $ex) {
            DB::rollBack();
            return false;
        }

        DB::commit();
        return $assessment;
    }

}
