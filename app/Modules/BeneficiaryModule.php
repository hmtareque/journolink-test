<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modules;

use App\Models\Beneficiary;

/**
 * Description of BeneficiaryModule
 *
 * @author hasan
 */
class BeneficiaryModule {
    //put your code here
    
    public function search($keyword){
        
        return Beneficiary::where('last_name', 'LIKE', '%'.$keyword.'%')->orWhere('postcode', 'LIKE', '%'.$keyword.'%')->orWhere('date_of_birth', 'LIKE', '%'.$keyword.'%')->get();
        
    }
    
    
}
