<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modules;


use DB;
use access;
use App\Models\School;
use App\Models\StudentClass;
use App\Models\Student;
use App\Models\Term;
use App\Models\Teacher;
use App\Models\TeacherClass;

/**
 * Description of SchoolModule
 *
 * @author hasan
 */
class SchoolModule {
    
    protected $access;
    
    public function __construct(AccessModule $access, S3Module $s3, NoteModule $note) {
        $this->access = $access;
        $this->s3 = $s3;
        $this->note = $note;
    }
    
    public function store($data){
        
        $school_data = array(
            'type' => $data['type'],
            'name' => $data['name'],
            'primary_contact_person' => $data['primary_contact'],
            'address_line_1' => $data['address_line_1'],
            'address_line_2' => $data['address_line_2'],
            'street' => $data['street'],
            'city' => $data['city'],
            'postcode' => $data['postcode'],
            'borough' => $data['borough'],
            'phone' => $data['phone'],
            'mobile' => $data['mobile'],
            'email' => $data['email'],
            'fax' => $data['fax'],
            'created_by' => auth()->id()
        );
        
        return School::create($school_data);
    }
    
    
    public function update($data, $id){
        
        $school_data = array(
            'type' => $data['type'],
            'name' => $data['name'],
            'primary_contact_person' => $data['primary_contact'],
            'address_line_1' => $data['address_line_1'],
            'address_line_2' => $data['address_line_2'],
            'street' => $data['street'],
            'city' => $data['city'],
            'postcode' => $data['postcode'],
            'borough' => $data['borough'],
            'phone' => $data['phone'],
            'mobile' => $data['mobile'],
            'email' => $data['email'],
            'fax' => $data['fax'],
            'updated_by' => auth()->id()
        );
        
        return School::where('id', $id)->update($school_data);
    }

    public function activate($id){
        
        $school_data = array(
            'active' => 1,
            'updated_by' => auth()->id()
        );
        
        return School::where('id', $id)->update($school_data);
    }
    
    public function deactivate($id){
        
        $school_data = array(
            'active' => 0,
            'updated_by' => auth()->id()
        );
        
        return School::where('id', $id)->update($school_data);
    }

    public function all(){
        return School::orderBy('name')
                ->get(array('id', 'type', 'name'));
    }
    
    public function byType(){
        $data = School::orderBy('type')
                ->orderBy('name')
                ->get(array('id', 'type', 'name'));
        
        $schools = array();
        foreach($data as $item){
            $schools[$item->type][] = $item;
        }
        
        return (object)$schools;
    }
    
    
    public function referralSchools(){
        
        $has_access = access::services('education');
        
        if($has_access['type'] == 'full'){
            $schools = School::where('type', 'referral')
                 ->orderBy('name')
                ->get(array('id', 'name', 'active'));
         
         
         if ($schools->count() > 0) {
            foreach ($schools as $school) {
                $school->no_of_students = Student::where('referred_by', $school->id)
                        ->where('status', 'studying')
                        ->count();
            }
        }
  
       return $schools;
        } else {
            return false;
        }
    }

    public function schools() {
        
        
        $access = access::services('education');
        
        if($access['type'] == 'full'){
            
            if($access['data'] == 'all'){
                $schools = School::where('type', '!=', 'referral')
                        ->orderBy('type')
                        ->orderBy('name')
                        ->get();
            } else {
                $schools = School::where('type', '!=', 'referral')
                        ->where('created_by', auth()->id())
                        ->orderBy('type')
                        ->orderBy('name')
                        ->get();
            }
            
            
        }elseif($access['type'] === 'custom'){
            
            if($access['data'] == 'all'){
                $schools = School::where('type', '!=', 'referral')
                        ->whereIn('id', $access['access'])
                        ->orderBy('type')
                        ->orderBy('name')
                        ->get();
            } else {
                $schools = School::where('type', '!=', 'referral')
                        ->whereIn('id', $access['access'])
                        ->where('created_by', auth()->id())
                        ->orderBy('type')
                        ->orderBy('name')
                        ->get();
            }
            
            
            
            
            
        }else {
            return false;
        }

        
                
           



        if ($schools->count() > 0) {
            foreach ($schools as $school) {
                $school->no_of_classes = StudentClass::where('school_id', $school->id)->where('status', 'progressing')->count();
                $school->no_of_students = Student::where('school_id', $school->id)->distinct('client_id')->get(array('client_id'))->count();
                $school->no_of_teachers = Teacher::where('school_id', $school->id)->count();
            }
        }

        return $schools;
    }

    public function info($school_id){
        
        $school = School::where('id', $school_id)->first();
        
        return $school;
        
    }
    
    public function details($school_id){
        
        $school = School::where('id', $school_id)->first();
        
        $school->classes = $this->schoolClasses($school_id);
        $school->students = $this->activeSchoolStudents($school_id);
        $school->teachers = $this->activeSchoolTeachers($school_id);
        $school->notes = $this->note->notes('school', $school_id);
        $school->docs = $this->s3->docs('school', $school_id);
        
        return $school;
        
    }
    
    public function referralSchoolDetails($school_id){
        
        $school = School::where('id', $school_id)->first();
        $school->students = $this->activeReferralSchoolStudents($school_id);
        return $school;
        
    }
    
    /* teachers */

    public function activeSchoolTeachers($school_id) {
        return Teacher::join('staffs', 'teachers.staff_id', '=', 'staffs.id')
                        ->where('school_id', $school_id)
                        ->where('teachers.active', 1)
                        ->get(array('teachers.id as id', 'staffs.id as staff_id', 'staffs.first_name', 'staffs.last_name'));
    }

    public function allSchoolTeachers($school_id) {

        $teachers = Teacher::join('staffs', 'teachers.staff_id', '=', 'staffs.id')
                ->where('school_id', $school_id)
                ->get(array('teachers.id as id', 'staffs.id as staff_id',  'staffs.first_name', 'staffs.last_name', 'staffs.dbs_number', 'staffs.dbs_issue_date', 'staffs.status', 'teachers.active', 'staffs.mobile', 'staffs.phone'));

        $info = array('student_classes.id', 'courses.title as course', 'student_groups.group as group', 'academic_years.year as year', 'teacher_classes.status');

        if ($teachers->count() > 0) {
            foreach ($teachers as $teacher) {

                $teacher->classes = TeacherClass::join('student_classes', 'teacher_classes.class_id', '=', 'student_classes.id')
                        ->join('student_groups', 'student_groups.id', '=', 'student_classes.student_group_id')
                        ->join('courses', 'courses.id', '=', 'student_classes.course_id')
                        ->join('academic_years', 'academic_years.id', '=', 'student_classes.academic_year_id')
                        ->where('student_classes.school_id', $school_id)
                        ->where('teacher_classes.teacher_id', $teacher->id)
                        ->where('student_classes.status', 'progressing')
                        ->orderBy('student_classes.academic_year_id')
                        ->orderBy('student_classes.course_id')
                        ->orderBy('student_classes.student_group_id')
                        ->get($info);
            }
        }

        return $teachers;
    }

    /* students */
    
    public function activeSchoolStudents($school_id){
        
        $info = array('clients.ref_no as ref_no', 'clients.id as client_id', 'clients.first_name', 'clients.middle_name', 'clients.last_name', 'clients.date_of_birth', 'clients.phone', 'clients.mobile', 'clients.email', DB::raw('count(students.id) as studying_in_classes'));
        
        $students = Student::join('clients', 'students.client_id', '=', 'clients.id')
                ->where('school_id', $school_id)
                ->where('status', 'studying')
                ->groupBy('clients.id', 'clients.ref_no', 'clients.first_name', 'clients.middle_name', 'clients.last_name', 'clients.date_of_birth', 'clients.phone', 'clients.mobile', 'clients.email')
                ->get($info);
        
        return $students;
        
    }
    
    public function activeReferralSchoolStudents($school_id){
        
        $info = array('clients.ref_no as ref_no', 'clients.id as client_id', 'clients.first_name', 'clients.middle_name', 'clients.last_name', 'clients.date_of_birth', 'clients.phone', 'clients.mobile', 'clients.email', DB::raw('count(students.id) as studying_in_classes'));
        
        $students = Student::join('clients', 'students.client_id', '=', 'clients.id')
                ->where('students.referred_by', $school_id)
                ->where('status', 'studying')
                ->groupBy('clients.id', 'clients.ref_no', 'clients.first_name', 'clients.middle_name', 'clients.last_name', 'clients.date_of_birth', 'clients.phone', 'clients.mobile', 'clients.email')
                ->get($info);
        
        return $students;
        
    }
    
    
    
    /* class */

    public function schoolClasses($school_id){
        
        
         $info = array('student_classes.id', 'courses.title as course', 'student_groups.group as group', 'academic_years.year as year');
        
        
            $access = access::categories('education', $school_id);


            $classes = false;
            if ($access['type'] === 'full') {
                if ($access['data'] === 'all') {
                    $classes = StudentClass::join('student_groups', 'student_groups.id', '=', 'student_classes.student_group_id')
                ->join('courses', 'courses.id', '=', 'student_classes.course_id')
                ->join('academic_years', 'academic_years.id', '=', 'student_classes.academic_year_id')
                ->where('school_id', $school_id)
                ->where('student_classes.status', 'progressing')
                            
                ->orderBy('student_classes.academic_year_id')
                ->orderBy('student_classes.course_id')
                ->orderBy('student_classes.student_group_id')
                ->get($info);
                } else {
                    $classes = StudentClass::join('student_groups', 'student_groups.id', '=', 'student_classes.student_group_id')
                ->join('courses', 'courses.id', '=', 'student_classes.course_id')
                ->join('academic_years', 'academic_years.id', '=', 'student_classes.academic_year_id')
                ->where('school_id', $school_id)
                ->where('student_classes.status', 'progressing')
                ->where('student_classes.created_by', auth()->id())
                ->orderBy('student_classes.academic_year_id')
                ->orderBy('student_classes.course_id')
                ->orderBy('student_classes.student_group_id')
                ->get($info);
                }
            } elseif ($access['type'] === 'custom') {

                if ($access['data'] === 'all') {
                    $classes = StudentClass::join('student_groups', 'student_groups.id', '=', 'student_classes.student_group_id')
                ->join('courses', 'courses.id', '=', 'student_classes.course_id')
                ->join('academic_years', 'academic_years.id', '=', 'student_classes.academic_year_id')
                ->where('school_id', $school_id)
                ->where('student_classes.status', 'progressing')
                            
                            ->whereIn('student_classes.id', $access['access'])
                            
                ->orderBy('student_classes.academic_year_id')
                ->orderBy('student_classes.course_id')
                ->orderBy('student_classes.student_group_id')
                ->get($info);
                } else {
                    $classes = StudentClass::join('student_groups', 'student_groups.id', '=', 'student_classes.student_group_id')
                ->join('courses', 'courses.id', '=', 'student_classes.course_id')
                ->join('academic_years', 'academic_years.id', '=', 'student_classes.academic_year_id')
                ->where('school_id', $school_id)
                ->where('student_classes.status', 'progressing')
                            
                            ->whereIn('student_classes.id', $access['access'])
                            ->where('student_classes.created_by', auth()->id())
                ->orderBy('student_classes.academic_year_id')
                ->orderBy('student_classes.course_id')
                ->orderBy('student_classes.student_group_id')
                ->get($info);
                }
            }
        
        
        
        
        
       
        
        return $classes;
    }
    
    public function schoolAllClasses($school_id){
        
      //  $access = access::categories($area, $service); 

        $info = array('student_classes.id', 'courses.title as course', 'student_groups.group as group', 'academic_years.year as year', 'student_classes.status');

        
        
            $access = access::categories('education', $school_id);


            $classes = false;
            if ($access['type'] === 'full') {
                if ($access['data'] === 'all') {
                    $classes = StudentClass::join('student_groups', 'student_groups.id', '=', 'student_classes.student_group_id')
                ->join('courses', 'courses.id', '=', 'student_classes.course_id')
                ->join('academic_years', 'academic_years.id', '=', 'student_classes.academic_year_id')
                ->where('school_id', $school_id)
                ->where('student_classes.status', 'progressing')
                            
                ->orderBy('student_classes.academic_year_id')
                ->orderBy('student_classes.course_id')
                ->orderBy('student_classes.student_group_id')
                ->get($info);
                } else {
                    $classes = StudentClass::join('student_groups', 'student_groups.id', '=', 'student_classes.student_group_id')
                ->join('courses', 'courses.id', '=', 'student_classes.course_id')
                ->join('academic_years', 'academic_years.id', '=', 'student_classes.academic_year_id')
                ->where('school_id', $school_id)
                ->where('student_classes.status', 'progressing')
                ->where('student_classes.created_by', auth()->id())
                ->orderBy('student_classes.academic_year_id')
                ->orderBy('student_classes.course_id')
                ->orderBy('student_classes.student_group_id')
                ->get($info);
                }
            } elseif ($access['type'] === 'custom') {

                if ($access['data'] === 'all') {
                    $classes = StudentClass::join('student_groups', 'student_groups.id', '=', 'student_classes.student_group_id')
                ->join('courses', 'courses.id', '=', 'student_classes.course_id')
                ->join('academic_years', 'academic_years.id', '=', 'student_classes.academic_year_id')
                ->where('school_id', $school_id)
                ->where('student_classes.status', 'progressing')
                            
                            ->whereIn('student_classes.id', $access['access'])
                            
                ->orderBy('student_classes.academic_year_id')
                ->orderBy('student_classes.course_id')
                ->orderBy('student_classes.student_group_id')
                ->get($info);
                } else {
                    $classes = StudentClass::join('student_groups', 'student_groups.id', '=', 'student_classes.student_group_id')
                ->join('courses', 'courses.id', '=', 'student_classes.course_id')
                ->join('academic_years', 'academic_years.id', '=', 'student_classes.academic_year_id')
                ->where('school_id', $school_id)
                ->where('student_classes.status', 'progressing')
                            
                            ->whereIn('student_classes.id', $access['access'])
                            ->where('student_classes.created_by', auth()->id())
                ->orderBy('student_classes.academic_year_id')
                ->orderBy('student_classes.course_id')
                ->orderBy('student_classes.student_group_id')
                ->get($info);
                }
            }

        $active = array();
        $other = array();
        if($classes){
        foreach ($classes as $class) {

            $class->no_of_students = Student::where('class_id', $class->id)->count();

            $class->teachers = TeacherClass::join('teachers', 'teachers.id', '=', 'teacher_classes.teacher_id')
                    ->join('staffs', 'teachers.staff_id', '=', 'staffs.id')
                    ->where('class_id', $class->id)
                    ->get(array('staffs.id as id', 'staffs.first_name', 'staffs.last_name', 'teacher_classes.status'));
            
            if($class->status == 'progressing'){
                $active[] = $class;
            } else {
                $other[] = $class;
            }
        } 
        
        $class_data = (object)array(
            'active' => $active,
            'other' => $other
        );

        return $class_data;
        }
        return false;
    }
    
    public function activeSchoolClasses($year_id, $school_id){
        
        $info = array('student_classes.id', 'courses.title as course', 'student_groups.group as group', 'academic_years.year as year');
        
        $classes = StudentClass::join('student_groups', 'student_groups.id', '=', 'student_classes.student_group_id')
                ->join('courses', 'courses.id', '=', 'student_classes.course_id')
                ->join('academic_years', 'academic_years.id', '=', 'student_classes.academic_year_id')
                ->where('student_classes.school_id', $school_id)
                ->where('student_classes.academic_year_id', $year_id)
                ->where('student_classes.status', 'progressing')
                ->orderBy('student_classes.course_id')
                ->orderBy('student_classes.student_group_id')
                ->get($info);
        
        return $classes;
    }
    
 
    
    public function yearTerms($year_id){
        return Term::where('academic_year_id', $year_id)
                ->where('completed', 0)
                ->orderBy('sort')
                ->get(array('id', 'term', 'dates'));
    }
    
    
}
