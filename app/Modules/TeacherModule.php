<?php

namespace App\Modules;

use access;
use DB;
use App\Models\Teacher;
use App\Models\TeacherClass;

use App\Models\Student;
use App\Models\TeacherAttendance;
use App\Models\ClassAttendance;

/**
 * Description of TeacherModule
 *
 * @author hasan
 */
class TeacherModule {
    //put your code here
    
    public function __construct(S3Module $s3, NoteModule $note) {
        $this->s3 = $s3;
        $this->note = $note;
    }
    
    public function all() {
        $info = array('teachers.id as teacher_id', 'schools.id as school_id', 'schools.name as school_name', 'staffs.id as staff_id', 'staffs.first_name', 'staffs.middle_name', 'staffs.last_name',
            'staffs.phone', 'staffs.mobile', 'staffs.email', 'staffs.has_paid_role as paid', 'staffs.dbs_number', 'staffs.dbs_issue_date', 'teachers.active');
        
        $access = access::services('education');

        $data = false;
        if ($access['type'] === 'full') {
            if ($access['data'] === 'all') {
                $data = Teacher::join('staffs', 'staffs.id', 'teachers.staff_id')
                        ->join('schools', 'schools.id', 'teachers.school_id')
                        ->get($info);
            } else {
                $data = Teacher::join('staffs', 'staffs.id', 'teachers.staff_id')
                        ->join('schools', 'schools.id', 'teachers.school_id')
                        ->where('teachers.created_by', auth()->id())
                        ->get($info);
            }
        } elseif ($access['type'] === 'custom') {

            if ($access['data'] === 'all') {
                $data = Teacher::join('staffs', 'staffs.id', 'teachers.staff_id')
                        ->join('schools', 'schools.id', 'teachers.school_id')
                        ->whereIn('schools.id', $access['access'])
                        ->get($info);
            } else {
                $data = Teacher::join('staffs', 'staffs.id', 'teachers.staff_id')
                        ->join('schools', 'schools.id', 'teachers.school_id')
                        ->whereIn('schools.id', $access['access'])
                        ->where('teachers.created_by', auth()->id())
                        ->get($info);
            }
        } elseif ($access['type'] === 'none') {
            return false;
        }


        $teachers = array();
        if ($data && $data->count() > 0) {
            
            $class_info = array('student_classes.id as class_id', 'courses.title as course', 'student_groups.group as group', 'academic_years.year as year', 'teacher_classes.status');
        
            
            foreach ($data as $teacher) {
                
                $class_access = access::categories('education', $teacher->school_id);
                
               
                $classes = TeacherClass::join('student_classes', 'student_classes.id', '=', 'teacher_classes.class_id')
                                ->join('student_groups', 'student_groups.id', '=', 'student_classes.student_group_id')
                                ->join('courses', 'courses.id', '=', 'student_classes.course_id')
                                ->join('academic_years', 'academic_years.id', '=', 'student_classes.academic_year_id')
                                ->where('teacher_classes.teacher_id', $teacher->teacher_id)
                                ->get($class_info);

                $has_class_access = 1;
                if($class_access['type'] === 'custom'){
                    $has_class_access = 0;
                    if($classes->count()>0){
                    foreach($classes as $teacher_class){
                        if(in_array($teacher_class->class_id, $class_access['access'])){
                            $has_class_access = 1;
                        }
                    }
                }
                    
                }

                if ($has_class_access == 1) {
                    $teachers[$teacher->school_id]['name'] = $teacher->school_name;
                    $teachers[$teacher->school_id]['teachers'][$teacher->teacher_id] = $teacher;
                    $teachers[$teacher->school_id]['teachers'][$teacher->teacher_id]['classes'] = $classes;
                }
            }
        }

        return $teachers;
    }
    
    public function activeTeacherList() {
        $info = array('teachers.id as id', 'schools.id as school_id', 'schools.name as school_name', 'staffs.id as staff_id', 'staffs.first_name', 'staffs.middle_name', 'staffs.last_name',
            'staffs.phone', 'staffs.mobile', 'staffs.email', 'staffs.has_paid_role as paid', 'staffs.dbs_number', 'staffs.dbs_issue_date', 'teachers.active');
        return Teacher::join('staffs', 'staffs.id', 'teachers.staff_id')
                        ->join('schools', 'schools.id', 'teachers.school_id')
                        ->where('teachers.active', 1)
                        ->get($info);
    }
    
    public function teacherList() {
        $info = array('teachers.id as id', 'schools.id as school_id', 'schools.name as school_name', 'staffs.id as staff_id', 'staffs.first_name', 'staffs.middle_name', 'staffs.last_name',
            'staffs.phone', 'staffs.mobile', 'staffs.email', 'staffs.has_paid_role as paid', 'staffs.dbs_number', 'staffs.dbs_issue_date', 'teachers.active');
        return Teacher::join('staffs', 'staffs.id', 'teachers.staff_id')
                        ->join('schools', 'schools.id', 'teachers.school_id')
                        ->get($info);
    }

    public function info($teacher_id){
        
        $info = array(
            'teachers.id as teacher_id',
            'users.id as user_id',
            'staffs.id as staff_id',
            'teachers.*',
            'staffs.*',
            'users.*',
        );
        
        $teacher = Teacher::join('staffs', 'staffs.id', 'teachers.staff_id')
                ->join('users', 'users.staff_id', 'staffs.id')
                ->where('teachers.id', $teacher_id)
                ->first($info);
        
        return $teacher;
    }
    
    public function details($teacher_id){
        
        $teacher = $this->info($teacher_id);
        $teacher->profile_picture = $this->s3->picture('staff_profile_picture', $teacher->staff_id);
        $teacher->running_classes = $this->runningClasses($teacher_id);
        $teacher->notes = $this->note->notes('teacher', $teacher_id);
        $teacher->immigration_docs = $this->s3->doc('staff_immigration', $teacher->staff_id);
        $teacher->docs = $this->s3->docs('teacher', $teacher_id);
        
        return $teacher;
    }

    public function store($data) {

        DB::beginTransaction();

        try {
            $staff_id = $data->get('staff');
            $schools = $data->get('schools');
            if (count($schools) > 0) {
                foreach ($schools as $school_id => $classes) {

                    $teacher = Teacher::where('school_id', $school_id)
                            ->where('staff_id', $staff_id)
                            ->first();

                    if (!$teacher) {
                        $teacher_data = array(
                            'school_id' => $school_id,
                            'staff_id' => $staff_id,
                            'created_by' => auth()->id()
                        );
                        $teacher = Teacher::create($teacher_data);
                    }
                    foreach ($classes as $class_id) {
                        $teacher_class = TeacherClass::where('teacher_id', $teacher->id)
                                ->where('class_id', $class_id)
                                ->first();

                        if (!$teacher_class) {
                            $teacher_class_data = array(
                                'teacher_id' => $teacher->id,
                                'class_id' => $class_id,
                                'created_at' => date('Y-m-d H:i:s'),
                                'created_by' => auth()->id()
                            );

                            TeacherClass::create($teacher_class_data);
                        }
                    }
                }
            }
        } catch (Exception $ex) {

            DB::rollBack();
            return false;
        }

        DB::commit();
        return $teacher;
    }

    public function assign($class_id, $teacher_id){
        
        $data = array(
            'class_id' => $class_id,
            'teacher_id' => $teacher_id,
            'status' => 'teaching',
            'created_by' => auth()->id()
        );
        
        return TeacherClass::create($data);
    }
    
    
    public function assignedTeacher($class_id, $teacher_id){
        return TeacherClass::where('class_id', $class_id)->where('teacher_id', $teacher_id)->first();
    }
    
    public function updateAssignedTeacher($class_id, $teacher_id, $request){
        
        $data = array(
            'teacher_id' => $request->get('teacher'),
            'status' => $request->get('status'),
            'updated_at' => date('Y-m-d'),
            'updated_by' => auth()->id()
        );
        
        TeacherClass::where('class_id', $class_id)->where('teacher_id', $teacher_id)->update($data);
        return $request->get('teacher');

    }
    
    
    public function runningClasses($teacher_id) {

        $class_info = array('student_classes.id as class_id', 'student_classes.school_id as school_id', 'schools.name as school', 'courses.title as course', 'student_groups.group as group', 'academic_years.year as year');

        $classes = TeacherClass::join('student_classes', 'student_classes.id', '=', 'teacher_classes.class_id')
                ->join('student_groups', 'student_groups.id', '=', 'student_classes.student_group_id')
                ->join('courses', 'courses.id', '=', 'student_classes.course_id')
                ->join('academic_years', 'academic_years.id', '=', 'student_classes.academic_year_id')
                ->join('schools', 'schools.id', '=', 'student_classes.school_id')
                ->where('teacher_classes.teacher_id', $teacher_id)
                ->where('teacher_classes.status', 'teaching')
                ->where('student_classes.status', 'progressing')
                ->get($class_info);

        if ($classes->count() > 0) {
            foreach ($classes as $class) {
                $class->students = Student::where('class_id', $class->class_id)->where('status', 'studying')->count();
                $class->held = ClassAttendance::where('class_id', $class->class_id)->count();
                $class->attended = TeacherAttendance::join('class_attendances', 'class_attendances.id', 'teacher_attendances.class_attendance_id')
                        ->where('class_attendances.class_id', $class->class_id)
                        ->where('teacher_attendances.teacher_id', $teacher_id)
                        ->count();
            }
        }
        return $classes;
    }

    public function classes($teacher_id){
        
    }
    
}
