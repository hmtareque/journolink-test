<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modules;

use DB;
use App\Models\AcademicYear;
use App\Models\Term;
use App\Models\StudentClass;
use App\Models\ClassAssessment;
use App\Models\StudentGroup;
use App\Models\Course;

/**
 * Description of EducationAdminModule
 *
 * @author hasan
 */
class EducationAdminModule {

    //put your code here

    public function storeAcademicYear($data) {
        $year_data = array(
            'year' => $data->get('year'),
            'created_by' => auth()->id()
        );

        return AcademicYear::create($year_data);
    }

    public function academicYears() {
        return AcademicYear::orderBy('completed', 'asc')
                        ->orderBy('sort')
                        ->take(20)
                        ->get();
    }

    public function academicYear($year_id) {
        return AcademicYear::where('id', $year_id)->first();
    }

    public function academicYearDetails($year_id) {
        $year = AcademicYear::where('id', $year_id)->first();
        if ($year) {
            $year->terms = Term::where('academic_year_id', $year_id)->orderBy('sort')->get();
            $year->terms_completed = Term::where('academic_year_id', $year_id)->where('completed', 1)->count();
            $year->no_of_classes = StudentClass::where('student_classes.academic_year_id', $year_id)->count();
            $year->class_completed = StudentClass::where('student_classes.academic_year_id', $year_id)->where('status', 'completed')->count();
        }
        return $year;
    }

    public function academicYearClasses($year_id) {
        $year = AcademicYear::where('id', $year_id)->first();
        if ($year) {
            $info = array('student_classes.id', 'schools.id as school_id', 'schools.name as school', 'courses.title as course', 'student_groups.group as group', 'academic_years.year as year', 'student_classes.status as status');
            $year->classes = StudentClass::join('schools', 'student_classes.school_id', '=', 'schools.id')
                    ->join('student_groups', 'student_groups.id', '=', 'student_classes.student_group_id')
                    ->join('courses', 'courses.id', '=', 'student_classes.course_id')
                    ->join('academic_years', 'academic_years.id', '=', 'student_classes.academic_year_id')
                    ->where('schools.type', '!=', 'referral')
                    ->where('student_classes.academic_year_id', $year_id)
                    ->orderBy('schools.name')
                    ->orderBy('student_classes.academic_year_id')
                    ->orderBy('student_classes.course_id')
                    ->orderBy('student_classes.student_group_id')
                    ->get($info);
            return $year;
        }
        return false;
    }

    public function updateAcademicYear($year_id, $data) {
        $year_data = array(
            'year' => $data->get('year'),
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => auth()->id()
        );

        return AcademicYear::where('id', $year_id)->update($year_data);
    }

    public function completeAcademicYear($year_id) {
        $year_data = array(
            'completed' => 1,
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => auth()->id()
        );

        return AcademicYear::where('id', $year_id)->update($year_data);
    }

    public function reopenAcademicYear($year_id) {
        $year_data = array(
            'completed' => 0,
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => auth()->id()
        );

        return AcademicYear::where('id', $year_id)->update($year_data);
    }

    public function removeAcademicYear($year_id) {
        $year_data = array(
            'deleted_by' => auth()->id()
        );

        AcademicYear::where('id', $year_id)->update($year_data);

        return AcademicYear::where('id', $year_id)->delete();
    }

    
    /* School Terms */
    
    public function terms($year_id) {
        return Term::where('academic_year_id', $year_id)->orderBy('sort')->get();
    }

    public function term($term_id) {
        return Term::where('id', $term_id)->first();
    }

    public function termDetails($term_id) {
        $term = Term::where('id', $term_id)->first();
        if ($term) {

            $info = array('class_assessments.id', 'student_classes.id as class_id', 'schools.id as school_id', 'schools.name as school', 'courses.title as course', 'student_groups.group as group', 'student_classes.status as status');

            $term->assessments = StudentClass::join('schools', 'student_classes.school_id', '=', 'schools.id')
                    ->join('student_groups', 'student_groups.id', '=', 'student_classes.student_group_id')
                    ->join('courses', 'courses.id', '=', 'student_classes.course_id')
                    ->join('class_assessments', 'class_assessments.class_id', '=', 'student_classes.id')
                    ->where('schools.type', '!=', 'referral')
                    ->where('class_assessments.term_id', $term_id)
                    ->orderBy('schools.name')
                    ->orderBy('student_classes.academic_year_id')
                    ->orderBy('student_classes.course_id')
                    ->orderBy('student_classes.student_group_id')
                    ->get($info);

            $term->assessment_completed = ClassAssessment::where('term_id', $term_id)->where('completed', 1)->count();

            return $term;
        }

        return false;
    }

    public function storeTerm($year_id, $data) {

        $term_data = array(
            'academic_year_id' => $year_id,
            'term' => $data->get('term'),
            'dates' => $data->get('dates'),
            'sort' => 0,
            'created_by' => auth()->id()
        );

        return Term::create($term_data);
    }

    public function updateTerm($term_id, $data) {
        $term_data = array(
            'term' => $data->get('term'),
            'dates' => $data->get('dates'),
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => auth()->id()
        );

        return Term::where('id', $term_id)->update($term_data);
    }

    public function saveTermsOrder($request) {

        $terms = $request->get('terms');

        DB::beginTransaction();

        try {
            if (count($terms) > 0) {
                $sort = 1;
                foreach ($terms as $term_id) {
                    echo $term_id . '=>' . $sort;
                    Term::where('id', $term_id)
                            ->update(array('sort' => $sort));
                    $sort++;
                }
            }
        } catch (Exception $ex) {
            DB::rollBack();
            return false;
        }

        DB::commit();
        return true;
    }

    public function completeTerm($term_id) {
        $term_data = array(
            'completed' => 1,
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => auth()->id()
        );

        return Term::where('id', $term_id)->update($term_data);
    }

    public function reopenTerm($term_id) {
        $term_data = array(
            'completed' => 0,
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => auth()->id()
        );

        return Term::where('id', $term_id)->update($term_data);
    }

    public function removeTerm($term_id) {
        $term_data = array(
            'deleted_by' => auth()->id()
        );

        Term::where('id', $term_id)->update($term_data);

        return Term::where('id', $term_id)->delete();
    }
    
    /* School Term End */
    
    
    /* Student Groups */
    
    public function studentGroups() {
        return StudentGroup::orderBy('sort')->get();
    }

    public function studentGroup($group_id) {
        return StudentGroup::where('id', $group_id)->first();
    }

    public function storeStudentGroup($data) {

        $group_data = array(
            'group' => $data->get('group'),
            'sort' => 0,
            'created_by' => auth()->id()
        );

        return StudentGroup::create($group_data);
    }

    public function updateStudentGroup($group_id, $data) {
        $group_data = array(
            'group' => $data->get('group'),
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => auth()->id()
        );

        return StudentGroup::where('id', $group_id)->update($group_data);
    }

    public function saveStudentGroupOrder($request) {

        $groups = $request->get('groups');

        DB::beginTransaction();

        try {
            if (count($groups) > 0) {
                $sort = 1;
                foreach ($groups as $group_id) {
                    echo $group_id . '=>' . $sort;
                    StudentGroup::where('id', $group_id)
                            ->update(array('sort' => $sort));
                    $sort++;
                }
            }
        } catch (Exception $ex) {
            DB::rollBack();
            return false;
        }

        DB::commit();
        return true;
    }

    public function makeInactiveStudentGroup($group_id) {
        $group_data = array(
            'active' => 0,
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => auth()->id()
        );

        return StudentGroup::where('id', $group_id)->update($group_data);
    }

    public function reactivateStudentGroup($group_id) {
        $group_data = array(
            'active' => 1,
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => auth()->id()
        );

        return StudentGroup::where('id', $group_id)->update($group_data);
    }

    public function removeStudentGroup($group_id) {
        $group_data = array(
            'deleted_by' => auth()->id()
        );

        StudentGroup::where('id', $group_id)->update($group_data);

        return StudentGroup::where('id', $group_id)->delete();
    }
    /* Student Groups End */
    
    
    /* Student Groups */
    
    public function courses() {
        return Course::orderBy('sort')->get();
    }

    public function course($course_id) {
        return Course::where('id', $course_id)->first();
    }

    public function storeCourse($data) {

        $course_data = array(
            'title' => $data->get('title'),
            'description' => $data->get('description'),
            'sort' => 0,
            'created_by' => auth()->id()
        );

        return Course::create($course_data);
    }

    public function updateCourse($course_id, $data) {
        $course_data = array(
            'title' => $data->get('title'),
            'description' => $data->get('description'),
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => auth()->id()
        );

        return Course::where('id', $course_id)->update($course_data);
    }

    public function saveCourseOrder($request) {

        $courses = $request->get('courses');

        DB::beginTransaction();

        try {
            if (count($courses) > 0) {
                $sort = 1;
                foreach ($courses as $course_id) {
                    Course::where('id', $course_id)
                            ->update(array('sort' => $sort));
                    $sort++;
                }
            }
        } catch (Exception $ex) {
            DB::rollBack();
            return false;
        }

        DB::commit();
        return true;
    }

    public function makeInactiveCourse($course_id) {
        $course_data = array(
            'active' => 0,
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => auth()->id()
        );

        return Course::where('id', $course_id)->update($course_data);
    }

    public function reactivateCourse($course_id) {
        $course_data = array(
            'active' => 1,
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => auth()->id()
        );

        return Course::where('id', $course_id)->update($course_data);
    }

    public function removeCourse($course_id) {
        $course_data = array(
            'deleted_by' => auth()->id()
        );

        Course::where('id', $course_id)->update($course_data);

        return Course::where('id', $course_id)->delete();
    }
    /* Student Groups End */

}
