<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modules;

use DB;
use App\Models\Note;
use App\Models\Staff;
use App\Models\User;

/**
 * Description of NoteModule
 *
 * @author hasan
 */
class NoteModule {
    //put your code here
    
    public function notes($object_type, $object_id){
        return Note::where('object_type', $object_type)
                ->where('object_id', $object_id)
                ->get();
    }
    
    public function note($id){
        return Note::join('users', 'users.id', 'notes.created_by')
                ->join('staffs', 'staffs.id', 'users.staff_id')
                ->where('notes.id', $id)
                ->first();
    }
    
    public function store($object_type, $object_id, $note, $private) {
        
        $note_data = array(
            'object_type' => $object_type,
            'object_id' => $object_id,
            'note' => $note,
            'private' => $private,
            'created_at' => date('Y-m-d H:i:s'),
            'created_by' => auth()->id(),
        );

        return Note::create($note_data);
    }

    public function update($id, $note, $private){
        
        $note_data = array(
            'note' => $note,
            'private' => $private,
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => auth()->id(),
        );

        return Note::where('id', $id)->update($note_data);
    }
    
    public function remove($id) {
        
        DB::beginTransaction();
        try {
            Note::where('id', $id)->update(array('created_by' => auth()->id()));
            Note::where('id', $id)->delete();
        } catch (Exception $ex) {
            DB::rollBack();
            return false;
        }
        DB::commit();
        return true;
        
    }

}
