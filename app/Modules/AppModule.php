<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modules;

use DB;
use App\Models\Event;
use App\Models\AppSettings;
use App\Models\User;

use Illuminate\Support\Facades\Storage;

/**
 * Description of AppModule
 *
 * @author hasan
 */
class AppModule {
    
    public function settings(){
        return AppSettings::all();
    }
    
    public function storeSetting(array $data) {
        
        $setting_data = array(
            'param' => strtolower(str_replace(" ", "_", trim($data['parameter']))),
            'type' => $data['type']
        );

        $setting_data['value'] = NULL;
        if ($data['type'] === "image") {
            $setting_data['value'] = Storage::disk('public')->put('images/app/settings', $data['setting_image']);
        } elseif (isset($data['setting_value'])) {
            $setting_data['value'] = trim($data['setting_value']);
        }

        if (isset($data['setting_options']) && trim($data['setting_options']) != "") {
            $setting_data['options'] = trim($data['setting_options']);
        }

        return AppSettings::create($setting_data);
    }

    public function updateSettings(array $data){
        
        DB::beginTransaction();
        try {
            foreach($data as $param => $value){
                
                $setting = AppSettings::where('param', $param)->first();
                
                if($setting){
                
                if($setting->type === "text" || $setting->type === "option"){
                    AppSettings::where('param', $param)->update(array('value' => trim($value)));
                }
                
                if($setting->type === "image"){
                       if(is_object($value)){
                        $image = Storage::disk('public')->put('images/web/settings', $value);
                        AppSettings::where('param', $param)->update(array('value' => trim($image)));
                       }
                    }
                }
            }
        } catch (Exception $ex) {
            DB::rollBack();
            return false;
        }
        
        DB::commit();
        return true;
    }
    
    public function deleteSetting($id){
        return AppSettings::where('id', $id)->forceDelete();
    }
    
    
    
    //////// admin ////////
    
   
    
    
    
}
