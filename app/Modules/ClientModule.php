<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modules;

use DB;
use App\Models\Client;
use App\Models\Engagement;

/**
 * Description of ClientModule
 *
 * @author hasan
 */
class ClientModule {
    
    public function __construct(S3Module $s3, NoteModule $note) {
        $this->s3 = $s3;
        $this->note = $note;
    }
    
    public function store($request){
        
        $data = $request->all();
        unset($data['_token']);
        unset($data['service']);

        
        DB::beginTransaction();
        try{
            $data['ref_no'] = $this->refNo();
            
            $data['date_of_birth'] = php_date($request->get('date_of_birth'));
            
            if($request->get('arrived_in_the_uk') != ""){
                $data['arrived_in_the_uk'] = php_date($request->get('arrived_in_the_uk'));
            }
            
            if($request->get('visa_expiry_date') != ""){
                $data['visa_expiry_date'] = php_date($request->get('visa_expiry_date'));
            }
            
            $data['has_consent_to_use_info'] = ($request->get('has_consent_to_use_info'))? 1 : 0;
            $data['has_consent_to_contacted_for_activities'] = ($request->get('has_consent_to_contacted_for_activities'))? 1 : 0;
            $data['has_consent_to_appear_in_publicity_photos'] = ($request->get('has_consent_to_appear_in_publicity_photos'))? 1 : 0;
            $data['has_consent_to_appear_in_publicity_videos'] = ($request->get('has_consent_to_appear_in_publicity_videos'))? 1 : 0;
            $data['subscribe_newsletter'] = ($request->get('subscribe_newsletter'))? 1 : 0;
            
            $data['created_by'] = auth()->id();
            $client = Client::create($data);
            
            $engagement_data = array(
                'client_id' => $client->id,
                'service' => $request->get('service')
            );
            
            Engagement::create($engagement_data);
            
        } catch (Exception $ex) {
            
            DB::rollBack();
            return false;
        }
        
        DB::commit();
        return $client;
    }
    
    public function info($client_id){
        $client = Client::where('id', $client_id)->first();
        if($client){
            $client->profile_picture = $this->s3->picture('client_profile_picture', $client_id);
            return $client;
        }
        return false;
    }
    
    public function update($client_id, $request){
        
        $data = $request->all();
        unset($data['_token']);
        unset($data['_method']);
        
        DB::beginTransaction();
        try{
            
            $data['date_of_birth'] = php_date($request->get('date_of_birth'));
            
            if($request->get('arrived_in_the_uk') != ""){
                $data['arrived_in_the_uk'] = php_date($request->get('arrived_in_the_uk'));
            }
            
            if($request->get('visa_expiry_date') != ""){
                $data['visa_expiry_date'] = php_date($request->get('visa_expiry_date'));
            }
            
            $data['has_consent_to_use_info'] = ($request->get('has_consent_to_use_info'))? 1 : 0;
            $data['has_consent_to_contacted_for_activities'] = ($request->get('has_consent_to_contacted_for_activities'))? 1 : 0;
            $data['has_consent_to_appear_in_publicity_photos'] = ($request->get('has_consent_to_appear_in_publicity_photos'))? 1 : 0;
            $data['has_consent_to_appear_in_publicity_videos'] = ($request->get('has_consent_to_appear_in_publicity_videos'))? 1 : 0;
            $data['subscribe_newsletter'] = ($request->get('subscribe_newsletter'))? 1 : 0;
            
            $data['updated_at'] = date('Y-m-d H:i:s');
            $data['updated_by'] = auth()->id();
            Client::where('id', $client_id)->update($data);

        } catch (Exception $ex) {
            
            DB::rollBack();
            return false;
        }
        
        DB::commit();
        return true;
    }
    
    public function engage($client_id, $service) {

        DB::beginTransaction();
        try {
            $engaged = Engagement::where('client_id', $client_id)->where('service', $service)->count();
            if ($engaged <= 0) {

                $engagement_data = array(
                    'client_id' => $client_id,
                    'service' => $service
                );
                Engagement::create($engagement_data);
            }
        } catch (Exception $ex) {
            DB::rollBack();
            return false;
        }
        DB::commit();
        return true;
    }

    public function refNo(){
        
        $last_ref_no = 0;
        $last_ref_date = date('ym');
        $last = Client::orderBy('id', 'desc')->first(array('ref_no'));
        
        if($last){
            $last_ref_no = substr($last->ref_no, 4);
            $last_ref_date = substr($last->ref_no, 0, 4);
        }

        $new_ref_no = $last_ref_no+1;
        
        $date = date('ym');
        
        if($date != $last_ref_date && intval($last_ref_no) != 1){
            $new_ref_no = 1;
        }

        $ref_no = date('ym').str_pad($new_ref_no, 4, '0', STR_PAD_LEFT); 
        
        return $ref_no;
    }
    
    
    public function checkDuplicate($request){
        
        $params = $request->all();
        unset($params['_token']);
        
        $checks = array();
        foreach ($params as $key => $value) {
                if ($key == 'date_of_birth') {
                    $checks[$key] = php_date($value);
                } else {
                    $checks[$key] = trim($value);
            }
        }
        
       // print_r($checks);
        
       // exit;
        
         $info = array('id', 'ref_no', 'first_name', 'middle_name', 'last_name', 'date_of_birth', 'ni_number', 'postcode');
        
        
        return Client::where(function($query) use ($checks) {
                        $query->where('postcode', 'like', $checks['postcode'] . '%')
                                ->where('last_name', $checks['last_name'])
                                ->where('date_of_birth', $checks['date_of_birth']);
                    })->take(100)->get($info);
     
       
       
    }
    
    public function verify($client_id){

        $client = Client::where('id', $client_id)->first(array('postcode'));
        
        return ($client)? $client->postcode : "";
        
        
    }

}
