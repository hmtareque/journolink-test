<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modules;

use App\Models\ServiceAccess;
use App\Models\DataAccess;

/**
 * Description of AccessModule
 *
 * @author hasan
 */
class AccessModule {
    //put your code here
    
    public function access(){
        
        
        $accesses = ServiceAccess::where('user_id', auth()->id())->get()->toArray();
        
        $data = array();
        
        foreach($accesses as $access){
            
            $data[$access['service_area']]['area'] = $access['area'];
            
            if($access['area'] == 'custom'){
                
                
                $data_services = DataAccess::where('access_id', $access['id'])->get();
                $services = array();
                foreach($data_services as $service){
                    $services[] = $service->service_id;
                    
                    if($service['categories'] != ''){
                        
                    }
                    
                    
                    
                }
                
                
                
                
                $data[$access['service_area']]['services'] = $services;
                

                
            }
            
            
            
        }
        
        
        return $data;
        
        
    }
    
    
}
