<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modules;

use access;
use DB;
use App\Models\Student;
use App\Models\StudentClass;
use App\Models\School;
use App\Models\ClassAttendance;
use App\Models\StudentAttendance;
use App\Models\Teacher;
use App\Models\TeacherClass;
use App\Models\TeacherAttendance;
use App\Models\ClassAssessment;
use App\Models\StudentAssessment;

/**
 * Description of ClassModule
 *
 * @author hasan
 */
class ClassModule {

    public function __construct(S3Module $s3, NoteModule $note) {
        $this->s3 = $s3;
        $this->note = $note;
    }

    public function activeClassList() {

        $schools = School::where('active', 1)->where('type', '!=', 'referral')->get(array('id', 'name'));
        $info = array('student_classes.id', 'courses.title as course', 'student_groups.group as group', 'academic_years.year as year');


        foreach ($schools as $school) {

            $classes = StudentClass::join('student_groups', 'student_groups.id', '=', 'student_classes.student_group_id')
                    ->join('courses', 'courses.id', '=', 'student_classes.course_id')
                    ->join('academic_years', 'academic_years.id', '=', 'student_classes.academic_year_id')
                    ->where('school_id', $school->id)
                    ->where('student_classes.status', 'progressing')
                    ->orderBy('student_classes.academic_year_id')
                    ->orderBy('student_classes.course_id')
                    ->orderBy('student_classes.student_group_id')
                    ->get($info);

            $school->classes = $classes;
        }


        return $schools;
    }

    public function info($class_id) {
        $info = array('student_classes.id as id', 'schools.id as school_id', 'schools.name as school',
            'courses.title as course', 'courses.id as course_id', 'student_groups.group as group',
            'student_groups.id as group_id', 'academic_years.year as year', 'academic_years.id as year_id',
            'student_classes.status');

        $class = StudentClass::join('schools', 'student_classes.school_id', '=', 'schools.id')
                ->join('student_groups', 'student_groups.id', '=', 'student_classes.student_group_id')
                ->join('courses', 'courses.id', '=', 'student_classes.course_id')
                ->join('academic_years', 'academic_years.id', '=', 'student_classes.academic_year_id')
                ->where('student_classes.id', $class_id)
                ->first($info);

        return $class;
    }

    public function details($class_id) {

        $class = $this->info($class_id);
        $class->teachers = $this->teachers($class_id);
        $class->students = $this->students($class_id);
        $class->attendances = $this->recentAttendances($class_id);
        $class->assessments = $this->assessments($class_id);
        $class->notes = $this->note->notes('class', $class_id);
        $class->docs = $this->s3->docs('class', $class_id);

        return $class;
    }

    public function students($class_id) {

        $info = array('clients.id as client_id', 'clients.ref_no', 'students.id as student_id',
            'students.date_of_enrolment', 'students.status', 'clients.first_name', 'clients.middle_name',
            'clients.last_name', 'clients.date_of_birth', 'clients.phone', 'clients.mobile', 'clients.email',
            'clients.next_of_kin', 'clients.next_of_kin_relationship', 'clients.next_of_kin_phone', 'clients.next_of_kin_mobile', 'clients.next_of_kin_email',
            'gp_name', 'gp_contact', 'has_medical_condition', 'medical_conditions', 'has_allergy', 'allergies', 'has_disability', 'disabilities'
        );

        $students = Student::join('clients', 'clients.id', 'students.client_id')
                ->where('students.class_id', $class_id)
                ->get($info);

        return $students;
    }

    public function activeStudents($class_id) {

        $info = array('clients.id as client_id', 'clients.ref_no', 'students.id as student_id',
            'students.date_of_enrolment', 'students.status', 'clients.first_name', 'clients.middle_name',
            'clients.last_name', 'clients.date_of_birth', 'clients.phone', 'clients.mobile', 'clients.email',
            'clients.next_of_kin', 'clients.next_of_kin_relationship', 'clients.next_of_kin_phone', 'clients.next_of_kin_mobile', 'clients.next_of_kin_email',
            'gp_name', 'gp_contact', 'has_medical_condition', 'medical_conditions', 'has_allergy', 'allergies', 'has_disability', 'disabilities'
        );

        $students = Student::join('clients', 'clients.id', 'students.client_id')
                ->where('students.class_id', $class_id)
                ->where('students.status', 'studying')
                ->get($info);

        return $students;
    }

    public function activeAssessedStudents($class_id, $assessment_id) {

        $info = array('clients.id as client_id', 'clients.ref_no', 'students.id as student_id',
            'students.date_of_enrolment', 'students.status', 'clients.first_name', 'clients.middle_name',
            'clients.last_name', 'clients.date_of_birth', 'clients.phone', 'clients.mobile', 'clients.email',
            'schools.name as referral_school');

        $students = Student::join('clients', 'clients.id', 'students.client_id')
                ->join('schools', 'schools.id', 'students.referred_by')
                ->where('students.class_id', $class_id)
                ->where('students.status', 'studying')
                ->get($info);



        if ($students->count() > 0) {
            foreach ($students as $student) {

                $assessments = ClassAssessment::join('student_assessments', 'student_assessments.class_assessment_id', 'class_assessments.id')
                        ->where('class_assessments.id', $assessment_id)
                        ->where('student_assessments.student_id', $student->student_id)
                        ->get(array('student_assessments.id', 'class_assessments.id as class_assessment_id', 'student_assessments.type', 'student_assessments.level', 'student_assessments.note', 'class_assessments.term_id',
                ));


                $data = array();
                if ($assessments->count() > 0) {
                    foreach ($assessments as $assessment) {
                        $data[$assessment->type] = array(
                            'id' => $assessment->id,
                            'level' => $assessment->level,
                            'note' => $assessment->note,
                            'docs' => $this->s3->doc('student_assessment_docs', $assessment->id)
                        );
                    }
                }


                $student->assessments = $data;
            }
        }




        return $students;
    }

    public function allClasses() {

        $info = array('student_classes.id', 'schools.id as school_id', 'schools.name as school', 'courses.title as course', 'student_groups.group as group', 'academic_years.year as year');

        $classes = StudentClass::join('schools', 'student_classes.school_id', '=', 'schools.id')
                ->join('student_groups', 'student_groups.id', '=', 'student_classes.student_group_id')
                ->join('courses', 'courses.id', '=', 'student_classes.course_id')
                ->join('academic_years', 'academic_years.id', '=', 'student_classes.academic_year_id')
                ->where('schools.type', '!=', 'mainstream')
                ->where('student_classes.status', 'progressing')
                ->orderBy('schools.type')
                ->orderBy('schools.name')
                ->orderBy('student_classes.academic_year_id')
                ->orderBy('student_classes.course_id')
                ->orderBy('student_classes.student_group_id')
                ->get($info);


        return $classes;
    }

    public function classesBySchool() {

        $access = access::services('education');
        
        
        

        if ($access['type'] === 'full') {
            if ($access['data'] === 'all') {
                $schools = School::where('active', 1)->where('type', '!=', 'referral')->get(array('id', 'name'));
            } else {
                $schools = School::where('active', 1)->where('created_by', auth()->id())->where('type', '!=', 'referral')->get(array('id', 'name'));
            }
        } elseif ($access['type'] === 'custom') {

            if ($access['data'] === 'all') {
                $schools = School::where('active', 1)
                        ->where('type', '!=', 'referral')
                        ->whereIn('id', $access['access'])
                        ->get(array('id', 'name'));
            } else {
                $schools = School::where('active', 1)
                        ->where('type', '!=', 'referral')
                        ->whereIn('id', $access['access'])
                        ->where('created_by', auth()->id())
                        ->get(array('id', 'name'));
            }
        } elseif ($access['type'] === 'none') {
            return false;
        }

        $info = array('student_classes.id', 'courses.title as course', 'student_groups.group as group', 'academic_years.year as year');

        
        
        
        
        if ($schools->count()>0) {
            foreach ($schools as $school) {

                $class_access = access::categories('education', $school->id);

                $classes = false;
                if ($class_access['type'] === 'full') {
                    if ($class_access['data'] === 'all') {
                        $classes = StudentClass::join('student_groups', 'student_groups.id', '=', 'student_classes.student_group_id')
                                ->join('courses', 'courses.id', '=', 'student_classes.course_id')
                                ->join('academic_years', 'academic_years.id', '=', 'student_classes.academic_year_id')
                                ->where('student_classes.school_id', $school->id)
                                ->where('student_classes.status', 'progressing')
                                ->orderBy('student_classes.academic_year_id')
                                ->orderBy('student_classes.course_id')
                                ->orderBy('student_classes.student_group_id')
                                ->get($info);
                    } else {
                        $classes = StudentClass::join('student_groups', 'student_groups.id', '=', 'student_classes.student_group_id')
                                ->join('courses', 'courses.id', '=', 'student_classes.course_id')
                                ->join('academic_years', 'academic_years.id', '=', 'student_classes.academic_year_id')
                                ->where('student_classes.school_id', $school->id)
                                ->where('student_classes.status', 'progressing')
                                ->where('created_by', auth()->id())
                                ->orderBy('student_classes.academic_year_id')
                                ->orderBy('student_classes.course_id')
                                ->orderBy('student_classes.student_group_id')
                                ->get($info);
                    }
                } elseif ($class_access['type'] === 'custom') {

                    if ($class_access['data'] === 'all') {
                        $classes = StudentClass::join('student_groups', 'student_groups.id', '=', 'student_classes.student_group_id')
                                ->join('courses', 'courses.id', '=', 'student_classes.course_id')
                                ->join('academic_years', 'academic_years.id', '=', 'student_classes.academic_year_id')
                                ->where('student_classes.school_id', $school->id)
                                ->where('student_classes.status', 'progressing')
                                ->whereIn('student_classes.id', $class_access['access'])
                                ->orderBy('student_classes.academic_year_id')
                                ->orderBy('student_classes.course_id')
                                ->orderBy('student_classes.student_group_id')
                                ->get($info);
                    } else {
                        $classes = StudentClass::join('student_groups', 'student_groups.id', '=', 'student_classes.student_group_id')
                                ->join('courses', 'courses.id', '=', 'student_classes.course_id')
                                ->join('academic_years', 'academic_years.id', '=', 'student_classes.academic_year_id')
                                ->where('student_classes.school_id', $school->id)
                                ->where('student_classes.status', 'progressing')
                                ->whereIn('student_classes.id', $class_access['access'])
                                ->where('student_classes.created_by', auth()->id())
                                ->orderBy('student_classes.academic_year_id')
                                ->orderBy('student_classes.course_id')
                                ->orderBy('student_classes.student_group_id')
                                ->get($info);
                    }
                }

                if ($classes && $classes->count() > 0) {
                    foreach ($classes as $class) {
                        $class->teachers = $this->teachers($class->id);
                        $class->held = ClassAttendance::where('class_id', $class->id)->count();
                        $class->students = Student::where('class_id', $class->id)->count();
                    }


                    $school->classes = $classes;
                }
            }
        }

        return $schools;
    }

    public function store($request) {

        $class_data = array(
            'academic_year_id' => $request->get('academic_year'),
            'school_id' => $request->get('school'),
            'student_group_id' => $request->get('group'),
            'course_id' => $request->get('course'),
            'created_by' => auth()->id()
        );

        return StudentClass::create($class_data);
    }

    public function update($class_id, $request) {

        $class_data = array(
            'academic_year_id' => $request->get('academic_year'),
            'school_id' => $request->get('school'),
            'student_group_id' => $request->get('group'),
            'course_id' => $request->get('course'),
            'status' => $request->get('status'),
            'updated_at' => date('Y-m-d H:i:s'),
            'updated_by' => auth()->id()
        );

        return StudentClass::where('id', $class_id)->update($class_data);
    }

    // attendance 

    public function storeAttendance($class_id, $request) {

        DB::beginTransaction();
        try {

            $class_attendance_data = array(
                'date' => date('Y-m-d', strtotime(php_date($request->get('date')))),
                'class_id' => $class_id,
                'created_by' => auth()->id()
            );

            $class_attendance = ClassAttendance::create($class_attendance_data);

            // insert teachers attendance
            foreach ($request->get('teachers') as $teacher_id) {
                $teacher_attendance_data = array(
                    'class_attendance_id' => $class_attendance->id,
                    'teacher_id' => $teacher_id
                );
                TeacherAttendance::create($teacher_attendance_data);
            }

            // insert students attendances
            foreach ($request->get('students') as $student) {

                $present = (isset($student['attended'])) ? 1 : 0;
                $student_attendance_data = array(
                    'class_attendance_id' => $class_attendance->id,
                    'student_id' => $student['id'],
                    'present' => $present,
                    'reason_for_absence' => ($present == 0) ? $student['reason_for_absence'] : NULL,
                );
                StudentAttendance::create($student_attendance_data);
            }
        } catch (Exception $ex) {
            DB::rollBack();
            return false;
        }

        DB::commit();
        return true;
    }

    public function updateAttendance($class_attendance_id, $request) {

        DB::beginTransaction();
        try {

            $class_attendance_data = array(
                'date' => date('Y-m-d', strtotime(php_date($request->get('date')))),
                'updated_at' => date('Y-m-d H:i:s'),
                'updated_by' => auth()->id()
            );

            ClassAttendance::where('id', $class_attendance_id)->update($class_attendance_data);


            TeacherAttendance::where('class_attendance_id', $class_attendance_id)->delete();
            // insert teachers attendance
            foreach ($request->get('teachers') as $teacher_id) {
                $teacher_attendance_data = array(
                    'class_attendance_id' => $class_attendance_id,
                    'teacher_id' => $teacher_id
                );
                TeacherAttendance::create($teacher_attendance_data);
            }



            foreach ($request->get('students') as $student) {

                $present = (isset($student['attended'])) ? 1 : 0;
                $student_attendance_data = array(
                    'present' => $present,
                    'reason_for_absence' => ($present == 0) ? $student['reason_for_absence'] : NULL,
                    'updated_at' => date('Y-m-d H:i:s'),
                );
                StudentAttendance::where('class_attendance_id', $class_attendance_id)->where('student_id', $student['id'])->update($student_attendance_data);
            }
        } catch (Exception $ex) {
            DB::rollBack();
            return false;
        }

        DB::commit();
        return true;
    }

    public function recentAttendances($class_id, $take = 5) {

        $attendances = ClassAttendance::where('class_id', $class_id)
                ->orderBy('class_attendances.date', 'desc')
                ->take($take)
                ->get(array('class_attendances.id', 'class_attendances.class_id', 'class_attendances.date'));

        foreach ($attendances as $attendance) {
            $attendance->students = StudentAttendance::where('class_attendance_id', $attendance->id)->count();
            $attendance->attended = StudentAttendance::where('class_attendance_id', $attendance->id)->where('present', 1)->count();
        }


        return $attendances;
    }

    public function attendances($class_id) {

        $attendances = ClassAttendance::where('class_id', $class_id)
                ->orderBy('date', 'desc')
                ->get(array('class_attendances.id', 'class_attendances.class_id', 'class_attendances.date'));

        foreach ($attendances as $attendance) {
            $attendance->students = StudentAttendance::where('class_attendance_id', $attendance->id)->count();
            $attendance->attended = StudentAttendance::where('class_attendance_id', $attendance->id)->where('present', 1)->count();

            $attendance->teachers = TeacherAttendance::join('teachers', 'teacher_attendances.teacher_id', 'teachers.id')
                    ->join('staffs', 'staffs.id', 'teachers.staff_id')
                    ->where('teacher_attendances.class_attendance_id', $attendance->id)
                    ->get(array('teachers.id as teacher_id', 'staffs.first_name', 'staffs.middle_name', 'staffs.last_name'));
        }

        return $attendances;
    }

    public function attendance($attendance_id) {

        $attendance = ClassAttendance::where('class_attendances.id', $attendance_id)->first();

        $info = array('student_attendances.id', 'clients.id as client_id', 'clients.ref_no', 'students.id as student_id',
            'students.date_of_enrolment', 'students.status', 'clients.first_name', 'clients.middle_name',
            'clients.last_name', 'clients.date_of_birth', 'clients.phone', 'clients.mobile', 'clients.email',
            'clients.next_of_kin', 'clients.next_of_kin_relationship', 'clients.next_of_kin_phone', 'clients.next_of_kin_mobile', 'clients.next_of_kin_email',
            'gp_name', 'gp_contact', 'has_medical_condition', 'medical_conditions', 'has_allergy', 'allergies', 'has_disability', 'disabilities',
            'student_attendances.present', 'student_attendances.reason_for_absence'
        );

        $attendance->students = StudentAttendance::join('students', 'student_attendances.student_id', 'students.id')
                ->join('clients', 'students.client_id', 'clients.id')
                ->where('class_attendance_id', $attendance_id)
                ->get($info);

        $attendance->teachers = TeacherAttendance::join('teachers', 'teacher_attendances.teacher_id', 'teachers.id')
                ->join('staffs', 'staffs.id', 'teachers.staff_id')
                ->where('teacher_attendances.class_attendance_id', $attendance_id)
                ->get(array('teachers.id as teacher_id', 'staffs.first_name', 'staffs.middle_name', 'staffs.last_name'));

        $attendance->teacher_ids = TeacherAttendance::where('teacher_attendances.class_attendance_id', $attendance_id)
                        ->pluck('teacher_id')->all();


        return $attendance;
    }

    // teachers 
    public function teachers($class_id) {

        $teacher_info = array('teachers.id as teacher_id', 'teacher_classes.class_id as class_id', 'staffs.first_name', 'staffs.middle_name', 'staffs.last_name',
            'staffs.phone', 'staffs.mobile', 'staffs.dbs_number', 'dbs_issue_date', 'teacher_classes.status');

        $teachers = TeacherClass::join('teachers', 'teacher_classes.teacher_id', 'teachers.id')
                ->join('staffs', 'staffs.id', 'teachers.staff_id')
                ->where('teacher_classes.class_id', $class_id)
                ->get($teacher_info);

        return $teachers;
    }

    public function activeTeachers($class_id) {

        $teacher_info = array('teachers.id as teacher_id', 'teacher_classes.class_id as class_id', 'staffs.first_name', 'staffs.middle_name', 'staffs.last_name',
            'staffs.phone', 'staffs.mobile', 'staffs.dbs_number', 'dbs_issue_date', 'teacher_classes.status');

        $teachers = TeacherClass::join('teachers', 'teacher_classes.teacher_id', 'teachers.id')
                ->join('staffs', 'staffs.id', 'teachers.staff_id')
                ->where('teacher_classes.class_id', $class_id)
                ->where('teacher_classes.status', 'teaching')
                ->get($teacher_info);

        return $teachers;
    }

    //terms 
    public function terms($class_id) {
        return StudentClass::join('school_terms', 'student_classes.academic_year_id', 'school_terms.academic_year_id')
                        ->where('student_classes.id', $class_id)
                        ->orderBy('school_terms.sort', 'desc')
                        ->get(array('school_terms.id', 'school_terms.term', 'school_terms.dates'));
    }

    // assessments 

    public function assessments($class_id) {
        return ClassAssessment::join('school_terms', 'school_terms.id', 'class_assessments.term_id')
                        ->where('class_id', $class_id)->get(array('class_assessments.id', 'school_terms.id as term_id', 'school_terms.term', 'school_terms.dates'));
    }

    public function assessment($id) {
        return ClassAssessment::join('school_terms', 'school_terms.id', 'class_assessments.term_id')
                        ->where('class_assessments.id', $id)->first(array('class_assessments.id', 'school_terms.id as term_id', 'school_terms.term', 'school_terms.dates'));
    }

    public function updateAssessment($class_assessment_id, $request) {

        $students = $request->get('students');

        if (is_array($students) && count($students) > 0) {
            foreach ($students as $student_id => $assessments) {

                foreach ($assessments as $type => $assessment) {

                    if (isset($assessment['id'])) {

                        $update_data = array(
                            'level' => $assessment['level'],
                            'note' => $assessment['note'],
                            'updated_at' => date('Y-m-d H:i:s'),
                            'updated_by' => auth()->id()
                        );

                        StudentAssessment::where('id', $assessment['id'])->update($update_data);
                    } else {

                        $create_data = array(
                            'class_assessment_id' => $class_assessment_id,
                            'student_id' => $student_id,
                            'type' => $type,
                            'level' => $assessment['level'],
                            'note' => $assessment['note'],
                            'created_by' => auth()->id()
                        );

                        StudentAssessment::create($create_data);
                    }
                }
            }
        }




        return true;
    }

}
