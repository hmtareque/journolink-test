<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modules;

use DB;
use App\Models\Client;
use App\Models\Payment;
use App\Models\StripePlan;
use App\Models\StripeCustomer;
use App\Models\StripeCustomerSource;
use App\Models\StripeSubscription;
use App\Models\StripeEvent;

/**
 * Description of StripeModule
 *
 * @author hasan
 */
class StripeModule {

    public function __construct() {
        \Stripe\Stripe::setApiKey(config('services.stripe.secret'));
    }

    /* plans */

    public function plans() {
        return StripePlan::orderBy('created_at', 'desc')->get();
    }
    
    public function servicePaymentPlans($service) {
        return StripePlan::where('service_area', $service)
                ->where('active', 1)
                ->orderBy('created_at', 'desc')->get();
    }

    public function storePlan($request) {

        $plan_id = trim(strtolower($request->get('service') . '_' . str_replace(' ', '_', $request->get('name')) . '_' . date('Ymd')));

        $stripe_plan_data = array(
            'id' => $plan_id,
            'name' => $request->get('name'),
            'interval' => $request->get('interval'),
            'currency' => 'gbp',
            'amount' => $request->get('amount') * 100,
            'statement_descriptor' => trim(strtoupper($request->get('statement_description'))),
        );

        DB::beginTransaction();
        try {
            \Stripe\Plan::create($stripe_plan_data);

            $plan_data = array(
                'service_area' => $request->get('service'),
                'plan_id' => $plan_id,
                'name' => $request->get('name'),
                'interval' => $request->get('interval'),
                'currency' => 'gbp',
                'amount' => $request->get('amount'),
                'statement_descriptor' => trim(strtoupper($request->get('statement_description'))),
                'created_by' => auth()->id()
            );

            StripePlan::create($plan_data);
        } catch (Exception $ex) {
            DB::rollBack();
            return (object) array('status' => false, 'error' => $ex->getMessage());
        }

        DB::commit();
        return (object) array('status' => true);
    }

    public function removePlan() {
        
    }

    /* end plans */


    /* customers */

    public function customers() {
        
    }

    /* charge */

    public function charge($client_id, $fee_id, $request) {

        DB::beginTransaction();

        try {
            $client = Client::find($client_id);
            $donation_amount = ($request->get('donate') == 1) ? $request->get('donation_amount') : 0;
            $stripe_payment_amount = ($request->get('amount') + $donation_amount) * 100;
            $stripe_token = $request->get('stripeToken');

            $stripe_customer_data = array(
                'description' => strtoupper(trim($client->first_name) . ' ' . trim($client->last_name) . ' (' . $client->id . ')'),
                'source' => $stripe_token
            );

            // Create a Customer:
            $stripe_customer = \Stripe\Customer::create($stripe_customer_data);


            $customer_data = array(
                'client_id' => $client->id,
                'customer_id' => $stripe_customer->id,
                'created_by' => auth()->id()
            );

            StripeCustomer::create($customer_data);

            $charge_data = array(
                "description" => trim($request->get('description')),
                "amount" => $stripe_payment_amount,
                "currency" => "gbp",
                "customer" => $stripe_customer->id
            );

            // Charge the Customer instead of the card:
            $charge = \Stripe\Charge::create($charge_data);

            $payment_data = array(
                'fee_id' => $fee_id,
                'description' => trim($request->get('description')),
                'payment_method' => 'card',
                'charge_id' => $charge->id,
                'amount' => $request->get('amount'),
                'has_donation' => $request->get('donate'),
                'donation_amount' => ($request->get('donate') == 1) ? $request->get('donation_amount') : 0,
                'status' => 'succeeded',
                'created_by' => auth()->id()
            );

            Payment::create($payment_data);
        } catch (Exception $ex) {
            DB::rollBack();
            return (object) array('status' => false, 'error' => $ex->getMessage());
        }

        DB::commit();
        return (object) array('status' => true);
    }

}
