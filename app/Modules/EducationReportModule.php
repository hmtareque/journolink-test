<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

namespace App\Modules;

use DB;

use App\Models\ClassAttendance;

use App\Models\Student;
use App\Models\StudentClass;
use App\Models\StudentAttendance;
use App\Models\School;

use App\Models\Teacher;
use App\Models\TeacherClass;
use App\Models\TeacherAttendance;

use App\Models\Course;

use App\Models\StudentAssessment;
use App\Models\ClassAssessment;

use App\Models\Note;


/**
 * Description of NoteModule
 *
 * @author hasan
 */
class EducationReportModule {
    
    public function __construct(NoteModule $note, S3Module $s3) {
        $this->note = $note;
        $this->s3 = $s3;
    }

    
    public function referralSchoolReport($request){
        
        
        $school = $request->get('school');
        $from = date('Y-m-d', strtotime(php_date($request->get('from'))));
        $to = date('Y-m-d', strtotime(php_date($request->get('to'))));
        
        $student_info = array(
          'schools.id as school_id', 'schools.name as school_name',  
          'student_classes.id as class_id', 'courses.title as course', 'student_groups.group as group',
          'students.id as student_id', 'students.date_of_enrolment as date_of_enrolment',
          'clients.id as client_id', 'clients.first_name', 'clients.middle_name', 'clients.last_name'
        );
        
        $students = Student::join('clients', 'clients.id', 'students.client_id')
                ->join('student_classes', 'student_classes.id', '=', 'students.class_id')
                ->join('schools', 'student_classes.school_id', '=', 'schools.id')
                ->join('student_groups', 'student_groups.id', '=', 'student_classes.student_group_id')
                ->join('courses', 'courses.id', '=', 'student_classes.course_id')
                ->join('academic_years', 'academic_years.id', '=', 'student_classes.academic_year_id')
                ->where('referred_by', $school)
                ->where('date_of_enrolment', '>=', $from)
                ->where('date_of_enrolment', '<=', $to)
                ->orderBy('students.date_of_enrolment', 'asc')
                ->get($student_info);

        $attendance_info = array(
          'student_attendances.id as attendance_id',
          'schools.id as school_id', 'schools.name as school_name',  
          'clients.id as client_id', 'clients.first_name', 'clients.middle_name', 'clients.last_name',
          'student_classes.id as class_id', 'courses.title as course', 'student_groups.group as group',
          'students.id as student_id', 'class_attendances.date as date', 'student_attendances.present',
          'student_attendances.reason_for_absence'
        );
        
        $data = StudentAttendance::join('class_attendances', 'student_attendances.class_attendance_id', 'class_attendances.id')
                ->join('student_classes', 'student_classes.id', '=', 'class_attendances.class_id')
                ->join('schools', 'student_classes.school_id', '=', 'schools.id')
                ->join('student_groups', 'student_groups.id', '=', 'student_classes.student_group_id')
                ->join('courses', 'courses.id', '=', 'student_classes.course_id')
                ->join('academic_years', 'academic_years.id', '=', 'student_classes.academic_year_id')
                ->join('students', 'students.id', 'student_attendances.student_id')
                ->join('clients', 'clients.id', 'students.client_id')
                ->where('students.referred_by', $school)
                ->where('class_attendances.date', '>=', $from)
                ->where('class_attendances.date', '<=', $to)
                ->orderBy('class_attendances.date', 'asc')
                ->get($attendance_info);
        
        $attendances = array();
        if($data->count()>0){
            foreach($data as $attendance){
                $attendances[$attendance->client_id]['name'] = $attendance->first_name.' '.$attendance->middle_name.' '.$attendance->last_name;
                $attendances[$attendance->client_id]['attendances'][$attendance->attendance_id] = $attendance;
            }
        }
        
        
        
        $report = array(
            'students' => $students,
            'attendances' => $attendances
        );
        
        
        return $report;
    }
    
    
    public function serviceManagerReport($request){
        $school = $request->get('school');
        $from = date('Y-m-d', strtotime(php_date($request->get('from'))));
        $to = date('Y-m-d', strtotime(php_date($request->get('to'))));
        
        
        $referral_schools = School::where('type', 'referral')->where('active', 1)->get(array('id', 'name'));
        
        if($referral_schools->count()>0){
            foreach($referral_schools as $referral_school){
                
                $enrolled_students = Student::where('school_id', $school)->where('referred_by', $referral_school->id)->get();
                
                $attended = 0;
                foreach($enrolled_students as $enrolled_student){
                    
                    $present = ClassAttendance::join('student_attendances', 'student_attendances.class_attendance_id', 'class_attendances.id')
                            //->where('class_attendances.id', $enrolled_student->class_id)
                            ->where('class_attendances.date', '>=', $from)
                            ->where('class_attendances.date', '<=', $to)
                            ->where('student_attendances.student_id', $enrolled_student->id)
                            ->where('student_attendances.present', 1)
                            ->count();
                    
                   // $present = StudentAttendance::where('student_id', $enrolled_student->id)->where('present', 1)->count();
                    
                    if($present>0){
                        $attended++;
                    }
                }

                $referral_school->total_enrolled = $enrolled_students->count('client_id');
                $referral_school->enrolled = Student::where('school_id', $school)
                        ->where('referred_by', $referral_school->id)
                        ->where('date_of_enrolment', '>=', $from)
                        ->where('date_of_enrolment', '<=', $to)
                        ->count('client_id');
                
                $referral_school->attended = $attended;

            }
        }
        
        
        //class 
        $class_info = array('student_classes.id as id', 'courses.title as course', 'student_groups.group as group');
        
        $classes = StudentClass::join('student_groups', 'student_groups.id', '=', 'student_classes.student_group_id')
                ->join('courses', 'courses.id', '=', 'student_classes.course_id')
                ->where('student_classes.school_id', $school)
                ->where('student_classes.status', 'progressing') 
                ->orderBy('student_classes.course_id')
                ->orderBy('student_classes.student_group_id')
                ->get($class_info);
            
            if($classes->count()>0){
                foreach($classes as $class){
                    
                $teacher_info = array('teachers.id as teacher_id', 'staffs.first_name', 'staffs.middle_name', 'staffs.last_name', 'teacher_classes.status as status');

                $teachers = TeacherClass::join('teachers', 'teacher_classes.teacher_id', 'teachers.id')
                        ->join('staffs', 'staffs.id', 'teachers.staff_id')
                        ->where('teacher_classes.class_id', $class->id)
                        ->get($teacher_info);

                $class->teachers = $teachers;
                $class->enrolled_students = Student::where('class_id', $class->id)->where('status', 'studying')->count();
                
                $class->enrolled = Student::where('class_id', $class->id)
                        ->where('date_of_enrolment', '>=', $from)
                        ->where('date_of_enrolment', '<=', $to)
                        ->count('client_id');
                
                $attendances = ClassAttendance::join('student_attendances', 'student_attendances.class_attendance_id', 'class_attendances.id')
                        ->where('student_attendances.present', 1)
                        ->where('class_attendances.class_id', $class->id)
                        ->where('class_attendances.date', '>=', $from)
                        ->where('class_attendances.date', '<=', $to)
                        ->groupBy('class_attendances.id', 'class_attendances.date')
                        ->orderBy('class_attendances.date', 'asc')
                        ->get(array('class_attendances.id as id', 'class_attendances.date', DB::raw('count(student_attendances.id) as attended')));
                
                
                
                 foreach($attendances as $attendance){
                      $attended_teacher_info = array('teachers.id as teacher_id', 'staffs.first_name', 'staffs.middle_name', 'staffs.last_name');

                     $attendance->teachers = TeacherAttendance::join('teachers', 'teacher_attendances.teacher_id', 'teachers.id')
                        ->join('staffs', 'staffs.id', 'teachers.staff_id')
                        ->where('teacher_attendances.class_attendance_id', $attendance->id)
                        ->get($attended_teacher_info);
                 }
                
                $class->attendances = $attendances;

                }
                
            }
            
            
            //course 
            $courses = Course::where('active', 1)->get(array('id', 'title'));
            
            if($courses->count()>0){
                foreach($courses as $course){
                    
                    $students = StudentClass::join('student_groups', 'student_groups.id', '=', 'student_classes.student_group_id')
                        ->join('courses', 'courses.id', '=', 'student_classes.course_id')
                        ->join('students', 'students.class_id', '=', 'student_classes.id')
                        ->where('student_classes.school_id', $school)
                        ->where('student_classes.status', 'progressing')
                        ->where('student_classes.course_id', $course->id)
                        ->get(array('students.id'));
                    
                    
                    $attended = 0;
                foreach($students as $student){
                    
                    $present = ClassAttendance::join('student_attendances', 'student_attendances.class_attendance_id', 'class_attendances.id')
                            //->where('class_attendances.id', $enrolled_student->class_id)
                            ->where('class_attendances.date', '>=', $from)
                            ->where('class_attendances.date', '<=', $to)
                            ->where('student_attendances.student_id', $student->id)
                            ->where('student_attendances.present', 1)
                            ->count();
                    
                   // $present = StudentAttendance::where('student_id', $enrolled_student->id)->where('present', 1)->count();
                    
                    if($present>0){
                        $attended++;
                    }
                }
                    
                    
                    
                    $course->total_enrolled = $students->count();

                    $course->enrolled = StudentClass::join('student_groups', 'student_groups.id', '=', 'student_classes.student_group_id')
                        ->join('courses', 'courses.id', '=', 'student_classes.course_id')
                        ->join('students', 'students.class_id', '=', 'student_classes.id')
                        ->where('student_classes.school_id', $school)
                        ->where('student_classes.status', 'progressing')
                        ->where('student_classes.course_id', $course->id)
                        ->where('students.date_of_enrolment', '>=', $from)
                        ->where('students.date_of_enrolment', '<=', $to)
                        ->count();
                
                    $course->attended = $attended;
                }
            }
            
          

            $report = array(
            'notes' => $this->note->notes('school', $school),
            'schools' => $referral_schools,
            'classes' => $classes,
            'courses' => $courses,
        );

        return $report;
    }
    
    public function studentReport($client_id, $request){
        
        $year = $request->get('year');
        $from = date('Y-m-d', strtotime(php_date($request->get('from'))));
        $to = date('Y-m-d', strtotime(php_date($request->get('to'))));
        
        //class 
        $class_info = array('student_classes.id as class_id', 'students.id as student_id', 'students.date_of_enrolment', 'schools.name as school', 'courses.title as course', 'student_groups.group as group', 'students.status as student_status');
        
        $classes = Student::join('clients', 'clients.id', 'students.client_id')
                ->join('student_classes', 'student_classes.id', '=', 'students.class_id')
                ->join('schools', 'student_classes.school_id', '=', 'schools.id')
                ->join('student_groups', 'student_groups.id', '=', 'student_classes.student_group_id')
                ->join('courses', 'courses.id', '=', 'student_classes.course_id')
                ->join('academic_years', 'academic_years.id', '=', 'student_classes.academic_year_id')
                
               // ->where('student_classes.status', 'progressing')
                ->where('students.client_id', $client_id)
                ->where('academic_years.id', $year)
                ->orderBy('students.date_of_enrolment', 'asc')
                ->get($class_info);
        
        $attendance_info = array('student_classes.id as class_id',  'schools.name as school', 'courses.title as course', 'student_groups.group as group', 'class_attendances.date', 'student_attendances.present', 'student_attendances.reason_for_absence', 'student_classes.status as class_status');
        
        
        $assessments = array();
        if($classes->count()>0){
            
            foreach($classes as $class){
            
          
                $data = StudentAssessment::join('class_assessments', 'class_assessments.id', 'student_assessments.class_assessment_id')
                        ->join('students', 'students.id', 'student_assessments.student_id')
                        ->join('student_classes', 'student_classes.id', '=', 'students.class_id')
                        ->join('schools', 'student_classes.school_id', '=', 'schools.id')
                        ->join('student_groups', 'student_groups.id', '=', 'student_classes.student_group_id')
                        ->join('courses', 'courses.id', '=', 'student_classes.course_id')
                        ->join('school_terms', 'school_terms.id', 'class_assessments.term_id')
                        ->join('academic_years', 'school_terms.academic_year_id', 'academic_years.id')
                        ->where('class_assessments.class_id', $class->class_id)
                        ->where('students.client_id', $client_id)
                        ->where('academic_years.id', $year)
                        ->orderBy('school_terms.sort', 'asc')
                        ->get(array('student_assessments.id', 'class_assessments.class_id as class_id', 
                            'courses.title as course', 'student_groups.group as group', 'schools.name as school_name', 
                            'class_assessments.id as class_assessment_id', 'student_assessments.type', 'student_assessments.level', 
                            'class_assessments.term_id', 'school_terms.term', 'school_terms.dates', 'academic_years.year',
                ));

                // print_r($assessments);

        
        foreach ($data as $assessment) {
            
            $terms[$assessment->term_id]['term'] = $assessment->term;
            $terms[$assessment->term_id]['year'] = $assessment->year;
            $terms[$assessment->term_id]['dates'] = $assessment->dates;
            $terms[$assessment->term_id]['assessments'][$assessment->type] = array(
                'id' => $assessment->id,
                'level' => $assessment->level,
                'note' => $assessment->note,
                'docs' => $this->s3->doc('student_assessment_docs', $assessment->id)
                
                );
            
            $assessments[$assessment->class_id]['class'] = $assessment->course.' '.$assessment->group;
            $assessments[$assessment->class_id]['school'] = $assessment->school_name;
            $assessments[$assessment->class_id]['terms'] = $terms;
            
           // $assessments[$assessment->class_id][$assessment->term_id]['year'] = $assessment->year;
            
        }
        
        
        
        
        
        
            }
            
           
        }
        
        
        
        
        $attendances = Student::join('clients', 'clients.id', 'students.client_id')
                ->join('student_classes', 'student_classes.id', '=', 'students.class_id')
                ->join('schools', 'student_classes.school_id', '=', 'schools.id')
                ->join('student_groups', 'student_groups.id', '=', 'student_classes.student_group_id')
                ->join('courses', 'courses.id', '=', 'student_classes.course_id')
                ->join('student_attendances', 'students.id', 'student_attendances.student_id')
                ->join('class_attendances', 'student_attendances.class_attendance_id', 'class_attendances.id')
               // ->where('student_classes.status', 'progressing')
                ->where('students.client_id', $client_id)
                ->where('student_classes.academic_year_id', $year)
                ->where('class_attendances.date', '>=', $from)
                ->where('class_attendances.date', '<=', $to)
                ->orderBy('class_attendances.date', 'asc')
                ->get($attendance_info);
        
        
        
        
        $notes = Note::where('object_type', 'student_client')
                ->where('object_id', $client_id)
                ->get();
        
        
        
        
        $report = array(
            'classes' => $classes,
            'attendances' => $attendances,
            'assessments' => $assessments,
            'notes' => $notes
        );
        
        return $report;
    }
    

}
