<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AssignTeacherRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $class_id = FormRequest::segment(4);
        
        return [
            'teacher' => 'required|unique:teacher_classes,teacher_id,NULL,id,class_id,'.$class_id
        ];
    }
    
    public function messages() {
        $messages['teacher.unique'] = 'This teacher already assigned for the class!';
        return $messages;
    }
}
