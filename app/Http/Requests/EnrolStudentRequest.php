<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use App\Models\Student;

class EnrolStudentRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        
        $rules = array(
            'type' => 'required',
            'enrolment_date' => 'required|uk_date',
            'referred_by' => 'required',
            'academic_year' => 'required',
            'term' => 'required',
            'school' => 'required',
            'classes' => 'required',
            'can_leave_on_own' => 'required_if:type,child',
            'have_to_collect_by' => 'required_if:can_leave_on_own,0',
        );
        
        $client_id = FormRequest::segment(4);
        $no_of_class = 0;
        $classes = FormRequest::get('classes');
        if(count($classes)>0){
            foreach($classes as $class){
                if(isset($class['class'])){
                   $no_of_class += 1;
                   $already_enrolled = Student::where('client_id', $client_id)
                           ->where('class_id', $class['class'])
                           ->count();
                   if($already_enrolled>0){
                       $rules['enrolled-'.$class['class'].'-class'] = 'required';
                      
                   }
                }
            }
        } 
        
        if($no_of_class<=0){
            $rules['class'] = 'required';
        }
        
        
        
        
       
        
        return $rules;
    }
    
    public function messages() {
        
        $messages['class.required'] = 'Please select at least one class to enrol.';
        $messages['enrolled-*-class.required'] = 'Already enrolled in this class';
        $messages['have_to_collect_by.required_if'] = 'Please provide who will collect the student.';
        return $messages;
        
    }
    
}