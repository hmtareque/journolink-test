<?php

namespace App\Http\Requests;

use App\Models\ClassAssessment;
use Illuminate\Foundation\Http\FormRequest;


class StoreStudentAssessmentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $student_id = FormRequest::get('student_id');
        $type = FormRequest::get('type');
        $class_id = FormRequest::segment(6);
        $term_id = FormRequest::get('term');
        $assessment = ClassAssessment::join('student_assessments', 'student_assessments.class_assessment_id', 'class_assessments.id')
                ->where('class_assessments.term_id', $term_id)
                ->where('class_assessments.class_id', $class_id)
                ->where('student_assessments.type', $type)
                ->where('student_assessments.student_id', $student_id)
                ->count();
        
        $exist = '';
        if($assessment>0){
            $exist = 'required';
        }
        
        
        return [
            'type' => 'required',
            'term' => 'required',
            'level' => 'required',
            'note' => '',
            'docs.*' => 'nullable|file',
            'exist' => $exist
        ];
    }
    
    public function messages() {
        $messages['exist.required'] = 'Assessment already exist!';
        return $messages;
    }
}
