<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use App\Models\Fee;
use App\Models\Payment;

class StorePaymentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        $fee_id = FormRequest::segment(7);
        $fee = Fee::find($fee_id);
        $paid = Payment::where('fee_id', $fee_id)->sum('amount');
        $due = ($fee->fee - $fee->discount) - $paid;

        return [
            'payment_method' => 'required',
            'amount' => 'required|numeric|max:'.$due,
            'donation_amount' => 'required_if:donate,1'
        ];
    }
}