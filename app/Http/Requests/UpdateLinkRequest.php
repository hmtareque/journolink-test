<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateLinkRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nav' => '',
            'name' => 'required',
            'type' => 'required',
            'post_link' => 'required_if:type,post',
            'internal_link' => 'required_if:type,internal',
            'external_link' => 'required_if:type,external|url',
            'nav_link' => 'required_if:type,nav|different:nav',
            'open_in_new_window' => ''
        ];
    }
}
