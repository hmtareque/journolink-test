<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Request;
use Illuminate\Foundation\Http\FormRequest;

class UpdateComponentRequest extends FormRequest
{
   /**
	 * Determine if the user is authorized to make this request.
	 *
	 * @return bool
	 */
	public function authorize() {
		return true;
	}

	/**
	 * Get the validation rules that apply to the request.
	 *
	 * @return array
	 */
	public function rules() {
		$request = Request::all();
		
		$property_names = array();
		
		if(isset($request['property'])){
		foreach ($request['property'] as $key => $property) {
			$property_names[$key] = $property['name'];
		}

                $rules['name'] = 'required';

		foreach ($request['property'] as $key => $property) {
			
			$names = $property_names;
			unset($names[$key]);
			
			$names_as_string = '';
			$i = 1;
			$total_names = count($names);
			foreach ($names as $name) {
				$names_as_string .= $name;
				$i++;

				if ($i <= $total_names) {
					$names_as_string .= ",";
				}
			}

			$rules['property.' . $key . '.name'] = 'required|not_in:'.$names_as_string;
			$rules['property.' . $key . '.type'] = 'required';
			$rules['property.' . $key . '.options'] = 'required_if:property.' . $key . '.type,select,checkbox,radio';
			//$rules['property.' . $key . '.required'] = 'required';
		}
		}
		
		
		return $rules;
	}

	public function messages() {
		$request = Request::all();
		$messages = array();
		
		if(isset($request['property'])){
		foreach ($request['property'] as $key => $property) {
			$messages['property.' . $key . '.name.required'] = 'The name field is required';
			$messages['property.' . $key . '.name.not_in'] = 'This name already exists.';
			$messages['property.' . $key . '.type.required'] = 'The type field is required';
			$messages['property.' . $key . '.options.required_if'] = 'The options field is required with field type ' . $property['type'];
			//$messages['property.' . $key . '.required'] = 'required';
		}
		}
		return $messages;
	}

}