<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreFeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client' => 'required',
            'service' => 'required',
            'fee_for_service' => 'required',
            'description' => 'required',
            'fee' => 'required|numeric',
            'discount' => 'nullable|numeric',
            'exempt_from_fee' => 'required',
            'exemption_reason' => 'required_if:exempt_from_fee,1'
        ];
    }
}