<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class StoreLinkRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'nav' => '',
            'name' => 'required',
            'type' => 'required',
            'post_link' => 'required_if:type,post',
            'internal_link' => 'required_if:type,internal',
           // 'external_link' => 'required_if:type,external|url',
           // 'nav_link' => 'required_if:type,nav|different:nav',
            'open_in_new_window' => ''
        ];
        
        
        if(Request::get('external_link') != ""){
            $rules['external_link'] = 'required_if:type,external|url';
        }
        
        if(Request::get('nav_link') != ""){
            $rules['nav_link'] = 'required_if:type,nav|different:nav';
        }
        
        return $rules;
    }
}
