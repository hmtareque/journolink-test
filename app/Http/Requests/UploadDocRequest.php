<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UploadDocRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'object_type' => 'required',
            'object_id' => 'required',
            'folder' => 'required',
            'name' => 'required',
            'files.*' => 'required|file',
            'private' => '',
            'return' => 'required'
        ];
    }
    
    
}
