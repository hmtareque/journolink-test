<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Request;
use App\Models\WebComponent;
use Illuminate\Foundation\Http\FormRequest;

class UpdateComponentItemRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $component_id = Request::segment(4);
        $component = WebComponent::find($component_id);
        $properties = json_decode($component->properties);
        
        
         $input = Request::all();
      //  print_r();
        
        //print_r($input);
        

        $rules['identifier'] = 'required|unique:web_component_items,identifier,' . Request::segment(6) . ',id,deleted_at,NULL';
        
        foreach ($properties as $property) {

            if ($property->visible == 1) {

                $validation = "";
                if ($property->required == 1) {
                    $validation = "required";
                }

                if ($property->type == "numeric") {
                    if ($validation != "") {
                        $validation .= "|numeric";
                    }

                    if ($validation == "") {
                        $validation = "numeric";
                    }
                }

                if ($property->type == "email") {
                    if ($validation != "") {
                        $validation .= "|email";
                    }

                    if ($validation == "") {
                        $validation = "email";
                    }
                }

                if ($property->type == "date") {
                    if ($validation != "") {
                        $validation .= "|date";
                    }

                    if ($validation == "") {
                        $validation = "date";
                    }
                }


                if ($property->type == "image") {

                    if ($input['item'][$property->id] == "") {
                        if ($validation != "" && !empty($input['item'][$property->id])) {
                            $validation .= "|image";
                        }

                        if ($validation == "" && !empty($input['item'][$property->id])) {
                            $validation = "image";
                        }
                    } else {
                        $validation = "image";
                    }
                }

                if ($property->type == "link") {
                    if ($validation != "") {
                        $validation .= "|url";
                    }

                    if ($validation == "") {
                        $validation = "url";
                    }
                }

                $rules['item.' . $property->id] = $validation;
            }
        }
        
      //  print_r($rules);

        return $rules;
    }
    
    public function messages() {
        
        $messages = array();
        
        $component_id = Request::segment(4);
        $component = WebComponent::find($component_id);
        $properties = json_decode($component->properties);

        foreach ($properties as $property) {
            $messages['item.'.$property->id.'.required'] = 'The '.$property->name.' field is required.';
            $messages['item.'.$property->id.'.numeric'] = 'The '.$property->name.' field must be a number.';
            $messages['item.'.$property->id.'.date'] = 'The '.$property->name.' is invalid date format.';
            $messages['item.'.$property->id.'.email'] = 'The '.$property->name.' field must be a valid email address.';
            $messages['item.'.$property->id.'.image'] = 'The '.$property->name.' field must be an image.';
            $messages['item.'.$property->id.'.url'] = 'The '.$property->name.' is invalid url format.';
           
        }

        return $messages;
        
    }

}
