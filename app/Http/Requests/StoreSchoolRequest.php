<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class StoreSchoolRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'type' => 'required',
            'name' => 'required|unique:schools,name',
            'primary_contact' => 'required_if:type,saturday,general',
            'address_line_1' => 'required_if:type,saturday,general',
            'address_line_2' => '',
            'street' => 'required_if:type,saturday,general',
            'city' => 'required_if:type,saturday,general',
            'postcode' => 'required_if:type,saturday,general',
            'borough' => 'required_if:type,saturday,general',
            'phone' => 'required_if:type,saturday,general',
            'mobile' => '',
            'email' => 'required_if:type,saturday,general', 
            'fax' => ''
        ];
    }
}
