<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateAcademicYearRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        
        $year_id = FormRequest::segment(5);
        
        return [
            'year' => 'required|unique:academic_years,year,'.$year_id.',id,deleted_at,NULL'
        ];
    }
}
