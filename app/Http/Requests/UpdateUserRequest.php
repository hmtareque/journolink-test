<?php

namespace App\Http\Requests;

use Illuminate\Support\Facades\Request;
use Illuminate\Foundation\Http\FormRequest;

class UpdateUserRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        return [
            'first_name' => 'required',
            'middle_name' => '',
            'last_name' => 'required',
            'ni_number' => 'required',
            'date_of_birth' => 'required|date_format:d/m/Y',
            'gender' => 'required',
            'other_gender' => 'required_if:gender,other',
            'profile_picture' => 'nullable|image',
            'address_line_1' => 'required',
            'address_line_2' => '',
            'street' => 'required',
            'city' => 'required',
            'postcode' => 'required',
            'phone' => 'required_without:mobile',
            'mobile' => 'required_without:phone',
            'email_address' => 'nullable|email',
            'immigration_status' => 'required',
            'other_immigration_status' => 'required_if:immigration_status,other',
            'passport_no' => 'required',
            'visa_expiry_date' => 'nullable|date_format:d/m/Y',
            'immigration_note' => '',
            'joined_at' => 'required|nullable|date_format:d/m/Y',
            'designation' => 'required',
            'has_paid_role' => 'required',
            'salary_type' => 'required_if:paid_role,1',
            'salary_amount' => 'required_if:paid_role,1',
            'salary_description' => '',
            'dbs_number' => 'required_if:teaching,1',
            'dbs_issue_date' => 'required_if:teaching,1|nullable|date_format:d/m/Y'
        ];
    }

}
