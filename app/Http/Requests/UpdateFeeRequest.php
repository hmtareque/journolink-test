<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

use App\Models\Fee;
use App\Models\Payment;

class UpdateFeeRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $fee_id = FormRequest::segment(7);
        
        $paid = Payment::where('fee_id', $fee_id)->sum('amount');
        
        $fee = FormRequest::get('fee');
        $discount = FormRequest::get('discount');
        $discount_allowed = (($fee - $discount) - $paid);
        $max_discount = ($discount_allowed>=0)? $discount : 0;

        $rules = [
            'fee_for_service' => 'required',
            'description' => 'required',
            'fee' => 'required|numeric|min:'.$paid,
            'discount' => 'nullable|numeric|max:'.$max_discount,
        ];
        
        if($paid <= 0){
            $rules['exempt_from_fee'] = 'required';
            $rules['exemption_reason'] = 'required_if:exempt_from_fee,1';
        }
        
        return $rules;
        
    }
}