<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

class StoreSettingRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $rules = [
            'parameter' => 'required|unique:web_settings,param',
            'type' => 'required',
            'setting_image' => 'required_if:type,image|image',
        ];

        if(Request::get('type') == "option"){
            $rules['setting_value'] = 'required|in:'.Request::get('setting_options');
            $rules['setting_options'] = 'required_if:type,option';
        } else {
            $rules['setting_value'] = 'required_unless:type,image';
        }
        
        return $rules;
        
    }
}
