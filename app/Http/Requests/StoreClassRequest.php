<?php

namespace App\Http\Requests;

use Illuminate\Http\Request;
use Illuminate\Foundation\Http\FormRequest;

use App\Models\StudentClass;

class StoreClassRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $academic_year = Request::get('academic_year');
        $school = Request::get('school');
        $course = Request::get('course');
        $group = Request::get('group');

        $rules = [
           'academic_year' => 'required',
           'school' => 'required',
           'course' => 'required',
           'group' => 'required',
           'exist' => ''
        ];
        
        $class_exist = StudentClass::where('academic_year_id', $academic_year)
                ->where('school_id', $school)
                ->where('course_id', $course)
                ->where('student_group_id', $group)
                ->where('status', '!=', 'stopped')
                ->count();
        
        if($class_exist>0){
            $rules['exist'] = 'required';
        }
        
        return $rules;

    }
    
    public function messages() {
        $messages['exist.required'] = 'This class already exist!';
        
        return $messages;
    } 
}
