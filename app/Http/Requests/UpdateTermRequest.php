<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateTermRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $year_id = FormRequest::segment(5);
        $term_id = FormRequest::segment(7);
        
        return [
            'term' => 'required|unique:school_terms,term,'.$term_id.',id,deleted_at,NULL,academic_year_id,'.$year_id
        ];
    }
}
