<?php

namespace App\Http\Requests;

use App\Models\WebSettings;
use App\Models\AppSettings;
use Illuminate\Support\Facades\Request;
use Illuminate\Foundation\Http\FormRequest;

class UpdateSettingsRequest extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        
        $settings_type = Request::segment(2);
        
        if ($settings_type === "web") {
            $settings = WebSettings::all();
        }elseif ($settings_type === "configs") {
            $settings = AppSettings::all();
        }

        $input = Request::all();

        if ($settings_type !== "") {
            $rules = array();
            if ($settings->count() > 0) {
                foreach ($settings as $setting) {

                    if ($setting->type === "text") {
                        $rules[$setting->param] = "required";
                    }

                    if ($setting->type === "option") {
                        $rules[$setting->param] = "required";
                    }

                    if ($setting->type === "image") {
                        if (isset($input[$setting->param]) && $input[$setting->param] == "") {
                            $rules[$setting->param] = "image";
                        }
                    }
                }
            }

            return $rules;
        }
    }

}
