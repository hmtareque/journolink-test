<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class UpdateStudentRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        
        return [
            'first_name' => 'required',
            'middle_name' => '',
            'last_name' => 'required',
            'date_of_birth' => 'required|uk_date',
            'gender' => 'required',
            'other_gender' => 'required_if:gender,other',
            'marital_status' => '',
            'ni_number' => '',
            'address_line_1' => 'required',
            'address_line_2' => '',
            'street' => 'required',
            'city' => 'required',
            'postcode' => 'required',
            'phone' => '',
            'mobile' => '',
            'email' => 'nullable|email',
            'next_of_kin' => 'required',
            'next_of_kin_relationship' => 'required',
            'next_of_kin_address_same_as_main' => 'required',
            'next_of_kin_address_line_1' => 'required_if:next_of_kin_address_same_as_main,0',
            'next_of_kin_address_line_2' => '',
            'next_of_kin_street' => 'required_if:next_of_kin_address_same_as_main,0',
            'next_of_kin_city' => 'required_if:next_of_kin_address_same_as_main,0',
            'next_of_kin_postcode' => 'required_if:next_of_kin_address_same_as_main,0',
            'next_of_kin_phone' => 'required_without:next_of_kin_mobile',
            'next_of_kin_mobile' => 'required_without:next_of_kin_phone',
            'next_of_kin_email' => 'nullable|email',
            'born_in_the_uk' => 'required',
            'arrived_in_the_uk' => 'required_if:born_in_the_uk,0|nullable|uk_date',
            'immigration_status' => 'required',
            'other_immigration_status' => 'required_if:immigration_status,other',
            'passport_no' => '',
            'visa_expiry_date' => 'nullable|uk_date',
            'homeoffice_ref_no' => '',
            'has_dependents' => '',
            'no_of_dependents' => '',
            'dependents_details' => '',
            'solicitor_name' => '',
            'solicitor_contact' => '',
            'immigration_note' => '',
            'first_language' => 'required',
            'other_first_language' => 'required_if:first_language,other',
            'level_of_english' => '',
            'interpreter_required' => '',
            'interpreter_language' => '',
            'education_institute' => '',
            'supervisor_name' => '',
            'supervisor_contact' => '',
            'education_note' => '',
            'gp_name' => '',
            'gp_contact' => '',
            'has_medical_condition' => 'required',
            'medical_conditions' => 'required_if:has_medical_condition,1',
            'has_allergy' => 'required',
            'allergies' => 'required_if:has_allergy,1',
            'has_disability' => 'required',
            'disabilities' => 'required_if:has_disability,1',
            'ethnicity' => 'required',
            'religion' => 'required',
            'other_religion' => 'required_if:religion,1',
            'country_of_origin' => 'required',
            'local_authority' => 'required',
            'local_authority_note' => '',
            'social_worker_name' => '',
            'social_worker_contact' => '',
            'need_additional_support' => 'required',
            'additional_support' => 'required_if:need_additional_support,1',
            'has_consent_to_use_info' => '',
            'has_consent_to_contacted_for_activities' => '',
            'has_consent_to_appear_in_publicity_photos' => '',
            'has_consent_to_appear_in_publicity_videos' => '',
            'subscribe_newsletter' => '',
            'heard_about_us_by' => 'required',
            'other_heard_about_us_by' => 'required_if:heard_about_us_by,other',
            'check_duplicate' => 'required_if:has_duplicate,1'
        ];
    }

}

