<?php

namespace App\Http\Requests;

use App\Models\ClassAttendance;
use Illuminate\Foundation\Http\FormRequest;

class StoreClassAttendance extends FormRequest {

    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize() {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules() {
        $class_id = FormRequest::segment(4);
        $date = FormRequest::get('date');
        $exist = '';
        if (trim($date) != "") {
            $attendance = ClassAttendance::where('class_id', $class_id)
                    ->where('date', date('Y-m-d', strtotime(php_date($date))))
                    ->count();
            if ($attendance > 0) {
                $exist = 'required';
            }
        }

        return [
            'date' => 'required|uk_date',
            'teachers' => 'required|array',
            'students.*.id' => 'required',
            'students.*.reason_for_absence' => 'required_without:students.*.attended',
            'exist' => $exist
        ];
    }

    public function messages() {
        $messages['exist.required'] = 'Already recorded the attendance for the class!';
        return $messages;
    }

}
