<?php



namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\StudentGroup;

class StudentGroupComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $groups;

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(StudentGroup $groups)
    {
        // Dependencies automatically resolved by service container...
        $this->group = $groups;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $groups = $this->group->all();
        $view->with('groups', $groups);
    }
}