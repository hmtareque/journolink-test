<?php



namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\Nav;

class NavComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $nav;

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(Nav $nav)
    {
        // Dependencies automatically resolved by service container...
        $this->nav = $nav;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $navs = Nav::orderBy('name')->get(array('id', 'name'));
        $view->with('navs', $navs);
    }
}