<?php



namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\Borough;

class BoroughComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $boroughes = Borough::orderBy('name', 'asc')->get(array('id', 'name'));
        $view->with('boroughes', $boroughes);
    }
}