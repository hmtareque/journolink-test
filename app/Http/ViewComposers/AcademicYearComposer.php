<?php



namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\AcademicYear;

class AcademicYearComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $academic_year;

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(AcademicYear $academic_year)
    {
        // Dependencies automatically resolved by service container...
        $this->$academic_year = $academic_year;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $academic_years = AcademicYear::where('completed', 0)
                ->orderBy('year', 'asc')
                ->get(array('id', 'year'));
        
        $view->with('academic_years', $academic_years);
    }
}