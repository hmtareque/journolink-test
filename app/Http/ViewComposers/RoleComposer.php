<?php



namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\Role;
use App\Models\Module;

class RoleComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $roles;
    protected $modules;

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(Role $roles, Module $modules)
    {
        // Dependencies automatically resolved by service container...
        $this->roles = $roles;
        $this->modules = $modules;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $view->with('roles', $this->roles->all());
        $view->with('modules', $this->modules->all());
    }
}