<?php



namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\School;

class SchoolComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $school;

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(School $school)
    {
        // Dependencies automatically resolved by service container...
        $this->school = $school;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $schools = $this->school->where('type', '!=', 'referral')
                ->where('active', 1)
                ->orderBy('name')
                ->get(array('id', 'type', 'name'));
        
        $referral_schools = $this->school->where('type', 'referral')
                ->where('active', 1)
                ->orderBy('name')
                ->get(array('id', 'type', 'name'));
        
        $view->with('schools', $schools);
        $view->with('referral_schools', $referral_schools);
    }
}