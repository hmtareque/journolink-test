<?php



namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\Course;

class CourseComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */
    protected $course;

    /**
     * Create a new profile composer.
     *
     * @param  UserRepository  $users
     * @return void
     */
    public function __construct(Course $course)
    {
        // Dependencies automatically resolved by service container...
        $this->course = $course;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $courses = Course::where('active', 1)->orderBy('title', 'asc')->get(array('id', 'title'));
        $view->with('courses', $courses);
    }
}