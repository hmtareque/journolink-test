<?php



namespace App\Http\ViewComposers;

use Illuminate\View\View;

use App\Models\PostCategory;
use App\Models\Post;

class PostComposer
{
    /**
     * The user repository implementation.
     *
     * @var UserRepository
     */

    protected $post_category;
    protected $post;

    /**
     * Create a new profile composer.
     *
     * @param  PostCategory $post_category
     * @return void
     */
    public function __construct(PostCategory $post_category, Post $post)
    {
        // Dependencies automatically resolved by service container...
        $this->post_category = $post_category;
        $this->post = $post;
    }

    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $categories = $this->post_category->where('visible', 1)->orderBy('name')->get(array('id', 'name', 'route'));
        $posts = $this->post->where('published', 1)->orderBy('title')->get(array('id', 'title', 'route'));
        $view->with('categories', $categories);
        $view->with('posts', $posts);
    }
}