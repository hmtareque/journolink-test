<?php



namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\ImmigrationStatus;

class ImmigrationStatusComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $statuses = ImmigrationStatus::orderBy('sort')->get(array('id', 'name'));
        $view->with('immigration_statuses', $statuses);
    }
}