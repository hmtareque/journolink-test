<?php



namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Modules\AuthModule;

class AccessComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $user_service_access = AuthModule::access(auth()->id());
        $view->with('user_service_access', $user_service_access);
    }
}