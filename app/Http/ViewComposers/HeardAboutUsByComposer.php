<?php



namespace App\Http\ViewComposers;

use Illuminate\View\View;
use App\Models\HeardAboutUsBy;

class HeardAboutUsByComposer
{
    /**
     * Bind data to the view.
     *
     * @param  View  $view
     * @return void
     */
    public function compose(View $view)
    {
        $sources = HeardAboutUsBy::orderBy('name')->get(array('id', 'name'));
        $view->with('heard_about_us_by', $sources);
    }
}