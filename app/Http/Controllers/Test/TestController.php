<?php

namespace App\Http\Controllers\Test;

use App\Http\Requests\TestRequest;
use App\Http\Controllers\Controller;

class TestController extends Controller
{
    
    public function index(){
        return view('app.blank');
    }
    
    public function table(){
       // sleep(10);
        return view('app.table');
    }
    
    
    public function getForm(){
        //sleep(15);
       return view('app.form');
    }
    
    public function postForm(testRequest $request){
        echo "<pre>";
        print_r($request->all());
        echo "</pre>";
    }

    
}
