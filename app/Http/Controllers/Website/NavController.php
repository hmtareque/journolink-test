<?php

namespace App\Http\Controllers\Website;

use App\Http\Requests\StoreNavRequest;
use App\Http\Requests\UpdateNavRequest;
use App\Http\Controllers\Controller;
use App\Modules\WebsiteModule;

class NavController extends Controller {

    private $name;
    private $module;
    private $website;

    public function __construct(WebsiteModule $website) {
        $this->module = 4;
        $this->name = trans('label.nav');
        $this->website = $website;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $this->authorize('read', $this->module);
        $navs = $this->website->navs();
        return view('web.navs.index', array('navs' => $navs));
    }

    public function links($nav_id) {
        $this->authorize('read', $this->module);
        $nav = $this->website->nav($nav_id);
        $links = $this->website->navLinks($nav_id);
        return view('web.navs.links.index', array('nav' => $nav, 'links' => $links));
    }

    public function createLink($nav_id) {
        $this->authorize('create', $this->module);
        $nav = $this->website->nav($nav_id);
        return view('web.navs.links.create', array('nav' => $nav));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $this->authorize('create', $this->module);
        return view('web.navs.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreNavRequest $request) {
        $this->authorize('create', $this->module);
        $post = $this->website->storeNav($request->all());
        if ($post) {
            // success event
            return redirect('manage/web/navs')->with('success', trans('alert.create_success', array('name' => $post->title)));
        }
        // failed event 
        return redirect('manage/web/navs/create')->with('warning', trans('alert.create_failed', array('name' => $this->name)));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $this->authorize('update', $this->module);
        $nav = $this->website->nav($id);
        return view('web.navs.edit', array('nav' => $nav));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateNavRequest $request, $id) {
        $this->authorize('update', $this->module);
        $nav = $this->website->updateNav($request->all(), $id);

        if ($nav) {
            // success event
            return redirect('manage/web/navs/' . $id . '/edit')->with('success', trans('alert.update_success', array('name' => $nav->name)));
        }
        // failed event 
        return redirect('manage/navs/' . $id . '/edit')->with('warning', trans('alert.update_failed', array('name' => $this->name)));
    }

    public function makeVisible($id) {
        $this->authorize('update', $this->module);
        $show = $this->website->makeNavVisible($id);
        if ($show) {
            // success event
            return redirect('manage/web/navs')->with('success', trans('alert.make_visible_success', array('name' => $this->name)));
        }
        // failed event 
        return redirect('manage/web/navs')->with('warning', trans('alert.make_visible_failed', array('name' => $this->name)));
    }

    public function hide($id) {
        $this->authorize('update', $this->module);
        $hide = $this->website->hideNav($id);
        if ($hide) {
            // success event
            return redirect('manage/web/navs')->with('success', trans('alert.hide_success', array('name' => $this->name)));
        }
        // failed event 
        return redirect('manage/web/navs')->with('warning', trans('alert.hide_failed', array('name' => $this->name)));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $this->authorize('delete', $this->module);
        $delete = $this->website->deleteNav($id);
        if ($delete) {
            // success event
            return redirect('manage/web/navs')->with('success', trans('alert.delete_success', array('name' => $this->name)));
        }
        // failed event 
        return redirect('manage/web/navs')->with('warning', trans('alert.delete_failed', array('name' => $this->name)));
    }

}
