<?php

namespace App\Http\Controllers\Website;

use App\Http\Requests\StorePostCategoryRequest;
use App\Http\Requests\UpdatePostCategoryRequest;
use App\Http\Controllers\Controller;
use App\Modules\WebsiteModule;

class PostCategoryController extends Controller {

    private $name;
    private $module;
    private $website;

    public function __construct(WebsiteModule $website) {
        $this->module = 4;
        $this->name = trans('label.post_category');
        $this->website = $website;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $this->authorize('read', $this->module);
        $categories = $this->website->postCategories();
        return view('web.posts.categories.index', array('categories' => $categories));
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function posts($category_id) {
        $this->authorize('read', $this->module);
        $category = $this->website->postCategory($category_id);
        $posts = $this->website->postsOfCategory($category_id);
        return view('web.posts.index', array('category' => $category, 'posts' => $posts));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $this->authorize('create', $this->module);
        return view('web.posts.categories.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePostCategoryRequest $request) {
        $this->authorize('create', $this->module);
        $category = $this->website->storePostCategory($request->all());
        if ($category) {
            // success event
            return redirect('manage/web/post-categories')->with('success', trans('alert.create_success', array('name' => $category->name)));
        }
        // failed event 
        return redirect('manage/web/post-categories/create')->with('warning', trans('alert.create_failed', array('name' => $this->name)));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $this->authorize('update', $this->module);
        $category = $this->website->postCategory($id);
        return view('web.posts.categories.edit', array('category' => $category));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePostCategoryRequest $request, $id) {
        $this->authorize('update', $this->module);
        $category = $this->website->updatePostCategory($request->all(), $id);

        if ($category) {
            // success event
            return redirect('manage/web/post-categories/' . $id . '/edit')->with('success', trans('alert.update_success', array('name' => $category->name)));
        }
        // failed event 
        return redirect('manage/web/post-categories/' . $id . '/edit')->with('warning', trans('alert.update_failed', array('name' => $this->name)));
    }

    public function makeVisible($id) {
        $this->authorize('update', $this->module);
        $show = $this->website->makeVisiblePostCategory($id);

        if ($show) {
            // success event
            return redirect('manage/web/post-categories')->with('success', trans('alert.make_visible_success', array('name' => $show->name)));
        }
        // failed event 
        return redirect('manage/web/post-categories')->with('warning', trans('alert.make_visible_failed', array('name' => $this->name)));
    }

    public function hide($id) {
        $this->authorize('update', $this->module);
        $hide = $this->website->hidePostCategory($id);
        if ($hide) {
            // success event
            return redirect('manage/web/post-categories')->with('success', trans('alert.hide_success', array('name' => $hide->name)));
        }
        // failed event 
        return redirect('manage/web/post-categories')->with('warning', trans('alert.hide_failed', array('name' => $this->name)));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $this->authorize('delete', $this->module);
        $delete = $this->website->deletePostCategory($id);

        if ($delete) {
            // success event
            return redirect('manage/web/post-categories')->with('success', trans('alert.delete_success', array('name' => $this->name)));
        }
        // failed event 
        return redirect('manage/web/post-categories')->with('warning', trans('alert.delete_failed', array('name' => $this->name)));
    }

}
