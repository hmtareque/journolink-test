<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreComponentRequest;
use App\Http\Requests\UpdateComponentRequest;
use App\Modules\WebsiteModule;

class ComponentController extends Controller {

    private $name;
    private $module;
    private $website;

    public function __construct(WebsiteModule $website) {
        $this->module = 4;
        $this->name = trans('label.component');
        $this->website = $website;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $this->authorize('read', $this->module);
        $components = $this->website->allComponents();
        return view('web.components.index', array('components' => $components));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $this->authorize('create', $this->module);
        return view('web.components.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreComponentRequest $request) {
        $this->authorize('create', $this->module);
        $component = $this->website->storeComponent($request->all());

        if ($component) {
            // success event
            return redirect('manage/web/components')->with('success', trans('alert.create_success', array('name' => $component->name)));
        }
        // failed event 
        return redirect('manage/web/components/create')->with('warning', trans('alert.create_failed', array('name' => $this->name)));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $this->authorize('update', $this->module);
        $component = $this->website->component($id);
        return view('web.components.edit', array('component' => $component));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateComponentRequest $request, $id) {
        $this->authorize('update', $this->module);
        $component = $this->website->updateComponent($request->all(), $id);

        if ($component) {
            // success event
            return redirect('manage/web/components/' . $id . '/edit')->with('success', trans('alert.update_success', array('name' => $component->name)));
        }
        // failed event 
        return redirect('manage/web/components/' . $id . '/edit')->with('warning', trans('alert.update_failed', array('name' => $this->name)));
    }

    public function makeVisible($id) {
        $this->authorize('update', $this->module);
        $publish = $this->website->makeComponentVisible($id);
        if ($publish) {
            // success event
            return redirect('manage/web/components')->with('success', trans('alert.make_visible_success', array('name' => $this->name)));
        }
        // failed event 
        return redirect('manage/web/components')->with('warning', trans('alert.make_visible_failed', array('name' => $this->name)));
    }

    public function hide($id) {
        $this->authorize('update', $this->module);
        $unpublish = $this->website->hideComponent($id);

        if ($unpublish) {
            // success event
            return redirect('manage/web/components')->with('success', trans('alert.hide_success', array('name' => $this->name)));
        }
        // failed event 
        return redirect('manage/web/components')->with('warning', trans('alert.hide_failed', array('name' => $this->name)));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $this->authorize('delete', $this->module);
        $delete = $this->website->deleteComponent($id);
        if ($delete) {
            // success event
            return redirect('manage/web/components')->with('success', trans('alert.delete_success', array('name' => $this->name)));
        }
        // failed event 
        return redirect('manage/web/components')->with('warning', trans('alert.delete_failed', array('name' => $this->name)));
    }

}
