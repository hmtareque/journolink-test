<?php

namespace App\Http\Controllers\Website;

use Illuminate\Support\Facades\Gate;
use Illuminate\Http\Request;
use App\Http\Requests\StoreLinkRequest;
use App\Http\Requests\UpdateLinkRequest;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use App\Modules\WebsiteModule;

class LinkController extends Controller {

    private $name;
    private $module;
    private $website;

    public function __construct(WebsiteModule $website) {
        $this->module = 4;
        $this->name = trans('label.link');
        $this->website = $website;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $this->authorize('read', $this->module);
        $links = $this->website->links();
        return view('web.navs.links.index', array('links' => $links));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $this->authorize('create', $this->module);
        return view('web.navs.links.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreLinkRequest $request) {
        $this->authorize('create', $this->module);
        $nav_id = $request->get('nav');
        $link = $this->website->storeLink($request->all());

        if ($link) {
            // success event
            return redirect('manage/web/navs/'.$nav_id.'/links')->with('success', trans('alert.create_success', array('name' => $link->name)));
        }
        // failed event 
        return redirect('manage/web/navs/'.$nav_id.'/links/create')->with('warning', trans('alert.create_failed', array('name' => $this->name)));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        $this->authorize('read', $this->module);
        $links = $this->website->linksOfNav($id);
        return view('web.navs.links.index', array('links' => $links));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $this->authorize('update', $this->module);
        $link = $this->website->link($id);
        $nav = $this->website->nav($link->nav_id);
        return view('web.navs.links.edit', array('nav' => $nav, 'link' => $link));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateLinkRequest $request, $id) {
        $this->authorize('update', $this->module);
        $link = $this->website->updateLink($request->all(), $id);

        if ($link) {
            // success event
            return redirect('manage/web/links/' . $id . '/edit')->with('success', trans('alert.update_success', array('name' => $link->name)));
        }
        // failed event 
        return redirect('manage/web/links/' . $id . '/edit')->with('warning', trans('alert.update_failed', array('name' => $this->name)));
    }

    public function makeVisible($id) {
        $this->authorize('update', $this->module);
        $link = $this->website->link($id);
        $show = $this->website->makeLinkVisible($id);
        if ($show) {
            // success event
            return redirect('manage/web/navs/'.$link->nav_id.'/links')->with('success', trans('alert.make_visible_success', array('name' => $show->name)));
        }
        // failed event 
        return redirect('manage/web/navs/'.$link->nav_id.'/links')->with('warning', trans('alert.make_visible_failed', array('name' => $show->name)));
    }

    public function hide($id) {
        $this->authorize('update', $this->module);
        $link = $this->website->link($id);
        $hide = $this->website->hideLink($id);
        if ($hide) {
            // success event
            return redirect('manage/web/navs/'.$hide->nav_id.'/links')->with('success', trans('alert.hide_success', array('name' => $hide->name)));
        }
        // failed event 
        return redirect('manage/web/navs/'.$link->nav_id.'/links')->with('warning', trans('alert.hide_failed', array('name' => $hide->name)));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $this->authorize('delete', $this->module);
        $link = $this->website->link($id);
        $delete = $this->website->deleteLink($id);
        if ($delete) {
            // success event
            return redirect('manage/web/navs/'.$link->nav_id.'/links')->with('success', trans('alert.delete_success', array('name' => $this->name)));
        }
        // failed event 
        return redirect('manage/web/navs/'.$link->nav_id.'/links')->with('warning', trans('alert.delete_failed', array('name' => $this->name)));
    }

}
