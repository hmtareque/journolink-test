<?php

namespace App\Http\Controllers\Website;

use App\Http\Requests\StorePostRequest;
use App\Http\Requests\UpdatePostRequest;
use App\Http\Controllers\Controller;
use App\Modules\WebsiteModule;

class PostController extends Controller {

    private $name;
    private $module;
    private $website;

    public function __construct(WebsiteModule $website) {
        $this->middleware('auth');
        $this->name = trans('label.post');
        $this->website = $website;
        $this->module = 4;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $this->authorize('read', $this->module);
        $posts = $this->website->posts();
        return view('web.posts.index', array('posts' => $posts));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $this->authorize('create', $this->module);
        return view('web.posts.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePostRequest $request) {
        $this->authorize('create', $this->module);
        $post = $this->website->storePost($request->all());
        if ($post) {
            // success event
            return redirect('manage/web/posts')->with('success', trans('alert.create_success', array('name' => $post->title)));
        }
        // failed event 
        return redirect('manage/web/posts/create')->with('warning', trans('alert.create_failed', array('name' => $this->name)));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //not in use yet
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id) {
        $this->authorize('update', $this->module);
        $post = $this->website->post($id);
        return view('web.posts.edit', array('post' => $post));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePostRequest $request, $id) {
        $this->authorize('update', $this->module);
        $post = $this->website->updatePost($request->all(), $id);
        if ($post) {
            // success event
            return redirect('manage/web/posts/' . $id . '/edit')->with('success', trans('alert.update_success', array('name' => $post->title)));
        }
        // failed event 
        return redirect('manage/web/posts/' . $id . '/edit')->with('warning', trans('alert.update_failed', array('name' => $this->name)));
    }

    public function publish($id) {
        $this->authorize('update', $this->module);
        $publish = $this->website->publishPost($id);
        if ($publish) {
            // success event
            return redirect('manage/web/posts')->with('success', trans('alert.publish_success', array('name' => $this->name)));
        }
        // failed event 
        return redirect('manage/web/posts')->with('warning', trans('alert.publish_failed', array('name' => $this->name)));
    }

    public function unpublish($id) {
        $this->authorize('update', $this->module);
        $unpublish = $this->website->unpublishPost($id);
        if ($unpublish) {
            // success event
            return redirect('manage/web/posts')->with('success', trans('alert.unpublish_success', array('name' => $this->name)));
        }
        // failed event 
        return redirect('manage/web/posts')->with('warning', trans('alert.unpublish_failed', array('name' => $this->name)));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $this->authorize('delete', $this->module);
        $delete = $this->website->deletePost($id);
        if ($delete) {
            // success event
            return redirect('manage/web/posts')->with('success', trans('alert.delete_success', array('name' => $this->name)));
        }
        // failed event 
        return redirect('manage/web/posts')->with('warning', trans('alert.delete_failed', array('name' => $this->name)));
    }

}
