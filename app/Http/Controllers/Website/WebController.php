<?php

namespace App\Http\Controllers\Website;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\WebsiteModule;

class WebController extends Controller
{
    private $website;
    
    public function __construct(WebsiteModule $website) {

        $this->website = $website;

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('web');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function post($slug)
    {
        $post = $this->website->postBySlug($slug);
        
        if($post){
            return view('web.public.post', array('post' => $post));
        }
        
        
        
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function news()
    {
        return view('web.public.news');
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function getContact()
    {
        return view('web.public.contact');
    }
    
    public function postContact(Request $request)
    {
        return view('web.public.contact');
    }

}
