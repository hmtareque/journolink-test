<?php

namespace App\Http\Controllers\Website;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreComponentItemRequest;
use App\Http\Requests\UpdateComponentItemRequest;
use App\Modules\WebsiteModule;

class ComponentItemController extends Controller {

    private $name;
    private $module;
    private $website;

    public function __construct(WebsiteModule $website) {
        $this->module = 4;
        $this->name = trans('label.component_item');
        $this->website = $website;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($component_id) {
        $this->authorize('read', $this->module);
        $component = $this->website->component($component_id);
        $items = $this->website->componentItems($component_id);
        return view('web.components.items.index', array('component' => $component, 'items' => $items));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($component_id) {
        $this->authorize('create', $this->module);
        $component = $this->website->component($component_id);
        return view('web.components.items.create', array('component' => $component));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreComponentItemRequest $request, $component_id) {
        $this->authorize('create', $this->module);

        $component = $this->website->storeComponentItem($request->all(), $component_id);

        if ($component) {
            // success event
            return redirect('manage/web/components/' . $component_id . '/items')->with('success', trans('alert.create_success', array('name' => $this->name)));
        }
        // failed event 
        return redirect('manage/web/components/' . $component_id . '/create')->with('warning', trans('alert.create_failed', array('name' => $this->name)));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id) {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($component_id, $item_id) {
        $this->authorize('update', $this->module);
        $item = $this->website->componentItem($item_id);
        $component = $this->website->component($component_id);
        return view('web.components.items.edit', array('component' => $component, 'item' => $item));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateComponentItemRequest $request, $component_id, $item_id) {
        $this->authorize('update', $this->module);
        $component = $this->website->updateComponentItem($request->all(), $item_id);

        if ($component) {
            // success event
            return redirect('manage/web/components/' . $component_id . '/items/' . $item_id . '/edit')->with('success', trans('alert.update_success', array('name' => $this->name)));
        }
        // failed event 
        return redirect('manage/web/components/' . $component_id . '/items/' . $item_id . '/edit')->with('warning', trans('alert.update_failed', array('name' => $this->name)));
    }

    public function makeVisible($component_id, $id) {
        $this->authorize('update', $this->module);
        $show = $this->website->makeComponentItemVisible($id);

        if ($show) {
            // success event
            return redirect('manage/web/components/' . $component_id . '/items')->with('success', trans('alert.make_visible_success', array('name' => $this->name)));
        }
        // failed event 
        return redirect('manage/web/components/' . $component_id . '/items')->with('warning', trans('alert.make_visible_failed', array('name' => $this->name)));
    }

    public function hide($component_id, $id) {
        $this->authorize('update', $this->module);
        $hide = $this->website->hideComponentItem($id);

        if ($hide) {
            // success event
            return redirect('manage/web/components/' . $component_id . '/items')->with('success', trans('alert.hide_success', array('name' => $this->name)));
        }
        // failed event 
        return redirect('manage/web/components/' . $component_id . '/items')->with('warning', trans('alert.hide_failed', array('name' => $this->name)));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($component_id, $id) {
        $this->authorize('delete', $this->module);
        $hide = $this->website->deleteComponentItem($id);

        if ($hide) {
            // success event
            return redirect('manage/web/components/' . $component_id . '/items')->with('success', trans('alert.delete_success', array('name' => $this->name)));
        }
        // failed event 
        return redirect('manage/web/components/' . $component_id . '/items')->with('warning', trans('alert.delete_failed', array('name' => $this->name)));
    }

}
