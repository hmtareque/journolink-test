<?php

namespace App\Http\Controllers\Website;

use App\Http\Requests\UpdateWebContactRequest;
use App\Http\Controllers\Controller;
use App\Modules\WebsiteModule;

class ContactController extends Controller
{

    private $name;
    private $module;
    private $website;
    
    public function __construct(WebsiteModule $website) {
        $this->module = 4;
        $this->name = trans('label.contacts');
        $this->website = $website;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
  
        $this->authorize('read', $this->module);
        $contacts = $this->website->contacts();
        $web_contacts = $this->website->webContacts();
        return view('web.contacts.index', array('contacts' => $contacts, 'web_contacts' => $web_contacts));
        
    }

    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $this->authorize('update', $this->module);
         $contacts = $this->website->contacts();
        return view('web.contacts.edit', array('contacts' => $contacts));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateWebContactRequest $request) {
            $this->authorize('update', $this->module);
            $contact = $this->website->updateContacts($request->all());
            if ($contact) {
                // success event
                return redirect('manage/web/contacts/update')->with('success', trans('alert.update_success', array('name' => $this->name)));
            }
            // failed event 
            return redirect('manage/web/contacts/update')->with('warning', trans('alert.update_failed', array('name' => $this->name)));
    }
    
  
}
