<?php

namespace App\Http\Controllers\Website;

use Illuminate\Support\Facades\Cache;

use App\Http\Requests\StoreSettingRequest;
use App\Http\Requests\UpdateSettingsRequest;
use App\Http\Controllers\Controller;
use App\Modules\WebsiteModule;

class WebSettingController extends Controller {

    private $name;
    private $module;
    private $website;

    public function __construct(WebsiteModule $website) {
        $this->middleware('auth');
        $this->website = $website;
        $this->module = 3;
        $this->name = trans('label.web_settings');
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $this->authorize('read', $this->module);
        $settings = $this->website->settings();
        return view('web.settings.index', array('settings' => $settings));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        $this->authorize('create', $this->module);
        return view('web.settings.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSettingRequest $request) {
        $this->authorize('create', $this->module);
        $store = $this->website->storeSetting($request->all());
        if ($store) {
             Cache::flush();
            return redirect('manage/web/settings')->with('success', trans('alert.create_success', array('name' => $this->name)));
        }
        return redirect('manage/web/settings')->with('failed', trans('alert.create_failed', array('name' => $this->name)));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function edit() {
        $this->authorize('update', $this->module);
        $settings = $this->website->settings();
        return view('web.settings.edit', array('settings' => $settings));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSettingsRequest $request) {
        $this->authorize('update', $this->module);
        $update = $this->website->updateSettings($request->all());
        if ($update) {
            Cache::flush();
            return redirect('manage/web/settings/edit')->with('success', trans('alert.update_success', array('name' => $this->name)));
        }
        return redirect('manage/web/settings/edit')->with('failed', trans('alert.update_failed', array('name' => $this->name)));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id) {
        $this->authorize('delete', $this->module);
        $delete = $this->website->deleteSetting($id);
        if ($delete) {
            return redirect('manage/web/settings')->with('success', trans('alert.delete_success', array('name' => $this->name)));
        }
        return redirect('manage/web/settings')->with('failed', trans('alert.delete_failed', array('name' => $this->name)));
    }

}
