<?php

namespace App\Http\Controllers\Ajax;

use App\Http\Controllers\Controller;

use App\Modules\SchoolModule;

class EducationController extends Controller
{
    public function __construct(SchoolModule $school) {
        $this->school = $school;
    }
    
    public function getSchoolClasses($year_id, $school_id){
        
        $classes = $this->school->activeSchoolClasses($year_id, $school_id);
        
        return $classes->toJson();
    }
    
    public function getYearTerms($year_id){
        
        $classes = $this->school->yearTerms($year_id);
        
        return $classes->toJson();
    }
    
}
