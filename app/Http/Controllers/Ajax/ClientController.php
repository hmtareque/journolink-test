<?php

namespace App\Http\Controllers\Ajax;

use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Modules\ClientModule;

class ClientController extends Controller {

    public function __construct(ClientModule $client) {
        $this->client = $client;
    }

    public function checkDuplicate(Request $request) {

        $clients = $this->client->checkDuplicate($request);

        if ($clients->count() > 0) {
            return view('app.ajax.duplicate-clients', array('clients' => $clients));
        }
    }
    
    public function verify($client_id) {

        
        $postcode = $this->client->verify($client_id);

        if ($postcode) {
            Session::put('checked', $client_id);
            return json_encode(array('status' => 'success', 'postcode' => $postcode));
        }
        
        return json_encode(array('status' => 'failed'));
    }

}
