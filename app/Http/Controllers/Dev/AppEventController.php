<?php

namespace App\Http\Controllers\Dev;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Modules\AppModule;

class AppEventController extends Controller
{
    private $name; 
    
    public function __construct(AppModule $app)
    {
        $this->app = $app;
        $this->middleware('auth');
        $this->module = 1;
        $this->name = trans('label.event');
    }
    
    /**
     * Display todays events of the App
     */
    public function recent(){

        $events = $this->app->recentEvents();
        return view('app.dev.events.index', array('events' => $events));
    }


}
