<?php

namespace App\Http\Controllers\Money;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\StoreFeeRequest;
use App\Http\Requests\UpdateFeeRequest;

use App\Modules\ClientModule;
use App\Modules\MoneyModule;

class FeeController extends Controller
{
    public function __construct(ClientModule $client, MoneyModule $money) {
        $this->client = $client;
        $this->money = $money;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($client_id, $service)
    {
         $client = $this->client->info($client_id);
         $fees = $this->money->fees($client_id, $service);
         return view('app.money.fees.index', array('client' => $client, 'service' => $service, 'fees' => $fees));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($client_id, $service)
    {
        $client = $this->client->info($client_id);
        return view('app.money.fees.create', array('client' => $client, 'service' => $service));
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreFeeRequest $request, $client_id, $service)
    {
        //echo '<pre>';
        //print_r($request->all());
        //echo '</pre>';
        
        $store = $this->money->storeFee($request);
        
         if($store){
            return redirect('manage/clients/'.$client_id.'/services/'.$service.'/fees')->with('success', 'Fee successfully created.');
        }
        
       return redirect('manage/clients/'.$client_id.'/services/'.$service.'/fees/create')->with('warning', 'Failed to create new fee! Please try again.');
   
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($client_id, $service, $fee_id)
    {
         $client = $this->client->info($client_id);
         $fee = $this->money->fee($fee_id);
         return view('app.money.fees.show', array('client' => $client, 'service' => $service, 'fee' => $fee));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($client_id, $service, $fee_id)
    {
        $client = $this->client->info($client_id);
        $fee = $this->money->fee($fee_id);
        return view('app.money.fees.edit', array('client' => $client, 'service' => $service, 'fee' => $fee));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateFeeRequest $request, $client_id, $service, $fee_id)
    {
          echo '<pre>';
        print_r($request->all());
        echo '</pre>';
        
       // exit;
        
        $update = $this->money->updateFee($fee_id, $request);
        
         if($update){
            return redirect('manage/clients/'.$client_id.'/services/'.$service.'/fees')->with('success', 'Payment successfully recorded.');
        }
        
        return redirect('manage/clients/'.$client_id.'/services/'.$service.'/fees/'.$fee_id.'/edit')->with('warning', 'fatPayment successfully recorded.');
   
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($client_id, $service, $fee_id)
    {
        $fee = $this->money->fee($fee_id);
        
        $removed = $this->money->removeFee($fee_id);

        if ($removed) {
            return redirect('manage/clients/' . $client_id . '/services/' . $service . '/fees')->with('success', 'Payment successfully recorded.');
        }

        return redirect('manage/clients/' . $client_id . '/services/' . $service . '/fees/' . $fee_id)->with('warning', 'fatPayment successfully recorded.');
    
    }
}