<?php

namespace App\Http\Controllers\Money;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\CreateSubscriptionRequest;
use App\Http\Requests\RemoveSubscriptionRequest;

use App\Modules\ClientModule;
use App\Modules\MoneyModule;
use App\Modules\StripeModule;

class SubscriptionController extends Controller
{
    public function __construct(ClientModule $client, MoneyModule $money, StripeModule $stripe) {
        $this->client = $client;
        $this->money = $money;
        $this->stripe = $stripe;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($client_id, $service, $fee_id)
    {
        $client = $this->client->info($client_id);
        $fee = $this->money->fee($fee_id);
        $plans = $this->stripe->servicePaymentPlans($service);
        return view('app.money.plans.subscribe', array('client' => $client, 'service' => $service, 'fee' => $fee, 'plans' => $plans));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateSubscriptionRequest $request, $client_id, $service, $fee_id)
    {
        echo $client_id.' = '.$service.' = '.$fee_id.'<br/>';
        echo "<pre>";
        print_r($request->all());
        echo "</pre>";
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
