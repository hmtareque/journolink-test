<?php

namespace App\Http\Controllers\Money;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StorePaymentRequest;
use App\Http\Requests\UpdatePaymentRequest;
use App\Http\Requests\StripePaymentRequest;

use App\Modules\ClientModule;
use App\Modules\MoneyModule;
use App\Modules\StripeModule;

class PaymentController extends Controller {

    public function __construct(ClientModule $client, MoneyModule $money, StripeModule $stripe) {
        $this->client = $client;
        $this->money = $money;
        $this->stripe = $stripe;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($client_id, $service, $fee_id) {
        $client = $this->client->info($client_id);
        $fee = $this->money->fee($fee_id);
        return view('app.money.fees.payments.create', array('client' => $client, 'service' => $service, 'fee' => $fee));
    }
    
    
    
    
    
    
    public function getCardPayment($client_id, $service, $fee_id){
        $client = $this->client->info($client_id);
        $fee = $this->money->fee($fee_id);
        return view('app.money.fees.payments.card', array('client' => $client, 'service' => $service, 'fee' => $fee));
    }
    
    public function postCardPayment(StripePaymentRequest $request, $client_id, $service, $fee_id) {
        
        $charge = $this->stripe->charge($client_id, $fee_id, $request);
        
        $base_url = url('/');
        if ($charge->status) {
            return redirect($base_url . '/manage#clients/' . $client_id . '/services/' . $service . '/fees/' . $fee_id)->with('success', 'Payment successfully recorded.');
        }


        return redirect($base_url . 'manage/clients/' . $client_id . '/services/' . $service . '/fees/' . $fee_id.'/card-payment');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StorePaymentRequest $request, $client_id, $service, $fee_id) {

       // echo "<pre>";
       // print_r($request->all());
       // echo "</pre>";

       // exit;

        $paid = $this->money->storePayment($fee_id, $request);

        if ($paid) {
            return redirect('manage/clients/' . $client_id . '/services/' . $service . '/fees/' . $fee_id)->with('success', 'Payment successfully recorded.');
        }

        return redirect('manage/clients/' . $client_id . '/services/' . $service . '/fees/' . $fee_id . '/payments/create')->with('warning', 'fatPayment successfully recorded.');
    }

    

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($client_id, $service, $fee_id, $payment_id) {
        $client = $this->client->info($client_id);
        $fee = $this->money->fee($fee_id);
        $payment = $this->money->payment($payment_id);
        return view('app.money.fees.payments.edit', array('client' => $client, 'service' => $service, 'fee' => $fee, 'payment' => $payment));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdatePaymentRequest $request, $client_id, $service, $fee_id, $payment_id) {
        $update = $this->money->updatePayment($payment_id, $request);

        if ($update) {
            return redirect('manage/clients/' . $client_id . '/services/' . $service . '/fees/' . $fee_id)->with('success', 'Payment successfully recorded.');
        }

        return redirect('manage/clients/' . $client_id . '/services/' . $service . '/fees/' . $fee_id . '/payments/create')->with('warning', 'fatPayment successfully recorded.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($client_id, $service, $fee_id, $payment_id) {
        
        $payment = $this->money->payment($payment_id);
    
        
        
        $removed = $this->money->removePayment($payment_id);

        if ($removed) {
            return redirect('manage/clients/' . $client_id . '/services/' . $service . '/fees/' . $fee_id)->with('success', 'Payment successfully recorded.');
        }

        return redirect('manage/clients/' . $client_id . '/services/' . $service . '/fees/' . $fee_id.'/payments/'.$payment_id)->with('warning', 'fatPayment successfully recorded.');
    
        
        
    }

}