<?php

namespace App\Http\Controllers\Money;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\CreateStripePlanRequest;

use App\Modules\StripeModule;

class PlanController extends Controller
{
    public function __construct(StripeModule $stripe) {
        $this->stripe = $stripe;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $plans = $this->stripe->plans();
        return view('app.money.plans.index', array('plans' => $plans));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
      return view('app.money.plans.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(CreateStripePlanRequest $request)
    {
        
        $store = $this->stripe->storePlan($request);
        
        if ($store->status) {
            return redirect('manage/payment-plans')->with('success', 'Payment plan successfully created.');
        }

        return redirect('manage/payment-plans/create')->with('warning', 'Failed to create Payment plan!. Please try again.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }
    
    
    
    

    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}