<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
       return view('app.blank');
    }
    
    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function dashboard()
    {
        
        
        $area_of_access = array();
        $area_of_access['education']['type'] = 'custom'; 
        $area_of_access['education']['schools'] = array(
            'id' => 4,
            'batches' => array(
                56,87
            )
        );
        
        $area_of_access['family']['type'] = 'none'; 
        $area_of_access['family']['services'] = array(
            'id' => 4,
            'categories' => array(
                56,87
            )
        );
        
        
        $area_of_access['advocacy']['type'] = 'custom'; 
        $area_of_access['advocacy']['services'] = array(
            'id' => 7,
            'categories' => array(
                56,87,90
            )
        );
        
        
        
        
       return view('app.dashboard', array('access' => $area_of_access));
    }
}
