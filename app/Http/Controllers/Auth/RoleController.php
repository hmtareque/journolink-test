<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use App\Http\Requests\StoreRoleRequest;
use App\Http\Requests\UpdateRoleRequest;
use App\Modules\AuthModule;
use Session;
use show;

class RoleController extends Controller {

    protected $name;
    protected $role;
    protected $module;

    public function __construct(AuthModule $auth) {
        $this->middleware('auth');
        $this->name = trans('label.role');
        $this->auth = $auth;
      //  $this->module = 15; //role module 
      //  $this->agent = Session::get('user.agent.id');
    }

    /**
     * Display a listing of the role.
     *
     * @return html
     */
    public function index() {
        $roles = $this->auth->allRoles();
        return view('auth.roles.index', array('roles' => $roles));
    }

    /**
     * Show the form for creating a new role.
     *
     * @return html 
     */
    public function create() {
        return view('auth.roles.create');
    }

    /**
     * Store a newly created role in storage.
     *
     * @return redirect 
     */
    public function store(StoreRoleRequest $request) {
        
        
       // echo "<pre>";
       // print_r($request->all());
       // echo "</pre>";
       // exit;
        

        $role = $this->auth->storeRole($request->all());

        if ($role) {
            return redirect('manage/roles')->with('success', trans('alert.create_success', array('name' => $role->name)));
        }

        return redirect('manage/roles/create')->with('warning', trans('alert.create_failed', array('name' => $this->name)));
    }

    /**
     * Display the specified role.
     *
     * @param  int  $id
     * @return redirect 
     */
    public function show($id) {
        $role = $this->auth->showRoleInfo($id);
        $users = $this->auth->usersOfRole($id);
        return view('auth.roles.show', array('role' => $role, 'users' => $users));
    }

    /**
     * Show the form for editing the specified role.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id) {
         $role = $this->auth->showRoleInfo($id);
        $users = $this->auth->usersOfRole($id);
        return view('auth.roles.edit', array('role' => $role, 'users' => $users));
    }

    /**
     * Update the specified role in storage.
     *
     * @param  int  $id
     * @return redirect 
     */
    public function update(UpdateRoleRequest $request, $id) {

        $update = $this->auth->updateRole($id, $request);

        if ($update) {
            return redirect('manage/roles/' . $id . '/edit')->with('success', trans('alert.update_success', array('name' => $this->name)));
        }

        return redirect('manage/roles/' . $id . '/edit')->with('warning', trans('alert.update_failed', array('name' => $this->name)));
    }

    /**
     * Remove the specified role from storage.
     *
     * @param  int  $id
     * @return redirect
     */
    public function destroy($id) {

//        $has_user = $this->auth->users($id);
//        if (count($has_user) > 0) {
//            return redirect('roles')->with('warning', count($has_user) . ' user(s) using role ' . $role . '. Please delete those users before deleting the role.');
//        }
        
        

        $delete = $this->auth->deleteRole($id);

        if ($delete) {
            return redirect('manage/roles')->with('success', trans('alert.delete_success', array('name' => $this->name)));
        }

        return redirect('manage/roles')->with('warning', trans('alert.delete_failed', array('name' => $this->name)));
    }

}
