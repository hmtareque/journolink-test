<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;

use Illuminate\Http\Request;
use App\Http\Requests\StoreUserRequest;
use App\Http\Requests\UpdateUserRequest;
use App\Http\Requests\LoginUpdateRequest;
//use App\Events\UserCreated;
//use App\Events\UserUpdated;
use Illuminate\Support\Facades\Auth;
use App\Modules\AuthModule;
use App\Modules\ClassModule;
use App\Modules\SettingModule;
//use show;

class UserController extends Controller {

    private $auth;
    private $module;
    private $name;

    public function __construct(SettingModule $setting, AuthModule $auth, ClassModule $class) {
        $this->setting = $setting;
        $this->auth = $auth;
        $this->class = $class;
        $this->module = "user";
        $this->name = trans('label.user');
    }
    
    /**
     * Display a listing of all users.
     *
     * @return html view 
     */
    public function index() {
        $users = $this->auth->allStaff();
        return view('auth.users.index', array('users' => $users));
    }

    /**
     * Show the form for creating a new user.
     *
     * @return html view
     */
    public function create() {
        $schools = $this->class->classesBySchool();
        $family_services = $this->setting->services('family');
        $advocacy_services = $this->setting->services('advocacy');
        return view('auth.users.create', array('schools' => $schools, 'family_services' => $family_services, 'advocacy_services' => $advocacy_services));
    }

    /**
     * Store a newly created user in storage.
     *
     * @param  StoreUserRequest  $request
     * @return redirect
     */
    public function store(StoreUserRequest $request) {

        $user = $this->auth->storeUser($request);

        if ($user) {
            // success event
          //  event(new UserCreated($request->all()));
            return redirect('manage/users')->with('success', trans('alert.create_success', array('name' => $this->name)));
        }
        // failed event 
        return redirect('manage/users/create')->with('warning', trans('alert.create_failed', array('name' => $this->name)));
    }


    /**
     * Show the form for editing the specified user.
     *
     * @param  int  $id user id 
     * @return html view 
     */
    public function edit($id) {

        $user = $this->auth->user($id);
        return view('auth.users.edit', array('user' => $user));
    }

    /**
     * Update the specified user in storage.
     *
     * @param  UpdateUserRequest  $request
     * @param  int  $id user id 
     * @return redirect
     */
    public function update(UpdateUserRequest $request, $id) {
       $user = $this->auth->updateUser($id, $request);

        if ($user) {
            // success event
           // event(new UserUpdated($request->all()));
            return redirect('manage/users/'.$id.'/edit')->with('success', trans('alert.update_success', array('name' => $this->name)));
        }
        // failed event 
        return redirect('manage/users/'.$id.'/edit')->with('warning', trans('alert.update_failed', array('name' => $this->name)));
    }
    
    
    public function block($user_id){
        $block = $this->auth->blockUser($user_id);

        if ($block) {
            // success event
            //event(new UserUpdated($request->all()));
            return redirect('manage/users/'.$user_id.'/login-details')->with('success', trans('alert.block_success', array('name' => $this->name)));
        }
        // failed event 
        return redirect('manage/users/'.$user_id.'/login-details')->with('warning', trans('alert.block_failed', array('name' => $this->name)));
    }
    
    
    public function unblock($user_id){
        $unblock = $this->auth->unblockUser($user_id);

        if ($unblock) {
            // success event
            //event(new UserUpdated($request->all()));
            return redirect('manage/users/'.$user_id.'/login-details')->with('success', trans('alert.unblock_success', array('name' => $this->name)));
        }
        // failed event 
        return redirect('manage/users/'.$user_id.'/login-details')->with('warning', trans('alert.unblock_failed', array('name' => $this->name)));
    }
    
    
    public function editAccess($user_id){

        $user = $this->auth->user($user_id);
        $schools = $this->class->activeClassList();
        $family_services = $this->setting->services('family');
        $advocacy_services = $this->setting->services('advocacy');
        $access = $this->auth->access($user_id);
        
       return view('auth.users.access.edit', array('user' => $user, 'schools' => $schools, 'family_services' => $family_services, 'advocacy_services' => $advocacy_services, 'access' => $access));
   
    }
   
    
    public function updateAccess(Request $request, $user_id){
        
        
        
        echo "<pre>";
        print_r($request->get('access'));
        echo "</pre>";
        
        
        
       // exit;
        

        $update = $this->auth->updateAccess($user_id, $request);
        
        if ($update) {
            // success event
            return redirect('manage/users/'.$user_id.'/access')->with('success', 'User access successfully updated.');
        }
        // failed event 
        return redirect('manage/users/'.$user_id.'/access')->with('warning', 'Failed to update user access! Please try again.');
    
    }
    
    public function editLoginDetails($user_id){

        $user = $this->auth->userLoginDetails($user_id);
       return view('auth.users.login-details', array('user' => $user));
   
    }
   
    
    public function updateLoginDetails(LoginUpdateRequest $request, $user_id){

        $update = $this->auth->updateLoginDetails($user_id, $request->all());
        
        if ($update) {
            // success event
            return redirect('manage/users/'.$user_id.'/login-details')->with('success', 'User login details successfully updated.');
        }
        // failed event 
        return redirect('manage/users/'.$user_id.'/login-details')->with('warning', 'Failed to update user login details! Please try again.');
    
    }
    
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function profile() {
        $user = $this->auth->profile();
        return view('auth.users.profile', array('user' => $user));
    }

}
