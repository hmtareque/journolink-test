<?php

namespace App\Http\Controllers\Education\Reports;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Dompdf\Dompdf;

use App\Http\Requests\SchoolReportRequest;
use App\Http\Requests\StudentReportRequest;

use App\Modules\EducationReportModule;
use App\Modules\StudentModule;
use App\Modules\ClientModule;

class ReportController extends Controller
{
    public function __construct(EducationReportModule $report, StudentModule $student, ClientModule $client) {
        $this->report = $report;
        $this->student = $student;
        $this->client = $client;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('app.education.reports.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getReferralSchool()
    {
        return view('app.education.reports.referral-school');
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postReferralSchool(SchoolReportRequest $request)
    {
        
//        echo "<pre>";
//        print_r($request->all());
//        echo "</pre>";
        
        $data = $this->report->referralSchoolReport($request);
        
        return view('app.education.reports.referral-school', array('request' => $request, 'data' => $data));
    }
    
    
    
    
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getServiceManager()
    {
        return view('app.education.reports.service-manager');
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postServiceManager(SchoolReportRequest $request)
    {
        
//        echo "<pre>";
//        print_r($request->all());
//        echo "</pre>";
        
        $data = $this->report->serviceManagerReport($request);
        
        return view('app.education.reports.service-manager', array('request' => $request, 'data' => $data));
    }
    

    
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function searchStudent(Request $request)
    {

        $keyword = $request->get('search');
        
        $students = false;
            if(trim($keyword) != ""){
            $students = $this->student->search($keyword);
        }

        return view('app.education.reports.student-search', array('request' => $request, 'students' => $students));
    }
    
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function getStudent($client_id)
    {
        $student = $this->student->details($client_id);
        return view('app.education.reports.student', array('student' => $student));
    }
    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function postStudent(StudentReportRequest $request, $client_id)
    {
        
//        echo "<pre>";
//        print_r($request->all());
//        echo "</pre>";
        $student = $this->student->details($client_id);
        $data = $this->report->studentReport($client_id, $request);
        
        return view('app.education.reports.student', array('request' => $request, 'student' => $student, 'data' => $data));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
