<?php

namespace App\Http\Controllers\Education\Classes;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\AssignTeacherRequest;
use App\Http\Requests\UpdateAssignedTeacherRequest;

use App\Modules\ClassModule;
use App\Modules\TeacherModule;

class TeacherController extends Controller
{
    public function __construct(ClassModule $class, TeacherModule $teacher) {
        $this->class = $class;
        $this->teacher = $teacher;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($class_id)
    {
         $class = $this->class->info($class_id);
        $teachers = $this->class->teachers($class_id);
        return view('app.education.classes.teachers.index', array('class' => $class, 'teachers' => $teachers));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($class_id)
    {
        $class = $this->class->info($class_id);
        $teachers = $this->teacher->activeTeacherList();
        
        return view('app.education.classes.teachers.create', array('class' => $class, 'teachers' => $teachers));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(AssignTeacherRequest $request, $class_id)
    {
        $teacher_id = $request->get('teacher');
        $assign = $this->teacher->assign($class_id, $teacher_id);
        
        if ($assign) {
            // success event
            return redirect('manage/education/classes/'.$class_id.'/teachers')->with('success', 'Teacher successfully assigned.');
        }
        // failed event 
        return redirect('manage/education/classes/'.$class_id.'/teachers/create')->with('warning', 'Failed to assign teacher. Please try again.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($class_id, $teacher_id)
    {
        $class = $this->class->info($class_id);
        $teachers = $this->teacher->teacherList();
        $assigned_teacher = $this->teacher->assignedTeacher($class_id, $teacher_id);
        
        return view('app.education.classes.teachers.edit', array('class' => $class, 'teachers' => $teachers, 'assigned_teacher' => $assigned_teacher));
   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAssignedTeacherRequest $request, $class_id, $teacher_id)
    {
        $teacher = $this->teacher->updateAssignedTeacher($class_id, $teacher_id, $request);
        
        if ($teacher) {
            // success event
            return redirect('manage/education/classes/'.$class_id.'/teachers/'.$teacher.'/edit')->with('success', 'Teacher successfully updated.');
        }
        // failed event 
        return redirect('manage/education/classes/'.$class_id.'/teachers/'.$teacher_id.'/edit')->with('warning', 'Failed to update teacher. Please try again.');
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
