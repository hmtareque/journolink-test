<?php

namespace App\Http\Controllers\Education\Classes;

use access;
use App\Http\Requests\StoreClassRequest;
use App\Http\Requests\UpdateClassRequest;

use App\Http\Controllers\Controller;

use App\Modules\ClassModule;

class ClassController extends Controller
{
    public function __construct(ClassModule $class) {
        $this->class = $class;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schools = $this->class->classesBySchool();
        return view('app.education.classes.index', array('schools' => $schools));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('app.education.classes.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreClassRequest $request)
    {
     
        
        $store = $this->class->store($request);
        
         if ($store) {
            // success event
            return redirect('manage/education/classes')->with('success', 'Class successfully created.');
        }
        // failed event 
        return redirect('manage/education/classes/create')->with('warning', 'Failed to create the class. Please try again.');
   
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $class = $this->class->details($id);
        
        $school_access = access::services('education');
        
        if($school_access['type'] === 'none' || ($school_access['type'] === 'custom' && !in_array($class->school_id, $school_access['access']))){
            return view('errors.403');
        }
        
        
        $class_access = access::categories('education', $class->school_id);
        
        if($class_access['type'] === 'none' || ($class_access['type'] === 'custom' && !in_array($class->id, $class_access['access']))){
            return view('errors.403');
        }
        
       
        
        
        
        
        return view('app.education.classes.show', array('class' => $class));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $class = $this->class->info($id);
        return view('app.education.classes.edit', array('class' => $class));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateClassRequest $request, $id)
    {
        $store = $this->class->update($id, $request);
        
         if ($store) {
            // success event
            return redirect('manage/education/classes/'.$id.'/edit')->with('success', 'School successfully created.');
        }
        // failed event 
        return redirect('manage/education/classes/'.$id.'/edit')->with('warning', 'Failed to create the school information. Please try again.');
   
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
