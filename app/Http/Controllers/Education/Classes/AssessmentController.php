<?php

namespace App\Http\Controllers\Education\Classes;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Modules\ClassModule;

class AssessmentController extends Controller
{
    public function __construct(ClassModule $class) {
        $this->class = $class;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($class_id)
    {
        return view('app.education.classes.assessments.index');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($class_id)
    {
        $class = $this->class->info($class_id);
        $class->teachers = $this->class->activeTeachers($class_id);
        $class->students = $this->class->activeStudents($class_id);
        return view('app.education.classes.assessments.create', array('class' => $class));
    
   }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $class_id)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($class_id, $class_assessment_id)
    {
        $class = $this->class->info($class_id);
        $class->teachers = $this->class->activeTeachers($class_id);
        $class->students = $this->class->activeAssessedStudents($class_id, $class_assessment_id);
        $class->assessment = $this->class->assessment($class_assessment_id);
        return view('app.education.classes.assessments.show', array('class' => $class));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($class_id, $class_assessment_id)
    {
        $class = $this->class->info($class_id);
        $class->teachers = $this->class->activeTeachers($class_id);
        $class->students = $this->class->activeAssessedStudents($class_id, $class_assessment_id);
        $class->assessment = $this->class->assessment($class_assessment_id);
        return view('app.education.classes.assessments.edit', array('class' => $class));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $class_id, $class_assessment_id)
    {
        
        $update = $this->class->updateAssessment($class_assessment_id, $request);
        
        if ($update) {
            // success event
            return redirect('manage/education/classes/'.$class_id.'/assessments/'.$class_assessment_id)->with('success', 'Class assessment successfully updated.');
        }
        // failed event 
        return redirect('manage/education/classes/'.$class_id.'/assessments/'.$class_assessment_id.'/edit')->with('warning', 'Failed to update the class assessment. Please try again.');
   
        
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($class_id, $assessment_id)
    {
        //
    }
}
