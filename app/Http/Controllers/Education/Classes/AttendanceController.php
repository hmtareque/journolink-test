<?php

namespace App\Http\Controllers\Education\Classes;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreClassAttendance;

use App\Modules\ClassModule;

class AttendanceController extends Controller
{
     public function __construct(ClassModule $class) {
        $this->class = $class;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($class_id)
    {
        $class = $this->class->info($class_id);
        $attendances = $this->class->attendances($class_id);
        return view('app.education.classes.attendances.index', array('class' => $class, 'attendances' => $attendances));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($class_id)
    {
        
       // echo $class_id;
        
        $class = $this->class->info($class_id);
        $class->teachers = $this->class->activeTeachers($class_id);
        $class->students = $this->class->activeStudents($class_id);
        return view('app.education.classes.attendances.create', array('class' => $class));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreClassAttendance $request, $class_id)
    {
        $store = $this->class->storeAttendance($class_id, $request);
        
        if ($store) {
            // success event
            return redirect('manage/education/classes/'.$class_id.'/attendances')->with('success', 'Class attendance successfully created.');
        }
        // failed event 
        return redirect('manage/education/classes/'.$class_id.'/attendances/create')->with('warning', 'Failed to create the class attendance. Please try again.');
   
        
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($class_id, $attendance_id)
    {
        $class = $this->class->info($class_id);
        $class->attendance = $this->class->attendance($attendance_id);
        return view('app.education.classes.attendances.show', array('class' => $class));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($class_id, $attendance_id)
    {
        $class = $this->class->info($class_id);
        $class->teachers = $this->class->activeTeachers($class_id);
        $class->attendance = $this->class->attendance($attendance_id);
        return view('app.education.classes.attendances.edit', array('class' => $class));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $class_id, $attendance_id)
    {
        $update = $this->class->updateAttendance($attendance_id, $request);
        
        if ($update) {
            // success event
            return redirect('manage/education/classes/'.$class_id.'/attendances/'.$attendance_id.'/edit')->with('success', 'Class attendance successfully created.');
        }
        // failed event 
        return redirect('manage/education/classes/'.$class_id.'/attendances/'.$attendance_id.'/edit')->with('warning', 'Failed to create the class attendance. Please try again.');
   
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($class_id, $attendance_id)
    {
        //
    }
}
