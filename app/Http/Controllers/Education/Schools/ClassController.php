<?php

namespace App\Http\Controllers\Education\Schools;

use access;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Modules\SchoolModule;

class ClassController extends Controller
{
    private $module;
    private $school;
    
    public function __construct(SchoolModule $school) {
        $this->module = 5;
        $this->school = $school;
    }
    
    public function manage($school_id){
        
        $this->authorize('read', $this->module);
        
        $access = access::services('education');
        if($access['type'] === 'custom' && !in_array($school_id, $access['access']) || $access === 'none'){
            return view('errors.403');
        }

        $school = $this->school->info($school_id);
        $classes = $this->school->schoolAllClasses($school_id);
        return view('app.education.schools.manage.classes', array('school' => $school, 'classes' => $classes));
    }
}
