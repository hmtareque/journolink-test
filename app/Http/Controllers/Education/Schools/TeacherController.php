<?php

namespace App\Http\Controllers\Education\Schools;

use access;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Modules\SchoolModule;

class TeacherController extends Controller
{
    private $module;
    private $school;
    
    public function __construct(SchoolModule $school) {
        $this->module = 7;
        $this->school = $school;
    }
    
    public function manage($school_id){
        
        $this->authorize('read', $this->module);
        
        $access = access::services('education');
        if($access['type'] === 'custom' && !in_array($school_id, $access['access']) || $access === 'none'){
            return view('errors.403');
        }
        
        $school = $this->school->info($school_id);
        $teachers = $this->school->allSchoolTeachers($school_id);
        return view('app.education.schools.manage.teachers', array('school' => $school, 'teachers' => $teachers));
    }
}
