<?php

namespace App\Http\Controllers\Education\Schools;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\StoreSchoolRequest;
use App\Http\Requests\UpdateSchoolRequest;

use App\Modules\SchoolModule;

use access;

class SchoolController extends Controller
{
    private $module;
    private $school;
    
    public function __construct(SchoolModule $school) {
        $this->module = 4;
        $this->school = $school;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $this->authorize('read', $this->module);
        $schools = $this->school->schools();
        $referral_schools = $this->school->referralSchools();
        return view('app.education.schools.index', array('schools' => $schools, 'referral_schools' => $referral_schools));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $this->authorize('create', $this->module);
        return view('app.education.schools.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSchoolRequest $request)
    {
        $this->authorize('create', $this->module);
        $store = $this->school->store($request->all());
        
          if ($store) {
            // success event
            return redirect('manage/education/schools')->with('success', 'School successfully created.');
        }
        // failed event 
        return redirect('manage/education/schools/create')->with('warning', 'Failed to create the school information. Please try again.');
   
   
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($school_id)
    {
        $this->authorize('read', $this->module);
        
       // print_r(access::services('education'));
        
        $access = access::services('education');
        if($access['type'] === 'custom' && !in_array($school_id, $access['access']) || $access === 'none'){
            return view('errors.403');
        }
        
        $school = $this->school->referralSchoolDetails($school_id);
        
        if($school->type == "referral"){
            if($access['type'] !== 'full'){
                return view('errors.403');
            }
            return view('app.education.schools.referral', array('school' => $school));
        }
        
        $school = $this->school->details($school_id);
        return view('app.education.schools.show', array('school' => $school));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($school_id)
    {
        $this->authorize('update', $this->module);
        
        $access = access::services('education');
        if($access['type'] === 'custom' && !in_array($school_id, $access['access']) || $access === 'none'){
            return view('errors.403');
        }

        $school = $this->school->info($school_id);
        
        if($school->type === "referral" && $access['type'] !== 'full'){
            return view('errors.403');
        }
        
        return view('app.education.schools.edit', array('school' => $school));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSchoolRequest $request, $school_id)
    {
        $this->authorize('update', $this->module);
        $update = $this->school->update($request->all(), $school_id);
        
          if ($update) {
            // success event
            return redirect('manage/education/schools/'.$school_id.'/edit')->with('success', 'School information successfully updated.');
        }
        // failed event 
        return redirect('manage/education/schools/'.$school_id.'/edit')->with('warning', 'Failed to updated the school information. Please try again.');
   
    }
    
    
    
 
    
    
    public function activate($school_id){
        $this->authorize('delete', $this->module);
        $activate = $this->school->activate($school_id);
        
          if ($activate) {
            // success event
            return redirect('manage/education/schools/'.$school_id)->with('success', 'School successfully activated.');
        }
        // failed event 
        return redirect('manage/education/schools/'.$school_id)->with('warning', 'Failed to activate the school. Please try again.');
   
    }
    
    public function deactivate($school_id){
        $this->authorize('delete', $this->module);
        $update = $this->school->deactivate($school_id);
        
          if ($update) {
            // success event
            return redirect('manage/education/schools/'.$school_id)->with('success', 'School successfully deactivated.');
        }
        // failed event 
        return redirect('manage/education/schools/'.$school_id)->with('warning', 'Failed to deactived the school. Please try again.');
   
    }
}
