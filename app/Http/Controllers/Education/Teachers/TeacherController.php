<?php

namespace App\Http\Controllers\Education\Teachers;

use access;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\StoreTeacherRequest;

use App\Modules\AuthModule;
use App\Modules\ClassModule;
use App\Modules\TeacherModule;

class TeacherController extends Controller
{
    public function __construct(TeacherModule $teacher, AuthModule $auth, ClassModule $class) {
        $this->teacher = $teacher;
        $this->auth = $auth;
        $this->class = $class;
        $this->module = "user";
        $this->name = trans('label.user');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $schools = $this->teacher->all();
        return view('app.education.teachers.index', array('schools' => $schools));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $staffs = $this->auth->activeUsers();
        $schools = $this->class->activeClassList();
        return view('app.education.teachers.create', array('staffs' => $staffs, 'schools' => $schools));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTeacherRequest $request)
    {
        
       // echo "<pre>";
       // print_r($request->all());
       // echo "</pre>";
        
        $store = $this->teacher->store($request);
            
       if ($store) {
            // success event
            return redirect('manage/education/teachers')->with('success', 'Teacher successfully created.');
        }
        // failed event 
        return redirect('manage/education/teachers/create')->with('warning', 'Failed to create the teacher. Please try again.');
   
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($teacher_id)
    {
        $teacher = $this->teacher->details($teacher_id);
        $school_access = access::services('education');
        if($school_access['type'] === 'none' || ($school_access['type'] === 'custom' && !in_array($teacher->school_id, $school_access['access']))){
            return view('errors.403');
        }

        $has_class_access = 1;

        if($school_access['type'] === 'custom'){
            
            if($teacher->running_classes->count()>0){
                $has_class_access = 0;
                foreach($teacher->running_classes as $teacher_class){
                    
                    $class_access = access::categories('education', $teacher_class->school_id);

                        if(in_array($teacher_class->class_id, $class_access['access'])){
                            $has_class_access = 1;
                        }
                    }
            }
            
            if($has_class_access == 0){
                return view('errors.403');
            }
        }

        $teacher->access = $this->auth->access($teacher->user_id);
        return view('app.education.teachers.show', array('teacher' => $teacher));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
