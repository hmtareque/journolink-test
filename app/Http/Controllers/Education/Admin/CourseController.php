<?php

namespace App\Http\Controllers\Education\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\StoreCourseRequest;
use App\Http\Requests\UpdateCourseRequest;

use App\Modules\EducationAdminModule;

class CourseController extends Controller
{
    public function __construct(EducationAdminModule $admin) {
        $this->module = 9;
        $this->admin = $admin;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $courses = $this->admin->courses();
        return view('app.education.admin.courses.index', array('courses' => $courses));
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function reorder() {
        $courses = $this->admin->courses();
        return view('app.education.admin.courses.reorder', array('courses' => $courses));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('app.education.admin.courses.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreCourseRequest $request) {
        $course = $this->admin->storeCourse($request);
        if ($course) {
            return redirect('manage/education/admin/courses')->with('success', 'Course successfully created.');
        }

        return redirect('manage/education/admin/courses/create')->with('error', 'Failed to create course! Please try again.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($course_id) {
        $course = $this->admin->course($course_id);
        return view('app.education.admin.courses.edit', array('course' => $course));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateCourseRequest $request, $course_id) {
        $course = $this->admin->updateCourse($course_id, $request);
        if ($course) {
            return redirect('manage/education/admin/courses/' . $course_id . '/edit')->with('success', 'Course successfully updated.');
        }

        return redirect('manage/education/admin/courses/' . $course_id . '/edit')->with('error', 'Failed to update course! Please try again.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function makeInctivate($course_id) {
        $course = $this->admin->makeInactiveCourse($course_id);
        if ($course) {
            return redirect('manage/education/admin/courses')->with('success', 'Course successfully updated.');
        }

        return redirect('manage/education/admin/courses')->with('error', 'Failed to update course! Please try again.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reactivate($course_id) {
        $course = $this->admin->reactivateCourse($course_id);
        if ($course) {
            return redirect('manage/education/admin/courses')->with('success', 'Course successfully updated.');
        }

        return redirect('manage/education/admin/courses')->with('error', 'Failed to update course! Please try again.');
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function saveOrder(Request $request) {
        $this->authorize('update', $this->module);
        $year = $this->admin->saveCourseOrder($request);
        if ($year) {
            return redirect('manage/education/admin/courses/reorder')->with('success', 'Courses order successfully updated.');
        }

        return redirect('manage/education/admin/courses/reorder')->with('error', 'Failed to update courses order! Please try again.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($course_id) {
        $course = $this->admin->removeCourse($course_id);
        if ($course) {
            return redirect('manage/education/admin/courses')->with('success', 'Course successfully updated.');
        }
        
        return redirect('manage/education/admin/courses/' . $course_id)->with('error', 'Failed to update course! Please try again.');
    }

}
