<?php

namespace App\Http\Controllers\Education\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreTermRequest;
use App\Http\Requests\UpdateTermRequest;
use App\Modules\EducationAdminModule;

class SchoolTermController extends Controller {

    public function __construct(EducationAdminModule $admin) {
        $this->module = 9;
        $this->admin = $admin;
    }

    
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($year_id) {
        $this->authorize('create', $this->module);
        $year = $this->admin->academicYear($year_id);
        return view('app.education.admin.terms.create', array('year' => $year));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreTermRequest $request, $year_id) {
        $this->authorize('create', $this->module);
        $year = $this->admin->storeTerm($year_id, $request);
        if ($year) {
            return redirect('manage/education/admin/academic-years/' . $year_id)->with('success', 'New Term successfully created.');
        }

        return redirect('manage/education/admin/academic-years/' . $year_id . '/terms/create')->with('error', 'Failed to create New Term! Please try again.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($year_id, $term_id) {
        $this->authorize('read', $this->module);
        $year = $this->admin->academicYear($year_id);
        $term = $this->admin->termDetails($term_id);
        return view('app.education.admin.terms.show', array('year' => $year, 'term' => $term));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($year_id, $term_id) {
        $this->authorize('update', $this->module);
        $year = $this->admin->academicYear($year_id);
        $term = $this->admin->term($term_id);
        return view('app.education.admin.terms.edit', array('year' => $year, 'term' => $term));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateTermRequest $request, $year_id, $term_id) {
        $this->authorize('update', $this->module);
        $year = $this->admin->updateTerm($term_id, $request);
        if ($year) {
            return redirect('manage/education/admin/academic-years/' . $year_id . '/terms/' . $term_id . '/edit')->with('success', 'Term successfully updated.');
        }

        return redirect('manage/education/admin/academic-years/' . $year_id . '/terms/' . $term_id . '/edit')->with('danger', 'Failed to update the Term! Please try again.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function complete($year_id, $term_id) {
        $this->authorize('update', $this->module);
        $year = $this->admin->completeTerm($term_id);
        if ($year) {
            return redirect('manage/education/admin/academic-years/' . $year_id . '/terms/' . $term_id)->with('success', 'Term successfully marked as completed.');
        }

        return redirect('manage/education/admin/academic-years/' . $year_id . '/terms/' . $term_id)->with('error', 'Failed to mark the term as complete! Please try again.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reopen($year_id, $term_id) {
        $this->authorize('update', $this->module);
        $year = $this->admin->reopenTerm($term_id);
        if ($year) {
            return redirect('manage/education/admin/academic-years/' . $year_id . '/terms/' . $term_id)->with('success', 'Term successfully reopened.');
        }

        return redirect('manage/education/admin/academic-years/' . $year_id . '/terms/' . $term_id)->with('error', 'Failed to reopen the term! Please try again.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function saveOrder(Request $request, $year_id) {
        $this->authorize('update', $this->module);
        $year = $this->admin->saveTermsOrder($request);
        if ($year) {
            return redirect('manage/education/admin/academic-years/' . $year_id)->with('success', 'Terms order successfully updated.');
        }

        return redirect('manage/education/admin/academic-years/' . $year_id)->with('error', 'Failed to update terms order! Please try again.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($year_id, $term_id) {
        $this->authorize('delete', $this->module);
        $year = $this->admin->removeTerm($term_id);
        if ($year) {
            return redirect('manage/education/admin/academic-years/' . $year_id)->with('success', 'Term successfully deleted.');
        }

        return redirect('manage/education/admin/academic-years/' . $year_id . '/terms/' . $term_id)->with('error', 'Failed to delete the term! Please try again.');
    }

}
