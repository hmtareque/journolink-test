<?php

namespace App\Http\Controllers\Education\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\StoreStudentGroupRequest;
use App\Http\Requests\UpdateStudentGroupRequest;

use App\Modules\EducationAdminModule;

class StudentGroupController extends Controller
{
    public function __construct(EducationAdminModule $admin) {
        $this->module = 9;
        $this->admin = $admin;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $student_groups = $this->admin->studentGroups();
        return view('app.education.admin.student-groups.index', array('groups' => $student_groups));
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function reorder() {
        $student_groups = $this->admin->studentGroups();
        return view('app.education.admin.student-groups.reorder', array('groups' => $student_groups));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('app.education.admin.student-groups.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreStudentGroupRequest $request) {
        $group = $this->admin->storeStudentGroup($request);
        if ($group) {
            return redirect('manage/education/admin/student-groups')->with('success', 'Student group successfully created.');
        }

        return redirect('manage/education/admin/student-groups/create')->with('error', 'Failed to create student group! Please try again.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($group_id) {
        $group = $this->admin->studentGroup($group_id);
        return view('app.education.admin.student-groups.edit', array('group' => $group));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateStudentGroupRequest $request, $group_id) {
        $group = $this->admin->updateStudentGroup($group_id, $request);
        if ($group) {
            return redirect('manage/education/admin/student-groups/' . $group_id . '/edit')->with('success', 'Student group successfully updated.');
        }

        return redirect('manage/education/admin/student-groups/' . $group_id . '/edit')->with('error', 'Failed to update student group! Please try again.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function makeInctivate($group_id) {
        $group = $this->admin->makeInactiveStudentGroup($group_id);
        if ($group) {
            return redirect('manage/education/admin/student-groups')->with('success', 'Student group successfully updated.');
        }

        return redirect('manage/education/admin/student-groups')->with('error', 'Failed to update student group! Please try again.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reactivate($group_id) {
        $group = $this->admin->reactivateStudentGroup($group_id);
        if ($group) {
            return redirect('manage/education/admin/student-groups')->with('success', 'Student group successfully updated.');
        }

        return redirect('manage/education/admin/student-groups')->with('error', 'Failed to update student group! Please try again.');
    }
    
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function saveOrder(Request $request) {
        $this->authorize('update', $this->module);
        $year = $this->admin->saveStudentGroupOrder($request);
        if ($year) {
            return redirect('manage/education/admin/student-groups/reorder')->with('success', 'Student groups order successfully updated.');
        }

        return redirect('manage/education/admin/student-groups/reorder')->with('error', 'Failed to update student groups order! Please try again.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($group_id) {
        $group = $this->admin->removeStudentGroup($group_id);
        if ($group) {
            return redirect('manage/education/admin/student-groups')->with('success', 'Student group successfully updated.');
        }

        return redirect('manage/education/admin/student-groups/' . $group_id)->with('error', 'Failed to update student group! Please try again.');
    }

}
