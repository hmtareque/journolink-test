<?php

namespace App\Http\Controllers\Education\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Requests\StoreAcademicYearRequest;
use App\Http\Requests\UpdateAcademicYearRequest;
use App\Modules\EducationAdminModule;

class AcademicYearController extends Controller {

    public function __construct(EducationAdminModule $admin) {
        $this->module = 9;
        $this->admin = $admin;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index() {
        $academic_years = $this->admin->academicYears();
        return view('app.education.admin.academic-years.index', array('years' => $academic_years));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create() {
        return view('app.education.admin.academic-years.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreAcademicYearRequest $request) {
        $year = $this->admin->storeAcademicYear($request);
        if ($year) {
            return redirect('manage/education/admin/academic-years')->with('success', 'Academic year successfully created.');
        }

        return redirect('manage/education/admin/academic-years')->with('error', 'Failed to create Academic year! Please try again.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($year_id) {
        $year = $this->admin->academicYearDetails($year_id);
        return view('app.education.admin.academic-years.show', array('year' => $year));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($year_id) {
        $year = $this->admin->academicYear($year_id);
        return view('app.education.admin.academic-years.edit', array('year' => $year));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateAcademicYearRequest $request, $year_id) {
        $year = $this->admin->updateAcademicYear($year_id, $request);
        if ($year) {
            return redirect('manage/education/admin/academic-years/' . $year_id . '/edit')->with('success', 'Academic year successfully updated.');
        }

        return redirect('manage/education/admin/academic-years/' . $year_id . '/edit')->with('error', 'Failed to update Academic year! Please try again.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function complete($year_id) {
        $year = $this->admin->completeAcademicYear($year_id);
        if ($year) {
            return redirect('manage/education/admin/academic-years/' . $year_id)->with('success', 'Academic year successfully updated.');
        }

        return redirect('manage/education/admin/academic-years/' . $year_id)->with('error', 'Failed to update Academic year! Please try again.');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function reopen($year_id) {
        $year = $this->admin->reopenAcademicYear($year_id);
        if ($year) {
            return redirect('manage/education/admin/academic-years/' . $year_id)->with('success', 'Academic year successfully updated.');
        }

        return redirect('manage/education/admin/academic-years/' . $year_id)->with('error', 'Failed to update Academic year! Please try again.');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($year_id) {
        $year = $this->admin->removeAcademicYear($year_id);
        if ($year) {
            return redirect('manage/education/admin/academic-years')->with('success', 'Academic year successfully updated.');
        }

        return redirect('manage/education/admin/academic-years/' . $year_id)->with('error', 'Failed to update Academic year! Please try again.');
    }

}
