<?php

namespace App\Http\Controllers\Education\Other;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;


use Dompdf\Dompdf;

use App\Http\Requests\SchoolReportRequest;
use App\Http\Requests\StudentReportRequest;

use App\Modules\EducationReportModule;
use App\Modules\StudentModule;
use App\Modules\ClientModule;

class PrintController extends Controller {

    public function __construct(EducationReportModule $report, StudentModule $student, ClientModule $client) {

        $this->report = $report;
        $this->student = $student;
        $this->client = $client;
    }
    
    
    public function referralSchoolReport(SchoolReportRequest $request){

         $data = $this->report->referralSchoolReport($request);
        
         $html = view('app.education.prints.referral-school-report', array('request' => $request, 'data' => $data));

        // instantiate and use the dompdf class
        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);

// (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');


// Render the HTML as PDF
        $dompdf->render();
// Output the generated PDF to Browser
        $dompdf->stream('referral_school_report.pdf'); //'test.pdf', array("Attachment" => false)
    
        
    }
    
    
    public function serviceManagerReport(SchoolReportRequest $request){
        $data = $this->report->serviceManagerReport($request);
        
        $html = view('app.education.prints.service-manager-report', array('request' => $request, 'data' => $data));
    
         // instantiate and use the dompdf class
        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);

// (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');


// Render the HTML as PDF
        $dompdf->render();
// Output the generated PDF to Browser
        $dompdf->stream('service_manager_report.pdf'); //'test.pdf', array("Attachment" => false)
    
        
    }
    
    
    public function studentReport(StudentReportRequest $request, $client_id){
        
        $data = $this->report->studentReport($client_id, $request);
        
        $html = view('app.education.prints.student-report', array('request' => $request, 'data' => $data));
    
         // instantiate and use the dompdf class
        $dompdf = new Dompdf();
        $dompdf->loadHtml($html);

// (Optional) Setup the paper size and orientation
        $dompdf->setPaper('A4', 'portrait');


// Render the HTML as PDF
        $dompdf->render();
       // Output the generated PDF to Browser
        $dompdf->stream('student_report.pdf'); //'test.pdf', array("Attachment" => false)
    
        
    }

}
