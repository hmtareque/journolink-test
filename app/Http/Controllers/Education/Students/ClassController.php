<?php

namespace App\Http\Controllers\Education\Students;

use Session;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\EnrolStudentRequest;

use App\Modules\ClientModule;
use App\Modules\StudentModule;

class ClassController extends Controller
{
    public function __construct(ClientModule $client, StudentModule $student) {
        $this->client = $client;
        $this->student = $student;
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($client_id)
    {
        if(!engaged($client_id, 'education')){
            $checked = Session::get('checked');
            if($checked != $client_id){
                return redirect('manage/education/students');
            }
        }
        
        Session::forget('checked');
        
        $client = $this->client->info($client_id);
        return view('app.education.students.classes.create', array('client' => $client));
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(EnrolStudentRequest $request, $client_id)
    {
        
     //   echo "<pre>";
     //   print_r($request->all());
     //   echo "</pre>";
        
       
        $enrol = $this->student->enrol($client_id, $request);
        
         
                 
        
        if($enrol){
       // success event
            return redirect('manage/education/students/'.$client_id)->with('success', 'Student profile picture successfully updated.');
        }
        // failed event 
        return redirect('manage/education/students/'.$client_id.'/classes/create')->with('warning', 'Failed to update the student profile picture. Please try again.');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($client_id, $class_id)
    {
        $client = $this->client->info($client_id);
        $class = $this->student->enrolledClass($client_id, $class_id);
        return view('app.education.students.classes.show', array('client' => $client, 'class' => $class));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
