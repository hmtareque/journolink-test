<?php

namespace App\Http\Controllers\Education\Students;

use Session;
use Illuminate\Http\Request;

use App\Http\Requests\StoreStudentRequest;
use App\Http\Requests\UpdateStudentRequest;
use App\Http\Requests\UploadProfilePictureRequest;
use App\Http\Controllers\Controller;

use App\Modules\MoneyModule;
use App\Modules\ClientModule;
use App\Modules\StudentModule;
use App\Modules\S3Module;

class StudentController extends Controller
{
    public function __construct(ClientModule $client, StudentModule $student, MoneyModule $money, S3Module $s3) {
        $this->client = $client;
        $this->student = $student;
        $this->money = $money;
        $this->s3 = $s3;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function recent()
    {
        $students = $this->student->recent();
        return view('app.education.students.index', array('students' => $students));
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function active()
    {
        $students = $this->student->active();
        return view('app.education.students.active', array('students' => $students));
        
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function search()
    {
        $students = $this->student->active();
        return view('app.education.students.index', array('students' => $students));
        
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $students = $this->student->all();
        return view('app.education.students.index', array('students' => $students));
        
    }
    
    

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('app.education.students.create');
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreStudentRequest $request)
    {
        $student = $this->client->store($request);
        
        if($student){
       // success event
            return redirect('manage/education/students')->with('success', 'Student '.$student->first_name.' '.$student->middle_name.' '.$student->last_name.' successfully created.');
        }
        // failed event 
        return redirect('manage/education/students/create')->with('warning', 'Failed to create the student. Please try again.');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($student_id)
    {

        if(!engaged($student_id, 'education')){
            $checked = Session::get('checked');
            if($checked != $student_id){
                return redirect('manage/education/students');
            }
        }
        
        $student = $this->student->details($student_id);
        $service_account_summary = $this->money->serviceAccountSummary($student_id, 'education');
        return view('app.education.students.show', array('student' => $student, 'service_account_summary' => $service_account_summary));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($student_id)
    {
        $student = $this->student->details($student_id);
        return view('app.education.students.edit', array('student' => $student));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateStudentRequest $request, $student_id)
    {
        $updated = $this->client->update($student_id, $request);
        
        if($updated){
       // success event
            return redirect('manage/education/students/'.$student_id.'/edit')->with('success', 'Student information successfully created.');
        }
        // failed event 
        return redirect('manage/education/students/'.$student_id.'/edit')->with('warning', 'Failed to update the student information. Please try again.');

    }
    
    public function enrol(StudentEnrolRequest $request, $student_id){
        
       
        
    }
    
    
    

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
