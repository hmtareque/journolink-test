<?php

namespace App\Http\Controllers\Education\Students;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\StoreStudentAssessmentRequest;

use App\Modules\ClientModule;
use App\Modules\StudentModule;
use App\Modules\ClassModule;

class AssessmentController extends Controller
{
    public function __construct(ClientModule $client, StudentModule $student, ClassModule $class) {
        $this->client = $client;
        $this->student = $student;
        $this->class = $class;
    }
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($client_id, $class_id)
    {
        $student = $this->student->enrolledStudent($client_id, $class_id);
        $class = $this->class->info($class_id);
        $class->terms = $this->class->terms($class_id);
        return view('app.education.students.assessments.create', array('student' => $student, 'class' => $class));
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreStudentAssessmentRequest $request, $client_id, $class_id)
    {
        
        $assessment = $this->student->storeAssessment($class_id, $request);
        
         if($assessment){
       // success event
            return redirect('manage/education/students/'.$client_id.'/classes/'.$class_id)->with('success', 'Student assessment successfully saved.');
        }
        // failed event 
        return redirect('manage/education/students/'.$client_id.'/classes/'.$class_id.'/create')->with('warning', 'Failed to save the student assessment. Please try again.');
    
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
