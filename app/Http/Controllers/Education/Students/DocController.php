<?php

namespace App\Http\Controllers\Education\Students;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Modules\S3Module;

class DocController extends Controller
{
    public function __construct(S3Module $s3) {
        $this->s3 = $s3;
    }
    
    
    public function picture(){
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function add(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function remove($id)
    {
        //
    }
}
