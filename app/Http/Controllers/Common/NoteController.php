<?php

namespace App\Http\Controllers\Common;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\StoreNoteRequest;
use App\Http\Requests\UpdateNoteRequest;

use App\Modules\NoteModule;

class NoteController extends Controller
{
    function __construct(NoteModule $note) {
        $this->note = $note;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreNoteRequest $request)
    {
        $object_type = $request->get('object_type'); 
        $object_id = $request->get('object_id'); 
        $note = $request->get('note'); 
        
        $private = ($request->get('private') == 1)? 1 : 0;
        
        $return = $request->get('return');

        $store = $this->note->store($object_type, $object_id, $note, $private);
        
        if($store){
            return redirect($return)->with('success', 'Note successfully created.');
        }
        
        return redirect($return)->with('error', 'Failed to create the note! Please try again.');
    
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateNoteRequest $request, $note_id)
    {
        $note = $request->get('note'); 
        $private = ($request->get('private') == 1)? 1 : 0;
        $return = $request->get('return');

        $update = $this->note->update($note_id, $note, $private);
        
        if($update){
            return redirect($return)->with('success', 'Note successfully updated.');
        }
        
        return redirect($return)->with('error', 'Failed to update the note! Please try again.');
    
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, $note_id)
    {
        $return = $request->get('return');
        
        $remove = $this->note->remove($note_id);
        
        if($remove){
            return redirect($return)->with('success', 'Note successfully deleted.');
        }
        
        return redirect($return)->with('error', 'Failed to delete the note! Please try again.');
    
    }
}
