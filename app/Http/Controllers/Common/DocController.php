<?php

namespace App\Http\Controllers\Common;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Requests\UploadPictureRequest;
use App\Http\Requests\UploadDocRequest;
use App\Http\Requests\AddDocRequest;
use App\Http\Requests\RemoveDocRequest;
use App\Http\Requests\RemoveFileRequest;

use App\Modules\S3Module;

class DocController extends Controller
{
    public function __construct(S3Module $s3) {
        $this->s3 = $s3;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function picture(UploadPictureRequest $request)
    {
        $object_type = $request->get('object_type'); 
        $object_id = $request->get('object_id'); 
        $folder = $request->get('folder'); 
        $name = $request->get('name'); 
        $picture = $request->file('picture');
        $return = $request->get('return');

        $store = $this->s3->storePicture($object_type, $object_id, $folder, $name, $picture);
        
        if($store){
            return redirect($return)->with('success', 'Picture successfully uploaded.');
        }
        
        return redirect($return)->with('error', 'Failed to uploaded the picture! Please try again.');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(UploadDocRequest $request)
    {
        $object_type = $request->get('object_type'); 
        $object_id = $request->get('object_id'); 
        $folder = $request->get('folder'); 
        $name = $request->get('name'); 
        $files = $request->file('files');
        $security = $request->get('security');
        $return = $request->get('return');

        $store = $this->s3->store($object_type, $object_id, $folder, $name, $files, $security);
        
        if($store){
            return redirect($return)->with('success', 'Document successfully uploaded.');
        }
        
        return redirect($return)->with('error', 'Failed to uploaded the document! Please try again.');
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function add(AddDocRequest $request)
    {
        $doc = $request->get('doc'); 
        $files = $request->file('files');
        $security = $request->get('security');
        $return = $request->get('return');

        $store = $this->s3->add($doc, $files, $security);
        
        if($store){
            return redirect($return)->with('success', 'Document successfully uploaded.');
        }
        
        return redirect($return)->with('error', 'Failed to uploaded the document! Please try again.');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function remove(RemoveFileRequest $request)
    {
        $doc = $request->get('doc'); 
        $file = $request->get('file');
        $return = $request->get('return');

        $store = $this->s3->remove($doc, $file);
        
        if($store){
            return redirect($return)->with('success', 'Document successfully uploaded.');
        }
        
        return redirect($return)->with('error', 'Failed to uploaded the document! Please try again.');
    }

    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(RemoveDocRequest $request)
    {
        $doc = $request->get('doc'); 
        $return = $request->get('return');
        
        $delete = $this->s3->destroy($doc);
        
        if($delete){
            return redirect($return)->with('success', 'Document successfully removed.');
        }
        
        return redirect($return)->with('error', 'Failed to remove the document! Please try again.');
    }
}
