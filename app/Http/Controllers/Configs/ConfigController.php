<?php

namespace App\Http\Controllers\Configs;

use Illuminate\Support\Facades\Cache;

use App\Http\Requests\StoreSettingRequest;
use App\Http\Requests\UpdateSettingsRequest;

use App\Http\Controllers\Controller;


use App\Modules\AppModule;

class ConfigController extends Controller
{
    private $name; 
    
    public function __construct(AppModule $app)
    {
        $this->app = $app;
        $this->middleware('dev');
        $this->name = trans('label.app_settings');
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $settings = $this->app->settings();
        return view('app.configs.index', array('settings' => $settings));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('app.configs.create');
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(StoreSettingRequest $request)
    {
        
        
        $store = $this->app->storeSetting($request->all());
        
        if($store){
            Cache::flush();
            return redirect('manage/configs')->with('success', trans('alert.create_success', array('name' => $this->name)));
        }
        
        return redirect('manage/configs')->with('failed', trans('alert.create_failed', array('name' => $this->name)));
        
        
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $settings = $this->app->settings();
        return view('app.configs.edit', array('settings' => $settings));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateSettingsRequest $request)
    {
         $update = $this->app->updateSettings($request->all());
        
        if($update){
            Cache::flush();
            return redirect('manage/configs/edit')->with('success', trans('alert.update_success', array('name' => $this->name)));
        }
        
        return redirect('manage/configs/edit')->with('failed', trans('alert.update_failed', array('name' => $this->name)));
        
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
         $delete = $this->app->deleteSetting($id);
        
        if($delete){
            return redirect('manage/configs')->with('success', trans('alert.delete_success', array('name' => $this->name)));
        }
        
        return redirect('manage/configs')->with('failed', trans('alert.delete_failed', array('name' => $this->name)));
        
    }
}
