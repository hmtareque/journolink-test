<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/post/{slug}', 'Website\WebController@post');
Route::get('/news', 'Website\WebController@news');
Route::get('/contact-us', 'Website\WebController@getContact');
Route::post('/contact-us', 'Website\WebController@postContact');
Route::get('/manage', 'HomeController@index'); //when website done - 'Website\WebController@index'
Route::get('/', 'HomeController@index'); 


Auth::routes();

Route::group(['prefix' => 'manage'], function () {
    
    ///// app /////
    Route::resource('clients', 'Clients\ClientController');
    
    // School 
    Route::get('education/assessments/classes/{class_id}/all', 'Education\AssessmentController@all');
    Route::resource('education/assessments', 'Education\AssessmentController');
    
    
    Route::resource('education/schools.notes', 'Education\Schools\NoteController');
    Route::resource('education/schools.docs', 'Education\Schools\DocController');
    
    Route::get('education/schools/{school_id}/classes/manage', 'Education\Schools\ClassController@manage');
    Route::get('education/schools/{school_id}/teachers/manage', 'Education\Schools\TeacherController@manage');
    
    
    Route::post('education/schools/{school_id}/deactivate', 'Education\Schools\SchoolController@deactivate');
    Route::post('education/schools/{school_id}/activate', 'Education\Schools\SchoolController@activate');
    Route::resource('education/schools', 'Education\Schools\SchoolController');
    
    
    // Class 
    Route::resource('education/classes.notes', 'Education\Classes\NoteController');
    Route::resource('education/classes.teachers', 'Education\Classes\TeacherController');
    Route::resource('education/classes.attendances', 'Education\Classes\AttendanceController');
    Route::resource('education/classes.assessments', 'Education\Classes\AssessmentController');
    Route::resource('education/classes.docs', 'Education\Classes\DocController');
    Route::resource('education/classes', 'Education\Classes\ClassController');
    
    //Student
    
    Route::resource('education/students.classes.assessments', 'Education\Students\AssessmentController');
    Route::resource('education/students.classes', 'Education\Students\ClassController');
    
    Route::get('education/active-students', 'Education\Students\StudentController@active');
    Route::resource('education/students', 'Education\Students\StudentController');
    
    // Teacher 
    Route::resource('education/teachers', 'Education\Teachers\TeacherController');
    
    
    
    // Education Print 
    Route::post('education/print/referral-school-report', 'Education\Other\PrintController@referralSchoolReport');
    Route::post('education/print/service-manager-report', 'Education\Other\PrintController@serviceManagerReport');
    Route::post('education/print/student-report/{client_id}', 'Education\Other\PrintController@studentReport');
    
    
    
    
    // Education Report
    Route::any('education/reports/search/students', 'Education\Reports\ReportController@searchStudent');
    
     Route::get('education/reports/students/{client_id}', 'Education\Reports\ReportController@getStudent');
    Route::post('education/reports/students/{client_id}', 'Education\Reports\ReportController@postStudent');
    
    Route::get('education/reports/referral-school', 'Education\Reports\ReportController@getReferralSchool');
    Route::post('education/reports/referral-school', 'Education\Reports\ReportController@postReferralSchool');
    Route::get('education/reports/service-manager', 'Education\Reports\ReportController@getServiceManager');
    Route::post('education/reports/service-manager', 'Education\Reports\ReportController@postServiceManager');
    
    
    
    Route::get('education/reports', 'Education\Reports\ReportController@index');
    
    // Education Admin
    Route::post('education/admin/academic-years/{year_id}/terms/save-order', 'Education\Admin\SchoolTermController@saveOrder');
    Route::resource('education/admin/academic-years.terms', 'Education\Admin\SchoolTermController');
    Route::post('education/admin/academic-years/{year_id}/mark-as-complete', 'Education\Admin\AcademicYearController@complete');
    Route::post('education/admin/academic-years/{year_id}/reopen', 'Education\Admin\AcademicYearController@reopen');
    Route::resource('education/admin/academic-years', 'Education\Admin\AcademicYearController');
    
    Route::post('education/admin/student-groups/{group_id}/save-order', 'Education\Admin\StudentGroupController@saveOrder');
    Route::post('education/admin/student-groups/{group_id}/make-inactive', 'Education\Admin\StudentGroupController@makeInctivate');
    Route::post('education/admin/student-groups/{group_id}/reactivate', 'Education\Admin\StudentGroupController@reactivate');
    Route::post('education/admin/student-groups/save-order', 'Education\Admin\StudentGroupController@saveOrder');
    Route::get('education/admin/student-groups/reorder', 'Education\Admin\StudentGroupController@reorder');
    Route::resource('education/admin/student-groups', 'Education\Admin\StudentGroupController');
    
    Route::post('education/admin/courses/{course_id}/save-order', 'Education\Admin\CourseController@saveOrder');
    Route::post('education/admin/courses/{course_id}/make-inactive', 'Education\Admin\CourseController@makeInctivate');
    Route::post('education/admin/courses/{course_id}/reactivate', 'Education\Admin\CourseController@reactivate');
    Route::post('education/admin/courses/save-order', 'Education\Admin\CourseController@saveOrder');
    Route::get('education/admin/courses/reorder', 'Education\Admin\CourseController@reorder');
    Route::resource('education/admin/courses', 'Education\Admin\CourseController');
    
    
    Route::get('education/admin', 'Education\Admin\AdminController@index');
   
    
    
    
    // Family 
    Route::resource('family/clients', 'Family\Clients\ClientController');
    
    // Case 
    Route::get('family/cases/{case_id}/assessments/types', 'Family\Cases\AssessmentController@types');
    Route::get('family/cases/{case_id}/assessments/{template_id}/create', 'Family\Cases\AssessmentController@create');
    Route::get('family/cases/{case_id}/assessments/{assessment_id}/compare', 'Family\Cases\AssessmentController@compare');
    Route::resource('family/cases.assessments', 'Family\Cases\AssessmentController');
    Route::resource('family/cases.actions', 'Family\Cases\ActionController');
    Route::resource('family/cases.docs', 'Family\Cases\DocController');
    Route::resource('family/cases', 'Family\Cases\CaseController');
    
    // Activity
    Route::resource('family/activities.attendees', 'Family\Activities\AttendeeController');
    Route::resource('family/activities.attendances', 'Family\Activities\AssessmentController');
    Route::resource('family/activities.assessments', 'Family\Activities\AssessmentController');
    Route::resource('family/activities.actions', 'Family\Activities\ActionController');
    Route::resource('family/activities.docs', 'Family\Activities\DocController');
    Route::resource('family/activities', 'Family\Activities\ActivityController');
    
    // Report
    Route::resource('family/reports', 'Family\Reports\ReportController');
    
    // Admin
    Route::resource('family/admin', 'Family\Admin\AdminController');
    
    
    // Advocacy
    Route::resource('advocacy/clients', 'Advocacy\Clients\ClientController');
    
    // Case 
    Route::get('advocacy/cases/{case_id}/assessments/types', 'Advocacy\Cases\AssessmentController@types');
    Route::get('advocacy/cases/{case_id}/assessments/{template_id}/create', 'Advocacy\Cases\AssessmentController@create');
    Route::get('advocacy/cases/{case_id}/assessments/{assessment_id}/compare', 'Advocacy\Cases\AssessmentController@compare');
    Route::resource('advocacy/cases.assessments', 'Advocacy\Cases\AssessmentController');
    Route::resource('advocacy/cases.actions', 'Advocacy\Cases\ActionController');
    Route::resource('advocacy/cases.docs', 'Advocacy\Cases\DocController');
    Route::resource('advocacy/cases', 'Advocacy\Cases\CaseController');
    
    // Activity
    Route::resource('advocacy/activities.attendees', 'Advocacy\Activities\AttendeeController');
    Route::resource('advocacy/activities.attendances', 'Advocacy\Activities\AssessmentController');
    Route::resource('advocacy/activities.assessments', 'Advocacy\Activities\AssessmentController');
    Route::resource('advocacy/activities.actions', 'Advocacy\Activities\ActionController');
    Route::resource('advocacy/activities.docs', 'Advocacy\Activities\DocController');
    Route::resource('advocacy/activities', 'Advocacy\Activities\ActivityController');
    
    // Report
    Route::resource('advocacy/reports', 'Advocacy\Reports\ReportController');
    
    // Admin
    Route::resource('advocacy/admin', 'Advocacy\Admin\AdminController');
    
    
    Route::resource('reports', 'Reports\ReportController');
    
    // Notice 
    Route::resource('notices/groups', 'Notices\GroupController');
    Route::resource('notices', 'Notices\NoticeController');
    
    // Common 
    Route::post('docs/picture', 'Common\DocController@picture');
    Route::post('docs/destroy', 'Common\DocController@destroy');
    Route::post('docs/remove', 'Common\DocController@remove');
    Route::post('docs/add', 'Common\DocController@add');
    Route::post('docs', 'Common\DocController@store');
    
    Route::resource('notes', 'Common\NoteController');
    
    

    // Assessment 
    Route::resource('assessments', 'Assessments\AssessmentController');
    
    // Resource
    Route::resource('resources', 'Resources\ResourceController');
    
    
    //Money 
    Route::get('clients/{client_id}/services/{service}/fees/{fee_id}/card-payment', 'Money\PaymentController@getCardPayment');
    Route::post('clients/{client_id}/services/{service}/fees/{fee_id}/card-payment', 'Money\PaymentController@postCardPayment');
    
    Route::resource('clients/{client_id}/services/{service}/fees', 'Money\FeeController');
    Route::resource('clients/{client_id}/services/{service}/fees.payments', 'Money\PaymentController');
    
    Route::resource('clients/{client_id}/services/{service}/fees.subscriptions', 'Money\SubscriptionController');
    Route::resource('payment-plans', 'Money\PlanController');
    
    
    
    // Config
    Route::get('configs/create', 'Configs\ConfigController@create');
    Route::get('configs/edit', 'Configs\ConfigController@edit');
    Route::put('configs', 'Configs\ConfigController@update');
    Route::get('configs', 'Configs\ConfigController@index');
    Route::post('configs/store', 'Configs\ConfigController@store');
    Route::delete('configs/{setting_id}', 'Configs\ConfigController@destroy');
    

    Route::get('/events/recent', 'Dev\AppEventController@recent');
    
    
    
    
    
    
    /////////////////
    
    Route::get('/', 'HomeController@index');
    Route::get('/dashboard', 'HomeController@dashboard');
    
    
    Route::get('/test', 'HomeController@test');
    
    
    # users route 
    Route::get('profile', 'Auth\UserController@profile');
    
    
    
    Route::post('users/{user_id}/block', 'Auth\UserController@block');
    Route::post('users/{user_id}/unblock', 'Auth\UserController@unblock');
    
    Route::get('users/{user_id}/access', 'Auth\UserController@editAccess');
    Route::post('users/{user_id}/access', 'Auth\UserController@updateAccess');
    
    Route::get('users/{user_id}/login-details', 'Auth\UserController@editLoginDetails');
    Route::post('users/{user_id}/login-details', 'Auth\UserController@updateLoginDetails');

    Route::resource('users', 'Auth\UserController');
    Route::resource('roles', 'Auth\RoleController');
    
    ////////////// Website manage routes ////////////////
    Route::get('web/post-categories/{category_id}/posts', 'Website\PostCategoryController@posts');
    Route::post('web/post-categories/{post_id}/show', 'Website\PostCategoryController@makeVisible');
    Route::post('web/post-categories/{post_id}/hide', 'Website\PostCategoryController@hide');
    Route::resource('web/post-categories', 'Website\PostCategoryController');
    
    Route::post('web/posts/{post_id}/publish', 'Website\PostController@publish');
    Route::post('web/posts/{post_id}/unpublish', 'Website\PostController@unpublish');
    Route::resource('web/posts', 'Website\PostController');
    
    Route::get('web/navs/{nav_id}/links/create', 'Website\NavController@createLink');
    Route::get('web/navs/{nav_id}/links', 'Website\NavController@links');
    Route::post('web/navs/{nav_id}/show', 'Website\NavController@makeVisible');
    Route::post('web/navs/{nav_id}/hide', 'Website\NavController@hide');
    Route::resource('web/navs', 'Website\NavController');
    
    Route::post('web/links/{link_id}/show', 'Website\LinkController@makeVisible');
    Route::post('web/links/{link_id}/hide', 'Website\LinkController@hide');
    Route::resource('web/links', 'Website\LinkController');
    
    Route::post('web/components/{component_id}/hide', 'Website\ComponentController@hide');
    Route::post('web/components/{component_id}/show', 'Website\ComponentController@makeVisible');
    Route::resource('web/components', 'Website\ComponentController');
    
    
    Route::post('web/components/{component_id}/items/{item_id}/hide', 'Website\ComponentItemController@hide');
    Route::post('web/components/{component_id}/items/{item_id}/show', 'Website\ComponentItemController@makeVisible');
    Route::resource('web/components.items', 'Website\ComponentItemController');
    
    
    Route::get('web/settings/create', 'Website\WebSettingController@create');
    Route::get('web/settings/edit', 'Website\WebSettingController@edit');
    Route::put('web/settings', 'Website\WebSettingController@update');
    Route::get('web/settings', 'Website\WebSettingController@index');
    Route::post('web/settings/store', 'Website\WebSettingController@store');
    Route::delete('web/settings/{setting_id}', 'Website\WebSettingController@destroy');
    
    
    
    //web contacts 
    Route::get('web/contacts/update', 'Website\ContactController@edit');
    Route::post('web/contacts/update', 'Website\ContactController@update');
    Route::get('web/contacts', 'Website\ContactController@index');

    ////////////////////////////////////////////////////////
    
    
    
    //////////// ajax /////////////
    
  
    Route::get('ajax/education/academic-years/{year_id}/schools/{school_id}/classes', 'Ajax\EducationController@getSchoolClasses');
    Route::get('ajax/education/academic-years/{year_id}/terms', 'Ajax\EducationController@getYearTerms');
    
    Route::post('ajax/clients/check-duplicate', 'Ajax\ClientController@checkDuplicate');
    Route::get('ajax/clients/{client_id}/verify', 'Ajax\ClientController@verify');
    

    
    
    
});





