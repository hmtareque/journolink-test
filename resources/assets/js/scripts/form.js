/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$(document).ready(function () {
    
   var xload = 1;
   var base_url = $('meta[name="base_url"]').attr('content') + '/';

   init();

    /**
     * Takes the current url and load the page through GET AJAX request
     */
    var segment = window.location.href.split('#')[1];
    var load_url = base_url + segment;
    $(window).load(function () {
        if (typeof segment !== "undefined") {
            if (segment.length > 0) {
                xReload(load_url);
            }
        }
        return false;
    });


    /**
     * Takes the hash option from href and load the page through GET AJAX reqeust
     */
    $(".xmenu li a").click(function () {
        var url = $(this).attr('href');
        var hash = url.split('#')[1];
        if (typeof hash !== "undefined") {
            if (hash.length > 0) {
                var url = base_url + hash;
                //xClick(url, hash);
                window.location.hash = hash;
            }
        }

        $(".nav > .dropdown").removeClass('open');
        $("#main-nav").removeClass('in');

        return false;
    });
//
//
//    /**
//     * Takes the hash option from href and load the page through GET AJAX reqeust
//     */
//
//    $(document).on('click', '.xclick', function () {
//        var url = $(this).attr('href');
//        var hash = url.split('#')[1];
//        if (typeof hash !== "undefined") {
//            if (hash.length > 0) {
//                var url = base_url + hash;
//                //  xClick(url, hash);
//
//                window.location.hash = hash;
//            }
//        }
//        return false;
//    });





    /**
     * AJAX form submit
     */
    $(document).on('submit', '.ajax-form', function () {

        //clearing the modal
        var modal = $('.modal');
        var background = $('.modal-backdrop');
        var body = $('body');

        var hash = $(this).data('hash');

        // inside event callbacks 'this' is the DOM element so we first 
        // wrap it in a jQuery object and then invoke ajaxSubmit 
        var submit_button = $(this).find('button[type="submit"]');
        var submit_button_txt = submit_button.text();

        $(this).ajaxSubmit({
            beforeSubmit: function () {
                submit_button.prop('disabled', true);
                submit_button.html('<i class="fa fa-spinner fa-pulse fa-fw"></i> Please wait ...');
            },
            dataType: 'html',
            success: function (content) {

                modal.remove();
                background.remove();
                body.removeClass('modal-open');
                body.css('padding-right', '0px');

                xload = 0;
                if (hash !== undefined) {
                    window.location.hash = hash;
                } else {
                    xload = 1;
                }

                $("#xcontent").html(content);
                submit_button.prop('disabled', false);
                submit_button.html('<i class="fa fa-save fa-fw"></i> Save');

                init();
            },
            error: function (xhr, status, text) {

                submit_button.prop('disabled', false);
                submit_button.html("<span><i class='fa fa-warning'></i>" + submit_button_txt + "</span>");

                console.log('>>> ' + status + ' => ' + text + ' <<<');

                if (xhr.status === 401) {
                    window.location.href = $('meta[name="base_url"]').attr('content');
                }

                if (xhr.status === 422) {
                    $('.ajax-form div').removeClass('has-error');
                    $('span.help-block').addClass('hide');
                    $('span.help-block').text('');
                    var response = JSON.parse(xhr.responseText);
                    $.each(response, function (id, msg) {
                        var html_id = id.replace(/\./g, '-');
                        $('#' + html_id).addClass('has-error');
                        $('#' + html_id + ' span.help-block').removeClass('hide');
                        $('#' + html_id + ' span.help-block').text(msg);

                    });
                } else {
                    var env = $('meta[name="env"]').attr('content');
                    if (env === "production" && xhr.status === 500) {
                        $("#xcontent").html('Something went terribly wrong!');
                    } else {
                        $("#xcontent").html(xhr.responseText);
                    }
                }
            }
        });

        // !!! Important !!! 
        // always return false to prevent standard browser submit and page navigation 
        return false;
    });
    
    
           /**
 * AJAX form submit for picture upload
 */
$(document).on('change', '.upload-picture', function () {
    var form = $(this).parent().parent("form.ajax-upload-form");
    var upload_status = $('#upload-status');
    $(form).ajaxSubmit({
        beforeSubmit: function () {
            upload_status.html("<span><i class='fa fa-cog fa-spin text-success'></i> Uploading ...</span>");
            $('.upload').prop('disabled', true);
        },
        dataType: 'html',
        success: function (content) {
            $("#xcontent").html(content);
            upload_status.html("<span><i class='fa fa-upload'></i> Upload Picture</span>");
            $('.upload').prop('disabled', false);
        },
       error: function (xhr, status, text) {
           
            if (xhr.status === 403) {
                $('#upload-response').html('<div class="alert alert-danger">Invalid Request <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            }

            if (xhr.status === 422) {
               var validation = JSON.parse(xhr.responseText);
                 $.each(validation, function(index, msg){
                        $('#upload-response').html('<div class="alert alert-danger">'+msg+' <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
                 });
            }

            if (xhr.status === 500) {
               $('#upload-response').html('<div class="alert alert-danger">Something went wrong! <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
            }
            
            upload_status.html("<span><i class='fa fa-upload'></i> Upload Picture</span>");
            $('.upload').prop('disabled', false);
            
        }
    });
    return false;
});


           /**
 * AJAX form submit for check client identity
 */

$(document).on('click', '.check-identity', function () {
    
    $('#client-id').val($(this).data('id'));
    $('#verify-return').val($(this).data('return'));
    $('#client-verify').val('');
    $('#client-identity-check-modal').modal('show');
    
});



//select other client
    $(document).on('click', '.check-identity', function () {

        var client_id = $(this).data('id');
        var redirect = $(this).data('redirect');
        $('#client-postcode').val("");
        $('#client-verify-modal').modal('toggle');

        $("#verify-client").click(function () {
            var postcode = $('#client-postcode').val();
            var base_url = $('meta[name="base_url"]').attr('content');

            if (postcode !== "") {
                if (client_id !== "undefined") {
                    $.ajax({
                        type: 'get',
                        url: base_url + '/ajax/clients/' + client_id+'/verify',
                        beforeSend: function () {
                            $("#" + client_id).html("<i class='fa fa-cog fa-spin'></i> checking ...");
                        },
                        dataType: 'json',
                        success: function (data) {
                            if (data.postcode === postcode) {
                               $('#client-verify-modal').modal('hide');
                                window.location.hash = redirect;
                            } else {
                                $("#client-verify-response").html("<span class='text-danger'>Postcode didn't match please try again.</span>");
                                $("#" + client_id).html("select");
                            }
                        },
                        error: function (xhr, status, text) {

                        }
                    });
                }
            }
        });

    }); 

       

    /* all click */
    window.onhashchange = function () {
        var url = window.location.href;
        var hash = url.split('#')[1];
        if (typeof hash !== "undefined" && xload === 1) {
            if (hash.length > 0) {
                var url = base_url + hash;
                xClick(url, hash);
            }
        }
        xload = 1;
        return false;
    };
    
    
    
    ///////////
    
    // add new property for object of shlott
$(document).on('click', '#add-new-property', function () {

    var item = Number($(this).val()) + 1;

    $.ajax({
        type: 'get',
        url: base_url + 'ajax/get-resource-property/' + item,
        dataType: 'html',
        success: function (item) {
            
        },
        error: function (xhr, status, text) {
            $("#loader").hide();
        }
    });

    $(this).val(item);
});

// remove object property from form 
$(document).on('click', '.remove_property', function () {
    $(this).parent().parent().remove();
});

// remove object property from form 
$(document).on('click', '.remove_existing_property', function () {
    //$(this).parent().slideUp();

    console.log($(this).val());

    var index = $(this).val();

    $("#property-" + index + "-deleted_at").val(1);

    $("#property-" + index).addClass("alert");
    $("#property-" + index).addClass("alert-danger");

    $("#restore-" + index).removeClass("hide");
    $("#restore-" + index).show();
    $(this).hide();

});

// remove object property from form 
$(document).on('click', '.restore_removed_property', function () {
    //$(this).parent().slideUp();
    var index = $(this).val();
    $("#property-" + index + "-deleted_at").val(0);
    $("#remove-" + index).removeClass("hide");
    $("#remove-" + index).show();
    $("#property-" + index).removeClass("alert");
    $("#property-" + index).removeClass("alert-danger");
    $(this).hide();
});
    
    
    
    
    //////////////

     $(document).on('click', '#add-component-property', function(){

        var item = $(this).data('item');

        var property = '<tr>'+
                    '<td>'+
                        '<div id="property-'+item+'-name">'+
                            '<input class="form-control" type="text" name="property['+item+'][name]" placeholder="Property Name">'+
                            '<span class="property-'+item+'-name-help-block help-block hide"></span>'+
                        '</div>'+
                    '</td>'+
                    '<td>'+
                        '<div id="property-'+item+'-type">'+
                            '<select class="component-property-type form-control" name="property['+item+'][type]" data-item="'+item+'">'+
                                '<option value="text">Text</option>'+
                                '<option value="numeric">Numeric</option>'+
                                '<option value="textarea">Text Area</option>'+
                                '<option value="select">Select</option>'+
                                '<option value="checkbox">Check Box</option>'+
                                '<option value="radio">Radio</option>'+
                                '<option value="email">Email</option>'+
                                '<option value="date">Date</option>'+
                                '<option value="image">Image</option>'+
                                '<option value="link">Link</option>'+
                            '</select>'+
                            '<span class="property-'+item+'-type-help-block help-block hide"></span>'+
                        '</div>'+
                    '</td>'+
                    '<td>'+
                        '<div id="property-'+item+'-options">'+
                            '<input class="form-control hide" type="text" id="property-'+item+'-option-value" name="property['+item+'][options]" placeholder="Options">'+
                            '<span class="property-'+item+'-options-help-block help-block hide"></span>'+
                        '</div>'+
                    '</td>'+
                    '<td>'+
                        '<label><input type="checkbox" name="property['+item+'][required]"> Yes</label>'+
                    '</td>'+
                    '<td>'+
                        '<input type="hidden" name="property['+item+'][visible]" value="1">'+
                        '<button type="button" class="btn btn-danger btn-xs remove-component-propery"><i class="fa fa-remove"></i> Remove</button>'+
                    '</td>'+
                '</tr>';
        item++;
        $(this).data('item', item);
        $('#component-property > tbody:last-child').append(property);
        return false;
    });
    
    
    $(document).on('click', '.remove-component-propery', function(){
        $(this).parent().parent().remove();
    });
    
    
    $(document).on('click', '.hide-component-propery', function(){
        $(this).parent().parent().addClass('bg-warning');
        $(this).addClass('hide');
        var item = $(this).data('item');
        $('#property-'+item+'-visible').val(0);
        $('#property-'+item+'-restore').removeClass('hide');
    });
    
    $(document).on('click', '.restore-component-propery', function(){
        $(this).parent().parent().removeClass('bg-warning');
        $(this).addClass('hide');
        var item = $(this).data('item');
        $('#property-'+item+'-visible').val(1);
        $('#property-'+item+'-hide').removeClass('hide');
    });
    
    $(document).on('change', '.component-property-type', function () {
        var item = $(this).data('item');
        var type = $(this).val();
        if (type === "select" || type === "checkbox" || type === "radio") {
            $('#property-' + item + '-option-value').removeClass('hide');
        } else {
            $('#property-' + item + '-option-value').val("");
             $('#property-' + item + '-option-value').addClass('hide');
        }
    });
    
    
    
    $(document).on("change", "#select-settings-option", function () {
        
        if ($(this).val() === "text") {
            $("#setting_value").removeClass("hide");
            $("#setting_value input[name='value']").prop("disabled", false);
            
            $("#setting_image").addClass("hide");
            $("#setting_image input[name='setting_image']").prop("disabled", true);
            
            $("#setting_options").addClass("hide");
            $("#setting_options input[name='setting_options']").prop("disabled", true);
        } 
        
        if ($(this).val() === "image") {
            $("#setting_value").addClass("hide");
            $("#setting_value input[name='value']").prop("disabled", true);
            
            $("#setting_image").removeClass("hide");
            $("#setting_image input[name='setting_image']").prop("disabled", false);
            
            $("#setting_options").addClass("hide");
            $("#setting_options input[name='setting_options']").prop("disabled", true);
        } 
        
        if ($(this).val() === "option") {
            $("#setting_value").removeClass("hide");
            $("#setting_value input[name='value']").prop("disabled", false);
            
            $("#setting_image").addClass("hide");
            $("#setting_image input[name='setting_image']").prop("disabled", true);
            
            $("#setting_options").removeClass("hide");
            $("#setting_options input[name='setting_options']").prop("disabled", false);
        } 

    });
    
    
    
 
    

    /* refresh file input */
    $(document).on('click', '.component-image-reloader', function () {
        var item = $(this).data("item");
        $('#actual-'+item+'-image').prop('disabled', false);
        var has_image = $('#actual-'+item+'-image').data('src');
        if(has_image !== ""){
             $('#image-'+item+'-preview').html('<img style="width: 64px; height: auto; margin-bottom: 5px;" src="'+has_image+'"/>');
        }
        $('#image-'+item+'-reloader').addClass('hide');
        $('#image-'+item+'-remover').removeClass('hide');
    });
    
    
    $(document).on('click', '.component-image-remover', function () {
        var item = $(this).data("item");
        $('#actual-'+item+'-image').prop('disabled', true);
        $('#item-'+item+'-file').val(''); 
        $('#image-'+item+'-preview').html('');
        $('#image-'+item+'-remover').addClass('hide');
        var has_image = $('#actual-'+item+'-image').val();
        if(has_image !== ""){
             $('#image-'+item+'-reloader').removeClass('hide');
        }
    });
    
    
    //preview files
    $(document).on('change', '.preview-file', function (event) {
        var item = $(this).data('item');
        var preview = "";
        $.each(event.target.files, function (index, file) {
            var ext = file.name.substr(file.name.lastIndexOf('.') + 1);
            var tmppath = URL.createObjectURL(event.target.files[index]);
            if (ext === 'png' || ext === 'jpg' || ext === 'jpeg' || ext === 'gif') {
                preview += '<img style="width: 64px; height: auto; margin-bottom: 5px;" src="' + tmppath + '" />';
            } else {
                preview += '<span class="text-dager">Selected file is not an image!</span>';
            }
        });
        $('#image-'+item+'-preview').html(preview);
        $('#image-'+item+'-remover').removeClass('hide');
    });
    
    
    
    $(document).on('change', '#select-nav-type', function () {
        if ($(this).val() === 'post') {
            $('#post_link').removeClass('hidden');
            $('#internal_link').addClass('hidden');
            $('#external_link').addClass('hidden');
            $('#nav_link').addClass('hidden');
        } 
        
        if ($(this).val() === 'internal') {
            $('#post_link').addClass('hidden');
            $('#internal_link').removeClass('hidden');
            $('#external_link').addClass('hidden');
            $('#nav_link').addClass('hidden');
        } 
        
        if ($(this).val() === 'external') {
            $('#post_link').addClass('hidden');
            $('#internal_link').addClass('hidden');
            $('#external_link').removeClass('hidden');
            $('#nav_link').addClass('hidden');
        } 
        
        if ($(this).val() === 'nav') {
            $('#post_link').addClass('hidden');
            $('#internal_link').addClass('hidden');
            $('#external_link').addClass('hidden');
            $('#nav_link').removeClass('hidden');
        } 
    });
    

    //form
    
    $(document).on('blur', '#last-name', function(){
        if($('#date-of-birth').val() != ''){
            checkDuplicateClient();
        }
    });
    
    $(document).on('change', '#date-of-birth', function(){
        if($('#last-name').val() != ''){
            checkDuplicateClient();
        }
    });
    
    $(document).on('blur', '#ni-number', function(){
          checkDuplicateClient();
    });
    
    $(document).on('blur', '#post-code', function(){
        if($('#last-name').val() != '' && $('#date-of-birth').val() != ''){
            checkDuplicateClient();
        }
    });
    
    

    $(document).on('change', 'select', function(){
        var other_field_name = "other_"+$(this).attr('name');
        if($(this).val() == 'other'){
            $('input[name="'+other_field_name+'"]').removeClass('hide');
        } else {
            $('input[name="'+other_field_name+'"]').addClass('hide');
        }
    });
    
    $(document).on('change', '#check-born-in-the-uk', function(){
       if($(this).val() == 1){
            $('#arrived_in_the_uk').addClass('hide');
        } else {
            $('#arrived_in_the_uk').removeClass('hide');
        }
    });
    
    $(document).on('change', '#has-dependent', function(){
       if($(this).val() == 1){
           $('#no_of_dependents').removeClass('hide');
           $('#dependents_details').removeClass('hide');
        } else {
           $('#no_of_dependents').addClass('hide');
           $('#dependents_details').addClass('hide');
        }
    });
    
    $(document).on('change', '#interpreter-required', function(){
       if($(this).val() == 1){
            $('#interpreter_language').removeClass('hide');
        } else {
            $('#interpreter_language').addClass('hide');
            
        }
    });
    
    $(document).on('change', '#has-disability', function(){
       if($(this).val() == 1){
            $('#disabilities').removeClass('hide');
        } else {
            $('#disabilities').addClass('hide');
        }
    });
    
    $(document).on('change', '#has-medical-conditions', function(){
       if($(this).val() == 1){
           $('#medical_conditions').removeClass('hide');
        } else {
           $('#medical_conditions').addClass('hide');
        }
    });
    
    $(document).on('change', '#has-allergy', function(){
       if($(this).val() == 1){
           $('#allergies').removeClass('hide');
        } else {
           $('#allergies').addClass('hide');
        }
    });
    
    $(document).on('change', '#need-additional-support', function(){
       if($(this).val() == 1){
            $('#additional_support').removeClass('hide');
        } else {
            $('#additional_support').addClass('hide');
        }
    });
    
   

    $(document).on('change', '#exempt-from-fee', function(){
        if($(this).val() == 1){
            $('#discount').addClass('hide');
            $('#paid').addClass('hide');
        } else {
            $('#discount').removeClass('hide');
            $('#paid').removeClass('hide');
        }
    });
    
    
    $(document).on('change', '#next-of-kin-address', function () {
        if ($(this).val() == 1) {
            $('#next-of-kin-address-data').addClass('hide');
        } else {
            $('#next-of-kin-address-data').removeClass('hide');
        }
    });
    
    $(document).on('change', '#is-paid', function () {
        if ($(this).val() == 1) {
            $('#salary-data').removeClass('hide');
        } else {
            $('#salary-data').addClass('hide');
        }
    });
    
    $(document).on('change', '#is-teaching', function () {
        if ($(this).val() == 1) {
            $('#teaching-data').removeClass('hide');
        } else {
            $('#teaching-data').addClass('hide');
        }
    });
    

    $(document).on('change', '.data-access-type', function () {
        if ($(this).val() == 'custom') {
            $('#' + $(this).data('access')).removeClass('hide');
        } else {
            $('#' + $(this).data('access')).addClass('hide');
        }
    });

    $(document).on('click', '.service', function () {
        var checked = $(this).prop('checked');
        if (checked) {
            $('.service-' + $(this).attr('id') + '-category').prop('checked', true);
        } else {
            $('.service-' + $(this).attr('id') + '-category').prop('checked', false);
        }
    });

    $(document).on('click', '.service-category', function () {
        var checked = $(this).prop('checked');
        if (checked) {
            $('#' + $(this).data('service')).prop('checked', true);
        } else {
            if ($('.service-' + $(this).data('service') + '-category:checked').length == 0) {
                $('#' + $(this).data('service')).prop('checked', false);
            }
        }
    });
    
});
    
    /**
 * Loads page from anchor click by AJAX GET and set url hash
 * @param {string} url
 * @param {string} hash
 * @returns {html}
 */

function xClick(url, hash) {
    $.ajax({
        type: 'get',
        url: url,
        dataType: 'html',
        beforeSend: function () {
            
           $("#page-name").addClass('hide');
           $("#preloader").removeClass('hide');
           
        },
        success: function (data) {
            
            window.location.hash = hash;
            $("#xcontent").html(data);
            $("#page-name").removeClass('hide');
            $("#preloader").addClass('hide');
            init();
            
        },
        error: function (xhr, status, text) {
            
            $("#page-name").removeClass('hide');
            $("#preloader").addClass('hide');
            
            console.log('>>> '+status+' => '+text+' <<<');

            if (xhr.status === 401) {
                 window.location.href = $('meta[name="base_url"]').attr('content');
            }

            var env = $('meta[name="env"]').attr('content');
             if(env === "production" && xhr.status === 500){
                $("#xcontent").html('Something went terribly wrong!');
            } else {
                $("#xcontent").html(xhr.responseText);
            }
        }
    });
}

/**
 * Loads page when window reload (refresh) of specific state by AJAX GET
 * 
 * @param {string} url
 * @returns {html}
 */

function xReload(url) {
    $.ajax({
        type: 'get',
        url: url,
        dataType: 'html',
        beforeSend: function () {
            
            $("#page-name").addClass('hide');
            $("#preloader").removeClass('hide');
            
        },
        success: function (content) {
            
            $("#xcontent").html(content);
            $("#page-name").removeClass('hide');
            $("#preloader").addClass('hide');
            init();
            
        },
        error: function (xhr, status, text) {
            
            $("#page-name").removeClass('hide');
            $("#preloader").addClass('hide');
            
            console.log('>>> '+status+' => '+text+' <<<');

            if (xhr.status === 401) {
                 window.location.href = $('meta[name="base_url"]').attr('content');
            }
            
            var env = $('meta[name="env"]').attr('content');
             if(env === "production" && xhr.status === 500){
                $("#xcontent").html('Something went terribly wrong!');
            } else {
                $("#xcontent").html(xhr.responseText);
            }
        }
    });

}

/**
 * Loads page when window reload (refresh) of specific state by AJAX GET
 * 
 * @param {string} url
 * @returns {html}
 */

function checkDuplicateClient() {
    $.ajax({
        type: 'post',
        url:  $('meta[name="base_url"]').attr('content')+'/ajax/clients/check-duplicate',
        data: {
            _token : $('meta[name="csrf_token"]').attr('content'),
            last_name : $('#last-name').val(),
            date_of_birth : $('#date-of-birth').val(),
            ni_number : $('#ni-number').val(),
            postcode : $('#post-code').val()
        },
        dataType: 'html',
        beforeSend: function () {
            $("#duplicate-clients").html('<span><i class="fa fa-cog fa-spin"></i> Checking duplicates ...</span>');
        },
        success: function (content) {
            $("#duplicate-clients").html(content);
            $("#check_duplicate").removeClass('hide');
            init();
        },
        error: function (xhr, status, text) {
            
            $("#duplicate-clients").html('');
            
            $("#page-name").removeClass('hide');
            $("#preloader").addClass('hide');
            
            console.log('>>> '+status+' => '+text+' <<<');

            if (xhr.status === 401) {
                 window.location.href = $('meta[name="base_url"]').attr('content');
            }
            
            var env = $('meta[name="env"]').attr('content');
             if(env === "production" && xhr.status === 500){
                $("#xcontent").html('Something went terribly wrong!');
            } else {
                $("#xcontent").html(xhr.responseText);
            }
        }
    });

}


function init() {
    
    
    
    $('[data-toggle="tooltip"]').tooltip();

    $('.data-table').dataTable({
        "bPaginate": true,
        "pagingType": "full",
        "bLengthChange": true,
        "bFilter": true,
       // "bSort": false,
        "bInfo": true,
        "bAutoWidth": false
    });

    tinymce.remove();

//////// tinymce 
tinymce.init({
  selector: 'textarea#editor',
  relative_urls: false,
  remove_script_host: false,
  
  force_br_newlines : true,
    force_p_newlines : false,
    gecko_spellcheck : true,  
   // forced_root_block : '', // Needed for 3.x

    remove_linebreaks : false,

  height: 500,
  theme: 'modern',
  plugins: [
    'advlist autolink lists link image charmap print preview hr anchor pagebreak',
    'searchreplace wordcount visualblocks visualchars code fullscreen',
    'insertdatetime media nonbreaking save table contextmenu directionality',
    'emoticons template paste textcolor colorpicker textpattern imagetools'
  ],
  file_browser_callback : elFinderBrowser,
  toolbar1: 'undo redo | bold italic | code | alignleft aligncenter alignright alignjustify | bullist numlist outdent indent | link image | print preview media | forecolor backcolor emoticons',
  image_advtab: true,
  content_css: [
    '//fonts.googleapis.com/css?family=Lato:300,300i,400,400i',
    '//www.tinymce.com/css/codepen.min.css'
  ]
 });
 
 function elFinderBrowser (field_name, url, type, win) {
  tinymce.activeEditor.windowManager.open({
    file: "http://skeleton.dev/elfinder/tinymce4",// use an absolute path!
    title: "File Manager 2",
    width: 800,
    height: 420,
    resizable: 'yes'
  }, 
  {
    setUrl: function (url) {
      win.document.getElementById(field_name).value = url;
    }
  });
  return false;
}  
    
    
      $(".jdate").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd/mm/yy',
        showAnim: 'slideDown',
        minDate: '-10Y',
        maxDate: "+10Y"
    });
    
    $(".jtoday").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd/mm/yy',
        showAnim: 'slideDown',
        minDate: '-10Y',
        maxDate: new Date()
    });
    
    
    
//
//    $('.min-data-table').dataTable({
//        "bPaginate": true,
//        "pagingType": "full",
//        "bLengthChange": false,
//        "bFilter": false,
//        "bSort": false,
//        "bInfo": true,
//        "bAutoWidth": false
//    });
//
//

//    
//    $(".jnowdate").datepicker({
//        changeMonth: true,
//        changeYear: true,
//        dateFormat: 'dd/mm/yy',
//        showAnim: 'slideDown',
//        minDate: '-5Y',
//        maxDate: new Date()
//    });
//
    $(".jdob").datepicker({
        changeMonth: true,
        changeYear: true,
        dateFormat: 'dd/mm/yy',
        showAnim: 'slideDown',
        minDate: '-95Y',
        maxDate: "-1Y"
    });
//
//    $(".jexpiry").datepicker({
//        changeMonth: true,
//        changeYear: true,
//        dateFormat: 'dd/mm/yy',
//        showAnim: 'slideDown',
//        minDate: '+0D',
//        maxDate: "+15Y"
//    });
//    
//     $(".monthpicker").datepicker({
//        dateFormat: 'MM yy',
//        maxDate: new Date(),
//        changeMonth: true,
//        changeYear: true,
//        showButtonPanel: true,
//        onClose: function(dateText, inst) { 
//            $(this).datepicker('setDate', new Date(inst.selectedYear, inst.selectedMonth, 1));
//            
//        }
//    });
//    
//    $(".scroller").simplyScroll({
//        customClass: 'vert',
//        orientation: 'vertical',
//        auto: true,
//        manualMode: 'loop',
//        frameRate: 20,
//        speed: 2
//    });
//    
//    
//    $("#bank-logo-scroller").simplyScroll({
//        auto: true,
//        manualMode: 'loop',
//        frameRate: 30,
//        speed: 3
//    });

}