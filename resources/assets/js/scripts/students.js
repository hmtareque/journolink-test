$(document).ready(function(){
    
    $(document).on('change', '#student-type', function(){
        
        var type = $(this).val();
        
        if(type === "adult"){
            $('.child-only').hide();
        } else {
            $('.child-only').show();
        }
        
    });
    
     $(document).on('change', '#can-leave-on-own', function(){
        if($(this).val() == 1){
            $('#have-to-collect-by').addClass('hide');
        } else {
            $('#have-to-collect-by').removeClass('hide');
        }
    });
    
      $(document).on('change', '#select-academic-year', function () {
        if ($(this).val() != "") {
            $.ajax({
                type: 'GET',
                url: $('meta[name="base_url"]').attr('content') + '/ajax/education/academic-years/' + $(this).val() + '/terms',
                dataType: 'json',
                success: function (data) {
                    var terms = '<select class="form-control" name="term">';
                    terms += '<option value=""> -- Please Select -- </option>';
                    if (data.length > 0) {
                        $.each(data, function (index, value) {
                            terms += '<option value="' + value.id + '"> ' + value.term + ' - '+value.dates+' </option>';
                        });
                    }
                    terms += '</select>';
                    $('#year-terms').html(terms);
                },
                error: function () {
                }
            });
        } else {
            var terms = '<select class="form-control" name="term">';
            terms += '<option value=""> -- Please Select -- </option>';
            terms += '</select>';
            $('#year-terms').html(terms);
        }
        
        $('#school-classes').html('<span class="label label-info">Please select school.</span>');
        $('#select-school').val('');

    });

    $(document).on('change', '#select-school', function(){
        
       var academic_year = $('#select-academic-year').val();
       if($(this).val() != "" && academic_year != ""){
       $.ajax({
           type : 'GET',
           url : $('meta[name="base_url"]').attr('content') + '/ajax/education/academic-years/'+academic_year+'/schools/'+$(this).val()+'/classes',
           dataType : 'json',
           success: function(data){
               if(data.length > 0){
                   var classes = '<ul class="list-group">';
                   $.each(data, function(index, value){
                        classes += '<li class="list-group-item">'
                                + '<div class="row">'
                                        + '<div class="col-xs-5">'
                                            + '<div class="checkbox">'
                                            + '<label><input type="checkbox" name="classes['+ index +'][class]" value="'+ value.id +'"/> '+value.course+' - '+value.group+' </label>'
                                            + '</div>'
                                        + '</div>'
                                        + '<div class="col-xs-7">'
                                            + '<select class="form-control" name="classes['+ index +'][level]">'
                                            + '<option value="below">Below</option>'
                                            + '<option value="inline">Inline</option>'
                                            + '<option value="above">Above</option>'
                                            + '</select>'
                                            + '<textarea class="form-control margin-top-2" name="classes['+ index +'][note]"></textarea>'
                                        + '</div>'
                                    + '</div>'
                                  + '<div id="enrolled-'+value.id+'-class"><span class="help-block hide"></span></div>'
                                + '</li>';
                   });
                   classes += '</ul>';
                   classes += '';
                   $('#school-classes').html(classes);
               } else {
                   $('#school-classes').html('<span class="label label-warning">No class progressing in this school!</span>');
               }
           },
           error: function(){
           }
       });
        } else {
               $('#school-classes').html('<span class="label label-info">Please select school.</span>');
        }
        
        
        if(academic_year == ""){
           $('#school-classes').html('<span class="label label-warning">Please select academic year.</span>');
       }
        
    });
});


