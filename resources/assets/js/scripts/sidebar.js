$(document).ready(function () {
     $("#sidebar-toggler").on('click', function(){
        $("#sidebar").toggle(250);
    });

    $(".main").on('click', function(){
        toggleSidebar();
    });
    
    $(window).on('resize', function(){
       toggleSidebar();
    });
    
    $(document).on('click', 'aside > ul > li > a.nav-parent', function(){
        
        $('#page-name').html($(this).children('span').html());

        $('aside > ul > li > a').children('i').removeClass('fa-minus');
        $('aside > ul > li > a').children('i').addClass('fa-plus');
        $('aside > ul ul').slideUp(300);
        
        $('aside > ul > li > a').addClass('nav-parent');
        $('aside > ul > li > a').removeClass('nav-parent-expanded');
         
        $(this).removeClass('nav-parent');
        $(this).addClass('nav-parent-expanded');
        
        $(this).siblings('ul').slideDown(300);
        $(this).children('i').removeClass('fa-plus');
        $(this).children('i').addClass('fa-minus');
        
      //  return false;

    });
    
    
    $(document).on('click', 'aside > ul > li > a.nav-parent-expanded', function(){
        
        $(this).removeClass('nav-parent-expanded');
        $(this).addClass('nav-parent');
     
        $(this).siblings('ul').slideUp(300);
        $(this).children('i').removeClass('fa-minus');
        $(this).children('i').addClass('fa-plus');
        
        return false;

    });

     $(document).on('click', 'aside ul li a', function(){
        $('aside ul li a').removeClass('active');
        $(this).addClass('active');
        
        if ($(this).siblings('ul').size() === 0) { 
            toggleSidebar();
        } 
        
       // if(!$(this).hasClass('nav-parent-expanded') && !$(this).hasClass('nav-parent')){
            
     //   }
        
        
    });
    
    
    
    
});  
    
function toggleSidebar() {
    var screen = $(window);
    if (screen.width() <= 752) {
        $("#sidebar").hide(250);
    } else {
        $("#sidebar").show(250);
    }
}


 
 
 

