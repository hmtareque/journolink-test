<div class="header">
    <div class="title"><i class="fa fa-lg fa-plus-square-o fa-fw"></i> {{trans('label.create_new_item', array('item' => trans('label.web_settings')))}}</div>
    <div class="links">
        @can("read", 4)
        <a href="#web/settings" class="btn btn-success btn-sm"><i class="fa fa-list"></i> {{trans('label.list_of_items', array('item' => trans('label.web_settings')))}}</a>
        @endcan

        @can("update", 4)
        <a href="#web/settings/edit" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> {{trans('label.edit_item', array('item' => trans('label.web_settings')))}}</a>
        @endcan
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-6">

        <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->

        <form action="{{url('manage/web/settings/store')}}" id="create-settings-form" class="form-horizontal ajax-form" novalidate="novalidate" method="post" data-hash="web/settings">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>

            <div class="form-group" id="parameter">
                <label class="col-sm-3 control-label">{{trans('label.parameter')}}</label>
                <div class="col-sm-9">
                    <input type="text" name="parameter" class="form-control" placeholder="{{trans('label.parameter')}}">

                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="type">
                <label class="col-sm-3 control-label">{{trans('label.type')}}</label>
                <div class="col-sm-9">

                    <select name="type" id="select-settings-option" class="form-control">
                        <option value="">{{trans('label.please_select')}}</option>
                        <option value="text">Text</option>
                        <option value="option">Option</option>
                        <option value="image">Image</option>
                    </select>               
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            <div class="form-group hide" id="setting_options">
                <label class="col-sm-3 control-label">{{trans('label.options')}}</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="setting_options" placeholder="{{trans('label.options')}}">
                    <span class="help-block hide"></span>
                    <div><small class="label label-danger">{{trans('label.options_must_be_separated_by_comma')}}</small></div>

                </div>
            </div>

            <div class="form-group hide" id="setting_value">
                <label class="col-sm-3 control-label">{{trans('label.value')}}</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="setting_value" placeholder="{{trans('label.value')}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group hide" id="setting_image">
                <label class="col-sm-3 control-label">{{trans('label.image')}}</label>
                <div class="col-sm-9">
                    <div id="image-setting_image-preview"></div>
                    <label>
                        <input class="btn btn-default btn-sm preview-file" data-item="setting_image" id="item-setting_image-file" type="file" name="setting_image" />
                    </label>
                    <span class="help-block hide"></span>
                    <div>
                        <a href="javascript:void(0);" class="btn btn-danger btn-xs component-image-remover hide" data-item="setting_image" id="image-setting_image-remover">{{trans('action.remove')}}</a>
                    </div>
                </div>
            </div>

            

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-primary btn-sm"> 
                        <i class="fa fa-save fa-fw"></i>&nbsp;{{trans('action.save')}}
                    </button>
                </div>
            </div>           

        </form>
    </div>
</div>

