<div class="header">
    <div class="title"><i class="fa fa-lg fa-edit fa-fw"></i> {{trans('label.edit_item', array('item' => trans('label.web_settings')))}}</div>
    <div class="links">
        @can("create", 4)
        <a href="#web/settings/create" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> {{trans('label.create_new_item', array('item' => trans('label.settings')))}}</a>
        @endcan
        
        @can("read", 4)
        <a href="#web/settings" class="btn btn-primary btn-sm"><i class="fa fa-list"></i> {{trans('label.list_of_items', array('item' => trans('label.web_settings')))}}</a>
        @endcan
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-10">

        <!-- Notify Area -->
        @include('app.shared.flash')
        <!-- Notfiy Area End -->

        @if($settings->count() > 0)


        <form action="{{url('manage/web/settings')}}" id="app-settings-form" class="form-horizontal ajax-form" novalidate="novalidate" method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <input type="hidden" name="_method" value="put"/>

            @foreach($settings as $setting)

            @if($setting->type == 'text')
            <div class="form-group" id="{{$setting->param}}">
                <label class="col-sm-3 control-label">{{str_replace('_', ' ', title_case($setting->param))}}</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="{{$setting->param}}" value="{{$setting->value}}"/>
                    <span class="help-block hide"></span>
                </div>
            </div>
            @endif

            @if($setting->type == "option") 
            <div class="form-group" id="{{$setting->param}}">
                <label class="col-sm-3 control-label">{{str_replace('_', ' ', title_case($setting->param))}}</label>
                <div class="col-sm-9">
                    <select name="{{$setting->param}}" id="select-settings-option" class="form-control">
                        <?php $options = explode(",", $setting->options) ?>
                        @if(count($options)>0)
                        @foreach($options as $option)
                        <option value="{{$option}}" @if($option == $setting->value) selected="" @endif>{{$option}}</option>

                        @endforeach 
                        @endif 
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>
            @endif 

            @if($setting->type == "image") 
            <div class="form-group" id="{{$setting->param}}">
                <label class="col-sm-3 control-label">{{str_replace('_', ' ', title_case($setting->param))}}</label>
                <div class="col-sm-9">
                    <div id="image-{{$setting->param}}-preview">
                        @if($setting->value)
                        <img style="width: 64px; height: auto; margin-bottom: 5px;" alt="64x64" src="{{asset($setting->value)}}"/>
                        @endif
                    </div>
                    <label>
                        <input class="btn btn-default btn-sm preview-file" data-item="{{$setting->param}}" id="item-{{$setting->param}}-file" type="file" name="{{$setting->param}}" />
                        <input id="actual-{{$setting->param}}-image" type="hidden" name="{{$setting->param}}" value="{{$setting->value}}" data-src="{{asset($setting->value)}}"/>
                    </label>
                    <span class="help-block hide"></span>
                    <div>
                        <a href="javascript:void(0);" class="btn btn-danger btn-xs component-image-remover @if(!$setting->value) hide @endif" data-item="{{$setting->param}}" id="image-{{$setting->param}}-remover">{{trans('action.remove')}}</a>
                        <a href="javascript:void(0);" class="btn btn-success btn-xs component-image-reloader hide" data-item="{{$setting->param}}" id="image-{{$setting->param}}-reloader">{{trans('action.reload')}}</a>
                    </div>
                </div>
            </div>
            @endif

            @endforeach 

            @can("update", 3) 
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-primary btn-sm"> 
                        <i class="fa fa-refresh fa-fw"></i> {{trans('action.update')}}
                    </button>
                </div>
            </div>
            @endcan
            
        </form>
    </div>

    @endif 

</div>
</div>

