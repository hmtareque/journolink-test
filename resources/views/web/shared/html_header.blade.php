<title>Web</title>
        <!-- Meta -->
        <meta charset="utf-8')}}">
        <meta name="viewport" content="width=device-width, initial-scale=1.0')}}">
        <meta name="description" content="">
        <meta name="author" content="">

        <!-- Favicon -->
        <link rel="shortcut icon" href="favicon.icon">

        <!-- Web Fonts -->
        <link rel='stylesheet' type='text/css' href='//fonts.googleapis.com/css?family=Open+Sans:400,300,600&amp;subset=cyrillic,latin'>

        <!-- CSS Global Compulsory -->
        <link rel="stylesheet" href="{{asset('web/plugins/bootstrap/css/bootstrap.min.css')}}">
        <link rel="stylesheet" href="{{asset('web/css/style.css')}}">

        <!-- CSS Header and Footer -->
        <link rel="stylesheet" href="{{asset('web/css/headers/header-v6.css')}}">
        <link rel="stylesheet" href="{{asset('web/css/footers/footer-v1.css')}}">

        <!-- CSS Implementing Plugins -->
        <link rel="stylesheet" href="{{asset('web/plugins/animate.css')}}">
        <link rel="stylesheet" href="{{asset('web/plugins/line-icons/line-icons.css')}}">
        <link rel="stylesheet" href="{{asset('web/plugins/font-awesome/css/font-awesome.min.css')}}">
        <link rel="stylesheet" href="{{asset('web/plugins/fancybox/source/jquery.fancybox.css')}}">
	<link rel="stylesheet" href="{{asset('web/plugins/owl-carousel/owl-carousel/owl.carousel.css')}}">
	<link rel="stylesheet" href="{{asset('web/plugins/master-slider/masterslider/style/masterslider.css')}}">
	<link rel='stylesheet' href="{{asset('web/plugins/master-slider/masterslider/skins/black-2/style.css')}}">
        
        <!-- CSS Page Style -->
	<link rel="stylesheet" href="{{asset('web/css/pages/page_search_inner.css')}}">
        <link rel="stylesheet" href="{{asset('web/css/pages/page_contact.css')}}">
        

        <!-- CSS Theme -->
        <link rel="stylesheet" href="{{asset('web/css/theme-colors/blue.css')}}">
        

        <!-- CSS Customization -->
        <link rel="stylesheet" href="{{asset('web/css/custom.css')}}">