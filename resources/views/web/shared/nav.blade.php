<div class="header-v6 header-classic-white header-sticky">
    <!-- Navbar -->
    <div class="navbar mega-menu" role="navigation">
        <div class="container">
            <!-- Brand and toggle get grouped for better mobile display -->
            <div class="menu-container">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>

                <!-- Navbar Brand -->
                <div class="navbar-brand">
                    <a href="{{url('/')}}">
                        <img class="shrink-logo" src="{{asset(web::logo())}}" alt="Logo">
                    </a>
                </div>
                <!-- ENd Navbar Brand -->
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-responsive-collapse">
                <div class="menu-container">
                    <ul class="nav navbar-nav">
                        @if(count(web::links('top-menu'))>0)
                        @foreach(web::links('top-menu') as $item)
                        @if($item->menu == 1) 
                        <li class="dropdown">
                            <a href="#" class="dropdown-toggle" data-toggle="dropdown">{{$item->name}} <span class="caret"></span></a>
                            @if(count($item->links)>0) 
                            <ul class="dropdown-menu">
                                @foreach($item->links as $link)
                                <li><a href="{{$link->link}}">{{$link->name}}</a></li>
                                @endforeach 
                            </ul>
                            @endif 
                        </li>
                        @else 
                        <li><a href="{{$item->link}}">{{$item->name}}</a></li>
                        @endif 
                        @endforeach	
                        @endif
                    </ul>
                </div>
            </div><!--/navbar-collapse-->
        </div>
    </div>
    <!-- End Navbar -->
</div>
