 <div class="footer-v1">
                <div class="footer" style="background: #dff1ff;">
                    <div class="container">
                        <div class="row">
                            <!-- About -->
                            <div class="col-md-4 md-margin-bottom-10">
                                <a href="index.html"><img style="width: 200px; height: auto;" src="{{asset(''.web::logo())}}" alt=""></a>
                                @if(web::component("about-us"))<p style="color: #000; margin-top: 5px;">{{web::component("about-us")->text}}</p>@endif
                            </div><!--/col-md-3-->
                            <!-- End About -->

                            <!-- Address -->
                            <div class="col-md-3 md-margin-bottom-10" style="color: #000;">
                                <div class="headline" style="margin-top: 0px; padding-top: 5px;"><h2 style="color: #255ABC;">Find Us</h2></div>
                                <ul class="list-unstyled">
                                    @if(web::settings("address"))  <li>{{web::settings("address")}}</li> @endif
                                    @if(web::settings("street"))  <li>{{web::settings("street")}}</li> @endif
                                    @if(web::settings("city"))  <li>{{web::settings("city")}}</li> @endif
                                    @if(web::settings("postcode"))  <li>{{web::settings("postcode")}}</li> @endif
                                    @if(web::settings("email"))  <li><i class="fa fa-envelope-o"></i>&nbsp;<a href="mailto:{{web::settings("email")}}">{{web::settings("email")}}</a></li> @endif
                                    @if(web::settings("phone"))  <li><i class="fa fa-phone"></i>&nbsp;{{web::settings("phone")}}</li> @endif
                                    @if(web::settings("fax"))  <li><i class="fa fa-fax"></i>&nbsp;{{web::settings("fax")}}</li> @endif
                                </ul>
                            </div><!--/col-md-3-->
                            <!-- End Address -->

                            <!-- Link List -->
                            <div class="col-md-5 md-margin-bottom-10">
                                
                                @if(web::components('accreditations'))
                                <div class="headline" style="margin-top: 0px; padding-top: 5px;"><h2 style="color: #255ABC;">Accreditations</h2></div>
                                <ul class="list-unstyled list-inline">
                                    @foreach(web::components('accreditations') as $accreditation)
                                <li><a href="#"><img style="max-width: 100px; height: auto;" src="{{asset(''.$accreditation->logo)}}" alt="{{$accreditation->title}}"/></a></li>
                                    
                                @endforeach 
                                   
                                </ul>
                                
                                @endif 
                                
                                
                                
                                @if(web::components('social-network-links'))
                               <div class="headline"><h2 style="color: #255ABC;">Follow Us</h2></div>
                                <ul class="list-unstyled list-inline">
                                    @foreach(web::components('social-network-links') as $social)
                                <li><a href="#"><img style="max-width: 50px; height: auto;" src="{{asset(''.$social->logo)}}" alt="{{$social->name}}"/></a></li>
                                    
                                @endforeach 
                                   
                                </ul>
                                
                                @endif
                                
                               
                                
                                
                               
                                
                                
                            </div><!--/col-md-3-->
                            <!-- End Link List -->

                            
                        </div>
                    </div>
                </div><!--/footer-->

                <div class="copyright" style="background: #fff;">
                    <div class="container" style="background: #fff;">
                        <div class="row">
                            <div class="col-md-6">
                                <p style="color: #000;">
                                    2015 &copy; All Rights Reserved.
                                    
                                    @if(count(web::links('footer-menu'))>0)
                        @foreach(web::links('footer-menu') as $item)
                        @if($item->menu != 1) 
                         
                        <a href="{{$item->link}}">{{$item->name}}</a> &nbsp;
                        @endif 
                        @endforeach	
                        @endif
                                    
                                </p>
                            </div>

                            <!-- Social Links -->
                            <div class="col-md-6">
                                <span class="pull-right" style="line-height: 100%; color: #255ABC;">Powered By <a href="http://www.exetie.com" title="Exetie Systems"><img src="{{asset('images/exetie.png')}}"/></a></span>
                            </div>
                            <!-- End Social Links -->
                        </div>
                    </div>
                </div><!--/copyright-->
            </div>
