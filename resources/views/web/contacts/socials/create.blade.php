<!-- Notify Area -->
@include('manage/shared/notify') 
<!-- Notfiy Area End -->

<div class="row">
    <div class="col-xs-12">
        <h5 class="txt-color-blueDark">
            <strong><i class="fa fa-file-text-o fa-fw"></i> {{trans('label.posts')}}</strong>
            <a href="#posts" class="btn btn-default btn-xs btn-circle txt-color-green" rel="tooltip" data-placement="top" data-original-title="{{trans('label.list_of_items', array('item' => 'Posts'))}}"><i class="fa fa-list"></i></a>
            <div class="btn-group">
                <button class="btn btn-default btn-xs btn-circle dropdown-toggle" data-toggle="dropdown">
                    <span class="caret"></span>
                </button>
                <ul class="dropdown-menu">
                    <li><a href="#post-categories"><i class="fa fa-list"></i> {{trans('label.list_of_items', array('item' => 'Post Categories'))}}</a></li>
                    <li><a href="#post-categories/create"><i class="fa fa-plus"></i> {{trans('label.create_new_item', array('item' => 'Post Categories'))}}</a></li>
                </ul>
            </div>
        </h5>
    </div>
</div>

<!-- widget grid -->
<section id="widget-grid" class="">
    <!-- START ROW -->
    <div class="row">
        <!-- NEW COL START -->
        <article class="col-xs-12">
            <div class="jarviswidget jarviswidget-color-blueDark"  id="wid-id-1" data-widget-editbutton="false" data-widget-custombutton="false" data-widget-deletebutton="false">
                <header>
                    <span class="widget-icon"> <i class="fa fa-plus"></i> </span>
                    <h2>{{trans('label.new_item', array('item' => 'Post'))}}</h2>				
                </header>
                <!-- widget div-->
                <div>
                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->
                    </div>
                    <!-- end widget edit box -->

                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        <form action="{{url('manage/posts')}}" id="post-form" class="smart-form ajax-form" novalidate="novalidate" method="post" name="posts">
                            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                            
                            <fieldset>
                                <div class="row">
                                <section class="col col-6">
                                        <label class="select">
                                            <select name="post_category">
                                                <option value="0" selected="" disabled="">{{trans('label.category')}}</option>
                                                @foreach(show::roles() as $role)
                                                <option value="{{$role->id}}">{{$role->name}}</option>
                                                @endforeach 
                                            </select> <i></i> 
                                        </label>
                                    </section>
                                </div>
                                <section id="title">
                                    <label for="title" class="input">
                                        <input type="text" name="title" placeholder="Title">
                                    </label>
                                    <span class="error"></span>
                                </section>
                                <section>
                                    <label for="meta_keywords" class="textarea">
                                        <textarea rows="3" name="meta_keywords" placeholder="Meta Keywords"></textarea> 
                                    </label>
                                </section>
                                <section>
                                    <label for="meta_description" class="textarea">
                                       <textarea rows="3" name="meta_description" placeholder="Meta Description"></textarea> 
                                    </label>
                                </section>
                                <section class="content">
                                    <label class="textarea">
                                            <textarea rows="5" name="content" placeholder="Content" id="editor"></textarea> 
                                    </label>
                                    <span class="error"></span>
                                </section>
                                
                                <div class="row">
                                <section class="col col-6">
                                        <label class="checkbox">
                                            <input type="checkbox" name="do_not_publish">
                                            <i></i>{{trans('label.do_not_publish')}}
                                        </label>
                                    </section>
                                </div>
                            </fieldset>
                            <footer>
                                <button type="submit" class="btn btn-primary">
                                    <i class="fa fa-save"></i>&nbsp;{{trans('action.save')}}
                                </button>
                            </footer>
                        </form>

                    </div>
                    <!-- end widget content -->
                </div>
                <!-- end widget div -->
            </div>
            <!-- end widget -->
        </article>
        <!-- END COL -->
    </div>
    <!-- END ROW -->
</section>

		

		