<div class="header" style="float: left; width: 100%;">
    <div class="title"><i class="fa fa-lg fa-list-alt fa-fw"></i> {{trans('label.list_of_items', array('item' => trans('label.navs')))}}</div>
</div>

<!-- Notify Area -->
@include('app.shared.flash') 
<!-- Notfiy Area End -->



<div class="row">
    <div class="col-xs-12 col-md-6">
        
        <h4 style="border-bottom: 1px solid #00b3ee; margin: 0px; padding: 7px 0px; font-weight: bold; margin-bottom: 15px;">{{trans('label.address')}}</h4>
        
        <form action="{{url('manage/web/contacts')}}" id="contacts-update-form" class="form-horizontal ajax-form" novalidate="novalidate" method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <input type="hidden" name="_method" value="put"/>

            <div class="form-group" id="business_name">
                <label class="col-sm-2 control-label">{{trans('label.business_name')}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="name" value="">
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            <div class="form-group" id="name">
                <label class="col-sm-2 control-label">{{trans('label.address_line_1')}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="name" value="">
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            <div class="form-group" id="name">
                <label class="col-sm-2 control-label">{{trans('label.address_line_2')}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="name" value="">
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            <div class="form-group" id="name">
                <label class="col-sm-2 control-label">{{trans('label.street')}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="name" value="">
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            
            <div class="form-group" id="name">
                <label class="col-sm-2 control-label">{{trans('label.city')}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="name" value="">
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            <div class="form-group" id="name">
                <label class="col-sm-2 control-label">{{trans('label.postcode')}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="name" value="">
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            <div class="form-group" id="name">
                <label class="col-sm-2 control-label">{{trans('label.country')}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="name" value="">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary btn-sm"> 
                        <i class="fa fa-refresh fa-fw"></i>&nbsp;{{trans('action.update')}}
                    </button>
                </div>
            </div>
        </form>
    </div>
    
    
    
    <div class="col-xs-12 col-md-6">
        
        
        <h4 style="border-bottom: 1px solid #00b3ee; margin: 0px; padding: 5px 0px; font-weight: bold; margin-bottom: 15px;">{{trans('label.address')}}</h4>
         <form action="{{url('manage/web/navs/')}}" id="nav-form" class="form-horizontal ajax-form" novalidate="novalidate" method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <input type="hidden" name="_method" value="put"/>
        
        <div class="form-group" id="name">
                <label class="col-sm-2 control-label">{{trans('label.email')}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="name" value="">
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            <div class="form-group" id="name">
                <label class="col-sm-2 control-label">{{trans('label.phone')}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="name" value="">
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            <div class="form-group" id="name">
                <label class="col-sm-2 control-label">{{trans('label.mobile')}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="name" value="">
                    <span class="help-block hide"></span>
                </div>
            </div>
            

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary btn-sm"> 
                        <i class="fa fa-refresh fa-fw"></i>&nbsp;{{trans('action.update')}}
                    </button>
                </div>
            </div>
        </form>
        
        
        
        
        <h4 style="border-bottom: 1px solid #00b3ee; margin: 0px; padding: 5px 0px; font-weight: bold; margin-bottom: 15px;">{{trans('label.address')}}</h4>
         
        <form action="{{url('manage/web/navs/')}}" id="nav-form" class="form-horizontal ajax-form" novalidate="novalidate" method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <input type="hidden" name="_method" value="put"/>

            <div class="form-group" id="name">
                <label class="col-sm-2 control-label">{{trans('label.facebook')}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="name" value="">
                    <span class="name-help-block help-block hide"></span>
                </div>
            </div>
            
            <div class="form-group" id="name">
                <label class="col-sm-2 control-label">{{trans('label.twitter')}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="name" value="">
                    <span class="name-help-block help-block hide"></span>
                </div>
            </div>
            
            <div class="form-group" id="name">
                <label class="col-sm-2 control-label">{{trans('label.youtube')}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="name" value="">
                    <span class="name-help-block help-block hide"></span>
                </div>
            </div>
            
            <div class="form-group" id="name">
                <label class="col-sm-2 control-label">{{trans('label.linkedin')}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="name" value="">
                    <span class="name-help-block help-block hide"></span>
                </div>
            </div>
            
            <div class="form-group" id="name">
                <label class="col-sm-2 control-label">{{trans('label.google_plus')}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="name" value="">
                    <span class="name-help-block help-block hide"></span>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary btn-sm"> 
                        <i class="fa fa-refresh fa-fw"></i>&nbsp;{{trans('action.update')}}
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>



<!-- widget grid -->
<section id="widget-grid" class="">
    <!-- row -->
    <div class="row">
        <!-- NEW WIDGET START -->
        <article class="col-xs-12 col-sm-12 col-md-12 col-lg-12">
            <!-- Widget ID (each widget will need unique ID)-->
            <div class="jarviswidget jarviswidget-color-blueDark" id="contacts-list-table" data-widget-editbutton="false" data-widget-deletebutton="false">
               

                <!-- widget div-->
                <div>
                    <!-- widget edit box -->
                    <div class="jarviswidget-editbox">
                        <!-- This area used as dropdown edit box -->
                    </div>
                    <!-- end widget edit box -->

                    @if($contacts->count() > 0)
                    <!-- widget content -->
                    <div class="widget-body no-padding">
                        
                        <table id="datatable_col_reorder" class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                                <tr>
                                    <th data-class="expand" width="65%">{{trans('label.title')}}</th>
                                    <th data-hide="phone,tablet" width="5%">{{trans('label.published')}}</th>
                                    <th data-hide="phone,tablet" width="15%">{{trans('label.created_at')}}</th>
                                    <th data-hide="phone,tablet" width="15%">{{trans('label.action')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($contacts as $contact) 
                                <tr>
                                    <td>{{$contact->title}}</td>
                                    <td class="text-center">
                                        @if($contact->published == 1)
                                        <i class="fa fa-check txt-color-green"></i>
                                        @else 
                                        <i class="fa fa-close txt-color-red"></i>
                                        @endif 
                                    </td>
                                    <td>{{date('d M Y H:i A', strtotime($contact->created_at))}}</td>
                                    <td>
                                        <a href="#contacts/{{$contact->id}}/edit" class="btn btn-default btn-xs btn-circle txt-color-blue" rel="tooltip" data-placement="top" data-original-title="{{trans('label.edit_item', array('item' => 'Post'))}}"><i class="fa fa-pencil"></i></a>

                                        @if($contact->published == 1)

                                        <button class="btn btn-default btn-xs btn-circle txt-color-red" data-toggle="modal" data-target="#unpublish-contact-{{$contact->id}}">
                                            <i class="fa fa-unlink"></i>
                                        </button>
                                        <!-- Modal -->
                                        <div class="modal fade" id="unpublish-contact-{{$contact->id}}" tabindex="-1" role="dialog" aria-labelledby="unpublishModal" aria-hidden="true" data-backdrop="static">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <form class="ajax-form" action="{{url('manage/contacts/'.$contact->id.'/unpublish')}}" method="contact">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                                &times;
                                                            </button>
                                                            <h4 class="modal-title" id="myModalLabel">{{trans('label.unpublish_item', array('item' => 'Post'))}}</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>{{trans('alert.sure_to_unpublish_item', array('item' => $contact->title))}}</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                                                {{trans('action.cancel')}}
                                                            </button>
                                                            <button type="submit" class="btn btn-primary btn-danger">
                                                                <i class="fa fa-unlink"></i>&nbsp; {{trans('action.unpublish')}}
                                                            </button>
                                                        </div>
                                                    </form> 
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div><!-- /.modal -->

                                        @else 

                                        <button class="btn btn-default btn-xs btn-circle txt-color-greenLight" data-toggle="modal" data-target="#publish-contact-{{$contact->id}}">
                                            <i class="fa fa-link"></i>
                                        </button>
                                        <!-- Modal -->
                                        <div class="modal fade" id="publish-contact-{{$contact->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <form class="ajax-form" action="{{url('manage/contacts/'.$contact->id.'/publish')}}" method="contact">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                                &times;
                                                            </button>
                                                            <h4 class="modal-title" id="myModalLabel">{{trans('label.publish_item', array('item' => 'Post'))}}</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>{{trans('alert.sure_to_publish_item', array('item' => $contact->title))}}</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default" data-dismiss="modal">
                                                                {{trans('action.cancel')}}
                                                            </button>
                                                            <button type="submit" class="btn btn-primary btn-success">
                                                                <i class="fa fa-link"></i>&nbsp; {{trans('action.publish')}}
                                                            </button>
                                                        </div>
                                                    </form> 
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div><!-- /.modal -->
                                        @endif 
                                    </td>
                                </tr>
                                @endforeach 
                            </tbody>
                        </table>
                    </div>
                    
                    @else 
                    
                    <div class="widget-body">
                        <p class="alert alert-info"><i class="fa fa-info"></i>&nbsp;{{trans('alert.no_item_found', array('item' => 'contact'))}}</p>
                    </div>
                    @endif
                    <!-- end widget content -->
                </div>
                <!-- end widget div -->
            </div>
            <!-- end widget -->
        </article>
        <!-- WIDGET END -->
    </div>
    <!-- end row -->
</section>
<!-- end widget grid -->




