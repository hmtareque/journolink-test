<div class="header">
    <div class="title"><i class="fa fa-lg fa-list-alt fa-fw"></i> {{trans('label.list_of_items', array('item' => trans('label.navs')))}}</div>
</div>


<div class="row">
    <div class="col-xs-12 col-md-7">
        
      <!-- Notify Area -->
@include('app.shared.flash') 
<!-- Notfiy Area End -->
        
        <form action="{{url('manage/web/address/update')}}" id="contacts-update-form" class="form-horizontal ajax-form" novalidate="novalidate" method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
           

            <div class="form-group" id="business_name">
                <label class="col-sm-2 control-label">{{trans('label.business_name')}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="business_name" placeholder="" value="">
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            <div class="form-group" id="name">
                <label class="col-sm-2 control-label">{{trans('label.address_line_1')}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="address_line_1" value="">
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            <div class="form-group" id="name">
                <label class="col-sm-2 control-label">{{trans('label.address_line_2')}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="name" value="">
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            <div class="form-group" id="name">
                <label class="col-sm-2 control-label">{{trans('label.street')}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="name" value="">
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            
            <div class="form-group" id="name">
                <label class="col-sm-2 control-label">{{trans('label.city')}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="name" value="">
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            <div class="form-group" id="name">
                <label class="col-sm-2 control-label">{{trans('label.postcode')}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="name" value="">
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            <div class="form-group" id="name">
                <label class="col-sm-2 control-label">{{trans('label.country')}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="name" value="">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary btn-sm"> 
                        <i class="fa fa-refresh fa-fw"></i>&nbsp;{{trans('action.update')}}
                    </button>
                </div>
            </div>
        </form>
    </div>
    
    
    
   
</div>








