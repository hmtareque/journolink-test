<div class="header">
    <div class="title"><i class="fa fa-lg fa-edit fa-fw"></i> {{trans('label.edit_item', array('item' => trans('label.contacts')))}}</div>

    <div class="links">
        @can("read", 4)
        <a href="#web/contacts" class="btn btn-success btn-sm"><i class="fa fa-info-circle fa-fw"></i> {{trans('label.contacts')}}</a>
        @endcan
    </div>
</div>


<div class="row">
    <div class="col-xs-12 col-md-8">

        <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->
        
        <form action="{{url('manage/web/contacts/update')}}" id="contacts-update-form" class="form-horizontal ajax-form" novalidate="novalidate" method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>


            <div class="form-group" id="business_name">
                <label class="col-sm-3 control-label">{{trans('label.business_name')}}</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="business_name" placeholder="{{trans('label.business_name')}}" value="{{$contacts->business_name}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="address_line_1">
                <label class="col-sm-3 control-label">{{trans('label.address_line_1')}}</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="address_line_1" placeholder="{{trans('label.address_line_1')}}" value="{{$contacts->address_line_1}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="address_line_2">
                <label class="col-sm-3 control-label">{{trans('label.address_line_2')}}</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="address_line_2" placeholder="{{trans('label.address_line_2')}}" value="{{$contacts->address_line_2}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="street">
                <label class="col-sm-3 control-label">{{trans('label.street')}}</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="street" placeholder="{{trans('label.street')}}" value="{{$contacts->street}}">
                    <span class="help-block hide"></span>
                </div>
            </div>


            <div class="form-group" id="city">
                <label class="col-sm-3 control-label">{{trans('label.city')}}</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="city" placeholder="{{trans('label.city')}}" value="{{$contacts->city}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="postcode">
                <label class="col-sm-3 control-label">{{trans('label.postcode')}}</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="postcode" placeholder="{{trans('label.postcode')}}" value="{{$contacts->postcode}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="county">
                <label class="col-sm-3 control-label">{{trans('label.county')}}</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="county" placeholder="{{trans('label.county')}}" value="{{$contacts->county}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="country">
                <label class="col-sm-3 control-label">{{trans('label.country')}}</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="country" placeholder="{{trans('label.country')}}" value="{{$contacts->country}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="email">
                <label class="col-sm-3 control-label">{{trans('label.email')}}</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="email" placeholder="{{trans('label.email')}}" value="{{$contacts->email}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="phone">
                <label class="col-sm-3 control-label">{{trans('label.phone')}}</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="phone" placeholder="{{trans('label.phone')}}" value="{{$contacts->phone}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="fax">
                <label class="col-sm-3 control-label">{{trans('label.fax')}}</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="fax" placeholder="{{trans('label.fax')}}" value="{{$contacts->fax}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="name">
                <label class="col-sm-3 control-label">{{trans('label.mobile')}}</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="mobile" placeholder="{{trans('label.mobile')}}" value="{{$contacts->mobile}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-primary btn-sm"> 
                        <i class="fa fa-refresh fa-fw"></i>&nbsp;{{trans('action.update')}}
                    </button>
                </div>
            </div>
        </form>
    </div>

</div>








