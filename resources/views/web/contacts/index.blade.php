<div class="header">
    <div class="title"><i class="fa fa-lg fa-map-signs fa-fw"></i> {{trans('label.contacts')}}</div>

    <div class="links">
        @can("update", 4)
        <a href="#web/contacts/update" class="btn btn-primary btn-sm"><i class="fa fa-edit fa-fw"></i> {{trans('label.edit_item', array('item' => trans('label.contacts')))}}</a>
        @endcan
    </div>

</div>

<div class="row">
    <div class="col-xs-12">
        <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-6">

        <h4 class="section-title">{{trans('label.address')}}</h4>

        <address>
            @if($contacts->business_name != "") {{$contacts->business_name}} <br/> @endif
            @if($contacts->address_line_1 != "") {{$contacts->address_line_1}} <br/> @endif
            @if($contacts->address_line_2 != "") {{$contacts->address_line_2}} <br/> @endif
            @if($contacts->street != "") {{$contacts->street}} <br/> @endif
            @if($contacts->city != "") {{$contacts->city}} <br/> @endif
            @if($contacts->postcode != "") {{$contacts->postcode}} <br/> @endif
            @if($contacts->county != "") {{$contacts->county}} <br/> @endif
            @if($contacts->country != "") {{$contacts->country}} @endif
        </address>


    </div>

    <div class="col-xs-12 col-md-6">

        <h4 class="section-title">{{trans('label.contacts')}}</h4>

        <ul class="list-unstyled">
            @if($contacts->email != "") <li>{{trans('label.email')}} : <strong>{{$contacts->email}}</strong></li> @endif
            @if($contacts->phone != "") <li>{{trans('label.phone')}} : <strong>{{$contacts->phone}}</strong></li> @endif
            @if($contacts->fax != "") <li>{{trans('label.fax')}} : <strong>{{$contacts->fax}}</strong></li> @endif
            @if($contacts->mobile != "") <li>{{trans('label.mobile')}} : <strong>{{$contacts->mobile}}</strong></li> @endif
        </ul>


    </div>

</div>



<div class="row">
    <div class="col-xs-12">

        <h4 class="section-title">{{trans('label.web_contacts')}}</h4>

        @if($web_contacts->count() > 0)
        <!-- widget content -->
        <div class="table-responsive">

            <table class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                    <tr>
                        <th>{{trans('label.name')}}</th>
                        <th>{{trans('label.email')}}</th>
                        <th>{{trans('label.subject')}}</th>
                        <th>{{trans('label.sent_at')}}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($web_contacts as $msg) 
                    <tr>
                        <td>{{$msg->name}}</td>
                        <td>{{$msg->email}}</td>

                        <td>
                            {{$msg->subject}}
                            <button class="btn btn-success btn-xs pull-right" data-toggle="modal" data-target="#contact-msg-{{$msg->id}}">
                                <i class="fa fa-comment"></i> {{trans('label.message')}}
                            </button>
                            <!-- Modal -->
                            <div class="modal fade" id="contact-msg-{{$msg->id}}" tabindex="-1" role="dialog" aria-labelledby="unpublishModal" aria-hidden="true" data-backdrop="static">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                &times;
                                            </button>
                                            <h4 class="modal-title" id="myModalLabel">{{trans('label.message')}}</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>{{$msg->message}}</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
                                                {{trans('action.cancel')}}
                                            </button>

                                        </div>
                                        </form> 
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->


                        </td>
                        <td>{{date('d M Y H:i A', strtotime($msg->created_at))}}</td>
                    </tr>
                    @endforeach 
                </tbody>
            </table>
        </div>

        @else 

        <p class="alert alert-info">
            <i class="fa fa-info"></i>&nbsp;{{trans('alert.no_item_found', array('item' => 'contact'))}}
        </p>

        @endif
        <!-- end widget content -->

    </div>
</div>


