<div class="header">
    <div class="title"><i class="fa fa-lg fa-list-alt fa-fw"></i> {{trans('label.create_new_item', array('item' => (isset($nav))? $nav->name.' '.trans('label.link') : trans('label.link')))}}</div>
    <div class="links">
        @can("read", 4)
        <a href="#web/navs/{{$nav->id}}/links" class="btn btn-primary btn-sm"><i class="fa fa-list"></i> {{trans('label.list_of_items', array('item' => $nav->name.' '.trans('label.link')))}}</a>
        @endcan

        @if(Auth::user()->can("read", 4) || Auth::user()->can("create", 4))
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{trans('label.navs')}} <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#web/navs"><i class="fa fa-list"></i> {{trans('label.list_of_items', array('item' => trans('label.navs')))}}</a></li>
                @can("create", 4) 
                <li><a href="#web/navs/create"><i class="fa fa-plus"></i> {{trans('label.create_new_item', array('item' => trans('label.nav')))}}</a></li>
                @endcan</ul>
        </div>
        @endcan
    </div>
</div>


<div class="row">
    <div class="col-xs-12 col-md-7">

        <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->

        <form action="{{url('manage/web/links')}}" id="nav-form" class="form-horizontal ajax-form" novalidate="novalidate" method="post" data-hash="web/navs/{{$nav->id}}/links">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <input type="hidden" name="nav" value="{{$nav->id}}"/>

            <div class="form-group" id="name">
                <label class="col-sm-2 control-label">{{trans('label.name')}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="name" placeholder="{{trans('label.name')}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="type">
                <label class="col-sm-2 control-label">{{trans('label.type')}}</label>
                <div class="col-sm-10">
                    <select name="type" id="select-nav-type" class="form-control">
                        <option selected="" value="">{{trans('label.please_select')}}</option>
                        <option value="post">{{trans('label.post')}}</option>
                        <option value="internal">{{trans('label.internal')}}</option>
                        <option value="external">{{trans('label.external')}}</option>
                        <option value="nav">{{trans('label.nav')}}</option>
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group hidden" id="post_link">
                <label class="col-sm-2 control-label">{{trans('label.post')}}</label>
                <div class="col-sm-10">
                    <select name="post_link" class="form-control">
                        <option value="" selected="" disabled="">{{trans('label.please_select')}}</option>
                        @foreach($posts as $post)
                        <option value="{{$post->route}}">{{$post->title}}</option>
                        @endforeach 
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group hidden" id="internal_link">
                <label class="col-sm-2 control-label">{{trans('label.internal_link')}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="internal_link" placeholder="{{trans('label.internal_link')}}" value="">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group hidden" id="external_link">
                <label class="col-sm-2 control-label">{{trans('label.name')}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="external_link" placeholder="{{trans('label.external_link')}}" value="">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="nav_link">
                <label class="col-sm-2 control-label">{{trans('label.link')}}</label>
                <div class="col-sm-10">
                    <select name="nav_link" class="form-control">
                        <option value="">{{trans('label.please_select')}}</option>
                        @foreach($navs as $nav_link)
                        <option value="{{$nav_link->id}}">{{$nav_link->name}}</option>
                        @endforeach 
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">{{trans('label.name')}}</label>
                <div class="col-sm-10">
                    <div class="checkbox">
                    <label>
                        <input type="checkbox" name="open_in_new_window">
                        {{trans('label.open_in_new_window')}}
                    </label>
                    </div>
                </div>
            </div>

            @can("create", 4)
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary btn-sm"> 
                        <i class="fa fa-save fa-fw"></i> {{trans('action.save')}}
                    </button>
                </div>
            </div>
            @endcan 
        </form>
    </div>
</div>





