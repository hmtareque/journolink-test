<div class="header">
    <div class="title"><i class="fa fa-lg fa-list-alt fa-fw"></i> {{trans('label.list_of_items', array('item' => ' '.trans('label.links')))}}</div>
    <div class="links">
        
        @if(Auth::user()->can("read", 4) || Auth::user()->can("create", 4))
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{trans('label.navs')}} <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#web/navs"><i class="fa fa-list"></i> {{trans('label.list_of_items', array('item' => trans('label.navs')))}}</a></li>
                @can("create", 4) 
                <li><a href="#web/navs/create"><i class="fa fa-plus"></i> {{trans('label.create_new_item', array('item' => trans('label.nav')))}}</a></li>
                @endcan</ul>
        </div>
        @endcan
    </div>
</div>


<div class="row">
    <div class="col-xs-12">

        <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->

        @if($links->count() > 0)
        <div class="table-responsive">

            <table class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th>{{trans('label.name')}}</th>
                        <th>{{trans('label.type')}}</th>
                        <th>{{trans('label.link')}}</th>
                        <th>{{trans('label.visible')}}</th>
                        @if(Auth::user()->can("read", 4) || Auth::user()->can("create", 4))
                        <th>{{trans('label.action')}}</th>
                        @endif 
                    </tr>
                </thead>
                <tbody>
                    @foreach($links as $link) 
                    <tr>
                        <td class="text-center"><strong>{{$link->id}}</strong></td>
                        <td>{{$link->name}}</td>
                        <td>{{$link->type}}</td>
                        <td>
                            @if($link->type == 'post' || $link->type == 'internal') 
                            {{url($link->link)}}
                            @elseif($link->type == 'external')
                            {{$link->link}}
                            @elseif($link->type == 'nav')
                            {{$link->link}}
                            @endif 
                        </td>
                        <td class="text-center">
                            @if($link->visible == 1)
                            <i class="fa fa-check txt-color-green"></i>
                            @else 
                            <i class="fa fa-close txt-color-red"></i>
                            @endif 
                        </td>

                        @if(Auth::user()->can("update", 4) || Auth::user()->can("delete", 4))
                        <td>
                            @can('update', 4)
                            <a href="#web/links/{{$link->id}}/edit" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="{{trans('label.edit_item', array('item' => $link->link))}}"><i class="fa fa-pencil"></i></a>
                            
                            @if($link->visible == 1)

                            <button class="btn btn-warning btn-xs" data-toggle="modal" data-target="#hide-link-{{$link->id}}">
                                <i class="fa fa-arrow-down"></i>
                            </button>
                            <!-- Modal -->
                            <div class="modal fade" id="hide-link-{{$link->id}}" tabindex="-1" role="dialog" aria-labelledby="showModal" aria-hidden="true" data-backdrop="static">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form class="ajax-form" action="{{url('manage/web/links/'.$link->id.'/hide')}}" method="post">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                    &times;
                                                </button>
                                                <h4 class="modal-title" id="myModalLabel">{{trans('action.hide')}}</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>{{trans('alert.sure_to_hide_item', array('item' => $link->name))}}</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
                                                    {{trans('action.cancel')}}
                                                </button>
                                                <button type="submit" class="btn btn-sm btn-warning">
                                                    <i class="fa fa-arrow-down"></i>&nbsp; {{trans('action.hide')}}
                                                </button>
                                            </div>
                                        </form> 
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->

                            @else 

                            <button class="btn btn-success btn-xs" data-toggle="modal" data-target="#show-link-{{$link->id}}">
                                <i class="fa fa-arrow-up"></i>
                            </button>
                            <!-- Modal -->
                            <div class="modal fade" id="show-link-{{$link->id}}" tabindex="-1" role="dialog" aria-labelledby="showModalLabel" aria-hidden="true" data-backdrop="static">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form class="ajax-form" action="{{url('manage/web/links/'.$link->id.'/show')}}" method="post">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                    &times;
                                                </button>
                                                <h4 class="modal-title" id="showModalLabel">{{trans('action.show')}}</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>{{trans('alert.sure_to_show_item', array('item' => $link->name))}}</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
                                                    {{trans('action.cancel')}}
                                                </button>
                                                <button type="submit" class="btn btn-sm btn-success">
                                                    <i class="fa fa-arrow-up"></i>&nbsp; {{trans('action.show')}}
                                                </button>
                                            </div>
                                        </form> 
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                            @endif
                            
                            @endcan 


                            @can('delete', 4)
                            <button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#delete-link-{{$link->id}}">
                                <i class="fa fa-trash"></i>
                            </button>
                            <!-- Modal -->
                            <div class="modal fade" id="delete-link-{{$link->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" data-backdrop="static">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form class="ajax-form" action="{{url('manage/web/links/'.$link->id)}}" method="post">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="_method" value="delete">

                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                    &times;
                                                </button>
                                                <h4 class="modal-title" id="deleteModalLabel">{{trans('action.delete')}}</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>{{trans('alert.sure_to_delete_item', array('item' => $link->name))}}</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
                                                    {{trans('action.cancel')}}
                                                </button>
                                                <button type="submit" class="btn btn-sm btn-danger">
                                                    <i class="fa fa-trash"></i>&nbsp; {{trans('action.delete')}}
                                                </button>
                                            </div>
                                        </form> 
                                    </div>
                                </div>
                            </div>
                            @endcan 

                        </td>
                        @endif 
                    </tr>
                    @endforeach 
                </tbody>
            </table>
        </div>

        @else 

        <p class="alert alert-info">
            <i class="fa fa-info"></i>&nbsp;{{trans('alert.no_item_found', array('item' => 'link'))}}
        </p>

        @endif
    </div>
</div>



