<div class="header">
    <div class="title"><i class="fa fa-lg fa-list-alt fa-fw"></i> {{trans('label.edit_item', array('item' => $nav->name.' '.trans('label.link')))}}</div>
    <div class="links">
        @can("create", 4)
        <a href="#web/navs/{{$nav->id}}/links/create" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> {{trans('label.create_new_item', array('item' => $nav->name.' '.trans('label.link')))}}</a>
        @endcan
        
        @can("read", 4)
        <a href="#web/navs/{{$nav->id}}/links" class="btn btn-primary btn-sm"><i class="fa fa-list"></i> {{trans('label.list_of_items', array('item' => $nav->name.' '.trans('label.link')))}}</a>
        @endcan

        @if(Auth::user()->can("read", 4) || Auth::user()->can("create", 4))
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{trans('label.navs')}} <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#web/navs"><i class="fa fa-list"></i> {{trans('label.list_of_items', array('item' => trans('label.navs')))}}</a></li>
                @can("create", 4) 
                <li><a href="#web/navs/create"><i class="fa fa-plus"></i> {{trans('label.create_new_item', array('item' => trans('label.nav')))}}</a></li>
                @endcan</ul>
        </div>
        @endcan
    </div>
</div>


<div class="row">
    <div class="col-xs-12 col-md-7">

        <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->

        <form action="{{url('manage/web/links/'.$link->id)}}" id="nav-form" class="form-horizontal ajax-form" novalidate="novalidate" method="post">
                            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                            <input type="hidden" name="_method" value="put"/>
            
            <input type="hidden" name="nav" value="{{$nav->id}}"/>

            <div class="form-group" id="name">
                <label class="col-sm-2 control-label">{{trans('label.name')}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="name" placeholder="{{trans('label.name')}}" value="{{$link->name}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="type">
                <label class="col-sm-2 control-label">{{trans('label.type')}}</label>
                <div class="col-sm-10">
                    <select name="type" id="select-nav-type" class="form-control">
                           <option selected="" value="">{{trans('label.please_select')}}</option>
                                                <option value="post" @if($link->type == "post") selected="" @endif>{{trans('label.post')}}</option>
                                                <option value="internal" @if($link->type == "internal") selected="" @endif>{{trans('label.internal')}}</option>
                                                <option value="external" @if($link->type == "external") selected="" @endif>{{trans('label.external')}}</option>
                                                <option value="nav" @if($link->type == "nav") selected="" @endif>{{trans('label.nav')}}</option>
                                            
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group @if($link->type != 'post') hidden @endif" id="post_link">
                <label class="col-sm-2 control-label">{{trans('label.post')}}</label>
                <div class="col-sm-10">
                    <select name="post_link" class="form-control">
                        <option value="" selected="" disabled="">{{trans('label.please_select')}}</option>
                        @foreach($posts as $post)
                        <option value="{{$post->route}}" @if($link->type == 'post' && $link->link == $post->route) selected="" @endif>{{$post->title}}</option>
                        @endforeach 
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group @if($link->type != 'internal') hidden @endif" id="internal_link">
                <label class="col-sm-2 control-label">{{trans('label.internal_link')}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="internal_link" placeholder="{{trans('label.internal_link')}}" @if($link->type == 'internal') value="{{$link->link}}" @endif>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group @if($link->type != 'external') hidden @endif" id="external_link">
                <label class="col-sm-2 control-label">{{trans('label.name')}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="external_link" placeholder="{{trans('label.external_link')}}" @if($link->type == 'external') value="{{$link->link}}" @endif>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group @if($link->type != 'nav') hidden @endif" id="nav_link">
                <label class="col-sm-2 control-label">{{trans('label.link')}}</label>
                <div class="col-sm-10">
                    <select name="nav_link" class="form-control">
                        <option value="">{{trans('label.please_select')}}</option>
                        @foreach($navs as $nav_link)
                        <option value="{{$nav_link->id}}" @if($link->type == 'nav' && $nav_link->id == $link->link) selected="" @endif>{{$nav_link->name}}</option>
                        @endforeach 
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">{{trans('label.name')}}</label>
                <div class="col-sm-10">
                    <div class="checkbox">
                    <label>
                        <input type="checkbox" name="open_in_new_window" @if($link->target == 1) checked="" @endif>
                        {{trans('label.open_in_new_window')}}
                    </label>
                    </div>
                </div>
            </div>

            @can("update", 4)
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary btn-sm"> 
                        <i class="fa fa-refresh fa-fw"></i> {{trans('action.update')}}
                    </button>
                </div>
            </div>
            @endcan 
        </form>
    </div>
</div>

