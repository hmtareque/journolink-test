<div class="header">
    <div class="title"><i class="fa fa-lg fa-list-alt fa-fw"></i> {{trans('label.create_new_item', array('item' => trans('label.nav')))}}</div>
    <div class="links">
        @can("read", 4)
        <a href="#web/navs" class="btn btn-primary btn-sm"><i class="fa fa-list"></i> {{trans('label.list_of_items', array('item' => trans('label.nav')))}}</a>
        @endcan
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-6">
        
        <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->

        <form action="{{url('manage/web/navs')}}" id="nav-form" class="form-horizontal ajax-form" novalidate="novalidate" method="post" data-hash="web/navs">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>

            <div class="form-group" id="name">
                <label class="col-sm-2 control-label">{{trans('label.name')}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="name">
                    <span class="name-help-block help-block hide" style="font-size: 0.8em;"></span>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary btn-sm"> 
                        <i class="fa fa-save fa-fw"></i> {{trans('action.save')}}
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>

