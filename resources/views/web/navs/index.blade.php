<div class="header">
    <div class="title"><i class="fa fa-lg fa-list-alt fa-fw"></i> {{trans('label.list_of_items', array('item' => trans('label.navs')))}}</div>
    <div class="links">
        @can("create", 4)
        <a href="#web/navs/create" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> {{trans('label.create_new_item', array('item' => trans('label.nav')))}}</a>
        @endcan
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-9">
        <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->

        @if($navs->count() > 0)

        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th>{{trans('label.name')}}</th>
                        <th>{{trans('label.route')}}</th>
                        <th class="text-center">{{trans('label.no_of_links')}}</th>
                        <th>{{trans('label.visible')}}</th>
                        @if(Auth::user()->can("create", 4) || Auth::user()->can("update", 4) || Auth::user()->can("delete", 4))
                        <th>{{trans('label.action')}}</th>
                        @endif 
                    </tr>
                </thead>
                <tbody>
                    @foreach($navs as $nav) 
                    <tr>
                        <td class="text-center"><strong>{{$nav->id}}</strong></td>
                        <td>
                            @if($nav->no_of_links > 0) 
                            <a href="#web/navs/{{$nav->id}}/links" data-toggle="tooltip" data-placement="top" title="{{trans('label.links_of_nav', array('nav' => $nav->name))}}">{{$nav->name}}</a> 
                            @else 
                            {{$nav->name}}
                            @endif
                        </td>
                        <td>{{$nav->route}}</td>
                        <td class="text-center">
                            {{$nav->no_of_links}}
                        </td>
                        <td class="text-center">
                            @if($nav->visible == 1)
                            <i class="fa fa-check text-success"></i>
                            @else 
                            <i class="fa fa-close text-danger"></i>
                            @endif 
                        </td>

                        @if(Auth::user()->can("create", 4) || Auth::user()->can("update", 4) || Auth::user()->can("delete", 4))
                        <td>
                            @can('create', 4)
                            @if($nav->visible == 1)
                            <a href="#web/navs/{{$nav->id}}/links/create" class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="top" title="{{trans('label.create_link_of_nav', array('nav' => $nav->name))}}"><i class="fa fa-plus"></i></a>
                            @endif 
                            @endcan 
                            
                            @can('update', 4)
                            <a href="#web/navs/{{$nav->id}}/edit" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="{{trans('label.edit_item', array('item' => $nav->name))}}"><i class="fa fa-pencil"></i></a>

                            @if($nav->visible == 1)

                            <button class="btn btn-warning btn-xs" data-toggle="modal" data-target="#hide-nav-{{$nav->id}}">
                                <i class="fa fa-arrow-down"></i>
                            </button>
                            <!-- Modal -->
                            <div class="modal fade" id="hide-nav-{{$nav->id}}" tabindex="-1" role="dialog" aria-labelledby="hideModal" aria-hidden="true" data-backdrop="static">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form class="ajax-form" action="{{url('manage/web/navs/'.$nav->id.'/hide')}}" method="post">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                    &times;
                                                </button>
                                                <h4 class="modal-title" id="myModalLabel">{{trans('action.hide')}}</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>{{trans('alert.sure_to_hide_item', array('item' => $nav->name))}}</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
                                                    {{trans('action.cancel')}}
                                                </button>
                                                <button type="submit" class="btn btn-sm btn-warning">
                                                    <i class="fa fa-arrow-down"></i>&nbsp; {{trans('action.hide')}}
                                                </button>
                                            </div>
                                        </form> 
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->

                            @else 

                            <button class="btn btn-success btn-xs" data-toggle="modal" data-target="#show-nav-{{$nav->id}}">
                                <i class="fa fa-arrow-up"></i>
                            </button>
                            <!-- Modal -->
                            <div class="modal fade" id="show-nav-{{$nav->id}}" tabindex="-1" role="dialog" aria-labelledby="showModalLabel" aria-hidden="true" data-backdrop="static">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form class="ajax-form" action="{{url('manage/web/navs/'.$nav->id.'/show')}}" method="post">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                    &times;
                                                </button>
                                                <h4 class="modal-title" id="myModalLabel">{{trans('action.show')}}</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>{{trans('alert.sure_to_show_item', array('item' => $nav->name))}}</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
                                                    {{trans('action.cancel')}}
                                                </button>
                                                <button type="submit" class="btn btn-sm btn-success">
                                                    <i class="fa fa-arrow-up"></i>&nbsp; {{trans('action.show')}}
                                                </button>
                                            </div>
                                        </form> 
                                    </div>
                                </div>
                            </div>
                            @endif 
                            @endcan 


                            @if($nav->no_of_links <=0 ) 
                            @can('delete', 4)
                            <button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#delete-nav-{{$nav->id}}">
                                <i class="fa fa-trash"></i>
                            </button>
                            <!-- Modal -->
                            <div class="modal fade" id="delete-nav-{{$nav->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" data-backdrop="static">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form class="ajax-form" action="{{url('manage/web/navs/'.$nav->id)}}" method="post">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="_method" value="delete">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                    &times;
                                                </button>
                                                <h4 class="modal-title" id="deleteModalLabel">{{trans('action.delete')}}</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>{{trans('alert.sure_to_delete_item', array('item' => $nav->name))}}</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
                                                    {{trans('action.cancel')}}
                                                </button>
                                                <button type="submit" class="btn btn-sm btn-danger">
                                                    <i class="fa fa-trash"></i>&nbsp; {{trans('action.delete')}}
                                                </button>
                                            </div>
                                        </form> 
                                    </div>
                                </div>
                            </div>
                            @endcan 
                            @endif 

                        </td>
                        @endif 
                    </tr>
                    @endforeach 
                </tbody>
            </table>
        </div>

        @else 

        <p class="alert alert-info">
            <i class="fa fa-info"></i>&nbsp;{{trans('alert.no_item_found', array('item' => 'nav'))}}
        </p>

        @endif

    </div>
</div>





