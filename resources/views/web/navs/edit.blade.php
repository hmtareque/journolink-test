<div class="header">
    <div class="title"><i class="fa fa-lg fa-edit fa-fw"></i> {{trans('label.edit_item', array('item' => trans('label.nav')))}}</div>
    <div class="links">
        @can("create", 4)
        <a href="#web/navs/create" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> {{trans('label.create_new_item', array('item' => trans('label.nav')))}}</a>
        @endcan

        @can("read", 4)
        <a href="#web/navs" class="btn btn-primary btn-sm"><i class="fa fa-list"></i> {{trans('label.list_of_items', array('item' => trans('label.nav')))}}</a>
        @endcan
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-6">

        <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->

        <form action="{{url('manage/web/navs/'.$nav->id)}}" id="nav-form" class="form-horizontal ajax-form" novalidate="novalidate" method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <input type="hidden" name="_method" value="put"/>

            <div class="form-group" id="name">
                <label class="col-sm-2 control-label">{{trans('label.name')}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="name" value="{{$nav->name}}">
                    <span class="name-help-block help-block hide"></span>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary btn-sm"> 
                        <i class="fa fa-refresh fa-fw"></i>&nbsp;{{trans('action.update')}}
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>




