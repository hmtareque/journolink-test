<div class="header" id="test-1">
    <div class="title"><i class="fa fa-lg fa-plus-square-o fa-fw"></i> {{trans('label.create_new_item', array('item' => trans('label.post')))}}</div>
    <div class="links">
        @can("read", 4)
        <a href="#web/posts" class="btn btn-success btn-sm"><i class="fa fa-list"></i> {{trans('label.list_of_items', array('item' => trans('label.post')))}}</a>
        @endcan

        @if(Auth::user()->can("read", 4) || Auth::user()->can("create", 4))
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{trans('label.post_category')}} <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#web/post-categories"><i class="fa fa-list"></i> {{trans('label.list_of_items', array('item' => trans('label.post_category')))}}</a></li>
                @can("create", 4) 
                <li><a href="#web/post-categories/create"><i class="fa fa-plus"></i> {{trans('label.create_new_item', array('item' => trans('label.post_category')))}}</a></li>
                @endcan
            </ul>
        </div>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <form class="form-horizontal ajax-form" action="{{url('manage/web/posts')}}" method="post" data-hash="web/posts">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <div class="form-group" id="post_category">
                <label class="col-sm-2 control-label">{{trans('label.category')}}</label>
                <div class="col-sm-10">
                    <select name="post_category" class="form-control">
                        <option value="" selected="" disabled="">{{trans('label.please_select')}}</option>
                        @if($categories->count() > 0)
                            @foreach($categories as $category)
                            <option value="{{$category->id}}">{{$category->name}}</option>
                            @endforeach 
                        @endif 
                    </select> 
                    <span class="post_category-help-block help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="title">
                <label class="col-sm-2 control-label">{{trans('label.title')}}</label>
                <div class="col-sm-10">
                    <input class="form-control" type="text" name="title" placeholder="{{trans('label.title')}}">
                    <span class="title-help-block help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="meta_keywords">
                <label class="col-sm-2 control-label">{{trans('label.meta_keywords')}}</label>
                <div class="col-sm-10">
                    <textarea class="form-control" rows="3" name="meta_keywords" placeholder="{{trans('label.meta_keywords')}}"></textarea> 

                    <span class="meta_keywords-help-block help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="meta_description">
                <label class="col-sm-2 control-label">{{trans('label.meta_description')}}</label>
                <div class="col-sm-10">
                    <textarea class="form-control" rows="3" name="meta_description" placeholder="{{trans('label.meta_description')}}"></textarea> 
                    <span class="meta_description-help-block help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="content">
                <label class="col-sm-2 control-label">{{trans('label.content')}}</label>
                <div class="col-sm-10">
                    <textarea id="editor" class="form-control" rows="3" name="content" placeholder="{{trans('label.content')}}"></textarea> 
                    <span class="content-help-block help-block hide"></span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label"></label>
                <div class="col-sm-10">
                    <label><input type="checkbox" name="do_not_publish" value="1"> {{trans('label.do_not_publish')}}</label>
                </div>
            </div>

            @can("create", 4)
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary btn-sm"> 
                        <i class="fa fa-save fa-fw"></i> {{trans('action.save')}}
                    </button>
                </div>
            </div>
            @endcan
        </form>
    </div>
</div>


     

