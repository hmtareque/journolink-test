<div class="header">
    <div class="title"><i class="fa fa-lg fa-list-alt fa-fw"></i> {{trans('label.list_of_items', array('item' => trans('label.post_category')))}}</div>
    <div class="links">
        @can("create", 4)
        <a href="#web/post-categories/create" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> {{trans('label.create_new_item', array('item' => trans('label.post_category')))}}</a>
        @endcan 
        
        @if(Auth::user()->can("read", 4) || Auth::user()->can("create", 4))
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{trans('label.posts')}} <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#web/posts"><i class="fa fa-list"></i> {{trans('label.list_of_items', array('item' => trans('label.posts')))}}</a></li>
                
                @can("create", 4)
                <li><a href="#web/posts/create"><i class="fa fa-plus"></i> {{trans('label.create_new_item', array('item' => trans('label.post')))}}</a></li>
                @endcan 
            </ul>
        </div>
        @endif 
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-8">

            <!-- Notify Area -->
            @include('app.shared.flash') 
            <!-- Notfiy Area End -->

                    @if($categories->count() > 0)
                    <!-- widget content -->
                    <div class="table-responsive">

                        <table class="table table-striped table-bordered table-hover" width="100%">
                            <thead>
                                <tr>
                                    <th width="5%" class="text-center">#</th>
                                    <th>{{trans('label.name')}}</th>
                                    <th>{{trans('label.no_of_posts')}}</th>
                                    <th>{{trans('label.visible')}}</th>
                                    @if(Auth::user()->can("update", 4) || Auth::user()->can("delete", 4))
                                    <th>{{trans('label.action')}}</th>
                                    @endif  
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($categories as $category) 
                                <tr>
                                    <td class="text-center"><strong>{{$category->id}}</strong></td>
                                    <td>
                                        @if($category->no_of_posts > 0)
                                        
                                            <a href="#web/post-categories/{{$category->id}}/posts" data-toggle="tooltip" data-placement="top" title="{{trans('label.list_of_items', array('item' => $category->name.' '.trans('label.post')))}}">
                                                {{$category->name}}
                                            </a>
                                        @else 
                                        {{$category->name}}
                                        @endif 
                                        </td>
                                    <td class="text-center">{{$category->no_of_posts}}</td>
                                    <td class="text-center">
                                        @if($category->visible == 1)
                                        <i class="fa fa-check text-success"></i>
                                        @else 
                                        <i class="fa fa-close text-danger"></i>
                                        @endif 
                                    </td>

                                    @if(Auth::user()->can("update", 4) || Auth::user()->can("delete", 4)) 
                                    <td>
                                        @can("update", 4)
                                        <a href="#web/post-categories/{{$category->id}}/edit" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="{{trans('label.edit_item', array('item' => $category->name))}}"><i class="fa fa-pencil"></i></a>

                                        @if($category->visible == 1)
                                        <button class="btn btn-warning btn-xs" data-toggle="modal" data-target="#hide-category-{{$category->id}}">
                                            <i class="fa fa-arrow-down"></i>
                                        </button>
                                        <!-- Modal -->
                                        <div class="modal fade" id="hide-category-{{$category->id}}" tabindex="-1" role="dialog" aria-labelledby="hideModal" aria-hidden="true" data-backdrop="static">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <form class="ajax-form" action="{{url('manage/web/post-categories/'.$category->id.'/hide')}}" method="post">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                                &times;
                                                            </button>
                                                            <h4 class="modal-title" id="hideModallevel">{{trans('action.hide')}}</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>{{trans('alert.sure_to_hide_item', array('item' => $category->name))}}</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
                                                                {{trans('action.cancel')}}
                                                            </button>
                                                            <button type="submit" class="btn btn-warning btn-sm">
                                                                <i class="fa fa-arrow-down"></i>&nbsp; {{trans('action.hide')}}
                                                            </button>
                                                        </div>
                                                    </form> 
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div><!-- /.modal -->

                                        @else 

                                        <button class="btn btn-success btn-xs" data-toggle="modal" data-target="#show-category-{{$category->id}}">
                                            <i class="fa fa-arrow-up"></i>
                                        </button>
                                        <!-- Modal -->
                                        <div class="modal fade" id="show-category-{{$category->id}}" tabindex="-1" role="dialog" aria-labelledby="showModal" aria-hidden="true" data-backdrop="static">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <form class="ajax-form" action="{{url('manage/web/post-categories/'.$category->id.'/show')}}" method="post">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                                &times;
                                                            </button>
                                                            <h4 class="modal-title" id="showModalLevel">{{trans('action.show')}}</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>{{trans('alert.sure_to_show_item', array('item' => $category->name))}}</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
                                                                {{trans('action.cancel')}}
                                                            </button>
                                                            <button type="submit" class="btn btn-success btn-sm">
                                                                <i class="fa fa-arrow-up"></i>&nbsp; {{trans('action.show')}}
                                                            </button>
                                                        </div>
                                                    </form> 
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div><!-- /.modal -->
                                        @endif 

                                        @endcan 
                                        
                                        @can("delete", 4)
                                        @if($category->no_of_posts<=0)
                                        <button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#delete-category-{{$category->id}}">
                                            <i class="fa fa-trash"></i>
                                        </button>
                                        <!-- Modal -->
                                        <div class="modal fade" id="delete-category-{{$category->id}}" tabindex="-1" role="dialog" aria-labelledby="unpublishModal" aria-hidden="true" data-backdrop="static">
                                            <div class="modal-dialog">
                                                <div class="modal-content">
                                                    <form class="ajax-form" action="{{url('manage/web/post-categories/'.$category->id)}}" method="post">
                                                        <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                                        <input type="hidden" name="_method" value="delete"/>
                                                        <div class="modal-header">
                                                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                                &times;
                                                            </button>
                                                            <h4 class="modal-title" id="myModalLabel">{{trans('action.delete')}}</h4>
                                                        </div>
                                                        <div class="modal-body">
                                                            <p>{{trans('alert.sure_to_delete_item', array('item' => $category->name))}}</p>
                                                        </div>
                                                        <div class="modal-footer">
                                                            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
                                                                {{trans('action.cancel')}}
                                                            </button>
                                                            <button type="submit" class="btn btn-danger btn-sm">
                                                                <i class="fa fa-trash"></i>&nbsp; {{trans('action.delete')}}
                                                            </button>
                                                        </div>
                                                    </form> 
                                                </div><!-- /.modal-content -->
                                            </div><!-- /.modal-dialog -->
                                        </div><!-- /.modal -->

                                        @endif 
                                        @endcan 

                                    </td>
                                    @endif
                                </tr>
                                @endforeach 
                            </tbody>
                        </table>
                    </div>

                    @else 

                    <p class="alert alert-info"><i class="fa fa-info"></i>&nbsp;{{trans('alert.no_item_found', array('item' => 'category'))}}</p>
                 
                    @endif
                   
                </div>
               
            </div>
         




