<div class="header">
    <div class="title"><i class="fa fa-lg fa-edit fa-fw"></i> {{trans('label.edit_item', array('item' => trans('label.post_category')))}}</div>
    <div class="links">
        @can("create", 4)
        <a href="#web/post-categories/create" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> {{trans('label.create_new_item', array('item' => trans('label.post_category')))}}</a>
        @endcan 
        
        @can("read", 4)
        <a href="#web/post-categories" class="btn btn-primary btn-sm"><i class="fa fa-list"></i> {{trans('label.list_of_items', array('item' => trans('label.post_categories')))}}</a>
        @endcan 
        
        @if(Auth::user()->can("read", 4) || Auth::user()->can("create", 4))
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{trans('label.posts')}} <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#web/posts"><i class="fa fa-list"></i> {{trans('label.list_of_items', array('item' => trans('label.posts')))}}</a></li>
                @can("create", 4)
                <li><a href="#web/posts/create"><i class="fa fa-plus"></i> {{trans('label.create_new_item', array('item' => trans('label.post')))}}</a></li>
                @endcan 
            </ul>
        </div>
        @endif 
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-8">

        <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->

        <form action="{{url('manage/web/post-categories/'.$category->id)}}" id="post-category-form" class="form-horizontal ajax-form" novalidate="novalidate" method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <input type="hidden" name="_method" value="put"/>

            <div class="form-group" id="name">
                <label class="col-sm-2 control-label">{{trans('label.name')}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="name" placeholder="{{trans('label.post_category')}}" value="{{$category->name}}">
                    <span class="name-help-block help-block hide"></span>
                </div>
            </div>

            @can("create", 4)
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary btn-sm"> 
                        <i class="fa fa-refresh fa-fw"></i> {{trans('action.update')}}
                    </button>
                </div>
            </div>
            @endcan 
        </form>
    </div>
</div>


