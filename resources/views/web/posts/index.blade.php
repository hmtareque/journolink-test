<div class="header">
    <div class="title"><i class="fa fa-lg fa-list-alt fa-fw"></i> {{trans('label.list_of_items', array('item' => (isset($category))? $category->name : trans('label.post')))}}</div>
    <div class="links">
        @can("create", 4)
        <a href="#web/posts/create" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> {{trans('label.create_new_item', array('item' => trans('label.post')))}}</a>
        @endcan

        @if(Auth::user()->can("read", 4) || Auth::user()->can("update", 4))
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{trans('label.post_category')}} <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#web/post-categories"><i class="fa fa-list"></i> {{trans('label.list_of_items', array('item' => trans('label.post_category')))}}</a></li>
                @can("create", 4) 
                <li><a href="#web/post-categories/create"><i class="fa fa-plus"></i> {{trans('label.create_new_item', array('item' => trans('label.post_category')))}}</a></li>
                @endcan</ul>
        </div>
        @endcan
    </div>
</div>

<!-- row -->
<div class="row">
    <div class="col-xs-12">

        <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->

        @if($posts->count() > 0)
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                    <tr>
                        <th width="5%" class="text-center">#</th>
                        <th width="60%">{{trans('label.title')}}</th>

                        @if(!isset($category))
                        <th width="15%">{{trans('label.category')}}</th>
                        @endif 

                        <th width="5%">{{trans('label.published')}}</th>
                        @if(Auth::user()->can("update", 4) || Auth::user()->can("delete", 4))
                        <th data-hide="phone,tablet" width="15%">{{trans('label.action')}}</th>
                        @endif 
                    </tr>
                </thead>
                <tbody>
                    @foreach($posts as $post) 
                    <tr>
                        <td class="text-center"><strong>{{$post->id}}</strong></td>
                        <td><span data-toggle="tooltip" width="100%" data-placement="top" title="{{$post->route}}">{{$post->title}}</span></td>

                        @if(!isset($category))
                        <td>@if($post->category != "") {{$post->category}} @else -- @endif</td>
                        @endif 

                        <td class="text-center">@if($post->published == 1)<i class="fa fa-check text-success"></i>@else <i class="fa fa-close text-danger"></i> @endif </td>

                        @if(Auth::user()->can("update", 4) || Auth::user()->can("delete", 4))
                        <td>
                            @can("update", 4)

                            <a href="#web/posts/{{$post->id}}/edit" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="{{trans('label.edit_item', array('item' => $post->title))}}"><i class="fa fa-pencil"></i></a>

                            @if($post->published == 1)

                            <button class="btn btn-warning btn-xs" data-toggle="modal" data-target="#unpublish-post-{{$post->id}}">
                                <i class="fa fa-arrow-down"></i>
                            </button>
                            <!-- Modal -->
                            <div class="modal fade" id="unpublish-post-{{$post->id}}" tabindex="-1" role="dialog" aria-labelledby="unpublishModal" aria-hidden="true" data-backdrop="static">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form class="ajax-form" action="{{url('manage/web/posts/'.$post->id.'/unpublish')}}" method="post">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                    &times;
                                                </button>
                                                <h4 class="modal-title" id="myModalLabel">{{trans('label.unpublish_item', array('item' => 'Post'))}}</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>{{trans('alert.sure_to_unpublish_item', array('item' => $post->title))}}</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
                                                    {{trans('action.cancel')}}
                                                </button>
                                                <button type="submit" class="btn btn-warning btn-sm">
                                                    <i class="fa fa-arrow-down"></i>&nbsp; {{trans('action.unpublish')}}
                                                </button>
                                            </div>
                                        </form> 
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->

                            @else 

                            <button class="btn btn-success btn-xs" data-toggle="modal" data-target="#publish-post-{{$post->id}}">
                                <i class="fa fa-arrow-up"></i>
                            </button>
                            <!-- Modal -->
                            <div class="modal fade" id="publish-post-{{$post->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form class="ajax-form" action="{{url('manage/web/posts/'.$post->id.'/publish')}}" method="post">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                    &times;
                                                </button>
                                                <h4 class="modal-title" id="myModalLabel">{{trans('label.publish_item', array('item' => 'Post'))}}</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>{{trans('alert.sure_to_publish_item', array('item' => $post->title))}}</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
                                                    {{trans('action.cancel')}}
                                                </button>
                                                <button type="submit" class="btn btn-sm btn-success">
                                                    <i class="fa fa-arrow-up"></i>&nbsp; {{trans('action.publish')}}
                                                </button>
                                            </div>
                                        </form> 
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                            @endif 
                            @endcan 



                            @can("delete", 4)
                            <button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#delete-category-{{$post->id}}">
                                <i class="fa fa-trash"></i>
                            </button>
                            <!-- Modal -->
                            <div class="modal fade" id="delete-category-{{$post->id}}" tabindex="-1" role="dialog" aria-labelledby="unpublishModal" aria-hidden="true" data-backdrop="static">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form class="ajax-form" action="{{url('manage/web/posts/'.$post->id)}}" method="post">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                            <input type="hidden" name="_method" value="delete"/>
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                    &times;
                                                </button>
                                                <h4 class="modal-title" id="myModalLabel">{{trans('action.delete')}}</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>{{trans('alert.sure_to_delete_item', array('item' => $post->title))}}</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
                                                    {{trans('action.cancel')}}
                                                </button>
                                                <button type="submit" class="btn btn-danger btn-sm">
                                                    <i class="fa fa-trash"></i>&nbsp; {{trans('action.delete')}}
                                                </button>
                                            </div>
                                        </form> 
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                            @endcan 

                        </td>
                        @endif 
                    </tr>
                    @endforeach 
                </tbody>
            </table>
        </div>

        @else 
        <p class="alert alert-info">
            <i class="fa fa-info"></i>&nbsp;{{trans('alert.no_item_found', array('item' => 'post'))}}
        </p>
        @endif

    </div>

</div>



