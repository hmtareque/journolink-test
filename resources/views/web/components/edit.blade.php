<div class="header">
    <div class="title"><i class="fa fa-lg fa-edit fa-fw"></i> {{trans('label.edit_item', array('item' => trans('label.component')))}}</div>
    <div class="links">
        @can("create", 4)
        <a href="#web/components/create" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> {{trans('label.create_new_item', array('item' => trans('label.component')))}}</a>
        @endcan 

        @can("read", 4)
        <a href="#web/components" class="btn btn-primary btn-sm"><i class="fa fa-list"></i> {{trans('label.list_of_items', array('item' => trans('label.component')))}}</a>
        @endcan
    </div>
</div>

<div class="row">
    <div class="col-xs-12">

        <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->

        <form action="{{url('manage/web/components/'.$component->id)}}" id="custom-component-form" class="form-horizontal ajax-form" novalidate="novalidate" method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <input type="hidden" name="_method" value="put"/>

            <div class="form-group" id="name">
                <label class="col-sm-2 control-label">{{trans('label.name')}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="name" placeholder="{{trans('label.name')}}" value="{{$component->name}}">
                    <span class="name-help-block help-block hide"></span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">{{trans('label.property')}}</label>
                <div class="col-sm-10">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-hover" id="component-property">
                            <thead>
                                <tr>
                                    <th with="10%">{{trans('label.name')}}</th>
                                    <th with="10%">{{trans('label.type')}}</th>
                                    <th with="60%">{{trans('label.options')}}</th>
                                    <th with="10%">{{trans('label.required')}}</th>
                                    <th with="10%">{{trans('label.action')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php $n = 0; ?>
                                @foreach($component->properties as $key => $property) 

                                <tr @if($property->visible == 0) class="bg-warning" @endif>
                                     <td>
                                        <div id="property-{{$key}}-name">
                                            <input class="form-control" type="text" name="property[{{$key}}][name]" placeholder="Property Name" value="{{$property->name}}">
                                            <span class="property-{{$key}}-name-help-block help-block hide"></span>
                                        </div>
                                    </td>
                                    <td> 
                                        <div id="property-{{$key}}-type">
                                            <select class="component-property-type form-control" name="property[{{$key}}][type]" data-item="{{$key}}">
                                                <option @if($property->type == "text") selected="" @endif value="text">Text</option>
                                                <option @if($property->type == "numeric") selected="" @endif value="numeric">Numeric</option>
                                                <option @if($property->type == "textarea") selected="" @endif value="textarea">Text Area</option>
                                                <option @if($property->type == "select") selected="" @endif value="select">Select</option>
                                                <option @if($property->type == "checkbox") selected="" @endif value="checkbox">Check Box</option>
                                                <option @if($property->type == "radio") selected="" @endif value="radio">Radio</option>
                                                <option @if($property->type == "email") selected="" @endif value="email">Email</option>
                                                <option @if($property->type == "date") selected="" @endif value="date">Date</option>
                                                <option @if($property->type == "image") selected="" @endif value="image">Image</option>
                                                <option @if($property->type == "link") selected="" @endif value="link">Link</option>
                                            </select>
                                            <span class="property-{{$key}}-type-help-block help-block hide"></span>
                                        </div>
                                    </td>
                                    <td>
                                        <div id="property-{{$key}}-options">
                                            <input class="form-control @if(!in_array($property->type, array('select', 'checkbox', 'radio'))) hide @endif" type="text" id="property-{{$key}}-option-value" name="property[{{$key}}][options]" placeholder="Options" value="{{$property->options}}">
                                            <span class="property-{{$key}}-options-help-block help-block hide"></span>
                                        </div>
                                    </td>
                                    <td>
                                        <label><input type="checkbox" name="property[{{$key}}][required]" @if($property->required == 1) checked="" @endif> Yes</label>
                                    </td>
                                    <td>
                                        <input id="property-{{$key}}-visible" type="hidden" name="property[{{$key}}][visible]" value="{{$property->visible}}">
                                        <button type="button" data-item="{{$key}}" id="property-{{$key}}-restore" class="btn btn-success btn-xs restore-component-propery @if($property->visible == 1) hide @endif"><i class="fa fa-repeat"></i> restore</button>
                                        <button type="button" data-item="{{$key}}" id="property-{{$key}}-hide" class="btn btn-danger btn-xs hide-component-propery @if($property->visible == 0) hide @endif"><i class="fa fa-remove"></i> remove</button>

                                    </td>
                                </tr>
                                <?php $n = $key + 1; ?>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="button" id="add-component-property" data-item="{{$n}}" class="btn btn-success btn-sm"><i class="fa fa-plus"></i>&nbsp; {{trans('label.create_new_item', array('item' => trans('label.property')))}}</button>
                </div>
            </div>

            @can("update", 4)
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary btn-sm"> 
                        <i class="fa fa-refresh fa-fw"></i> {{trans('action.update')}}
                    </button>
                </div>
            </div>  
            @endcan 
        </form>            

    </div>
</div>




