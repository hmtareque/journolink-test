<div class="header">
    <div class="title"><i class="fa fa-lg fa-plus-square-o fa-fw"></i> {{trans('label.create_new_item', array('item' => $component->name))}}</div>
    <div class="links">
        @can("read", 4)
        <a href="#web/components/{{$component->id}}/items" class="btn btn-primary btn-sm"><i class="fa fa-list"></i> {{trans('label.list_of_items', array('item' => $component->name))}}</a>
        @endcan
        
        @if(Auth::user()->can("read", 4) || Auth::user()->can("create", 4))
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{trans('label.components')}} <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#web/components"><i class="fa fa-list"></i> {{trans('label.list_of_items', array('item' => trans('label.component')))}}</a></li>
                @can("create", 4) 
                <li><a href="#web/components/create"><i class="fa fa-plus"></i> {{trans('label.create_new_item', array('item' => trans('label.component')))}}</a></li>
                @endcan
            </ul>
        </div>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-8">

        <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->

        <form class="form-horizontal ajax-form" action="{{url('manage/web/components/'.$component->id.'/items')}}" id="component-form" novalidate="novalidate" method="post" name="web/components/{{$component->id}}/items" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>

            <div class="form-group" id="identifier">
                <label class="col-sm-2 control-label">{{trans('label.identifier')}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="identifier" placeholder="{{trans('label.identifier')}}">
                    <span class="identifier-help-block help-block hide"></span>
                </div>
            </div>

            @foreach($component->properties as $field)

            @if($field->visible == 1)

            @if($field->type == 'text')
            <div class="form-group" id="item-{{$field->id}}">
                <label class="col-sm-2 control-label">{{title_case($field->name)}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="item[{{$field->id}}]" placeholder="{{title_case($field->name)}}">
                    <span class="item-{{$field->id}}-help-block help-block hide"></span>
                </div>
            </div>
            @endif


            @if($field->type == 'numeric')
            <div class="form-group" id="item-{{$field->id}}">
                <label class="col-sm-2 control-label">{{title_case($field->name)}}</label>
                <div class="col-sm-10">
                    <input type="number" class="form-control" name="item[{{$field->id}}]" placeholder="{{title_case($field->name)}}">
                    <span class="item-{{$field->id}}-help-block help-block hide"></span>
                </div>
            </div>
            @endif

            @if($field->type == 'textarea')
            <div class="form-group" id="item-{{$field->id}}">
                <label class="col-sm-2 control-label">{{title_case($field->name)}}</label>
                <div class="col-sm-10">
                    <textarea class="form-control" name="item[{{$field->id}}]" placeholder="{{title_case($field->name)}}"></textarea>
                    <span class="item-{{$field->id}}-help-block help-block hide"></span>
                </div>
            </div>
            @endif 


            @if($field->type == 'date')
            <div class="form-group" id="item-{{$field->id}}">
                <label class="col-sm-2 control-label">{{title_case($field->name)}}</label>
                <div class="col-sm-10">
                    <input type="date" class="form-control" name="item[{{$field->id}}]" placeholder="{{title_case($field->name)}}">
                    <span class="item-{{$field->id}}-help-block help-block hide"></span>
                </div>
            </div>
            @endif

            @if($field->type == 'email')
            <div class="form-group" id="item-{{$field->id}}">
                <label class="col-sm-2 control-label">{{title_case($field->name)}}</label>
                <div class="col-sm-10">
                    <input type="email" class="form-control" name="item[{{$field->id}}]" placeholder="{{title_case($field->name)}}">
                    <span class="item-{{$field->id}}-help-block help-block hide"></span>
                </div>
            </div>
            @endif

            @if($field->type == 'select')
            <div class="form-group" id="item-{{$field->id}}">
                <label class="col-sm-2 control-label">{{title_case($field->name)}}</label>
                <div class="col-sm-10">
                    <select name="item[{{$field->id}}]" class="form-control">
                        @foreach(explode(',',$field->options) as $option) 
                        <option value="{{$option}}">{{$option}}</option>
                        @endforeach
                    </select>
                    <span class="item-{{$field->id}}-help-block help-block hide"></span>
                </div>
            </div>
            @endif

            @if($field->type == 'checkbox')
            <div class="form-group" id="item-{{$field->id}}">
                <label class="col-sm-2 control-label">{{title_case($field->name)}}</label>
                <div class="col-sm-10">
                    @foreach(explode(',',$field->options) as $option)
                     <div class="checkbox">
                    <label><input type="checkbox" name="item[{{$field->id}}]" value="{{$option}}">{{$option}}</label>
                     </div>
                    @endforeach
                    <span class="item-{{$field->id}}-help-block help-block hide"></span>
                </div>
            </div>
            @endif

            @if($field->type == 'radio')
            <div class="form-group" id="item-{{$field->id}}">
                <label class="col-sm-2 control-label">{{title_case($field->name)}}</label>
                <div class="col-sm-10">
                    @foreach(explode(',',$field->options) as $option) 
                    <div class="radio">
                    <label><input type="radio" name="item[{{$field->id}}]" value="{{$option}}"> {{$option}}</label>
                    </div>
                    @endforeach
                    <span class="item-{{$field->id}}-help-block help-block hide"></span>
                </div>
            </div>
            @endif

            @if($field->type == 'image')
            
            
            <div class="form-group" id="item-{{$field->id}}">
                <label class="col-sm-2 control-label">{{title_case($field->name)}}</label>
                <div class="col-sm-10">
                    <div id="image-{{$field->id}}-preview"></div>
                    <label>
                        <input class="btn btn-default btn-sm preview-file" data-item="{{$field->id}}" id="item-{{$field->id}}-file" type="file" name="item[{{$field->id}}]" />
                    </label>
                    <span class="help-block hide"></span>
                    <div>
                     <a href="javascript:void(0);" class="btn btn-danger btn-xs component-image-remover hide" data-item="{{$field->id}}" id="image-{{$field->id}}-remover">{{trans('action.remove')}}</a>
                    </div>
                </div>
            </div>

            @endif


            @if($field->type == 'link')
            <div class="form-group" id="item-{{$field->id}}">
                <label class="col-sm-2 control-label">{{title_case($field->name)}}</label>
                <div class="col-sm-10">
                    <input class="form-control" type="text" name="item[{{$field->id}}]" placeholder="{{$field->name}}">
                    <span class="item-{{$field->id}}-help-block help-block hide"></span>
                </div>
            </div>
            @endif

            @endif

            @endforeach 

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary btn-sm"> 
                        <i class="fa fa-save fa-fw"></i> {{trans('action.save')}}
                    </button>
                </div>
            </div>
        </form>

    </div>
</div>





