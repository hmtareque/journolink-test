<div class="header">
    <div class="title"><i class="fa fa-lg fa-plus-square-o fa-fw"></i> {{trans('label.edit_item', array('item' => $component->name))}}</div>
    <div class="links">

        @can("create", 4)
        <a href="#web/components/{{$component->id}}/items/create" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> {{trans('label.create_new_item', array('item' => $component->name))}}</a>
        @endcan

        @can("read", 4)
        <a href="#web/components/{{$component->id}}/items" class="btn btn-primary btn-sm"><i class="fa fa-list"></i> {{trans('label.list_of_items', array('item' => $component->name))}}</a>
        @endcan

        @if(Auth::user()->can("read", 4) || Auth::user()->can("create", 4))
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{trans('label.components')}} <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#web/components"><i class="fa fa-list"></i> {{trans('label.list_of_items', array('item' => trans('label.component')))}}</a></li>
                @can("create", 4) 
                <li><a href="#web/components/create"><i class="fa fa-plus"></i> {{trans('label.create_new_item', array('item' => trans('label.component')))}}</a></li>
                @endcan
            </ul>
        </div>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-8">

        <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->

        <form action="{{url('manage/web/components/'.$item->component_id.'/items/'.$item->id)}}" id="component-form" class="form-horizontal ajax-form" novalidate="novalidate" method="post" enctype="multipart/form-data">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <input type="hidden" name="_method" value="put"/>

            <div class="form-group" id="identifier">
                <label class="col-sm-2 control-label">{{trans('label.identifier')}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="identifier" placeholder="{{trans('label.identifier')}}" value="{{$item->identifier}}">
                    <span class="identifier-help-block help-block hide"></span>
                </div>
            </div>

            @foreach($item->properties as $field)

            @if($field->type == 'text')
            <div class="form-group" id="item-{{$field->id}}">
                <label class="col-sm-2 control-label">{{title_case($field->name)}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="item[{{$field->id}}]" placeholder="{{$field->name}}" value="{{$field->value}}">
                    <span class="item-{{$field->id}}-help-block help-block hide"></span>
                </div>
            </div>
            @endif


            @if($field->type == 'numeric')
            <div class="form-group" id="item-{{$field->id}}">
                <label class="col-sm-2 control-label">{{title_case($field->name)}}</label>
                <div class="col-sm-10">
                    <input type="number" class="form-control" name="item[{{$field->id}}]" placeholder="{{$field->name}}" value="{{$field->value}}">
                    <span class="item-{{$field->id}}-help-block help-block hide"></span>
                </div>
            </div>
            @endif

            @if($field->type == 'textarea')
            <div class="form-group" id="item-{{$field->id}}">
                <label class="col-sm-2 control-label">{{title_case($field->name)}}</label>
                <div class="col-sm-10">
                    <textarea class="form-control" name="item[{{$field->id}}]" placeholder="{{title_case($field->name)}}">{{$field->value}}</textarea>
                    <span class="item-{{$field->id}}-help-block help-block hide"></span>
                </div>
            </div>
            @endif 


            @if($field->type == 'date')
            <div class="form-group" id="item-{{$field->id}}">
                <label class="col-sm-2 control-label">{{title_case($field->name)}}</label>
                <div class="col-sm-10">
                    <input type="date" class="form-control" name="item[{{$field->id}}]" placeholder="{{$field->name}}" value="{{$field->value}}">
                    <span class="item-{{$field->id}}-help-block help-block hide"></span>
                </div>
            </div>
            @endif

            @if($field->type == 'email')
            <div class="form-group" id="item-{{$field->id}}">
                <label class="col-sm-2 control-label">{{title_case($field->name)}}</label>
                <div class="col-sm-10">
                    <input type="email" class="form-control" name="item[{{$field->id}}]" placeholder="{{$field->name}}" value="{{$field->value}}">
                    <span class="item-{{$field->id}}-help-block help-block hide"></span>
                </div>
            </div>
            @endif

            @if($field->type == 'select')
            <div class="form-group" id="item-{{$field->id}}">
                <label class="col-sm-2 control-label">{{title_case($field->name)}}</label>
                <div class="col-sm-10">
                    <select name="item[{{$field->id}}]" class="form-control">
                        @foreach(explode(',',$field->options) as $option) 
                        <option value="{{$option}}" @if($field->value == $option) selected="" @endif>{{$option}}</option>
                        @endforeach
                    </select>
                    <span class="item-{{$field->id}}-help-block help-block hide"></span>
                </div>
            </div>
            @endif

            @if($field->type == 'checkbox')
            <div class="form-group" id="item-{{$field->id}}">
                <label class="col-sm-2 control-label">{{title_case($field->name)}}</label>
                <div class="col-sm-10">
                    @foreach(explode(',',$field->options) as $option)
                    <div class="checkbox">
                        <label><input type="checkbox" name="item[{{$field->id}}]" value="{{$option}}" @if($field->value == $option) checked="" @endif>{{$option}}</label>
                    </div>
                    @endforeach
                    <span class="item-{{$field->id}}-help-block help-block hide"></span>
                </div>
            </div>
            @endif

            @if($field->type == 'radio')
            <div class="form-group" id="item-{{$field->id}}">
                <label class="col-sm-2 control-label">{{title_case($field->name)}}</label>
                <div class="col-sm-10">
                    @foreach(explode(',',$field->options) as $option) 
                    <div class="radio">
                        <label><input type="radio" name="item[{{$field->id}}]" value="{{$option}}" @if($field->value == $option) checked="" @endif> {{$option}}</label>
                    </div>
                    @endforeach
                    <span class="item-{{$field->id}}-help-block help-block hide"></span>
                </div>
            </div>
            @endif

            @if($field->type == 'image')
            <div class="form-group" id="item-{{$field->id}}">
                <label class="col-sm-2 control-label">{{title_case($field->name)}}</label>
                <div class="col-sm-10">
                    <div id="image-{{$field->id}}-preview">
                         @if($field->value)
                          <img style="width: 64px; height: auto; margin-bottom: 5px;" alt="64x64" src="{{asset($field->value)}}"/>
                         @endif
                    </div>
                    <label>
                        <input class="btn btn-default btn-sm preview-file" data-item="{{$field->id}}" id="item-{{$field->id}}-file" type="file" name="item[{{$field->id}}]" />
                        <input id="actual-{{$field->id}}-image" type="hidden" name="item[{{$field->id}}]" value="{{$field->value}}" data-src="{{asset($field->value)}}"/>
                    </label>
                    <span class="item-{{$field->id}}-help-block help-block hide"></span>
                    <div>
                     <a href="javascript:void(0);" class="btn btn-danger btn-xs component-image-remover @if(!$field->value) hide @endif" data-item="{{$field->id}}" id="image-{{$field->id}}-remover">{{trans('action.remove')}}</a>
                     <a href="javascript:void(0);" class="btn btn-success btn-xs component-image-reloader hide" data-item="{{$field->id}}" id="image-{{$field->id}}-reloader">{{trans('action.reload')}}</a>
                    </div>
                </div>
            </div>
            @endif


            @if($field->type == 'link')
            <div class="form-group" id="item-{{$field->id}}">
                <label class="col-sm-2 control-label">{{title_case($field->name)}}</label>
                <div class="col-sm-10">
                    <input class="form-control" type="text" name="item[{{$field->id}}]" placeholder="{{$field->name}}" value="{{$field->value}}">
                    <span class="item-{{$field->id}}-help-block help-block hide"></span>
                </div>
            </div>
            @endif

            @endforeach 

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary btn-sm"> 
                        <i class="fa fa-refresh fa-fw"></i> {{trans('action.update')}}
                    </button>
                </div>
            </div>
        </form>

    </div>
</div>




