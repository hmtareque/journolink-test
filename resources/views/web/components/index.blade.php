<div class="header">
    <div class="title"><i class="fa fa-lg fa-list-alt fa-fw"></i> {{trans('label.list_of_items', array('item' => trans('label.components')))}}</div>
    <div class="links">
        @can("create", 4)
        <a href="#web/components/create" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> {{trans('label.create_new_item', array('item' => trans('label.component')))}}</a>
        @endcan
    </div>
</div>


<!-- row -->
<div class="row">
    <div class="col-xs-12 col-md-9">
        <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->
        @if($components->count() > 0)
        <div class="table-responsive">

            <table class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                    <tr>
                        <th class="text-center">#</th>
                        <th>{{trans('label.name')}}</th>
                        <th>{{trans('label.identifier')}}</th>
                        <th>{{trans('label.no_of_items')}}</th>
                        <th>{{trans('label.visible')}}</th>
                        @if(Auth::user()->can("create", 4) || Auth::user()->can("update", 4) || Auth::user()->can("delete", 4))
                        <th>{{trans('label.action')}}</th>
                        @endif 
                    </tr>
                </thead>
                <tbody>
                    @foreach($components as $component) 
                    <tr>
                        <td class="text-center"><strong>{{$component->id}}</strong></td>
                        <td>
                            @if($component->no_of_items > 0)
                            <a href="#web/components/{{$component->id}}/items" data-toggle="tooltip" data-placement="top" title="{{trans('label.items_of_component', array('component' => $component->name))}}">{{$component->name}}</a>
                            @else 
                            {{$component->name}}
                            @endif 
                        </td>

                        <td>{{$component->route}}</td>
                        <td class="text-center">{{$component->no_of_items}}</td>
                        <td class="text-center">@if($component->visible == 1)<i class="fa fa-check text-success"></i>@else <i class="fa fa-close text-danger"></i> @endif </td>
                        @if(Auth::user()->can("create", 4) || Auth::user()->can("update", 4) || Auth::user()->can("delete", 4))
                        <td>
                            @can("create", 4)
                            <a href="#web/components/{{$component->id}}/items/create" class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="top" title="{{trans('label.create_new_item', array('item' => $component->name))}}"><i class="fa fa-plus"></i></a>
                            @endcan 

                            @can("update", 4)
                            <a href="#web/components/{{$component->id}}/edit" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="{{trans('label.edit_item', array('item' => $component->name))}}"><i class="fa fa-pencil"></i></a>
                            
                            @if($component->visible == 1)
                            <button class="btn btn-warning btn-xs" data-toggle="modal" data-target="#hide-component-{{$component->id}}">
                                <i class="fa fa-arrow-down"></i>
                            </button>
                            <!-- Modal -->
                            <div class="modal fade" id="hide-component-{{$component->id}}" tabindex="-1" role="dialog" aria-labelledby="unpublishModal" aria-hidden="true" data-backdrop="static">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form class="ajax-form" action="{{url('manage/web/components/'.$component->id.'/hide')}}" method="post">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                    &times;
                                                </button>
                                                <h4 class="modal-title" id="myModalLabel">{{trans('action.hide')}}</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>{{trans('alert.sure_to_hide_item', array('item' => $component->name))}}</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
                                                    {{trans('action.cancel')}}
                                                </button>
                                                <button type="submit" class="btn btn-warning btn-sm">
                                                    <i class="fa fa-arrow-down"></i>&nbsp; {{trans('action.hide')}}
                                                </button>
                                            </div>
                                        </form> 
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->

                            @else 

                            <button class="btn btn-success btn-xs" data-toggle="modal" data-target="#show-component-{{$component->id}}">
                                <i class="fa fa-arrow-up"></i>
                            </button>
                            <!-- Modal -->
                            <div class="modal fade" id="show-component-{{$component->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form class="ajax-form" action="{{url('manage/web/components/'.$component->id.'/show')}}" method="post">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                    &times;
                                                </button>
                                                <h4 class="modal-title" id="myModalLabel">{{trans('action.show')}}</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>{{trans('alert.sure_to_show_item', array('item' =>  $component->name))}}</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
                                                    {{trans('action.cancel')}}
                                                </button>
                                                <button type="submit" class="btn btn-sm btn-success">
                                                    <i class="fa fa-arrow-up"></i>&nbsp; {{trans('action.show')}}
                                                </button>
                                            </div>
                                        </form> 
                                    </div><!-- /.modal-content -->
                                </div><!-- /.modal-dialog -->
                            </div><!-- /.modal -->
                            @endif 

                            @endcan 

                            @if($component->no_of_items <=0)
                            @can("delete", 4)
                            <button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#delete-component-{{$component->id}}">
                                <i class="fa fa-trash"></i>
                            </button>
                            <!-- Modal -->
                            <div class="modal fade" id="delete-component-{{$component->id}}" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true" data-backdrop="static">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form class="ajax-form" action="{{url('manage/web/components/'.$component->id)}}" method="post">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="_method" value="delete">

                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                    &times;
                                                </button>
                                                <h4 class="modal-title" id="deleteModalLabel">{{trans('action.delete')}}</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>{{trans('alert.sure_to_delete_item', array('item' => $component->name))}}</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
                                                    {{trans('action.cancel')}}
                                                </button>
                                                <button type="submit" class="btn btn-sm btn-danger">
                                                    <i class="fa fa-trash"></i>&nbsp; {{trans('action.delete')}}
                                                </button>
                                            </div>
                                        </form> 
                                    </div>
                                </div>
                            </div>
                            @endcan 
                            @endif
                        </td>
                        @endif 
                    </tr>
                    @endforeach 
                </tbody>
            </table>
        </div>

        @else 

        <p class="alert alert-info">
            <i class="fa fa-info"></i>&nbsp;{{trans('alert.no_item_found', array('item' => trans('label.component')))}}
        </p>

        @endif
    </div>
</div>





