<div class="header">
    <div class="title"><i class="fa fa-lg fa-plus-square-o fa-fw"></i> {{trans('label.create_new_item', array('item' => trans('label.component')))}}</div>
    <div class="links">
        @can("read", 4)
        <a href="#web/components" class="btn btn-primary btn-sm"><i class="fa fa-list"></i> {{trans('label.list_of_items', array('item' => trans('label.component')))}}</a>
        @endcan
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <form action="{{url('manage/web/components')}}" id="custom-component-form" class="form-horizontal ajax-form" novalidate="novalidate" method="post" data-hash="web/components">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <div class="form-group" id="name">
                <label class="col-sm-2 control-label">{{trans('label.name')}}</label>
                <div class="col-sm-10">
                    <input type="text" class="form-control" name="name" placeholder="{{trans('label.name')}}">
                    <span class="name-help-block help-block hide"></span>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-2 control-label">{{trans('label.property')}}</label>
                <div class="col-sm-10">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover table-hover" id="component-property">
                            <thead>
                                <tr>
                                    <th with="10%">{{trans('label.name')}}</th>
                                    <th with="10%">{{trans('label.type')}}</th>
                                    <th with="60%">{{trans('label.options')}}</th>
                                    <th with="10%">{{trans('label.required')}}</th>
                                    <th with="10%">{{trans('label.action')}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>
                                        <div id="property-0-name">
                                            <input class="form-control" type="text" name="property[0][name]" placeholder="Property Name">
                                            <span class="property-0-name-help-block help-block hide"></span>
                                        </div>
                                    </td>
                                    <td> 
                                        <div id="property-0-type">
                                            <select class="component-property-type form-control" name="property[0][type]" data-item="0">
                                                <option value="text">Text</option>
                                                <option value="numeric">Numeric</option>
                                                <option value="textarea">Text Area</option>
                                                <option value="select">Select</option>
                                                <option value="checkbox">Check Box</option>
                                                <option value="radio">Radio</option>
                                                <option value="email">Email</option>
                                                <option value="date">Date</option>
                                                <option value="image">Image</option>
                                                <option value="link">Link</option>
                                            </select>
                                            <span class="property-0-type-help-block help-block hide"></span>
                                        </div>
                                    </td>
                                    <td>
                                        <div id="property-0-options">
                                            <input class="form-control hide" type="text" id="property-0-option-value" name="property[0][options]" placeholder="Options">
                                            <span class="property-0-options-help-block help-block hide"></span>
                                        </div>
                                    </td>
                                    <td>
                                        <label><input type="checkbox" name="property[0][required]"> Yes</label>
                                    </td>
                                    <td>
                                        <input type="hidden" name="property[0][visible]" value="1">
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="button" id="add-component-property" data-item="1" class="btn btn-success btn-sm"><i class="fa fa-plus"></i>&nbsp; {{trans('label.create_new_item', array('item' => trans('label.property')))}}</button>
                </div>
            </div>

            @can("create", 4)
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary btn-sm"> 
                        <i class="fa fa-save fa-fw"></i> {{trans('action.save')}}
                    </button>
                </div>
            </div>
            @endcan 
        </form>

    </div>
</div>







