@extends('layouts.web')

@section('content')


<!--=== Breadcrumbs ===-->
<div class="breadcrumbs" style="background: #dff1ff;">
    <div class="container">
        <h1 class="pull-left" style="text-transform: uppercase; font-weight: bold; color: #008bdc;">{{$post->title}}</h1>
    </div>
</div><!--/breadcrumbs-->
<!--=== End Breadcrumbs ===-->

<!--=== Blog Posts ===-->
<div class="bg-color-light" >
    <div class="container content-sm" style="padding-top: 15px;">
        <div class="row">
            <!-- Blog All Posts -->
            <div class="col-md-9">
                <!-- News v3 -->
                <div class="news-v3 bg-color-white margin-bottom-30">

                    <div class="news-v3-in" id="page-content">

                        {!! nl2br($post->content) !!}

                    </div>
                </div>
                <!-- End News v3 -->


            </div>
            <!-- End Blog All Posts -->

            <!-- Blog Sidebar -->
            <div class="col-md-3">

                <div class="headline-v2"><h2>Trending</h2></div>

                @if(count(web::posts("news"))>0)

                <!-- Latest Links -->
                <ul class="list-unstyled blog-latest-posts margin-bottom-50">

                    @foreach(web::posts("news") as $news)

                    <li>
                        <h3><a href="{{url('post/'.$news->slug)}}">{{$news->title}}</a></h3>
                        <small>Posted at {{date('d F Y', strtotime($news->created_at))}}</small>

                    </li>

                    @endforeach 

                    @else 

                    <p>Nothing trending!</p>

                    @endif

                </ul>
                <!-- End Latest Links -->



            </div>
            <!-- End Blog Sidebar -->
        </div>
    </div><!--/end container-->
</div>
<!--=== End Blog Posts ===-->

@endsection
