@extends('layouts.web')

@section('content')

<!--=== Breadcrumbs ===-->
<div class="breadcrumbs" style="background: #dff1ff;">
    <div class="container">
        <h1 class="pull-left" style="text-transform: uppercase; font-weight: bold; color: #008bdc;">Contact Us</h1>
    </div>
</div><!--/breadcrumbs-->
<!--=== End Breadcrumbs ===-->

<!--=== Content Part ===-->
<div class="container content">
    <div class="row margin-bottom-60">
        <div class="col-md-6 col-sm-6" style="overflow: hidden;">
            
            <!-- Google Map -->
            <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2482.880524550334!2d-0.06423098422972166!3d51.51540787963647!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x48761ccce9267eab%3A0xb1e19238b0d8ebce!2sNew+Rd%2C+London+E1+1HE!5e0!3m2!1sen!2suk!4v1478211870211" width="555" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            <!-- End Google Map -->
        </div>
        <div class="col-md-6 col-sm-6">
            <!-- Get in Touch -->
            <h3>Get in Touch</h3>
            @if(web::component("contact-us"))<p>{{web::component("contact-us")->text}}</p><br/>@endif
            

            <!-- Contacts -->
            <div class="headline"><h2>Contacts</h2></div>
            <ul class="list-unstyled who margin-bottom-30">
                <li><i class="fa fa-home"></i>&nbsp;@if(web::settings("address"))  {{web::settings("address")}} @endif
                    @if(web::settings("street"))  <{{web::settings("street")}}, @endif
                    @if(web::settings("city"))  {{web::settings("city")}}, @endif
                    @if(web::settings("postcode"))  {{web::settings("postcode")}} @endif</li>
                @if(web::settings("email"))  <li><i class="fa fa-envelope-o"></i>&nbsp;<a href="mailto:{{web::settings("email")}}">{{web::settings("email")}}</a></li> @endif
                @if(web::settings("phone"))  <li><i class="fa fa-phone"></i>&nbsp;{{web::settings("phone")}}</li> @endif
                @if(web::settings("fax"))  <li><i class="fa fa-fax"></i>&nbsp;{{web::settings("fax")}}</li> @endif

            </ul>

            

            <!-- Business Hours -->
            <div class="headline"><h2>Business Hours</h2></div>
            <ul class="list-unstyled margin-bottom-30">
                @if(web::settings("open_monday_to_friday"))<li><strong>Monday-Friday:</strong> {{web::settings("open_monday_to_friday")}}</li> @endif
                @if(web::settings("open_saturday"))<li><strong>Saturday:</strong> {{web::settings("open_saturday")}}</li> @endif
                @if(web::settings("open_sunday"))<li><strong>Sunday:</strong> {{web::settings("open_sunday")}}</li> @endif
            </ul>
        </div>
    </div>
</div>
@endsection
