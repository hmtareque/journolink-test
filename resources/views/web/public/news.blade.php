@extends('layouts.web')

@section('content')

<!--=== Breadcrumbs ===-->
<div class="breadcrumbs" style="background: #dff1ff;">
    <div class="container">
        <h1 class="pull-left" style="text-transform: uppercase; font-weight: bold; color: #008bdc;">News</h1>
    </div>
</div><!--/breadcrumbs-->
<!--=== End Breadcrumbs ===-->

<!--=== Blog Posts ===-->
<div class="bg-color-light">
    <div class="container content-sm" style="padding-top: 15px;">
        <div class="row">
            <!-- Blog All Posts -->
            <div class="col-md-9">


                <!-- News v3 -->
                <div class="news-v3 bg-color-white margin-bottom-30">

                    <div class="news-v3-in" style="font-size: 1.15em;">
                        
                        <h2 class="headline">Recent News</h2>
                        
                        @if(count(web::posts("news"))>0)
                        
                        @foreach(web::posts("news") as $news)
                        
                        <!-- Begin Inner Results -->
                        <div class="inner-results">
                            <h3><a href="{{url('post/'.$news->slug)}}">{{$news->title}}</a></h3>
                            <ul class="list-inline down-ul">
                                <li>Posted at {{date('d F Y', strtotime($news->created_at))}}</li>
                            </ul>
                        </div>
                        <!-- Begin Inner Results -->

                        @endforeach 
                        
                        @else 
                        
                        <p>
                            http://www.banglamirrornews.com/2015/04/blackstones-solicitors-launching-ceremony-held/
                        </p>

                        @endif 

                    </div>
                </div>
                <!-- End News v3 -->


            </div>
            <!-- End Blog All Posts -->
            
            <div class="col-md-3">
                <!-- Contacts -->
                <div class="headline"><h2>Contacts</h2></div>
                <ul class="list-unstyled who margin-bottom-30">
                    @if(web::settings("address"))<li>{{web::settings("address")}}</li> @endif
                    @if(web::settings("street"))<li>{{web::settings("street")}}</li> @endif
                    @if(web::settings("city"))<li>{{web::settings("city")}}</li> @endif
                    @if(web::settings("postcode"))<li>{{web::settings("postcode")}}</li> @endif
                    @if(web::settings("email"))<li><i class="fa fa-envelope-o"></i>&nbsp;<a href="mailto:{{web::settings("email")}}">{{web::settings("email")}}</a></li> @endif
                    @if(web::settings("phone"))<li><i class="fa fa-phone"></i>&nbsp;{{web::settings("phone")}}</li> @endif
                    @if(web::settings("fax"))<li><i class="fa fa-fax"></i>&nbsp;{{web::settings("fax")}}</li> @endif

                </ul>

                <!-- Business Hours -->
                <div class="headline"><h2>Business Hours</h2></div>
                <ul class="list-unstyled margin-bottom-30">
                    @if(web::settings("open_monday_to_friday"))<li><strong>Monday-Friday:</strong> {{web::settings("open_monday_to_friday")}}</li> @endif
                    @if(web::settings("open_saturday"))<li><strong>Saturday:</strong> {{web::settings("open_saturday")}}</li> @endif
                    @if(web::settings("open_sunday"))<li><strong>Sunday:</strong> {{web::settings("open_sunday")}}</li> @endif
                </ul>

            </div><!--/col-md-3-->

          
        </div>
    </div><!--/end container-->
</div>
<!--=== End Blog Posts ===-->

@endsection
