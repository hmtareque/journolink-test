<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>App</title>



        

        <!-- Bootstrap -->
        <link href="{{asset('css/app.css')}}" rel="stylesheet">
        
        <style>
            
            .app-name {
                float: left; 
                width: 230px; 
                height: 50px; 
                line-height: 50px; 
                background: #2aabd2; 
                font-weight: bold; 
                font-size: 1.5em; 
                color: #fff;
            }
            
            .content-area {
                margin-top: 50px;
                margin-left: 230px; 
                min-height: 1650px; 
                border-left: 1px solid #ddd;
              
            }

            .sidebar {
                position: fixed;
                top: 50px;
                left: 0px;
                width: 230px;
                height: 650px;
                z-index: 1000;
                display: block;
                padding: 0px 0px;
                overflow-x: hidden;
                overflow-y: auto; /* Scrollable contents if viewport is shorter than content. */
                background-color: #f5f5f5;
                


            }
            

            .sidebar-list {
                list-style: none;
                margin: 0px;
                padding: 0px;
                
            }

            .sidebar-list > li {
                margin: 0px;
                padding: 0px;
               
            }

            .sidebar-list > li > a {
                display: block;
                overflow: hidden;
                padding: 10px 15px;
                line-height: 15px;
                vertical-align: middle;
                color: #337ab7;
                text-decoration: none;
                border-bottom: 1px solid #ddd;
            }
            
            .sidebar-list li a:hover {
                text-decoration: none;
                color: #000;
                background: #b9def0;
            }
            
            
            .dropdown-menu > li > a:hover {
                background: #2aabd2;
                color: #fff;
            }
            

            .sidebar-list > li > ul {
                list-style: none;
                margin: 0px;
                padding: 0px;
                background: #e6f5ff;
                display: none;
            }

            .sidebar-list > li > ul li {
                margin: 0px;
                padding: 0px;
               
            }

           .sidebar-list > li > ul li a {
                display: block;
                overflow: hidden;
                padding: 6px 15px;
                color: #000;
                text-decoration: none;
                border-bottom: 1px solid #d9edf7; 
            }
            
            .sidebar-list > li > ul li a:hover {
                background: #2aabd2;
                color: #fff;
            }

            .sidebar-sublist li:last-child  a {
                border-bottom: 1px solid #ddd;
            }
            
            .sidebar-sublist li a:hover {
                background: #b9def0;
            }
            
            
            
            
            
            /* Small devices (tablets, 768px and up) */
@media (max-width: 768px) { 
    
    .app-name {
        
        background: #337ab7;
    }
    
    .sidebar {
        display: none;
        border-right: 1px solid #ddd;
    }
    
    .content-area {
        margin-left: 0px;
        border-left: none;
    }
}


        </style>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <header style="position: fixed; top: 0; height: 50px; z-index: 10000; width: 100%;">
            <div style="position: fixed; top: 0; margin-left: 0px;"> <span style="margin-left: 10px;">APP NAME</span> </div>
            <div style="position: fixed; top: 0; left: 230px; background: #337ab7;">
                
                <div class="container-fluid">
                
                
                <div class="row">
                    <div class="col-xs-8">
                           <ul class="nav navbar-nav top-navbar hidden-xs">
                  <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i> Menu 1 <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">Action</a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="#">Separated link</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="#">One more separated link</a></li>
                    </ul>
                  </li>

                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bicycle"></i> Menu 2 <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">Action</a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="#">Separated link</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="#">One more separated link</a></li>
                    </ul>
                  </li>

                  <li class="dropdown hidden-sm">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-unlock"></i> Menu 3 <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">Action</a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="#">Separated link</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="#">One more separated link</a></li>
                    </ul>
                  </li>
                </ul>
                    </div>
                    
                    
                    <div class="col-xs-4">
                        
                         <ul class="nav navbar-nav navbar-right top-navbar hidden-xs">
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-tasks" style="font-size: 1.0em; color: #fff;"></i><sup style="background: green; padding: 2px 5px; color: #fff; font-size: 0.7em; border-radius: 10px; margin-left: -2px; margin-top: -10px;">5</sup></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">Action</a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="#">Separated link</a></li>
                    </ul>
                  </li>

                  <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-envelope" style="font-size: 1.0em; color: #fff;"></i> <sup style="background: orange; padding: 2px 5px; color: #fff; font-size: 0.7em; border-radius: 10px; margin-left: -7px; margin-top: -10px;">5</sup></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">Action</a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="#">Separated link</a></li>
                    </ul>
                  </li>

                  <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bell" style="font-size: 1.0em; color: #fff;"></i> <sup style="background: orangered; padding: 2px 5px; color: #fff; font-size: 0.7em; border-radius: 10px; margin-left: -8px; margin-top: -10px;">5</sup></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">Action</a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="#">Separated link</a></li>
                    </ul>
                  </li>

                  <li class="dropdown hidden-sm">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i> Profile <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">Action</a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="#">Separated link</a></li>
                    </ul>
                  </li>

                  <li><a href="#" style="color: red;"><i class="fa fa-sign-out"></i></a></li>
                </ul>
                        
                        
                    </div>
                </div>
                </div>
                
                
                
            
            <div style="float: right; height: 50px; line-height: 50px;" class="visible-xs">
                <button class="btn btn-info" id="navicon"><i class="fa fa-navicon"></i></button>
            </div>
                
            </div>
                
        </header>
        
        
        
        
        

     
            <div class="sidebar" id="main-sidebar">
                @include('app.shared.sidebar')
            </div>
            <div class="content-area" style="background: #fff;">
                <div>
                    @yield('content')
                </div>
            </div>

            <div  style="background: #f5f5f5; height: 100px; border-top: 1px solid #ddd;">

                <div class="">footer left</div>
                <div class="">footer right</div>

            </div>
     




<script src="{{asset('js/app.js')}}"></script>

<script>
    $("#navicon").on('click', function(){
        $("#main-sidebar").toggle(300);
    });

     $(".content-area").on('click', function(){
        toggleSidebar();
    });
    
    $(window).on('resize', function(){
       toggleSidebar();
    });
    
    
    $(document).on('click', '.sidebar-list > li > a.cl-1', function(){

        $('.sidebar-list li > a').children('i').removeClass('fa-minus');
        $('.sidebar-list li > a').children('i').addClass('fa-plus');
        $('.sidebar-list ul').slideUp(600);
        
        $('.sidebar-list li > a').addClass('cl-1');
         $('.sidebar-list li > a').removeClass('cl-2');
        $(this).removeClass('cl-1');
        $(this).addClass('cl-2');
        
        $(this).siblings('ul').slideDown(600);
        $(this).children('i').removeClass('fa-plus');
        $(this).children('i').addClass('fa-minus');
        
        
        
        
    });
    
    
    $(document).on('click', '.sidebar-list > li > a.cl-2', function(){
        
        $(this).removeClass('cl-2');
        $(this).addClass('cl-1');
     
        $(this).siblings('ul').slideUp(600);
        $(this).children('i').removeClass('fa-minus');
        $(this).children('i').addClass('fa-plus');

    });
    
    
    
    function toggleSidebar(){
        
        
         var screen = $(window); //this = window
         
        // alert(screen.width());
        
        if (screen.width() <= 768) {
          
        $("#main-sidebar").hide(300);
        
        } else {
            $("#main-sidebar").show(300);
        }
    }
    
    
</script>
</body>
</html>
