<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="base_url" content="{{url('/manage')}}">
        <meta name="csrf_token" content="{{csrf_token()}}">
        <meta name="env" content="{{ config('app.env') }}">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>{{ config('settings.name') }}</title>

        <link href="{{asset('css/app-requisite.css')}}" rel="stylesheet">
        <link href="{{asset('css/app.css')}}" rel="stylesheet">
        
        <!--Load the AJAX API-->
    <script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
    <script type="text/javascript">

//combo
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawVisualization);

      function drawVisualization() {
        // Some raw data (not necessarily accurate)
        var data = google.visualization.arrayToDataTable([
         ['Month', 'Bolivia', 'Ecuador', 'Madagascar', 'Papua New Guinea', 'Rwanda', 'Average'],
         ['2004/05',  165,      938,         522,             998,           450,      614.6],
         ['2005/06',  135,      1120,        599,             1268,          288,      682],
         ['2006/07',  157,      1167,        587,             807,           397,      623],
         ['2007/08',  139,      1110,        615,             968,           215,      609.4],
         ['2008/09',  136,      691,         629,             1026,          366,      569.6]
      ]);

    var options = {
      title : 'Monthly Coffee Production by Country',
      vAxis: {title: 'Cups'},
      hAxis: {title: 'Month'},
      seriesType: 'bars',
      series: {5: {type: 'line'}}
    };

    var chart = new google.visualization.ComboChart(document.getElementById('chart_div'));
    chart.draw(data, options);
  }
  
  // pie 
  
  google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart);

      function drawChart() {

        var data = google.visualization.arrayToDataTable([
          ['Task', 'Hours per Day'],
          ['Work',     11],
          ['Eat',      2],
          ['Commute',  2],
          ['Watch TV', 2],
          ['Sleep',    7]
        ]);

        var options = {
          title: 'My Daily Activities'
        };

        var chart = new google.visualization.PieChart(document.getElementById('piechart'));

        chart.draw(data, options);
      }
      
      // line 
      
      google.charts.load('current', {'packages':['corechart']});
      google.charts.setOnLoadCallback(drawChart2);

      function drawChart2() {
        var data = google.visualization.arrayToDataTable([
          ['Year', 'Sales', 'Expenses'],
          ['2004',  1000,      400],
          ['2005',  1170,      460],
          ['2006',  660,       1120],
          ['2007',  1030,      540]
        ]);

        var options = {
          title: 'Company Performance',
          curveType: 'function',
          legend: { position: 'bottom' }
        };

        var chart2 = new google.visualization.LineChart(document.getElementById('curve_chart'));

        chart2.draw(data, options);
      }
      
      
    </script>
        
        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <header>
            @include('app.shared.header')
        </header>
        
        <aside id="sidebar">
            @include('app.shared.sidebar')
        </aside>
        
        <div class="main">
            <div class="heading">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12 col-sm-12 col-md-6">
                            <h2 class="title" id="page-name"><i class="fa fa-lg fa-dashboard fa-fw"></i> Dashboard</h2>
                            <h2 class="title hide" id="preloader"><i class="fa fa-lg fa-spinner fa-pulse fa-fw"></i> {{trans('label.loading')}}</h2>
                        </div>
                        <div class="hidden-xs hidden-sm col-md-6">
                            <div class="row">
                                <div class="col-xs-4 info" style="border-left: 1px solid #ddd; min-height: 50px;">Education<br/><strong class="text-success">72</strong></div>
                                <div class="col-xs-4 info" style="border-left: 1px solid #ddd; min-height: 50px;">Family<br/><strong class="text-warning">13</strong></div>
                                <div class="col-xs-4 info" style="border-left: 1px solid #ddd; min-height: 50px;">Advocacy<br/><strong class="text-danger">51</strong></div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="content">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12">
                            <div id="xcontent">
                                @yield('content')
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="footer">
                <div class="container-fluid">
                    <div class="row">
                        <div class="col-xs-12 col-sm-7 col-md-6">
                            <div class="left-side">
                                
                                <div class="app-copyright">The {{ config('settings.app_name') }} &COPY; {{date('Y')}}</div>
                                <div class="exetie">The system is design and developed by {{ config('settings.dev_name') }}</div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-5 col-md-6">
                            <div class="right-side">
                                <div class="last-app-activity">Last account activity  52 mins ago</div>
                                <a class="btn btn-success btn-xs btn-help" href="{{url('/')}}" role="button"><i class="fa fa-hand-pointer-o"></i> Help</a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script type="text/javascript" src="{{asset('js/app-requisite.js')}}"></script>
        <script src="{{asset('js/tinymce/tinymce.min.js')}}"></script> 
        <script type="text/javascript" src="{{asset('js/app.js')}}"></script>
</body>
</html>
