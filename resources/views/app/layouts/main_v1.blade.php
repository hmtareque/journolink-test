<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>App</title>



        

        <!-- Bootstrap -->
        <link href="{{asset('css/app.css')}}" rel="stylesheet">
        
        <style>

            .sidebar {
                margin: 0px !important;
                padding: 0px !important;

            }

            .sidebar-list {
                list-style: none;
                margin: 0px;
                padding: 0px;
            }

            .sidebar-list li {
                margin: 0px;
                padding: 0px;
               
            }

            .sidebar-list li a {
                display: block;
                overflow: hidden;
                padding: 10px 15px;
                width: 100%;
                border-bottom: 1px solid #ddd;
            }
            
            .sidebar-list li a:hover {
                text-decoration: none;
                color: #000;
                background: #f2f2f2;
            }
            
            
            .dropdown-menu > li > a:hover {
                background: #2aabd2;
                color: #fff;
            }



        </style>

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>

        <header style="margin-top: 50px;">
            @include('app.shared.nav')
        </header>
        
        
        

        <div class="container-fluid" style="border-bottom: 1px solid #ddd;"z
            <div class="row">
                <div class="col-xs-12 col-sm-4 col-md-3 col-lg-2 sidebar" style="min-height: 51px;">
                    @include('app.shared.sidebar')
                </div>
                <div class="col-xs-12 col-sm-8 col-md-9 col-lg-10" style="min-height: 600px; border-left: 1px solid #ddd;">
                    <div class="content" style="padding: 0px;">
                        @yield('content')
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="container-fluid" style="background: #f5f5f5; height: 100px;">
    <div class="row">
        <div class="col-xs-12 col-sm-6">footer left</div>
        <div class="col-xs-12 col-sm-6">footer right</div>
    </div>
</div>

<script src="{{asset('js/app.js')}}"></script>
</script>
</body>
</html>
