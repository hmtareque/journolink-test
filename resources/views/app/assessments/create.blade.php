<div class="header">
    <div class="title"><i class="fa fa-lg fa-cubes fa-fw"></i> Assessments</div>
    <div class="links">
        <a href="#assessments" class="btn btn-success btn-sm"><i class="fa fa-list-alt fa-fw"></i> All Assessments</a>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-8">

        <form class="form-horizontal ajax-form" action="{{url('manage/education/students')}}" method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>


            <h4 class="section-title">Assessment</h4>

            <div class="form-group" id="service">
                <label class="col-sm-3 control-label">Service Area</label>
                <div class="col-sm-9">
                    <select class="form-control" name="service">
                        <option value=""> -- Please Select -- </option>
                       
                    </select>
                </div>
            </div>
            
            <div class="form-group" id="service">
                <label class="col-sm-3 control-label">Service</label>
                <div class="col-sm-9">
                    <select class="form-control" name="service">
                        <option value=""> -- Please Select -- </option>
                       
                    </select>
                </div>
            </div>

            <div class="form-group" id="category">
                <label class="col-sm-3 control-label">Category</label>
                <div class="col-sm-9">
                    <select class="form-control" name="category">
                        <option value=""> -- Please Select -- </option>
                       
                    </select>
                </div>
            </div>
            
            <div class="form-group" id="first_name">
                <label class="col-sm-3 control-label">Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="first_name" placeholder="Name">
                    <span class="help-block hide"></span>
                </div>
            </div>


            <div class="form-group" id="description">
                <label class="col-sm-3 control-label">Description</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="description"></textarea>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-primary"> 
                        <i class="fa fa-save fa-fw"></i> Save
                    </button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-xs-12 col-md-4">
        @include('app.shared.feed')
    </div>
</div>

