<div class="header">
    <div class="title"><i class="fa fa-lg fa-question-circle-o fa-fw"></i> Assessments</div>
    <div class="links">
        <a href="#assessments/create" class="btn btn-primary btn-sm"><i class="fa fa-edit fa-fw"></i> Update Assessment</a>
        <a href="#assessments" class="btn btn-success btn-sm"><i class="fa fa-list-alt fa-fw"></i> All Assessments</a>
    </div>
</div>


<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-9">

        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Assessment</h3>
            </div>
            <div class="panel-body">
                
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <ul class="list-unstyled">
                    <li><strong>Name</strong> : Assessment Name</li>
                    <li><strong>Description</strong> <p>This is assessment description</p></li>
                </ul>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <ul class="list-unstyled">
                    <li><strong>Area</strong> : Family</li>
                    <li><strong>Service</strong> : Housing</li>
                    <li><strong>Category</strong> : Money</li>
                </ul>
                    </div>
                </div>
                
                
            </div>
        </div>

        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Measures</h3>
            </div>
            <div class="panel-body">
                
                <ul class="list-unstyled list-group" id="sortable">
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-xs-6">Motivation and confidence</div>
                            <div class="col-xs-4"><small>no progress, started to progress, significant progress</small></div>
                            <div class="col-xs-2">
                                <a href="" class="btn btn-primary btn-xs"><i class="fa fa-edit fa-fw"></i>edit</a>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-xs-6">Social integration and resilience</div>
                            <div class="col-xs-4"><small>no progress, started to progress, significant progress</small></div>
                            <div class="col-xs-2">
                                <a href="" class="btn btn-primary btn-xs"><i class="fa fa-edit fa-fw"></i>edit</a>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-xs-6">English language and communication skills</div>
                            <div class="col-xs-4"><small>no progress, started to progress, significant progress</small></div>
                            <div class="col-xs-2">
                                <a href="" class="btn btn-primary btn-xs"><i class="fa fa-edit fa-fw"></i>edit</a>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-xs-6">Increased mental wellbeing</div>
                            <div class="col-xs-4"><small>no progress, started to progress, significant progress</small></div>
                            <div class="col-xs-2">
                                <a href="" class="btn btn-primary btn-xs"><i class="fa fa-edit fa-fw"></i>edit</a>
                            </div>
                        </div>
                    </li>
                    <li class="list-group-item">
                        <div class="row">
                            <div class="col-xs-6">Did you vote Donald Trump</div>
                            <div class="col-xs-4">
                                <small>yes, no</small>
                                <label class="label label-success">other</label>
                                <label class="label label-danger">explain</label>
                            </div>
                            <div class="col-xs-2">
                                <a href="" class="btn btn-primary btn-xs"><i class="fa fa-edit fa-fw"></i>edit</a>
                            </div>
                        </div>
                    </li>
                </ul>
                
                
                
                 <div class="collapse margin-top-5" id="note-form">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Add New Measure</div>
                        <div class="panel-body">
                            <form class="ajax-form" action="{{url('')}}" method="post">
                                <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                <div class="form-group" id="note">
                                    <label for="">Measure</label>
                                        <textarea name="note" class="form-control"></textarea>
                                        <span class="help-block hide"></span>
                                    
                                </div>
                                <div class="form-group" id="note">
                                    <label for="">Type</label>
                                    <select name="type" class="form-control">
                                        <option> -- Select -- </option>
                                        <option>Text</option>
                                        <option>Options</option>
                                        <option>Options with Other</option>
                                    </select>
                                    <span class="help-block hide"></span>
                                </div>
                                <div class="form-group" id="note">
                                    <label for="">Options</label>
                                        <textarea name="note" class="form-control"></textarea>
                                        <span class="help-block hide"></span>
                                        <label class="label label-danger">Must be separated by comma(,)</label>
                                </div>
                                <div class="form-group" id="note">
                                    <label for="">Need Explanation</label>
                                    <select name="type" class="form-control">
                                        <option>No</option>
                                        <option>Yes</option>
                                    </select>
                                    <span class="help-block hide"></span>
                                </div>
                                <div class="form-group">
                                        <button type="submit" class="btn btn-primary"> 
                                            <i class="fa fa-save fa-fw"></i> Save
                                        </button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
                
                
                
            </div>
            <div class="panel-footer">
                <a class="btn btn-xs btn-success" role="button" data-toggle="collapse" href="#note-form" aria-expanded="false" aria-controls="collapseExample">
                    <i class="fa fa-plus fa-fw"></i> Add New Measure
                </a>
                    <a href="" class="btn btn-primary btn-xs"><i class="fa fa-list-ol fa-fw"></i>Save Order</a>
            
            </div>
        </div>

     

    </div>
    <div class="col-md-3 col-xs-12">
        @include('app.shared.feed')
    </div>
</div>

<script type="text/javascript">
    //sorting lists 
$(function () {
    $("#sortable").sortable();
    $("#sortable").disableSelection();
});
</script>
