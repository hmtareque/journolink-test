<div class="header">
    <div class="title"><i class="fa fa-lg fa-question-circle-o fa-fw"></i> Assessments</div>
    <div class="links">
        <a href="#assessments/create" class="btn btn-success btn-sm"><i class="fa fa-plus fa-fw"></i> Create New Assessment</a>
    </div>
</div>

<div class="row">
    <div class="col-md-9 col-xs-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Assessments</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table data-table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th width="55%">Assessment</th>
                                <th width="15%">Area</th>
                                <th width="15%">Service</th>
                                <th width="15%">Category</th>          
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><a href="#assessments/1">Assessment Name 1</a></td>
                                <td>All</td>
                                <td>--</td>
                                <td>--</td>
                            </tr>
                            <tr>
                                <td><a href="#assessments/1">Assessment Name 2</a></td>
                                <td>Family</td>
                                <td>Housing</td>
                                <td>Money</td>
                            </tr>
                            <tr>
                                <td><a href="#assessments/1">Assessment Name 3</a></td>
                                <td>Advocacy</td>
                                <td>Immigration Advice</td>
                                <td>--</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
    <div class="col-md-3 col-xs-12">
        @include('app.shared.feed')
    </div>
</div>




