<div class="header">
    <div class="title"><i class="fa fa-lg fa-cubes fa-fw"></i> Resources</div>
    <div class="links">
        <a href="" class="btn btn-success btn-sm"><i class="fa fa-plus fa-fw"></i> Create New Resource</a>
    </div>
</div>

<div class="row">
    <div class="col-md-9 col-xs-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Notices</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table data-table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>Resource</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <div>Resource 1</div>
                                    <div>
                                        <ul class="list-unstyled list-inline">
                                            <li><a href="">link 1</a></li>
                                            <li><a href="">link 2</a></li>
                                            <li><a href="">link 3</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>

                            <tr>
                                <td>
                                    <div>Resource 2</div>
                                    <div>
                                        <ul class="list-unstyled list-inline">
                                            <li><a href="">link 1</a></li>
                                            <li><a href="">link 2</a></li>
                                        </ul>
                                    </div>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
    <div class="col-md-3 col-xs-12">
        @include('app.shared.feed')
    </div>
</div>




