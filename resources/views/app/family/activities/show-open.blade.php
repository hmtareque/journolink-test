<div class="header">
    <div class="title"><i class="fa fa-lg fa-book fa-fw"></i> Activity</div>
    <div class="links">
        <a href="#family/activities/1/edit" class="btn btn-success btn-sm"><i class="fa fa-edit fa-fw"></i> Update Activity</a>
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Cases <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#family/cases"><i class="fa fa-list"></i> All Cases</a></li>
                <li><a href="#family/cases/create"><i class="fa fa-plus"></i> Create New Case</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-8">
        
        <div class="row">
            <div class="col-xs-12">
                
                  <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Activity</h3>
                    </div>
                    <div class="panel-body">
                        
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <ul class="list-unstyled">
                                <li><strong>Service</strong> : Youth</li>
                                <li><strong>Category</strong> : -- </li>
                                <li><strong>Activity</strong> : Higher Education Workshop</li>
                                <li><strong>Topic</strong> : Higher Education</li>
                            </ul>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <ul class="list-unstyled">
                                <li><strong>Registration</strong> : <label class="label label-success">not required</label></li>
                                <li><strong>Recurrence</strong> : Weekly</li>
                            </ul>
                            </div>
                        </div>
                        
                            
                    </div>
                </div>
                
                
                
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Occurrence</h3>
                    </div>
                    <div class="panel-body">
                        
                         <div class="table-responsive">
                    <table class="table data-table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>Date</th>
                                <th colspan="4" class="text-center">No of Participants</th>
                                <th>Note</th>
                                <th>Assessment</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td></td>
                                <td class="text-center">Male</td>
                                <td class="text-center">Female</td>
                                <td class="text-center">Other</td>
                                <td class="text-center">Total</td>
                                <td></td>
                                <td></td>
                            </tr>

                            <tr>
                                <td><a href="">03/11/1998</a></td>
                                <td class="text-center">12</td>
                                <td class="text-center">5</td>
                                <td class="text-center">1</td>
                                <td class="text-center">18</td>
                                <td><a href="" class="btn btn-xs btn-default btn-block"><i class="fa fa-info-circle fa-fw"></i>note</a></td>
                                <td>
                                    <a href="" class="btn btn-xs btn-primary btn-block"><i class="fa fa-bar-chart fa-fw"></i>assessment</a>
                                </td>
                            </tr>

                            <tr>
                                <td><a href="">03/11/1998</a></td>
                                <td class="text-center">8</td>
                                <td class="text-center">5</td>
                                <td class="text-center">1</td>
                                <td class="text-center">14</td>
                                <td><a href="" class="btn btn-xs btn-default btn-block"><i class="fa fa-info-circle fa-fw"></i>note</a></td>
                                <td>
                                    <a href="" class="btn btn-xs btn-warning btn-block"><i class="fa fa-plus fa-fw"></i>assess</a>
                                </td>
                            </tr>

                        </tbody>
                    </table>
                </div>


                    </div>
                    <div class="panel-footer">
                        <a href="#family/activities/1/occurances/create" class="btn btn-success btn-xs"><i class="fa fa-plus fa-fw"></i>Register New Occurrence</a>
                        <a href="#family/activities/1/occurances" class="btn btn-primary btn-xs"><i class="fa fa-wrench fa-fw"></i>Manage Occurrence</a>
                    </div>
                </div>
            </div>
        </div>
        
    
     
           
        
     
          <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Actions</h3>
            </div>
            <div class="panel-body">
                
                
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>Action Types</th>
                                <th class="text-center">Taken</th>
                                <th class="text-right">Time Spent (min)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>One to One Session</td>
                                <td class="text-center">2</td>
                                <td class="text-right">70min</td>
                            </tr>

                            <tr>
                                 <td>Weekly Football Activity</td>
                                <td class="text-center">5</td>
                                <td class="text-right">30min</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Total</th>
                                <th class="text-center">7</th>
                                <th class="text-right">1 hour 40 min</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                
                
                
                <h4 class="section-title">Recent Actions</h4>
                
                
                
                <div class="panel panel-success">
                    <div class="panel-heading">Attended mentoring session</div>
                    <div class="panel-body">A prominent Thai human rights lawyer faces a prison term of up to 150 years if convicted of royal defamation under Thailand's royal insult law, according to a legal watchdog.</div>
                    <div class="panel-footer">Created By Mamudu Abudu At 12/09/2017 6:25pm</div>
                </div>

                <div class="panel panel-success">
                    <div class="panel-heading">College support</div>
                    <div class="panel-body">
                        It was not clear what exactly Prawet, 57, wrote on Facebook, but the AFP news agency said in one of the recent posts he encouraged Thais to push the boundaries of the lese majeste law.
                        <ul class="list-unstyled list-inline">
                            <li><i class="fa fa-paperclip fa-fw"></i> <a href="">image</a></li>
                            <li><i class="fa fa-paperclip fa-fw"></i> <a href="">excel</a></li>
                            <li><i class="fa fa-paperclip fa-fw"></i> <a href="">pdf</a></li>
                        </ul>
                    </div>
                    <div class="panel-footer">Created By Hussain Tawhid At 17/03/2017 6:25pm</div>
                </div>

                <div class="panel panel-success">
                    <div class="panel-heading">ESOL support</div>
                    <div class="panel-body">The small bronze monument, which lay in Bangkok's heavily-policed Royal Plaza, marked the 1932 revolution that ended absolute monarchy in Thailand.</div>
                    <div class="panel-footer">Created By Tahmid Ahmed At 12/05/2017 6:25pm</div>
                </div>

                <div class="collapse margin-top-5" id="note-form">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Create New Action</div>
                        <div class="panel-body">
                            <form class="form-horizontal ajax-form" action="{{url('')}}" method="post">
                                <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                
                                <div class="form-group" id="type">
                                    <div class="col-xs-12">
                                        <select name="type" class="form-control">
                                            <option> -- Please Select -- </option>
                                            <option>Action</option>
                                        </select>
                                        <span class="help-block hide"></span>
                                    </div>
                                </div>
                                <div class="form-group" id="note">
                                    <div class="col-xs-12">
                                        <textarea name="note" class="form-control" placeholder="Description"></textarea>
                                        <span class="help-block hide"></span>
                                    </div>
                                </div>
                                <div class="form-group" id="time_spent">
                                    <div class="col-xs-12">
                                        <input type="text" class="form-control" name="time_spent" placeholder="Time Spent"/>
                                        <span class="help-block hide"></span>
                                    </div>
                                </div>
                                <div class="form-group" id="files">
                                    <div class="col-xs-12">
                                        <input type="file" class="btn btn-sm btn-default" name="files[]" multiple=""/>
                                        <span class="help-block hide"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <button type="submit" class="btn btn-primary"> 
                                            <i class="fa fa-save fa-fw"></i> Save
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>


            </div>
            <div class="panel-footer">
                <a class="btn btn-xs btn-success" role="button" data-toggle="collapse" href="#note-form" aria-expanded="false" aria-controls="collapseExample">
                    <i class="fa fa-plus fa-fw"></i> Create New Action
                </a>

                <a href="#family/cases/1/actions" class="btn btn-primary btn-xs"><i class="fa fa-wrench fa-fw"></i>Manage Actions</a>

            </div>
        </div>
      
        

    </div>
    
    <div class="col-xs-12 col-sm-12 col-md-4">
 
                <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Facilitator(s)</h3>
            </div>
            <div class="panel-body">
                <ul class="list-unstyled">
                    <li><strong>Person(s)</strong> : Kate Duffy</li>
                    <li><strong>Organisation(s)</strong> : Paiwand</li>
                </ul>
            </div>
        </div>
        
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Partner(s)</h3>
            </div>
            <div class="panel-body">
                <p>Harrow Council</p>
            </div>
        </div>
        
        <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Location</h3>
                    </div>
                    <div class="panel-body">
                        <ul class="list-unstyled">
                                <li>23 Love Lane</li>
                                <li>Heart Street</li>
                                <li>Mindland</li>
                                <li>LV5 8VM</li>
                            </ul>
                    </div>
                </div>


          <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Recent Documents</h3>
                    </div>
                    <div class="panel-body">
                        <ul class="list-unstyled">
                            <li><i class="fa fa-file-o fa-fw"></i><a href="#">Document 1</a></li>
                            <li><i class="fa fa-file-o fa-fw"></i><a href="#">Document 2</a></li>
                            <li><i class="fa fa-file-o fa-fw"></i><a href="#">Document 3</a></li>
                        </ul>
                        
                         <div class="collapse margin-top-5" id="doc-form">
                            <div class="panel panel-primary">
                                <div class="panel-heading">Upload New Doc</div>
                                <div class="panel-body">
                                    <form class="form-horizontal ajax-form" action="{{url('')}}" method="post">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                        <div class="form-group" id="note">
                                            <div class="col-xs-12">
                                                <input type="text" name="name" class="form-control" placeholder="File Name"/>
                                                <span class="help-block hide"></span>
                                            </div>
                                        </div>
                                        <div class="form-group" id="files">
                                            <div class="col-xs-12">
                                                <input type="file" class="btn btn-sm btn-default" name="files[]" multiple=""/>
                                                <span class="help-block hide"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <button type="submit" class="btn btn-primary"> 
                                                    <i class="fa fa-upload fa-fw"></i> Upload
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="panel-footer">
                        <a class="btn btn-xs btn-success" role="button" data-toggle="collapse" href="#doc-form" aria-expanded="false" aria-controls="collapseExample">
                            <i class="fa fa-upload fa-fw"></i> Upload New Doc
                        </a>
                        <a class="btn btn-xs btn-primary" href="#family/cases/1/docs"><i class="fa fa-wrench fa-fw"></i>Manage Documents</a>
                       
                    </div>
                </div>
      
    </div>

</div>







