<div class="header">
    <div class="title"><i class="fa fa-lg fa-book fa-fw"></i> Activities</div>
    <div class="links">
        <a href="#family/activities/create" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Create New Activity</a>
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Cases <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#family/cases"><i class="fa fa-list"></i> All Cases</a></li>
                <li><a href="#family/cases/create"><i class="fa fa-plus"></i> Create New Case</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-9 col-xs-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Active Activities</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th colspan="6">Youth</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="info">
                                <th width="10%">Ref. No.</th>
                                <th width="25%">Activity</th>
                                <th width="25%">Topic</th>
                                <th width="10%">Registration</th>
                                <th width="10%">Recurrence</th>
                                <th width="10%">Attendees</th>
                            </tr>
                            <tr>
                                <td> <a href="#family/activities/1">KD0018976</a></td>
                                <td>Higher Education Workshop</td>
                                <td>Higher Education</td>
                                <td class="text-center">required</td>
                                <td class="text-center">daily</td>
                                <td class="text-center">8</td>
                            </tr>
                            <tr>
                                <td> <a href="#family/activities/2">BK987650</a></td>
                                <td>Music Class</td>
                                <td>Classical Afghan Music</td>
                                <td class="text-center">not required</td>
                                <td class="text-center">weekly</td>
                                <td class="text-center">--</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                
            
                
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th colspan="6">Employability</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="info">
                                <th width="10%">Ref. No.</th>
                                <th width="25%">Activity</th>
                                <th width="25%">Topic</th>
                                <th width="10%">Registration</th>
                                <th width="10%">Recurrence</th>
                                <th width="10%">Attendees</th>
                            </tr>
                            <tr>
                                <td><a href="#family/activities/1">KD0018976</a></td>
                                <td>CV Writing</td>
                                <td>Employability</td>
                                <td class="text-center">required</td>
                                <td class="text-center">weekly</td>
                                <td class="text-center">25</td>
                            </tr>
                            <tr>
                                <td><a href="#family/activities/3">KD0018976</a></td>
                                <td>Interview Technique</td>
                                <td>Employability</td>
                                <td class="text-center">required</td>
                                <td class="text-center">once</td>
                                <td class="text-center"> -- </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
    <div class="col-md-3 col-xs-12">
        @include('app.shared.feed')
    </div>
</div>




