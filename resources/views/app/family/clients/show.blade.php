<div class="header">
    <div class="title"><i class="fa fa-lg fa-user-circle fa-fw"></i> Client</div>
    <div class="links">
        <a href="#family/cases/1/edit" class="btn btn-primary btn-sm"><i class="fa fa-edit fa-fw"></i> Update Client</a>
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Cases <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#family/cases"><i class="fa fa-list"></i> All Cases</a></li>
                <li><a href="#family/cases/create"><i class="fa fa-plus"></i> Create New Case</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-8">

        <div class="row">
            <div class="col-xs-12">


                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Client Details</h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-xs-12 col-md-4">
                            <div class="panel panel-success">
                                <div class="panel-body">
                                    <img style="max-height: auto; width: 100%;" class="img-responsive img-rounded" src="{{asset('img/test/beckwith.png')}}"/>
                                </div>
                                <div class="panel-footer">
                                    <form class="form-inline ajax-upload-form"  method="POST" action="{{url('manage-files')}}" enctype="multipart/form-data" data-id="asset--files">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                        <div style="margin: 0px;" class="file-upload btn btn-xs btn-success">
                                            <span id="upload-status"><i class="fa fa-refresh fa-fw"></i>Update Picture</span>
                                            <input type="hidden" name="folder" value=""/>
                                            <input type="file" name="files" class="upload" multiple=""/>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-8">
                            <ul class="list-unstyled">
                                <li><strong>Name</strong> : Christine  <strong style="text-transform: uppercase;">Gill</strong></li>
                                <li><strong>Date of Birth</strong> : 19/04/1992</li>
                                <li><strong>Gender</strong> : <i class="fa fa-female fa-fw text-danger"></i>Femail</li>
                            </ul>

                            <div class="row">
                                <div class="col-xs-12 col-md-5">
                                    <ul class="list-unstyled">
                                        <li><strong>Address</strong></li>
                                        <li>23 Love Lane</li>
                                        <li>Heart Street</li>
                                        <li>Mindland</li>
                                        <li>LV5 8VM</li>
                                        <li><strong>Harrow</strong></li>
                                    </ul>
                                </div>
                                <div class="col-xs-12 col-md-7">
                                    <ul class="list-unstyled">
                                        <li><strong>Contact</strong></li>
                                        <li>Phone : 0203 153 1076</li>
                                        <li>Mobile : 075 3535 1616</li>
                                        <li>Email : <a href="mailto:hasan@exetie.com">hasan@exetie.com</a></li>
                                    </ul>
                                </div>
                            </div>

                            <ul class="list-unstyled">
                                <li><strong>Social Worker</strong> : Jim Fitz (contact: 07965 454567)</li>
                                <li><strong>Local Authority</strong> Harrow</li>
                            </ul>

                            <ul class="list-unstyled">
                                <li><strong>Education Institute</strong> : Westic College</li>
                                <li><strong>First Language</strong> : Arabic</li>
                                <li><strong>Level of ESOL</strong> : <label class="label label-warning">Inlive</label></li>
                                <li><strong>Ethnicity</strong> : Yameni</li>
                                <li><strong>Religion</strong> : <label class="label label-danger">not disclosed</label></li>
                            </ul>

                            <ul class="list-unstyled">
                                <li><strong><i class="fa fa-info-circle text-info fa-fw"></i></strong>Heard about Paiwand by Newspaper</li>
                                <li><strong><i class="fa fa-check text-success fa-fw"></i></strong>Has consent to appear in publicity photos</li>
                                <li><strong><i class="fa fa-close text-danger fa-fw"></i></strong>Has consent to appear in publicity videos</li>
                                <li><strong><i class="fa fa-check text-success fa-fw"></i></strong>Subscribe Newsletter</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Open Cases</h3>
            </div>
            <div class="panel-body">


                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>Case 1</th>
                                <th>Category</th>
                                <th class="text-center">Actions Taken</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><a href="#family/cases/1">Case 1</a></td>
                                <td class="text-center">Housing</td>
                                <td class="text-center">22</td>
                            </tr>
                            <tr>
                                <td><a href="#family/cases/1">Case 2</a></td>
                                <td class="text-center">Employability</td>
                                <td class="text-center">22</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
            <div class="panel-footer">

                <a class="btn btn-xs btn-primary" href="#family/clients/1/cases"><i class="fa fa-wrench fa-fw"></i>Manage Cases</a>

            </div>
        </div>


        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Registered in Activities</h3>
            </div>
            <div class="panel-body">


                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>Activity</th>
                                <th class="text-center">Held</th>
                                <th class="text-center">Attended</th>
                                <th class="text-center">Attendance</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><a href="#family/activities/1">Activity 1</a></td>
                                <td class="text-center">32</td>
                                <td class="text-center">22</td>
                                <td class="text-center">
                                    <a href="" class="btn btn-primary btn-xs"><i class="fa fa-calendar-check-o fa-fw"></i>attendances</a>
                                </td>
                            </tr>

                            <tr>
                                <td><a href="#family/activities/1">Activity 2</a></td>
                                <td class="text-center">32</td>
                                <td class="text-center">30</td>
                                <td class="text-center">
                                    <a href="" class="btn btn-primary btn-xs"><i class="fa fa-calendar-check-o fa-fw"></i>attendances</a>
                                </td>
                            </tr>

                            <tr>
                                <td><a href="#family/activities/1">Activity 3</a></td>
                                <td class="text-center">32</td>
                                <td class="text-center">9</td>
                                <td class="text-center">
                                    <a href="" class="btn btn-primary btn-xs"><i class="fa fa-calendar-check-o fa-fw"></i>attendances</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
            <div class="panel-footer">

                <a class="btn btn-xs btn-primary" href="#family/clients/1/activities"><i class="fa fa-wrench fa-fw"></i>Manage Activities</a>

            </div>
        </div>


        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Assessments</h3>
            </div>
            <div class="panel-body">


                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>Assessment</th>
                                <th class="text-center">Taken</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Self Harm</td>
                                <td class="text-center">2</td>
                                <td class="text-center">
                                    <a href="#family/cases/1/assessments/1/compare" class="btn btn-primary btn-xs"><i class="fa fa-bar-chart-o fa-fw"></i>compare</a>
                                </td>
                            </tr>

                            <tr>
                                <td>Violent Thoughts</td>
                                <td class="text-center">1</td>
                                <td class="text-center">
                                    <a href="#family/cases/1/assessments/1/compare" class="btn btn-primary btn-xs"><i class="fa fa-bar-chart-o fa-fw"></i>compare</a>
                                </td>
                            </tr>

                            <tr>
                                <td>Employability Prospect</td>
                                <td class="text-center">2</td>
                                <td class="text-center">
                                    <a href="#family/cases/1/assessments/1/compare" class="btn btn-primary btn-xs"><i class="fa fa-bar-chart-o fa-fw"></i>compare</a>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
            <div class="panel-footer">

                <a class="btn btn-xs btn-success" href="#family/cases/1/assessments/types"><i class="fa fa-plus fa-fw"></i>Take New Assessment</a>
                <a href="#family/cases/1/assessments" class="btn btn-primary btn-xs"><i class="fa fa-wrench fa-fw"></i>Manage Assessments</a>

            </div>
        </div>



        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Actions</h3>
            </div>
            <div class="panel-body">


                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>Action Types</th>
                                <th class="text-center">Taken</th>
                                <th class="text-right">Time Spent (min)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>One to One Session</td>
                                <td class="text-center">2</td>
                                <td class="text-right">70min</td>
                            </tr>

                            <tr>
                                <td>Weekly Football Activity</td>
                                <td class="text-center">5</td>
                                <td class="text-right">30min</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Total</th>
                                <th class="text-center">7</th>
                                <th class="text-right">1 hour 40 min</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>



                <h4 class="section-title">Recent Actions</h4>



                <div class="panel panel-success">
                    <div class="panel-heading">Attended mentoring session</div>
                    <div class="panel-body">A prominent Thai human rights lawyer faces a prison term of up to 150 years if convicted of royal defamation under Thailand's royal insult law, according to a legal watchdog.</div>
                    <div class="panel-footer">Created By Mamudu Abudu At 12/09/2017 6:25pm</div>
                </div>

                <div class="panel panel-success">
                    <div class="panel-heading">College support</div>
                    <div class="panel-body">
                        It was not clear what exactly Prawet, 57, wrote on Facebook, but the AFP news agency said in one of the recent posts he encouraged Thais to push the boundaries of the lese majeste law.
                        <ul class="list-unstyled list-inline">
                            <li><i class="fa fa-paperclip fa-fw"></i> <a href="">image</a></li>
                            <li><i class="fa fa-paperclip fa-fw"></i> <a href="">excel</a></li>
                            <li><i class="fa fa-paperclip fa-fw"></i> <a href="">pdf</a></li>
                        </ul>
                    </div>
                    <div class="panel-footer">Created By Hussain Tawhid At 17/03/2017 6:25pm</div>
                </div>

                <div class="panel panel-success">
                    <div class="panel-heading">ESOL support</div>
                    <div class="panel-body">The small bronze monument, which lay in Bangkok's heavily-policed Royal Plaza, marked the 1932 revolution that ended absolute monarchy in Thailand.</div>
                    <div class="panel-footer">Created By Tahmid Ahmed At 12/05/2017 6:25pm</div>
                </div>

                <div class="collapse margin-top-5" id="note-form">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Create New Action</div>
                        <div class="panel-body">
                            <form class="form-horizontal ajax-form" action="{{url('')}}" method="post">
                                <input type="hidden" name="_token" value="{{csrf_token()}}"/>

                                <div class="form-group" id="type">
                                    <div class="col-xs-12">
                                        <select name="type" class="form-control">
                                            <option> -- Please Select -- </option>
                                            <option>Action</option>
                                        </select>
                                        <span class="help-block hide"></span>
                                    </div>
                                </div>
                                <div class="form-group" id="note">
                                    <div class="col-xs-12">
                                        <textarea name="note" class="form-control" placeholder="Description"></textarea>
                                        <span class="help-block hide"></span>
                                    </div>
                                </div>
                                <div class="form-group" id="time_spent">
                                    <div class="col-xs-12">
                                        <input type="text" class="form-control" name="time_spent" placeholder="Time Spent"/>
                                        <span class="help-block hide"></span>
                                    </div>
                                </div>
                                <div class="form-group" id="files">
                                    <div class="col-xs-12">
                                        <input type="file" class="btn btn-sm btn-default" name="files[]" multiple=""/>
                                        <span class="help-block hide"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <button type="submit" class="btn btn-primary"> 
                                            <i class="fa fa-save fa-fw"></i> Save
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>


            </div>
            <div class="panel-footer">
                <a class="btn btn-xs btn-success" role="button" data-toggle="collapse" href="#note-form" aria-expanded="false" aria-controls="collapseExample">
                    <i class="fa fa-plus fa-fw"></i> Create New Action
                </a>

                <a href="#family/cases/1/actions" class="btn btn-primary btn-xs"><i class="fa fa-wrench fa-fw"></i>Manage Actions</a>

            </div>
        </div>



    </div>

    <div class="col-xs-12 col-sm-12 col-md-4">

        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Next of Kin</h3>
            </div>
            <div class="panel-body">
                <ul class="list-unstyled">
                    <li><strong>Fake Fakerson</strong> (Husband)</li>
                    <li>23 Love Lane</li>
                    <li>Heart Street</li>
                    <li>Mindland</li>
                    <li>LV5 8VM</li>
                    <li>Phone : 0203 153 1076</li>
                    <li>Mobile : 075 3535 1616</li>
                    <li>Email : <a href="mailto:hasan@exetie.com">hasan@exetie.com</a></li>
                </ul>
            </div>
        </div>

        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Recent Payments</h3>
            </div>
            <div class="panel-body">

                <ul class="list-unstyled">
                    <li><i class="fa fa-money fa-fw"></i> 12/09/2016 -- &pound;25.00</li>
                    <li><i class="fa fa-money fa-fw"></i> 12/09/2016 -- &pound;10.00</li>
                    <li><i class="fa fa-money fa-fw"></i> 12/09/2016 -- &pound;15.00</li>
                </ul>

                <div class="btn btn-danger btn-sm">Due: &pound;17.00</div>

            </div>
            <div class="panel-footer">
                <a class="btn btn-xs btn-success" href="#education/students/1/edit"><i class="fa fa-plus fa-fw"></i>Make Payment</a>
                <a class="btn btn-xs btn-primary" href="#education/students/1/edit"><i class="fa fa-file-excel-o fa-fw"></i>Account</a>
            </div>
        </div>



        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Immigration Status</h3>
            </div>
            <div class="panel-body">
                <ul class="list-unstyled">
                    <li><strong>Status</strong> : Refugee</li>
                    <li>Arrived in the UK : 12/09/2016</li>
                </ul>

                <ul class="list-unstyled">
                    <li><strong>Documents</strong></li>
                    <li><i class="fa fa-file-o fa-fw"></i><a href="#">Document 1</a></li>
                    <li><i class="fa fa-file-o fa-fw"></i><a href="#">Document 2</a></li>
                </ul>
            </div>
            <div class="panel-footer">
                <a class="btn btn-xs btn-primary" href="#education/students/1/edit"><i class="fa fa-edit fa-fw"></i>Update</a>
            </div>
        </div>



        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Medical Information</h3>
            </div>
            <div class="panel-body">
                <strong>Medical Conditions</strong>
                <p>Thrush can affect other areas of skin, such as the armpits, groin and between the fingers. This usually causes a red, itchy or painful rash that scales over with white or yellow discharge. The rash may not be so obvious on darker skin.</p>
                <ul class="list-unstyled">
                    <li><strong>Medical Condition Documents</strong></li>
                    <li><i class="fa fa-file-o fa-fw"></i><a href="#">Document 1</a></li>
                    <li><i class="fa fa-file-o fa-fw"></i><a href="#">Document 2</a></li>
                </ul>

                <strong>Allergies</strong>
                <p>Allergies, also known as allergic diseases, are a number of conditions caused by hypersensitivity of the immune system to something in the environment that usually causes little or no problem in most people. </p>
                <ul class="list-unstyled">
                    <li><strong>Allergy Documents</strong></li>
                    <li><i class="fa fa-file-o fa-fw"></i><a href="#">Document 1</a></li>
                    <li><i class="fa fa-file-o fa-fw"></i><a href="#">Document 2</a></li>
                </ul>
            </div>
            <div class="panel-footer">
                <a class="btn btn-xs btn-primary" href="#education/students/1/edit"><i class="fa fa-edit fa-fw"></i>Update</a>
            </div>
        </div>





        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Recent Documents</h3>
            </div>
            <div class="panel-body">
                <ul class="list-unstyled">
                    <li><i class="fa fa-file-o fa-fw"></i><a href="#">Document 1</a></li>
                    <li><i class="fa fa-file-o fa-fw"></i><a href="#">Document 2</a></li>
                    <li><i class="fa fa-file-o fa-fw"></i><a href="#">Document 3</a></li>
                </ul>

                <div class="collapse margin-top-5" id="doc-form">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Upload New Doc</div>
                        <div class="panel-body">
                            <form class="form-horizontal ajax-form" action="{{url('')}}" method="post">
                                <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                <div class="form-group" id="note">
                                    <div class="col-xs-12">
                                        <input type="text" name="name" class="form-control" placeholder="File Name"/>
                                        <span class="help-block hide"></span>
                                    </div>
                                </div>
                                <div class="form-group" id="files">
                                    <div class="col-xs-12">
                                        <input type="file" class="btn btn-sm btn-default" name="files[]" multiple=""/>
                                        <span class="help-block hide"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <button type="submit" class="btn btn-primary"> 
                                            <i class="fa fa-upload fa-fw"></i> Upload
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>

            </div>
            <div class="panel-footer">
                <a class="btn btn-xs btn-success" role="button" data-toggle="collapse" href="#doc-form" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-upload fa-fw"></i> Upload New Doc</a>
                <a class="btn btn-xs btn-primary" href="#family/cases/1/docs"><i class="fa fa-wrench fa-fw"></i>Manage Documents</a>
            </div>
        </div>
    </div>
</div>







