<div class="header">
    <div class="title"><i class="fa fa-lg fa-users fa-fw"></i> Clients</div>
    <div class="links">
        <a href="#family/clients/create" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Create New Client</a>
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Cases <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#family/cases"><i class="fa fa-list"></i> All Cases</a></li>
                <li><a href="#family/cases/create"><i class="fa fa-plus"></i> Create New Case</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-9 col-xs-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Clients</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table data-table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th width="25%">Name</th>
                                <th width="25%" class="text-center">No of Cases</th>
                                <th width="30%" class="text-center">Activities</th>
                                <th width="10%" class="text-center">Borough</th>
                                <th width="10%" class="text-center">Contact</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td> <a href="#family/clients/1">Kate Deen</a></td>
                                 <td class="text-center">3</td>
                                <td class="text-center">7</td>
                                <td>Barnet</td>
                                <td>07965 786543</td>
                               
                            </tr>
                            <tr>
                                <td> <a href="#family/clients/1">Bimol Kumar</a></td>
                                <td class="text-center">1</td>
                                <td class="text-center"><label class="label label-danger">none</label></td>
                                <td>Harrow</td>
                                <td>0203 567 9876</td>
                                
                            </tr>
                            <tr>
                                <td> <a href="#family/clients/1">Mike Jonson</a></td>
                                <td class="text-center">1</td>
                                <td class="text-center">2</td>
                                <td>Harrow</td>
                                <td><a href="mailto:email@domain.com">email@domain.com</a></td>
                                
                            </tr>
                            <tr>
                                <td> <a href="#family/clients/1">Sopie Patrick</a></td>
                                <td class="text-center"><label class="label label-danger">none</label></td>
                                <td class="text-center">2</td>
                                <td>Ealing</td>
                                <td>07953 678543</td>
                                
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
    <div class="col-md-3 col-xs-12">
        @include('app.shared.feed')
    </div>
</div>




