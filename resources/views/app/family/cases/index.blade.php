<div class="header">
    <div class="title"><i class="fa fa-lg fa-book fa-fw"></i> Cases</div>
    <div class="links">
        <a href="#family/cases/create" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Create New Case</a>
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Activities <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#family/activities"><i class="fa fa-list"></i> All Activities</a></li>
                <li><a href="#family/activities/create"><i class="fa fa-plus"></i> Create New Activity</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-9 col-xs-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Cases In Progress</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th colspan="6">Youth</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="info">
                                <th width="25%">Ref. No.</th>
                                <th width="25%">Category</th>
                                <th width="30%">Client</th>
                                <th width="10%" class="text-center">Assessments</th>
                                <th width="10%" class="text-center">Actions</th>
                            </tr>
                            <tr>
                                <td> <a href="#family/cases/1">KD0018976</a></td>
                                <td> -- </td>
                                <td>Kate Deen</td>
                                <td class="text-center">3</td>
                                <td class="text-center">7</td>
                            </tr>
                            <tr>
                                <td> <a href="#family/cases/1">BK987650</a></td>
                                <td> -- </td>
                                <td>Bimol Kumar</td>
                                <td class="text-center">1</td>
                                <td class="text-center"><label class="label label-danger">none</label></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th colspan="6">Housing</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="info">
                                <th width="25%">Ref. No.</th>
                                <th width="25%">Category</th>
                                <th width="30%">Client</th>
                                <th width="10%" class="text-center">Assessments</th>
                                <th width="10%" class="text-center">Actions</th>
                            </tr>
                            <tr>
                                <td> <a href="#family/cases/1">MK98765</a></td>
                                <td>Money</td>
                                <td>Mike Jonson</td>
                                <td class="text-center">1</td>
                                <td class="text-center">2</td>
                            </tr>
                            <tr>
                                <td> <a href="#family/cases/1">SP002876</a></td>
                                <td>Life Skill</td>
                                <td>Sopie Patrick</td>
                                <td class="text-center"><label class="label label-danger">none</label></td>
                                <td class="text-center">2</td>
                            </tr>
                            <tr>
                                <td> <a href="#family/cases/1">DT007654</a></td>
                                <td>Health</td>
                                <td>Donald Trump</td>
                                <td class="text-center"><label class="label label-danger">none</label></td>
                                <td class="text-center"><label class="label label-danger">none</label></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th colspan="6">Employability</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="info">
                                <th width="25%">Ref. No.</th>
                                <th width="25%">Category</th>
                                <th width="30%">Client</th>
                                <th width="10%" class="text-center">Assessments</th>
                                <th width="10%" class="text-center">Actions</th>
                            </tr>
                            <tr>
                                <td> <a href="#family/cases/1">TM001234</a></td>
                                <td>Future Preparation</td>
                                <td>Teresa May</td>
                                <td class="text-center">3</td>
                                <td class="text-center">5</td>
                            </tr>
                            <tr>
                                <td> <a href="#family/cases/1">CASEREF</a></td>
                                <td>Future Unleash</td>
                                <td>Jeremy Corbin</td>
                                <td class="text-center">3</td>
                                <td class="text-center">7</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
    <div class="col-md-3 col-xs-12">
        @include('app.shared.feed')
    </div>
</div>




