<div class="header">
    <div class="title"><i class="fa fa-lg fa-users fa-fw"></i> Beneficiaries</div>
    <div class="links">
        <a href="" class="btn btn-success btn-sm"><i class="fa fa-plus fa-fw"></i> Create New Beneficiary</a>
        <a href="" class="btn btn-primary btn-sm"><i class="fa fa-search fa-fw"></i> Search Beneficiary</a>
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Actions <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href=""><i class="fa fa-list"></i> Action 1</a></li>
                <li><a href=""><i class="fa fa-plus"></i> Action 2</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-4">
        <div class="panel panel-primary">
            <div class="panel-heading">Info Title</div>
            <div class="panel-body">Info Body</div>
            <div class="panel-footer"><a href="" class="btn btn-primary btn-xs"><i class="fa fa-search fa-fw"></i> Info Link</a></div>
        </div>
    </div>
    <div class="col-xs-12 col-md-4">
        <div class="panel panel-primary">
            <div class="panel-heading">Info Title</div>
            <div class="panel-body">Info Body</div>
            <div class="panel-footer"><a href="" class="btn btn-primary btn-xs"><i class="fa fa-search fa-fw"></i> Info Link</a></div>
        </div>
    </div>
    <div class="col-xs-12 col-md-4">
        <div class="panel panel-primary">
            <div class="panel-heading">Info Title</div>
            <div class="panel-body">Info Body</div>
            <div class="panel-footer"><a href="" class="btn btn-primary btn-xs"><i class="fa fa-search fa-fw"></i> Info Link</a></div>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Recent Beneficiaries</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table data-table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th width="15%">Name</th>
                                <th width="15%">Engagements</th>
                                <th width="20%">Attributes</th>
                                <th width="25%">Address</th>
                                <th width="25%">Contact</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>
                                    <a href="#family/clients/1">Kate Deen</a>
                                    <br/>BM00165743
                                    <br/><label class="label label-danger">engaged</label>
                                </td>
                                <td>
                                    <div>Classes</div>
                                    <div>Cases</div>
                                    <div>Activities</div>
                                </td>
                                <td>Arrived In the UK<br/>Firt Language<br/>Ethicity<br/>Religion</td>
                                <td>Address Line 1<br/> Street <br/> Postcode <br/> Borough</td>
                                
                                <td>Phone: 00203 478 6543<br/>Mobile: 07965 786543<br/>Email: email@domain.com</td>
                               
                            </tr>
                            <tr>
                                <td> <a href="#family/clients/1">Bimol Kumar</a>
                                    <br/>BM00165743</td>
                               <td>
                                    <div>Classes</div>
                                    <div>Cases</div>
                                    <div>Activities</div>
                                </td>
                                <td>Arrived In the UK<br/>Firt Language<br/>Ethicity<br/>Religion</td>
                                <td>Address Line 1<br/> Street <br/> Postcode <br/> Borough</td>
                                
                                <td>Phone: 00203 478 6543<br/>Mobile: 07965 786543<br/>Email: email@domain.com</td>
                                
                            </tr>
                            <tr>
                                <td> <a href="#family/clients/1">Mike Jonson</a><br/>BM00165743
                                <br/><label class="label label-success">advanced</label></td>
                                <td>
                                    <div>Classes</div>
                                    <div>Cases</div>
                                    <div>Activities</div>
                                </td>
                                <td>Arrived In the UK<br/>Firt Language<br/>Ethicity<br/>Religion</td>
                                <td>Address Line 1<br/> Street <br/> Postcode <br/> Borough</td>
                                
                                <td>Phone: 00203 478 6543<br/>Mobile: 07965 786543<br/>Email: email@domain.com</td>
                                
                            </tr>
                            <tr>
                                <td> <a href="#family/clients/1">Sopie Patrick</a><br/>BM00165743</td>
                                <td>
                                    <div>Classes</div>
                                    <div>Cases</div>
                                    <div>Activities</div>
                                </td>
                                <td>Arrived In the UK<br/>Firt Language<br/>Ethicity<br/>Religion</td>
                                <td>Address Line 1<br/> Street <br/> Postcode <br/> Borough</td>
                                
                                <td>Phone: 00203 478 6543<br/>Mobile: 07965 786543<br/>Email: email@domain.com</td>
                                
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
</div>




