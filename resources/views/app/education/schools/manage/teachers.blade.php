<div class="header">
    <div class="title"><i class="fa fa-lg fa-wrench fa-fw"></i> Manage {{$school->name}} Teachers</div>
    <div class="links">
        @can("read", 4)
        <a href="#education/schools/{{$school->id}}" class="btn btn-success btn-sm"><i class="fa fa-info-circle fa-fw"></i> {{$school->name}}</a>
        @endcan 
    </div>
</div>


<div class="row">
    <div class="col-xs-12 col-md-8">
        
        <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->

   
        <div class="row">
            <div class="col-xs-12">

                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Teachers</h3>
                    </div>
                    <div class="panel-body">

                        @if($teachers->count()>0) 
                           
                        <div class="table-responsive">
                            <table class="table @if($teachers->count()>10) data-table @endif table-striped table-bordered table-hover table-condensed" width="100%">
                                <thead>
                                    <tr>
                                        <th width="30%">Teacher</th>
                                        <th width="45%">Teaching Class</th>
                                        <th width="25%">Contact</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($teachers as $teacher) 
                                    <tr>
                                        <td><a href="#education/teachers/{{$teacher->id}}">{{$teacher->first_name}} {{$teacher->last_name}}</a></td>
                                        <td>
                                            
                                            @if($teacher->classes->count()>0)
                                            <ul class="list-unstyled">
                                            @foreach($teacher->classes as $class)
                                            <li>
                                                
                                                {{$class->course}} - {{$class->group}} - {{$class->year}}
                                                 
                                                
                                                @if($class->status == 'teaching')
                                                <label class="label label-success">{{$class->status}}</label>
                                                @else 
                                                <label class="label label-warning">{{$class->status}}</label>
                                                @endif 
                                            </li>
                                            @endforeach 
                                            </ul>
                                            @else 
                                            <label class="text-danger">not teaching in any class!</label>
                                            @endif 
                                            
                                        </td>
                                        
                                        <td>
                                            <ul class="list-unstyled">
                                                @if($teacher->phone != '')<li><i class="fa fa-phone fa-fw"></i><small>{{$teacher->phone}}</small></li>@endif
                                                @if($teacher->mobile != '')<li><i class="fa fa-mobile-phone fa-fw"></i><small>{{$teacher->mobile}}</small></li>@endif
                                            </ul>
                                        </td>
                                        
                                    </tr>
                                    @endforeach 
                                </tbody>
                            </table>
                        </div>
                        @else 
                        <div class="alert alert-warning">
                            No teacher found in this moment!
                        </div>
                        @endif

                    </div>
                </div>

                
            </div>
        </div>
        
        
    </div>
    
<div class="col-xs-12 col-md-4"></div>
</div> 





