<div class="header">
    <div class="title"><i class="fa fa-lg fa-wrench fa-fw"></i> Manage {{$school->name}} Classes</div>
    <div class="links">
        @can("read", 4)
        <a href="#education/schools/{{$school->id}}" class="btn btn-success btn-sm"><i class="fa fa-info-circle fa-fw"></i> {{$school->name}}</a>
        @endcan 
    </div>
</div>


<div class="row">
    <div class="col-xs-12 col-md-9">
        
        <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->

        <div class="row">
            <div class="col-xs-12">

                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Classes in Progress</h3>
                    </div>
                    <div class="panel-body">

                        @if(count($classes->active)>0) 
                           
                        <div class="table-responsive">
                            <table class="table @if(count($classes->active)>10) data-table @endif table-striped table-bordered table-hover table-condensed" width="100%">
                                <thead>
                                    <tr>
                                        <th width="30%">Class</th>
                                        <th width="25%" class="text-center">No of Student(s)</th>
                                        <th width="45%">Assigned Teacher(s)</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($classes->active as $active_class) 
                                    <tr>
                                        <td><a href="#education/classes/{{$active_class->id}}">{{$active_class->course}} {{$active_class->group}}</a> <small>{{$active_class->year}}</small></td>
                                        <td class="text-center">{{$active_class->no_of_students}}</td>
                                        <td>
                                            @if($active_class->teachers->count()>0) 
                                            <ul class="list-unstyled">
                                                @foreach($active_class->teachers as $teacher)
                                                <li>
                                                    @if(Auth::user()->can("read", 7))
                                                    <a href="#education/teachers/{{$teacher->id}}">{{$teacher->first_name}} {{$teacher->middle_name}} {{$teacher->last_name}}</a> 
                                                    @else 
                                                    {{$teacher->first_name}} {{$teacher->middle_name}} {{$teacher->last_name}}
                                                    @endif 
                                                    
                                                    @if($teacher->status == 'teaching')
                                                    <label class="label label-success">{{$teacher->status}}</label>
                                                    @else 
                                                    <label class="label label-warning">{{$teacher->status}}</label>
                                                    @endif 
                                            </li>
                                                @endforeach 
                                            </ul>
                                            @else 
                                            <label class="label label-danger">none!</label>
                                            @endif 
                                        </td>
                                    </tr>
                                    @endforeach 
                                </tbody>
                            </table>
                        </div>
                        @else 
                        <div class="alert alert-warning">
                            No class found in this moment!
                        </div>
                        @endif

                    </div>
                </div>

                 <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Other Class(es)</h3>
                    </div>
                    <div class="panel-body">

                        @if(count($classes->other)>0) 
                           
                        <div class="table-responsive">
                            <table class="table @if(count($classes->other)>10) data-table @endif table-striped table-bordered table-hover table-condensed" width="100%">
                                <thead>
                                    <tr>
                                        <th width="20%">Academic Year</th>
                                        <th width="30%">Class</th>
                                        <th width="25%" class="text-center">No of Student(s)</th>
                                        <th width="25%" class="text-center">Status</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($classes->other as $other_class) 
                                    <tr>
                                        <td>{{$other_class->year}}</td>
                                        <td><a href="#education/classes/{{$other_class->id}}">{{$other_class->course}} {{$other_class->group}}</a></td>
                                        <td class="text-center">{{$other_class->no_of_students}}</td>
                                        <td class="text-center">
                                            @if($other_class->status == 'complete') 
                                            <label class="label label-success">{{$other_class->status}}</label>
                                            @else 
                                            <label class="label label-warning">{{$other_class->status}}</label>
                                            @endif 
                                        </td>
                                    </tr>
                                    @endforeach 
                                </tbody>
                            </table>
                            
                        </div>
                        @else 
                        <div class="alert alert-warning">
                            No class found in this moment!
                        </div>
                        @endif
                        
                     
                    </div>
                </div>
            </div>
        </div>
        
        
    </div>
<div class="col-xs-12 col-md-3"></div>
</div> 





