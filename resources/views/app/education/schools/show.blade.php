<div class="header">
    <div class="title"><i class="fa fa-lg fa-home fa-fw"></i> {{$school->name}}</div>
    <div class="links">

        @can("update", 4)
        <a href="#education/schools/{{$school->id}}/edit" class="btn btn-sm btn-primary"><i class="fa fa-edit fa-fw"></i>  Update {{$school->name}} Info</a>
        @endcan 

        @can("delete", 4)
        @if($school->active == 1) 
        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#deactivation-modal">
            <i class="fa fa-lock fa-fw"></i> Deactivate {{$school->name}}
        </button>
        @else 
        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#activation-modal">
            <i class="fa fa-unlock fa-fw"></i> Activate {{$school->name}}
        </button>
        @endif 
        @endcan 

    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-8">
        
        <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->


        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">School</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <h4 class="section-title">{{$school->name}}</h4>
                        <ul class="list-unstyled">
                            <li><strong>{{$school->primary_contact_person}}</strong></li>
                            <li>{{$school->address_line_1}}</li>
                            @if($school->address_line_2 != "")<li>{{$school->address_line_2}}</li>@endif
                            <li>{{$school->street}}</li>
                            <li>{{$school->city}}</li>
                            <li>{{$school->postcode}}</li>
                            <li>{{$school->borough}}</li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <h4 class="section-title">Contacts</h4>
                        <ul class="list-unstyled">
                            <li><strong>Phone</strong> : {{$school->phone}}</li>
                            <li><strong>Mobile</strong> : @if($school->mobile != "") {{$school->mobile}} @else -- @endif</li>
                            <li><strong>Email</strong> : <a href="mailto:{{$school->email}}">{{$school->email}}</a></li>
                            <li><strong>Fax</strong> : @if($school->fax != "") {{$school->fax}} @else -- @endif</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Active Students</h3>
            </div>
            <div class="panel-body">

                @if($school->students->count()>0) 

                <div class="table-responsive">
                    <table class="table @if($school->students->count()>10) data-table @endif table-striped table-bordered table-hover table-condensed" width="100%">
                        <thead>
                            <tr>
                                <th width="25%">Name</th>
                                <th width="20%">Date of Birth</th>
                                <th width="30%">Contacts</th>
                                <th width="25%" class="text-center">Studying in Class</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($school->students as $student) 
                            <tr>
                                <td><a href="#education/students/{{$student->client_id}}"><strong>{{$student->first_name}} {{$student->middle_name}} {{$student->last_name}}</strong></a></td>
                                <td>{{date('d M Y', strtotime($student->date_of_birth))}}</td>
                                <td>
                                    <small>
                                    @if($student->phone != "") 
                                    {{$student->phone}}
                                    @elseif($student->mobile != "") 
                                    {{$student->mobile}}
                                    @elseif($student->email != "") 
                                    <a href=="mailto:{{$student->email}}">{{$student->email}}</a>
                                    @endif
                                    </small>
                                </td>
                                
                                <td class="text-center">{{$student->studying_in_classes}}</td>
                            </tr>
                            @endforeach 
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="4">Total Active Students - {{$school->students->count()}}</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                @else 
                <div class="alert alert-warning">
                    No student studying in this school in this moment!
                </div>
                @endif
                
            </div>
        </div>

        <!-- Note Panel Start -->
        @include('app.shared.notes', ['module_id' => 4, 'notes' => $school->notes, 'object_type' => 'school', 'object_id' => $school->id, 'return' => 'manage/education/schools/'.$school->id])
        <!-- Note Panel End -->

    </div>
    <div class="col-xs-12 col-sm-12 col-md-4">
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Classes in Progress</h3>
                    </div>
                    <div class="panel-body">
                        @if($school->classes && $school->classes->count()>0)
                        <ul class="list-unstyled">
                            @foreach($school->classes as $class)
                            <li>
                                <i class="fa fa-book fa-fw"></i> 
                                @if(Auth::user()->can("read", 5))
                                <a href="#education/classes/{{$class->id}}">
                                    <strong>{{$class->course}}</strong>  <small>{{$class->group}} - {{$class->year}}</small>
                                </a>
                                @else
                                <strong>{{$class->course}}</strong>  <small>{{$class->group}} - {{$class->year}}</small>
                                @endif 
                            </li>
                            @endforeach 
                        </ul>
                        @else 
                        <p class="alert alert-warning"><i class="fa fa-warning"></i> No class is progressing!</p>
                        @endif 
                    </div>
                    @if(Auth::user()->can("update", 5) && $school->classes && $school->classes->count()>0)
                    <div class="panel-footer">
                        <a href="#education/schools/{{$school->id}}/classes/manage" class="btn btn-primary btn-xs"><i class="fa fa-wrench fa-fw"></i>Manage Classes</a>
                    </div>
                    @endif
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Teachers</h3>
                    </div>
                    <div class="panel-body">
                        @if($school->teachers->count()>0)
                        <ul class="list-unstyled">
                            @foreach($school->teachers as $teacher)
                            <li><i class="fa fa-user fa-fw"></i>
                                @if(Auth::user()->can("read", 7))
                                <a href="#education/teachers/{{$teacher->id}}">{{$teacher->first_name}} {{$teacher->last_name}}</a>
                                @else 
                                {{$teacher->first_name}} {{$teacher->last_name}}
                                @endif 
                            </li>
                            @endforeach 
                        </ul>
                        @else 
                        <p class="alert alert-warning"><i class="fa fa-warning"></i> No teacher found!</p>
                        @endif
                    </div>
                    @if(Auth::user()->can("read", 7) && $school->teachers->count()>0)
                    <div class="panel-footer">
                        <a href="#education/schools/{{$school->id}}/teachers/manage" class="btn btn-primary btn-xs"><i class="fa fa-wrench fa-fw"></i>Manage Teachers</a>
                    </div>
                    @endif 
                </div>
                
                @include('app.shared.docs', ['module_id' => 4, 'docs' => $school->docs, 'object_type' => 'school', 'object_id' => $school->id, 'folder' => 'school/'.$school->id, 'return' => 'manage/education/schools/'.$school->id]) 
                
            </div>
        </div>

    </div>
</div>


<!-- Deactivation Modal -->
<div class="modal fade" id="deactivation-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Deactivate {{$school->name}}</h4>
            </div>

            <form class="ajax-form" action="{{url('manage/education/schools/'.$school->id.'/deactivate')}}" method="post">
                <input type="hidden" name="_token" value="{{csrf_token()}}"/>

                <div class="modal-body">
                    <p>Are you really sure to deactivate the school? This will stop all operations of the school.</p>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-lock fa-fw"></i> Deactivate</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Deactivation Modal -->
<div class="modal fade" id="activation-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Activate {{$school->name}}</h4>
            </div>

            <form class="ajax-form" action="{{url('manage/education/schools/'.$school->id.'/activate')}}" method="post">
                <input type="hidden" name="_token" value="{{csrf_token()}}"/>

                <div class="modal-body">
                    <p>Are you really sure to activate the school? This will resume all operations of the school.</p>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-unlock fa-fw"></i> Activate</button>
                </div>
            </form>
        </div>
    </div>
</div>



