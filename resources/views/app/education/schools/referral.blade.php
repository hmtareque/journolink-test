<div class="header">
    <div class="title"><i class="fa fa-lg fa-home fa-fw"></i> {{$school->name}}</div>
    <div class="links">
        @can("update", 4)
         <a href="#education/schools/{{$school->id}}/edit" class="btn btn-sm btn-primary"><i class="fa fa-edit fa-fw"></i>  Update {{$school->name}} Info</a>
        @endif 
        
         @can("delete", 4)
         @if($school->active == 1) 
        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#deactivation-modal">
            <i class="fa fa-lock fa-fw"></i> Deactivate {{$school->name}}
        </button>
        @else 
        <button type="button" class="btn btn-success btn-sm" data-toggle="modal" data-target="#activation-modal">
            <i class="fa fa-unlock fa-fw"></i> Activate {{$school->name}}
        </button>
        @endif 
        @endif
        
        <a href="#education/schools" class="btn btn-sm btn-info"><i class="fa fa-list-alt fa-fw"></i> All Schools</a>

    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        
    </div>
</div>


<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-9">
        
        <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->
        
        
        <div class="row">
            <div class="col-xs-12">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">School</h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <h4 class="section-title">{{$school->name}}</h4>
                                <ul class="list-unstyled">
                                    <li><strong>{{$school->primary_contact_person}}</strong></li>
                                    <li>{{$school->address_line_1}}</li>
                                    @if($school->address_line_2 != "")<li>{{$school->address_line_2}}</li>@endif
                                    <li>{{$school->street}}</li>
                                    <li>{{$school->city}}</li>
                                    <li>{{$school->postcode}}</li>
                                    <li>{{$school->borough}}</li>
                                </ul>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <h4 class="section-title">Contacts</h4>
                                <ul class="list-unstyled">
                                    <li><strong>Phone</strong> : {{$school->phone}}</li>
                                    <li><strong>Mobile</strong> : @if($school->mobile != "") {{$school->mobile}} @else -- @endif</li>
                                    <li><strong>Email</strong> : <a href="mailto:{{$school->email}}">{{$school->email}}</a></li>
                                    <li><strong>Fax</strong> : @if($school->fax != "") {{$school->fax}} @else -- @endif</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>


         <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Active Students</h3>
            </div>
            <div class="panel-body">




                @if($school->students->count()>0) 

                <div class="table-responsive">
                    <table class="table @if($school->students->count()>10) data-table @endif table-striped table-bordered table-hover table-condensed" width="100%">
                        <thead>
                            <tr>
                                <th width="25%">Name</th>
                                <th width="20%">Date of Birth</th>
                                <th width="30%">Contacts</th>
                                <th width="25%" class="text-center">Studying in Class</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($school->students as $student) 
                            <tr>
                                <td><a href="#education/students/{{$student->client_id}}"><strong>{{$student->first_name}} {{$student->middle_name}} {{$student->last_name}}</strong></a></td>
                                <td>{{date('d M Y', strtotime($student->date_of_birth))}}</td>
                                <td>
                                    <small>
                                    @if($student->phone != "") 
                                    {{$student->phone}}
                                    @elseif($student->mobile != "") 
                                    {{$student->mobile}}
                                    @elseif($student->email != "") 
                                    <a href=="mailto:{{$student->email}}">{{$student->email}}</a>
                                    @endif
                                    </small>
                                </td>
                                <td class="text-center">{{$student->studying_in_classes}}</td>
                            </tr>
                            @endforeach 
                        </tbody>
                        <tfoot>
                            <tr>
                                <th colspan="4">Total Active Students - {{$school->students->count()}}</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                @else 
                <div class="alert alert-warning">
                    No active student found referred by {{$school->name}}.
                </div>
                @endif
            </div>
        </div>


    </div>
    <div class="col-xs-12 col-md-3">
        @include('app.shared.feed')
    </div>
</div>


<!-- Deactivation Modal -->
<div class="modal fade" id="deactivation-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Deactivate {{$school->name}}</h4>
            </div>

            <form class="ajax-form" action="{{url('manage/education/schools/'.$school->id.'/deactivate')}}" method="post">
                <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                
                <div class="modal-body">
                <p>Are you really sure to deactivate the school? This will stop all operations of the school.</p>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger btn-sm"><i class="fa fa-lock fa-fw"></i> Deactivate</button>
                </div>
            </form>
        </div>
    </div>
</div>


<!-- Deactivation Modal -->
<div class="modal fade" id="activation-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title" id="myModalLabel">Activate {{$school->name}}</h4>
            </div>

            <form class="ajax-form" action="{{url('manage/education/schools/'.$school->id.'/activate')}}" method="post">
                <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                
                <div class="modal-body">
                <p>Are you really sure to activate the school? This will resume all operations of the school.</p>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-success btn-sm"><i class="fa fa-unlock fa-fw"></i> Activate</button>
                </div>
            </form>
        </div>
    </div>
</div>