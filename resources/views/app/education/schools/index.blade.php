<div class="header">
    <div class="title"><i class="fa fa-lg fa-home fa-fw"></i> Schools</div>
    <div class="links">
        @can("create", 4)
        <a href="#education/schools/create" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Create New School</a>
        @endcan 
    </div>
</div>

<div class="row">
    <div class="col-md-9 col-xs-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Schools</h3>
            </div>
            <div class="panel-body">
                @if($schools && $schools->count() > 0)
                <table class="table table-striped table-bordered table-condensed table-hover" width="100%">
                    <thead>
                        <tr>
                            <th width="40%">Name</th>
                            <th width="20%" class="text-center">Classes</th>
                            <th width="20%" class="text-center">Students</th>
                            <th width="20%" class="text-center">Teachers</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($schools as $school)
                        <tr>
                            <td><a href="#education/schools/{{$school->id}}">{{$school->name}}</a> @if($school->active == 0) <span class="label label-warning">not active</span> @endif</td>
                            <td class="text-center">{{$school->no_of_classes}}</td>
                            <td class="text-center">{{$school->no_of_students}}</td>
                            <td class="text-center">{{$school->no_of_teachers}}</td>
                        </tr>
                        @endforeach 
                    </tbody>
                </table>
                @else 
                <p class="alert alert-warning">
                    <i class="fa fa-info"></i>&nbsp; No school found!
                </p>
                @endif
            </div>
        </div>


        @if($referral_schools)
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Referral Schools</h3>
            </div>
            <div class="panel-body">
                @if($referral_schools->count() > 0)
                <table class="table table-striped table-bordered table-condensed table-hover" width="100%">
                    <thead>
                        <tr>
                            <th width="80%">Name</th>
                            <th class="text-center">Students</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($referral_schools as $referral)
                        <tr>
                            <td><a href="#education/schools/{{$referral->id}}">{{$referral->name}}</a> @if($referral->active == 0) <span class="label label-warning">not active</span> @endif</td>
                            <td class="text-center">{{$referral->no_of_students}}</td>
                        </tr>
                        @endforeach 
                    </tbody>
                </table>
                @else 
                <p class="alert alert-warning">
                    <i class="fa fa-info"></i>&nbsp; No school found!
                </p>
                @endif
            </div>
        </div>
        @endif
    </div>

    <div class="col-xs-12 col-md-3">
        @include('app.shared.feed')
    </div>
</div>




