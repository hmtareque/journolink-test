<div class="header">
    <div class="title"><i class="fa fa-lg fa-home fa-fw"></i> School Docs</div>
    <div class="links">
        @can("create", 6)
        <a href="#education/schools/1/notes/create" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Create New Doc</a>
        @endcan 
        
        @if(Auth::user()->can("read", 6) || Auth::user()->can("create", 6))
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Classes <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#education/schools"><i class="fa fa-list"></i> All Schools</a></li>
                @can("create", 7)
                <li><a href="#education/schools/create"><i class="fa fa-plus"></i> Create New School</a></li>
                @endcan 
            </ul>
        </div>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-md-9 col-xs-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Docs</h3>
            </div>
            <div class="panel-body">
               
                <table class="table data-table table-striped table-bordered table-condensed table-hover" width="100%">
                    <thead>
                        <tr>
                            <th width="25%">Name</th>
                            <th width="30%">Files</th>
                            <th width="15%">Uploaded By</th>
                            <th width="15%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>Document Name</td>
                            <td>
                                <ul class="list-unstyled">
                                    <li><a href="#"><i class="fa fa-file-o fa-fw"></i> Document 1</a></li>
                                    <li><a href="#"><i class="fa fa-file-o fa-fw"></i> Document 2</a></li>
                                    <li><a href="#"><i class="fa fa-file-o fa-fw"></i> Document 3</a></li>
                                    
                                </ul>
                            </td>
                            <td>
                                Hasan Tareque<br/>
                                12/09/2017 7.09pm
                            </td>
                            <td>
                                <a href="" class="btn btn-xs btn-danger btn-block margin-top-2"><i class="fa fa-trash fa-fw"></i> delete</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                              Document Name
                            </td>
                            <td>
                                <ul class="list-unstyled">
                                    <li><a href="#"><i class="fa fa-file-o fa-fw"></i> Document 1</a></li>
                                   
                                </ul>
                            </td>
                            <td>
                                Jane Soymore<br/>
                                15/09/2016 3.30pm
                            </td>
                            <td>
                                <a href="" class="btn btn-xs btn-danger btn-block margin-top-2"><i class="fa fa-trash fa-fw"></i> delete</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                              Document Name
                            </td>
                            <td>
                                <ul class="list-unstyled">
                                    <li><a href="#"><i class="fa fa-file-o fa-fw"></i> Document 1</a></li>
                                    <li><a href="#"><i class="fa fa-file-o fa-fw"></i> Document 2</a></li>
                                    
                                </ul>
                            </td>
                            <td>
                                Cacias Clay<br/>
                                09/02/2015 9.37am
                            </td>
                            <td>
                                <a href="" class="btn btn-xs btn-danger btn-block margin-top-2"><i class="fa fa-trash fa-fw"></i> delete</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-md-3">
        @include('app.shared.feed')
    </div>
</div>