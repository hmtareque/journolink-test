<div class="header">
    <div class="title"><i class="fa fa-lg fa-plus-square-o fa-fw"></i> Create New School</div>
    <div class="links">
        @can("read", 4)
        <a href="#education/schools" class="btn btn-success btn-sm"><i class="fa fa-list-alt fa-fw"></i> All Schools</a>
        @endcan 
    </div>
</div>


<div class="row">
    <div class="col-xs-12 col-md-8">

        <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->

        <form class="form-horizontal ajax-form" action="{{url('manage/education/schools')}}" method="post" data-hash="education/schools">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>

            <div class="form-group" id="type">
                <label class="col-sm-3 control-label">Type</label>
                <div class="col-sm-9">
                    <select class="form-control" id="student-type" name="type">
                        <option value="saturday">Saturday</option>
                        <option value="general">General</option>
                        <option value="referral">Referral</option>
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="name">
                <label class="col-sm-3 control-label">Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="name" placeholder="Name">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="primary_contact">
                <label class="col-sm-3 control-label">Primary Contact Person</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="primary_contact" placeholder="Primary Contact">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <h4 class="section-title">Address</h4>

            <div class="form-group" id="address_line_1">
                <label class="col-sm-3 control-label">Address Line 1</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="address_line_1" placeholder="Address Line 1">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="address_line_2">
                <label class="col-sm-3 control-label">Address Line 2</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="address_line_2" placeholder="Address Line 2">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="street">
                <label class="col-sm-3 control-label">Street</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="street" placeholder="Street">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="city">
                <label class="col-sm-3 control-label">City</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="city" placeholder="City">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="postcode">
                <label class="col-sm-3 control-label">Postcode</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="postcode" placeholder="Postcode">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="borough">
                <label class="col-sm-3 control-label">Borough</label>
                <div class="col-sm-9">
                    <select class="form-control" name="borough">
                        <option value=""> -- Please Select -- </option>
                        @foreach($boroughes as $borough)
                        <option value="{{trim($borough->name)}}">{{$borough->name}}</option>
                        @endforeach 
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <h4 class="section-title">Contacts</h4>

            <div class="form-group" id="phone">
                <label class="col-sm-3 control-label">Phone</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="phone" placeholder="Phone">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="mobile">
                <label class="col-sm-3 control-label">Mobile</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="mobile" placeholder="Mobile">
                    <span class="help-block hide"></span>
                </div>
            </div>



            <div class="form-group" id="email">
                <label class="col-sm-3 control-label">Email Address</label>
                <div class="col-sm-9">
                    <input type="email" class="form-control" name="email" placeholder="Email Address">
                    <span class="help-block hide"></span>
                </div>
            </div>


            <div class="form-group" id="fax">
                <label class="col-sm-3 control-label">Fax</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="fax" placeholder="Fax" value="">
                    <span class="help-block hide"></span>
                </div>
            </div>


            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-primary"> 
                        <i class="fa fa-save fa-fw"></i> Save
                    </button>
                </div>
            </div>
        </form>

    </div>
    <div class="col-xs-12 col-md-4">
         @include('app.shared.feed')
    </div>
</div>