<div class="header">
    <div class="title"><i class="fa fa-lg fa-edit fa-fw"></i> {{$school->name}}</div>
    <div class="links">
        @can("read", 4)
        <a href="#education/schools/{{$school->id}}" class="btn btn-success btn-sm"><i class="fa fa-info-circle fa-fw"></i> {{$school->name}}</a>
        @endcan 
    </div>
</div>


<div class="row">
    <div class="col-xs-12 col-md-8">
        
        <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->

     <form class="form-horizontal ajax-form" action="{{url('manage/education/schools/'.$school->id)}}" method="post">
                <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                <input type="hidden" name="_method" value="put"/>
             
                
                    <div class="form-group" id="type">
                        <label class="col-sm-3 control-label">Type</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="student-type" name="type">
                                <option value="saturday" @if($school->type == "saturday") selected="" @endif>Saturday</option>
                                <option value="general" @if($school->type == "general") selected="" @endif>General</option>
                                <option value="referral" @if($school->type == "referral") selected="" @endif>Referral</option>
                            </select>
                            <span class="help-block hide"></span>
                        </div>
                    </div>

                    <div class="form-group" id="name">
                        <label class="col-sm-3 control-label">Name</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="name" placeholder="Name" value="{{$school->name}}">
                            <span class="help-block hide"></span>
                        </div>
                    </div>

                    <div class="form-group" id="primary_contact">
                        <label class="col-sm-3 control-label">Primary Contact</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="primary_contact" placeholder="Primary Contact" value="{{$school->primary_contact_person}}">
                            <span class="help-block hide"></span>
                        </div>
                    </div>

                    <h4 class="section-title">Address</h4>

                    <div class="form-group" id="address_line_1">
                        <label class="col-sm-3 control-label">Address Line 1</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="address_line_1" placeholder="Address Line 1" value="{{$school->address_line_1}}">
                            <span class="help-block hide"></span>
                        </div>
                    </div>

                    <div class="form-group" id="address_line_2">
                        <label class="col-sm-3 control-label">Address Line 2</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="address_line_2" placeholder="Address Line 2" value="{{$school->address_line_2}}">
                            <span class="help-block hide"></span>
                        </div>
                    </div>

                    <div class="form-group" id="street">
                        <label class="col-sm-3 control-label">Street</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="street" placeholder="Street" value="{{$school->street}}">
                            <span class="help-block hide"></span>
                        </div>
                    </div>

                    <div class="form-group" id="city">
                        <label class="col-sm-3 control-label">City</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="city" placeholder="City" value="{{$school->city}}">
                            <span class="help-block hide"></span>
                        </div>
                    </div>

                    <div class="form-group" id="postcode">
                        <label class="col-sm-3 control-label">Postcode</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="postcode" placeholder="Postcode" value="{{$school->postcode}}">
                            <span class="help-block hide"></span>
                        </div>
                    </div>
                    
                    <div class="form-group" id="borough">
                        <label class="col-sm-3 control-label">Borough</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="borough">
                                <option value=""> -- Please Select -- </option>
                                @foreach($boroughes as $borough)
                                <option value="{{trim($borough->name)}}" @if(trim($borough->name) == trim($school->borough)) selected="" @endif>{{$borough->name}}</option>
                                @endforeach 
                            </select>
                            <span class="help-block hide"></span>
                        </div>
                    </div>
                    
                    <h4 class="section-title">Contacts</h4>

                    <div class="form-group" id="phone">
                        <label class="col-sm-3 control-label">Phone</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="phone" placeholder="Phone" value="{{$school->phone}}">
                            <span class="help-block hide"></span>
                        </div>
                    </div>

                    <div class="form-group" id="mobile">
                        <label class="col-sm-3 control-label">Mobile</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="mobile" placeholder="Mobile" value="{{$school->mobile}}">
                            <span class="help-block hide"></span>
                        </div>
                    </div>

                    <div class="form-group" id="email">
                        <label class="col-sm-3 control-label">Email Address</label>
                        <div class="col-sm-9">
                            <input type="email" class="form-control" name="email" placeholder="Email Address" value="{{$school->email}}">
                            <span class="help-block hide"></span>
                        </div>
                    </div>

                    <div class="form-group" id="fax">
                        <label class="col-sm-3 control-label">Fax</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="fax" placeholder="Fax" value="{{$school->fax}}">
                            <span class="help-block hide"></span>
                        </div>
                    </div>
                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <button type="submit" class="btn btn-primary"> 
                            <i class="fa fa-refresh fa-fw"></i> Update
                        </button>
                    </div>
                </div>
            </form>
    </div>
    
    <div class="col-xs-12 col-md-4">
         @include('app.shared.feed')
    </div>
</div> 



