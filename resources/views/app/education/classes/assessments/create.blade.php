<div class="header">
    <div class="title"><i class="fa fa-lg fa-user-circle-o fa-fw"></i> Edit Assessment of Class for Term 1</div>
    <div class="links">
        <a href="#education/classes/1" class="btn btn-success btn-sm"><i class="fa fa-info-circle fa-fw"></i> Class</a>
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Assessments <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#education/classes"><i class="fa fa-list"></i> All Assessments</a></li>
                <li><a href="##education/classes/1/assessments/1"><i class="fa fa-plus"></i> Assessment of Term 1</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Assessment</h3>
            </div>
             <form class="form-horizontal ajax-form" action="{{url('manage/education/students')}}" method="post">
                <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="panel panel-success">
                            <div class="panel-body">
                                <ul class="list-unstyled">
                                    <li><strong>Academic Year</strong> : {{$class->year}}</li>
                                    <li><strong>School</strong> : {{$class->school}}</li>
                                    <li><strong>Course</strong> : {{$class->course}}</li>
                                    <li><strong>Student Group</strong> : {{$class->group}}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="panel panel-success">
                            <div class="panel-body">
                                <strong>Teacher(s)</strong>
                                @if($class->teachers->count()>0) 
                                <ul class="list-unstyled">
                                    @foreach($class->teachers as $teacher) 
                                    <li>
                                        <div>{{$teacher->first_name}} {{$teacher->middle_name}} {{$teacher->last_name}} <label class="label label-default">{{$teacher->status}}</label></div>
                                        <div>Phone : @if($teacher->phone != '') {{$teacher->phone}} @else <label class="label label-warning">not found</label> @endif</div>
                                        <div>Mobile : @if($teacher->mobile != '') {{$teacher->mobile}} @else <label class="label label-warning">not found</label> @endif</div>
                                    </li>
                                    @endforeach 
                                </ul>
                                @else 
                                <div class="alert alert-warning">No teacher assigned for this class!</div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                <h4 class="section-title">Term 1 Assessment</h4>
                
                @if($class->students->count()>0) 
                    <div class="table-responsive margin-top-10">
                        <table class="table @if($class->students->count()>10) data-table @endif table-striped table-bordered table-hover" width="100%">
                            <thead>
                                <tr>
                                    <th width="25%">Student</th>
                                    <th width="10%">Health</th>
                                    <th width="10%">Contact</th>
                                    <th width="10%" class="text-center">Attended</th>
                                    <th width="35%">Reason of Absence</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($class->students as $student) 
                                <tr>
                                    <td>
                                        <input type="hidden" name="students[{{$student->student_id}}][id]" value="{{$student->student_id}}"/>
                                        <a href="#education/students/{{$student->client_id}}"><strong>{{$student->first_name}} {{$student->middle_name}} {{$student->last_name}}</strong></a>
                                    </td>
                                    <td>
                                        <a href="#" class="btn btn-xs btn-success" data-toggle="modal" data-target="#student-{{$student->client_id}}-modal" data-whatever="@mdo"><i class="fa fa-heartbeat fa-fw" aria-hidden="true"></i> health</a>
                                        <div class="modal fade" id="student-{{$student->client_id}}-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title" id="exampleModalLabel">{{$student->first_name}} {{$student->middle_name}} {{$student->last_name}} Health Information</h4>
                                                    </div>
                                                    <div class="modal-body">

                                                        <ul class="list-unstyled">
                                                            <li><strong>GP Name</strong> : @if($student->gp_name != '') {{$student->gp_name}} @else <label class="label label-warning">not found</label> @endif</li>
                                                            <li><strong>GP Contact</strong> : @if($student->gp_contact != '') {{$student->gp_contact}} @else <label class="label label-warning">not found</label> @endif</li>
                                                        </ul>

                                                        <ul class="list-unstyled">
                                                            <li><strong>Medical Conditions</strong></li>
                                                            @if($student->has_medical_condition == 1) 
                                                            <li>{{$student->medical_conditions}}</li>

                                                            @else 
                                                            <li><label class="label label-success">Has no medical conditions</label></li>
                                                            @endif 
                                                        </ul>

                                                        <ul class="list-unstyled">
                                                            <li><strong>Allergies</strong></li>
                                                            @if($student->has_allergy == 1) 
                                                            <li>{{$student->allergies}}</li>
                                                            @else 
                                                            <li><label class="label label-success">Has no allergy</label></li>
                                                            @endif 
                                                        </ul>

                                                        <ul class="list-unstyled">
                                                            <li><strong>Disabilities</strong></li>
                                                            @if($student->has_disability == 1) 
                                                            <li>{{$student->disabilities}}</li>
                                                            @else 
                                                            <li><label class="label label-success">Has no disability</label></li>
                                                            @endif 
                                                        </ul>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td>
                                        <a href="#" class="btn btn-xs btn-success" data-toggle="modal" data-target="#student-{{$student->client_id}}-next-of-kin" data-whatever="@mdo"><i class="fa fa-phone fa-fw" aria-hidden="true"></i> contacts</a>
                                        <div class="modal fade" id="student-{{$student->client_id}}-next-of-kin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title" id="exampleModalLabel">{{$student->first_name}} {{$student->middle_name}} {{$student->last_name}} Contacts</h4>
                                                    </div>
                                                    <div class="modal-body">

                                                        <ul class="list-unstyled">
                                                            <li><strong>Contact</strong></li>
                                                            <li>Phone : @if($student->phone != '') {{$student->phone}} @else <label class="label label-warning">not found</label> @endif</li>
                                                            <li>Mobile : @if($student->mobile != '') {{$student->mobile}} @else <label class="label label-warning">not found</label> @endif</li>
                                                            <li>Email : @if($student->email != '') <a href="mailto:{{$student->email}}">{{$student->email}}</a> @else <label class="label label-warning">not found</label> @endif</li>
                                                        </ul>

                                                        <ul class="list-unstyled">
                                                            <li><strong>Next of Kin : {{$student->next_of_kin}}</strong> ({{$student->next_of_kin_relationship}})</li>
                                                            <li>Phone : @if($student->next_of_kin_phone != '') {{$student->next_of_kin_phone}} @else <label class="label label-warning">not found</label> @endif</li>
                                                            <li>Mobile : @if($student->next_of_kin_mobile != '') {{$student->next_of_kin_mobile}} @else <label class="label label-warning">not found</label> @endif</li>
                                                            <li>Email : @if($student->next_of_kin_email != '') <a href="mailto:{{$student->next_of_kin_email}}">{{$student->next_of_kin_email}}</a> @else <label class="label label-warning">not found</label> @endif</li>
                                                        </ul>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                    <td class="text-center">
                                        <div class="checkbox">
                                            <label><input type="checkbox" name="students[{{$student->student_id}}][attended]" value="1" checked=""> Yes</label>
                                        </div>
                                    </td>
                                    <td>
                                        <div id="students-{{$student->student_id}}-reason_for_absence">
                                            <input type="text" name="students[{{$student->student_id}}][reason_for_absence]" class="form-control" style="width: 100%;" placeholder="Reason for Absence"/>
                                        </div>
                                    </td>
                                </tr>
                                @endforeach 
                            </tbody>
                        </table>
                    </div>
                    @else 
                    <div class="alert alert-warning">No student enrolled in this class!</div>
                    @endif
               

                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th width="10%">Student</th>
                                <th width="10%">Referral School</th>
                                <th width="35%">
                                    Level<br/><strong>Start</strong>
                                </th>
                                <th width="35%">
                                    Level<br/><strong>Current</strong>
                                </th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><a href="#education/students/1">MH987655M</a></td>
                                <td>Hasan Tareque</td>
                                <td>School 1</td>
                                <td>
                                    <select name="level" class="form-control">
                                        <option value="below">Below</option>
                                        <option value="inline">Inline</option>
                                        <option value="above">Above</option>
                                    </select>
                                    <textarea class="form-control" name="note" placeholder="Note"></textarea>
                                    <input type="file" name="doc[]" class="btn btn-default btn-sm"/>
                                </td>
                                <td>
                                    <select name="level" class="form-control">
                                        <option value="below">Below</option>
                                        <option value="inline">Inline</option>
                                        <option value="above">Above</option>
                                    </select>
                                    <textarea class="form-control" name="note" placeholder="Note"></textarea>
                                    <input type="file" name="doc[]" class="btn btn-default btn-sm"/>
                                </td>
                            </tr>
                            <tr>
                                <td><a href="#education/students/1">MH987655M</a></td>
                                <td>Hussain Taher</td>
                                <td>Self</td>
                                <td>
                                    <select name="level" class="form-control">
                                        <option value="below">Below</option>
                                        <option value="inline">Inline</option>
                                        <option value="above">Above</option>
                                    </select>
                                    <textarea class="form-control" name="note" placeholder="Note"></textarea>
                                    <input type="file" name="doc[]" class="btn btn-default btn-sm"/>
                                </td>
                                <td>
                                    <select name="level" class="form-control">
                                        <option value="below">Below</option>
                                        <option value="inline">Inline</option>
                                        <option value="above">Above</option>
                                    </select>
                                    <textarea class="form-control" name="note" placeholder="Note"></textarea>
                                    <input type="file" name="doc[]" class="btn btn-default btn-sm"/>
                                    <a href=""><i class="fa fa-paperclip fa-fw"></i>docs</a>
                                </td>
                            </tr>
                            <tr>
                                <td><a href="#education/students/1">MH987655M</a></td>
                                <td>Hussain Tawhid</td>
                                <td>School 3</td>
                                <td>
                                    <select name="level" class="form-control">
                                        <option value="below">Below</option>
                                        <option value="inline">Inline</option>
                                        <option value="above">Above</option>
                                    </select>
                                    <textarea class="form-control" name="note" placeholder="Note"></textarea>
                                    <input type="file" name="doc[]" class="btn btn-default btn-sm"/>
                                </td>
                                <td>
                                    <select name="level" class="form-control">
                                        <option value="below">Below</option>
                                        <option value="inline">Inline</option>
                                        <option value="above">Above</option>
                                    </select>
                                    <textarea class="form-control" name="note" placeholder="Note"></textarea>
                                    <input type="file" name="doc[]" class="btn btn-default btn-sm"/>
                                </td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
                <div class="panel-footer">
                     <button type="submit" class="btn btn-primary"> 
                            <i class="fa fa-refresh fa-fw"></i> Update
                        </button>
                </div>
             </form>
        </div>
    </div>
</div>




