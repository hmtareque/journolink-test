<div class="header">
    <div class="title"><i class="fa fa-lg fa-user-circle-o fa-fw"></i> Assessments of Class 1</div>
    <div class="links">
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Term Assessments <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#education/classes/1/assessments/1"><i class="fa fa-list"></i> Term 1</a></li>
                <li><a href="#education/classes/1/assessments/1"><i class="fa fa-list"></i> Term 2</a></li>
                <li><a href="#education/classes/1/assessments/1"><i class="fa fa-list"></i> Term 3</a></li>
             </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Assessment</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="panel panel-success">
                            <div class="panel-body">
                                <ul class="list-unstyled">
                                    <ul class="list-unstyled">
                                        <li><strong>Term</strong></li>
                                        <li><strong>Academic Year</strong></li>
                                        <li><strong>Course</strong></li>
                                        <li><strong>Student Group</strong></li>
                                        <li><strong>School</strong></li>
                                    </ul>
                                </ul>
                            </div>
                        </div>
                    </div>

                    <div class="col-xs-12 col-md-6">
                        <div class="panel panel-success">
                            <div class="panel-body">
                                <ul class="list-unstyled">
                                    <li><strong>Teacher</strong></li>
                                    <li><strong>Teacher</strong></li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <h4 class="section-title">Term 1 Assessment</h4>

                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th width="10%">Ref. No.</th>
                                <th width="15%">Student</th>
                                <th width="15%">Referral School</th>
                                <th width="20%" colspan="2" class="text-center">Term 1</th>
                                <th width="20%" colspan="2" class="text-center">Term 2</th>
                                <th width="20%" colspan="2" class="text-center">Term 3</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td colspan="3"></td>
                                <td width="10%" class="bg-success">Started</td>
                                <td width="10%" class="bg-success">Achieved</td>
                                <td width="10%">Started</td>
                                <td width="10%">Achieved</td>
                                <td width="10%" class="bg-info">Started</td>
                                <td width="10%" class="bg-info">Achieved</td>
                            </tr>
                            <tr>
                                <td><a href="#education/students/1">MH987655M</a></td>
                                <td>Hasan Tareque</td>
                                <td>School 1</td>
                                <td class="bg-success">
                                    <label class="btn btn-danger btn-xs btn-block">below</label>
                                    <a href="" class="btn btn-default btn-xs btn-block"><i class="fa fa-info-circle fa-fw"></i>note</a>
                                    <a href="" class="btn btn-default btn-xs btn-block"><i class="fa fa-paperclip fa-fw"></i>docs</a>
                                </td>
                                <td class="bg-success"><label class="btn btn-warning btn-xs btn-block">inline</label></td>
                                <td><label class="btn btn-warning btn-xs btn-block">inline</label></td>
                                <td><label class="btn btn-success btn-xs btn-block">above</label></td>
                                <td class="bg-info"><label class="btn btn-warning btn-xs btn-block">inline</label></td>
                                <td class="bg-info"><label class="btn btn-success btn-xs btn-block">above</label></td>
                            </tr>
                            <tr>
                                <td><a href="#education/students/1">MH987655M</a></td>
                                <td>Hussain Taher</td>
                                <td>Self</td>
                                <td class="bg-success"><label class="btn btn-warning btn-xs btn-block">inline</label></td>
                                <td class="bg-success"><label class="btn btn-success btn-xs btn-block">above</label></td>
                                <td><label class="btn btn-warning btn-xs btn-block">inline</label></td>
                                <td><label class="btn btn-success btn-xs btn-block">above</label></td>
                                <td class="bg-info"><label class="btn btn-warning btn-xs btn-block">inline</label></td>
                                <td class="bg-info"><label class="btn btn-success btn-xs btn-block">above</label></td>
                            </tr>
                            <tr>
                                <td><a href="#education/students/1">MH987655M</a></td>
                                <td>Hussain Tawhid</td>
                                <td>School 3</td>
                                <td class="bg-success"><label class="btn btn-danger btn-xs btn-block">below</label></td>
                                <td class="bg-success"><label class="btn btn-success btn-xs btn-block">above</label></td>
                                <td>
                                    <label class="btn btn-danger btn-xs btn-block">below</label>
                                    <a href="" class="btn btn-default btn-xs btn-block"><i class="fa fa-info-circle fa-fw"></i>note</a>
                                    <a href="" class="btn btn-default btn-xs btn-block"><i class="fa fa-paperclip fa-fw"></i>docs</a>
                                </td>
                                <td><label class="btn btn-warning btn-xs btn-block">inline</label></td>
                                <td class="bg-info"><label class="btn btn-warning btn-xs btn-block">inline</label></td>
                                <td class="bg-info"><label class="btn btn-success btn-xs btn-block">above</label></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>

    