<div class="header">
    <div class="title"><i class="fa fa-lg fa-user-circle-o fa-fw"></i> Assessment</div>
    <div class="links">
        <a href="#education/students/create" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> All Assessments</a>
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Classes <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#education/classes"><i class="fa fa-list"></i> All Classes</a></li>
                <li><a href="#education/classes/create"><i class="fa fa-plus"></i> Create New Class</a></li>
             </ul>
        </div>
    </div>
</div>


<div class="row">
    <div class="col-xs-12">
        
        <div class="row">
            <div class="col-xs-12 col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Assessment</h3>
                    </div>
                    <div class="panel-body">
                        <ul class="list-unstyled">
                            <li><strong>Created By</strong></li>
                            <li><strong>Created At</strong></li>
                        </ul>
                    </div>
                </div>
            </div>
            
            <div class="col-xs-12 col-md-6">
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Class</h3>
                    </div>
                    <div class="panel-body">
                        <ul class="list-unstyled">
                            <li><strong>Academic Year</strong></li>
                            <li><strong>Course</strong></li>
                            <li><strong>Student Group</strong></li>
                            <li><strong>School</strong></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        
      
        
      
        
        <div class="row">
            <div class="col-xs-12">
                 <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                               
                                <th>Student</th>
                                <th colspan="3">Start or Referral School</th>
                                <th colspan="3">Paiwand Sautuday School</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td></td>
                                <td>Term 1</td>
                                <td>Term 2</td>
                                <td>Term 3</td>
                                <td>Term 1</td>
                                <td>Term 2</td>
                                <td>Term 3</td>
                            </tr>
                            <tr>
                                <td>Hasan Tareque</td>
                                <td>below</td>
                                <td>below</td>
                                <td>below</td>
                                <td>below</td>
                                <td>below</td>
                                <td>below</td>
                            </tr>
                            <tr>
                                <td>Hussain Taher</td>
                                <td>below</td>
                                <td>above</td>
                                <td>above</td>
                                <td>above</td>
                                <td>above</td>
                                <td>above</td>
                            </tr>
                            <tr>
                                <td>Hussain Tawhid</td>
                                <td>inline</td>
                                <td>above</td>
                                <td>inline</td>
                                <td>above</td>
                                <td>inline</td>
                                <td>above</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        

    </div>
</div>
    