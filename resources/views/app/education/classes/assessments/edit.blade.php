<div class="header">
    <div class="title"><i class="fa fa-lg fa-user-circle-o fa-fw"></i> Edit Assessment</div>
    <div class="links">
        <a href="#education/classes/{{$class->id}}" class="btn btn-success btn-sm"><i class="fa fa-info-circle fa-fw"></i> Class {{$class->course}} {{$class->group}}</a>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-9">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Assessment Details</h3>
            </div>
            <form class="form-horizontal ajax-form" action="{{url('manage/education/classes/'.$class->id.'/assessments/'.$class->assessment->id)}}" method="post" data-hash="{{url('manage/education/classes/'.$class->id.'/assessments/'.$class->assessment->id)}}">
                <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                <input type="hidden" name="_method" value="put"/>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-xs-12 col-md-6">
                            <div class="panel panel-success">
                                <div class="panel-body">
                                    <ul class="list-unstyled">
                                        <li><strong>Academic Year</strong> : {{$class->year}}</li>
                                        <li><strong>School</strong> : {{$class->school}}</li>
                                        <li><strong>Course</strong> : {{$class->course}}</li>
                                        <li><strong>Student Group</strong> : {{$class->group}}</li>
                                    </ul>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-md-6">
                            <div class="panel panel-success">
                                <div class="panel-body">
                                    <strong>Teacher(s)</strong>
                                    @if($class->teachers->count()>0) 
                                    <ul class="list-unstyled">
                                        @foreach($class->teachers as $teacher) 
                                        <li>
                                            <div>{{$teacher->first_name}} {{$teacher->middle_name}} {{$teacher->last_name}}</div>
                                            <div>Status: <label class="label label-default">{{$teacher->status}}</label></div>
                                            <div>Phone : @if($teacher->phone != '') {{$teacher->phone}} @else <label class="label label-warning">not assessed</label> @endif</div>
                                            <div>Mobile : @if($teacher->mobile != '') {{$teacher->mobile}} @else <label class="label label-warning">not assessed</label> @endif</div>
                                        </li>
                                        @endforeach 
                                    </ul>
                                    @else 
                                    <div class="alert alert-warning">No teacher assigned for this class!</div>
                                    @endif
                                </div>
                            </div>
                        </div>
                    </div>


                    <h4 class="section-title">{{$class->assessment->term}} <small>{{$class->assessment->dates}}</small></h4>

                    @if($class->students->count()>0) 
                    <div class="table-responsive margin-top-10">
                        <table class="table @if($class->students->count()>10) data-table @endif table-striped table-bordered table-hover" width="100%">
                            <thead>
                                <tr>
                                    <th width="30%">Student</th>
                                    <th width="35%" class="text-center">External</th>
                                    <th width="35%" class="text-center">Internal</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($class->students as $student) 
                                <?php $assessment = $student->assessments; ?>
                                <tr>
                                    <td>

                                        <a href="#education/students/{{$student->client_id}}"><strong>{{$student->first_name}} {{$student->middle_name}} {{$student->last_name}}</strong></a>
                                        <div>{{$student->referral_school}}</div>
                                    </td>
                                    <td>
                                        @if(isset($assessment['external']))
                                        <?php $external = $assessment['external']; ?>
                                        <input type="hidden" name="students[{{$student->student_id}}][external][id]" value="{{$external['id']}}"/>
                                        <select name="students[{{$student->student_id}}][external][level]" class="form-control">
                                            <option value="" @if($external['level'] == '') selected="" @endif>Not Assessed</option>
                                            <option value="below" @if($external['level'] == 'below') selected="" @endif>Below</option>
                                            <option value="inline" @if($external['level'] == 'inline') selected="" @endif>Inline</option>
                                            <option value="above" @if($external['level'] == 'above') selected="" @endif>Above</option>
                                        </select>
                                        <textarea class="form-control" name="students[{{$student->student_id}}][external][note]" placeholder="Note">{{$external['note']}}</textarea>
                                        @else 
                                        <select name="students[{{$student->student_id}}][external][level]" class="form-control">
                                            <option value="">Not Assessed</option>
                                            <option value="below">Below</option>
                                            <option value="inline">Inline</option>
                                            <option value="above">Above</option>
                                        </select>
                                        <textarea class="form-control" name="students[{{$student->student_id}}][external][note]" placeholder="Note"></textarea>
                                        @endif 
                                    <td>
                                        @if(isset($assessment['internal']))
                                        <?php $internal = $assessment['internal']; ?>
                                        <input type="hidden" name="students[{{$student->student_id}}][internal][id]" value="{{$internal['id']}}"/>
                                        <select name="students[{{$student->student_id}}][internal][level]" class="form-control">
                                            <option value="" @if($internal['level'] == '') selected="" @endif>Not Assessed</option>
                                            <option value="below" @if($internal['level'] == 'below') selected="" @endif>Below</option>
                                            <option value="inline" @if($internal['level'] == 'inline') selected="" @endif>Inline</option>
                                            <option value="above" @if($internal['level'] == 'above') selected="" @endif>Above</option>
                                        </select>
                                        <textarea class="form-control" name="students[{{$student->student_id}}][internal][note]" placeholder="Note">{{$internal['note']}}</textarea>
                                        @else 
                                        <select name="students[{{$student->student_id}}][internal][level]" class="form-control">
                                            <option value="">Not Assessed</option>
                                            <option value="below">Below</option>
                                            <option value="inline">Inline</option>
                                            <option value="above">Above</option>
                                        </select>
                                        <textarea class="form-control" name="students[{{$student->student_id}}][internal][note]" placeholder="Note"></textarea>
                                        @endif
                                    </td>
                                </tr>
                                @endforeach 
                            </tbody>
                        </table>
                    </div>
                    @else 
                    <div class="alert alert-warning">No student enrolled in this class!</div>
                    @endif

                </div>
                @if($class->students->count()>0)
                <div class="panel-footer">
                    <button type="submit" class="btn btn-primary"> 
                        <i class="fa fa-refresh fa-fw"></i> Update
                    </button>
                </div>
                @endif 
            </form>
        </div>
    </div>
</div>




