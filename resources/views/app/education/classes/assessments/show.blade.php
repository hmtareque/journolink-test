<div class="header">
    <div class="title"><i class="fa fa-lg fa-user-circle-o fa-fw"></i> Assessment</div>
    <div class="links">
        @can("update", 5)
        <a href="#education/classes/{{$class->id}}/assessments/{{$class->assessment->id}}/edit" class="btn btn-primary btn-sm"><i class="fa fa-edit fa-fw"></i> Update Assessment</a>
        @endcan 
        
        <a href="#education/classes/{{$class->id}}" class="btn btn-success btn-sm"><i class="fa fa-info-circle fa-fw"></i> Class {{$class->course}} {{$class->group}}</a>
    </div>
</div>

<div class="row">
    <div class="col-xs-9">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Assessment Details</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="panel panel-success">
                            <div class="panel-body">
                                <ul class="list-unstyled">
                                    <li><strong>Academic Year</strong> : {{$class->year}}</li>
                                    <li><strong>School</strong> : {{$class->school}}</li>
                                    <li><strong>Course</strong> : {{$class->course}}</li>
                                    <li><strong>Student Group</strong> : {{$class->group}}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="panel panel-success">
                            <div class="panel-body">
                                <strong>Teacher(s)</strong>
                                @if($class->teachers->count()>0) 
                                <ul class="list-unstyled">
                                    @foreach($class->teachers as $teacher) 
                                    <li>
                                        <div>{{$teacher->first_name}} {{$teacher->middle_name}} {{$teacher->last_name}} <label class="label label-default">{{$teacher->status}}</label></div>
                                        <div>Phone : @if($teacher->phone != '') {{$teacher->phone}} @else <label class="label label-warning">not found</label> @endif</div>
                                        <div>Mobile : @if($teacher->mobile != '') {{$teacher->mobile}} @else <label class="label label-warning">not found</label> @endif</div>
                                    </li>
                                    @endforeach 
                                </ul>
                                @else 
                                <div class="alert alert-warning">No teacher assigned for this class!</div>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>

                
               
                
                <h4 class="section-title">{{$class->assessment->term}} <small>{{$class->assessment->dates}}</small></h4>

                    @if($class->students->count()>0) 
                    <div class="table-responsive margin-top-10">
                        <table class="table @if($class->students->count()>10) data-table @endif table-striped table-bordered table-hover" width="100%">
                            <thead>
                                <tr>
                                    <th width="30%">Student</th>
                                    <th width="10%" class="text-center">Type</th>
                                    <th width="30%" class="text-center">Assessment</th>
                                    <th width="30%">Documents</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($class->students as $student) 
                                
                               
                                <?php $assessment = $student->assessments; ?>
                                <tr>
                                    <td rowspan="2">
                                        <input type="hidden" name="students[{{$student->student_id}}][id]" value="{{$student->student_id}}"/>
                                        <a href="#education/students/{{$student->client_id}}"><strong>{{$student->first_name}} {{$student->middle_name}} {{$student->last_name}}</strong></a>
                                        <div>{{$student->referral_school}}</div>
                                    </td>
                                    <td class="text-center"><label class="label label-info">External</label></td>
                                    <td class="text-center">
                                        
                                        @if(isset($assessment['external']))
                                        
                                            <?php $external = $assessment['external']; ?>
                                            @if($external['level'] == 'below') 
                                            <label class="btn btn-xs btn-danger btn-block">{{$external['level']}}</label>
                                            @elseif($external['level'] == 'inline') 
                                            <label class="btn btn-xs btn-warning btn-block">{{$external['level']}}</label>
                                            @elseif($external['level'] == 'above') 
                                            <label class="btn btn-xs btn-success btn-block">{{$external['level']}}</label>
                                            @else
                                            <label class="label label-default">not assessed</label>
                                            @endif
                                            
                                            @if($external['note'] != "")<small>{{$external['note']}}</small>@endif
                                        
                                        @else 
                                            <label class="btn btn-xs btn-default btn-block">not assessed</label>
                                        @endif 
                                        
                                        
                                        
                                    <td></td>
                                </tr>
                                <tr>
                                    <td class="text-center"><label class="label label-primary">Internal</label></td>
                                    <td class="text-center">
                                        @if(isset($assessment['internal']))
                                        
                                            <?php $internal = $assessment['internal']; ?>
                                            @if($internal['level'] == 'below') 
                                            <label class="btn btn-xs btn-danger btn-block">{{$internal['level']}}</label>
                                            @elseif($internal['level'] == 'inline') 
                                            <label class="btn btn-xs btn-warning btn-block">{{$internal['level']}}</label>
                                            @elseif($internal['level'] == 'above') 
                                            <label class="btn btn-xs btn-success btn-block">{{$internal['level']}}</label>
                                            @else
                                            <label class="label label-default">not assessed</label>
                                            @endif
                                            
                                            @if($internal['note'] != "")<small>{{$internal['note']}}</small>@endif
                                     
                                        @else 
                                            <label class="label label-default">not assessed</label>
                                        @endif
                                        
                                        
                                    </td>
                                    <td></td>
                                   
                                </tr>
                                @endforeach 
                            </tbody>
                        </table>
                    </div>
                    @else 
                    <div class="alert alert-warning">No student enrolled in this class!</div>
                    @endif
                
                

             
            </div>
        </div>
    </div>
</div>




