<div class="header">
    <div class="title"><i class="fa fa-lg fa-edit fa-fw"></i> Update Assigned Teacher</div>
    <div class="links">
         <a href="#education/classes/{{$class->id}}/teachers" class="btn btn-info btn-sm"><i class="fa fa-list-alt fa-fw"></i> All Teachers of {{$class->course}} - {{$class->group}}</a>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-8">
        
         <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->

          <form class="form-horizontal ajax-form" action="{{url('manage/education/classes/'.$class->id.'/teachers/'.$assigned_teacher->teacher_id)}}" method="post">
                <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                <input type="hidden" name="_method" value="put"/>
                
                <div class="form-group" id="class">
                    <label class="col-sm-3 control-label">Class</label>
                     <div class="col-sm-9">
                         <label class="control-label">{{$class->school}} - {{$class->course}} - {{$class->group}}</label>
                       <span class="help-block hide"></span>
                    </div>
                </div>
                
                <div class="form-group" id="teacher">
                    <label class="col-sm-3 control-label">Teacher</label>
                     <div class="col-sm-9">
                        <select class="form-control" name="teacher">
                            <option value=""> -- Please Select -- </option>
                            @if($teachers->count()>0) 
                                @foreach($teachers as $teacher) 
                                <option value="{{$teacher->id}}" @if($teacher->id == $assigned_teacher->teacher_id) selected="" @endif>{{$teacher->first_name}} {{$teacher->middle_name}} {{$teacher->last_name}}</option>
                                @endforeach 
                            @endif  
                        </select>
                       <span class="help-block hide"></span>
                    </div>
                </div>
                
                <div class="form-group" id="status">
                    <label class="col-sm-3 control-label">Status</label>
                     <div class="col-sm-9">
                        <select class="form-control" name="status">
                            <option value=""> -- Please Select -- </option>
                              <option value="teaching" @if($assigned_teacher->status == 'teaching') selected="" @endif> Teaching </option>
                              <option value="left" @if($assigned_teacher->status == 'left') selected="" @endif> Left </option>
                              <option value="stopped" @if($assigned_teacher->status == 'stopped') selected="" @endif> Stopped </option>
                              <option value="completed" @if($assigned_teacher->status == 'completed') selected="" @endif> Completed </option>
                        </select>
                       <span class="help-block hide"></span>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <div id="exist"><span class="help-block hide"></span></div>
                        <button type="submit" class="btn btn-primary"> 
                            <i class="fa fa-refresh fa-fw"></i> Update
                        </button>
                    </div>
                </div>
            </form>
    </div>
    <div class="col-xs-12 col-md-4">
    </div>
</div>