<div class="header">
    <div class="title"><i class="fa fa-lg fa-user-circle-o fa-fw"></i> Class Teachers</div>
    <div class="links">
        @can("create", 6)
        <a href="#education/classes/{{$class->id}}/teachers/create" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Assign New Teacher</a>
        @endcan 
        
        @if(Auth::user()->can("read", 6) || Auth::user()->can("create", 6))
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Classes <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#education/classes"><i class="fa fa-list"></i> All Classes</a></li>
                @can("create", 7)
                <li><a href="#education/classes/create"><i class="fa fa-plus"></i> Create New Class</a></li>
                @endcan 
            </ul>
        </div>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-md-9 col-xs-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">{{$class->course}} {{$class->group}} {{$class->school}} Teachers</h3>
            </div>
            <div class="panel-body">
                @if($teachers->count()>0) 
               <div class="table-responsive">
                <table class="table table-striped table-bordered table-hover" width="100%">
                    <thead>
                        <tr>
                            <th width="35%">Teacher</th>
                            <th width="25%">DBS Number</th>
                            <th width="25%" class="text-center">DBS Issue Date</th>
                            <th width="15%">Status</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($teachers as $teacher) 
                        <tr>
                            <td>{{$teacher->first_name}} {{$teacher->middle_name}} {{$teacher->last_name}}</td>
                            <td>{{$teacher->dbs_number}}</td>
                            <td>{{date('d M Y', strtotime($teacher->dbs_issue_date))}}</td>
                            <td><label class="label label-default">{{$teacher->status}}</label></td>
                            <td>
                                <a href="#education/classes/{{$teacher->class_id}}/teachers/{{$teacher->teacher_id}}/edit" class="btn btn-xs btn-primary btn-block"><i class="fa fa-edit fa-fw"></i> update</a>
                           </td>
                        </tr>
                        @endforeach 
                    </tbody>
                </table>
               </div>
                @else 
                <div class="alert alert-warning">No teacher assigned for this class!</div>
                @endif 
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-md-3">
        @include('app.shared.feed')
    </div>
</div>