<div class="header">
    <div class="title"><i class="fa fa-lg fa-plus-square-o fa-fw"></i> Assign New Teacher</div>
    <div class="links">
         <a href="#education/classes" class="btn btn-success btn-sm"><i class="fa fa-list-alt fa-fw"></i> All Class</a>
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Students <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#education/students"><i class="fa fa-list"></i> All Students</a></li>
                <li><a href="#education/students/create"><i class="fa fa-plus"></i> Enrol New Student</a></li>
             </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-8">

          <form class="form-horizontal ajax-form" action="{{url('manage/education/classes/'.$class->id.'/teachers')}}" method="post" data-hash="education/classes/{{$class->id}}/teachers">
                <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                
                <div class="form-group" id="class">
                    <label class="col-sm-3 control-label">Class</label>
                     <div class="col-sm-9">
                         <label class="control-label">{{$class->school}} - {{$class->course}} - {{$class->group}}</label>
                       <span class="help-block hide"></span>
                    </div>
                </div>
                
                <div class="form-group" id="teacher">
                    <label class="col-sm-3 control-label">Teacher</label>
                     <div class="col-sm-9">
                        <select class="form-control" name="teacher">
                            <option value=""> -- Please Select -- </option>
                            @if($teachers->count()>0) 
                                @foreach($teachers as $teacher) 
                                <option value="{{$teacher->id}}">{{$teacher->first_name}} {{$teacher->middle_name}} {{$teacher->last_name}}</option>
                                @endforeach 
                            @endif  
                        </select>
                       <span class="help-block hide"></span>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <div id="exist"><span class="help-block hide"></span></div>
                        <button type="submit" class="btn btn-primary"> 
                            <i class="fa fa-save fa-fw"></i> Assign
                        </button>
                    </div>
                </div>
            </form>
    </div>
    <div class="col-xs-12 col-md-4">
    </div>
</div>