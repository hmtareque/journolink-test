<div class="header">
    <div class="title"><i class="fa fa-lg fa-calendar-check-o fa-fw"></i>Attendances of {{$class->course}} {{$class->group}} </div>
    <div class="links">
        
        <a href="#education/classes/{{$class->id}}" class="btn btn-success btn-sm"><i class="fa fa-info-circle fa-fw"></i>Class {{$class->course}} {{$class->group}}</a>
        
        @can("update", 5)
        <a href="#education/classes/{{$class->id}}/attendances/create" class="btn btn-primary btn-sm"><i class="fa fa-plus fa-fw"></i>Record Attendances</a>
        @endcan 
        
    </div>
</div>


<div class="row">
    <div class="col-xs-12 col-md-9">
        <div class="panel panel-info">
            <div class="panel-heading">Attendances</div>
            <div class="panel-body">
                @if($attendances->count()>0) 
                 <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th width="15%">Date</th>
                                <th width="15%" class="text-center">Students</th>
                                <th width="15%" class="text-center">Present</th>
                                <th width="15%" class="text-center">Absent</th>
                                <th width="40%" >Teachers</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($attendances as $attendance)
                            <tr>
                                <td><a href="#education/classes/{{$attendance->class_id}}/attendances/{{$attendance->id}}">{{date('d M Y', strtotime($attendance->date))}}</a></td>
                                <td class="text-center">{{$attendance->students}}</td>
                                <td class="text-center">{{$attendance->attended}}</td>
                                <td class="text-center"><span @if(($attendance->students - $attendance->attended)>0) class="text-danger" @endif>{{$attendance->students - $attendance->attended}}</span></td>
                                <td>
                                    @if($attendance->teachers->count()>0) 
                                    @foreach($attendance->teachers as $teacher) 
                                    <div><small>{{$teacher->first_name}} {{$teacher->middle_name}} {{$teacher->last_name}}</small></div>
                                    @endforeach 
                                    @else 
                                    <label class="label label-danger">not found!</label>
                                    @endif 
                                    
                                    
                                </td>
                            </tr>
                            @endforeach 
                        </tbody>
                    </table>
                </div>
                @else 
                <div class="alert alert-warning">No class held yet!</div>
                @endif 
            </div>
        </div>
    </div>
    <div class="col-md-3 col-xs-12">
        @include('app.shared.feed')
    </div>
</div>




