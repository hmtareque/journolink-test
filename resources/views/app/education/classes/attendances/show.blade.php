<div class="header">
    <div class="title"><i class="fa fa-lg fa-calendar-check-o fa-fw"></i> Attendance</div>
    <div class="links">
        @can("update", 5)
        <a href="#education/classes/{{$class->id}}/attendances/{{$class->attendance->id}}/edit" class="btn btn-primary btn-sm"><i class="fa fa-edit fa-fw"></i> Update this Attendance</a>
        @endcan 
        
        <a href="#education/classes/{{$class->id}}/attendances" class="btn btn-info btn-sm"><i class="fa fa-list-alt fa-fw"></i> Attendances</a>
    
    </div>
</div>

<div class="row">
    <div class="col-xs-9">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Attendance</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <div class="panel panel-success">
                            <div class="panel-body">
                                <ul class="list-unstyled">
                                    <li><strong>Academic Year</strong> : {{$class->year}}</li>
                                    <li><strong>School</strong> : {{$class->school}}</li>
                                    <li><strong>Course</strong> : {{$class->course}}</li>
                                    <li><strong>Student Group</strong> : {{$class->group}}</li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-md-6">
                        <div class="panel panel-success">
                            <div class="panel-body">
                                 <ul class="list-unstyled">
                                        <li><strong>Teachers</strong> :</li>
                                        @if($class->attendance->teachers->count()>0) 
                                        @foreach($class->attendance->teachers as $teacher) 
                                        <li>{{$teacher->first_name}} {{$teacher->middle_name}} {{$teacher->last_name}}</li>
                                        @endforeach 
                                        @else 
                                        <li><label class="label label-danger">not found!</label></li>
                                        @endif
                                    </ul>
                            </div>
                        </div>
                    </div>
                </div>

                <h4 class="section-title">Attendance - {{date('d M Y', strtotime($class->attendance->date))}}</h4>

                <?php $total_attended = 0; ?>
                @if($class->attendance->students->count()>0) 
                <div class="table-responsive margin-top-10">
                    <table class="table @if($class->attendance->students->count()>10) data-table @endif table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th width="35%">Student</th>
                                <th width="10%">Health</th>
                                <th width="10%">Contacts</th>
                                <th width="10%" class="text-center">Attended</th>
                                <th width="35%">Reason of Absence</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($class->attendance->students as $student) 
                            <tr>
                                <td>
                                    <input type="hidden" name="students[{{$student->student_id}}][id]" value="{{$student->student_id}}"/>
                                    <a href="#education/students/{{$student->client_id}}"><strong>{{$student->first_name}} {{$student->middle_name}} {{$student->last_name}}</strong></a>
                                </td>
                                <td>
                                    <a href="#" class="btn btn-xs btn-success" data-toggle="modal" data-target="#student-{{$student->client_id}}-modal" data-whatever="@mdo"><i class="fa fa-heartbeat fa-fw" aria-hidden="true"></i> health</a>
                                    <div class="modal fade" id="student-{{$student->client_id}}-modal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="exampleModalLabel">{{$student->first_name}} {{$student->middle_name}} {{$student->last_name}}</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <ul class="list-unstyled">
                                                        <li><strong>Next of Kin : {{$student->next_of_kin}}</strong> ({{$student->next_of_kin_relationship}})</li>
                                                        <li>Phone : @if($student->next_of_kin_phone != '') {{$student->next_of_kin_phone}} @else <label class="label label-warning">not found</label> @endif</li>
                                                        <li>Mobile : @if($student->next_of_kin_mobile != '') {{$student->next_of_kin_mobile}} @else <label class="label label-warning">not found</label> @endif</li>
                                                        <li>Email : @if($student->next_of_kin_email != '') <a href="mailto:{{$student->next_of_kin_email}}">{{$student->next_of_kin_email}}</a> @else <label class="label label-warning">not found</label> @endif</li>
                                                    </ul>

                                                    <ul class="list-unstyled">
                                                        <li><strong>GP Name</strong> : @if($student->gp_name != '') {{$student->gp_name}} @else <label class="label label-warning">not found</label> @endif</li>
                                                        <li><strong>GP Contact</strong> : @if($student->gp_contact != '') {{$student->gp_contact}} @else <label class="label label-warning">not found</label> @endif</li>
                                                    </ul>

                                                    <ul class="list-unstyled">
                                                        <li><strong>Medical Conditions</strong></li>
                                                        @if($student->has_medical_condition == 1) 
                                                        <li>{{$student->medical_conditions}}</li>

                                                        @else 
                                                        <li><label class="label label-success">Has no medical conditions</label></li>
                                                        @endif 
                                                    </ul>

                                                    <ul class="list-unstyled">
                                                        <li><strong>Allergies</strong></li>
                                                        @if($student->has_allergy == 1) 
                                                        <li>{{$student->allergies}}</li>
                                                        @else 
                                                        <li><label class="label label-success">Has no allergy</label></li>
                                                        @endif 
                                                    </ul>

                                                    <ul class="list-unstyled">
                                                        <li><strong>Disabilities</strong></li>
                                                        @if($student->has_disability == 1) 
                                                        <li>{{$student->disabilities}}</li>
                                                        @else 
                                                        <li><label class="label label-success">Has no disability</label></li>
                                                        @endif 
                                                    </ul>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>


                                </td>
                                <td>
                                        <a href="#" class="btn btn-xs btn-success" data-toggle="modal" data-target="#student-{{$student->client_id}}-next-of-kin" data-whatever="@mdo"><i class="fa fa-phone fa-fw" aria-hidden="true"></i> contacts</a>
                                        <div class="modal fade" id="student-{{$student->client_id}}-next-of-kin" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content">
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title" id="exampleModalLabel">{{$student->first_name}} {{$student->middle_name}} {{$student->last_name}} Contacts</h4>
                                                    </div>
                                                    <div class="modal-body">

                                                        <ul class="list-unstyled">
                                                            <li><strong>Contact</strong></li>
                                                            <li>Phone : @if($student->phone != '') {{$student->phone}} @else <label class="label label-warning">not found</label> @endif</li>
                                                            <li>Mobile : @if($student->mobile != '') {{$student->mobile}} @else <label class="label label-warning">not found</label> @endif</li>
                                                            <li>Email : @if($student->email != '') <a href="mailto:{{$student->email}}">{{$student->email}}</a> @else <label class="label label-warning">not found</label> @endif</li>
                                                        </ul>

                                                        <ul class="list-unstyled">
                                                            <li><strong>Next of Kin : {{$student->next_of_kin}}</strong> ({{$student->next_of_kin_relationship}})</li>
                                                            <li>Phone : @if($student->next_of_kin_phone != '') {{$student->next_of_kin_phone}} @else <label class="label label-warning">not found</label> @endif</li>
                                                            <li>Mobile : @if($student->next_of_kin_mobile != '') {{$student->next_of_kin_mobile}} @else <label class="label label-warning">not found</label> @endif</li>
                                                            <li>Email : @if($student->next_of_kin_email != '') <a href="mailto:{{$student->next_of_kin_email}}">{{$student->next_of_kin_email}}</a> @else <label class="label label-warning">not found</label> @endif</li>
                                                        </ul>

                                                    </div>
                                                    <div class="modal-footer">
                                                        <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </td>
                                <td class="text-center">
                                    @if($student->present == 1) 
                                    <?php $total_attended++; ?>
                                    <span class="text-success"><i class="fa fa-check fa-fw"></i></span>
                                    @else 
                                    <span class="text-danger"><i class="fa fa-close fa-fw"></i></span> 
                                    @endif
                                </td>
                                <td>{{$student->reason_for_absence}}</td>
                            </tr>
                            @endforeach 
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>No of Students - {{$class->attendance->students->count()}}</th>
                                <th colspan="2"></th>
                                <th class="text-center">{{$total_attended}}</th>
                                <th></th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                @else 
                <div class="alert alert-warning">No student enrolled in this class!</div>
                @endif 
            </div>
        </div>
    </div>
</div>
