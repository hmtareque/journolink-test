<div class="header">
    <div class="title"><i class="fa fa-lg fa-edit fa-fw"></i> Update Class</div>
    <div class="links">
        
         <a href="#education/classes/{{$class->id}}" class="btn btn-success btn-sm"><i class="fa fa-info-circle fa-fw"></i> Class Info</a>
        
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Classes <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#education/classes"><i class="fa fa-list"></i> All Class</a></li>
                <li><a href="#education/classes/create"><i class="fa fa-plus"></i> Create New Class</a></li>
             </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-8">
        
        <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->
        
        <pre>
            <?php print_r($class); ?>
        </pre>
        
          <form class="form-horizontal ajax-form" action="{{url('manage/education/classes/'.$class->id)}}" method="post">
                <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                <input type="hidden" name="_method" value="put"/>
                
                <div class="form-group" id="academic_year">
                    <label class="col-sm-3 control-label">Academic Year</label>
                     <div class="col-sm-9">
                        <select class="form-control" name="academic_year">
                            <option value=""> -- Please Select -- </option>
                            @foreach($academic_years as $academic_year) 
                             <option value="{{$academic_year->id}}" @if($class->year_id == $academic_year->id) selected="" @endif>{{$academic_year->year}}</option>
                            @endforeach 
                        </select>
                       <span class="help-block hide"></span>
                    </div>
                </div>
                
                <div class="form-group" id="school">
                    <label class="col-sm-3 control-label">School</label>
                     <div class="col-sm-9">
                        <select class="form-control" name="school">
                            <option value=""> -- Please Select -- </option>
                            @foreach($schools as $school) 
                             <option value="{{$school->id}}" @if($class->school_id == $school->id) selected="" @endif>{{$school->name}}</option>
                            @endforeach 
                        </select>
                       <span class="help-block hide"></span>
                    </div>
                </div>
                
                 <div class="form-group" id="course">
                    <label class="col-sm-3 control-label">Course</label>
                     <div class="col-sm-9">
                        <select class="form-control" name="course">
                            <option value=""> -- Please Select -- </option>
                            @foreach($courses as $course) 
                             <option value="{{$course->id}}" @if($class->course_id == $course->id) selected="" @endif>{{$course->title}}</option>
                            @endforeach
                        </select>
                       <span class="help-block hide"></span>
                    </div>
                </div>
                
                <div class="form-group" id="group">
                    <label class="col-sm-3 control-label">Group</label>
                     <div class="col-sm-9">
                        <select class="form-control" name="group">
                           <option value=""> -- Please Select -- </option>
                            @foreach($groups as $group) 
                             <option value="{{$group->id}}" @if($class->group_id == $group->id) selected="" @endif>{{$group->group}}</option>
                            @endforeach
                        </select>
                       <span class="help-block hide"></span>
                    </div>
                </div>
                
                <div class="form-group" id="status">
                    <label class="col-sm-3 control-label">Status</label>
                     <div class="col-sm-9">
                        <select class="form-control" name="status">
                           <option value=""> -- Please Select -- </option>
                           <option value="progressing" @if($class->status == 'progressing') selected="" @endif>Progressing</option>
                           <option value="postponed" @if($class->status == 'postponed') selected="" @endif>Postponed</option>
                           <option value="stopped" @if($class->status == 'stopped') selected="" @endif>Stopped</option>
                        <option value="complete" @if($class->status == 'complete') selected="" @endif>Complete</option>
                        </select>
                       <span class="help-block hide"></span>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <div id="exist">
                            <span class="help-block hide"></span>
                        </div>
                        
                        <button type="submit" class="btn btn-primary"> 
                            <i class="fa fa-save fa-fw"></i> Save
                        </button>
                    </div>
                </div>
            </form>

    </div>
    <div class="col-xs-12 col-md-4">
    </div>
</div>