<div class="header">
    <div class="title"><i class="fa fa-lg fa-book fa-fw"></i> Classes</div>
    <div class="links">
         @can("update", 5)
        <a href="#education/classes/{{$class->id}}/edit" class="btn btn-primary btn-sm"><i class="fa fa-edit fa-fw"></i> Update Class</a>
        @endif 
        
        <a href="#education/classes" class="btn btn-info btn-sm"><i class="fa fa-list-alt fa-fw"></i> All Classes</a>
    </div>
</div>


<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-8">

        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Class</h3>
            </div>
            <div class="panel-body">
                <ul class="list-unstyled">
                    <li><strong>Academic Year</strong> : {{$class->year}}</li>
                    <li><strong>School</strong> : {{$class->school}}</li>
                    <li><strong>Course</strong> : {{$class->course}}</li>
                    <li><strong>Student Group</strong> : {{$class->group}}</li>
                </ul>
            </div>
        </div>

        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Enrolled Students</h3>
            </div>
            <div class="panel-body">
                @if($class->students->count()>0) 
                <div class="table-responsive">
                    <table class="table @if($class->students->count()>10) data-table @endif table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Ref. No.</th>
                                <th>Status</th>
                                <th>Date of Birth</th>
                                <th>Contact</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($class->students as $student) 
                            <tr>
                                <td><a href="#education/students/{{$student->client_id}}"><strong>{{$student->first_name}} {{$student->middle_name}} {{$student->last_name}}</strong></a></td>
                                <td>{{$student->ref_no}}</td>
                                <td><label class="label label-default">{{$student->status}}</label></td>
                                <td>{{date('d M Y', strtotime($student->date_of_birth))}}</td>
                                <td>
                                    @if($student->phone != "") 
                                    {{$student->phone}}
                                    @elseif($student->mobile != "") 
                                    {{$student->mobile}}
                                    @elseif($student->email != "") 
                                    {{$student->email}}
                                    @endif 
                                </td>
                            </tr>
                            @endforeach 
                        </tbody>
                    </table>
                </div>
                @else 
                <p class="alert alert-warning">No student enrolled in this class!</p>
                @endif 
            </div>
        </div>

       
        
        <!-- Note Panel Start -->
        @include('app.shared.notes', ['module_id' => 5, 'notes' => $class->notes, 'object_type' => 'class', 'object_id' => $class->id, 'return' => 'manage/education/classes/'.$class->id])
        <!-- Note Panel End -->
        
        
        

    </div>
    <div class="col-xs-12 col-sm-12 col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Teacher(s)</h3>
            </div>
            <div class="panel-body">
                @if($class->teachers->count()>0) 
                <ul class="list-group list-unstyled">
                    @foreach($class->teachers as $teacher) 
                    <li class="list-group-item">
                        <div>
                            {{$teacher->first_name}} {{$teacher->middle_name}} {{$teacher->last_name}} <label class="label label-default">{{$teacher->status}}</label>
                        </div>
                        
                        <div>Phone : @if($teacher->phone != '') {{$teacher->phone}} @else <label class="label label-warning">not found</label> @endif</div>
                        <div>Mobile : @if($teacher->mobile != '') {{$teacher->mobile}} @else <label class="label label-warning">not found</label> @endif</div>
                    </li>
                    @endforeach 
                </ul>
                @else 
                <div class="alert alert-warning">No teacher assigned for this class!</div>
                @endif  
            </div>
            
             @can("update", 7)
            <div class="panel-footer">
                <a href="#education/classes/{{$class->id}}/teachers" class="btn btn-primary btn-xs"><i class="fa fa-wrench fa-fw"></i>Manage Teachers</a>
            </div>
             @endif 
        </div>
        
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Recent Attendances</h3>
            </div>
            <div class="panel-body">
                @if($class->attendances->count()>0) 
                <ul class="list-unstyled">
                    @foreach($class->attendances as $attendance)
                    <li><i class="fa fa-calendar-check-o fa-fw"></i><a href="#education/classes/{{$attendance->class_id}}/attendances/{{$attendance->id}}">{{date('d M Y', strtotime($attendance->date))}}</a> <label class="label label-default">{{$attendance->attended}}/{{$attendance->students}}</label></li>
                    @endforeach 
                </ul>
                @else 
                <div class="alert alert-warning">No class held yet!</div>
                @endif 
            </div>
            
            @can("update", 5)
            <div class="panel-footer">
                <a class="btn btn-xs btn-success" href="#education/classes/{{$class->id}}/attendances/create"><i class="fa fa-check-circle-o fa-fw"></i>Record Attendance</a>
                <a class="btn btn-xs btn-primary" href="#education/classes/{{$class->id}}/attendances"><i class="fa fa-list-alt fa-fw"></i>All Attendances</a>
            </div>
            @endif 
        </div>
           
        
        @if($class->assessments->count()>0)
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Assessments</h3>
            </div>
            <div class="panel-body">
                <ul class="list-unstyled">
                    @foreach($class->assessments as $assessment) 
                    <li>
                        <i class="fa fa-check-square-o fa-fw"></i><a href="#education/classes/{{$class->id}}/assessments/{{$assessment->id}}">{{$assessment->term}}</a> <small>{{$assessment->dates}}</small>
                    </li>
                    @endforeach 
                </ul>
            </div>
            <!--
            <div class="panel-footer">
                <a class="btn btn-xs btn-primary" href="#education/classes/{{$class->id}}/assessments"><i class="fa fa-list-alt fa-fw"></i>All Assessments</a>
            </div>
            -->
        </div>
        @endif 
        
       
        <!-- class docs -->
        @include('app.shared.docs', ['module_id' => 5, 'docs' => $class->docs, 'object_type' => 'class', 'object_id' => $class->id, 'folder' => 'class/'.$class->id, 'return' => 'manage/education/classes/'.$class->id]) 
        <!-- class docs end -->

     

    </div>

</div>







