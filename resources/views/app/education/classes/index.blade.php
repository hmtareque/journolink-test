<div class="header">
    <div class="title"><i class="fa fa-lg fa-book fa-fw"></i> Classes</div>
    <div class="links">
         @can("create", 5)
        <a href="#education/classes/create" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Create New Class</a>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-md-9 col-xs-12">
        
          <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->
        
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Classes In Progress</h3>
            </div>
            <div class="panel-body">

                @if($schools->count() > 0)
                @foreach($schools as $school)
                
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th colspan="4">{{$school->name}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            @if($school->classes && $school->classes->count()>0)
                            <tr class="info">
                                <th width="40%">Class</th>
                                <th width="30%">Teachers</th>
                                <th width="15%" class="text-center">Held</th>
                                <th width="15%" class="text-center">Students</th>
                            </tr>
                            @foreach($school->classes as $class)
                            <tr>
                                <td> <a href="#education/classes/{{$class->id}}">{{$class->course}} - {{$class->group}} </a><small>({{$class->year}})</small></td>
                                <td>
                                    
                                    @if($class->teachers->count()>0) 
                                    @foreach($class->teachers as $teacher)
                                    <div>{{$teacher->first_name}} {{$teacher->middle_name}} {{$teacher->last_name}} <label class="label label-default">{{$teacher->status}}</label></div>
                                    @endforeach 
                                    @else 
                                    <small class="text-danger">No teacher assinged!</small>
                                    @endif 

                                </td>
                                <td class="text-center">{{$class->held}}</td>
                                <td class="text-center">{{$class->students}}</td>
                            </tr>
                            @endforeach 
                            
                            @else 
                            <tr class="danger">
                                <td colspan="4">
                                    <i class="fa fa-warning"></i>&nbsp; No class progressing in {{$school->name}}!
                                </td>
                            </tr>
                            @endif
                            
                        </tbody>
                    </table>
                </div>
                
                @endforeach        
                
                @endif
            </div>
        </div>
    </div>
    <div class="col-md-3 col-xs-12">
        @include('app.shared.feed')
    </div>
</div>