<div class="header">
    <div class="title"><i class="fa fa-lg fa-plus-square-o fa-fw"></i> Create New Student Class</div>
    <div class="links">
        
         <a href="#education/classes" class="btn btn-success btn-sm"><i class="fa fa-list-alt fa-fw"></i> All Class</a>
        
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Students <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#education/students"><i class="fa fa-list"></i> All Students</a></li>
                <li><a href="#education/students/create"><i class="fa fa-plus"></i> Enrol New Student</a></li>
             </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-8">
          <form class="form-horizontal ajax-form" action="{{url('manage/education/classes')}}" method="post" data-hash="education/classes">
                <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                
                <div class="form-group" id="academic_year">
                    <label class="col-sm-3 control-label">Academic Year</label>
                     <div class="col-sm-9">
                        <select class="form-control" name="academic_year">
                            <option value=""> -- Please Select -- </option>
                            @foreach($academic_years as $academic_year) 
                             <option value="{{$academic_year->id}}">{{$academic_year->year}}</option>
                            @endforeach 
                        </select>
                       <span class="help-block hide"></span>
                    </div>
                </div>
                
                <div class="form-group" id="school">
                    <label class="col-sm-3 control-label">School</label>
                     <div class="col-sm-9">
                        <select class="form-control" name="school">
                            <option value=""> -- Please Select -- </option>
                            @foreach($schools as $school) 
                             <option value="{{$school->id}}" @if(Request::get('school') == $school->id) selected="" @endif>{{$school->name}}</option>
                            @endforeach 
                        </select>
                       <span class="help-block hide"></span>
                    </div>
                </div>
                
                 <div class="form-group" id="course">
                    <label class="col-sm-3 control-label">Course</label>
                     <div class="col-sm-9">
                        <select class="form-control" name="course">
                            <option value=""> -- Please Select -- </option>
                            @foreach($courses as $course) 
                             <option value="{{$course->id}}">{{$course->title}}</option>
                            @endforeach
                        </select>
                       <span class="help-block hide"></span>
                    </div>
                </div>
                
                <div class="form-group" id="group">
                    <label class="col-sm-3 control-label">Group</label>
                     <div class="col-sm-9">
                        <select class="form-control" name="group">
                           <option value=""> -- Please Select -- </option>
                            @foreach($groups as $group) 
                             <option value="{{$group->id}}">{{$group->group}}</option>
                            @endforeach
                        </select>
                       <span class="help-block hide"></span>
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <div id="exist">
                            <span class="help-block hide"></span>
                        </div>
                        
                        <button type="submit" class="btn btn-primary"> 
                            <i class="fa fa-save fa-fw"></i> Save
                        </button>
                    </div>
                </div>
            </form>

    </div>
    <div class="col-xs-12 col-md-4">
    </div>
</div>