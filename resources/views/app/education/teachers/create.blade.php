<div class="header">
    <div class="title"><i class="fa fa-lg fa-plus-square-o fa-fw"></i> Create New Teacher</div>
    <div class="links">
        <a href="#education/teachers" class="btn btn-success btn-sm"><i class="fa fa-list-alt fa-fw"></i> Teachers</a>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-8">

        <form class="form-horizontal ajax-form" action="{{url('manage/education/teachers')}}" method="post" data-hash="education/teachers">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>

            <h4 class="section-title">New Teacher</h4>

            <div class="form-group" id="staff">
                <label class="col-sm-3 control-label">Staff: </label>
                <div class="col-sm-9">
                    <select class="form-control" name="staff">
                        <option value=""> -- Please Select -- </option>
                        @foreach($staffs as $staff) 
                        <option value="{{$staff->id}}">{{$staff->first_name}} {{$staff->middle_name}} {{$staff->last_name}} - {{$staff->designation}}</option>
                        @endforeach 
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="schools">
                <label class="col-sm-3 control-label">Assign Class(es): </label>
                <div class="col-sm-9">
                    @if($schools->count()>0)
                    <ul class="list-group margin-top-5">
                        @foreach($schools as $school) 
                        
                        @if($school->classes->count()>0)
                        
                            @foreach($school->classes as $class) 
                            <li class="list-group-item">
                                <div class="checkbox">
                                    <label>
                                        <input type="checkbox" name="schools[{{$school->id}}][]" value="{{$class->id}}" > {{$school->name}} - {{$class->course}} - {{$class->group}}
                                    </label>
                                </div>
                            </li>
                            @endforeach
                            @endif
                        @endforeach 
                    </ul>
                    @endif
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-primary"> 
                        <i class="fa fa-save fa-fw"></i> {{trans('action.save')}}
                    </button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-xs-12 col-md-4"></div>
</div>

