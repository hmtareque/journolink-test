<div class="header">
    <div class="title"><i class="fa fa-lg fa-user-circle-o fa-fw"></i> Teacher</div>
    <div class="links">
        <a href="#users/{{$teacher->user_id}}/edit" class="btn btn-primary btn-sm"><i class="fa fa-edit fa-fw"></i> Update {{$teacher->first_name}} {{$teacher->middle_name}} {{$teacher->last_name}}</a>
        <a href="#education/teachers" class="btn btn-info btn-sm"><i class="fa fa-list-alt fa-fw"></i> Teachers</a>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-8">
        <div class="row">
            <div class="col-xs-12">
               

                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Personal Information</h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-xs-12 col-md-4">
                            <div class="panel panel-success">
                                <div class="panel-body">
                                    <img class="img-responsive img-rounded profile-pic" @if($teacher->profile_picture) src="{{$teacher->profile_picture}}" @else src="{{asset('img/avatar.png')}}" @endif/>
                                </div>
                                <div class="panel-footer">
                                    <div id="upload-response"></div>
                                    <form class="form-inline ajax-upload-form"  method="POST" action="{{url('manage/docs/picture')}}" enctype="multipart/form-data">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                        <input type="hidden" name="object_type" value="staff_profile_picture"/>
                                        <input type="hidden" name="object_id" value="{{$teacher->staff_id}}"/>
                                        <input type="hidden" name="folder" value="staffs/{{$teacher->staff_id}}/picture"/>
                                        <input type="hidden" name="name" value="Profile Picture"/>
                                        <input type="hidden" name="return" value="manage/education/teachers/{{$teacher->teacher_id}}"/>
                                        <div class="file-upload btn btn-xs btn-success">
                                            <span id="upload-status"><i class="fa fa-refresh fa-fw"></i>Update Picture</span>
                                            <input type="file" name="picture" class="upload upload-picture"/>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>


                        <div class="col-xs-12 col-md-8">
                            <ul class="list-unstyled">
                                <li><strong>Name</strong> : {{$teacher->first_name}} {{$teacher->middle_name}}  <strong>{{$teacher->last_name}}</strong></li>
                                <li><strong>Date of Birth</strong> : {{date('d M Y', strtotime($teacher->date_of_birth))}}</li>
                                <li>
                                    <strong>Gender</strong> : 
                                    @if($teacher->gender == 'male') 
                                    <i class="fa fa-male fa-fw text-danger"></i>Male
                                    @elseif($teacher->gender == 'female') 
                                    <i class="fa fa-female fa-fw text-danger"></i>Female
                                    @elseif($teacher->gender == 'other') 
                                    <i class="fa fa-info-circle fa-fw text-danger"></i> {{$teacher->other_gender}}
                                    @endif 

                                </li>
                            </ul>

                            <div class="row">
                                <div class="col-xs-12 col-md-5">
                                    <ul class="list-unstyled">
                                        <li><strong>Address</strong></li>
                                        <li>{{$teacher->address_line_1}}</li>
                                        @if($teacher->address_line_2 != '')<li>{{$teacher->address_line_2}}</li>@endif
                                        <li>{{$teacher->street}}</li>
                                        <li>{{$teacher->city}}</li>
                                        <li><strong>{{$teacher->postcode}}</strong></li>
                                    </ul>
                                </div>
                                <div class="col-xs-12 col-md-7">
                                    <ul class="list-unstyled">
                                        <li><strong>Contact</strong></li>
                                        <li>Phone : @if($teacher->phone != '') {{$teacher->phone}} @else <label class="label label-warning">not found</label> @endif</li>
                                        <li>Mobile : @if($teacher->mobile != '') {{$teacher->mobile}} @else <label class="label label-warning">not found</label> @endif</li>
                                        <li>Email : @if($teacher->email != '') <a href="mailto:{{$teacher->email}}">{{$teacher->email}}</a> @else <label class="label label-warning">not found</label> @endif</li>
                                    </ul>
                                </div>
                            </div>

                            <ul class="list-unstyled">
                                <li><strong>DBS Number</strong> : @if($teacher->dbs_number != "") {{$teacher->dbs_number}} @else <label class="label label-danger">not found</label> @endif</li>
                                <li><strong>DBS Issue Date</strong> : @if($teacher->dbs_issue_date != "") {{date('d M Y', strtotime($teacher->dbs_issue_date))}} @else <label class="label label-danger">not found</label> @endif</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Teaching Classes</h3>
            </div>
            <div class="panel-body">

                @if($teacher->running_classes->count()>0)
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th width="55%">Class</th>
                                <th width="15%" class="text-center">Students</th>
                                <th width="15%" class="text-center">Held</th>
                                <th width="15%" class="text-center">Attended</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($teacher->running_classes as $class)
                            <tr>
                                <td><a href="#education/teachers/{{$teacher->teacher_id}}/classes/{{$class->class_id}}">{{$class->course}} - {{$class->group}}</a> <small>{{$class->school}} {{$class->year}}</small></td>
                                <td class="text-center">{{$class->students}}</td>
                                <td class="text-center">{{$class->held}}</td>
                                <td class="text-center">{{$class->attended}}</td>
                            </tr>
                            @endforeach 
                        </tbody>
                    </table>
                </div>
                @else 
                <div class="alert alert-warning">Not teaching in any class!</div>
                @endif 
                


            </div>
            <div class="panel-footer">
                <a href="#education/teachers/1/classes" class="btn btn-primary btn-xs"><i class="fa fa-wrench fa-fw"></i>Manage Classes</a>
            </div>
        </div>



      <!-- Note Panel Start -->
        @include('app.shared.notes', ['module_id' => 7, 'notes' => $teacher->notes, 'object_type' => 'teacher', 'object_id' => $teacher->teacher_id, 'return' => 'manage/education/teachers/'.$teacher->teacher_id])
        <!-- Note Panel End -->



    </div>

    <div class="col-xs-12 col-sm-12 col-md-4">

        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Position</h3>
            </div>
            <div class="panel-body">
                <ul class="list-unstyled">
                    <li><strong>Joined At</strong> : @if($teacher->joined_at != "") {{date('d M Y', strtotime($teacher->joined_at))}} @else <label class="label label-danger">not found</label> @endif</li>    
                    <li><strong>Designation</strong> : {{$teacher->designation}}</li>
                    <li><strong>Paid Role</strong> : @if($teacher->has_paid_role == 1) <label class="label label-success">yes</label> @else <label class="label label-danger">no</label> @endif</li>
                    <li><strong>Salary Type</strong> : {{$teacher->salary_type}}</li>
                    <li><strong>Salary</strong> : &pound;{{$teacher->salary_amount}}</li>
                    @if($teacher->salary_description != "")<li><small>{{$teacher->salary_description}}</small></li>@endif
                </ul>
            </div>
        </div>


        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Data Access</h3>
            </div>
            <div class="panel-body">
                @if(count($teacher->access)>0) 
                <table class="table table-striped table-bordered table-hover table-condensed" width="100%">     
                    <tr>
                        <th>Service</th>
                        <th class="text-center">Area</th>
                        <th class="text-center">Data</th>
                    </tr>
                    @foreach($teacher->access as $service => $access)
                    <tr>
                        <td>{{$service}}</td>
                        <td class="text-center">

                            @if($access['type'] == 'full') 
                            <label class="label label-success">full</label>
                            @elseif($access['type'] == 'custom')
                            <label class="label label-warning">custom</label>
                            @elseif($access['type'] == 'none') 
                            <label class="label label-danger">none</label>
                            @endif 
                        </td>

                        <td class="text-center">

                            @if($access['type'] == 'custom') 
                            @if($access['data'] == 'all') 
                            <label class="label label-success">all</label>
                            @elseif($access['data'] == 'user')
                            <label class="label label-warning">user only</label>
                            @endif 
                            @else 
                            <label class="label label-success">all</label>
                            @endif 

                        </td>
                    </tr>
                    @endforeach 
                </table>
                @else 
                <div class="alert alert-warning">No access information found!</div>
                @endif 
            </div>
            <div class="panel-footer">
                <a class="btn btn-xs btn-primary" href="#users/{{$teacher->user_id}}/access"><i class="fa fa-edit fa-fw"></i>Update Access</a>
            </div>
        </div>

        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Immigration Status</h3>
            </div>
            <div class="panel-body">
                <ul class="list-unstyled">
                    <li> 
                        <strong>Status</strong> : 
                        @if($teacher->immigration_status == 'other')
                        {{$teacher->other_immigration_status}} (other)
                        @else 
                        {{$teacher->immigration_status}}
                        @endif 
                    </li>

                    @if($teacher->born_in_the_uk == 1) 
                    <li>Born in the UK</li>
                    @else 
                    <li>Arrived in the UK : {{date('d M Y', strtotime($teacher->arrived_in_the_uk))}}</li>
                    @endif 

                    @if($teacher->immigration_note != '')
                    <li><strong>Note</strong> : {{$teacher->immigration_note}}</li>
                    @endif 

                </ul>

                
               @include('app.shared.doc', ['module_id' => 7, 'name' => 'Immigration Documents', 'id' => 'immigration-doc', 'docs' => $teacher->immigration_docs, 'object_type' => 'staff_immigration', 'object_id' => $teacher->staff_id, 'folder' => 'staffs/'.$teacher->staff_id.'/immigration', 'return' => 'manage/education/teachers/'.$teacher->teacher_id])

            </div>
            
        </div>


        @include('app.shared.docs', ['module_id' => 7, 'docs' => $teacher->docs, 'object_type' => 'teacher', 'object_id' => $teacher->teacher_id, 'folder' => 'teacher/'.$teacher->teacher_id, 'return' => 'manage/education/teachers/'.$teacher->teacher_id]) 
                

    </div>

</div>







