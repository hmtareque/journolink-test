<div class="header">
    <div class="title"><i class="fa fa-lg fa-user-circle-o fa-fw"></i> Teachers</div>
    <div class="links">
        @can("create", 7)
        <a href="#education/teachers/create" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Create New Teacher</a>
        @endcan 
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-9">
        <div class="panel panel-info">
            <div class="panel-heading">All Teachers</div>
            <div class="panel-body">

                @if($schools && count($schools)>0) 
                @foreach($schools as $school)
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th colspan="5">{{$school['name']}}</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="info">
                                <th width="25%">Name</th>
                                <th width="20%">Contact</th>
                                <th width="20%">DBS</th>
                                <th width="35%">Class(es)</th>
                            </tr>
                            @foreach($school['teachers'] as $teacher)
                            <tr>
                                <td>
                                    <a href="#education/teachers/{{$teacher->teacher_id}}">{{$teacher->first_name}} {{$teacher->middle_name}} {{$teacher->last_name}}</a>
                                    @if($teacher->paid != 1) <label class="label label-success">volunteer</label>@endif
                                </td>
                                <td>
                                    @if($teacher->email != "") 
                                    {{$teacher->email}}
                                    @elseif($teacher->mobile != "")
                                    {{$teacher->mobile}}
                                    @elseif($teacher->phone != "") 
                                    {{$teacher->phone}}
                                    @else 
                                    <label class="label label-danger"><i class="fa fa-warning"></i> not found</label>
                                    @endif 
                                </td>
                                <td>
                                    <ul class="list-unstyled">
                                        <li>{{$teacher->dbs_number}}</li>
                                        @if($teacher->dbs_number != "" && $teacher->dbs_issue_date != "") <li>{{date('d M Y', strtotime($teacher->dbs_issue_date))}}</li> @endif 
                                    </ul>
                                    
                                    

                                </td>
                                <td>
                                    @if($teacher->classes->count()>0)
                                    <ul class="list-unstyled">
                                        @foreach($teacher->classes as $class)
                                        <li>{{$class->year}} {{$class->course}} {{$class->group}} <label class="label label-default">{{$class->status}}</label></li>
                                        @endforeach 
                                    </ul>
                                    @else 
                                    <label class="label label-danger">none</label>
                                    @endif 
                                </td>
                            </tr>
                            @endforeach 
                        </tbody>
                    </table>
                </div>
                @endforeach 

                @else

                <div class="alert alert-warning">No teachers found.</div>

                @endif 

            </div>
        </div>
    </div>

    <div class="col-xs-12 col-md-3">
        @include('app.shared.feed')
    </div> 
</div>


