<div class="header">
    <div class="title"><i class="fa fa-lg fa-home fa-fw"></i> Referral School</div>
</div>

<div class="row">
    <div class="col-xs-12">

        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Report</h3>
            </div>
            <div class="panel-body">
                
            
                
                <div class="row">
                    <div class="col-xs-12 col-md-8">
                          <form class="form-horizontal ajax-form" action="{{url('manage/education/reports/search/students')}}" method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>

                    <div class="form-group" id="school">
                        <label class="col-sm-3 control-label">Search</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="search" placeholder="Search Student" @if(isset($request)) value="{{$request['search']}}" @endif>
                            <span class="help-block"></span>
                            <span class="label label-info">Search Student</span>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary btn-sm"> 
                                <i class="fa fa-search fa-fw"></i> Search
                            </button>
                        </div>
                    </div>
                          </form>
                    </div>
                    <div class="col-xs-12 col-md-4"></div>
                </div>
                


                @if($students)
                <h4 class="section-title">Students</h4>

                @if($students->count()>0) 
                <table class="table table-bordered data-table table-hover">
                    <thead>
                        <tr>
                            <th>Name</th>
                            <th class="text-center">Date of Birth</th>
                            <th>Contact</th>
                            <th>Next of Kin</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($students as $student) 
                        <tr>
                            <td><a href="#education/reports/students/{{$student->client_id}}">{{$student->first_name}} {{$student->middle_name}} {{$student->last_name}}</a></td>
                            <td class="text-center">{{date('d M Y', strtotime($student->date_of_birth))}}</td>
                            <td>
                                @if($student->mobile != "") 
                                {{$student->mobile}}
                                @elseif($student->phone != "") 
                                {{$student->phone}}
                                @elseif($student->email != "") 
                                {{$student->email}}
                                @endif 
                            </td>
                            <td>
                                {{$student->next_of_kin}} <small>({{$student->next_of_kin_relationship}})</small>
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>  
                @else 
                <div class="alert alert-warning">No student found!</div>
                @endif
                
                @endif 

            </div>
        </div>
    </div>
</div>


