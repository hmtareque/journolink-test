<div class="header">
    <div class="title"><i class="fa fa-lg fa-file-text fa-fw"></i> Service Manager Report</div>
    <div class="links">
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Other Reports <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="##education/reports/referral-school"><i class="fa fa-file-text-o fa-fw"></i>Referral School Report</a></li>
                <li><a href="#education/reports/search/students"><i class="fa fa-file-text-o fa-fw"></i> Student Report</a></li>
             </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">

        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Report</h3>
            </div>
            <div class="panel-body">
                
                <div class="row">
                    <div class="col-xs-12 col-md-8">
                         <form class="form-horizontal ajax-form" action="{{url('manage/education/reports/service-manager')}}" method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>

                    <div class="form-group" id="school">
                    <label class="col-sm-3 control-label">School</label>
                     <div class="col-sm-9">
                        <select class="form-control" name="school">
                            <option value=""> -- Please Select -- </option>
                            @foreach($schools as $school) 
                             <option value="{{$school->id}}" @if(Request::get('school') == $school->id) selected="" @endif>{{$school->name}}</option>
                            @endforeach 
                        </select>
                       <span class="help-block hide"></span>
                    </div>
                </div>

                    <div class="form-group" id="date">
                        <label class="col-sm-3 control-label">From: </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control jtoday" name="from" placeholder="From" @if(isset($request['from'])) value="{{date('d/m/Y', strtotime(php_date($request['from'])))}}" @else value="{{date('d/m/Y', strtotime('-1 week'))}}" @endif>
                                   <span class="help-block hide"></span>
                        </div>
                    </div>

                    <div class="form-group" id="date">
                        <label class="col-sm-3 control-label">To: </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control jtoday" name="to" placeholder="To" @if(isset($request['to'])) value="{{date('d/m/Y', strtotime(php_date($request['to'])))}}" @else value="{{date('d/m/Y')}}" @endif>
                                   <span class="help-block hide"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary btn-sm"> 
                                <i class="fa fa-file-text fa-fw"></i> Show 
                            </button>

                        </div>
                    </div>
                </form>
                    </div>
                    <div class="col-xs-12 col-md-4"></div>
                </div>
                
                
               

                @if(isset($data))
                <h4 class="section-title">From <span class="text-primary">{{date('d M Y', strtotime(php_date($request['from'])))}}</span> To <span class="text-primary">{{date('d M Y', strtotime(php_date($request['to'])))}}</span></h4>

                <h5><strong>Notes</strong></h5>
                @if($data['notes']->count()>0) 
                <table class="table table-bordered table-condensed table-hover">
                    <thead>
                        <tr><th>Notes</th></tr>
                    </thead> 
                    <tbody>
                    @foreach($data['notes'] as $note) 
                    <tr>
                        <td>
                            <p>{{$note->note}}<p>
                                <small class="text-primary">
                                    Created By {{ username($note->created_by)}} At {{date('d M Y g:i a', strtotime($note->created_at))}}
                                    @if($note->updated_by)
                                    and Updated by {{ username($note->updated_by)}} At {{date('d M Y g:i a', strtotime($note->updated_at))}}
                                    @endif 
                                </small>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>  
                @else 
                <div class="alert alert-warning">No note found!</div>
                @endif 
                
                
                <h5><strong>Schools</strong></h5>
                @if($data['schools']->count()>0) 
                <table class="table table-bordered table-condensed table-hover">
                    <thead>
                        <tr>
                            <th rowspan="2" width="40%">School</th>
                            <th rowspan="2" width="20%" class="text-center">Total <br/>Enrolled Students</th>
                            <th colspan="2" class="text-center">{{date('d M Y', strtotime(php_date($request['from'])))}} to {{date('d M Y', strtotime(php_date($request['to'])))}}</th>
                        </tr>
                        <tr>
                            <th width="20%" class="text-center">Enrolled</th>
                            <th width="20%" class="text-center">Attended</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data['schools'] as $school) 
                    <tr>
                        <td>{{$school->name}}</td>
                        <td class="text-center">{{$school->total_enrolled}}</td>
                        <td class="text-center">{{$school->enrolled}}</td>
                        <td class="text-center">{{$school->attended}}</td>
                    </tr>
                    @endforeach
                    </tbody>
                    
                    
                </table>
                @else 
                <div class="alert alert-warning">No data found!</div>
                @endif 
                
                <h5><strong>Classes</strong></h5>
                @if($data['classes']->count()>0) 
                <table class="table table-bordered table-condensed table-hover">
                    <thead>
                        <tr>
                            <th rowspan="2" width="15%">Class</th>
                            <th rowspan="2" width="15%">Assigned <br/>Teachers</th>
                            <th rowspan="2" width="10%" class="text-center">Enrolled<br/>Students</th>
                            <th colspan="4" width="60%" class="text-center">{{date('d M Y', strtotime(php_date($request['from'])))}} to {{date('d M Y', strtotime(php_date($request['to'])))}}</th>
                        </tr>
                        <tr>
                            <th width="10%" class="text-center">Enrolled</th>
                            <th width="20%" class="text-center">Date</th>
                            <th width="10%" class="text-center">Attended</th>
                            <th width="20%">Teachers</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        @foreach($data['classes'] as $class) 
                        
                        <?php $rowspan = ($class->attendances->count() == 0 )? 1 : $class->attendances->count()+1; ?>
                        
                        <tr>
                        <td class="text-middle text-center" rowspan="{{$rowspan}}">{{$class->course}} {{$class->group}}</td>
                        <td class="text-middle" rowspan="{{$rowspan}}">
                            @if($class->teachers->count()>0)
                            <ul class="list-unstyled">
                            @foreach($class->teachers as $teacher) 
                            <li>
                                <small>{{$teacher->first_name}} {{$teacher->middle_name}} {{$teacher->last_name}}</small>
                                @if($teacher->status != 'teaching') <label class="label label-warning">{{$teacher->status}}</label>@endif
                            </li>
                            @endforeach 
                            </ul>
                            @else 
                            @endif 
                        </td>
                        <td class="text-middle text-center" rowspan="{{$rowspan}}" class="text-center">{{$class->enrolled_students}}</td>
                        <td class="text-middle text-center" rowspan="{{$rowspan}}" class="text-center">{{$class->enrolled}}</td>
                        
                        @if($class->attendances->count() == 0) 
                        <td colspan="3"></td>
                        @endif 
                        
                    </tr>
                    
                    
                        
                        
                           @if($class->attendances->count()>0)
                           @foreach($class->attendances as $attendance) 
                          <tr>
                                <td width="20%" class="text-center">{{date('d M Y', strtotime($attendance->date))}}</td>
                                <td width="20%" class="text-center">{{$attendance->attended}}</td>
                                <td width="40%">
                                    @if($attendance->teachers->count()>0)
                                    <ul class="list-unstyled">
                                    @foreach($attendance->teachers as $attended_teacher) 
                                    <li> <small>{{$attended_teacher->first_name}} {{$attended_teacher->middle_name}} {{$attended_teacher->last_name}}</small></li>
                                
                                    @endforeach 
                                    </ul>
                                    @endif 
                                    
                                </td>
                            </tr>
                            @endforeach 
                            
                           
                            @endif
                       
                        
                  
                    @endforeach
                    </tbody>
                </table>
                @else 
                <div class="alert alert-warning">No data found!</div>
                @endif
                
                
                <h5><strong>Subjects</strong></h5>
                @if($data['courses']->count()>0) 
                <table class="table table-bordered table-condensed table-hover">
                    <thead>
                        <tr>
                            <th rowspan="2" width="40%">Subject</th>
                            <th rowspan="2" width="20%" class="text-center">Total <br/>Enrolled Students</th>
                            <th colspan="2" width="40%" class="text-center">{{date('d M Y', strtotime(php_date($request['from'])))}} to {{date('d M Y', strtotime(php_date($request['to'])))}}</th>
                        </tr>
                        <tr>
                            <th width="20%" class="text-center">Enrolled</th>
                            <th width="20%" class="text-center">Attended</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data['courses'] as $course) 
                    <tr>
                        <td>{{$course->title}}</td>
                        <td class="text-center">{{$course->total_enrolled}}</td>
                        <td class="text-center">{{$course->enrolled}}</td>
                        <td class="text-center">{{$course->attended}}</td>
                    </tr>
                    @endforeach
                    </tbody>
                    
                    
                </table>
                @else 
                <div class="alert alert-warning">No data found!</div>
                @endif 

                <h5><strong>Fees</strong></h5>
                
                <table class="table table-bordered table-condensed table-hover">
                   
                    <tr>
                        <td>Total Fees Collected from {{date('d M Y', strtotime(php_date($request['from'])))}} to {{date('d M Y', strtotime(php_date($request['to'])))}}</td>
                        <td class="text-right">&pound;400.00</td>
                    </tr>
                    
                    <tr>
                        <td>Fees Pending</td>
                        <td class="text-right">&pound;320.00</td>
                    </tr>
                    
                </table>
                
                @if($data['notes']->count()>0 | $data['schools']->count()>0 || $data['classes']->count()>0 || $data['courses']->count()>0)
                <hr/>
                <form class="form-horizontal" action="{{url('manage/education/print/service-manager-report')}}" method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    <input type="hidden" name="school" @if(isset($request['school'])) value="{{$request['school']}}" @endif>
                    <input type="hidden" name="from" @if(isset($request['from'])) value="{{date('d/m/Y', strtotime(php_date($request['from'])))}}" @else value="{{date('d/m/Y', strtotime('-1 week'))}}" @endif>
                    <input type="hidden" name="to" @if(isset($request['to'])) value="{{date('d/m/Y', strtotime(php_date($request['to'])))}}" @else value="{{date('d/m/Y')}}" @endif>
                    
                     <button type="submit" class="btn btn-info btn-sm pull-right"> 
                        <i class="fa fa-file-pdf-o fa-fw"></i> Download as PDF
                    </button> 
                </form>
                @endif 
              
                @endif
            </div>

            
        </div>

    </div>
   
</div>


