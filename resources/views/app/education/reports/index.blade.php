<div class="header">
    <div class="title"><i class="fa fa-lg fa-files-o fa-fw"></i> Reports</div>
</div>



<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-9">

<div class="row">
    <div class="col-xs-12 col-md-3">
        <div class="panel panel-default">
            <div class="panel-body text-center">
                <a href="#education/reports/referral-school"><i class="fa fa-home fa-3x"></i></a>
                <div class="margin-top-10"><small class="text-muted">Referral School</small></div>
            </div>
        </div>
    </div>
    
    <div class="col-xs-12 col-md-3">
        <div class="panel panel-default">
            <div class="panel-body text-center">
                <a href="#education/reports/service-manager"><i class="fa fa-user-circle fa-3x"></i></a>
                <div class="margin-top-10"><small class="text-muted">Service Manager</small></div>
            </div>
        </div>
    </div>
    
    
    <div class="col-xs-12 col-md-3">
        <div class="panel panel-default">
            <div class="panel-body text-center">
                <a href="#education/reports/search/students"><i class="fa fa-user fa-3x"></i></a>
                <div class="margin-top-10"><small class="text-muted">Student</small></div>
            </div>
        </div>
    </div>
    
   
</div>
        
          </div>
    <div class="col-md-3 col-xs-12">
        @include('app.shared.feed')
    </div>
</div>


