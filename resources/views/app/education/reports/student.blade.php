<div class="header">
    <div class="title"><i class="fa fa-lg fa-file-text fa-fw"></i> Student Report</div>
    <div class="links">
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Other Reports <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#education/reports/referral-school"><i class="fa fa-file-text-o fa-fw"></i>Referral School Report</a></li>
                <li><a href="#education/reports/service-manager"><i class="fa fa-file-text-o fa-fw"></i>Service Manager Report</a></li>
             </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">

        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">{{$student->first_name}} {{$student->middle_name}} {{$student->last_name}} Report</h3>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12 col-md-8">
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="panel panel-default">
                                    <div class="panel-body">
                                        <div class="col-xs-12 col-md-4">
                                            <div class="panel panel-success">
                                                <div class="panel-body">
                                                    <img class="img-responsive img-rounded" @if($student->profile_picture) src="{{$student->profile_picture}}" @else src="{{asset('img/avatar.png')}}" @endif/>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="col-xs-12 col-md-8">
                                            <ul class="list-unstyled">
                                                <li><strong>Name</strong> : {{$student->first_name}} {{$student->middle_name}}  <strong>{{$student->last_name}}</strong></li>
                                                <li><strong>Date of Birth</strong> : {{date('d M Y', strtotime($student->date_of_birth))}}</li>
                                                <li>
                                                    <strong>Gender</strong> : 
                                                    @if($student->gender == 'male') 
                                                    <i class="fa fa-male fa-fw text-danger"></i>Male
                                                    @elseif($student->gender == 'female') 
                                                    <i class="fa fa-female fa-fw text-danger"></i>Female
                                                    @elseif($student->gender == 'other') 
                                                    <i class="fa fa-info-circle fa-fw text-danger"></i> {{$student->other_gender}}
                                                    @endif 
                                                </li>
                                            </ul>
                                            <div class="row">
                                                <div class="col-xs-12 col-md-5">
                                                    <ul class="list-unstyled">
                                                        <li><strong>Address</strong></li>
                                                        <li>{{$student->address_line_1}}</li>
                                                        @if($student->address_line_2 != '')<li>{{$student->address_line_2}}</li>@endif
                                                        <li>{{$student->street}}</li>
                                                        <li>{{$student->city}}</li>
                                                        <li><strong>{{$student->postcode}}</strong></li>
                                                        <li>@if($student->local_authority != '') {{$student->local_authority}} @else <label class="label label-warning">borough not found</label> @endif</li>
                                                    </ul>
                                                </div>
                                                <div class="col-xs-12 col-md-7">
                                                    <ul class="list-unstyled">
                                                        <li><strong>Contact</strong></li>
                                                        <li>Phone : @if($student->phone != '') {{$student->phone}} @else <label class="label label-warning">not found</label> @endif</li>
                                                        <li>Mobile : @if($student->mobile != '') {{$student->mobile}} @else <label class="label label-warning">not found</label> @endif</li>
                                                        <li>Email : @if($student->email != '') <a href="mailto:{{$student->email}}">{{$student->email}}</a> @else <label class="label label-warning">not found</label> @endif</li>
                                                    </ul>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>


                        <form class="form-horizontal ajax-form" action="{{url('manage/education/reports/students/'.$student->id)}}" method="post">
                            <input type="hidden" name="_token" value="{{csrf_token()}}"/>

                            <div class="form-group" id="year">
                                <label class="col-sm-3 control-label">Academic Year</label>
                                <div class="col-sm-9">
                                    <select class="form-control" name="year">
                                        <option value=""> -- Please Select -- </option>
                                        @foreach($academic_years as $academic_year) 
                                        <option value="{{$academic_year->id}}" @if(isset($request['year']) && $request['year'] == $academic_year->id) selected="" @endif>{{$academic_year->year}}</option>
                                        @endforeach 
                                    </select>
                                    <span class="help-block hide"></span>
                                </div>
                            </div>

                            <div class="form-group" id="date">
                                <label class="col-sm-3 control-label">From: </label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control jtoday" name="from" placeholder="From" @if(isset($request['from'])) value="{{date('d/m/Y', strtotime(php_date($request['from'])))}}" @else value="{{date('d/m/Y', strtotime('-1 week'))}}" @endif>
                                           <span class="help-block hide"></span>
                                </div>
                            </div>

                            <div class="form-group" id="date">
                                <label class="col-sm-3 control-label">To: </label>
                                <div class="col-sm-9">
                                    <input type="text" class="form-control jtoday" name="to" placeholder="To" @if(isset($request['to'])) value="{{date('d/m/Y', strtotime(php_date($request['to'])))}}" @else value="{{date('d/m/Y')}}" @endif>
                                           <span class="help-block hide"></span>
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-9 col-sm-offset-3">
                                    <button type="submit" class="btn btn-primary btn-sm"> 
                                        <i class="fa fa-file-text fa-fw"></i> Show 
                                    </button> 

                                    <a class="btn btn-success btn-sm" href="#education/reports/search/students">Search Again</a>

                                </div>
                            </div>
                        </form>
                    </div>
                    
                    <div class="col-xs-12 col-md-4">
                        <div class="panel panel-default">

                            <div class="panel-body">
                                <strong class="text-primary">Next of Kin</strong>
                                <ul class="list-unstyled">
                                    <li><strong>{{$student->next_of_kin}}</strong> ({{$student->next_of_kin_relationship}})</li>
                                </ul>
                                <ul class="list-unstyled">
                                    <li><strong>Address</strong></li>
                                    @if($student->next_of_kin_address_same_as_main == 1)
                                    <li>Same as student address</li>
                                    @else 
                                    <li>{{$student->next_of_kin_address_line_1}}</li>
                                    @if($student->next_of_kin_address_line_2 != '')<li>{{$student->next_of_kin_address_line_2}}</li>@endif
                                    <li>{{$student->next_of_kin_street}}</li>
                                    <li>{{$student->next_of_kin_city}}</li>
                                    <li><strong>{{$student->next_of_kin_postcode}}</strong></li>
                                    @endif 
                                </ul>

                                <ul class="list-unstyled">
                                    <li><strong>Contact</strong></li>
                                    <li>Phone : @if($student->next_of_kin_phone != '') {{$student->next_of_kin_phone}} @else <label class="label label-warning">not found</label> @endif</li>
                                    <li>Mobile : @if($student->next_of_kin_mobile != '') {{$student->next_of_kin_mobile}} @else <label class="label label-warning">not found</label> @endif</li>
                                    <li>Email : @if($student->next_of_kin_email != '') <a href="mailto:{{$student->next_of_kin_email}}">{{$student->next_of_kin_email}}</a> @else <label class="label label-warning">not found</label> @endif</li>
                                </ul>
                            </div>
                        </div>


                    </div>
                </div>




                @if(isset($data))
                <h4 class="section-title">Report</h4>

                <h5><strong>Currently Enrolled in Classes</strong></h5>

                @if($data['classes']->count()>0) 
                <table class="table table-bordered table-condensed table-hover">
                    <thead>
                        <tr>
                            <th>Enrolled At</th>
                            <th>School</th>
                            <th>Subject</th>
                            <th>Year Group</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($data['classes'] as $class) 
                        <tr>
                            <td>{{date('d M Y', strtotime($class->date_of_enrolment))}}</td>
                            <td>{{$class->school}}</td>
                            <td>{{$class->course}}</td>
                            <td>{{$class->group}}</td>
                            <td>{{$class->student_status}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>  
                @else 
                <div class="alert alert-warning">No student enrolled in this period!</div>
                @endif 
                
                <h5><strong>Attendance From <span class="text-primary">{{date('d M Y', strtotime(php_date($request['from'])))}}</span> To <span class="text-primary">{{date('d M Y', strtotime(php_date($request['to'])))}}</span></strong></h5>


                
                @if($data['attendances']->count()>0) 
                <table class="table table-bordered table-condensed table-hover">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Class</th>
                            <th class="text-center">Attended</th>
                            <th>Reason for Absence</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($data['attendances'] as $attendance) 
                        <tr>
                            <td>{{date('d M Y', strtotime($attendance->date))}}</td>
                            <td>
                                {{$attendance->course}} {{$attendance->group}} {{$attendance->school}} <small class="text-primary">{{$attendance->class_status}}</small>
                            </td>
                            <td class="text-center">
                                @if($attendance->present == 1)
                                <label class="label label-success">Yes</label>
                                @else 
                                <label class="label label-danger">No</label>
                                @endif 
                            </td>
                            <td>
                                @if($attendance->present == 0) <small>{{$attendance->reason_for_absence}}</small> @endif 
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>  
                @else 
                <div class="alert alert-warning">No student attended in this period!</div>
                @endif
                
                <h5><strong>Assessments</strong></h5>

                @if(count($data['assessments'])>0) 
                <table class="table table-bordered table-condensed table-hover">
                    <thead>
                        <tr>
                            <th rowspan="2" width="20%">Class</th>
                            <th rowspan="2" width="30%">Term</th>
                            <th colspan="2" width="50%" class="text-center">Assessment</th>
                        </tr>
                        <tr>
                            <th width="25%" class="text-center">External</th>
                            <th width="25%" class="text-center">Internal</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($data['assessments'] as $class)
                        <tr>
                            <td rowspan="{{count($class['terms'])+1}}">{{$class['class']}} {{$class['school']}}</td>
                        </tr>
                        @foreach($class['terms'] as $term)
                        <tr>
                            
                            <td><strong>{{$term['term']}}</strong> {{$term['year']}}<br/><small>{{$term['dates']}}</small></td>
                            <td class="text-center">
                                @if(isset($term['assessments']['external']))
                                
                                <?php $external_level = $term['assessments']['external']['level']; ?>
                                @if($external_level == 'below') 
                                <label class="btn btn-xs btn-danger btn-block">{{$external_level}}</label>
                                @elseif($external_level == 'inline') 
                                <label class="btn btn-xs btn-warning btn-block">{{$external_level}}</label>
                                @elseif($external_level == 'above') 
                                <label class="btn btn-xs btn-success btn-block">{{$external_level}}</label>
                                @else 
                                <label class="label label-default">not assessed</label>
                                @endif 
                                
                                @if($term['assessments']['external']['note'] != "")<small>{{$term['assessments']['external']['note']}}</small>@endif

                                @if(isset($term['assessments']['external']['docs'])) 
                                    <?php $docs = $term['assessments']['external']['docs']; ?>
                                    @if($docs && count($docs->files)>0)
                                        <ul class="list-unstyled list-inline">
                                        @foreach($docs->files as $file) 
                                        <li><a href="{{$file['link']}}" target="_blank"><i class="fa fa-paperclip fa-fw"></i> {{substr($file['name'], 10, 10)}}</a></li>
                                        @endforeach 
                                        </ul>
                                    @endif 
                                @endif 
                                
                                
                                
                                @else 
                                <label class="btn btn-xs btn-default btn-block">not assessed</label>
                                @endif
                                
                                
                                
                                
                            </td>
                            <td class="text-center">
                                
                                @if(isset($term['assessments']['internal']))
                                
                                <?php $internal_level = $term['assessments']['internal']['level']; ?>
                                
                                @if($internal_level == 'below') 
                                <label class="btn btn-xs btn-danger btn-block">{{$internal_level}}</label>
                                @elseif($internal_level == 'inline') 
                                <label class="btn btn-xs btn-warning btn-block">{{$internal_level}}</label>
                                @elseif($internal_level == 'above') 
                                <label class="btn btn-xs btn-success btn-block">{{$internal_level}}</label>
                                @else 
                                <label class="label label-default">not assessed</label>
                                @endif 
                                
                                @if($term['assessments']['internal']['note'] != "")<small>{{$term['assessments']['internal']['note']}}</small>@endif

                                @if(isset($term['assessments']['internal']['docs'])) 
                                    <?php $docs = $term['assessments']['internal']['docs']; ?>
                                    @if($docs && count($docs->files)>0)
                                        <ul class="list-unstyled list-inline">
                                        @foreach($docs->files as $file) 
                                        <li><a href="{{$file['link']}}" target="_blank"><i class="fa fa-paperclip fa-fw"></i> {{substr($file['name'], 10, 10)}}</a></li>
                                        @endforeach 
                                        </ul>
                                    @endif 
                                @endif 

                                @else 
                                <label class="label label-default">not assessed</label>
                                @endif
                                
                            </td>
                        </tr>
                        @endforeach 
                        @endforeach 
                       
                    </tbody>
                </table>  
                @else 
                <div class="alert alert-warning">No assessment found!</div>
                @endif 
                
               
                
              
                <h5><strong>Notes</strong></h5>
                @if($data['notes']->count()>0) 
                <table class="table table-bordered table-condensed table-hover">
                    <thead>
                        <tr><th>Notes</th></tr>
                    </thead> 
                    <tbody>
                    @foreach($data['notes'] as $note) 
                    <tr>
                        <td>
                            <p>{{$note->note}}<p>
                                <small class="text-primary">
                                    Created By {{ username($note->created_by)}} At {{date('d M Y g:i a', strtotime($note->created_at))}}
                                    @if($note->updated_by)
                                    and Updated by {{ username($note->updated_by)}} At {{date('d M Y g:i a', strtotime($note->updated_at))}}
                                    @endif 
                                </small>
                        </td>
                    </tr>
                    @endforeach
                    </tbody>
                </table>  
                @else 
                <div class="alert alert-warning">No note found!</div>
                @endif 
                
                
                
                
                @if($data['classes']->count()>0 | $data['attendances']->count()>0 || $data['assessments']->count()>0 || $data['notes']->count()>0)
                <hr/>
                <form class="form-horizontal" action="{{url('manage/education/print/student-report/'.$student->id)}}" method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    <input type="hidden" name="year" @if(isset($request['year'])) value="{{$request['year']}}" @endif>
                    <input type="hidden" name="from" @if(isset($request['from'])) value="{{date('d/m/Y', strtotime(php_date($request['from'])))}}" @else value="{{date('d/m/Y', strtotime('-1 week'))}}" @endif>
                    <input type="hidden" name="to" @if(isset($request['to'])) value="{{date('d/m/Y', strtotime(php_date($request['to'])))}}" @else value="{{date('d/m/Y')}}" @endif>
                    
                     <button type="submit" class="btn btn-info btn-sm pull-right"> 
                        <i class="fa fa-file-pdf-o fa-fw"></i> Download as PDF
                    </button> 
                </form>
                @endif 

                @endif

            </div>


        </div>

    </div>

</div>


