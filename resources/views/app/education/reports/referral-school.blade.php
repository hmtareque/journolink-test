<div class="header">
    <div class="title"><i class="fa fa-lg fa-file-text fa-fw"></i> Referral School Report</div>
    <div class="links">
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Other Reports <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#education/reports/service-manager"><i class="fa fa-file-text-o fa-fw"></i>Service Manager Report</a></li>
                <li><a href="#education/reports/search/students"><i class="fa fa-file-text-o fa-fw"></i>Student Report</a></li>
             </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">

        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Referral School Report</h3>
            </div>
            <div class="panel-body">
                
                
                <div class="row">
                    <div class="col-xs-12 col-md-8">
                          <form class="form-horizontal ajax-form" action="{{url('manage/education/reports/referral-school')}}" method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>

                    <div class="form-group" id="school">
                        <label class="col-sm-3 control-label">Referral School</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="school">
                                <option value=""> -- Please Select -- </option>
                                @foreach($referral_schools as $referral)
                                <option value="{{$referral->id}}" @if(isset($request['school']) && $request['school'] == $referral->id) selected="" @endif>{{$referral->name}}</option>
                                @endforeach 
                            </select>
                            <span class="help-block hide"></span>
                        </div>
                    </div>

                    <div class="form-group" id="date">
                        <label class="col-sm-3 control-label">From: </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control jtoday" name="from" placeholder="From" @if(isset($request['from'])) value="{{date('d/m/Y', strtotime(php_date($request['from'])))}}" @else value="{{date('d/m/Y', strtotime('-1 week'))}}" @endif>
                                   <span class="help-block hide"></span>
                        </div>
                    </div>

                    <div class="form-group" id="date">
                        <label class="col-sm-3 control-label">To: </label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control jtoday" name="to" placeholder="To" @if(isset($request['to'])) value="{{date('d/m/Y', strtotime(php_date($request['to'])))}}" @else value="{{date('d/m/Y')}}" @endif>
                                   <span class="help-block hide"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-9 col-sm-offset-3">
                            <button type="submit" class="btn btn-primary btn-sm"> 
                                <i class="fa fa-file-text fa-fw"></i> Show 
                            </button>

                        </div>
                    </div>
                </form>
                    </div>
                    <div class="col-xs-12 col-md-4"></div>
                </div>

                @if(isset($data))
                <h4 class="section-title">From <span class="text-primary">{{date('d M Y', strtotime(php_date($request['from'])))}}</span> To <span class="text-primary">{{date('d M Y', strtotime(php_date($request['to'])))}}</span></h4>

                <h5><strong>Enrolled Students</strong></h5>
                
                @if($data['students']->count()>0) 
                <table class="table table-bordered table-condensed table-hover">
                    <thead>
                        <tr>
                            <th>Student</th>
                            <th>Enrolled At</th>
                            <th>School</th>
                            <th>Subject</th>
                            <th>Year Group</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($data['students'] as $student) 
                        <tr>
                            <td><a href="#education/students/{{$student->client_id}}" target="_blank">{{$student->first_name}} {{$student->middle_name}} {{$student->last_name}}</a></td>
                            <td>{{date('d M Y', strtotime($student->date_of_enrolment))}}</td>
                            <td>{{$student->school_name}}</td>
                            <td>{{$student->course}}</td>
                            <td>{{$student->group}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>  
                @else 
                <div class="alert alert-warning">No student enrolled in this period!</div>
                @endif 

                <h5><strong>Students Attendance</strong></h5>
                
                @if(count($data['attendances'])>0) 
                <table class="table table-bordered table-condensed table-hover">
                    <thead>
                        <tr>
                            
                            <th>Student</th>
                            <th>Date</th>
                            <th>School</th>
                            <th>Class</th>
                            <th class="text-center">Attended</th>
                            <th>Reason for Absence</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data['attendances'] as $student)
                        <tr>
                            <td class="text-middle" rowspan="{{count($student['attendances'])+1}}">{{$student['name']}}</td>
                            
                        </tr>
                        @foreach($student['attendances'] as $attendance)
                        
                        <tr @if($attendance->present == 0) class="danger" @endif>
                            <td>{{date('d M Y', strtotime($attendance->date))}}</td>
                            <td>{{$attendance->school_name}}</td>
                            <td>{{$attendance->course}} {{$attendance->group}}</td>
                            <td class="text-center">
                                @if($attendance->present == 1)
                                <label class="label label-success">Yes</label>
                                @else 
                                <label class="label label-danger">No</label>
                                @endif 
                            </td>
                            <td>
                                @if($attendance->present == 0) <small>{{$attendance->reason_for_absence}}</small> @endif 
                            </td>
                        </tr>
                        @endforeach 
                        @endforeach
                    </tbody>
                </table>  
                @else 
                <div class="alert alert-warning">No student attendances found in this period!</div>
                @endif
                
                @if($data['students']->count()>0 || count($data['attendances']) > 0)
                <hr/>
                <form class="form-horizontal" action="{{url('manage/education/print/referral-school-report')}}" method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    <input type="hidden" name="school" @if(isset($request['school'])) value="{{$request['school']}}" @endif>
                    <input type="hidden" name="from" @if(isset($request['from'])) value="{{date('d/m/Y', strtotime(php_date($request['from'])))}}" @else value="{{date('d/m/Y', strtotime('-1 week'))}}" @endif>
                    <input type="hidden" name="to" @if(isset($request['to'])) value="{{date('d/m/Y', strtotime(php_date($request['to'])))}}" @else value="{{date('d/m/Y')}}" @endif>
                    
                     <button type="submit" class="btn btn-info btn-sm pull-right"> 
                        <i class="fa fa-file-pdf-o fa-fw"></i> Download as PDF
                    </button> 
                </form>
                @endif 
                
                @endif
            </div>
        </div>
    </div>
</div>


