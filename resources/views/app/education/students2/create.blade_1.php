<div class="header">
    <div class="title"><i class="fa fa-lg fa-list-alt fa-fw"></i> New Student</div>
    <div class="links">
        
        <a href="#education/students/create" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Enrol New Student</a>
        
    </div>
</div>


<div class="row">
    <div class="col-xs-12 col-md-8">
        
        
        
        
          <form class="form-horizontal ajax-form" action="{{url('manage/form')}}" method="post">
                <input type="hidden" name="_token" value="{{csrf_token()}}"/>

                <div class="form-group" id="first_name">
                    <label class="col-sm-3 control-label">First Name</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="first_name" placeholder="First Name">
                        <span class="help-block hide"></span>
                    </div>
                </div>
                
                <div class="form-group" id="last_name">
                    <label class="col-sm-3 control-label">Last Name</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="last_name" placeholder="Last Name">
                        <span class="help-block hide"></span>
                    </div>
                </div>

                <div class="form-group" id="date_of_birth">
                    <label class="col-sm-3 control-label">Date of Birth</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="date_of_birth" placeholder="Date of Birth">
                        <span class="help-block hide"></span>
                    </div>
                </div>
                
                <div class="form-group" id="ni_number">
                    <label class="col-sm-3 control-label">NI Number</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="ni_number" placeholder="NI Number">
                        <span class="help-block hide"></span>
                    </div>
                </div>
                
                
               <div class="form-group" id="gender">
                    <label class="col-sm-3 control-label">Gender</label>
                    <div class="col-sm-9">
                        <select class="form-control" name="gender">
                            <option>1</option>
                            <option>1</option>
                        </select>
                        
                        <input type="text" class="form-control" style="margin-top: 5px;" name="other_gender" placeholder="Other">
                        <span class="help-block hide"></span>
                    </div>
                </div>

                
                
                <div class="form-group" id="marital_status">
                    <label class="col-sm-3 control-label">Marital Status</label>
                    <div class="col-sm-9">
                        <select class="form-control" name="marital_status">
                            <option>1</option>
                            <option>1</option>
                        </select>
                        <span class="help-block hide"></span>
                    </div>
                </div>
                
                <h4 class="section-title">Address</h4>
                
                <div class="form-group" id="address_line_1">
                    <label class="col-sm-3 control-label">Address Line 1</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="address_line_1" placeholder="Address Line 1">
                        <span class="help-block hide"></span>
                    </div>
                </div>
                
                <div class="form-group" id="address_line_2">
                    <label class="col-sm-3 control-label">Address Line 2</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="address_line_2" placeholder="Address Line 2">
                        <span class="help-block hide"></span>
                    </div>
                </div>
                
                <div class="form-group" id="street">
                    <label class="col-sm-3 control-label">Street</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="street" placeholder="Street">
                        <span class="help-block hide"></span>
                    </div>
                </div>
                
                <div class="form-group" id="city">
                    <label class="col-sm-3 control-label">City</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="city" placeholder="City">
                        <span class="help-block hide"></span>
                    </div>
                </div>
                
                <div class="form-group" id="postcode">
                    <label class="col-sm-3 control-label">Postcode</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="postcode" placeholder="Postcode">
                        <span class="help-block hide"></span>
                    </div>
                </div>
                
                <div class="form-group" id="borough">
                    <label class="col-sm-3 control-label">Borough</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="borough" placeholder="Borough">
                        <span class="help-block hide"></span>
                    </div>
                </div>
                
                 <h4 class="section-title">Contacts</h4>
                
                <div class="form-group" id="phone">
                    <label class="col-sm-3 control-label">Phone</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="phone" placeholder="Phone">
                        <span class="help-block hide"></span>
                    </div>
                </div>
                 
                 <div class="form-group" id="email">
                    <label class="col-sm-3 control-label">Email Address</label>
                    <div class="col-sm-9">
                        <input type="email" class="form-control" name="email" placeholder="Email Address">
                        <span class="help-block hide"></span>
                    </div>
                </div>
                 
                 <div class="form-group" id="mobile">
                    <label class="col-sm-3 control-label">Mobile</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="mobile" placeholder="Mobile">
                        <span class="help-block hide"></span>
                    </div>
                </div>
                
                
                 <h4 class="section-title">Next of Keen</h4>
                 
                 <div class="form-group" id="next_of_keen">
                    <label class="col-sm-3 control-label">Name</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="next_of_keen" placeholder="Next of Keen">
                        <span class="help-block hide"></span>
                    </div>
                </div>
                 
                  <div class="form-group" id="next_of_keen_relationship">
                    <label class="col-sm-3 control-label">Relationship</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="next_of_keen_relationship" placeholder="Relationship">
                        <span class="help-block hide"></span>
                    </div>
                </div>
                 
                 <div class="form-group" id="address_line_1">
                    <label class="col-sm-3 control-label">Address Line 1</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="next_of_keen_address_line_1" placeholder="Address Line 1">
                        <span class="help-block hide"></span>
                    </div>
                </div>
                
                <div class="form-group" id="address_line_2">
                    <label class="col-sm-3 control-label">Address Line 2</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="next_of_keen_address_line_2" placeholder="Address Line 2">
                        <span class="help-block hide"></span>
                    </div>
                </div>
                
                <div class="form-group" id="street">
                    <label class="col-sm-3 control-label">Street</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="next_of_keen_street" placeholder="Street">
                        <span class="help-block hide"></span>
                    </div>
                </div>
                
                <div class="form-group" id="city">
                    <label class="col-sm-3 control-label">City</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="next_of_keen_city" placeholder="City">
                        <span class="help-block hide"></span>
                    </div>
                </div>
                
                <div class="form-group" id="postcode">
                    <label class="col-sm-3 control-label">Postcode</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="next_of_keen_postcode" placeholder="Postcode">
                        <span class="help-block hide"></span>
                    </div>
                </div>
                
                <div class="form-group" id="phone">
                    <label class="col-sm-3 control-label">Phone</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="next_of_keen_phone" placeholder="Phone">
                        <span class="help-block hide"></span>
                    </div>
                </div>
                 
                 <div class="form-group" id="email">
                    <label class="col-sm-3 control-label">Email Address</label>
                    <div class="col-sm-9">
                        <input type="email" class="form-control" name="next_of_keen_email" placeholder="Email Address">
                        <span class="help-block hide"></span>
                    </div>
                </div>
                 
                 <div class="form-group" id="mobile">
                    <label class="col-sm-3 control-label">Mobile</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="next_of_keen_mobile" placeholder="Mobile">
                        <span class="help-block hide"></span>
                    </div>
                </div>
                 
                 
                 <h4 class="section-title">Immigration</h4>
                 
                 <div class="form-group" id="born_in_the_uk">
                    <label class="col-sm-3 control-label"></label>
                    <div class="col-sm-9">
                        <div class="checkbox">
                            <label><input type="checkbox" name="born_in_the_uk" value="1"/> Born in the UK</label>
                        </div>
                        <span class="help-block hide"></span>
                    </div>
                </div>
                 
                 
                 <div class="form-group" id="arrived_in_the_uk">
                    <label class="col-sm-3 control-label">Arrived in the UK</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="arrived_in_the_uk" placeholder="Arrived in the UK">
                        <span class="help-block hide"></span>
                    </div>
                </div>
                 
                 <div class="form-group" id="immigration_status">
                    <label class="col-sm-3 control-label">Immigration Status</label>
                     <div class="col-sm-9">
                        <select class="form-control" name="immigration_status">
                            <option>1</option>
                            <option>1</option>
                        </select>
                        
                        <input type="text" class="form-control" style="margin-top: 5px;" name="other_immigration_status" placeholder="Other">
                        <span class="help-block hide"></span>
                    </div>
                </div>
                 
                
                 
                 <div class="form-group" id="immigration_note">
                    <label class="col-sm-3 control-label">Note</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="immigration_note"></textarea>
                        <span class="help-block hide"></span>
                    </div>
                </div>
                 
          
                 <h4 class="section-title">Education</h4>
                 
                 <div class="form-group" id="educational_institute">
                    <label class="col-sm-3 control-label">Current Institute</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="educational_institute" placeholder="Institute">
                        <span class="help-block hide"></span>
                    </div>
                </div>
                 
                 <div class="form-group" id="immigration_note">
                    <label class="col-sm-3 control-label">Note</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="educational_note"></textarea>
                        <span class="help-block hide"></span>
                    </div>
                </div>
                 

                 <h4 class="section-title">Language</h4>
                 
                 <div class="form-group" id="first_language">
                    <label class="col-sm-3 control-label">First Language</label>
                    <div class="col-sm-9">
                        <select class="form-control" name="first_language">
                            <option>1</option>
                            <option>1</option>
                        </select>
                        
                        <input type="text" class="form-control" style="margin-top: 5px;" name="other_first_language" placeholder="Other">
                        <span class="help-block hide"></span>
                    </div>
                </div>
                 
                 <div class="form-group" id="level_of_esol">
                    <label class="col-sm-3 control-label">Level of ESOL</label>
                    <div class="col-sm-9">
                       <select class="form-control" name="level_of_esol">
                            <option>1</option>
                            <option>2</option>
                        </select>
                        <span class="help-block hide"></span>
                    </div>
                </div>
                 
                 <div class="form-group" id="interpreter_required">
                    <label class="col-sm-3 control-label"></label>
                    <div class="col-sm-9">
                        <div class="checkbox">
                            <label>  <input type="checkbox" name="interpreter_required" placeholder="Interpreter Required"> Interpreter Required </label>
                        </div>
                        <select class="form-control" name="interpret_language">
                            <option>1</option>
                            <option>2</option>
                        </select>
                        
                        <span class="help-block hide"></span>
                    </div>
                </div>
                 
                 
                 <h4 class="section-title">Ethnicity</h4>
                 
                 <div class="form-group" id="ethnicity">
                    <label class="col-sm-3 control-label">Ethnicity</label>
                    <div class="col-sm-9">
                        <select class="form-control" name="ethnicity">
                            <option>1</option>
                            <option>1</option>
                        </select>
                        
                        <input type="text" class="form-control" style="margin-top: 5px;" name="other_ethnicity" placeholder="Other">
                        <span class="help-block hide"></span>
                    </div>
                </div>
                 
                 <div class="form-group" id="country_of_origin">
                    <label class="col-sm-3 control-label">Country of Origin</label>
                    <div class="col-sm-9">
                        <select class="form-control" name="country_of_origin">
                            <option>1</option>
                            <option>1</option>
                        </select>
                        <span class="help-block hide"></span>
                    </div>
                </div>
                 
                 
                 
                 
                 <h4 class="section-title">Religion</h4>
                 
                 <div class="form-group" id="religion">
                    <label class="col-sm-3 control-label">Religion</label>
                    <div class="col-sm-9">
                        <select class="form-control" name="religion">
                            <option>1</option>
                            <option>1</option>
                        </select>
                        
                        <input type="text" class="form-control" style="margin-top: 5px;" name="other_religion" placeholder="Other">
                        <span class="help-block hide"></span>
                    </div>
                </div>
                 
                 
                 <h4 class="section-title">Health Information</h4>
                 
                 <div class="form-group" id="medical_conditions">
                    <label class="col-sm-3 control-label">Medical Conditions</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="medical_conditions"></textarea>
                        <span class="help-block hide"></span>
                    </div>
                </div>
                 
                 <div class="form-group" id="medical_condition_docs">
                    <label class="col-sm-3 control-label">Medical Documents</label>
                    <div class="col-sm-9">
                        <input type="file" class="btn btn-sm btn-default" name="medical_condition_docs[]" multiple=""/>
                        <span class="help-block hide"></span>
                    </div>
                </div>
                 
                 
                 <div class="form-group" id="allergies">
                    <label class="col-sm-3 control-label">Allergies</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="allergies"></textarea>
                        <span class="help-block hide"></span>
                    </div>
                </div>
                 
                 <div class="form-group" id="allergy_docs">
                    <label class="col-sm-3 control-label">Allergy Documents</label>
                    <div class="col-sm-9">
                        <input type="file" class="btn btn-sm btn-default" name="allergy_docs[]" multiple=""/>
                        <span class="help-block hide"></span>
                    </div>
                </div>
                 
                 <h4 class="section-title">Local Authority</h4>
                 
                 <div class="form-group" id="local_authority">
                    <label class="col-sm-3 control-label">Local Authority</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="local_authority" placeholder="Local Authority">
                        <span class="help-block hide"></span>
                    </div>
                </div>
                 
                 <div class="form-group" id="allergies">
                    <label class="col-sm-3 control-label">Social Worker</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="social_worker" placeholder="Social Worker">
                        <input type="text" class="form-control" style="margin-top: 5px;" name="social_worker_contact" placeholder="Social Worker's Contact">
                        <span class="help-block hide"></span>
                    </div>
                </div>
                 
                 <h4 class="section-title">Learning Support</h4>
                 
                 <div class="form-group" id="allergies">
                    <label class="col-sm-3 control-label">Support</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="allergies"></textarea>
                        <span class="help-block hide"></span>
                    </div>
                </div>
                 
                 <div class="form-group" id="documents">
                    <label class="col-sm-3 control-label">Supporting Documents</label>
                    <div class="col-sm-9">
                        <input type="file" class="btn btn-sm btn-default" name="learning_supporting_documents[]" multiple=""/>
                        <span class="help-block hide"></span>
                    </div>
                </div>
                 
                 <h4 class="section-title">Documents</h4>
                 
                 <div class="form-group" id="documents">
                    <label class="col-sm-3 control-label">Documents</label>
                    <div class="col-sm-9">
                        <input type="file" class="btn btn-sm btn-default" name="documents[]" multiple=""/>
                        <span class="help-block hide"></span>
                    </div>
                </div>
                 
                 <h4 class="section-title">Leave Permission</h4>
                 
                <div class="form-group" id="country_of_origin">
                    <label class="col-sm-3 control-label">Can leave on own</label>
                    <div class="col-sm-9">
                        <select class="form-control" name="heard_about_us_by">
                            <option>No</option>
                            <option>Yes</option>
                        </select>
                        <input type="text" class="form-control" style="margin-top: 5px;" name="collected_by" placeholder="Have to Collected By">
                        
                        <span class="help-block hide"></span>
                    </div>
                </div>
                 
                 
                 <h4 class="section-title">Consents</h4>
                 
                 <div class="form-group" id="has_consent_to_appear_in_publicity_photos">
                    <label class="col-sm-3 control-label"></label>
                    <div class="col-sm-9">
                        <div class="checkbox">
                            <label><input type="checkbox" name="has_consent_to_appear_in_publicity_photos" value="1" checked=""/> Has consent to appear in publicity photos</label>
                        </div>
                        <span class="help-block hide"></span>
                    </div>
                </div>
                 
                 <div class="form-group" id="has_consent_to_appear_in_publicity_videos">
                    <label class="col-sm-3 control-label"></label>
                    <div class="col-sm-9">
                        <div class="checkbox">
                            <label><input type="checkbox" name="has_consent_to_appear_in_publicity_videos" value="1" checked=""/> Has consent to appear in publicity videos</label>
                        </div>
                        <span class="help-block hide"></span>
                    </div>
                </div>
                 
                 <div class="form-group" id="subscribed_newsletter">
                    <label class="col-sm-3 control-label"></label>
                    <div class="col-sm-9">
                        <div class="checkbox">
                            <label><input type="checkbox" name="subscribe_newsletter" value="1" checked=""/> Subscribe Newsletter</label>
                        </div>
                        <span class="help-block hide"></span>
                    </div>
                </div>
                 
                 
                 <h4 class="section-title">About Us</h4>
                 
                <div class="form-group" id="country_of_origin">
                    <label class="col-sm-3 control-label">Known by</label>
                    <div class="col-sm-9">
                        <select class="form-control" name="heard_about_us_by">
                            <option>1</option>
                            <option>1</option>
                        </select>
                        <span class="help-block hide"></span>
                    </div>
                </div>
                 
                

                <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <button type="submit" class="btn btn-primary"> 
                            <i class="fa fa-save fa-fw"></i> Save
                        </button>
                    </div>
                </div>
            </form>
        
        
    </div>
<div class="col-xs-12 col-md-4"></div>
</div>