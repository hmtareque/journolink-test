<div class="header">
    <div class="title"><i class="fa fa-lg fa-user-plus fa-fw"></i> New Student</div>
    <div class="links">
        <a href="#education/students" class="btn btn-success btn-sm"><i class="fa fa-list-alt fa-fw"></i> All Active Students</a>
        
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-8">
        <form class="form-horizontal ajax-form" action="{{url('manage/education/students')}}" method="post" data-hash="education/students">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <input type="hidden" name="service" value="education"/>

            <h4 class="section-title">Personal Information</h4>

            <div class="form-group" id="first_name">
                <label class="col-sm-3 control-label">First Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="first_name" placeholder="First Name">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="middle_name">
                <label class="col-sm-3 control-label">Middle Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="middle_name" placeholder="Middle Name">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="last_name">
                <label class="col-sm-3 control-label">Last Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="last-name" name="last_name" placeholder="Last Name">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="date_of_birth">
                <label class="col-sm-3 control-label">Date of Birth</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control jdob" id="date-of-birth" name="date_of_birth" placeholder="Date of Birth">
                    <span class="help-block hide"></span>
                </div>
            </div>

            

            <div class="form-group" id="gender">
                <label class="col-sm-3 control-label">Gender</label>
                <div class="col-sm-9">
                    <select class="form-control" name="gender">
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                        <option value="other">Other</option>
                    </select>
                    <div id="other_gender">
                        <input type="text" class="form-control hide margin-top-5" name="other_gender" placeholder="Other Gender">
                        <span class="help-block hide"></span>
                    </div>
                </div>
            </div>

            <div class="form-group" id="marital_status">
                <label class="col-sm-3 control-label">Marital Status</label>
                <div class="col-sm-9">
                    <select class="form-control" name="marital_status">
                        <option value="single">Single</option>
                        <option value="married">Married</option>
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="ni_number">
                <label class="col-sm-3 control-label">NI Number</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="ni_number" placeholder="NI Number">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <h4 class="section-title">Address</h4>

            <div class="form-group" id="address_line_1">
                <label class="col-sm-3 control-label">Address Line 1</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="address_line_1" placeholder="Address Line 1">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="address_line_2">
                <label class="col-sm-3 control-label">Address Line 2</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="address_line_2" placeholder="Address Line 2">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="street">
                <label class="col-sm-3 control-label">Street</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="street" placeholder="Street">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="city">
                <label class="col-sm-3 control-label">City</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="city" placeholder="City">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="postcode">
                <label class="col-sm-3 control-label">Postcode</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="postcode" placeholder="Postcode">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="borough">
                <label class="col-sm-3 control-label">Borough</label>
                <div class="col-sm-9">
                    <select class="form-control" name="borough">
                        <option value=""> -- Please Select -- </option>
                        @foreach($boroughes as $borough)
                        <option value="{{trim($borough->name)}}">{{$borough->name}}</option>
                        @endforeach 
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            
            <div id="duplicate-clients"></div>
            

            <h4 class="section-title">Contacts</h4>

            <div class="form-group" id="phone">
                <label class="col-sm-3 control-label">Phone</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="phone" placeholder="Phone">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="email">
                <label class="col-sm-3 control-label">Email Address</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="email" placeholder="Email Address">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="mobile">
                <label class="col-sm-3 control-label">Mobile</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="mobile" placeholder="Mobile">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <h4 class="section-title">Next of Kin</h4>

            <div class="form-group" id="next_of_kin">
                <label class="col-sm-3 control-label">Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="next_of_kin" placeholder="Next of Kin">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="next_of_kin_relationship">
                <label class="col-sm-3 control-label">Relationship</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="next_of_kin_relationship" placeholder="Relationship">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="next-of-kin-address-same-as-main">
                <label class="col-sm-3 control-label">Address Same as Main</label>
                <div class="col-sm-9">
                    <select class="form-control" id="next-of-kin-address" name="next_of_kin_address_same_as_main">
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="hide" id="next-of-kin-address-data">

                <div class="form-group" id="next_of_kin_address_line_1">
                    <label class="col-sm-3 control-label">Address Line 1</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="next_of_kin_address_line_1" placeholder="Address Line 1">
                        <span class="help-block hide"></span>
                    </div>
                </div>

                <div class="form-group" id="next_of_kin_address_line_2">
                    <label class="col-sm-3 control-label">Address Line 2</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="next_of_kin_address_line_2" placeholder="Address Line 2">
                        <span class="help-block hide"></span>
                    </div>
                </div>

                <div class="form-group" id="next_of_kin_street">
                    <label class="col-sm-3 control-label">Street</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="next_of_kin_street" placeholder="Street">
                        <span class="help-block hide"></span>
                    </div>
                </div>

                <div class="form-group" id="next_of_kin_city">
                    <label class="col-sm-3 control-label">City</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="next_of_kin_city" placeholder="City">
                        <span class="help-block hide"></span>
                    </div>
                </div>

                <div class="form-group" id="next_of_kin_postcode">
                    <label class="col-sm-3 control-label">Postcode</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="next_of_kin_postcode" placeholder="Postcode">
                        <span class="help-block hide"></span>
                    </div>
                </div>

            </div>

            <div class="form-group" id="next_of_kin_phone">
                <label class="col-sm-3 control-label">Phone</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="next_of_kin_phone" placeholder="Phone">
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            <div class="form-group" id="next_of_kin_mobile">
                <label class="col-sm-3 control-label">Mobile</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="next_of_kin_mobile" placeholder="Mobile">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="next_of_kin_email">
                <label class="col-sm-3 control-label">Email Address</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="next_of_kin_email" placeholder="Email Address">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <h4 class="section-title">Immigration</h4>

            <div class="form-group" id="born_in_the_uk">
                <label class="col-sm-3 control-label">Born in the UK</label>
                <div class="col-sm-9">
                    <select class="form-control" id="check-born-in-the-uk" name="born_in_the_uk">
                        <option value="0">No</option>
                        <option value="1">Yes</option>
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="arrived_in_the_uk">
                <label class="col-sm-3 control-label">Arrived in the UK</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control jtoday" name="arrived_in_the_uk" placeholder="Arrived in the UK">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="immigration_status">
                <label class="col-sm-3 control-label">Immigration Status</label>
                <div class="col-sm-9">
                    <select class="form-control" name="immigration_status">
                        <option value=""> -- Please Select -- </option>
                        @foreach($immigration_statuses as $status)
                        <option value="{{$status->id}}">{{$status->name}}</option>
                        @endforeach 
                        <option value="other">Other</option>
                    </select>
                    <div id="other_immigration_status">
                        <input type="text" class="form-control hide margin-top-5" name="other_immigration_status" placeholder="Other Immigration Status">
                        <span class="help-block hide"></span>
                    </div>
                </div>
            </div>

            <div class="form-group" id="passport_no">
                <label class="col-sm-3 control-label">Passport No.</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control jdate" name="passport_no" placeholder="Passport No.">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="visa_expiry_date">
                <label class="col-sm-3 control-label">Visa Expiry Date</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control jtoday" name="visa_expiry_date" placeholder="Visa Expiry Date">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="homeoffice_ref_no">
                <label class="col-sm-3 control-label">Home Office Ref No.</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control jdate" name="homeoffice_ref_no" placeholder="Home Office Ref No.">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="has_dependent">
                <label class="col-sm-3 control-label">Has Dependents</label>
                <div class="col-sm-9">
                    <select class="form-control" id="has-dependent" name="has_dependents">
                        <option value="0">No</option>
                        <option value="1">Yes</option>
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>


            <div class="form-group hide" id="no_of_dependents">
                <label class="col-sm-3 control-label">No of Dependents</label>
                <div class="col-sm-9">
                    <select class="form-control" id="next-of-kin-address" name="no_of_dependents">
                        <option value="0">None</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                        <option value="4">4</option>
                        <option value="5">5</option>
                        <option value="6">6</option>
                        <option value="7">7</option>
                        <option value="8">8</option>
                        <option value="9">9</option>
                        <option value="10">10</option>
                        <option value="11">11</option>
                        <option value="12">12</option>
                        <option value="13">13</option>
                        <option value="14">14</option>
                        <option value="15">15</option>
                        <option value="16">16</option>
                        <option value="17">17</option>
                        <option value="18">18</option>
                        <option value="19">19</option>
                        <option value="20">20</option>
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group hide" id="dependents_details">
                <label class="col-sm-3 control-label">Dependents Details</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="dependents_details"></textarea>
                    <span class="help-block hide"></span>
                </div>
            </div>


            <div class="form-group" id="solicitor_name">
                <label class="col-sm-3 control-label">Solicitor Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="solicitor_name" placeholder="Solicitor Name">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="solicitor_contact">
                <label class="col-sm-3 control-label">Solicitor Contact</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="solicitor_contact" placeholder="Solicitor Contact">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="immigration_note">
                <label class="col-sm-3 control-label">Note</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="immigration_note"></textarea>
                    <span class="help-block hide"></span>
                </div>
            </div>



            <h4 class="section-title">Language</h4>

            <div class="form-group" id="first_language">
                <label class="col-sm-3 control-label">First Language</label>
                <div class="col-sm-9">
                    <select class="form-control" name="first_language">
                        <option value=""> -- Please Select -- </option>
                        @foreach(languages() as $code => $language)
                        <option value="{{$code}}">{{$language}}</option>
                        @endforeach 
                        <option value="other">Other</option>
                    </select>
                    <div id="other_first_language">
                        <input type="text" class="form-control hide margin-top-5" name="other_first_language" placeholder="Other First Language">
                        <span class="help-block hide"></span>
                    </div>
                </div>
            </div>

            <div class="form-group" id="level_of_english">
                <label class="col-sm-3 control-label">Level of English <small>(ESOL)</small></label>
                <div class="col-sm-9">
                    <select class="form-control" name="level_of_english">
                        <option value="below">Below</option>
                        <option value="inline">Inline</option>
                        <option value="above">Above</option>
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="interpreter_required">
                <label class="col-sm-3 control-label">Interpreter Required</label>
                <div class="col-sm-9">
                    <select class="form-control" id="interpreter-required" name="interpreter_required">
                        <option value="0">No</option>
                        <option value="1">Yes</option>
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group hide" id="interpreter_language">
                <label class="col-sm-3 control-label">Interpreter Language</label>
                <div class="col-sm-9">
                    <select class="form-control" name="interpreter_language">
                        <option value=""> -- Please Select -- </option>
                        @foreach(languages() as $code => $language)
                        <option value="{{$code}}">{{$language}}</option>
                        @endforeach 
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <h4 class="section-title">Education</h4>

            <div class="form-group" id="education_institute">
                <label class="col-sm-3 control-label">Institute</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="education_institute" placeholder="Institute">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="supervisor_name">
                <label class="col-sm-3 control-label">Supervisor Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="supervisor_name" placeholder="Supervisor Name">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="supervisor_contact">
                <label class="col-sm-3 control-label">Supervisor Contact</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="supervisor_contact" placeholder="Supervisor Contact">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="education_note">
                <label class="col-sm-3 control-label">Note</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="education_note"></textarea>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <h4 class="section-title">Health</h4>

            <div class="form-group" id="gp_name">
                <label class="col-sm-3 control-label">GP Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="gp_name" placeholder="GP Name">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="gp_contact">
                <label class="col-sm-3 control-label">GP Contact</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="gp_contact" placeholder="GP Contact">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="has_medical_condition">
                <label class="col-sm-3 control-label">Has Medical Conditions</label>
                <div class="col-sm-9">
                    <select class="form-control" id="has-medical-conditions" name="has_medical_condition">
                        <option value="0">No</option>
                        <option value="1">Yes</option>
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group hide" id="medical_conditions">
                <label class="col-sm-3 control-label">Medical Conditions</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="medical_conditions"></textarea>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="has_allergy">
                <label class="col-sm-3 control-label">Has Allergy</label>
                <div class="col-sm-9">
                    <select class="form-control" id="has-allergy" name="has_allergy">
                        <option value="0">No</option>
                        <option value="1">Yes</option>
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group hide" id="allergies">
                <label class="col-sm-3 control-label">Allergies</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="allergies"></textarea>
                    <span class="help-block hide"></span>
                </div>
            </div>




            <div class="form-group" id="has_disability">
                <label class="col-sm-3 control-label">Has Disability</label>
                <div class="col-sm-9">
                    <select class="form-control" id="has-disability" name="has_disability">
                        <option value="0">No</option>
                        <option value="1">Yes</option>
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group hide" id="disabilities">
                <label class="col-sm-3 control-label">Disabilities</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="disabilities"></textarea>
                    <span class="help-block hide"></span>
                </div>
            </div>


            <h4 class="section-title">Race, Religion and Origin</h4>

            <div class="form-group" id="ethnicity">
                <label class="col-sm-3 control-label">Ethnicity</label>
                <div class="col-sm-9">
                    <select class="form-control" name="ethnicity">
                        <option value=""> -- Please Select -- </option>
                        @foreach(ethnicities() as $ethnicity_group => $ethnicities)
                        <optgroup label="{{$ethnicity_group}}">
                            @foreach($ethnicities as $ethnicity_code => $ethnicity) 
                            <option value="{{$ethnicity_code}}">{{$ethnicity}}</option>
                            @endforeach 
                        </optgroup>
                        @endforeach 
                    </select>
                </div>
            </div>

            <div class="form-group" id="religion">
                <label class="col-sm-3 control-label">Religion</label>
                <div class="col-sm-9">
                    <select class="form-control" name="religion">
                        <option value=""> -- Please Select -- </option>
                        @foreach(religion() as $religion)
                        <option value="{{$religion}}">{{$religion}}</option>
                        @endforeach 
                        <option value="other">Other</option>
                    </select>

                    <input type="text" class="form-control margin-top-5 hide" name="other_religion" placeholder="Other Religion">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="country_of_origin">
                <label class="col-sm-3 control-label">Country of Origin</label>
                <div class="col-sm-9">
                    <select class="form-control" name="country_of_origin">
                        <option value=""> -- Please Select -- </option>
                        @foreach(countries() as $code => $country)
                        <option value="{{$code}}">{{$country}}</option>
                        @endforeach 
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <h4 class="section-title">Local Authority</h4>

            <div class="form-group" id="local_authority">
                <label class="col-sm-3 control-label">Local Authority</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="local_authority" placeholder="Local Authority">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="local_authority_note">
                <label class="col-sm-3 control-label">Note</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="local_authority_note"></textarea>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="social_worker_name">
                <label class="col-sm-3 control-label">Social Worker Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="social_worker_name" placeholder="Social Worker Name">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="social_worker_contact">
                <label class="col-sm-3 control-label">Social Worker Contact</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="social_worker_contact" placeholder="Social Worker Contact">
                    <span class="help-block hide"></span>
                </div>
            </div>


            <h4 class="section-title">Additional Support</h4>
            <div class="form-group" id="need_additional_support">
                <label class="col-sm-3 control-label">Need Support</label>
                <div class="col-sm-9">
                    <select class="form-control" id="need-additional-support" name="need_additional_support">
                        <option value="0">No</option>
                        <option value="1">Yes</option>
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group hide" id="additional_support">
                <label class="col-sm-3 control-label">Additional Support</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="additional_support"></textarea>
                    <span class="help-block hide"></span>
                </div>
            </div>



            <h4 class="section-title">Consents</h4>

            <div class="form-group" id="has_consent_to_use_info">
                <label class="col-sm-3 control-label"></label>
                <div class="col-sm-9">
                    <div class="checkbox">
                        <label><input type="checkbox" name="has_consent_to_use_info" value="1" checked=""/> Has consent to use information by {{config('settings.app_name')}}</label>
                    </div>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="has_consent_to_contacted_for_activities">
                <label class="col-sm-3 control-label"></label>
                <div class="col-sm-9">
                    <div class="checkbox">
                        <label><input type="checkbox" name="has_consent_to_contacted_for_activities" value="1" checked=""/> Has consent to contacted by {{config('settings.app_name')}} for Activities</label>
                    </div>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="has_consent_to_appear_in_publicity_photos">
                <label class="col-sm-3 control-label"></label>
                <div class="col-sm-9">
                    <div class="checkbox">
                        <label><input type="checkbox" name="has_consent_to_appear_in_publicity_photos" value="1" checked=""/> Has consent to appear in publicity photos</label>
                    </div>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="has_consent_to_appear_in_publicity_videos">
                <label class="col-sm-3 control-label"></label>
                <div class="col-sm-9">
                    <div class="checkbox">
                        <label><input type="checkbox" name="has_consent_to_appear_in_publicity_videos" value="1" checked=""/> Has consent to appear in publicity videos</label>
                    </div>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="subscribed_newsletter">
                <label class="col-sm-3 control-label"></label>
                <div class="col-sm-9">
                    <div class="checkbox">
                        <label><input type="checkbox" name="subscribe_newsletter" value="1" checked=""/> Subscribe Newsletter</label>
                    </div>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <h4 class="section-title">About Us</h4>
            <div class="form-group" id="heard_about_us_by">
                <label class="col-sm-3 control-label">Heard about us by</label>
                <div class="col-sm-9">
                    <select class="form-control" name="heard_about_us_by">
                        <option value=""> -- Please Select -- </option>
                        @foreach($heard_about_us_by as $source)
                        <option value="{{$source->name}}">{{$source->name}}</option>
                        @endforeach 
                        <option value="other">Other</option>
                    </select>
                    <div id="other_heard_about_us_by">
                        <input type="text" class="form-control hide margin-top-5" name="other_heard_about_us_by" placeholder="Other Source">
                        <span class="help-block hide"></span>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-primary"> 
                        <i class="fa fa-save fa-fw"></i> Save
                    </button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-xs-12 col-md-4">
        @include('app.shared.feed')
    </div>
</div>

