<div class="header">
    <div class="title"><i class="fa fa-lg fa-user-circle-o fa-fw"></i> Students</div>
    <div class="links">
        
        <a href="#education/students/create" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Enrol New Student</a>
        
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Classes <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#education/classes"><i class="fa fa-list"></i> All Classes</a></li>
                <li><a href="#education/classes/create"><i class="fa fa-plus"></i> Create New Class</a></li>
             </ul>
        </div>
       
        
    </div>
</div>



<div class="row">
    <div class="col-xs-12 col-md-9">
        <h4 class="section-title">Search Students</h4>
        
         <form class="form-horizontal ajax-form" action="{{url('manage/form')}}" method="post">
                <input type="hidden" name="_token" value="{{csrf_token()}}"/>

                <div class="form-group" id="name">
                    <label class="col-sm-2 control-label">Name</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" name="keyword" placeholder="Last Name or Date of Birth or Postcode or Phone or Mobile">
                        <span class="help-block hide"></span>
                    </div>
                </div>

                <div class="form-group" id="email">
                    <label for="inputEmail3" class="col-sm-2 control-label">School</label>
                    <div class="col-sm-10">
                        <select name="school" class="form-control">
                            <option> -- Please Select -- </option>
                        </select>
                        <span class="help-block hide"></span>
                    </div>
                </div>
                
                <div class="form-group">
                    <label for="inputEmail3" class="col-sm-2 control-label">Class</label>
                    <div class="col-sm-10">
                        <select name="school" class="form-control">
                            <option> -- Please Select -- </option>
                        </select>
                        <span class="help-block hide"></span>
                    </div>
                </div>
                
               

               

                <div class="form-group">
                    <div class="col-sm-offset-2 col-sm-10">
                        <button type="submit" class="btn btn-primary"> 
                            <i class="fa fa-search fa-fw"></i> Search
                        </button>
                    </div>
                </div>
            </form>
        
        
        
        
        <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->

   
        <div class="table-responsive">
            <table class="table table-striped table-bordered table-hover" width="100%">
                <thead>
                    <tr>
                        <th>Ref. No.</th>
                        <th>Name</th>
                        <th>Date of Birth</th>
                        <th>School</th>
                        <th>Class</th>
                        <th>Postcode</th>
                        <th>Contact</th>
                        
                    </tr>
                </thead>
                <tbody>
                   
                    <tr>
                        <td class="text-center"><strong></strong></td>
                        <td></td>

                        
                        <td></td>
                       

                        <td class="text-center">
                        <td>
                            
                        </td>
                     
                    </tr>
              
                </tbody>
            </table>
        </div>

       
        <p class="alert alert-info">
            <i class="fa fa-info"></i>
        </p>
        
        
        
        
        
        
        
    </div>
    <div class="col-xs-12 col-md-3">
        
        
        
    </div>
</div>