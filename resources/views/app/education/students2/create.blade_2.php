<div class="header">
    <div class="title"><i class="fa fa-lg fa-plus-square-o fa-fw"></i> Enrol New Student</div>
    <div class="links">

        <a href="#education/students" class="btn btn-primary btn-sm"><i class="fa fa-list-alt fa-fw"></i> All Students</a>

    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-9">
        <form class="form-horizontal ajax-form" action="{{url('manage/education/students')}}" method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>

            <h4 class="section-title">Personal Information</h4>

            <div class="form-group" id="first_name">
                <label class="col-sm-3 control-label">First Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="first_name" placeholder="First Name">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="middle_name">
                <label class="col-sm-3 control-label">Middle Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="middle_name" placeholder="Middle Name">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="last_name">
                <label class="col-sm-3 control-label">Last Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="last_name" placeholder="Last Name">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="date_of_birth">
                <label class="col-sm-3 control-label">Date of Birth</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control jdob" name="date_of_birth" placeholder="Date of Birth">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="gender">
                <label class="col-sm-3 control-label">Gender</label>
                <div class="col-sm-9">
                    <select class="form-control" name="gender">
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                        <option value="other">Other</option>
                    </select>
                    <div id="other_gender">
                        <input type="text" class="form-control hide margin-top-5" name="other_gender" placeholder="Other Gender">
                        <span class="help-block hide"></span>
                    </div>
                </div>
            </div>

            <h4 class="section-title">Enrolment</h4>

            <div class="form-group" id="type">
                <label class="col-sm-3 control-label">Type</label>
                <div class="col-sm-9">
                    <select class="form-control" id="student-type" name="type">
                        <option value="child">Child</option>
                        <option value="adult">Adult</option>
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="enrolment_date">
                <label class="col-sm-3 control-label">Enrolment Date</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control jtoday" name="enrolment_date" placeholder="Enrolment Date">
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            <div class="form-group" id="referred_by">
                <label class="col-sm-3 control-label">Referral School</label>
                <div class="col-sm-9">
                    <select class="form-control" name="referred_by">
                        <option value=""> -- Please Select -- </option>
                        @foreach($referral_schools as $referral)
                        <option value="{{$referral->id}}">{{$referral->name}}</option>
                        @endforeach 
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="academic_year">
                <label class="col-sm-3 control-label">Academic Year</label>
                <div class="col-sm-9">
                    <select class="form-control" id="select-academic-year" name="academic_year">
                        <option value=""> -- Please Select -- </option>
                        @foreach($academic_years as $academic_year) 
                        <option value="{{$academic_year->id}}">{{$academic_year->year}}</option>
                        @endforeach 
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="term">
                <label class="col-sm-3 control-label">Term</label>
                <div class="col-sm-9" id="year-terms">
                    <select class="form-control" name="term">
                        <option value=""> -- Please Select -- </option>
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="school">
                <label class="col-sm-3 control-label">School</label>
                <div class="col-sm-9">
                    <select class="form-control" id="select-school" name="school">
                        <option value=""> -- Please Select -- </option>
                        @foreach($schools as $school)
                        <option value="{{$school->id}}">{{$school->name}}</option>
                        @endforeach 
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="classes">
                <label class="col-sm-3 control-label">Class</label>
                <div class="col-sm-9" id="school-classes">
                    <span class="label label-info">Please select school.</span>
                </div>
            </div>


            <h4 class="section-title">Address</h4>

            <div class="form-group" id="address_line_1">
                <label class="col-sm-3 control-label">Address Line 1</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="address_line_1" placeholder="Address Line 1">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="address_line_2">
                <label class="col-sm-3 control-label">Address Line 2</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="address_line_2" placeholder="Address Line 2">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="street">
                <label class="col-sm-3 control-label">Street</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="street" placeholder="Street">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="city">
                <label class="col-sm-3 control-label">City</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="city" placeholder="City">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="postcode">
                <label class="col-sm-3 control-label">Postcode</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="postcode" placeholder="Postcode">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="borough">
                <label class="col-sm-3 control-label">Borough</label>
                <div class="col-sm-9">
                    <select class="form-control" name="borough">
                        <option value=""> -- Please Select -- </option>
                        @foreach($boroughes as $borough)
                        <option value="{{trim($borough->name)}}">{{$borough->name}}</option>
                        @endforeach 
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <h4 class="section-title">Contacts</h4>

            <div class="form-group" id="phone">
                <label class="col-sm-3 control-label">Phone</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="phone" placeholder="Phone">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="email">
                <label class="col-sm-3 control-label">Email Address</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="email" placeholder="Email Address">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="mobile">
                <label class="col-sm-3 control-label">Mobile</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="mobile" placeholder="Mobile">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <h4 class="section-title">Next of Kin</h4>

            <div class="form-group" id="next_of_kin">
                <label class="col-sm-3 control-label">Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="next_of_kin" placeholder="Next of Kin">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="next_of_kin_relationship">
                <label class="col-sm-3 control-label">Relationship</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="next_of_kin_relationship" placeholder="Relationship">
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            <div class="form-group" id="next-of-kin-address-same-as-main">
                <label class="col-sm-3 control-label">Address Same as Main</label>
                <div class="col-sm-9">
                    <select class="form-control" id="next-of-kin-address" name="next_of_kin_address_same_as_main">
                        <option value="1">Yes</option>
                        <option value="0">Not</option>
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            <div class="hide" id="next-of-kin-address-data">

            <div class="form-group" id="next_of_kin_address_line_1">
                <label class="col-sm-3 control-label">Address Line 1</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="next_of_kin_address_line_1" placeholder="Address Line 1">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="next_of_kin_address_line_2">
                <label class="col-sm-3 control-label">Address Line 2</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="next_of_kin_address_line_2" placeholder="Address Line 2">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="next_of_kin_street">
                <label class="col-sm-3 control-label">Street</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="next_of_kin_street" placeholder="Street">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="next_of_kin_city">
                <label class="col-sm-3 control-label">City</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="next_of_kin_city" placeholder="City">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="next_of_kin_postcode">
                <label class="col-sm-3 control-label">Postcode</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="next_of_kin_postcode" placeholder="Postcode">
                    <span class="help-block hide"></span>
                </div>
            </div>
                
            </div>

            <div class="form-group" id="next_of_kin_phone">
                <label class="col-sm-3 control-label">Phone</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="next_of_kin_phone" placeholder="Phone">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="next_of_kin_email">
                <label class="col-sm-3 control-label">Email Address</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="next_of_kin_email" placeholder="Email Address">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="next_of_kin_mobile">
                <label class="col-sm-3 control-label">Mobile</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="next_of_kin_mobile" placeholder="Mobile">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <h4 class="section-title">Immigration</h4>

            <div class="form-group" id="born_in_the_uk">
                <label class="col-sm-3 control-label"></label>
                <div class="col-sm-9">
                    <div class="checkbox">
                        <label><input type="checkbox" id="check-born-in-the-uk" name="born_in_the_uk" value="1"/> Born in the UK</label>
                    </div>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="arrived_in_the_uk">
                <label class="col-sm-3 control-label">Arrived in the UK</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control jtoday" name="arrived_in_the_uk" placeholder="Arrived in the UK">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="immigration_status">
                <label class="col-sm-3 control-label">Immigration Status</label>
                <div class="col-sm-9">
                    <select class="form-control" name="immigration_status">
                        <option value=""> -- Please Select -- </option>
                        @foreach($immigration_statuses as $status)
                        <option value="{{$status->name}}">{{$status->name}}</option>
                        @endforeach 
                        <option value="other">Other</option>
                    </select>
                    <div id="other_immigration_status">
                        <input type="text" class="form-control hide margin-top-5" name="other_immigration_status" placeholder="Other Immigration Status">
                        <span class="help-block hide"></span>
                    </div>
                </div>
            </div>

            <div class="form-group" id="immigration_note">
                <label class="col-sm-3 control-label">Note</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="immigration_note"></textarea>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <h4 class="section-title">Language</h4>

            <div class="form-group" id="first_language">
                <label class="col-sm-3 control-label">First Language</label>
                <div class="col-sm-9">
                    <select class="form-control" name="first_language">
                        <option value=""> -- Please Select -- </option>
                        @foreach(languages() as $language)
                        <option value="{{$language}}">{{$language}}</option>
                        @endforeach 
                        <option value="other">Other</option>
                    </select>
                    <div id="other_first_language">
                        <input type="text" class="form-control hide margin-top-5" name="other_first_language" placeholder="Other First Language">
                        <span class="help-block hide"></span>
                    </div>
                </div>
            </div>

            <h4 class="section-title">Ethnicity</h4>

            <div class="form-group" id="ethnicity">
                <label class="col-sm-3 control-label">Ethnicity</label>
                <div class="col-sm-9">
                    <select class="form-control" name="ethnicity">
                        <option value=""> -- Please Select -- </option>
                        @foreach(nationalities() as $ethnicity)
                        <option value="{{$ethnicity}}">{{$ethnicity}}</option>
                        @endforeach 
                        <option value="other">Other</option>
                    </select>
                    <div id="other_ethnicity">
                        <input type="text" class="form-control hide margin-top-5" name="other_ethnicity" placeholder="Other Ethnicity">
                        <span class="help-block hide"></span>
                    </div>
                </div>
            </div>

            <h4 class="section-title">Health Information</h4>

            <div class="form-group" id="medical_conditions">
                <label class="col-sm-3 control-label">Medical Conditions</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="medical_conditions"></textarea>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="medical_condition_docs">
                <label class="col-sm-3 control-label">Medical Documents</label>
                <div class="col-sm-9">
                    <input type="file" class="btn btn-sm btn-default" name="medical_condition_docs[]" multiple=""/>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="allergies">
                <label class="col-sm-3 control-label">Allergies</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="allergies"></textarea>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="allergy_docs">
                <label class="col-sm-3 control-label">Allergy Documents</label>
                <div class="col-sm-9">
                    <input type="file" class="btn btn-sm btn-default" name="allergy_docs[]" multiple=""/>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <h4 class="section-title">Additional Support</h4>

            <div class="form-group" id="additional_support">
                <label class="col-sm-3 control-label">Support</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="additional_support"></textarea>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="additional_support_docs">
                <label class="col-sm-3 control-label">Documents</label>
                <div class="col-sm-9">
                    <input type="file" class="btn btn-sm btn-default" name="additional_support_docs[]" multiple=""/>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="child-only">
                <h4 class="section-title">Leave Permission</h4>
                <div class="form-group" id="can_leave_on_own">
                    <label class="col-sm-3 control-label">Can leave on own</label>
                    <div class="col-sm-9">
                        <select class="form-control" id="can-leave-on-own" name="can_leave_on_own">
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                        <div id="have_to_collect_by">
                            <input type="text" class="form-control hide margin-top-5" id="have-to-collect-by" name="have_to_collect_by" placeholder="Have to Collected By">
                            <span class="help-block hide"></span>
                        </div>
                    </div>
                </div>
            </div>

            <h4 class="section-title">Payments</h4>

            <div class="form-group" id="payment_description">
                <label class="col-sm-3 control-label">Description</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="payment_description" placeholder="Payment Description" value="Saturday school fee">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="fee">
                <label class="col-sm-3 control-label">Fee</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control text-right" name="fee" placeholder="Fee Amount" value="">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="exempt_from_fee">
                <label class="col-sm-3 control-label">Exempt from fee</label>
                <div class="col-sm-9">
                    <select class="form-control" id="exempt-from-fee" name="exempt_from_fee">
                        <option value="0">No</option>
                        <option value="1">Yes</option>
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="discount">
                <label class="col-sm-3 control-label">Discount</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control text-right" name="discount" placeholder="Discount" value="">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="paid">
                <label class="col-sm-3 control-label">Paid</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control text-right" name="paid" placeholder="Paid" value="">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="payment_note">
                <label class="col-sm-3 control-label">Note</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="payment_note"></textarea>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="payment_documents">
                <label class="col-sm-3 control-label">Documents</label>
                <div class="col-sm-9">
                    <input type="file" class="btn btn-sm btn-default" name="payment_docs[]" multiple=""/>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <h4 class="section-title">Consents</h4>
            <div class="form-group" id="has_consent_to_appear_in_publicity_photos">
                <label class="col-sm-3 control-label"></label>
                <div class="col-sm-9">
                    <div class="checkbox">
                        <label><input type="checkbox" name="has_consent_to_appear_in_publicity_photos" value="1" checked=""/> Has consent to appear in publicity photos</label>
                    </div>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="has_consent_to_appear_in_publicity_videos">
                <label class="col-sm-3 control-label"></label>
                <div class="col-sm-9">
                    <div class="checkbox">
                        <label><input type="checkbox" name="has_consent_to_appear_in_publicity_videos" value="1" checked=""/> Has consent to appear in publicity videos</label>
                    </div>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="subscribed_newsletter">
                <label class="col-sm-3 control-label"></label>
                <div class="col-sm-9">
                    <div class="checkbox">
                        <label><input type="checkbox" name="subscribe_newsletter" value="1" checked=""/> Subscribe Newsletter</label>
                    </div>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <h4 class="section-title">About Us</h4>
            <div class="form-group" id="heard_about_us_by">
                <label class="col-sm-3 control-label">Heard about us by</label>
                <div class="col-sm-9">
                    <select class="form-control" name="heard_about_us_by">
                        <option value=""> -- Please Select -- </option>
                        @foreach($heard_about_us_by as $source)
                        <option value="{{$source->name}}">{{$source->name}}</option>
                        @endforeach 
                        <option value="other">Other</option>
                    </select>
                    <div id="other_heard_about_us_by">
                        <input type="text" class="form-control hide margin-top-5" name="other_heard_about_us_by" placeholder="Other Source">
                        <span class="help-block hide"></span>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-primary"> 
                        <i class="fa fa-save fa-fw"></i> Save
                    </button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-xs-12 col-md-3">
        @include('app.shared.feed')
    </div>
</div>

