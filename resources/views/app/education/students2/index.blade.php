<div class="header">
    <div class="title"><i class="fa fa-lg fa-user-circle-o fa-fw"></i> Students</div>
    <div class="links">
        <a href="#education/students/create" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Create New Student</a>
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Classes <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#education/classes"><i class="fa fa-list"></i> All Classes</a></li>
                <li><a href="#education/classes/create"><i class="fa fa-plus"></i> Create New Class</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-9">

        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Recent Students</h3>
            </div>
            <div class="panel-body">
                @if($students->count()>0) 
                <div class="table-responsive">
                    <table class="table data-table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>Ref. No.</th>
                                <th>Name</th>
                                <th>Date of Birth</th>
                                <th>Postcode</th>
                                <th>Contact</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($students as $student) 
                            <tr>
                                <td><a href="#education/students/{{$student->id}}"><strong>{{$student->ref_no}}</strong></a></td>
                                <td>{{$student->first_name}} {{$student->middle_name}}  <strong>{{$student->last_name}} </strong></td>
                                <td>{{$student->date_of_birth}}</td>
                                <td>{{$student->postcode}}</td>
                                <td>
                                    @if($student->mobile != "") 
                                    {{$student->mobile}}
                                    @elseif($student->phone != "") 
                                    {{$student->phone}}
                                    @else 
                                    <a href="mailto:{{$student->email}}">{{$student->email}}</a>
                                    @endif 
                                </td>
                            </tr>
                            @endforeach 
                        </tbody>
                    </table>
                </div>
                @else 
                <div class="alert alert-warning">No student found! Please create student.</div>
                @endif
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-md-3">
        @include('app.shared.feed')
    </div> 

</div>

