<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link href="{{asset('css/app-requisite.css')}}" rel="stylesheet">
        
  <style>
    @page { margin: 100px 25px; }
    header { position: fixed; top: -60px; left: 0px; right: 0px; background-color: lightblue; height: 50px; }
    footer { position: fixed; bottom: -60px; left: 0px; right: 0px; background-color: lightblue; height: 50px; }
    p { page-break-after: always; }
    p:last-child { page-break-after: never; } 

    body {
        background: #fff;
    }
    
  </style>
</head>
<body>
  <header>header</header>
  <footer>footer</footer>
  <main>
      
        @if(isset($data))
                <h4 class="section-title">From <span class="text-primary">{{date('d M Y', strtotime(php_date($request['from'])))}}</span> To <span class="text-primary">{{date('d M Y', strtotime(php_date($request['to'])))}}</span></h4>

                <h5><strong>Enrolled Students</strong></h5>
                
                @if($data['students']->count()>0) 
                <table class="table table-bordered table-condensed table-hover">
                    <thead>
                        <tr>
                            <th>Student</th>
                            <th>Enrolled At</th>
                            <th>School</th>
                            <th>Subject</th>
                            <th>Year Group</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($data['students'] as $student) 
                        <tr>
                            <td>{{$student->first_name}} {{$student->middle_name}} {{$student->last_name}}</td>
                            <td>{{date('d M Y', strtotime($student->date_of_enrolment))}}</td>
                            <td>{{$student->school_name}}</td>
                            <td>{{$student->course}}</td>
                            <td>{{$student->group}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>  
                @else 
                <div class="alert alert-warning">No student enrolled in this period!</div>
                @endif 

                <h5><strong>Students Attendance</strong></h5>
                
                @if(count($data['attendances'])>0) 
                <table class="table table-bordered table-condensed table-hover">
                    <thead>
                        <tr>
                            
                            <th>Student</th>
                            <th>Date</th>
                            <th>School</th>
                            <th>Class</th>
                            <th class="text-center">Attended</th>
                            <th>Reason for Absence</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data['attendances'] as $student)
                       
                        @foreach($student['attendances'] as $attendance)
                        
                        <tr @if($attendance->present == 0) class="danger" @endif>
                             <td>{{$student['name']}}</td>
                            <td>{{date('d M Y', strtotime($attendance->date))}}</td>
                            <td>{{$attendance->school_name}}</td>
                            <td>{{$attendance->course}} {{$attendance->group}}</td>
                            <td class="text-center">
                                @if($attendance->present == 1) Yes @else No @endif 
                            </td>
                            <td>
                                @if($attendance->present == 0) <small>{{$attendance->reason_for_absence}}</small> @endif 
                            </td>
                        </tr>
                        @endforeach 
                        @endforeach
                    </tbody>
                </table>  
                @else 
                <div class="alert alert-warning">No student attendances found in this period!</div>
                @endif 
                
                @endif 

  </main>
</body>
</html>



