<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link href="{{asset('css/app-requisite.css')}}" rel="stylesheet">
        
  <style>
    @page { margin: 100px 25px; }
    header { position: fixed; top: -60px; left: 0px; right: 0px; background-color: lightblue; height: 50px; }
    footer { position: fixed; bottom: -60px; left: 0px; right: 0px; background-color: lightblue; height: 50px; }
    p { page-break-after: always; }
    p:last-child { page-break-after: never; } 

    body {
        background: #fff;
    }
    
  </style>
</head>
<body>
  <header>header</header>
  <footer>footer</footer>
  <main>
      
       
                @if(isset($data))
                <h4 class="section-title">Report</h4>

                <h5><strong>Currently Enrolled in Classes</strong></h5>

                @if($data['classes']->count()>0) 
                <table class="table table-bordered table-condensed table-hover">
                    <thead>
                        <tr>
                            <th>Enrolled At</th>
                            <th>School</th>
                            <th>Subject</th>
                            <th>Year Group</th>
                            <th>Status</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($data['classes'] as $student) 
                        <tr>
                            <td>{{date('d M Y', strtotime($student->date_of_enrolment))}}</td>
                            <td>{{$student->school}}</td>
                            <td>{{$student->course}}</td>
                            <td>{{$student->group}}</td>
                            <td>{{$student->student_status}}</td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>  
                @else 
                <div class="alert alert-warning">No student enrolled in this period!</div>
                @endif 
                
                <h5><strong>Attendance From <span class="text-primary">{{date('d M Y', strtotime(php_date($request['from'])))}}</span> To <span class="text-primary">{{date('d M Y', strtotime(php_date($request['to'])))}}</span></strong></h5>


                
                @if($data['attendances']->count()>0) 
                <table class="table table-bordered table-condensed table-hover">
                    <thead>
                        <tr>
                            <th>Date</th>
                            <th>Class</th>
                            <th class="text-center">Attended</th>
                            <th>Reason for Absence</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($data['attendances'] as $attendance) 
                        <tr @if($attendance->present == 0) class="danger" @endif>
                            <td>{{date('d M Y', strtotime($attendance->date))}}</td>
                            <td>
                                {{$attendance->course}} {{$attendance->group}} {{$attendance->school}} <small class="text-primary">{{$attendance->class_status}}</small>
                            </td>
                            <td class="text-center">
                                @if($attendance->present == 1) Yes @else No @endif 
                            </td>
                            <td>
                                @if($attendance->present == 0) <small>{{$attendance->reason_for_absence}}</small> @endif 
                            </td>
                        </tr>
                        @endforeach
                    </tbody>
                </table>  
                @else 
                <div class="alert alert-warning">No student attended in this period!</div>
                @endif
                
                <h5><strong>Assessments</strong></h5>

                @if(count($data['assessments'])>0) 
                <table class="table table-bordered table-condensed table-hover">
                    <thead>
                        <tr>
                            <th rowspan="2" width="50%">Term</th>
                            <th colspan="2" width="50%" class="text-center">Assessment</th>
                        </tr>
                        <tr>
                            <th width="25%" class="text-center">External</th>
                            <th width="25%" class="text-center">Internal</th>
                        </tr>
                    </thead>
                    <tbody>

                        @foreach($data['assessments'] as $class)
                        <tr>
                            <td colspan="3"><strong>{{$class['class']}} {{$class['school']}}</strong></td>
                        </tr>
                        @foreach($class['terms'] as $term)
                        <tr>
                            
                            <td><strong>{{$term['term']}}</strong> {{$term['year']}}<br/><small>{{$term['dates']}}</small></td>
                            <td class="text-center">
                                @if(isset($term['assessments']['external']))
                                
                                <?php $external_level = $term['assessments']['external']['level']; ?>
                                @if($external_level == 'below') 
                                <label class="btn btn-xs btn-danger btn-block">{{$external_level}}</label>
                                @elseif($external_level == 'inline') 
                                <label class="btn btn-xs btn-warning btn-block">{{$external_level}}</label>
                                @elseif($external_level == 'above') 
                                <label class="btn btn-xs btn-success btn-block">{{$external_level}}</label>
                                @else 
                                <label class="btn btn-xs btn-default btn-block">not assessed</label>
                                @endif 
                                
                                @if($term['assessments']['external']['note'] != "")<small>{{$term['assessments']['external']['note']}}</small>@endif

                                @if(isset($term['assessments']['external']['docs'])) 
                                    <?php $docs = $term['assessments']['external']['docs']; ?>
                                    @if($docs && count($docs->files)>0)
                                        <ul class="list-unstyled list-inline">
                                        @foreach($docs->files as $file) 
                                        <li><a href="{{$file['link']}}" target="_blank"><i class="fa fa-paperclip fa-fw"></i> {{substr($file['name'], 10, 10)}}</a></li>
                                        @endforeach 
                                        </ul>
                                    @endif 
                                @endif 
                                
                                
                                
                                @else 
                                <label class="btn btn-xs btn-default btn-block"class="btn btn-xs btn-default btn-block">not assessed</label>
                                @endif
                                
                                
                                
                                
                            </td>
                            <td class="text-center">
                                
                                @if(isset($term['assessments']['internal']))
                                
                                <?php $internal_level = $term['assessments']['internal']['level']; ?>
                                
                                @if($internal_level == 'below') 
                                <label class="btn btn-xs btn-danger btn-block">{{$internal_level}}</label>
                                @elseif($internal_level == 'inline') 
                                <label class="btn btn-xs btn-warning btn-block">{{$internal_level}}</label>
                                @elseif($internal_level == 'above') 
                                <label class="btn btn-xs btn-success btn-block">{{$internal_level}}</label>
                                @else 
                                <label class="btn btn-xs btn-default btn-block">not assessed</label>
                                @endif 
                                
                                @if($term['assessments']['internal']['note'] != "")<small>{{$term['assessments']['internal']['note']}}</small>@endif

                                @if(isset($term['assessments']['internal']['docs'])) 
                                    <?php $docs = $term['assessments']['internal']['docs']; ?>
                                    @if($docs && count($docs->files)>0)
                                        <ul class="list-unstyled list-inline">
                                        @foreach($docs->files as $file) 
                                        <li><a href="{{$file['link']}}" target="_blank"><i class="fa fa-paperclip fa-fw"></i> {{substr($file['name'], 10, 10)}}</a></li>
                                        @endforeach 
                                        </ul>
                                    @endif 
                                @endif 

                                @else 
                                <label class="btn btn-xs btn-default btn-block">not assessed</label>
                                @endif
                                
                            </td>
                        </tr>
                        @endforeach 
                        @endforeach 
                       
                    </tbody>
                </table>  
                @else 
                <div class="alert alert-warning">No assessment found!</div>
                @endif 
                
               
                
              
                <h5><strong>Notes</strong></h5>
                @if($data['notes']->count()>0) 
                <ul>
                    @foreach($data['notes'] as $note) 
                    <li>
                            {{$note->note}}
                                <small class="text-primary">
                                    Created By {{ username($note->created_by)}} At {{date('d M Y g:i a', strtotime($note->created_at))}}
                                    @if($note->updated_by)
                                    and Updated by {{ username($note->updated_by)}} At {{date('d M Y g:i a', strtotime($note->updated_at))}}
                                    @endif 
                                </small>
                    </li>
                    @endforeach
                </ul>  
                @else 
                <div class="alert alert-warning">No note found!</div>
                @endif 
                
                @endif 

  </main>
</body>
</html>



