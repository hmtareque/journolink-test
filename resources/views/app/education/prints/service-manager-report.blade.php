<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <link href="{{asset('css/app-requisite.css')}}" rel="stylesheet">
        
  <style>
    @page { margin: 100px 25px; }
    header { position: fixed; top: -60px; left: 0px; right: 0px; background-color: lightblue; height: 50px; }
    footer { position: fixed; bottom: -60px; left: 0px; right: 0px; background-color: lightblue; height: 50px; }
    p { page-break-after: always; }
    p:last-child { page-break-after: never; } 

    body {
        background: #fff;
    }
    
  </style>
</head>
<body>
  <header>header</header>
  <footer>footer</footer>
  <main>
      
        

                @if(isset($data))
                <h4 class="section-title">From <span class="text-primary">{{date('d M Y', strtotime(php_date($request['from'])))}}</span> To <span class="text-primary">{{date('d M Y', strtotime(php_date($request['to'])))}}</span></h4>

                <h5><strong>Notes</strong></h5>
                @if($data['notes']->count()>0) 
                <ul>
                    @foreach($data['notes'] as $note) 
                    <li>
                            {{$note->note}}
                                <small class="text-primary">
                                    Created By {{ username($note->created_by)}} At {{date('d M Y g:i a', strtotime($note->created_at))}}
                                    @if($note->updated_by)
                                    and Updated by {{ username($note->updated_by)}} At {{date('d M Y g:i a', strtotime($note->updated_at))}}
                                    @endif 
                                </small>
                    </li>
                    @endforeach
                </ul>  
                @else 
                <div class="alert alert-warning">No note found!</div>
                @endif 
                
                
                <h5><strong>Schools</strong></h5>
                @if($data['schools']->count()>0) 
                <table class="table table-bordered table-condensed">
                    <thead>
                        <tr>
                            <th rowspan="2" width="40%">School</th>
                            <th rowspan="2" width="20%" class="text-center">Total <br/>Enrolled Students</th>
                            <th colspan="2" class="text-center">{{date('d M Y', strtotime(php_date($request['from'])))}} to {{date('d M Y', strtotime(php_date($request['to'])))}}</th>
                        </tr>
                        <tr>
                            <th width="20%" class="text-center">Enrolled</th>
                            <th width="20%" class="text-center">Attended</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data['schools'] as $school) 
                    <tr>
                        <td>{{$school->name}}</td>
                        <td class="text-center">{{$school->total_enrolled}}</td>
                        <td class="text-center">{{$school->enrolled}}</td>
                        <td class="text-center">{{$school->attended}}</td>
                    </tr>
                    @endforeach
                    </tbody>
                    
                    
                </table>
                @else 
                <div class="alert alert-warning">No data found!</div>
                @endif 
                
                <h5><strong>Classes</strong></h5>
                @if($data['classes']->count()>0) 
                <table class="table table-bordered table-condensed table-hover">
                    <thead>
                        <tr>
                            <th rowspan="2" width="15%">Class</th>
                            <th rowspan="2" width="15%">Assigned <br/>Teachers</th>
                            <th rowspan="2" width="10%" class="text-center">Enrolled<br/>Students</th>
                            <th colspan="4" width="60%" class="text-center">{{date('d M Y', strtotime(php_date($request['from'])))}} to {{date('d M Y', strtotime(php_date($request['to'])))}}</th>
                        </tr>
                        <tr>
                            <th width="10%" class="text-center">Enrolled</th>
                            <th width="20%" class="text-center">Date</th>
                            <th width="10%" class="text-center">Attended</th>
                            <th width="20%">Teachers</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        @foreach($data['classes'] as $class) 
                        
                       
                    
                        
                        
                           @if($class->attendances->count()>0)
                           @foreach($class->attendances as $attendance) 
                          <tr>
                              <td class="text-middle text-center">{{$class->course}} {{$class->group}}</td>
                        <td class="text-middle">
                            @if($class->teachers->count()>0)
                            <ul class="list-unstyled">
                            @foreach($class->teachers as $teacher) 
                            <li>
                                <small>{{$teacher->first_name}} {{$teacher->middle_name}} {{$teacher->last_name}}</small>
                                @if($teacher->status != 'teaching') <label class="label label-warning">{{$teacher->status}}</label>@endif
                            </li>
                            @endforeach 
                            </ul>
                            @else 
                            @endif 
                        </td>
                        <td class="text-middle text-center">{{$class->enrolled_students}}</td>
                        <td class="text-middle text-center">{{$class->enrolled}}</td>
                        
                                <td width="20%" class="text-center">{{date('d M Y', strtotime($attendance->date))}}</td>
                                <td width="20%" class="text-center">{{$attendance->attended}}</td>
                                <td width="40%">
                                    @if($attendance->teachers->count()>0)
                                    <ul class="list-unstyled">
                                    @foreach($attendance->teachers as $attended_teacher) 
                                    <li> <small>{{$attended_teacher->first_name}} {{$attended_teacher->middle_name}} {{$attended_teacher->last_name}}</small></li>
                                
                                    @endforeach 
                                    </ul>
                                    @endif 
                                    
                                </td>
                            </tr>
                            @endforeach 
                            
                           
                            @endif
                       
                        
                  
                    @endforeach
                    </tbody>
                </table>
                @else 
                <div class="alert alert-warning">No data found!</div>
                @endif
                
                
                <h5><strong>Subjects</strong></h5>
                @if($data['courses']->count()>0) 
                <table class="table table-bordered table-condensed table-hover">
                    <thead>
                        <tr>
                            <th rowspan="2" width="40%">Subject</th>
                            <th rowspan="2" width="20%" class="text-center">Total <br/>Enrolled Students</th>
                            <th colspan="2" width="40%" class="text-center">{{date('d M Y', strtotime(php_date($request['from'])))}} to {{date('d M Y', strtotime(php_date($request['to'])))}}</th>
                        </tr>
                        <tr>
                            <th width="20%" class="text-center">Enrolled</th>
                            <th width="20%" class="text-center">Attended</th>
                        </tr>
                    </thead>
                    <tbody>
                        @foreach($data['courses'] as $course) 
                    <tr>
                        <td>{{$course->title}}</td>
                        <td class="text-center">{{$course->total_enrolled}}</td>
                        <td class="text-center">{{$course->enrolled}}</td>
                        <td class="text-center">{{$course->attended}}</td>
                    </tr>
                    @endforeach
                    </tbody>
                    
                    
                </table>
                @else 
                <div class="alert alert-warning">No data found!</div>
                @endif 

                <h5><strong>Fees</strong></h5>
                
                <table class="table table-bordered table-condensed table-hover">
                   
                    <tr>
                        <td>Total Fees Collected from {{date('d M Y', strtotime(php_date($request['from'])))}} to {{date('d M Y', strtotime(php_date($request['to'])))}}</td>
                        <td class="text-right">&pound;400.00</td>
                    </tr>
                    
                    <tr>
                        <td>Fees Pending</td>
                        <td class="text-right">&pound;320.00</td>
                    </tr>
                    
                </table>
                
                @endif

  </main>
</body>
</html>



