<div class="header">
    <div class="title"><i class="fa fa-lg fa-edit fa-fw"></i> Edit Academic Year</div>
    <div class="links">
        <a href="#education/admin/academic-years/{{$year->id}}" class="btn btn-info btn-sm"><i class="fa fa-info-circle fa-fw"></i> Academic Year {{$year->year}}</a>
        <a href="#education/admin/academic-years" class="btn btn-success btn-sm"><i class="fa fa-list-alt fa-fw"></i> Academic Years</a>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-8">

        <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->

        <form class="form-horizontal ajax-form" action="{{url('manage/education/admin/academic-years/'.$year->id)}}" method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <input type="hidden" name="_method" value="put"/>

            <div class="form-group" id="year">
                <label class="col-sm-3 control-label">Academic Year</label>
                <div class="col-sm-9">
                    <input type="text" name="year" class="form-control" value="{{$year->year}}"/>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <div id="exist">
                        <span class="help-block hide"></span>
                    </div>

                    <button type="submit" class="btn btn-primary"> 
                        <i class="fa fa-save fa-fw"></i> Update
                    </button>
                </div>
            </div>
        </form>

    </div>
    <div class="col-xs-12 col-md-4">
    </div>
</div>