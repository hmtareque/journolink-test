<div class="header">
    <div class="title"><i class="fa fa-lg fa-calendar fa-fw"></i> Academic Year {{$year->year}} </div> 


    <div class="links">
        
        @if($year->complete == 1)
        <sup class="label label-success"><i class="fa fa-check fa-fw"></i> completed</sup>
            @can("update", 9)
            <button type="button" data-toggle="modal" data-target="#reopen-academic-year" class="btn btn-info btn-sm"><i class="fa fa-history fa-fw"></i>Reopen</button>
            @endcan 
        @else 

        @can("update", 9)
        <a href="#education/admin/academic-years/{{$year->id}}/edit" class="btn btn-info btn-sm"><i class="fa fa-edit fa-fw"></i> Update</a>
        @endcan 
        
        @can("create", 9)
        <a href="#education/admin/academic-years/{{$year->id}}/terms/create" class="btn btn-success btn-sm"><i class="fa fa-plus fa-fw"></i> Create New Term</a>
        @endcan
        
        @can("update", 9)
            @if(($year->terms->count() == $year->terms_completed) && ($year->no_of_classes == $year->class_completed) && $year->complete == 0 && $year->terms->count() != 0) 
            <button type="button" data-toggle="modal" data-target="#complete-academic-year" class="btn btn-primary btn-sm"><i class="fa fa-check fa-fw"></i>Mark as Complete</button>
            @elseif($year->terms->count() == 0 && $year->no_of_classes == 0) 
                @can("delete", 9)
                <button type="button" data-toggle="modal" data-target="#remove-academic-year" class="btn btn-danger btn-sm"><i class="fa fa-trash fa-fw"></i>Remove</button>
                @endcan 
            @endif 
        @endcan 

        @endif
        
        <a href="#education/admin/academic-years" class="btn btn-primary btn-sm"><i class="fa fa-list-alt fa-fw"></i> Academic Years</a>
    </div>

</div>

<div class="row">
    <div class="col-md-9 col-xs-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Academic Year</h3>
            </div>
            <div class="panel-body">

                <!-- Notify Area -->
                @include('app.shared.flash') 
                <!-- Notfiy Area End -->

                <ul class="list-unstyled">
                    <li><strong>Academic Year</strong> : {{$year->year}}</li>
                </ul>
                <div class="row ">
                    <div class="col-xs-12">
                        <h4 class="section-title">Terms</h4>
                        @if($year->terms->count()>0)
                        <form class="ajax-form" action="{{url('manage/education/admin/academic-years/'.$year->id.'/terms/save-order')}}" method="post">
                            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                            <ul class="list-unstyled list-group" id="sortable">
                                @foreach($year->terms as $term)
                                <li class="list-group-item">
                                    <input type="hidden" name="terms[]" value="{{$term->id}}"/>
                                    <a href="#education/admin/academic-years/{{$year->id}}/terms/{{$term->id}}"><strong>{{$term->term}}</strong></a> <small>{{$term->dates}}</small>
                                    @if($term->complete == 1) 
                                    <span class="label label-success pull-right"><i class="fa fa-check fa-fw"></i> completed</span>
                                    @else 
                                    <span class="label label-default pull-right"><i class="fa fa-spin fa-cog fa-fw"></i> in progress ...</span>
                                    @endif 
                                </li>
                                @endforeach 
                            </ul>
                            @can("update", 9)
                                @if($year->terms->count()>1)
                                <button type='submit' class="btn btn-primary btn-xs"><i class="fa fa-list-ol fa-fw"></i> Save Terms Order</button>
                                @endif
                            @endcan 
                        </form>
                        @else 
                        <div class="alert alert-warning">No term found for this year!</div>
                        @endif 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-3">
        @include('app.shared.feed')
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="complete-academic-year" tabindex="-1" role="dialog" aria-labelledby="completeModal">
    <div class="modal-dialog" role="document">
        <form class="ajax-form" action="{{url('manage/education/admin/academic-years/'.$year->id.'/mark-as-complete')}}" method="post" data-hash="education/admin/academic-years/{{$year->id}}">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="completeModal">Mark as Complete</h4>
                </div>
                <div class="modal-body"><p>All the <strong>Classes</strong> and <strong>Terms</strong> of the academic year will be completed as well.</p>
                    <p>Are you sure to mark the <strong>Academic Year {{$year->year}}</strong> as complete?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-sm">Yes</button>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="reopen-academic-year" tabindex="-1" role="dialog" aria-labelledby="reopenModal">
    <div class="modal-dialog" role="document">
        <form class="ajax-form" action="{{url('manage/education/admin/academic-years/'.$year->id.'/reopen')}}" method="post" data-hash="manage/education/admin/academic-years/{{$year->id}}">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="reopenModal">Reopen</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure to reopen the <strong>Academic Year {{$year->year}}</strong>?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-sm">Yes</button>
                </div>
            </div>
        </form>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="remove-academic-year" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <form class="ajax-form" action="{{url('manage/education/admin/academic-years/'.$year->id)}}" method="post" data-hash="manage/education/admin/academic-years">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <input type="hidden" name="_method" value="delete"/>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Remove</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure to remove the <strong>Academic Year {{$year->year}}</strong>?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger btn-sm">Yes</button>
                </div>
            </div>
        </form>
    </div>
</div>



<script type="text/javascript">
    //sorting lists 
    $(function () {
        $("#sortable").sortable();
        $("#sortable").disableSelection();
    });
</script>


