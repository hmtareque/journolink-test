<div class="header">
    <div class="title"><i class="fa fa-lg fa-calendar fa-fw"></i> Academic Years</div>
    <div class="links">
        @can("create", 9)
        <a href="#education/admin/academic-years/create" class="btn btn-success btn-sm"><i class="fa fa-plus fa-fw"></i> Create New Academic Year</a>
        @endcan 
    </div>
</div>

<div class="row">
    <div class="col-md-9 col-xs-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Academic Years</h3>
            </div>
            <div class="panel-body">

                <!-- Notify Area -->
                @include('app.shared.flash') 
                <!-- Notfiy Area End -->

                @if($years->count()>0)
                <ul class="list-unstyled list-group">
                    @foreach($years as $year)
                    <li class="list-group-item">
                        <a href="#education/admin/academic-years/{{$year->id}}"><strong>{{$year->year}}</strong></a>
                        @if($year->complete == 1) 
                        <span class="label label-success pull-right"><i class="fa fa-check fa-fw"></i> completed</span>
                        @else 
                        <span class="label label-default pull-right"><i class="fa fa-spin fa-cog fa-fw"></i> in progress ...</span>
                        @endif 
                    </li>
                    @endforeach 
                </ul>
                @else 
                @endif 
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-3">
        @include('app.shared.feed')
    </div>
</div>
