<div class="header">
    <div class="title"><i class="fa fa-lg fa-book fa-fw"></i> Courses</div>
    <div class="links">
        @can("create", 11)
        <a href="#education/admin/courses/create" class="btn btn-success btn-sm"><i class="fa fa-plus fa-fw"></i> Create New Course</a>
        @endcan 
        <a href="#education/admin/courses/reorder" class="btn btn-info btn-sm"><i class="fa fa-list-ol fa-fw"></i> Reorder Courses</a>
    </div>
</div>

<div class="row">
    <div class="col-md-9 col-xs-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Groups</h3>
            </div>
            <div class="panel-body">

                <!-- Notify Area -->
                @include('app.shared.flash') 
                <!-- Notfiy Area End -->

                @if($courses->count()>0)

                    <ul class="list-unstyled list-course">
                        @foreach($courses as $course)

                        <li class="list-group-item">
                            <div class="row">
                                <div class="col-xs-12 col-md-9">
                                    <strong>{{$course->title}}</strong>
                            <p><small class="text-muted">{{$course->description}}</small></p>
                                </div>
                                <div class="col-xs-12 col-md-3">
                                    @if($course->active == 1) 
                            <button type="button" data-toggle="modal" data-target="#make-inactive-{{$course->id}}" class="btn btn-success btn-xs pull-right"><i class="fa fa-arrow-down fa-fw"></i>deactivate</button>

                            <!-- Modal -->
                            <div class="modal fade" id="make-inactive-{{$course->id}}" tabindex="-1" role="dialog" aria-labelledby="MakeInactiveModal">
                                <div class="modal-dialog" role="document">
                                    <form class="ajax-form" action="{{url('manage/education/admin/courses/'.$course->id.'/make-inactive')}}" method="post">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="MakeInactiveModal">Make Inactive</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>Are you sure to deactivate the <strong>{{$course->course}}</strong> course?</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary btn-sm">Yes</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>

                            @else 
                            <button type="button" data-toggle="modal" data-target="#reactivate-{{$course->id}}" class="btn btn-warning btn-xs pull-right"><i class="fa fa-arrow-up fa-fw"></i>activate</button>

                            <!-- Modal -->
                            <div class="modal fade" id="reactivate-{{$course->id}}" tabindex="-1" role="dialog" aria-labelledby="ReactivateModal">
                                <div class="modal-dialog" role="document">
                                    <form class="ajax-form" action="{{url('manage/education/admin/courses/'.$course->id.'/reactivate')}}" method="post">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="ReactivateModal">Reactive</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>Are you sure to reactivate the <strong>{{$course->course}}</strong> course?</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-primary btn-sm">Yes</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                            @endif 
                            
                            <a href="#education/admin/courses/{{$course->id}}/edit" class="btn btn-primary btn-xs pull-right margin-right-5"><i class="fa fa-edit fa-fw"></i> edit</a>
                            
                                </div>
                            </div>
                            
                            
                        </li>
                        @endforeach 
                    </ul>
                @else 
                
                <div class="alert alert-warning">No student course found!</div>
                
                @endif 
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-3">
        @include('app.shared.feed')
    </div>
</div>






