<div class="header">
    <div class="title"><i class="fa fa-lg fa-users fa-fw"></i> Student Groups</div>
    <div class="links">
        @can("create", 11)
        <a href="#education/admin/student-groups/create" class="btn btn-success btn-sm"><i class="fa fa-plus fa-fw"></i> Create New Group</a>
        @endcan 
        <a href="#education/admin/student-groups" class="btn btn-info btn-sm"><i class="fa fa-info-circle fa-fw"></i> Student Groups</a>
    </div>
</div>

<div class="row">
    <div class="col-md-9 col-xs-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Groups</h3>
            </div>
            <div class="panel-body">

                <!-- Notify Area -->
                @include('app.shared.flash') 
                <!-- Notfiy Area End -->

                @if($groups->count()>0)
                <form class="ajax-form" action="{{url('manage/education/admin/student-groups/save-order')}}" method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    <ul class="list-unstyled list-group" id="sortable">
                        @foreach($groups as $group)
                        <li class="list-group-item">
                            <input type="hidden" name="groups[]" value="{{$group->id}}"/>
                            <strong>{{$group->group}}</strong>
                            @if($group->active == 1) 
                            <label class="label label-success pull-right"><i class="fa fa-arrow-down fa-fw"></i>active</label>
                            @else 
                            <label class="label label-warning pull-right"><i class="fa fa-arrow-down fa-fw"></i>not active</label>
                            @endif 
                        </li>
                        @endforeach
                    </ul> 
                    @can("update", 11)
                    <button type='submit' class="btn btn-primary btn-xs"><i class="fa fa-list-ol fa-fw"></i> Save Order</button>  
                    @endcan 
                </form>
                @else 

                <div class="alert alert-warning">No student group found!</div>

                @endif 
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-3">
        @include('app.shared.feed')
    </div>
</div>


<script type="text/javascript">
    //sorting lists 
    $(function () {
        $("#sortable").sortable();
        $("#sortable").disableSelection();
    });
</script>








