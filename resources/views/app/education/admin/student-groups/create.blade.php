<div class="header">
    <div class="title"><i class="fa fa-lg fa-plus-square-o fa-fw"></i> Create New Student Group</div>
    <div class="links">
        <a href="#education/admin/student-groups" class="btn btn-success btn-sm"><i class="fa fa-list-alt fa-fw"></i> Student Groups</a>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-8">
        <form class="form-horizontal ajax-form" action="{{url('manage/education/admin/student-groups')}}" method="post" data-hash="education/admin/student-groups">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <div class="form-group" id="group">
                <label class="col-sm-3 control-label">Group</label>
                <div class="col-sm-9">
                    <input type="text" name="group" class="form-control"/>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <div id="exist">
                        <span class="help-block hide"></span>
                    </div>
                    <button type="submit" class="btn btn-primary"> 
                        <i class="fa fa-save fa-fw"></i> Save
                    </button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-xs-12 col-md-4">
    </div>
</div>