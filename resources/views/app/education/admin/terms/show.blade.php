<div class="header">
    <div class="title"><i class="fa fa-lg fa-calendar fa-fw"></i> {{$term->term}} Term</div> 

    <div class="links">
        @if($term->complete == 1)
        <span class="label label-success"><i class="fa fa-check fa-fw"></i> completed</span>
            @can("update", 11)
            <button type="button" data-toggle="modal" data-target="#reopen-academic-year" class="btn btn-info btn-sm"><i class="fa fa-history fa-fw"></i>Reopen</button>
            @endcan 
        @else 

        @can("update", 11)
        <a href="#education/admin/academic-years/{{$year->id}}/terms/{{$term->id}}/edit" class="btn btn-primary btn-sm"><i class="fa fa-edit fa-fw"></i> Update</a>
        @endcan 
    
        @if(($term->assessments->count() == $term->assessment_completed) && $term->complete == 0 && $term->assessments->count() != 0) 
            @can("update", 11)
            <button type="button" data-toggle="modal" data-target="#complete-term" class="btn btn-primary btn-sm"><i class="fa fa-check fa-fw"></i>Mark as Complete</button>
            @endcan 
        @elseif($term->assessments->count() == 0) 
            @can("delete", 11)
            <button type="button" data-toggle="modal" data-target="#remove-term" class="btn btn-danger btn-sm"><i class="fa fa-trash fa-fw"></i>Remove</button>
            @endcan 
        @endif 

        @endif

       <a href="#education/admin/academic-years/{{$year->id}}" class="btn btn-info btn-sm"><i class="fa fa-info-circle fa-fw"></i> Academic Year {{$year->year}}</a>
    </div>

</div>

<div class="row">
    <div class="col-md-9 col-xs-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Term</h3>
            </div>
            <div class="panel-body">

                <!-- Notify Area -->
                @include('app.shared.flash') 
                <!-- Notfiy Area End -->

                <ul class="list-unstyled">
                    <li><strong>Academic Year</strong> : {{$year->year}}</li>
                    <li><strong>Term</strong> : {{$term->term}}</li>
                    <li><strong>Dates</strong> : @if($term->dates != "") {{$term->dates}} @else -- @endif</li>
                </ul>
                <div class="row ">
                    <div class="col-xs-12">
                        <h4 class="section-title">Assessments</h4>
                        @if($term->assessments->count()>0)
                            <ul class="list-unstyled list-group">
                                @foreach($term->assessments as $assessment)
                                <li class="list-group-item">
                                    <a href="#education/classes/{{$assessment->class_id}}/assessments/{{$assessment->id}}">{{$assessment->course}} - {{$assessment->group}} </a>
                                    <small>{{$assessment->school}}</small>
                                    @if($assessment->complete == 1) 
                                    <span class="label label-success pull-right"><i class="fa fa-check fa-fw"></i> completed</span>
                                    @else 
                                    <span class="label label-default pull-right"><i class="fa fa-spin fa-cog fa-fw"></i> in progress ...</span>
                                    @endif 
                                </li>
                                @endforeach 
                            </ul>
                        @else 
                        <div class="alert alert-warning">No assessment found for this term!</div>
                        @endif 
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-xs-12 col-md-3">
        @include('app.shared.feed')
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="complete-academic-year" tabindex="-1" role="dialog" aria-labelledby="completeModal">
    <div class="modal-dialog" role="document">
        <form class="ajax-form" action="{{url('manage/education/admin/academic-years/'.$year->id.'/terms/'.$term->id.'/mark-as-complete')}}" method="post" data-hash="education/admin/academic-years/{{$year->id}}/terms/{{$term->id}}">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="completeModal">Mark as Complete</h4>
                </div>
                <div class="modal-body"><p>All the <strong>Assessment</strong> of the {{$term->term}} term will be completed as well.</p>
                    <p>Are you sure to mark the <strong>{{$term->term}}</strong> of Academic Year {{$year->year}} as complete?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-sm">Yes</button>
                </div>
            </div>
        </form>
    </div>
</div>

<!-- Modal -->
<div class="modal fade" id="reopen-term" tabindex="-1" role="dialog" aria-labelledby="reopenModal">
    <div class="modal-dialog" role="document">
        <form class="ajax-form" action="{{url('manage/education/admin/academic-years/'.$year->id.'/terms/'.$term->id.'/reopen')}}" method="post" data-hash="education/admin/academic-years/{{$year->id}}/terms/{{$term->id}}">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="reopenModal">Reopen</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure to reopen the <strong>{{$term->term}}</strong> term of Academic Year {{$year->year}}?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-primary btn-sm">Yes</button>
                </div>
            </div>
        </form>
    </div>
</div>


<!-- Modal -->
<div class="modal fade" id="remove-term" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
    <div class="modal-dialog" role="document">
        <form class="ajax-form" action="{{url('manage/education/admin/academic-years/'.$year->id.'/terms/'.$term->id)}}" method="post" data-hash="education/admin/academic-years/{{$year->id}}">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <input type="hidden" name="_method" value="delete"/>
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title" id="myModalLabel">Remove</h4>
                </div>
                <div class="modal-body">
                    <p>Are you sure to remove the <strong>{{$term->term}}</strong> term of Academic Year {{$year->year}}?</p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                    <button type="submit" class="btn btn-danger btn-sm">Remove</button>
                </div>
            </div>
        </form>
    </div>
</div>