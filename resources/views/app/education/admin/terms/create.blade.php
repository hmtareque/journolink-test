<div class="header">
    <div class="title"><i class="fa fa-lg fa-plus-square-o fa-fw"></i> Create New Term</div>
    <div class="links">
        <a href="#education/admin/academic-years/{{$year->id}}" class="btn btn-info btn-sm"><i class="fa fa-info-circle fa-fw"></i> Academic Year {{$year->year}}</a>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-8">
        <form class="form-horizontal ajax-form" action="{{url('manage/education/admin/academic-years/'.$year->id.'/terms')}}" method="post" data-hash="education/classes">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>

            <div class="form-group" id="year">
                <label class="col-sm-3 control-label">Academic Year</label>
                <div class="col-sm-9 ">
                    <label class="control-label">{{$year->year}}</label>
                </div>
            </div>

            <div class="form-group" id="term">
                <label class="col-sm-3 control-label">Term</label>
                <div class="col-sm-9">
                    <input type="text" name="term" class="form-control"/>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="dates">
                <label class="col-sm-3 control-label">Dates</label>
                <div class="col-sm-9">
                    <input type="text" name="dates" class="form-control"/>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <div id="exist">
                        <span class="help-block hide"></span>
                    </div>

                    <button type="submit" class="btn btn-primary"> 
                        <i class="fa fa-save fa-fw"></i> Save
                    </button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-xs-12 col-md-4">
    </div>
</div>