<div class="header">
    <div class="title"><i class="fa fa-lg fa-calendar fa-fw"></i> Academic Years</div>
    <div class="links">
        <a href="" class="btn btn-success btn-sm" data-toggle="modal" data-target="#create-academic-year-modal"><i class="fa fa-plus"></i> Create New Class</a>
        
    </div>
</div>


<div class="row">
    <div class="col-md-8 col-xs-12">
        
        @if($years->count()>0)
        <ul class="list-group">
        @foreach($years as $year)
         <li class="list-group-item">
      <strong>{{$year->year}}</strong>
      
      
      
      <div class="pull-right">
  <button type="button" data-toggle="modal" data-target="#edit-{{$year->id}}-academic-year-modal" class="btn btn-primary btn-xs">edit</button>
  <button type="button" data-toggle="modal" data-target="#complete-{{$year->id}}-academic-year-modal" class="btn btn-success btn-xs">complete</button>
<button type="button" data-toggle="modal" data-target="#delete-{{$year->id}}-academic-year-modal" class="btn btn-danger btn-xs">delete</button>
  </div>
      
      
      <!-- Modal -->
<div class="modal fade" id="edit-{{$year->id}}-academic-year-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
      
      
      <!-- Modal -->
<div class="modal fade" id="complete-{{$year->id}}-academic-year-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
      
      
        <!-- Modal -->
<div class="modal fade" id="delete-{{$year->id}}-academic-year-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>
      
  </li>
        @endforeach 
        </ul>
        @else 
        
        @endif 
        
        
 

    </div>
    <div class="col-md-4 col-xs-12">
        @include('app.shared.feed')
    </div>
</div>



<!-- Modal -->
<div class="modal fade" id="create-academic-year-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title" id="myModalLabel">Modal title</h4>
      </div>
      <div class="modal-body">
        ...
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary">Save changes</button>
      </div>
    </div>
  </div>
</div>









