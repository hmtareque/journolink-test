<div class="header">
    <div class="title"><i class="fa fa-lg fa-cog fa-fw"></i> Administration</div>
</div>



<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-9">

<div class="row">
    <div class="col-xs-12 col-md-3">
        <div class="panel panel-default">
            <div class="panel-body text-center">
                <a href="#education/admin/academic-years"><i class="fa fa-calendar fa-3x"></i></a>
                <div class="margin-top-10"><small class="text-muted">Academic Years</small></div>
            </div>
        </div>
    </div>
    
    <div class="col-xs-12 col-md-3">
        <div class="panel panel-default">
            <div class="panel-body text-center">
                <a href="#education/admin/student-groups"><i class="fa fa-users fa-3x"></i></a>
                <div class="margin-top-10"><small class="text-muted">Student Groups</small></div>
            </div>
        </div>
    </div>
    
    
    <div class="col-xs-12 col-md-3">
        <div class="panel panel-default">
            <div class="panel-body text-center">
                <a href="#education/admin/courses"><i class="fa fa-book fa-3x"></i></a>
                <div class="margin-top-10"><small class="text-muted">Courses</small></div>
            </div>
        </div>
    </div>
    
   
</div>
        
          </div>
    <div class="col-md-3 col-xs-12">
        @include('app.shared.feed')
    </div>
</div>


