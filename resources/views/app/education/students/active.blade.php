<div class="header">
    <div class="title"><i class="fa fa-lg fa-user-circle-o fa-fw"></i> Students</div>
    <div class="links">
        <a href="#education/students" class="btn btn-primary btn-sm"><i class="fa fa-list-alt fa-fw"></i>All Students</a>
        <a href="#education/students/create" class="btn btn-success btn-sm"><i class="fa fa-plus fa-fw"></i>Create New Student</a>
        
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-9">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Active Students</h3>
            </div>
            <div class="panel-body">
                @if($students->count()>0) 
                <div class="table-responsive">
                    <table class="table data-table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Ref. No.</th>
                                <th class="text-center">Enrolled in Class</th>
                                <th>Contact</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($students as $student) 
                            <tr>
                                <td><a href="#education/students/{{$student->id}}">{{$student->first_name}} {{$student->middle_name}}  <strong>{{$student->last_name}} </strong></a></td>
                                <td>{{$student->ref_no}}</td>
                                <td class="text-center">{{$student->no_of_class}}</td>
                                <td>
                                    @if($student->mobile != "") 
                                    {{$student->mobile}}
                                    @elseif($student->phone != "") 
                                    {{$student->phone}}
                                    @else 
                                    <a href="mailto:{{$student->email}}">{{$student->email}}</a>
                                    @endif 
                                </td>
                            </tr>
                            @endforeach 
                        </tbody>
                    </table>
                </div>
                @else 
                <div class="alert alert-warning">No active student found! Please create student.</div>
                @endif
            </div>
        </div>
    </div>

    <div class="col-xs-12 col-md-3">
        @include('app.shared.feed')
    </div> 

</div>

