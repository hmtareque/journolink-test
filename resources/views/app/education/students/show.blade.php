<div class="header">
    <div class="title"><i class="fa fa-lg fa-user-circle-o fa-fw"></i> Student</div>
    <div class="links">
        @can("update", 6)
        <a href="#education/students/{{$student->id}}/edit" class="btn btn-primary btn-sm"><i class="fa fa-edit fa-fw"></i> Update Student</a>
        @endcan 
        
        <a href="#education/active-students" class="btn btn-success btn-sm"><i class="fa fa-user-o fa-fw"></i> Active Students</a>

    </div>
</div>


<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-8">

        <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->

        <div class="row">
            <div class="col-xs-12">
                
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">{{$student->first_name}} {{$student->middle_name}} {{$student->last_name}} Details</h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-xs-12 col-md-4">
                            <div class="panel panel-success">
                                <div class="panel-body">
                                    <img class="img-responsive img-rounded" @if($student->profile_picture) src="{{$student->profile_picture}}" @else src="{{asset('img/avatar.png')}}" @endif/>
                                </div>
                                <div class="panel-footer">
                                    <div id="upload-response"></div>
                                    <form class="form-inline ajax-upload-form"  method="POST" action="{{url('manage/docs/picture')}}" enctype="multipart/form-data">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                        <input type="hidden" name="object_type" value="client_profile_picture"/>
                                        <input type="hidden" name="object_id" value="{{$student->id}}"/>
                                        <input type="hidden" name="folder" value="clients/{{$student->id}}/picture"/>
                                        <input type="hidden" name="name" value="Profile Picture"/>
                                        <input type="hidden" name="return" value="manage/education/students/{{$student->id}}"/>
                                        <div class="file-upload btn btn-xs btn-success">
                                            <span id="upload-status"><i class="fa fa-refresh fa-fw"></i>Update Picture</span>
                                            <input type="file" name="picture" class="upload upload-picture"/>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>


                        <div class="col-xs-12 col-md-8">
                            <ul class="list-unstyled">
                                <li><strong>Name</strong> : {{$student->first_name}} {{$student->middle_name}}  <strong>{{$student->last_name}}</strong></li>
                                <li><strong>Date of Birth</strong> : {{date('d M Y', strtotime($student->date_of_birth))}}</li>
                                <li>
                                    <strong>Gender</strong> : 
                                    @if($student->gender == 'male') 
                                    <i class="fa fa-male fa-fw text-danger"></i>Male
                                    @elseif($student->gender == 'female') 
                                    <i class="fa fa-female fa-fw text-danger"></i>Female
                                    @elseif($student->gender == 'other') 
                                    <i class="fa fa-info-circle fa-fw text-danger"></i> {{$student->other_gender}}
                                    @endif 

                                </li>
                            </ul>

                            <div class="row">
                                <div class="col-xs-12 col-md-5">
                                    <ul class="list-unstyled">
                                        <li><strong>Address</strong></li>
                                        <li>{{$student->address_line_1}}</li>
                                        @if($student->address_line_2 != '')<li>{{$student->address_line_2}}</li>@endif
                                        <li>{{$student->street}}</li>
                                        <li>{{$student->city}}</li>
                                        <li><strong>{{$student->postcode}}</strong></li>
                                    </ul>
                                </div>
                                <div class="col-xs-12 col-md-7">
                                    <ul class="list-unstyled">
                                        <li><strong>Contact</strong></li>
                                        <li>Phone : @if($student->phone != '') {{$student->phone}} @else <label class="label label-warning">not found</label> @endif</li>
                                        <li>Mobile : @if($student->mobile != '') {{$student->mobile}} @else <label class="label label-warning">not found</label> @endif</li>
                                        <li>Email : @if($student->email != '') <a href="mailto:{{$student->email}}">{{$student->email}}</a> @else <label class="label label-warning">not found</label> @endif</li>
                                    </ul>
                                </div>
                            </div>

                            <ul class="list-unstyled">
                                <li><strong>Borough</strong> : @if($student->local_authority != '') {{$student->local_authority}} @else <label class="label label-warning">not found</label> @endif</li>
                            </ul>

                            <ul class="list-unstyled">
                                <li>
                                    <strong>First Language</strong> : 
                                    @if(trim($student->first_language) == '')
                                    <label class="label label-warning">not found</label>
                                    @elseif($student->first_language == 'other') 
                                    {{$student->other_first_language}} (other)
                                    @else
                                    {{languages($student->first_language)}}
                                    @endif 
                                </li>
                                <li><strong>Ethnicity</strong> : @if(trim($student->ethnicity) != '') {{ethnicities($student->ethnicity)}} @else <label class="label label-warning">not found</label> @endif</li>
                                <li>
                                    <strong>Religion</strong> : 
                                    @if(trim($student->religion) == '') 
                                    <label class="label label-warning">not found</label>
                                    @elseif($student->religion == 'other') 
                                    {{$student->other_religion}} (other)
                                    @else
                                    {{$student->religion}}
                                    @endif
                                </li>
                            </ul>

                            <ul class="list-unstyled">
                                <li>
                                    <strong>@if($student->has_consent_to_use_info == 1) <i class="fa fa-check text-success fa-fw"></i> @else <i class="fa fa-close text-danger fa-fw"></i> @endif</strong>
                                    Has consent to use information by {{config('settings.name')}}</li>
                                <li>
                                    <strong>@if($student->has_consent_to_contacted_for_activities == 1) <i class="fa fa-check text-success fa-fw"></i> @else <i class="fa fa-close text-danger fa-fw"></i> @endif</strong>
                                    Has consent to contacted by {{config('settings.name')}} for Activities</li>
                                <li>
                                    <strong>@if($student->has_consent_to_appear_in_publicity_photos == 1) <i class="fa fa-check text-success fa-fw"></i> @else <i class="fa fa-close text-danger fa-fw"></i> @endif</strong>
                                    Has consent to appear in publicity photos</li>
                                <li>
                                    <strong>@if($student->has_consent_to_appear_in_publicity_videos == 1) <i class="fa fa-check text-success fa-fw"></i> @else <i class="fa fa-close text-danger fa-fw"></i> @endif</strong>
                                    Has consent to appear in publicity videos
                                </li>
                                <li>
                                    <strong>@if($student->subscribe_newsletter == 1) <i class="fa fa-check text-success fa-fw"></i> @else <i class="fa fa-close text-danger fa-fw"></i> @endif</strong>
                                    Subscribe Newsletter
                                </li>
                                <li>
                                    <strong><i class="fa fa-info-circle text-info fa-fw"></i></strong>
                                    Heard about {{config('settings.name')}} by @if($student->heard_about_us_by == 'other')  {{$student->other_heard_about_us_by}} (other) @else {{$student->heard_about_us_by}} @endif
                                </li>

                            </ul>

                        </div>
                    </div>
                </div>
            </div>
        </div>


        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Current Enrolled Classes</h3>
            </div>
            <div class="panel-body">
                @if($student->classes->count()>0)
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>Class</th>
                                <th>School</th>
                                <th>Enrolled At</th>
                                <th>Status</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach($student->classes as $class) 
                            <tr>
                                <td><a href="#education/students/{{$student->id}}/classes/{{$class->id}}"><strong>{{$class->group}}-{{$class->course}}</strong></a> <small>{{$class->year}}</small></td>
                                <td>{{$class->school}}</td>
                                <td>{{date('d M Y', strtotime($class->date_of_enrolment))}}</td>
                                <td><label class="label label-default">{{$class->status}}</label></td>
                            </tr>
                            @endforeach 
                        </tbody>
                    </table>
                </div>
                @else
                <div class="alert alert-warning">{{$student->first_name}} {{$student->middle_name}} {{$student->last_name}} not enrolled in any class!</div>
                @endif 
            </div>
            <div class="panel-footer">
                <a class="btn btn-xs btn-success" href="#education/students/{{$student->id}}/classes/create"><i class="fa fa-plus fa-fw"></i> Enrol in New Class</a>
            </div>
        </div>
        
        
        <!-- Note Panel Start -->
        @include('app.shared.notes', ['module_id' => 8, 'notes' => $student->notes, 'object_type' => 'education_client', 'object_id' => $student->id, 'return' => 'manage/education/students/'.$student->id])
        <!-- Note Panel End -->
        
        

      
    </div>
    
    
    
    

    <div class="col-xs-12 col-sm-12 col-md-4">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Next of Kin</h3>
            </div>
            <div class="panel-body">
                <ul class="list-unstyled">
                    <li><strong>{{$student->next_of_kin}}</strong> ({{$student->next_of_kin_relationship}})</li>
                </ul>
                <ul class="list-unstyled">
                    <li><strong>Address</strong></li>
                    @if($student->next_of_kin_address_same_as_main == 1)
                    <li>Same as student address</li>
                    @else 
                    <li>{{$student->next_of_kin_address_line_1}}</li>
                    @if($student->next_of_kin_address_line_2 != '')<li>{{$student->next_of_kin_address_line_2}}</li>@endif
                    <li>{{$student->next_of_kin_street}}</li>
                    <li>{{$student->next_of_kin_city}}</li>
                    <li><strong>{{$student->next_of_kin_postcode}}</strong></li>
                    @endif 
                </ul>

                <ul class="list-unstyled">
                    <li><strong>Contact</strong></li>
                    <li>Phone : @if($student->next_of_kin_phone != '') {{$student->next_of_kin_phone}} @else <label class="label label-warning">not found</label> @endif</li>
                    <li>Mobile : @if($student->next_of_kin_mobile != '') {{$student->next_of_kin_mobile}} @else <label class="label label-warning">not found</label> @endif</li>
                    <li>Email : @if($student->next_of_kin_email != '') <a href="mailto:{{$student->next_of_kin_email}}">{{$student->next_of_kin_email}}</a> @else <label class="label label-warning">not found</label> @endif</li>
                </ul>
            </div>
        </div>

        
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Payment Summary</h3>
            </div>
            <div class="panel-body">
                
                <?php //print_r($service_account_summary); ?>
                
                @if($service_account_summary->recent_payments->count()>0)
                <div><strong>Recent Payments</strong></div>
                <table class="table table-bordered table-condensed">
                    <tr>
                        <th>Date</th>
                        <th>Description</th>
                        <th>Amount</th>
                    </tr>
                    @foreach($service_account_summary->recent_payments as $payment) 
                    <tr>
                        <td>{{date('d/m/Y', strtotime($payment->created_at))}}</td>
                        <td>{{substr($payment->description, 0, 20)}} ...</td>
                        <td class="text-right">&pound;{{$payment->amount}}</td>
                    </tr>
                    @endforeach 
                </table>
                @else 
                <div class="alert alert-warning">No payment found!</div>
                @endif 

                @if($service_account_summary->due > 0) <label class="label label-danger">Due: &pound;{{number_format($service_account_summary->due, 2, '.', '')}}</label>@endif
                @if($service_account_summary->exempt > 0) <label class="label label-success">Exempt: &pound;{{number_format($service_account_summary->due, 2, '.', '')}}</label>@endif
                

            </div>
           
            <div class="panel-footer">
                <a class="btn btn-xs btn-success" href="#clients/{{$student->id}}/services/education/fees"><i class="fa fa-database fa-fw"></i> Account</a>
           </div>
         
        </div>



        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Immigration Status</h3>
            </div>
            <div class="panel-body">
                <ul class="list-unstyled">
                    <li>
                        <strong>Status</strong> : 
                        @if($student->immigration_status == 'other')
                        {{$student->other_immigration_status}} (other)
                        @else 
                        {{$student->immigration_status}}
                        @endif 
                    </li>

                    @if($student->born_in_the_uk == 1) 
                    <li>Born in the UK</li>
                    @else 
                    <li>Arrived in the UK : {{date('d M Y', strtotime($student->arrived_in_the_uk))}}</li>
                    @endif 

                    @if($student->immigration_note != '')
                    <li><strong>Note</strong> : {{$student->immigration_note}}</li>
                    @endif 

                </ul>
                
               

               @include('app.shared.doc', ['module_id' => 6, 'name' => 'Immigration Documents', 'id' => 'immigration-doc', 'docs' => $student->immigration_docs, 'object_type' => 'client_immigration', 'object_id' => $student->id, 'folder' => 'clients/'.$student->id.'/immigration', 'return' => 'manage/education/students/'.$student->id])



            </div>
        </div>



        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Health</h3>
            </div>
            <div class="panel-body">

                <!-- hide for now
                <ul class="list-unstyled">
                    <li><strong>GP Name</strong> : @if($student->gp_name != '') {{$student->gp_name}} @else <label class="label label-warning">not found</label> @endif</li>
                    <li><strong>GP Contact</strong> : @if($student->gp_contact != '') {{$student->gp_contact}} @else <label class="label label-warning">not found</label> @endif</li>
                </ul>

                <hr/>
                -->
                

                <ul class="list-unstyled">
                    <li><strong>Medical Conditions</strong></li>
                    @if($student->has_medical_condition == 1) 
                    <li>{{$student->medical_conditions}}</li>
                    @else 
                    <li><label class="label label-success">Has no medical conditions</label></li>
                    @endif 
                </ul>
                
                
                
                @if($student->has_medical_condition == 1) 
                @include('app.shared.doc', ['module_id' => 6, 'name' => 'Medical Condition Documents', 'id' => 'medical-condition-doc', 'docs' => $student->medical_condition_docs, 'object_type' => 'client_medical_condition', 'object_id' => $student->id, 'folder' => 'clients/'.$student->id.'/medical-conditions', 'return' => 'manage/education/students/'.$student->id])
                @endif



                <hr/>

                <ul class="list-unstyled">
                    <li><strong>Allergies</strong></li>
                    @if($student->has_allergy == 1) 
                    <li>{{$student->allergies}}</li>
                    <li class="margin-top-5">
                        <strong>Allergies Documents</strong>
                        @if($student->allergy_docs) 
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#allergy-doc">
                            <i class="fa fa-trash fa-fw"></i>
                        </button>
                        <!-- Modal -->
                        <div class="modal fade" id="allergy-doc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <form class="ajax-form" action="{{url('manage/docs/destroy')}}" method="post">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                    <input type="hidden" name="doc" value="{{$student->allergy_docs->id}}"/>
                                    <input type="hidden" name="return" value="manage/education/students/{{$student->id}}"/>
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Delete Document</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>Are you sure to <strong class="text-danger">delete</strong> the <strong>Allergies Document</strong>?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        @endif 
                    </li>

                    @if($student->allergy_docs) 
                    @foreach($student->allergy_docs->files as $key => $file) 
                    <li class="margin-top-5">
                        <i class="fa fa-file-o fa-fw"></i>
                        <a href="{{$file['link']}}" target="_blank"> .. {{substr($file['name'], 18, 20)}}</a>
                        <!-- Button trigger modal -->
                        <a href="" class="label label-default" data-toggle="modal" data-target="#remove-{{$key}}-file"><i class="fa fa-close fa-fw"></i></a>
                        <!-- Modal -->
                        <div class="modal fade" id="remove-{{$key}}-file" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <form class="ajax-form" action="{{url('manage/docs/remove')}}" method="post">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                    <input type="hidden" name="doc" value="{{$student->allergy_docs->id}}"/>
                                    <input type="hidden" name="file" value="{{$key}}"/>
                                    <input type="hidden" name="return" value="manage/education/students/{{$student->id}}"/>
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Delete File</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>Are you sure to <strong class="text-danger">delete</strong> the <strong>{{$file['name']}}</strong> file?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </li>
                    @endforeach 

                    @endif

                    <li>
                        @if($student->allergy_docs)
                        <div class="collapse margin-top-5" id="add-allergy-doc-form">
                            <div class="panel panel-primary">
                                <div class="panel-body">
                                    <form class="form-horizontal ajax-form" action="{{url('manage/docs/add')}}" method="post">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                        <input type="hidden" name="doc" value="{{$student->allergy_docs->id}}"/>
                                        <input type="hidden" name="security" value="0"/>
                                        <input type="hidden" name="return" value="manage/education/students/{{$student->id}}"/>
                                        <div class="form-group" id="files">
                                            <div class="col-xs-12">
                                                <input type="file" class="btn btn-sm btn-default" name="files[]" multiple=""/>
                                                <span class="help-block hide"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <button type="submit" class="btn btn-xs btn-primary"> 
                                                    <i class="fa fa-upload fa-fw"></i> Upload
                                                </button>
                                                <a class="btn btn-xs btn-danger" role="button" data-toggle="collapse" href="#add-allergy-doc-form" aria-expanded="false" aria-controls="collapseExample">
                                                    <i class="fa fa-close fa-fw"></i> Close
                                                </a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <a class="btn btn-xs btn-success margin-top-5" role="button" data-toggle="collapse" href="#add-allergy-doc-form" aria-expanded="false" aria-controls="collapseExample">
                            <i class="fa fa-plus fa-fw"></i> Add Allergies Doc
                        </a>
                        @else 
                        <div class="collapse margin-top-5" id="upload-allergy-doc-form">
                            <div class="panel panel-primary">
                                <div class="panel-body">
                                    <form class="form-horizontal ajax-form" action="{{url('manage/docs')}}" method="post">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                        <input type="hidden" name="object_type" value="client_allergy_docs"/>
                                        <input type="hidden" name="object_id" value="{{$student->id}}"/>
                                        <input type="hidden" name="folder" value="clients/{{$student->id}}/allergies"/>
                                        <input type="hidden" name="name" value="Allergies Documents"/>
                                        <input type="hidden" name="security" value="0"/>
                                        <input type="hidden" name="return" value="manage/education/students/{{$student->id}}"/>
                                        <div class="form-group" id="files">
                                            <div class="col-xs-12">
                                                <input type="file" class="btn btn-sm btn-default" name="files[]" multiple=""/>
                                                <span class="help-block hide"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <button type="submit" class="btn btn-xs btn-primary"> 
                                                    <i class="fa fa-upload fa-fw"></i> Upload
                                                </button>
                                                <a class="btn btn-xs btn-danger" role="button" data-toggle="collapse" href="#upload-allergy-doc-form" aria-expanded="false" aria-controls="collapseExample">
                                                    <i class="fa fa-close fa-fw"></i> Close
                                                </a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <a class="btn btn-xs btn-success" role="button" data-toggle="collapse" href="#upload-allergy-doc-form" aria-expanded="false" aria-controls="collapseExample">
                            <i class="fa fa-upload fa-fw"></i> Upload Allergies Doc
                        </a>
                        @endif 
                    </li>
                    @else 
                    <li><label class="label label-success">Has no allergy</label></li>
                    @endif 
                </ul>

                <hr/>

                <ul class="list-unstyled">
                    <li><strong>Disabilities</strong></li>
                    @if($student->has_disability == 1) 
                    <li>{{$student->disabilities}}</li>
                    <li class="margin-top-5">
                        <strong>Disability Documents</strong>
                        @if($student->disability_docs) 
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#disability-doc">
                            <i class="fa fa-trash fa-fw"></i>
                        </button>
                        <!-- Modal -->
                        <div class="modal fade" id="disability-doc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <form class="ajax-form" action="{{url('manage/docs/destroy')}}" method="post">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                    <input type="hidden" name="doc" value="{{$student->disability_docs->id}}"/>
                                    <input type="hidden" name="return" value="manage/education/students/{{$student->id}}"/>
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Delete Document</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>Are you sure to <strong class="text-danger">delete</strong> the <strong>Disability Documents</strong>?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        @endif 
                    </li>

                    @if($student->disability_docs) 
                    @foreach($student->disability_docs->files as $key => $file) 
                    <li class="margin-top-5">
                        <i class="fa fa-file-o fa-fw"></i>
                        <a href="{{$file['link']}}" target="_blank"> .. {{substr($file['name'], 18, 20)}}</a>
                        <!-- Button trigger modal -->
                        <a href="" class="label label-default" data-toggle="modal" data-target="#remove-{{$key}}-file"><i class="fa fa-close fa-fw"></i></a>
                        <!-- Modal -->
                        <div class="modal fade" id="remove-{{$key}}-file" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <form class="ajax-form" action="{{url('manage/docs/remove')}}" method="post">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                    <input type="hidden" name="doc" value="{{$student->disability_docs->id}}"/>
                                    <input type="hidden" name="file" value="{{$key}}"/>
                                    <input type="hidden" name="return" value="manage/education/students/{{$student->id}}"/>
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Delete File</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>Are you sure to <strong class="text-danger">delete</strong> the <strong>{{$file['name']}}</strong> file?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </li>
                    @endforeach 

                    @endif

                    <li>
                        @if($student->disability_docs)
                        <div class="collapse margin-top-5" id="add-disability-doc-form">
                            <div class="panel panel-primary">
                                <div class="panel-body">
                                    <form class="form-horizontal ajax-form" action="{{url('manage/docs/add')}}" method="post">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                        <input type="hidden" name="doc" value="{{$student->disability_docs->id}}"/>
                                        <input type="hidden" name="security" value="0"/>
                                        <input type="hidden" name="return" value="manage/education/students/{{$student->id}}"/>
                                        <div class="form-group" id="files">
                                            <div class="col-xs-12">
                                                <input type="file" class="btn btn-sm btn-default" name="files[]" multiple=""/>
                                                <span class="help-block hide"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <button type="submit" class="btn btn-xs btn-primary"> 
                                                    <i class="fa fa-upload fa-fw"></i> Upload
                                                </button>
                                                <a class="btn btn-xs btn-danger" role="button" data-toggle="collapse" href="#add-disability-doc-form" aria-expanded="false" aria-controls="collapseExample">
                                                    <i class="fa fa-close fa-fw"></i> Close
                                                </a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <a class="btn btn-xs btn-success margin-top-5" role="button" data-toggle="collapse" href="#add-disability-doc-form" aria-expanded="false" aria-controls="collapseExample">
                            <i class="fa fa-plus fa-fw"></i> Add Disability Doc
                        </a>
                        @else 
                        <div class="collapse margin-top-5" id="upload-disability-doc-form">
                            <div class="panel panel-primary">
                                <div class="panel-body">
                                    <form class="form-horizontal ajax-form" action="{{url('manage/docs')}}" method="post">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                        <input type="hidden" name="object_type" value="client_disability_docs"/>
                                        <input type="hidden" name="object_id" value="{{$student->id}}"/>
                                        <input type="hidden" name="folder" value="clients/{{$student->id}}/disabilities"/>
                                        <input type="hidden" name="name" value="Disability Documents"/>
                                        <input type="hidden" name="security" value="0"/>
                                        <input type="hidden" name="return" value="manage/education/students/{{$student->id}}"/>
                                        <div class="form-group" id="files">
                                            <div class="col-xs-12">
                                                <input type="file" class="btn btn-sm btn-default" name="files[]" multiple=""/>
                                                <span class="help-block hide"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <button type="submit" class="btn btn-xs btn-primary"> 
                                                    <i class="fa fa-upload fa-fw"></i> Upload
                                                </button>
                                                <a class="btn btn-xs btn-danger" role="button" data-toggle="collapse" href="#upload-disability-doc-form" aria-expanded="false" aria-controls="collapseExample">
                                                    <i class="fa fa-close fa-fw"></i> Close
                                                </a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <a class="btn btn-xs btn-success" role="button" data-toggle="collapse" href="#upload-disability-doc-form" aria-expanded="false" aria-controls="collapseExample">
                            <i class="fa fa-upload fa-fw"></i> Upload Disability Doc
                        </a>
                        @endif 
                    </li>
                    @else 
                    <li><label class="label label-success">Has no disability</label></li>
                    @endif 
                </ul>

            </div>
        </div>


        <div class="panel panel-info">
            <div class="panel-heading"><h3 class="panel-title">Additional Support</h3></div>
            <div class="panel-body">
                <ul class="list-unstyled">
                    <li><strong>Additional Support</strong></li>
                    @if($student->need_additional_support == 1) 
                    <li>{{$student->additional_support}}</li>
                    <li class="margin-top-5">
                        <strong>Additional Support Documents</strong>
                        @if($student->additional_support_docs) 
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#additional-support-doc">
                            <i class="fa fa-trash fa-fw"></i>
                        </button>
                        <!-- Modal -->
                        <div class="modal fade" id="additional-support-doc" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <form class="ajax-form" action="{{url('manage/docs/destroy')}}" method="post">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                    <input type="hidden" name="doc" value="{{$student->additional_support_docs->id}}"/>
                                    <input type="hidden" name="return" value="manage/education/students/{{$student->id}}"/>
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Delete Document</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>Are you sure to <strong class="text-danger">delete</strong> the <strong>Additional Support Document</strong>?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                        @endif 
                    </li>

                    @if($student->additional_support_docs) 
                    @foreach($student->additional_support_docs->files as $key => $file) 
                    <li class="margin-top-5">
                        <i class="fa fa-file-o fa-fw"></i>
                        <a href="{{$file['link']}}" target="_blank"> .. {{substr($file['name'], 18, 20)}}</a>
                        <!-- Button trigger modal -->
                        <a href="" class="label label-default" data-toggle="modal" data-target="#remove-{{$key}}-file"><i class="fa fa-close fa-fw"></i></a>
                        <!-- Modal -->
                        <div class="modal fade" id="remove-{{$key}}-file" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <form class="ajax-form" action="{{url('manage/docs/remove')}}" method="post">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                    <input type="hidden" name="doc" value="{{$student->additional_support_docs->id}}"/>
                                    <input type="hidden" name="file" value="{{$key}}"/>
                                    <input type="hidden" name="return" value="manage/education/students/{{$student->id}}"/>
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Delete File</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>Are you sure to <strong class="text-danger">delete</strong> the <strong>{{$file['name']}}</strong> file?</p>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </li>
                    @endforeach 

                    @endif

                    <li>
                        @if($student->additional_support_docs)
                        <div class="collapse margin-top-5" id="add-additional-support-doc-form">
                            <div class="panel panel-primary">
                                <div class="panel-body">
                                    <form class="form-horizontal ajax-form" action="{{url('manage/docs/add')}}" method="post">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                        <input type="hidden" name="doc" value="{{$student->additional_support_docs->id}}"/>
                                        <input type="hidden" name="security" value="0"/>
                                        <input type="hidden" name="return" value="manage/education/students/{{$student->id}}"/>
                                        <div class="form-group" id="files">
                                            <div class="col-xs-12">
                                                <input type="file" class="btn btn-sm btn-default" name="files[]" multiple=""/>
                                                <span class="help-block hide"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <button type="submit" class="btn btn-xs btn-primary"> 
                                                    <i class="fa fa-upload fa-fw"></i> Upload
                                                </button>
                                                <a class="btn btn-xs btn-danger" role="button" data-toggle="collapse" href="#add-additional-support-doc-form" aria-expanded="false" aria-controls="collapseExample">
                                                    <i class="fa fa-close fa-fw"></i> Close
                                                </a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <a class="btn btn-xs btn-success margin-top-5" role="button" data-toggle="collapse" href="#add-additional-support-doc-form" aria-expanded="false" aria-controls="collapseExample">
                            <i class="fa fa-plus fa-fw"></i> Add Additional Support Doc
                        </a>
                        @else 
                        <div class="collapse margin-top-5" id="upload-additional-support-doc-form">
                            <div class="panel panel-primary">
                                <div class="panel-body">
                                    <form class="form-horizontal ajax-form" action="{{url('manage/docs')}}" method="post">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                        <input type="hidden" name="object_type" value="client_additional_support_docs"/>
                                        <input type="hidden" name="object_id" value="{{$student->id}}"/>
                                        <input type="hidden" name="folder" value="clients/{{$student->id}}/additional-support"/>
                                        <input type="hidden" name="name" value="Additional Support Documents"/>
                                        <input type="hidden" name="security" value="0"/>
                                        <input type="hidden" name="return" value="manage/education/students/{{$student->id}}"/>
                                        <div class="form-group" id="files">
                                            <div class="col-xs-12">
                                                <input type="file" class="btn btn-sm btn-default" name="files[]" multiple=""/>
                                                <span class="help-block hide"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <button type="submit" class="btn btn-xs btn-primary"> 
                                                    <i class="fa fa-upload fa-fw"></i> Upload
                                                </button>
                                                <a class="btn btn-xs btn-danger" role="button" data-toggle="collapse" href="#upload-additional-support-doc-form" aria-expanded="false" aria-controls="collapseExample">
                                                    <i class="fa fa-close fa-fw"></i> Close
                                                </a>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <a class="btn btn-xs btn-success" role="button" data-toggle="collapse" href="#upload-additional-support-doc-form" aria-expanded="false" aria-controls="collapseExample">
                            <i class="fa fa-upload fa-fw"></i> Upload Additional Support Doc
                        </a>
                        @endif 
                    </li>
                    @else 
                    <li><label class="label label-success">Does not need any additional support</label></li>
                    @endif 
                </ul>
            </div>
        </div>
        
        
        
        
        
        
        
       @include('app.shared.docs', ['module_id' => 6, 'docs' => $student->docs, 'object_type' => 'education_client', 'object_id' => $student->id, 'folder' => 'clients/'.$student->id, 'return' => 'manage/education/students/'.$student->id]) 
                
        

    </div>

</div>







