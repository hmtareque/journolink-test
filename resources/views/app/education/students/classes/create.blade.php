<div class="header">
    <div class="title"><i class="fa fa-lg fa-plus-square-o fa-fw"></i> Enrol in a Class</div>
    <div class="links">
        <a href="#education/students/{{$client->id}}" class="btn btn-success btn-sm"><i class="fa fa-info-circle fa-fw"></i> {{$client->first_name}} {{$client->middle_name}} {{$client->last_name}}</a>

        <a href="#education/students" class="btn btn-primary btn-sm"><i class="fa fa-list-alt fa-fw"></i> All Students</a>

    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-9">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="panel-title">Personal Details</div>
            </div>
            <div class="panel-body">
                <ul class="list-unstyled">
                    <li><strong>Name</strong> : {{$client->first_name}} {{$client->middle_name}}  <strong>{{$client->last_name}}</strong></li>
                    <li><strong>Date of Birth</strong> : {{date('d M Y', strtotime($client->date_of_birth))}}</li>
                    <li>
                        <strong>Gender</strong> : 
                        @if($client->gender == 'male') 
                        <i class="fa fa-male fa-fw text-danger"></i>Male
                        @elseif($client->gender == 'female') 
                        <i class="fa fa-female fa-fw text-danger"></i>Female
                        @elseif($client->gender == 'other') 
                        <i class="fa fa-info-circle fa-fw text-danger"></i> {{$client->other_gender}}
                        @endif 

                    </li>
                </ul>

                <div class="row">
                    <div class="col-xs-12 col-md-5">
                        <ul class="list-unstyled">
                            <li><strong>Address</strong></li>
                            <li>{{$client->address_line_1}}</li>
                            @if($client->address_line_2 != '')<li>{{$client->address_line_2}}</li>@endif
                            <li>{{$client->street}}</li>
                            <li>{{$client->city}}</li>
                            <li><strong>{{$client->postcode}}</strong></li>
                        </ul>
                    </div>
                    <div class="col-xs-12 col-md-7">
                        <ul class="list-unstyled">
                            <li><strong>Contact</strong></li>
                            <li>Phone : @if($client->phone != '') {{$client->phone}} @else <label class="label label-warning">not found</label> @endif</li>
                            <li>Mobile : @if($client->mobile != '') {{$client->mobile}} @else <label class="label label-warning">not found</label> @endif</li>
                            <li>Email : @if($client->email != '') <a href="mailto:{{$client->email}}">{{$client->email}}</a> @else <label class="label label-warning">not found</label> @endif</li>
                        </ul>
                    </div>
                </div>

                <ul class="list-unstyled">
                    <li><strong>Borough</strong> : @if($client->local_authority != '') {{$client->local_authority}} @else <label class="label label-warning">not found</label> @endif</li>
                </ul>
            </div>
        </div>


        <form class="form-horizontal ajax-form" action="{{url('manage/education/students/'.$client->id.'/classes')}}" method="post" data-hash="education/students/{{$client->id}}">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>

            <h4 class="section-title">Enrolment</h4>

            <div class="form-group" id="type">
                <label class="col-sm-3 control-label">Type</label>
                <div class="col-sm-9">
                    <select class="form-control" id="student-type" name="type">
                        <option value="child">Child</option>
                        <option value="adult">Adult</option>
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="enrolment_date">
                <label class="col-sm-3 control-label">Enrolment Date</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control jtoday" name="enrolment_date" placeholder="Enrolment Date">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="referred_by">
                <label class="col-sm-3 control-label">Referral School</label>
                <div class="col-sm-9">
                    <select class="form-control" name="referred_by">
                        <option value=""> -- Please Select -- </option>
                        @foreach($referral_schools as $referral)
                        <option value="{{$referral->id}}">{{$referral->name}}</option>
                        @endforeach 
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="academic_year">
                <label class="col-sm-3 control-label">Academic Year</label>
                <div class="col-sm-9">
                    <select class="form-control" id="select-academic-year" name="academic_year">
                        <option value=""> -- Please Select -- </option>
                        @foreach($academic_years as $academic_year) 
                        <option value="{{$academic_year->id}}">{{$academic_year->year}}</option>
                        @endforeach 
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="term">
                <label class="col-sm-3 control-label">Term</label>
                <div class="col-sm-9" id="year-terms">
                    <select class="form-control" name="term">
                        <option value=""> -- Please Select -- </option>
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="school">
                <label class="col-sm-3 control-label">School</label>
                <div class="col-sm-9">
                    <select class="form-control" id="select-school" name="school">
                        <option value=""> -- Please Select -- </option>
                        @foreach($schools as $school)
                        <option value="{{$school->id}}">{{$school->name}}</option>
                        @endforeach 
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="classes">
                <label class="col-sm-3 control-label">Class</label>
                <div class="col-sm-9" id="school-classes">
                    <span class="label label-info">Please select school.</span>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="child-only">
                <h4 class="section-title">Leave Permission</h4>
                <div class="form-group" id="can_leave_on_own">
                    <label class="col-sm-3 control-label">Can leave on own</label>
                    <div class="col-sm-9">
                        <select class="form-control" id="can-leave-on-own" name="can_leave_on_own">
                            <option value="1">Yes</option>
                            <option value="0">No</option>
                        </select>
                        <div id="have_to_collect_by">
                            <input type="text" class="form-control hide margin-top-5" id="have-to-collect-by" name="have_to_collect_by" placeholder="Have to Collected By">
                            <span class="help-block hide"></span>
                        </div>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <div id="class"><span class="help-block hide"></span></div>
                    <button type="submit" class="btn btn-primary"> 
                        <i class="fa fa-save fa-fw"></i> Save
                    </button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-xs-12 col-md-3">
        @include('app.shared.feed')
    </div>
</div>

