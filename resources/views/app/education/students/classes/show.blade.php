<div class="header">
    <div class="title"><i class="fa fa-lg fa-book fa-fw"></i> Class Details</div>
    <div class="links">

        <a href="#education/students/{{$client->id}}" class="btn btn-primary btn-sm"><i class="fa fa-info-circle fa-fw"></i> {{$client->first_name}} {{$client->middle_name}} {{$client->last_name}}</a>

    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-8">


        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="panel-title">Class Details</div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <ul class="list-unstyled">
                            <li><strong>Name</strong> : {{$client->first_name}} {{$client->middle_name}}  <strong>{{$client->last_name}}</strong></li>
                            <li><strong>Date of Birth</strong> : {{date('d M Y', strtotime($client->date_of_birth))}}</li>
                            <li class="margin-top-5"><strong>Class</strong>: {{$class->course}} - {{$class->group}} <label class="label label-default">{{$class->class_status}}</label></li>
                            <li><strong>Date of Enrolment</strong>: {{date('d M Y', strtotime($class->date_of_enrolment))}}</li>
                            <li><strong>School</strong>: {{$class->school}}</li>
                            <li><strong>Academic Year</strong>: {{$class->year}}</li>
                            <li><strong>Status</strong>: <label class="label label-default">{{$class->status}}</label></li>
                        </ul>
                    </div>
                    
                    <div class="col-xs-12 col-md-6">
                <ul class="list-unstyled">
                    <li><strong>Next of Kin</strong></li>
                   <li>{{$client->next_of_kin}} ({{$client->next_of_kin_relationship}})</li>
                    
               
                    <li class="margin-top-5"><strong>Next of Kin Contact</strong></li>
                    <li>Phone : @if($client->next_of_kin_phone != '') {{$client->next_of_kin_phone}} @else <label class="label label-warning">not assessed</label> @endif</li>
                    <li>Mobile : @if($client->next_of_kin_mobile != '') {{$client->next_of_kin_mobile}} @else <label class="label label-warning">not assessed</label> @endif</li>
                    <li>Email : @if($client->next_of_kin_email != '') <a href="mailto:{{$client->next_of_kin_email}}">{{$client->next_of_kin_email}}</a> @else <label class="label label-warning">not assessed</label> @endif</li>
                </ul>
                    </div>
                </div>
            </div>
        </div>

        <div class="table-responsive">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="panel-title">Attendance</div>
            </div>
            <div class="panel-body">
                <?php $total_attended = 0; ?>
                @if($class->attendances->count()>0) 
                <div class="table-responsive">
                    <table class="table table-bordered table-condensed table-hover">
                        <thead>
                        <th width="20%">Date</th>
                        <th width="15%" class="text-center">Attended</th>
                        <th width="65%">Reason of Absent</th>
                        </thead>
                        <tbody>
                            @foreach($class->attendances as $attendance) 
                            <tr>
                                <td>{{date('d M Y', strtotime($attendance->date))}}</td>
                                <td class="text-center">
                                    @if($attendance->present == 1) 
                                    <?php $total_attended++; ?>
                                    <span class="text-success"><i class="fa fa-check fa-fw"></i></span>
                                    @else 
                                    <span class="text-danger"><i class="fa fa-close fa-fw"></i></span> 
                                    @endif
                                </td>
                                <td>{{$attendance->reason_for_absence}}</td>
                            </tr>
                            @endforeach 
                        </tbody>
                        <tfoot>
                            <tr>
                                <td>Total - <strong>{{$class->attendances->count()}}</strong></td>
                                <td class="text-center"><strong>{{$total_attended}}</strong></td>
                                <td></td>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                @else 
                <div class="alert alert-warning">Never attended in this class!</div>
                @endif 
            </div>
        </div>
        </div>
        
        <div class="table-responsive">
        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="panel-title">Assessments</div>
            </div>
            <div class="panel-body">

                @if(count($class->assessments)>0)
                <table class="table table-bordered table-condensed table-hover">
                    <thead>
                        <th width="30%">Term</th>
                        <th width="10%">Type</th>
                        <th width="30%" class="text-center">Assessment</th>
                        <th width="30%" class="text-center">Documents</th>
                        
                    </thead>
                    <tbody> 
                        @foreach($class->assessments as $term) 
                        <tr>
                            <td rowspan="2"><strong>{{$term['term']}}</strong> {{$term['year']}}<br/><small>{{$term['dates']}}</small> </td>
                            <td class="text-center"><label class="label label-info">External</label></td>
                            <td class="text-center">
                                @if(isset($term['assessments']['external']))
                                
                                <?php $external_level = $term['assessments']['external']['level']; ?>
                                @if($external_level == 'below') 
                                <label class="btn btn-xs btn-danger btn-block">{{$external_level}}</label>
                                @elseif($external_level == 'inline') 
                                <label class="btn btn-xs btn-warning btn-block">{{$external_level}}</label>
                                @elseif($external_level == 'above') 
                                <label class="btn btn-xs btn-success btn-block">{{$external_level}}</label>
                                @else
                                <label class="label label-default">not assessed</label>
                                @endif 
                                
                                @if($term['assessments']['external']['note'] != "")<small>{{$term['assessments']['external']['note']}}</small>@endif

                                @else 
                                <label class="label label-default">not assessed</label>
                                @endif 
                            </td>
                            <td>
                                @if(isset($term['assessments']['external']['docs'])) 
                                    <?php $docs = $term['assessments']['external']['docs']; ?>
                                    @if($docs && count($docs->files)>0)
                                        <ul class="list-unstyled list-inline">
                                        @foreach($docs->files as $file) 
                                        <li><a href="{{$file['link']}}" target="_blank"><i class="fa fa-paperclip fa-fw"></i> {{substr($file['name'], 10, 10)}}</a></li>
                                        @endforeach 
                                        </ul>
                                    @endif 
                                @endif 
                            </td>
                       </tr>
                        
                        <tr>
                            <td class="text-center"><label class="label label-primary">Internal</label></td>
                            <td class="text-center">
                                @if(isset($term['assessments']['internal']))
                                <?php $internal_level = $term['assessments']['internal']['level']; ?>
                                @if($internal_level == 'below') 
                                <label class="btn btn-xs btn-danger btn-block">{{$internal_level}}</label>
                                @elseif($internal_level == 'inline') 
                                <label class="btn btn-xs btn-warning btn-block">{{$internal_level}}</label>
                                @elseif($internal_level == 'above') 
                                <label class="btn btn-xs btn-success btn-block">{{$internal_level}}</label>
                                @else
                                <label class="label label-default">not assessed</label>
                                @endif
                                
                                @if($term['assessments']['internal']['note'] != "")<small>{{$term['assessments']['internal']['note']}}</small>@endif
  
                                @else 
                                <label class="label label-default">not assessed</label>
                                @endif 
                            </td>
                            <td>
                                @if(isset($term['assessments']['external']['docs'])) 
                                    <?php $docs = $term['assessments']['external']['docs']; ?>
                                    @if($docs && count($docs->files)>0)
                                        <ul class="list-unstyled list-inline">
                                        @foreach($docs->files as $file) 
                                        <li><a href="{{$file['link']}}" target="_blank"><i class="fa fa-paperclip fa-fw"></i> {{substr($file['name'], 10, 10)}}</a></li>
                                        @endforeach 
                                        </ul>
                                    @endif 
                                @endif 
                            </td>
                        </tr>
                        @endforeach 
                     </tbody>
                </table>
                @else 
                <div>No assessment of {{$client->first_name}} {{$client->middle_name}} {{$client->last_name}} not assessed for this class!</div>
                @endif 
            </div>
            <div class="panel-footer">
                <a class="btn btn-success btn-xs" href="#education/students/{{$client->id}}/classes/{{$class->id}}/assessments/create"><i class="fa fa-check-circle fa-fw"></i> Record New Assessment</a>
            </div>
        </div>
        </div>


        
    </div>
    <div class="col-xs-12 col-md-4">
        @include('app.shared.feed')
    </div>
</div>

