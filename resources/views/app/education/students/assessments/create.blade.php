<div class="header">
    <div class="title"><i class="fa fa-lg fa-plus-square-o fa-fw"></i> Enrol in a Class</div>
    <div class="links">
        <a href="#education/students/{{$student->client_id}}" class="btn btn-success btn-sm"><i class="fa fa-info-circle fa-fw"></i> {{$student->first_name}} {{$student->middle_name}} {{$student->last_name}}</a>
        <a href="#education/students/{{$student->client_id}}/classes/{{$class->id}}" class="btn btn-primary btn-sm"><i class="fa fa-info-circle fa-fw"></i> {{$class->course}} - {{$class->group}} Info</a>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-9">

        <div class="panel panel-info">
            <div class="panel-heading">
                <div class="panel-title">Class Details</div>
            </div>
            <div class="panel-body">
                <div class="row">
                    <div class="col-xs-12 col-md-6">
                        <ul class="list-unstyled">
                            <li><strong>Name</strong> : {{$student->first_name}} {{$student->middle_name}}  <strong>{{$student->last_name}}</strong></li>
                            <li><strong>Date of Birth</strong> : {{date('d M Y', strtotime($student->date_of_birth))}}</li>
                            <li class="margin-top-5"><strong>Class</strong>: {{$class->course}} - {{$class->group}} <label class="label label-default">{{$class->class_status}}</label></li>
                            <li><strong>Date of Enrolment</strong>: {{date('d M Y', strtotime($class->date_of_enrolment))}}</li>
                            <li><strong>School</strong>: {{$class->school}}</li>
                            <li><strong>Academic Year</strong>: {{$class->year}}</li>
                            <li><strong>Status</strong>: <label class="label label-default">{{$class->status}}</label></li>
                        </ul>
                    </div>

                    <div class="col-xs-12 col-md-6">
                        <ul class="list-unstyled">
                            <li><strong>Next of Kin</strong></li>
                            <li>{{$student->next_of_kin}} ({{$student->next_of_kin_relationship}})</li>


                            <li class="margin-top-5"><strong>Next of Kin Contact</strong></li>
                            <li>Phone : @if($student->next_of_kin_phone != '') {{$student->next_of_kin_phone}} @else <label class="label label-warning">not found</label> @endif</li>
                            <li>Mobile : @if($student->next_of_kin_mobile != '') {{$student->next_of_kin_mobile}} @else <label class="label label-warning">not found</label> @endif</li>
                            <li>Email : @if($student->next_of_kin_email != '') <a href="mailto:{{$student->next_of_kin_email}}">{{$student->next_of_kin_email}}</a> @else <label class="label label-warning">not found</label> @endif</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        
 
        <form class="form-horizontal ajax-form" action="{{url('manage/education/students/'.$student->client_id.'/classes/'.$class->id.'/assessments')}}" method="post" data-hash="education/students/{{$student->client_id}}/classes/{{$class->id}}">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <input type="hidden" name="student_id" value="{{$student->id}}"/>

            <h4 class="section-title">Assessment</h4>

            <div class="form-group" id="type">
                <label class="col-sm-3 control-label">Type</label>
                <div class="col-sm-9">
                    <select class="form-control" id="student-type" name="type">
                        <option value="external">External</option>
                        <option value="internal">Internal</option>
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="term">
                <label class="col-sm-3 control-label">Term</label>
                <div class="col-sm-9" id="year-terms">
                    <select class="form-control" name="term">
                        <option value=""> -- Please Select -- </option>
                        @foreach($class->terms as $term)
                        <option value="{{$term->id}}">{{$term->term}} {{$term->dates}}</option>
                        @endforeach
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="level">
                <label class="col-sm-3 control-label">Level</label>
                <div class="col-sm-9">
                    <select name="level" class="form-control">
                        <option value="below">Below</option>
                        <option value="inline">Inline</option>
                        <option value="above">Above</option>
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="note">
                <label class="col-sm-3 control-label">Note</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="note" placeholder="Note"></textarea>
                    <span class="help-block hide"></span>
                </div>
            </div>

           

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <div id="exist"><span class="help-block hide"></span></div>
                    <button type="submit" class="btn btn-primary"> 
                        <i class="fa fa-save fa-fw"></i> Save
                    </button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-xs-12 col-md-3">
        @include('app.shared.feed')
    </div>
</div>

