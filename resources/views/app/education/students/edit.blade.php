<div class="header">
    <div class="title"><i class="fa fa-lg fa-plus-square fa-fw"></i> Edit Student</div>
    <div class="links">
        <a href="#education/students/{{$student->id}}" class="btn btn-success btn-sm"><i class="fa fa-info-circle fa-fw"></i> {{$student->first_name}} {{$student->middle_name}} {{$student->last_name}}</a>
        <a href="#education/students" class="btn btn-info btn-sm"><i class="fa fa-list-alt fa-fw"></i> Recent Students</a>

    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-8">

        <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->

        <form class="form-horizontal ajax-form" action="{{url('manage/education/students/'.$student->id)}}" method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <input type="hidden" name="_method" value="put"/>

            <h4 class="section-title">Personal Information</h4>

            <div class="form-group" id="first_name">
                <label class="col-sm-3 control-label">First Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="first_name" placeholder="First Name" value="{{$student->first_name}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="middle_name">
                <label class="col-sm-3 control-label">Middle Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="middle_name" placeholder="Middle Name" value="{{$student->middle_name}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="last_name">
                <label class="col-sm-3 control-label">Last Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="last-name" name="last_name" placeholder="Last Name" value="{{$student->last_name}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="date_of_birth">
                <label class="col-sm-3 control-label">Date of Birth</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control jdob" id="date-of-birth" name="date_of_birth" placeholder="Date of Birth" value="{{date('d/m/Y', strtotime($student->date_of_birth))}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="gender">
                <label class="col-sm-3 control-label">Gender</label>
                <div class="col-sm-9">
                    <select class="form-control" name="gender">
                        <option value="male" @if($student->gender == 'male') selected="" @endif>Male</option>
                        <option value="female" @if($student->gender == 'female') selected="" @endif>Female</option>
                        <option value="other" @if($student->gender == 'other') selected="" @endif>Other</option>
                    </select>
                    <div id="other_gender">
                        <input type="text" class="form-control @if($student->gender != 'other') hide @endif margin-top-5" name="other_gender" placeholder="Other Gender" value="{{$student->other_gender}}">
                        <span class="help-block hide"></span>
                    </div>
                </div>
            </div>

            <div id="duplicate-clients"></div>

            <h4 class="section-title">Address</h4>

            <div class="form-group" id="address_line_1">
                <label class="col-sm-3 control-label">Address Line 1</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="address_line_1" placeholder="Address Line 1" value="{{$student->address_line_1}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="address_line_2">
                <label class="col-sm-3 control-label">Address Line 2</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="address_line_2" placeholder="Address Line 2" value="{{$student->address_line_2}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="street">
                <label class="col-sm-3 control-label">Street</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="street" placeholder="Street" value="{{$student->street}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="city">
                <label class="col-sm-3 control-label">City</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="city" placeholder="City" value="{{$student->city}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="postcode">
                <label class="col-sm-3 control-label">Postcode</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" id="post-code" name="postcode" placeholder="Postcode" value="{{$student->postcode}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="local_authority">
                <label class="col-sm-3 control-label">Borough or Local Authority</label>
                <div class="col-sm-9">
                    <select class="form-control" name="local_authority">
                        <option value=""> -- Please Select -- </option>
                        @foreach($boroughes as $borough)
                        <option value="{{trim($borough->name)}}" @if(trim($borough->name) == trim($student->local_authority)) selected='' @endif>{{$borough->name}}</option>
                        @endforeach 
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <h4 class="section-title">Contacts</h4>

            <div class="form-group" id="phone">
                <label class="col-sm-3 control-label">Phone</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="phone" placeholder="Phone" value="{{$student->phone}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="email">
                <label class="col-sm-3 control-label">Email Address</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="email" placeholder="Email Address" value="{{$student->email}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="mobile">
                <label class="col-sm-3 control-label">Mobile</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="mobile" placeholder="Mobile" value="{{$student->mobile}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <h4 class="section-title">Next of Kin</h4>

            <div class="form-group" id="next_of_kin">
                <label class="col-sm-3 control-label">Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="next_of_kin" placeholder="Next of Kin" value="{{$student->next_of_kin}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="next_of_kin_relationship">
                <label class="col-sm-3 control-label">Relationship</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="next_of_kin_relationship" placeholder="Relationship" value="{{$student->next_of_kin_relationship}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="next-of-kin-address-same-as-main">
                <label class="col-sm-3 control-label">Address Same as Main</label>
                <div class="col-sm-9">
                    <select class="form-control" id="next-of-kin-address" name="next_of_kin_address_same_as_main">
                        <option value="1" @if($student->next_of_kin_address_same_as_main == 1) selected='' @endif>Yes</option>
                        <option value="0" @if($student->next_of_kin_address_same_as_main == 0) selected='' @endif>No</option>
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="@if($student->next_of_kin_address_same_as_main == 1) hide @endif" id="next-of-kin-address-data">

                <div class="form-group" id="next_of_kin_address_line_1">
                    <label class="col-sm-3 control-label">Address Line 1</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="next_of_kin_address_line_1" placeholder="Address Line 1" value="{{$student->next_of_kin_address_line_1}}">
                        <span class="help-block hide"></span>
                    </div>
                </div>

                <div class="form-group" id="next_of_kin_address_line_2">
                    <label class="col-sm-3 control-label">Address Line 2</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="next_of_kin_address_line_2" placeholder="Address Line 2" value="{{$student->next_of_kin_address_line_2}}">
                        <span class="help-block hide"></span>
                    </div>
                </div>

                <div class="form-group" id="next_of_kin_street">
                    <label class="col-sm-3 control-label">Street</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="next_of_kin_street" placeholder="Street" value="{{$student->next_of_kin_street}}">
                        <span class="help-block hide"></span>
                    </div>
                </div>

                <div class="form-group" id="next_of_kin_city">
                    <label class="col-sm-3 control-label">City</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="next_of_kin_city" placeholder="City" value="{{$student->next_of_kin_city}}">
                        <span class="help-block hide"></span>
                    </div>
                </div>

                <div class="form-group" id="next_of_kin_postcode">
                    <label class="col-sm-3 control-label">Postcode</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="next_of_kin_postcode" placeholder="Postcode" value="{{$student->next_of_kin_postcode}}">
                        <span class="help-block hide"></span>
                    </div>
                </div>

            </div>

            <div class="form-group" id="next_of_kin_phone">
                <label class="col-sm-3 control-label">Phone</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="next_of_kin_phone" placeholder="Phone" value="{{$student->next_of_kin_phone}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="next_of_kin_mobile">
                <label class="col-sm-3 control-label">Mobile</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="next_of_kin_mobile" placeholder="Mobile" value="{{$student->next_of_kin_mobile}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="next_of_kin_email">
                <label class="col-sm-3 control-label">Email Address</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="next_of_kin_email" placeholder="Email Address" value="{{$student->next_of_kin_email}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <h4 class="section-title">Immigration</h4>

            <div class="form-group" id="born_in_the_uk">
                <label class="col-sm-3 control-label">Born in the UK</label>
                <div class="col-sm-9">
                    <select class="form-control" id="check-born-in-the-uk" name="born_in_the_uk">
                        <option value="0" @if($student->born_in_the_uk == 0) selected="" @endif>No</option>
                        <option value="1" @if($student->born_in_the_uk == 1) selected="" @endif>Yes</option>
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="arrived_in_the_uk">
                <label class="col-sm-3 control-label">Arrived in the UK</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control jtoday" name="arrived_in_the_uk" placeholder="Arrived in the UK" value="{{date('d/m/Y', strtotime($student->arrived_in_the_uk))}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="immigration_status">
                <label class="col-sm-3 control-label">Immigration Status</label>
                <div class="col-sm-9">
                    <select class="form-control" name="immigration_status">
                        <option value=""> -- Please Select -- </option>
                        @foreach($immigration_statuses as $status)
                        <option value="{{$status->name}}" @if(trim($status->name) == trim($student->immigration_status)) selected="" @endif>{{$status->name}}</option>
                        @endforeach 
                        <option value="other" @if(trim($status->name) == 'other') selected="" @endif>Other</option>
                    </select>
                    <div id="other_immigration_status">
                        <input type="text" class="form-control @if(trim($status->name) != 'other') hide @endif margin-top-5" name="other_immigration_status" placeholder="Other Immigration Status" value="{{$student->other_immigration_status}}">
                        <span class="help-block hide"></span>
                    </div>
                </div>
            </div>


            <div class="form-group" id="immigration_note">
                <label class="col-sm-3 control-label">Note</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="immigration_note">{{$student->immigration_note}}</textarea>
                    <span class="help-block hide"></span>
                </div>
            </div>



            <h4 class="section-title">Language</h4>

            <div class="form-group" id="first_language">
                <label class="col-sm-3 control-label">First Language</label>
                <div class="col-sm-9">
                    <select class="form-control" name="first_language">
                        <option value=""> -- Please Select -- </option>
                        @foreach(languages() as $code => $language)
                        <option value="{{$code}}" @if($code == $student->first_language) selected="" @endif>{{$language}}</option>
                        @endforeach 
                        <option value="other" @if($student->first_language == 'other') selected="" @endif>Other</option>
                    </select>
                    <div id="other_first_language">
                        <input type="text" class="form-control @if($student->first_language != 'other') hide @endif margin-top-5" name="other_first_language" placeholder="Other First Language">
                        <span class="help-block hide"></span>
                    </div>
                </div>
            </div>


            <h4 class="section-title">Health</h4>

            <div class="form-group" id="gp_name">
                <label class="col-sm-3 control-label">GP Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="gp_name" placeholder="GP Name" value="{{$student->gp_name}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="gp_contact">
                <label class="col-sm-3 control-label">GP Contact</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="gp_contact" placeholder="GP Contact" value="{{$student->gp_contact}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="has_medical_condition">
                <label class="col-sm-3 control-label">Has Medical Conditions</label>
                <div class="col-sm-9">
                    <select class="form-control" id="has-medical-conditions" name="has_medical_condition">
                        <option value="0" @if($student->has_medical_condition == 0) selected="" @endif>No</option>
                        <option value="1" @if($student->has_medical_condition == 1) selected="" @endif>Yes</option>
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group hide" id="medical_conditions">
                <label class="col-sm-3 control-label">Medical Conditions</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="medical_conditions">{{$student->medical_conditions}}</textarea>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="has_allergy">
                <label class="col-sm-3 control-label">Has Allergy</label>
                <div class="col-sm-9">
                    <select class="form-control" id="has-allergy" name="has_allergy">
                        <option value="0" @if($student->has_allergy == 0) selected="" @endif>No</option>
                        <option value="1" @if($student->has_allergy == 1) selected="" @endif>Yes</option>
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group hide" id="allergies">
                <label class="col-sm-3 control-label">Allergies</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="allergies">{{$student->allergies}}</textarea>
                    <span class="help-block hide"></span>
                </div>
            </div>




            <div class="form-group" id="has_disability">
                <label class="col-sm-3 control-label">Has Disability</label>
                <div class="col-sm-9">
                    <select class="form-control" id="has-disability" name="has_disability">
                        <option value="0" @if($student->has_disability == 0) selected="" @endif>No</option>
                        <option value="1" @if($student->has_disability == 1) selected="" @endif>Yes</option>
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group hide" id="disabilities">
                <label class="col-sm-3 control-label">Disabilities</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="disabilities">{{$student->disabilities}}</textarea>
                    <span class="help-block hide"></span>
                </div>
            </div>


            <h4 class="section-title">Race, Religion and Origin</h4>

            <div class="form-group" id="ethnicity">
                <label class="col-sm-3 control-label">Ethnicity</label>
                <div class="col-sm-9">
                    <select class="form-control" name="ethnicity">
                        <option value=""> -- Please Select -- </option>
                        @foreach(ethnicities() as $ethnicity_group => $ethnicities)
                        <optgroup label="{{$ethnicity_group}}">
                            @foreach($ethnicities as $ethnicity_code => $ethnicity) 
                            <option value="{{$ethnicity_code}}" @if($student->ethnicity == $ethnicity_code) selected="" @endif>{{$ethnicity}}</option>
                            @endforeach 
                        </optgroup>
                        @endforeach 
                    </select>
                </div>
            </div>

            <div class="form-group" id="religion">
                <label class="col-sm-3 control-label">Religion</label>
                <div class="col-sm-9">
                    <select class="form-control" name="religion">
                        <option value=""> -- Please Select -- </option>
                        @foreach(religion() as $religion)
                        <option value="{{$religion}}" @if($student->religion == $religion) selected="" @endif>{{$religion}}</option>
                        @endforeach 
                        <option value="other" @if($student->religion == 'other') selected="" @endif>Other</option>
                    </select>

                    <input type="text" class="form-control margin-top-5 @if($student->religion != 'other') hide @endif" name="other_religion" placeholder="Other Religion">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="country_of_origin">
                <label class="col-sm-3 control-label">Country of Origin</label>
                <div class="col-sm-9">
                    <select class="form-control" name="country_of_origin">
                        <option value=""> -- Please Select -- </option>
                        @foreach(countries() as $code => $country)
                        <option value="{{$code}}" @if($student->country_of_origin == $code) selected="" @endif>{{$country}}</option>
                        @endforeach 
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>




            <h4 class="section-title">Additional Support</h4>
            <div class="form-group" id="need_additional_support">
                <label class="col-sm-3 control-label">Need Support</label>
                <div class="col-sm-9">
                    <select class="form-control" id="need-additional-support" name="need_additional_support">
                        <option value="0" @if($student->need_additional_support == 0) selected="" @endif>No</option>
                        <option value="1" @if($student->need_additional_support == 1) selected="" @endif>Yes</option>
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group @if($student->need_additional_support == 0) hide @endif" id="additional_support">
                <label class="col-sm-3 control-label">Additional Support</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="additional_support">{{$student->additional_support}}</textarea>
                    <span class="help-block hide"></span>
                </div>
            </div>



            <h4 class="section-title">Consents</h4>

            <div class="form-group" id="has_consent_to_use_info">
                <label class="col-sm-3 control-label"></label>
                <div class="col-sm-9">
                    <div class="checkbox">
                        <label><input type="checkbox" name="has_consent_to_use_info" value="1" @if($student->has_consent_to_use_info == 1) checked="" @endif/> Has consent to use information by {{config('settings.app_name')}}</label>
                    </div>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="has_consent_to_contacted_for_activities">
                <label class="col-sm-3 control-label"></label>
                <div class="col-sm-9">
                    <div class="checkbox">
                        <label><input type="checkbox" name="has_consent_to_contacted_for_activities" value="1" @if($student->has_consent_to_contacted_for_activities == 1) checked="" @endif/> Has consent to contacted by {{config('settings.app_name')}} for Activities</label>
                    </div>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="has_consent_to_appear_in_publicity_photos">
                <label class="col-sm-3 control-label"></label>
                <div class="col-sm-9">
                    <div class="checkbox">
                        <label><input type="checkbox" name="has_consent_to_appear_in_publicity_photos" value="1" @if($student->has_consent_to_appear_in_publicity_photos == 1) checked="" @endif/> Has consent to appear in publicity photos</label>
                    </div>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="has_consent_to_appear_in_publicity_videos">
                <label class="col-sm-3 control-label"></label>
                <div class="col-sm-9">
                    <div class="checkbox">
                        <label><input type="checkbox" name="has_consent_to_appear_in_publicity_videos" value="1" @if($student->has_consent_to_appear_in_publicity_videos == 1) checked="" @endif/> Has consent to appear in publicity videos</label>
                    </div>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="subscribed_newsletter">
                <label class="col-sm-3 control-label"></label>
                <div class="col-sm-9">
                    <div class="checkbox">
                        <label><input type="checkbox" name="subscribe_newsletter" value="1" @if($student->subscribe_newsletter == 1) checked="" @endif/> Subscribe Newsletter</label>
                    </div>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <h4 class="section-title">About Us</h4>
            <div class="form-group" id="heard_about_us_by">
                <label class="col-sm-3 control-label">Heard about us by</label>
                <div class="col-sm-9">
                    <select class="form-control" name="heard_about_us_by">
                        <option value=""> -- Please Select -- </option>
                        @foreach($heard_about_us_by as $source)
                        <option value="{{$source->name}}" @if(trim($source->name) == trim($student->heard_about_us_by)) selected="" @endif>{{$source->name}}</option>
                        @endforeach 
                        <option value="other" @if(trim($source->name) == 'other') selected="" @endif>Other</option>
                    </select>
                    <div id="other_heard_about_us_by">
                        <input type="text" class="form-control @if(trim($source->name) != 'other') hide @endif margin-top-5" name="other_heard_about_us_by" placeholder="Other Source" value="{{$student->other_heard_about_us_by}}">
                        <span class="help-block hide"></span>
                    </div>
                </div>
            </div>


            <div class="form-group hide" id="check_duplicate">
                <label class="col-sm-3 control-label"></label>
                <div class="col-sm-9">
                    <div class="checkbox">
                        <label><input type="checkbox" name="check_duplicate" value="1"/> Checked Duplicates</label>
                    </div>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-primary"> 
                        <i class="fa fa-save fa-fw"></i> Save
                    </button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-xs-12 col-md-4">
        @include('app.shared.feed')
    </div>
</div>

