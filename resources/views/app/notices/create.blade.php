<div class="header">
    <div class="title"><i class="fa fa-lg fa-newspaper-o fa-fw"></i> Notices</div>
    <div class="links">
        <a href="#notices" class="btn btn-success btn-sm"><i class="fa fa-list-alt fa-fw"></i> All Notices</a>
    </div>
</div>

<div class="row">
    <div class="col-md-8 col-xs-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">New Notice</h3>
            </div>
            <div class="panel-body">
                
                  <form class="form-horizontal ajax-form" action="{{url('manage/web/posts')}}" method="post" data-hash="web/posts">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            
            <div class="form-group" id="post_category">
                <label class="col-sm-2 control-label">Target Group</label>
                <div class="col-sm-10">
                    <select name="post_category" class="form-control">
                        <option value="" selected="" disabled=""> -- Please Select -- </option>
                        <option value="1"> All Beneficiaries </option>
                        <option value="2"> All Students </option>
                    </select> 
                    <span class="post_category-help-block help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="title">
                <label class="col-sm-2 control-label">{{trans('label.title')}}</label>
                <div class="col-sm-10">
                    <input class="form-control" type="text" name="title" placeholder="{{trans('label.title')}}">
                    <span class="title-help-block help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="content">
                <label class="col-sm-2 control-label">{{trans('label.content')}}</label>
                <div class="col-sm-10">
                    <textarea id="editor" class="form-control" rows="3" name="content" placeholder="{{trans('label.content')}}"></textarea> 
                    <span class="content-help-block help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="title">
                <label class="col-sm-2 control-label">Send After</label>
                <div class="col-sm-10">
                    <input class="form-control jdate" type="text" name="title" placeholder="Send After">
                    <span class="title-help-block help-block hide"></span>
                </div>
            </div>

            @can("create", 4)
            <div class="form-group">
                <div class="col-sm-offset-2 col-sm-10">
                    <button type="submit" class="btn btn-primary btn-sm"> 
                        <i class="fa fa-save fa-fw"></i> {{trans('action.save')}}
                    </button>
                </div>
            </div>
            @endcan
        </form>

            </div>
        </div>
    </div>
    <div class="col-md-3 col-xs-12">
        @include('app.shared.feed')
    </div>
</div>




