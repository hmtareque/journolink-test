<div class="header">
    <div class="title"><i class="fa fa-lg fa-newspaper-o fa-fw"></i> Notices</div>
    <div class="links">
        <a href="#notices/create" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Create New Notice</a>
    </div>
</div>

<div class="row">
    <div class="col-md-9 col-xs-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Notices</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table data-table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th width="45%">Title</th>
                                <th width="25%">Target Group</th>
                                <th width="15%">Created At</th>
                                <th width="15%">Created By</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td> <a href="">Paiwand is Having an Event</a></td>
                                <td>All Beneficiaries</td>
                                <td>12/09/2016</td>
                                <td>Vladimir Putin</td>
                            </tr>
                  
                            <tr>
                                <td> <a href="">Summar Vacation Dates</a></td>
                                <td>Class A - Students</td>
                                <td>12/09/2016</td>
                                <td>Barak Obama</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
    <div class="col-md-3 col-xs-12">
        @include('app.shared.feed')
    </div>
</div>




