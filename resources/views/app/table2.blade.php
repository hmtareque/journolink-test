<div class="header">
    <div class="title"><i class="fa fa-lg fa-users fa-fw"></i> Groups</div>
    <div class="links">
        <a href="#family/clients/create" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Create New Group</a>
    </div>
</div>

<div class="row">
    <div class="col-md-9 col-xs-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Groups</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table data-table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th width="75%">Group</th>
                                <th width="15%" class="text-center">No. of People</th>
                                <th width="15%">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>All Beneficiaries</td>
                                <td class="text-center">3</td>
                                <td>
                                    <a href="" class="btn btn-default btn-xs btn-block">show</a>
                                </td>
                               
                            </tr>
                            <tr>
                                <td>All Teachers</td>
                                <td class="text-center">1</td>
                                <td>
                                    <a href="" class="btn btn-default btn-xs btn-block">show</a>
                                </td>
                                
                            </tr>
                            <tr>
                                <td>Class A - Students</td>
                                <td class="text-center">1</td>
                                <td>
                                    <a href="" class="btn btn-default btn-xs btn-block">show</a>
                                </td>
                                
                            </tr>
                            <tr>
                                <td>Activity 3 - Attendees</td>
                                <td class="text-center"><label class="label label-danger">none</label></td>
                                <td></td>
                                
                            </tr>
                            <tr>
                                <td>Custom Group</td>
                                <td class="text-center">7</td>
                                <td>
                                    <a href="" class="btn btn-primary btn-xs btn-block">manage</a>
                                </td>
                                
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
    <div class="col-md-3 col-xs-12">
        @include('app.shared.feed')
    </div>
</div>




