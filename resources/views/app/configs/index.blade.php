<div class="header">
    <div class="title"><i class="fa fa-lg fa-cogs fa-fw"></i> {{trans('label.app_settings')}}</div>
    <div class="links">
        <a href="#configs/create" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> {{trans('label.create_new_item', array('item' => trans('label.app_settings')))}}</a>
        <a href="#configs/edit" class="btn btn-primary btn-sm"><i class="fa fa-edit"></i> {{trans('label.edit_item', array('item' => trans('label.app_settings')))}}</a>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-8">

        <!-- Notify Area -->
        @include('app.shared.flash')
        <!-- Notfiy Area End -->

        @if($settings->count() > 0)

        <ul class="list-unstyled list-group">
            @foreach($settings as $setting)
            <li class="list-group-item">
                <strong>{{str_replace('_', ' ', title_case($setting->param))}}</strong> <code>{{$setting->param}}</code> <strong><i class="fa fa-arrow-right fa-fw"></i></strong>

                <!-- text type setting -->
                @if($setting->type == 'text')
                {{$setting->value}}
                @endif

                <!-- option type setting -->
                @if($setting->type == "option") 
                {{$setting->value}} <code>{{json_encode($setting->options)}}</code>
                @endif 

                <!-- image type setting -->
                @if($setting->type == "image") 
                <img style="width: 64px; height: auto; margin-bottom: 5px;" alt="64x64" src="{{asset('storage/'.$setting->value)}}"/>
                @endif

                @if($setting->removable == 1) 
                <button class="btn btn-danger btn-xs pull-right" data-toggle="modal" data-target="#delete-setting-{{$setting->id}}">
                    <i class="fa fa-trash-o"></i> {{trans('action.remove')}}
                </button>

                <!-- Modal -->
                <div class="modal fade" id="delete-setting-{{$setting->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
                    <div class="modal-dialog">
                        <div class="modal-content">
                            <form id="delete-setting-form" class="ajax-form" action="{{url('manage/configs/'.$setting->id)}}" method="post">
                                <input type="hidden" name="_token" value="{{ csrf_token() }}"/>
                                <input type="hidden" name="_method" value="delete"/>

                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                        &times;
                                    </button>
                                    <h4 class="modal-title" id="myModalLabel">{{trans('action.delete')}}</h4>
                                </div>
                                <div class="modal-body">
                                    <p>{{trans('alert.sure_to_delete_item', array('item' => $setting->param))}}</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
                                        {{trans('action.cancel')}}
                                    </button>
                                    <button type="submit" class="btn btn-danger btn-sm">
                                        <i class="fa fa-trash-o fa-fw"></i> {{trans('action.yes')}}
                                    </button>
                                </div>
                            </form> 
                        </div>
                    </div>
                </div>
                @endif

            </li>

            @endforeach 
        </ul>
        
        @else  

       <p class="alert alert-info">
            <i class="fa fa-info"></i>&nbsp;{{trans('alert.no_item_found', array('item' => trans('label.app_settings')))}}
        </p>
        
        @endif 

    </div>
</div>

