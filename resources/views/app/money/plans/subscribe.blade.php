<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="base_url" content="{{url('/manage')}}">
        <meta name="csrf_token" content="{{csrf_token()}}">
        <meta name="env" content="{{ config('app.env') }}">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>{{ config('settings.name') }}</title>

        <link href="{{asset('css/app-requisite.css')}}" rel="stylesheet">
        <link href="{{asset('css/app.css')}}" rel="stylesheet"> 
        <script src="https://js.stripe.com/v3/"></script>  

        <style> 
            /**
             * The CSS shown here will not be introduced in the Quickstart guide, but shows
             * how you can use CSS to style your Element's container.
             */
            .StripeElement {
                background-color: white;
                padding: 8px 12px;
                border-radius: 4px;
                border: 1px solid transparent;
                box-shadow: 0 1px 3px 0 #e6ebf1;
                -webkit-transition: box-shadow 150ms ease;
                transition: box-shadow 150ms ease;
            }

            .StripeElement--focus {
                box-shadow: 0 1px 3px 0 #cfd7df;
            }

            .StripeElement--invalid {
                border-color: #fa755a;
            }

            .StripeElement--webkit-autofill {
                background-color: #fefde5 !important;
            }
        </style>


    </head>
    <body>
        <div class="container">
            <div class="col-xs-12 col-md-6 col-md-offset-3">
                
                <h2 class="text-center" style="margin-bottom: 25px; border-bottom: 1px solid #ddd; padding-bottom: 10px;">{{ config('settings.name') }}<br/><small>Subscribe to Payment Plan</small></h2>


                <form class="form-horizontal" action="{{url('manage/clients/'.$fee->client_id.'/services/'.$fee->service_area.'/fees/'.$fee->id.'/subscriptions')}}" method="post" id="payment-form"> 
                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>

                    <div class="form-group" id="description">
                        <label class="col-sm-3 control-label">Description</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="description">{{$fee->description}}</textarea>
                            <span class="help-block hide"></span>
                        </div>
                    </div>

                     <div class="form-group" id="fee">
                        <label class="col-sm-3 control-label">Due</label>
                        <div class="col-sm-9">
                            <label class="control-label text-danger" style="font-size: 2.0em;">&pound;{{number_format($fee->due, 2, '.', '')}}</label>
                        </div>
                    </div>

                    <div class="form-group" id="plan">
                        <label class="col-sm-3 control-label">Payment Plan</label>
                        <div class="col-sm-9">
                            <select id="plan" class="form-control" name="plan">
                                <option value=""> --- Please Select --- </option>
                                @if($plans->count()>0)
                                @foreach($plans as $plan)
                                <option value="{{$plan->id}}">{{$plan->name}} (&pound;{{$plan->amount}} every {{$plan->interval}})</option>
                                @endforeach 
                                @endif 
                            </select>
                            <span class="help-block hide"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Alert After</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control jdate" name="alert_after" placeholder="Alert After" value="">
                            <span class="help-block hide"></span>
                        </div>
                    </div>
                    
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Stop After</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control jdate" name="stop_after" placeholder="Stop After" value="">
                            <span class="help-block hide"></span>
                        </div>
                    </div>

                    <div class="form-group" id="fee">
                        <label class="col-sm-3 control-label">Card Details</label>
                        <div class="col-sm-9">
                            <div id="card-element">
                                <!-- a Stripe Element will be inserted here. -->
                            </div>
                            <div id="card-errors" class="text-danger" role="alert"></div>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-primary"> 
                                <i class="fa fa-money fa-fw"></i> Pay
                            </button>
                        </div> 
                    </div>
                </form>
            </div> 



            <?php //print_r($errors); ?>


        </div>

        <script type="text/javascript" src="{{asset('js/app-requisite.js')}}"></script>
        <script src="{{asset('js/tinymce/tinymce.min.js')}}"></script> 
        <script type="text/javascript" src="{{asset('js/app.js')}}"></script> 


        <script>

//    ate a Stripe client
var stripe = Stripe('pk_test_Ftmq2F2PQU4AaAwzNaHEqfy1');

// Create an instance of Elements
var elements = stripe.elements();

// Custom styling can be passed to options when creating an Element.
// (Note that this demo uses a wider set of styles than the guide below.)
var style = {
    base: {
        color: '#32325d',
        lineHeight: '24px',
        fontFamily: '"Helvetica Neue", Helvetica, sans-serif',
        fontSmoothing: 'antialiased',
        fontSize: '16px',
        '::placeholder': {
            color: '#aab7c4'
        }
    },
    invalid: {
        color: '#fa755a',
        iconColor: '#fa755a'
    }
};

// Create an instance of the card Element
var card = elements.create('card', {style: style});

// Add an instance of the card Element into the `card-element` <div>
card.mount('#card-element');

// Handle real-time validation errors from the card Element.
card.addEventListener('change', function (event) {
    var displayError = document.getElementById('card-errors');
    if (event.error) {
        displayError.textContent = event.error.message;
    } else {
        displayError.textContent = '';
    }
});

// Handle form submission
var form = document.getElementById('payment-form');
form.addEventListener('submit', function (event) {
    event.preventDefault();

    this.disabled = true;
    this.value = 'Please wait ...';

    stripe.createToken(card).then(function (result) {
        if (result.error) {
            // Inform the user if there was an error
            var errorElement = document.getElementById('card-errors');
            errorElement.textContent = result.error.message;
        } else {
            // Send the token to your server
            stripeTokenHandler(result.token);
        }
    });
});

function stripeTokenHandler(token) {
    // Insert the token ID into the form so it gets submitted to the server
    var form = document.getElementById('payment-form');
    var hiddenInput = document.createElement('input');
    hiddenInput.setAttribute('type', 'hidden');
    hiddenInput.setAttribute('name', 'stripeToken');
    hiddenInput.setAttribute('value', token.id);
    form.appendChild(hiddenInput);

    // Submit the form
    form.submit();
}

        </script>

    </body>
</html>