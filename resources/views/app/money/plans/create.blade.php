<div class="header">
    <div class="title"><i class="fa fa-lg fa-plus-square fa-fw"></i> Create New Plan</div>
    <div class="links">
        <a href="#payment-plans" class="btn btn-primary btn-sm"><i class="fa fa-list-alt fa-fw"></i> All Plans</a>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-9">
        
         <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->
        
        
        <form class="form-horizontal ajax-form" action="{{url('manage/payment-plans')}}" method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>

            <h4 class="section-title">Payment Plan</h4>

            
            <div class="form-group" id="service">
                <label class="col-sm-3 control-label">Service</label>
                <div class="col-sm-9">
                    <select class="form-control" name="service">
                        <option value=""> -- Please Select -- </option>
                        <option value="education">Education</option>
                        <option value="family">Family</option>
                        <option value="advocacy">Advocacy</option>
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>
            
           
            <div class="form-group" id="name">
                <label class="col-sm-3 control-label">Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="name" placeholder="Name" value="">
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            <div class="form-group" id="amount">
                <label class="col-sm-3 control-label">Amount</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control text-right" name="amount" placeholder="Amount" value="">
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            <div class="form-group" id="interval">
                <label class="col-sm-3 control-label">Interval</label>
                <div class="col-sm-9">
                    <select class="form-control" name="interval">
                        <option value=""> -- Please Select -- </option>
                        <option value="day">Daily</option>
                        <option value="week">Weekly</option>
                        <option value="month">Monthly</option>
                        <option value="year">Yearly</option>
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="statement_description">
                <label class="col-sm-3 control-label">Statement Description</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="statement_description" placeholder="Statement Description" value="">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <button type="submit" class="btn btn-primary"> 
                            <i class="fa fa-save fa-fw"></i> Save
                        </button>
                    </div>
            </div>
            
        </form>
    </div>
</div>