<div class="header">
    <div class="title"><i class="fa fa-lg fa-money fa-fw"></i> Payment Plans</div>
    <div class="links">
        <a href="#payment-plans/create" class="btn btn-success btn-sm"><i class="fa fa-plus fa-fw"></i> Create Payment Plan</a>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-9">

        <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->

        <h4 class="section-title">Payment Plans</h4>

        @if($plans->count()>0) 
        <div class="table-responsive">
            <table class="table table-condensed table-bordered table-hover" width="100%">
                <thead>
                    <tr>
                        <th width="15%">Service</th>
                        <th width="40%">Name</th>
                        <th width="15%" class="text-center">Interval</th>
                        <th width="15%" class="text-center">Amount</th>
                        <th width="15%" class="text-center">Customers</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($plans as $plan) 
                    <tr>
                        <td><strong><small>{{strtoupper($plan->service_area)}}</small></strong></td>
                        <td><a href="#payment-plans/{{$plan->id}}">{{$plan->name}}</a> <small title="Statement Descriptor">{{$plan->statement_descriptor}}</small></td>
                        <td class="text-center">{{$plan->interval}}</td>
                        <td class="text-center">&pound;{{$plan->amount}}</td>
                        <td class="text-center"><strong>{{rand(2,10)}}</strong></td>
                    </tr>
                    @endforeach 
                </tbody>
            </table>
        </div>

        @else 
        <div class="alert alert-warning">No payment plan found.</div>
        @endif 

    </div>
</div>