<div class="header">
    <div class="title">
        <i class="fa fa-lg fa-money fa-fw"></i> 
        @if($service == 'education') 
        Create New Education Service Fee
        @elseif($service == 'family') 
        Create Family Service Fee
        @elseif($service == 'advocacy') 
        Create Advocacy Service Fee
        @endif
    </div>
    <div class="links">
        <a href="#education/students/{{$client->id}}" class="btn btn-info btn-sm"><i class="fa fa-info-circle fa-fw"></i> {{$client->first_name}} {{$client->middle_name}} {{$client->last_name}}</a>
        <a href="#clients/{{$client->id}}/services/{{$service}}/fees" class="btn btn-success btn-sm"><i class="fa fa-database fa-fw"></i> Account</a>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-9">

        <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->

        @include('app.shared.client_personal_info', ['client' => $client])

        <div class="panel panel-info">
            <div class="panel panel-heading">Create New Fee</div>
            <div class="panel-body">
                <form class="form-horizontal ajax-form" action="{{url('manage/clients/'.$client->id.'/services/'.$service.'/fees')}}" method="post" data-hash="clients/{{$client}}/services/{{$service}}/fees">
                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    <input type="hidden" name="client" value="{{$client->id}}">
                    <input type="hidden" name="service" value="{{$service}}">

                    <div class="form-group" id="description">
                        <label class="col-sm-3 control-label">Description</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" name="description" placeholder="Description" value="">
                            <span class="help-block hide"></span>
                        </div>
                    </div>

                    <div class="form-group" id="fee_for_service">
                        <label class="col-sm-3 control-label">Fee For Service</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="fee_for_service">
                                <option value=""> -- Please Select -- </option>
                                <?php $services = fee_services($client->id, $service); ?>
                                @if(count($services)>0) 
                                @foreach($services as $service) 
                                <option value="{{$service->id}}">{{$service->name}}</option>
                                @endforeach
                                @else 
                                @endif 
                            </select>
                            <span class="help-block hide"></span>
                        </div>
                    </div>

                    <div class="form-group" id="fee">
                        <label class="col-sm-3 control-label">Fee Amount</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control text-right" name="fee" placeholder="Fee Amount" value="">
                            <span class="help-block hide"></span>
                        </div>
                    </div>

                    <div class="form-group" id="exempt_from_fee">
                        <label class="col-sm-3 control-label">Exempt from fee</label>
                        <div class="col-sm-9">
                            <select class="form-control" id="exempt-from-fee" name="exempt_from_fee">
                                <option value="0">No</option>
                                <option value="1">Yes</option>
                            </select>
                            <span class="help-block hide"></span>
                        </div>
                    </div>

                    <div class="form-group" id="discount">
                        <label class="col-sm-3 control-label">Discount</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control text-right" name="discount" placeholder="Discount" value="">
                            <span class="help-block hide"></span>
                        </div>
                    </div>

                    <div class="form-group" id="payment_note">
                        <label class="col-sm-3 control-label">Exemption Reason</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="exemption_reason"></textarea>
                            <span class="help-block hide"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-primary"> 
                                <i class="fa fa-save fa-fw"></i> Save
                            </button>
                        </div>
                    </div>

                </form>
            </div>
        </div>



    </div>
</div>
