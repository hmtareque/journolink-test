<div class="header">
    <div class="title"><i class="fa fa-lg fa-money fa-fw"></i> Fee Details</div>
    <div class="links">
        @if($fee->due > 0 && $fee->exempt == 0) 
        <a href="#clients/{{$fee->client_id}}/services/{{$fee->service_area}}/fees/{{$fee->id}}/payments/create" class="btn btn-success btn-sm"><i class="fa fa-plus fa-fw"></i> Make Payment</a>
        <a href="{{url('manage/clients/'.$fee->client_id.'/services/'.$fee->service_area.'/fees/'.$fee->id.'/card-payment')}}" class="btn btn-warning btn-sm"><i class="fa fa-credit-card fa-fw"></i> Make Payment by Card <i class="fa fa-external-link fa-fw"></i></a>
        <a href="{{url('manage/clients/'.$fee->client_id.'/services/'.$fee->service_area.'/fees/'.$fee->id.'/subscriptions/create')}}" class="btn btn-info btn-sm"><i class="fa fa-calendar-times-o fa-fw"></i> Add to Payment Plan<i class="fa fa-external-link fa-fw"></i></a>
        @endif
        
        <a href="#clients/{{$fee->client_id}}/services/{{$fee->service_area}}/fees" class="btn btn-primary btn-sm"><i class="fa fa-database fa-fw"></i> Account</a>

        @if($fee->payments->count() == 0) 

        <!-- Button trigger modal -->
        <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#remove-fee-modal">
            <i class="fa fa-trash fa-fw"></i>Remove
        </button>

        <!-- Modal -->
        <div class="modal fade" id="remove-fee-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">

                <form class="ajax-form" action="{{url('manage/clients/'.$fee->client_id.'/services/'.$fee->service_area.'/fees/'.$fee->id)}}" method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    <input type="hidden" name="_method" value="delete"/>
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Remove Fee</h4>
                        </div>
                        <div class="modal-body">
                            <p>Are you sure to remove the fee <strong>{{$fee->description}} (&pound;{{$fee->fee}})</strong>?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger btn-sm">Remove</button>
                        </div>
                    </div>

                </form>
            </div>
        </div>

        @endif 

    </div>
</div>

<div class="row">
    <div class="col-md-9 col-xs-12">
        
        
        <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->
        
        @include('app.shared.client_personal_info', ['client' => $client])
        
        
        
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Fee Details</h3>
            </div>
            <div class="panel-body">
                
                
                
                
                
                <div class="table-responsive">
                    <table class="table table-condensed table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th width="15%" class="text-center">Date</th>
                                <th width="45%">Description</th>
                                <th width="10%" class="text-center">Fee</th>
                                <th width="10%" class="text-center">Discount</th>
                                <th width="10%" class="text-center">Exempt</th>
                                <th width="10%" class="text-center">Amount</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td class="text-center">
                                    <a href="#clients/{{$fee->client_id}}/services/{{$fee->service_area}}/fees/{{$fee->id}}/edit">{{date('d M Y', strtotime($fee->created_at))}}</a>
                                </td>
                                <td>{{$fee->description}}</td>
                                <td class="text-right">{{$fee->fee}}</td>
                                <td class="text-right">{{$fee->discount}}</td>

                                <td class="text-center">
                                    @if($fee->exempt == 1) 
                                    <button type="button" class="btn btn-warning btn-xs" data-container="body" data-toggle="popover" data-placement="top" data-content="{{$fee->reason_for_exemption}}">
                                        <i class="fa fa-info-circle fa-fw"></i> Yes
                                    </button>
                                    @else 
                                    <label class="label label-default">no</label>
                                    @endif 
                                </td>

                                <td class="text-right">{{number_format($fee->fee - $fee->discount, 2, '.', '')}}</td>

                            </tr>

                            @if($fee->payments->count()>0) 
                            <tr class="warning"><td colspan="6"><strong>Payments</strong></td></tr>
                            <tr class="info">
                                <th width="15%" class="text-center">Date</th>
                                <th width="45%">Note</th>
                                <th width="40%" colspan="3">Method</th>
                                <th width="10%">Amount</th>
                            </tr>
                            @foreach($fee->payments as $payment) 
                            <tr>
                                <td class="text-center">
                                    @if($payment->payment_method != "card")
                                    <a href="#clients/{{$fee->client_id}}/services/{{$fee->service_area}}/fees/{{$fee->id}}/payments/{{$payment->id}}/edit">{{date('d M Y', strtotime($payment->created_at))}}</a>
                                    @else 
                                    {{date('d M Y', strtotime($payment->created_at))}}
                                    @endif 
                                </td>
                                <td>
                                    {{$payment->description}}
                                    
                                    @if($payment->has_donation == 1)
                                    <label class="label label-success">made &pound;{{$payment->donation_amount}} donation!</label>
                                    @endif 
                                    
                                    
                                </td>
                                <td colspan="3">{{str_replace('_', ' ', strtoupper($payment->payment_method))}}</td>
                                <td class="text-right">{{$payment->amount}}</td>

                            </tr>
                            @endforeach

                            <tr>
                                <td colspan=5 class="text-right"><strong>Total Paid</strong></td>
                                <td class="text-right"><strong>{{$fee->paid}}</strong></td>
                            </tr>

                            <tr>
                                <td colspan="5" class="text-right"><strong>Due</strong></td>
                                <td class="text-right">@if($fee->due == 0) <label class="label label-success">nothing</label> @else <strong class="text-danger">{{number_format($fee->due, 2, '.', '')}}</strong> @endif </td>
                            </tr>

                            @endif 

                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-xs-12">
        @include('app.shared.feed')
    </div>
</div>


