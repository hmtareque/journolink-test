<div class="header">
    <div class="title"><i class="fa fa-lg fa-money fa-fw"></i> Update Payment</div>
    <div class="links">

        <a href="#clients/{{$fee->client_id}}/services/{{$fee->service_area}}/fees/{{$fee->id}}" class="btn btn-success btn-sm"><i class="fa fa-info-circle fa-fw"></i> Fee Details</a>
        <a href="#clients/{{$fee->client_id}}/services/{{$fee->service_area}}/fees/{{$fee->id}}/payments/create" class="btn btn-primary btn-sm"><i class="fa fa-plus fa-fw"></i> Make Payment</a>
        <a href="{{url('manage/clients/'.$fee->client_id.'/services/'.$fee->service_area.'/fees/'.$fee->id.'/card-payment')}}" class="btn btn-warning btn-sm"><i class="fa fa-credit-card fa-fw"></i> Make Payment by Card <i class="fa fa-external-link fa-fw"></i></a>

    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-9">

        <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->

        @include('app.shared.client_personal_info', ['client' => $client])

        <div class="panel panel-info">
            <div class="panel-heading">Update Payment</div>
            <div class="panel-body">
                <form class="form-horizontal ajax-form" action="{{url('manage/clients/'.$fee->client_id.'/services/'.$fee->service_area.'/fees/'.$fee->id.'/payments/'.$payment->id)}}" method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    <input type="hidden" name="_method" value="put"/>

                    <div class="form-group" id="fee">
                        <label class="col-sm-3 control-label">Fee For</label>
                        <div class="col-sm-9">
                            <label class="control-label">{{$fee->description}}</label>
                        </div>
                    </div>

                    <div class="form-group" id="fee">
                        <label class="col-sm-3 control-label">Due</label>
                        <div class="col-sm-9">
                            <label class="control-label text-danger" style="font-size: 2.0em;">&pound;{{number_format($fee->due, 2, '.', '')}}</label>
                        </div>
                    </div>

                    <div class="form-group" id="description">
                        <label class="col-sm-3 control-label">Description</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="description">{{$payment->description}}</textarea>
                            <span class="help-block hide"></span>
                        </div>
                    </div>

                    <div class="form-group" id="payment_method">
                        <label class="col-sm-3 control-label">Payment Method</label>
                        <div class="col-sm-9">
                            <select class="form-control" name="payment_method">
                                <option value=""> -- Please Select -- </option>
                                <option value="cash" @if($payment->payment_method == 'cash') selected="" @endif>Cash</option>
                                <option value="cheque" @if($payment->payment_method == 'cheque') selected="" @endif>Cheque</option>
                                <option value="bank_transfer" @if($payment->payment_method == 'bank_transfer') selected="" @endif>Bank Transfer</option>
                            </select>
                            <span class="help-block hide"></span>
                        </div>
                    </div>

                    <div class="form-group" id="amount">
                        <label class="col-sm-3 control-label">Amount</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control text-right" name="amount" placeholder="Amount" value="{{$payment->amount}}">
                            <span class="help-block hide"></span>
                        </div>
                    </div>

                    <div class="form-group" id="donate">
                        <label class="col-sm-3 control-label">Want to Donate?</label>
                        <div class="col-sm-9">
                            <select id="payment-method" class="form-control" name="donate">
                                <option value="1" @if($payment->has_donation == 1) selected="" @endif>Yes</option>
                                <option value="0" @if($payment->has_donation == 0) selected="" @endif>No</option>
                            </select>
                            <span class="help-block hide"></span>
                        </div>
                    </div>

                    <div class="form-group" id="donation_amount">
                        <label class="col-sm-3 control-label">Donation Amount</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control text-right" name="donation_amount" placeholder="Donation Amount" value="{{$payment->donation_amount}}">
                            <span class="help-block hide"></span>
                        </div>
                    </div>

                    <div class="form-group">
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-primary btn-sm"> 
                                <i class="fa fa-save fa-fw"></i> Update
                            </button>
                            <!-- Button trigger modal -->
                            <button type="button" class="btn btn-danger btn-sm" data-toggle="modal" data-target="#remove-fee-modal">
                                <i class="fa fa-trash fa-fw"></i>Remove
                            </button>
                            <!-- Modal -->
                            <div class="modal fade" id="remove-fee-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                <div class="modal-dialog" role="document">

                                    <form class="ajax-form" action="{{url('manage/clients/'.$fee->client_id.'/services/'.$fee->service_area.'/fees/'.$fee->id.'/payments/'.$payment->id)}}" method="post">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                        <input type="hidden" name="_method" value="delete"/>

                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                <h4 class="modal-title" id="myModalLabel">Remove Payment</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>Are you sure to remove the payment <strong>&pound;{{$payment->amount}}</strong>?</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                                                <button type="submit" class="btn btn-danger btn-sm">Remove</button>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>