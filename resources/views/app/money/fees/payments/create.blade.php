<div class="header">
    <div class="title"><i class="fa fa-lg fa-money fa-fw"></i> New Payment</div>
    <div class="links">
        <a href="#clients/{{$fee->client_id}}/services/{{$fee->service_area}}/fees/{{$fee->id}}" class="btn btn-success btn-sm"><i class="fa fa-info-circle fa-fw"></i> Fee Details</a>
        <a href="{{url('manage/clients/'.$fee->client_id.'/services/'.$fee->service_area.'/fees/'.$fee->id.'/card-payment')}}" class="btn btn-warning btn-sm"><i class="fa fa-credit-card fa-fw"></i> Make Payment by Card <i class="fa fa-external-link fa-fw"></i></a>
        <a href="#clients/{{$fee->client_id}}/services/{{$fee->service_area}}/fees" class="btn btn-primary btn-sm"><i class="fa fa-database fa-fw"></i> Account</a>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-9">

        <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->

        @include('app.shared.client_personal_info', ['client' => $client])

        <div class="panel panel-info">
            <div class="panel-heading">Make New Payment</div>
            <div class="panel-body">
                <form class="form-horizontal ajax-form" action="{{url('manage/clients/'.$fee->client_id.'/services/'.$fee->service_area.'/fees/'.$fee->id.'/payments')}}" method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    <div class="form-group" id="fee">
                        <label class="col-sm-3 control-label">Fee For</label>
                        <div class="col-sm-9">
                            <label class="control-label">{{$fee->description}}</label>
                        </div>
                    </div>

                    <div class="form-group" id="fee">
                        <label class="col-sm-3 control-label">Due</label>
                        <div class="col-sm-9">
                            <label class="control-label text-danger" style="font-size: 2.0em;">&pound;{{number_format($fee->due, 2, '.', '')}}</label>
                        </div>
                    </div>

                    <div class="form-group" id="description">
                        <label class="col-sm-3 control-label">Description</label>
                        <div class="col-sm-9">
                            <textarea class="form-control" name="description"></textarea>
                            <span class="help-block hide"></span>
                        </div>
                    </div>

                    <div class="form-group" id="payment_method">
                        <label class="col-sm-3 control-label">Payment Method</label>
                        <div class="col-sm-9">
                            <select id="payment-method" class="form-control" name="payment_method">
                                <option value=""> -- Please Select -- </option>
                                <option value="cash">Cash</option>
                                <option value="cheque">Cheque</option>
                                <option value="bank_transfer">Bank Transfer</option>
                            </select>
                            <span class="help-block hide"></span>
                        </div>
                    </div>

                    <div class="form-group" id="amount">
                        <label class="col-sm-3 control-label">Amount</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control text-right" id="payment-amount" name="amount" placeholder="Amount" value="">
                            <span class="help-block hide"></span>
                        </div>
                    </div>

                    <div class="form-group" id="donate">
                        <label class="col-sm-3 control-label">Want to Donate?</label>
                        <div class="col-sm-9">
                            <select id="payment-method" class="form-control" name="donate">
                                <option value="1">Yes</option>
                                <option value="0">No</option>
                            </select>
                            <span class="help-block hide"></span>
                        </div>
                    </div>

                    <div class="form-group" id="donation_amount">
                        <label class="col-sm-3 control-label">Donation Amount</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control text-right" name="donation_amount" placeholder="Donation Amount" value="">
                            <span class="help-block hide"></span>
                        </div>
                    </div>

                    <div class="form-group" id="normal-payment-form">
                        <div class="col-sm-offset-3 col-sm-9">
                            <button type="submit" class="btn btn-primary"> 
                                <i class="fa fa-money fa-fw"></i> Pay
                            </button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
