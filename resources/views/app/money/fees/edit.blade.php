<div class="header">
    <div class="title"><i class="fa fa-lg fa-edit fa-fw"></i> Update Fee</div>
    <div class="links">
        <a href="#clients/{{$fee->client_id}}/services/{{$fee->service_area}}/fees/create" class="btn btn-primary btn-sm"><i class="fa fa-plus fa-fw"></i> Create New Fee</a>
        <a href="#clients/{{$fee->client_id}}/services/{{$fee->service_area}}/fees/{{$fee->id}}" class="btn btn-success btn-sm"><i class="fa fa-info-circle fa-fw"></i> Fee Details</a>
        <a href="#clients/{{$fee->client_id}}/services/{{$fee->service_area}}/fees" class="btn btn-success btn-sm"><i class="fa fa-list-alt fa-fw"></i>Account</a>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-9">
        
        
        <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->
        
        @include('app.shared.client_personal_info', ['client' => $client])
        
        
        
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Update Fee</h3>
            </div>
            <div class="panel-body">
        
        
     
        

        <form class="form-horizontal ajax-form" action="{{url('manage/clients/'.$fee->client_id.'/services/'.$fee->service_area.'/fees/'.$fee->id)}}" method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <input type="hidden" name="_method" value="put"/>

            
            

            <div class="form-group" id="description">
                <label class="col-sm-3 control-label">Description</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="description" placeholder="Description" value="{{$fee->description}}">
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            <div class="form-group" id="fee_for_service">
                <label class="col-sm-3 control-label">Fee For Service</label>
                <div class="col-sm-9">
                    <select class="form-control" name="fee_for_service">
                        <option value=""> -- Please Select -- </option>
                        <?php $services = fee_services($fee->client_id, $fee->service_area); ?>
                        @if(count($services)>0) 
                        @foreach($services as $service) 
                         <option value="{{$service->id}}" @if($fee->service_id == $service->id) selected="" @endif>{{$service->name}}</option>
                        @endforeach
                        @else 
                        @endif 
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="fee">
                <label class="col-sm-3 control-label">Fee Amount</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control text-right" name="fee" placeholder="Fee Amount" value="{{$fee->fee}}">
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            @if($fee->payments->count()<=0) 
            <div class="form-group" id="exempt_from_fee">
                <label class="col-sm-3 control-label">Exempt from fee</label>
                <div class="col-sm-9">
                    <select class="form-control" id="exempt-from-fee" name="exempt_from_fee">
                        <option value="0" @if($fee->exempt == 0) selected="" @endif>No</option>
                        <option value="1" @if($fee->exempt == 1) selected="" @endif>Yes</option>
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>
            @endif 

            <div class="form-group" id="discount">
                <label class="col-sm-3 control-label">Discount</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control text-right" name="discount" placeholder="Discount" value="{{$fee->discount}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            @if($fee->payments->count()<=0) 
            <div class="form-group" id="exemption_reason">
                <label class="col-sm-3 control-label">Exemption Reason</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="exemption_reason">{{$fee->reason_for_exemption}}</textarea>
                    <span class="help-block hide"></span>
                </div>
            </div>
            @endif 

            <div class="form-group">
                    <div class="col-sm-offset-3 col-sm-9">
                        <button type="submit" class="btn btn-primary"> 
                            <i class="fa fa-refresh fa-fw"></i> Update
                        </button>
                    </div>
            </div>
            
        </form>
                
                
                
    </div>
    </div>
</div>
    
    

<script>
    $(function () {
        $('[data-toggle="popover"]').popover()
    })
</script>