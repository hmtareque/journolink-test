<div class="header">
    <div class="title">
        <i class="fa fa-lg fa-database fa-fw"></i> 
        @if($service == 'education') 
        Education Service Account
        @elseif($service == 'family') 
        Family Service Account
        @elseif($service == 'advocacy') 
        Advocacy Service Account
        @endif 
    </div>
    <div class="links">
        <a href="#education/students/{{$client->id}}" class="btn btn-info btn-sm"><i class="fa fa-info-circle fa-fw"></i> {{$client->first_name}} {{$client->middle_name}} {{$client->last_name}}</a>
        <a href="#clients/{{$client->id}}/services/{{$service}}/fees/create" class="btn btn-success btn-sm"><i class="fa fa-plus fa-fw"></i> Create New Fee</a>
    </div>
</div>

<div class="row">
    <div class="col-md-9 col-xs-12">
        
        <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->
        
        @include('app.shared.client_personal_info', ['client' => $client])
        
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Account</h3>
            </div>
            <div class="panel-body">
                @if($fees->count()>0) 
                <div class="table-responsive">
                    <table class="table table-condensed table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th width="15%" class="text-center">Date</th>
                                <th width="35%">Description</th>
                                <th width="10%" class="text-center">Fee</th>
                                <th width="10%" class="text-center">Discount</th>
                                <th width="10%" class="text-center">Exempt</th>
                                <th width="10%" class="text-center">Paid</th>
                                <th width="10%" class="text-center">Balance</th>
                            </tr>
                        </thead>
                        <tbody>

                            @foreach($fees as $fee) 

                            <tr>
                                <td class="text-center">
                                    <a href="#clients/{{$fee->client_id}}/services/{{$fee->service_area}}/fees/{{$fee->id}}">{{date('d M Y', strtotime($fee->created_at))}}</a>
                                </td>
                                <td>
                                    {{$fee->description}}
                                    
                                    @if($fee->exempt == 1) 
                                    <!-- Button trigger modal -->
                                    <button type="button" class="btn btn-xs btn-warning" data-toggle="modal" data-target="#exempt-{{$fee->id}}-modal">
                                        <i class="fa fa-warning fa-fw"></i>exempt
                                    </button>

                                    <!-- Modal -->
                                    <div class="modal fade" id="exempt-{{$fee->id}}-modal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                                        <div class="modal-dialog" role="document">
                                            <div class="modal-content">
                                                <div class="modal-header">
                                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                    <h4 class="modal-title" id="myModalLabel">Reason for Exemption</h4>
                                                </div>
                                                <div class="modal-body">
                                                    <p>{{$fee->reason_for_exemption}}</p>
                                                </div>
                                                <div class="modal-footer">
                                                    <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    @endif 
                                </td>
                                <td class="text-right">{{$fee->fee}}</td>
                                <td class="text-right">{{$fee->discount}}</td>
                                <td class="text-right">{{$fee->exempt_amount}}</td>
                                <td class="text-right">{{$fee->paid}}</td>
                                <td class="text-right">@if($fee->balance == 0 && $fee->exempt == 0) <label class="label label-success">paid</label> @elseif($fee->exempt == 1) -- @else {{number_format($fee->balance, 2, '.', '')}} @endif </td>
                            </tr>
                            @endforeach 

                        <tfoot>
                            <tr>
                                <th class="text-right" colspan="2">Total</th>
                                <th class="text-right">{{number_format($fees->total_fee, 2, '.','')}}</th>
                                <th class="text-right">{{number_format($fees->total_discount, 2, '.','')}}</th>
                                <th class="text-right">{{number_format($fees->total_exempt, 2, '.','')}}</th>
                                <th class="text-right">{{number_format($fees->total_paid, 2, '.','')}}</th>
                                <th class="text-right text-danger">@if($fees->balance<=0) <label class="label label-success">nothing due</label> @else {{number_format($fees->balance, 2, '.', '')}} @endif</th>
                            </tr>
                        </tfoot>

                        </tbody>
                    </table>
                </div>

                @else 
                <div class="alert alert-warning">No fees found.</div>
                @endif 

            </div>
        </div>
    </div>
    <div class="col-md-3 col-xs-12">
        @include('app.shared.feed')
    </div>
</div>
