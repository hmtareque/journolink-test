<div class="panel panel-info">
    <div class="panel-heading">
        <h3 class="panel-title">Client Details</h3>
    </div>
    <div class="panel-body">
        <div class="col-xs-12 col-md-4">
            <div class="panel panel-success">
                <div class="panel-body">
                    <img class="img-responsive img-rounded" @if($client->profile_picture) src="{{$client->profile_picture}}" @else src="{{asset('img/avatar.png')}}" @endif/>
                </div>
            </div>
        </div>
        <div class="col-xs-12 col-md-8">
            <ul class="list-unstyled">
                <li><strong>Name</strong> : {{$client->first_name}} {{$client->middle_name}}  <strong>{{$client->last_name}}</strong></li>
                <li><strong>Date of Birth</strong> : {{date('d M Y', strtotime($client->date_of_birth))}}</li>
                <li>
                    <strong>Gender</strong> : 
                    @if($client->gender == 'male') 
                    <i class="fa fa-male fa-fw text-danger"></i>Male
                    @elseif($client->gender == 'female') 
                    <i class="fa fa-female fa-fw text-danger"></i>Female
                    @elseif($client->gender == 'other') 
                    <i class="fa fa-info-circle fa-fw text-danger"></i> {{$client->other_gender}}
                    @endif 
                </li>
            </ul>

            <div class="row">
                <div class="col-xs-12 col-md-5">
                    <ul class="list-unstyled">
                        <li><strong>Address</strong></li>
                        <li>{{$client->address_line_1}}</li>
                        @if($client->address_line_2 != '')<li>{{$client->address_line_2}}</li>@endif
                        <li>{{$client->street}}</li>
                        <li>{{$client->city}}</li>
                        <li><strong>{{$client->postcode}}</strong></li>
                        <li><strong>Borough</strong> : @if($client->local_authority != '') {{$client->local_authority}} @else <label class="label label-warning">not found</label> @endif</li>
                    </ul>
                </div>
                <div class="col-xs-12 col-md-7">
                    <ul class="list-unstyled">
                        <li><strong>Contact</strong></li>
                        <li>Phone : @if($client->phone != '') {{$client->phone}} @else <label class="label label-warning">not found</label> @endif</li>
                        <li>Mobile : @if($client->mobile != '') {{$client->mobile}} @else <label class="label label-warning">not found</label> @endif</li>
                        <li>Email : @if($client->email != '') <a href="mailto:{{$client->email}}">{{$client->email}}</a> @else <label class="label label-warning">not found</label> @endif</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>

