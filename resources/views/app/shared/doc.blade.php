<ul class="list-unstyled">
    <li><strong>{{$name}}</strong></li>
    @if($docs) 
    @foreach($docs->files as $key => $file) 
    <li class="margin-top-5">
        <i class="fa fa-file-o fa-fw"></i>
        <a href="{{$file['link']}}" target="_blank"> .. {{substr($file['name'], 18, 20)}}</a>
        <!-- Button trigger modal -->
        <a href="" class="label label-default" data-toggle="modal" data-target="#remove-{{$key}}-file"><i class="fa fa-close fa-fw"></i></a>
        <!-- Modal -->
        <div class="modal fade" id="remove-{{$key}}-file" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <form class="ajax-form" action="{{url('manage/docs/remove')}}" method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    <input type="hidden" name="doc" value="{{$docs->id}}"/>
                    <input type="hidden" name="file" value="{{$key}}"/>
                    <input type="hidden" name="return" value="{{$return}}"/>
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Delete File</h4>
                        </div>
                        <div class="modal-body">
                            <p>Are you sure to <strong class="text-danger">delete</strong> the <strong>{{$file['name']}}</strong> file?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </li>
    @endforeach 

    <li>
        <!-- add doc form -->
        <div class="collapse margin-top-5" id="add-{{$id}}-form">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <form class="form-horizontal ajax-form" action="{{url('manage/docs/add')}}" method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                        <input type="hidden" name="doc" value="{{$docs->id}}"/>
                        <input type="hidden" name="security" value="0"/>
                        <input type="hidden" name="return" value="{{$return}}"/>
                        <div class="form-group" id="files">
                            <div class="col-xs-12">
                                <input type="file" class="btn btn-sm btn-default" name="files[]" multiple=""/>
                                <span class="help-block hide"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <button type="submit" class="btn btn-xs btn-primary"> 
                                    <i class="fa fa-upload fa-fw"></i> Upload
                                </button>
                                <a class="btn btn-xs btn-danger" role="button" data-toggle="collapse" href="#add-{{$id}}-form" aria-expanded="false" aria-controls="collapseExample">
                                    <i class="fa fa-close fa-fw"></i> Close
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>

        <!-- remove doc modal -->
        <div class="modal fade" id="remove-{{$id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
            <div class="modal-dialog" role="document">
                <form class="ajax-form" action="{{url('manage/docs/destroy')}}" method="post">
                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    <input type="hidden" name="doc" value="{{$docs->id}}"/>
                    <input type="hidden" name="return" value="{{$return}}"/>
                    <div class="modal-content">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                            <h4 class="modal-title" id="myModalLabel">Delete Document</h4>
                        </div>
                        <div class="modal-body">
                            <p>Are you sure to <strong class="text-danger">delete</strong> all <strong>Immigration Documents</strong>?</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>

        <div class="margin-top-5">
            <button type="button" class="btn btn-success btn-xs" data-toggle="collapse" data-target="#add-{{$id}}-form">
                <i class="fa fa-plus fa-fw"></i> Add New
            </button>

            <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#remove-{{$id}}">
                <i class="fa fa-trash fa-fw"></i> Remove
            </button>
        </div>
    </li>

    @else 
    <li>
        <div class="collapse margin-top-5" id="upload-{{$id}}-form">
            <div class="panel panel-primary">
                <div class="panel-body">
                    <form class="form-horizontal ajax-form" action="{{url('manage/docs')}}" method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                        <input type="hidden" name="object_type" value="{{$object_type}}"/>
                        <input type="hidden" name="object_id" value="{{$object_id}}"/>
                        <input type="hidden" name="folder" value="{{$folder}}"/>
                        <input type="hidden" name="name" value="{{$name}}"/>
                        <input type="hidden" name="security" value="0"/>
                        <input type="hidden" name="return" value="{{$return}}"/>
                        <div class="form-group" id="files">
                            <div class="col-xs-12">
                                <input type="file" class="btn btn-sm btn-default" name="files[]" multiple=""/>
                                <span class="help-block hide"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <button type="submit" class="btn btn-xs btn-primary"> 
                                    <i class="fa fa-upload fa-fw"></i> Upload
                                </button>
                                <a class="btn btn-xs btn-danger" role="button" data-toggle="collapse" href="#upload-{{$id}}-form" aria-expanded="false" aria-controls="collapseExample">
                                    <i class="fa fa-close fa-fw"></i> Close
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <a class="btn btn-xs btn-success" role="button" data-toggle="collapse" href="#upload-{{$id}}-form" aria-expanded="false" aria-controls="collapseExample">
            <i class="fa fa-upload fa-fw"></i> Upload {{$name}}
        </a>
    </li>
    @endif
</ul>