<div class="profile">
    <div class="avatar"><a class="img-circle" href="#profile"><img src="{{avatar()}}"/></a></div>
    <div class="identity">
        <div class="name"><a href="#profile">{{user()->first_name}} {{user()->last_name}}</a></div>
        <div class="designation">{{user()->designation}}</div>
    </div>
</div>

<ul>
    <li>
        <a class="" href="{{url('/manage')}}">
            <span class="nav-parent-name"><i class="fa fa-lg fa-dashboard fa-fw"></i> Dashboard</span>
        </a>
    </li>
    
    <!--
    <li>
        <a class="" href="#beneficiaries">
            <span class="nav-parent-name"><i class="fa fa-lg fa-user-plus fa-fw"></i> Beneficiaries</span>
        </a>
    </li>
    -->
   
    @if(isset($user_service_access['education']['type']) && in_array($user_service_access['education']['type'], array('full', 'custom')))
    <li>
        <a class="nav-parent" href="#">
            <span class="nav-parent-name"><i class="fa fa-lg fa-book fa-fw"></i> Education</span> <i class="fa fa-plus pull-right"></i>
        </a>
        <ul>
            @can('read', 4)
            <li><a href="#education/schools">Schools</a></li>
            @endcan 
            
            @can('read', 5)
            <li><a href="#education/classes">Classes</a></li>
            @endcan 
            
            @can('read', 6)
            <li><a href="#education/active-students">Students</a></li>
            @endcan
            
            @can('read', 7)
            <li><a href="#education/teachers">Teachers</a></li>
            @endcan 
            
            @can('read', 8)
            <li><a href="#education/reports">Reports</a></li>
            @endcan 
            
            @can('read', 9)
            <li><a href="#education/admin">Administration</a></li>
            @endcan 
        </ul>
    </li>
    @endif
    
    
    @if(isset($user_service_access['family']['type']) && in_array($user_service_access['family']['type'], array('full', 'custom')))
    <li>
        <a class="nav-parent" href="#">
            <span class="nav-parent-name"><i class="fa fa-lg fa-users fa-fw"></i> Family</span> <i class="fa fa-plus pull-right"></i>
        </a>
        <ul>
            <li><a href="#family/clients">Clients</a></li>
            <li><a href="#family/cases">Cases</a></li>
            <li><a href="#family/activities">Activities</a></li>
            <li><a href="#family/reports">Reports</a></li>
            <li><a href="#family/admin">Administration</a></li>

        </ul>
    </li>
    @endif 
    
    
    @if(isset($user_service_access['advocacy']['type']) && in_array($user_service_access['advocacy']['type'], array('full', 'custom')))
    <li>
        <a class="nav-parent" href="#">
            <span class="nav-parent-name"><i class="fa fa-lg fa-hand-pointer-o fa-fw"></i> Advocacy</span> <i class="fa fa-plus pull-right"></i>
        </a>
        <ul>
            <li><a href="#advocacy/clients">Clients</a></li>
            <li><a href="#advocacy/cases">Cases</a></li>
            <li><a href="#advocacy/activities">Activities</a></li>
            <li><a href="#advocacy/reports">Reports</a></li>
            <li><a href="#advocacy/admin">Administration</a></li>
        </ul>
    </li>
    @endif 
    
    <li>
        <a class="nav-parent" href="#">
            <span class="nav-parent-name"><i class="fa fa-lg fa-bar-chart-o fa-fw"></i> Reports</span> <i class="fa fa-plus pull-right"></i>
        </a>
        <ul>
            <li><a href="#reports">Report 1</a></li>
            <li><a href="#reports">Report 2</a></li>
            <li><a href="#reports">Report 3</a></li>
        </ul>
    </li>
    
    <li>
        <a class="nav-parent" href="#">
            <span class="nav-parent-name"><i class="fa fa-lg fa-newspaper-o fa-fw"></i> Notices</span> <i class="fa fa-plus pull-right"></i>
        </a>
        <ul>
            <li><a href="#notices/groups">Groups</a></li>
            <li><a href="#notices">Notices</a></li>
        </ul>
    </li>
    
 
    
    @if(Auth::user()->can('read', 6) || Auth::user()->can('read', 7)) 
    <li>
        <a class="nav-parent" href="#">
            <span class="nav-parent-name"><i class="fa fa-lg fa-globe fa-fw"></i> {{trans('label.website')}}</span> <i class="fa fa-plus pull-right"></i>
        </a>
        <ul>
            <li><a href="#web/posts" title="{{trans('label.posts')}}">{{trans('label.posts')}}</a></li>
            <li><a href="#web/components" title="{{trans('label.components')}}">{{trans('label.components')}}</a></li>
            <li><a href="#web/navs" title="{{trans('label.navs')}}">{{trans('label.navs')}}</a></li>
            <li><a href="#web/links" title="{{trans('label.links')}}">{{trans('label.links')}}</a></li>
            
            <li><a href="#web/contacts" title="{{trans('label.contacts')}}">{{trans('label.contacts')}}</a></li>
            @can('read', 6)
            <li><a href="#web/settings" title="{{trans('label.settings')}}">{{trans('label.settings')}}</a></li>
            @endcan
        </ul>
    </li>
    @endif
    


    <li>
        <a class="nav-parent" href="#">
            <span class="nav-parent-name"><i class="fa fa-lg fa-cogs fa-fw"></i> Administration</span> <i class="fa fa-plus pull-right"></i>
        </a>
        <ul>
            <li><a href="#users">{{trans('label.users')}}</a></li>
            <li><a href="#roles">{{trans('label.roles')}}</a></li>
            
            <li><a href="#payment-plans">Payment Plans</a></li>
         <!--   <li><a href="#assessments">Assessments</a></li> -->
        </ul>
    </li>
    
    <li>
        <a class="" href="#resources">
            <span class="nav-parent-name"><i class="fa fa-lg fa-cubes fa-fw"></i> Resources</span>
        </a>
    </li>
    
    @if(in_array(Auth::user()->email, config('dev.emails'))) 
    <li>
        <a class="" href="#configs">
            <span class="nav-parent-name"><i class="fa fa-lg fa-puzzle-piece fa-fw"></i> {{trans('label.configurations')}}</span>
        </a>
    </li>
    @endif 
</ul>


