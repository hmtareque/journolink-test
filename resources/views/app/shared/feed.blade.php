<h4 class="section-title">Recent Activity</h4>

<div class="panel panel-success">
    <div class="panel-body">
         Jack Smith opened a new case for Mitila Mish at 12/09/2016 12.30pm
    </div>
</div>

<div class="panel panel-success">
    <div class="panel-body">
        Rabada Micklam register attendance for class 2 at 08/04/2016 4.20pm
    </div>
</div>

<div class="panel panel-success">
    <div class="panel-body">
        Mental health assessment taken for Donald Trump by Nitish Rana at 09/05/2016 9.30am
    </div>
</div>

<div class="panel panel-success">
    <div class="panel-body">
        New client created by Kate Duffy at 03/01/2016 11.09am 
    </div>
</div>

<div class="panel panel-success">
    <div class="panel-body">
        New new student enrolled in class 5 by Emily Beckwith at 03/02/2016 11.09am 
    </div>
</div>

