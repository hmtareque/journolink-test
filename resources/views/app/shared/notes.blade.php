<div class="panel panel-info">
    <div class="panel-heading">
        <h3 class="panel-title">Notes</h3>
    </div>
    <div class="panel-body">
        @if($notes->count()>0) 
        @foreach($notes as $note) 
        <div class="panel panel-default">

            <div class="panel-body">
                <p>{{$note->note}}<p>
                    <small class="text-primary">
                        Created By {{ username($note->created_by)}} At {{date('d M Y g:i a', strtotime($note->created_at))}}
                        @if($note->updated_by)
                        and Updated by {{ username($note->updated_by)}} At {{date('d M Y g:i a', strtotime($note->updated_at))}}
                        @endif 
                    </small>
            </div>
            
            @can("update", $module_id)
            <div class="panel-footer">
                <div class="row">
                    <div class="col-xs-12">
                        
                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-primary btn-xs" data-toggle="modal" data-target="#note-{{$note->id}}-edit">
                            <i class="fa fa-edit fa-fw"></i>edit
                        </button>

                        <!-- Button trigger modal -->
                        <button type="button" class="btn btn-danger btn-xs" data-toggle="modal" data-target="#note-{{$note->id}}-remove">
                            <i class="fa fa-trash fa-fw"></i>remove
                        </button>


                        <!-- Update Note Modal -->
                        <div class="modal fade" id="note-{{$note->id}}-edit" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <form class="ajax-form" action="{{url('manage/notes/'.$note->id)}}" method="post">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                    <input type="hidden" name="_method" value="put"/>
                                    <input type="hidden" name="return" value="{{$return}}"/>
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Update Note</h4>
                                        </div>
                                        <div class="modal-body">
                                            <div class="form-group" id="note">
                                                <textarea name="note" class="form-control">{{$note->note}}</textarea>
                                                <span class="help-block hide"></span>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-primary btn-sm">Update</button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>

                        <!-- Update Note Modal -->
                        <div class="modal fade" id="note-{{$note->id}}-remove" tabindex="-1" role="dialog" aria-labelledby="myModalLabel">
                            <div class="modal-dialog" role="document">
                                <form class="ajax-form" action="{{url('manage/notes/'.$note->id)}}" method="post">
                                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                    <input type="hidden" name="_method" value="delete"/>
                                    <input type="hidden" name="return" value="{{$return}}"/>
                                    <div class="modal-content">
                                        <div class="modal-header">
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                            <h4 class="modal-title" id="myModalLabel">Delete Note</h4>
                                        </div>
                                        <div class="modal-body">
                                            <p>Are you sure to <strong class="text-danger">delete</strong> the following note?</p>
                                            <p>{{$note->note}}</p>
                                        </div>
                                        
                                        <div class="modal-footer">
                                            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">Close</button>
                                            <button type="submit" class="btn btn-danger btn-sm">Delete</button>
                                        </div>
                                       
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            @endif
        </div>
        @endforeach 
        @else 
        <p class="alert alert-warning">No note found!</p>
        @endif 

        <div class="collapse margin-top-5" id="note-form">
            <div class="panel panel-primary">
                <div class="panel-heading">Create New Note</div>
                <div class="panel-body">
                    <form class="form-horizontal ajax-form" action="{{url('manage/notes')}}" method="post">
                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                        <input type="hidden" name="object_type" value="{{$object_type}}"/>
                        <input type="hidden" name="object_id" value="{{$object_id}}"/>
                        <input type="hidden" name="return" value="{{$return}}"/>
                        <div class="form-group" id="note">
                            <div class="col-xs-12">
                                <textarea name="note" class="form-control"></textarea>
                                <span class="help-block hide"></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <div class="col-xs-12">
                                <button type="submit" class="btn btn-primary btn-xs"> 
                                    <i class="fa fa-save fa-fw"></i> Save
                                </button>
                                <a class="btn btn-xs btn-danger" role="button" data-toggle="collapse" href="#note-form" aria-expanded="false" aria-controls="collapseExample">
                                    <i class="fa fa-close fa-fw"></i> close
                                </a>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    @can("update", $module_id)
    <div class="panel-footer">
        <a class="btn btn-xs btn-success" role="button" data-toggle="collapse" href="#note-form" aria-expanded="false" aria-controls="collapseExample">
            <i class="fa fa-plus fa-fw"></i> Create New Note
        </a>
    </div>
    @endif
</div>