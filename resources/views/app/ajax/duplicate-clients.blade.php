<div class="panel panel-primary margin-top-10">
    <div class="panel-heading">
        <h3 class="panel-title">Similar data found</h3>
    </div>
    <div class="panel-body">
        @if($clients->count()>0)

        <div class="table-responsive">
            <table class="table @if($clients->count()>10) data-table @endif table-striped table-condensed table-bordered table-hover" width="100%">
                <thead>
                    <tr>
                        <th>Name</th>
                        <th>Ref. No.</th>
                        <th>Contact</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($clients as $client) 
                    <tr>
                        <td>{{$client->first_name}} {{$client->middle_name}}<strong>{{$client->last_name}}</strong></td>
                        <td>{{$client->ref_no}}</td>
                        <td>{{$client->date_of_birth}}</td>
                        <td>@if(engaged($client->id, 'education')) 
                            <a href="#education/students/{{$client->id}}">select</a>               
                            @else 
                            <span class="btn btn-xs btn-success check-identity" data-id="{{$client->id}}" data-redirect="education/students/{{$client->id}}">select</span>
                            @endif 
                        </td>
                    </tr>
                    @endforeach 
                </tbody>
            </table>
        </div>

        <div id="check_duplicate">
            <input type="hidden" name="has_duplicate" value="1"/>
            <div class="checkbox">
                <label><input type="checkbox" name="check_duplicate" value="1"/> Please confirm it is not duplicate.</label>
            </div>
            <span class="help-block hide"></span>
        </div>

        @endif
    </div>
</div>