@extends('app.layouts.main')

@section('content')

<div class="row">
    <div class="col-xs-12 col-md-8">
        <div class="row"><div class="col-xs-12"><div id="chart_div"></div></div></div>
        <div class="row"><div class="col-xs-12"><div id="piechart"></div></div></div>
        <div class="row"><div class="col-xs-12"><div id="curve_chart"></div></div></div>
    </div>
    <div class="col-xs-12 col-md-4">
        @include('app.shared.feed')
    </div>
</div>




@endsection 

