<div class="header">
    <div class="title"><i class="fa fa-lg fa-book fa-fw"></i> Case Assessment</div>
    <div class="links">
        <a href="#advocacy/cases/1" class="btn btn-success btn-sm"><i class="fa fa-info-circle fa-fw"></i> Case Info</a>
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Activities <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#advocacy/activities"><i class="fa fa-list"></i> All Activities</a></li>
                <li><a href="#advocacy/activities/create"><i class="fa fa-plus"></i> Create New Activity</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-8 col-xs-12">
        
         <form>
        
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Update Assessment</h3>
            </div>
            <div class="panel-body">

                   <div class="form-group">
                       <label for="">Knowing how seek help</label>
                       <div class="radio">
                           <label>
                               <input type="radio" name="optionsRadios0" id="optionsRadios1" value="option1" checked>
                               Beginner 
                           </label>
                       </div>
                       <div class="radio">
                           <label>
                               <input type="radio" name="optionsRadios0" id="optionsRadios2" value="option2">
                               Intermediate
                           </label>
                       </div>
                       <div class="radio">
                           <label>
                               <input type="radio" name="optionsRadios0" id="optionsRadios3" value="option3">
                               Expert
                           </label>
                       </div>
                   </div>
                
                   <div class="form-group">
                       <label for="">Opening Bank Account</label>
                       <div class="radio">
                           <label>
                               <input type="radio" name="optionsRadios1" id="optionsRadios1" value="option1" checked>
                               Beginner 
                           </label>
                       </div>
                       <div class="radio">
                           <label>
                               <input type="radio" name="optionsRadios1" id="optionsRadios2" value="option2">
                               Intermediate
                           </label>
                       </div>
                       <div class="radio">
                           <label>
                               <input type="radio" name="optionsRadios1" id="optionsRadios3" value="option3">
                               Expert
                           </label>
                       </div>
                   </div>
                
                   <div class="form-group">
                       <label for="">Reading and understanding bank statement</label>
                       <div class="radio">
                           <label>
                               <input type="radio" name="optionsRadios2" id="optionsRadios1" value="option1" checked>
                               Beginner 
                           </label>
                       </div>
                       <div class="radio">
                           <label>
                               <input type="radio" name="optionsRadios2" id="optionsRadios2" value="option2">
                               Intermediate
                           </label>
                       </div>
                       <div class="radio">
                           <label>
                               <input type="radio" name="optionsRadios2" id="optionsRadios3" value="option3">
                               Expert
                           </label>
                       </div>
                   </div>
                
                   <div class="form-group">
                       <label for="">Applying for a National Insurance Number</label>
                       <div class="radio">
                           <label>
                               <input type="radio" name="optionsRadios3" id="optionsRadios1" value="option1" checked>
                               Beginner 
                           </label>
                       </div>
                       <div class="radio">
                           <label>
                               <input type="radio" name="optionsRadios3" id="optionsRadios2" value="option2">
                               Intermediate
                           </label>
                       </div>
                       <div class="radio">
                           <label>
                               <input type="radio" name="optionsRadios3" id="optionsRadios3" value="option3">
                               Expert
                           </label>
                       </div>
                   </div>
                
                   <div class="form-group">
                       <label for="">Applying for Income Support/Job Seekers Allowance and Housing Benefit</label>
                       <div class="radio">
                           <label>
                               <input type="radio" name="optionsRadios4" id="optionsRadios1" value="option1" checked>
                               Beginner 
                           </label>
                       </div>
                       <div class="radio">
                           <label>
                               <input type="radio" name="optionsRadios4" id="optionsRadios2" value="option2">
                               Intermediate
                           </label>
                       </div>
                       <div class="radio">
                           <label>
                               <input type="radio" name="optionsRadios4" id="optionsRadios3" value="option3">
                               Expert
                           </label>
                       </div>
                   </div>
        

                
            </div>
            
            <div class="panel-footer">
                <button type="submit" class="btn btn-primary"> 
                        <i class="fa fa-refresh fa-fw"></i> Update
                    </button>
            </div>
        </div>
             
             </form>
    </div>
    <div class="col-md-4 col-xs-12">
        @include('app.shared.feed')
    </div>
</div>




