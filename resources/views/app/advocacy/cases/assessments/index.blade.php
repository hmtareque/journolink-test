<div class="header">
    <div class="title"><i class="fa fa-lg fa-book fa-fw"></i> Case Assessment</div>
    <div class="links">
        <a href="#advocacy/cases/create" class="btn btn-success btn-sm"><i class="fa fa-info-circle fa-fw"></i> Case Info</a>
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Activities <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#advocacy/activities"><i class="fa fa-list"></i> All Activities</a></li>
                <li><a href="#advocacy/activities/create"><i class="fa fa-plus"></i> Create New Activity</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-9 col-xs-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Cases Assessments</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th width="70%">Assessment</th>
                                <th width="15%">Created At</th>
                                <th width="15%">Created By</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><a href="#advocacy/cases/1/assessments/1">Personal Circumstance Assessment</a></td>
                                <td>23/09/2016</td>
                                <td>Abul Kher</td>
                            </tr>
                            <tr>
                                <td><a href="#advocacy/cases/1/assessments/1">Mental Health Assessment</a></td>
                                <td>27/10/2016</td>
                                <td>Jack Smith</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-3 col-xs-12">
        @include('app.shared.feed')
    </div>
</div>




