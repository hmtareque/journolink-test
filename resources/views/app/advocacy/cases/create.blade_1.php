<div class="header">
    <div class="title"><i class="fa fa-lg fa-book fa-fw"></i> New Case</div>
    <div class="links">
        <a href="#advocacy/cases" class="btn btn-success btn-sm"><i class="fa fa-list-alt fa-fw"></i> All Cases</a>
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Activities <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#advocacy/activities"><i class="fa fa-list"></i> All Activities</a></li>
                <li><a href="#advocacy/activities/create"><i class="fa fa-plus"></i> Create New Activity</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-8">
        <form class="form-horizontal ajax-form" action="{{url('manage/education/students')}}" method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            
            
            <h4 class="section-title">Case Information</h4>
            
            <div class="form-group" id="service">
                <label class="col-sm-3 control-label">Service</label>
                <div class="col-sm-9">
                    <select class="form-control" name="service">
                        <option value=""> -- Please Select -- </option>
                        <option value="1">Youth</option>
                        <option value="1">Housing</option>
                        <option value="1">Employability</option>
                        <option value="1">Parenting</option>
                    </select>
                </div>
            </div>
            
            <div class="form-group" id="category">
                <label class="col-sm-3 control-label">Category</label>
                <div class="col-sm-9">
                    <select class="form-control" name="category">
                        <option value=""> -- Please Select -- </option>
                        <option value="1">Money</option>
                        <option value="1">Health</option>
                        <option value="1">Education</option>
                        <option value="1">Life</option>
                    </select>
                </div>
            </div>
            
            <div class="form-group" id="type">
                <label class="col-sm-3 control-label">Type</label>
                <div class="col-sm-9">
                    <select class="form-control" name="type">
                        <option value=""> -- Please Select -- </option>
                    </select>
                </div>
            </div>
            
            <div class="form-group" id="description">
                <label class="col-sm-3 control-label">Description</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="description"></textarea>
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            

            <h4 class="section-title">Personal Information</h4>

            <div class="form-group" id="first_name">
                <label class="col-sm-3 control-label">First Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="first_name" placeholder="First Name">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="middle_name">
                <label class="col-sm-3 control-label">Middle Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="middle_name" placeholder="Middle Name">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="last_name">
                <label class="col-sm-3 control-label">Last Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="last_name" placeholder="Last Name">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="date_of_birth">
                <label class="col-sm-3 control-label">Date of Birth</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control jdob" name="date_of_birth" placeholder="Date of Birth">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="gender">
                <label class="col-sm-3 control-label">Gender</label>
                <div class="col-sm-9">
                    <select class="form-control" name="gender">
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                        <option value="other">Other</option>
                    </select>
                    <div id="other_gender">
                        <input type="text" class="form-control hide margin-top-5" name="other_gender" placeholder="Other Gender">
                        <span class="help-block hide"></span>
                    </div>
                </div>
            </div>

            <h4 class="section-title">Address</h4>

            <div class="form-group" id="address_line_1">
                <label class="col-sm-3 control-label">Address Line 1</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="address_line_1" placeholder="Address Line 1">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="address_line_2">
                <label class="col-sm-3 control-label">Address Line 2</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="address_line_2" placeholder="Address Line 2">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="street">
                <label class="col-sm-3 control-label">Street</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="street" placeholder="Street">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="city">
                <label class="col-sm-3 control-label">City</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="city" placeholder="City">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="postcode">
                <label class="col-sm-3 control-label">Postcode</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="postcode" placeholder="Postcode">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="borough">
                <label class="col-sm-3 control-label">Borough</label>
                <div class="col-sm-9">
                    <select class="form-control" name="borough">
                        <option value=""> -- Please Select -- </option>
                        @foreach($boroughes as $borough)
                        <option value="{{trim($borough->name)}}">{{$borough->name}}</option>
                        @endforeach 
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <h4 class="section-title">Contacts</h4>

            <div class="form-group" id="phone">
                <label class="col-sm-3 control-label">Phone</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="phone" placeholder="Phone">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="email">
                <label class="col-sm-3 control-label">Email Address</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="email" placeholder="Email Address">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="mobile">
                <label class="col-sm-3 control-label">Mobile</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="mobile" placeholder="Mobile">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <h4 class="section-title">Next of Kin</h4>

            <div class="form-group" id="next_of_kin">
                <label class="col-sm-3 control-label">Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="next_of_kin" placeholder="Next of Kin">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="next_of_kin_relationship">
                <label class="col-sm-3 control-label">Relationship</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="next_of_kin_relationship" placeholder="Relationship">
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            <div class="form-group" id="next-of-kin-address-same-as-main">
                <label class="col-sm-3 control-label">Address Same as Main</label>
                <div class="col-sm-9">
                    <select class="form-control" id="next-of-kin-address" name="next_of_kin_address_same_as_main">
                        <option value="1">Yes</option>
                        <option value="0">Not</option>
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            <div class="hide" id="next-of-kin-address-data">

            <div class="form-group" id="next_of_kin_address_line_1">
                <label class="col-sm-3 control-label">Address Line 1</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="next_of_kin_address_line_1" placeholder="Address Line 1">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="next_of_kin_address_line_2">
                <label class="col-sm-3 control-label">Address Line 2</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="next_of_kin_address_line_2" placeholder="Address Line 2">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="next_of_kin_street">
                <label class="col-sm-3 control-label">Street</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="next_of_kin_street" placeholder="Street">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="next_of_kin_city">
                <label class="col-sm-3 control-label">City</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="next_of_kin_city" placeholder="City">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="next_of_kin_postcode">
                <label class="col-sm-3 control-label">Postcode</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="next_of_kin_postcode" placeholder="Postcode">
                    <span class="help-block hide"></span>
                </div>
            </div>
                
            </div>

            <div class="form-group" id="next_of_kin_phone">
                <label class="col-sm-3 control-label">Phone</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="next_of_kin_phone" placeholder="Phone">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="next_of_kin_email">
                <label class="col-sm-3 control-label">Email Address</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="next_of_kin_email" placeholder="Email Address">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="next_of_kin_mobile">
                <label class="col-sm-3 control-label">Mobile</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="next_of_kin_mobile" placeholder="Mobile">
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            <h4 class="section-title">Local Authority</h4>
                 
                 <div class="form-group" id="local_authority">
                    <label class="col-sm-3 control-label">Local Authority</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="local_authority" placeholder="Local Authority">
                        <span class="help-block hide"></span>
                    </div>
                </div>
                 
                 <div class="form-group" id="allergies">
                    <label class="col-sm-3 control-label">Social Worker</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="social_worker" placeholder="Social Worker">
                        <input type="text" class="form-control margin-top-5" name="social_worker_contact" placeholder="Social Worker's Contact">
                        <span class="help-block hide"></span>
                    </div>
                </div>

            <h4 class="section-title">Immigration</h4>

            <div class="form-group" id="born_in_the_uk">
                <label class="col-sm-3 control-label"></label>
                <div class="col-sm-9">
                    <div class="checkbox">
                        <label><input type="checkbox" id="check-born-in-the-uk" name="born_in_the_uk" value="1"/> Born in the UK</label>
                    </div>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="arrived_in_the_uk">
                <label class="col-sm-3 control-label">Arrived in the UK</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control jtoday" name="arrived_in_the_uk" placeholder="Arrived in the UK">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="immigration_status">
                <label class="col-sm-3 control-label">Immigration Status</label>
                <div class="col-sm-9">
                    <select class="form-control" name="immigration_status">
                        <option value=""> -- Please Select -- </option>
                        @foreach($immigration_statuses as $status)
                        <option value="{{$status->name}}">{{$status->name}}</option>
                        @endforeach 
                        <option value="other">Other</option>
                    </select>
                    <div id="other_immigration_status">
                        <input type="text" class="form-control hide margin-top-5" name="other_immigration_status" placeholder="Other Immigration Status">
                        <span class="help-block hide"></span>
                    </div>
                </div>
            </div>

            <div class="form-group" id="immigration_note">
                <label class="col-sm-3 control-label">Note</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="immigration_note"></textarea>
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            

            <h4 class="section-title">Language</h4>

            <div class="form-group" id="first_language">
                <label class="col-sm-3 control-label">First Language</label>
                <div class="col-sm-9">
                    <select class="form-control" name="first_language">
                        <option value=""> -- Please Select -- </option>
                        @foreach(languages() as $language)
                        <option value="{{$language}}">{{$language}}</option>
                        @endforeach 
                        <option value="other">Other</option>
                    </select>
                    <div id="other_first_language">
                        <input type="text" class="form-control hide margin-top-5" name="other_first_language" placeholder="Other First Language">
                        <span class="help-block hide"></span>
                    </div>
                </div>
            </div>
            
             <div class="form-group" id="level_of_esol">
                    <label class="col-sm-3 control-label">Level of ESOL</label>
                    <div class="col-sm-9">
                       <select class="form-control" name="level_of_esol">
                            <option>Below</option>
                            <option>Inline</option>
                            <option>Above</option>
                        </select>
                        <span class="help-block hide"></span>
                    </div>
                </div>
            
            <div class="form-group" id="interpreter_required">
                    <label class="col-sm-3 control-label">Interpreter Required</label>
                    <div class="col-sm-9">
                       <select class="form-control" name="interpreter_required">
                            <option value="0">No</option>
                            <option value="1">Yes</option>
                        </select>
                        <span class="help-block hide"></span>
                    </div>
                </div>
                 
                 <div class="form-group" id="interpret_language">
                    <label class="col-sm-3 control-label">Interpreter Language</label>
                    <div class="col-sm-9">
                         <select class="form-control" name="interpret_language">
                        <option value=""> -- Please Select -- </option>
                        @foreach(languages() as $language)
                        <option value="{{$language}}">{{$language}}</option>
                        @endforeach 
                         </select>
                        <span class="help-block hide"></span>
                    </div>
                </div>
            
            
             <h4 class="section-title">Education</h4>
                 
                 <div class="form-group" id="educational_institute">
                    <label class="col-sm-3 control-label">Current Institute</label>
                    <div class="col-sm-9">
                        <input type="text" class="form-control" name="educational_institute" placeholder="Institute">
                        <span class="help-block hide"></span>
                    </div>
                </div>
                 
                 <div class="form-group" id="immigration_note">
                    <label class="col-sm-3 control-label">Note</label>
                    <div class="col-sm-9">
                        <textarea class="form-control" name="educational_note"></textarea>
                        <span class="help-block hide"></span>
                    </div>
                </div>

            <h4 class="section-title">Race & Religion</h4>

            <div class="form-group" id="ethnicity">
                <label class="col-sm-3 control-label">Ethnicity</label>
                <div class="col-sm-9">
                    <select class="form-control" name="ethnicity">
                        <option value=""> -- Please Select -- </option>
                        @foreach(nationalities() as $ethnicity)
                        <option value="{{$ethnicity}}">{{$ethnicity}}</option>
                        @endforeach 
                        <option value="other">Other</option>
                    </select>
                    <div id="other_ethnicity">
                        <input type="text" class="form-control hide margin-top-5" name="other_ethnicity" placeholder="Other Ethnicity">
                        <span class="help-block hide"></span>
                    </div>
                </div>
            </div>
            
            <div class="form-group" id="religion">
                    <label class="col-sm-3 control-label">Religion</label>
                    <div class="col-sm-9">
                        <select class="form-control" name="religion">
                            <option value=""> -- Please Select -- </option>
                            <option>Christian</option>
                            <option>Muslim</option>
                            <option>Jew</option>
                            <option>Hindu</option>
                            <option value="other">Other</option>
                        </select>
                        
                        <input type="text" class="form-control margin-top-5 hide" name="other_religion" placeholder="Other">
                        <span class="help-block hide"></span>
                    </div>
                </div>
            
           

            <h4 class="section-title">Health Information</h4>

            <div class="form-group" id="medical_conditions">
                <label class="col-sm-3 control-label">Medical Conditions</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="medical_conditions"></textarea>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="medical_condition_docs">
                <label class="col-sm-3 control-label">Medical Documents</label>
                <div class="col-sm-9">
                    <input type="file" class="btn btn-sm btn-default" name="medical_condition_docs[]" multiple=""/>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="allergies">
                <label class="col-sm-3 control-label">Allergies</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="allergies"></textarea>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="allergy_docs">
                <label class="col-sm-3 control-label">Allergy Documents</label>
                <div class="col-sm-9">
                    <input type="file" class="btn btn-sm btn-default" name="allergy_docs[]" multiple=""/>
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            
            <div class="form-group" id="has_disability">
                    <label class="col-sm-3 control-label">Has Disability</label>
                    <div class="col-sm-9">
                       <select class="form-control" name="has_disability">
                            <option value="0">No</option>
                            <option value="1">Yes</option>
                        </select>
                        <span class="help-block hide"></span>
                    </div>
                </div>
            
            <div class="form-group" id="disability">
                <label class="col-sm-3 control-label">Disability</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="disability"></textarea>
                    <span class="help-block hide"></span>
                </div>
            </div>

            

            <h4 class="section-title">Payments</h4>

            <div class="form-group" id="payment_description">
                <label class="col-sm-3 control-label">Description</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="payment_description" placeholder="Payment Description" value="Family service fee">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="fee">
                <label class="col-sm-3 control-label">Fee</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control text-right" name="fee" placeholder="Fee Amount" value="">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="exempt_from_fee">
                <label class="col-sm-3 control-label">Exempt from fee</label>
                <div class="col-sm-9">
                    <select class="form-control" id="exempt-from-fee" name="exempt_from_fee">
                        <option value="0">No</option>
                        <option value="1">Yes</option>
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="discount">
                <label class="col-sm-3 control-label">Discount</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control text-right" name="discount" placeholder="Discount" value="">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="paid">
                <label class="col-sm-3 control-label">Paid</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control text-right" name="paid" placeholder="Paid" value="">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="payment_note">
                <label class="col-sm-3 control-label">Note</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="payment_note"></textarea>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="payment_documents">
                <label class="col-sm-3 control-label">Documents</label>
                <div class="col-sm-9">
                    <input type="file" class="btn btn-sm btn-default" name="payment_docs[]" multiple=""/>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <h4 class="section-title">Consents</h4>
            <div class="form-group" id="has_consent_to_appear_in_publicity_photos">
                <label class="col-sm-3 control-label"></label>
                <div class="col-sm-9">
                    <div class="checkbox">
                        <label><input type="checkbox" name="has_consent_to_appear_in_publicity_photos" value="1" checked=""/> Has consent to appear in publicity photos</label>
                    </div>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="has_consent_to_appear_in_publicity_videos">
                <label class="col-sm-3 control-label"></label>
                <div class="col-sm-9">
                    <div class="checkbox">
                        <label><input type="checkbox" name="has_consent_to_appear_in_publicity_videos" value="1" checked=""/> Has consent to appear in publicity videos</label>
                    </div>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="subscribed_newsletter">
                <label class="col-sm-3 control-label"></label>
                <div class="col-sm-9">
                    <div class="checkbox">
                        <label><input type="checkbox" name="subscribe_newsletter" value="1" checked=""/> Subscribe Newsletter</label>
                    </div>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <h4 class="section-title">About Us</h4>
            <div class="form-group" id="heard_about_us_by">
                <label class="col-sm-3 control-label">Heard about us by</label>
                <div class="col-sm-9">
                    <select class="form-control" name="heard_about_us_by">
                        <option value=""> -- Please Select -- </option>
                        @foreach($heard_about_us_by as $source)
                        <option value="{{$source->name}}">{{$source->name}}</option>
                        @endforeach 
                        <option value="other">Other</option>
                    </select>
                    <div id="other_heard_about_us_by">
                        <input type="text" class="form-control hide margin-top-5" name="other_heard_about_us_by" placeholder="Other Source">
                        <span class="help-block hide"></span>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-primary"> 
                        <i class="fa fa-save fa-fw"></i> Save
                    </button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-xs-12 col-md-4">
        @include('app.shared.feed')
    </div>
</div>

