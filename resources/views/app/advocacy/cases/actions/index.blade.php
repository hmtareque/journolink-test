<div class="header">
    <div class="title"><i class="fa fa-lg fa-book fa-fw"></i> Case Actions</div>
    <div class="links">
        @can("create", 6)
        <a href="#advocacy/cases/1" class="btn btn-success btn-sm"><i class="fa fa-info-circle fa-fw"></i> Case Info</a>
        @endcan 
        
        @if(Auth::user()->can("read", 6) || Auth::user()->can("create", 6))
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
               Cases <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#advocacy/cases"><i class="fa fa-list"></i> All Cases</a></li>
                @can("create", 7)
                <li><a href="#advocacy/cases/create"><i class="fa fa-plus"></i> Create New Case</a></li>
                @endcan 
            </ul>
        </div>
        @endif
    </div>
</div>

<div class="row">
    <div class="col-md-9 col-xs-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Actions</h3>
            </div>
            <div class="panel-body">
               
                <table class="table data-table table-striped table-bordered table-condensed table-hover" width="100%">
                    <thead>
                        <tr>
                            <th width="55%">Action Taken</th>
                            <th width="15%">Time Spent</th>
                            <th width="15%" class="text-center">Created By</th>
                            <th width="15%">Action</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <td>
                                The founder Chris Kahler showed me a map with pulsing lights showing responses coming in from smartphone users across the UK. "It's a very simple idea," he says. "The difficult part is deciding who to serve the questions to and how to make sense of the results when you get them back.
                            </td>
                            <td>45 min</td>
                            <td>
                                Hasan Tareque<br/>
                                12/09/2017 7.09pm
                            </td>
                            <td>
                                <a href="" class="btn btn-xs btn-primary btn-block"><i class="fa fa-edit fa-fw"></i> edit</a>
                                <a href="" class="btn btn-xs btn-danger btn-block margin-top-2"><i class="fa fa-trash fa-fw"></i> delete</a>
                            </td>
                        </tr>
                        <tr>
                            <td>
                                That is where machine learning comes in, with the system teaching itself to assess the likely background of someone agreeing to take part in a survey. Like Essencient, Chris Kahler claims to have successfully predicted the outcome of the EU referendum and the US presidential election.
                            </td>
                            <td>45 min</td>
                            <td>
                                Jane Soymore<br/>
                                15/09/2016 3.30pm
                            </td>
                            <td>
                                   <a href="" class="btn btn-xs btn-primary btn-block"><i class="fa fa-edit fa-fw"></i> edit</a>
                                <a href="" class="btn btn-xs btn-danger btn-block margin-top-2"><i class="fa fa-trash fa-fw"></i> delete</a>
                            
                            </td>
                        </tr>
                        <tr>
                            <td>
                                And his technology seems to have convinced both investors and customers. Qriously has been backed by the venture capital firm which put money behind the likes of Twitter and Oculus and it was hired a a few weeks ago to predict the results of the French presidential election.
                            </td>
                            <td>45 min</td>
                            <td>
                                Cacias Clay<br/>
                                09/02/2015 9.37am
                            </td>
                            <td>
                                <a href="" class="btn btn-xs btn-primary btn-block"><i class="fa fa-edit fa-fw"></i> edit</a>
                                <a href="" class="btn btn-xs btn-danger btn-block margin-top-2"><i class="fa fa-trash fa-fw"></i> delete</a>
                            </td>
                        </tr>
                    </tbody>
                </table>
                
            </div>
        </div>
        
        
        
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Action Summary</h3>
            </div>
            <div class="panel-body">
                
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>Action Types</th>
                                <th class="text-center">Taken</th>
                                <th class="text-right">Time Spent (min)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>One to One Session</td>
                                <td class="text-center">2</td>
                                <td class="text-right">70min</td>
                            </tr>

                            <tr>
                                 <td>Weekly Football Activity</td>
                                <td class="text-center">5</td>
                                <td class="text-right">30min</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Total</th>
                                <th class="text-center">7</th>
                                <th class="text-right">1 hour 40 min</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                
            </div>
        </div>
        
        
        
        
    </div>

    <div class="col-xs-12 col-md-3">
        @include('app.shared.feed')
    </div>
</div>