<div class="header">
    <div class="title"><i class="fa fa-lg fa-book fa-fw"></i> New Case</div>
    <div class="links">
        <a href="#advocacy/clients/create" class="btn btn-success btn-sm"><i class="fa fa-plus fa-fw"></i> Create New Client</a>
        <a href="#advocacy/cases" class="btn btn-primary btn-sm"><i class="fa fa-list-alt fa-fw"></i> All Cases</a>
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Activities <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#advocacy/activities"><i class="fa fa-list"></i> All Activities</a></li>
                <li><a href="#advocacy/activities/create"><i class="fa fa-plus"></i> Create New Activity</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-8">

        <h4 class="section-title">Client</h4>
        <form class="form-inline ajax-form" action="{{url('manage/education/students')}}" method="post">
            <div class="row">
                <div class="col-sm-3 text-right"> <label class="control-label">Search</label></div>
                <div class="col-sm-9">
                    <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                    <div class="form-group" id="first_name">
                        <input type="text" class="form-control" name="first_name" placeholder="Search">
                        <span class="help-block hide"></span>
                    </div>
                    <div class="form-group">
                        <button type="submit" class="btn btn-success"> 
                            <i class="fa fa-search fa-fw"></i> Search
                        </button>
                    </div>
                </div>
            </div>
        </form>

        <div class="panel panel-primary margin-top-10">
            <div class="panel-heading">
                <h3 class="panel-title">Search Result</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table data-table table-striped table-condensed table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>Ref. No.</th>
                                <th>Name</th>
                                <th>Date of Birth</th>
                                <th>Select</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td><a href="#education/students/1"><strong>HT0120170001M</strong></a></td>
                                <td>Hasan <strong>Tareque</strong></td>
                                <td>17/09/2011</td>
                                <td><a href="" class="btn btn-success btn-xs"><i class="fa fa-check fa-fw"></i>select</a></td>
                            </tr>

                            <tr>
                                <td><a href="#education/students/1"><strong>FB0120170002F</strong></a></td>
                                <td>Fatima <strong>Basit</strong></td>
                                <td>03/11/1998</td>
                                <td><a href="" class="btn btn-success btn-xs"><i class="fa fa-check fa-fw"></i>select</a></td>

                            </tr>

                            <tr>
                                <td><a href="#education/students/1"><strong>BN0120170003F</strong></a></td>
                                <td>Behnaz <strong>Nessar</strong></td>
                                <td>05/08/1999</td>
                                <td><a href="" class="btn btn-success btn-xs"><i class="fa fa-check fa-fw"></i>select</a></td>

                            </tr>

                            <tr>
                                <td><a href="#education/students/1"><strong>ML0120170004M</strong></a></td>
                                <td>Malgorzata <strong>Lipka</strong></td>
                                <td>02/07/1987</td>
                                <td><a href="" class="btn btn-success btn-xs"><i class="fa fa-check fa-fw"></i>select</a></td>

                            </tr>

                            <tr>
                                <td><a href="#education/students/1"><strong>NB0120170005F</strong></a></td>
                                <td>Norin <strong>Bakhshzaad</strong></td>
                                <td>17/09/1995</td>
                                <td><a href="" class="btn btn-success btn-xs"><i class="fa fa-check fa-fw"></i>select</a></td>

                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>




        <form class="form-horizontal ajax-form" action="{{url('manage/education/students')}}" method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>


            <h4 class="section-title">Case Information</h4>

            <div class="form-group" id="service">
                <label class="col-sm-3 control-label">Service</label>
                <div class="col-sm-9">
                    <select class="form-control" name="service">
                        <option value=""> -- Please Select -- </option>
                        <option value="1">Community Advocacy Service</option>
                        <option value="1">Immigration Advice Service</option>
                        <option value="1">Mental Health Service</option>
                    </select>
                </div>
            </div>

            <div class="form-group" id="category">
                <label class="col-sm-3 control-label">Category</label>
                <div class="col-sm-9">
                    <select class="form-control" name="category">
                        <option value=""> -- Please Select -- </option>
                        <option value="1">Counselling</option>
                        <option value="1">Mental Health</option>
                    </select>
                </div>
            </div>

            <div class="form-group" id="type">
                <label class="col-sm-3 control-label">Type</label>
                <div class="col-sm-9">
                    <select class="form-control" name="type">
                        <option value=""> -- Please Select -- </option>
                    </select>
                </div>
            </div>

            <div class="form-group" id="description">
                <label class="col-sm-3 control-label">Description</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="description"></textarea>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-primary"> 
                        <i class="fa fa-save fa-fw"></i> Save
                    </button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-xs-12 col-md-4">
        @include('app.shared.feed')
    </div>
</div>

