<div class="header">
    <div class="title"><i class="fa fa-lg fa-book fa-fw"></i> Assessments</div>
    <div class="links">
        <a href="#advocacy/cases/1/assessments/1/edit" class="btn btn-primary btn-sm"><i class="fa fa-edit fa-fw"></i> Update Assessment</a>
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Activities <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#advocacy/activities"><i class="fa fa-list"></i> All Activities</a></li>
                <li><a href="#advocacy/activities/create"><i class="fa fa-plus"></i> Create New Activity</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Assessment</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th width="80%">Measure</th>
                                <th width="20%">Assessment</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>Opening Bank Account</td>
                                <td>Beginner </td>
                            </tr>
                            <tr>
                                <td>Reading and understanding bank statement</td>
                                <td>Intermediate</td>
                            </tr>
                            <tr>
                                <td>Knowing how seek help</td>
                                <td>Beginner</td>
                            </tr>
                            <tr>
                                <td>Applying for a National Insurance Number</td>
                                <td>Intermediate</td>
                            </tr>
                            <tr>
                                <td>Applying for Income Support/Job Seekers Allowance and Housing Benefit</td>
                                <td>Beginner</td>
                            </tr>
                            <tr>
                                <td>Ability to budget</td>
                                <td>Beginner</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>




