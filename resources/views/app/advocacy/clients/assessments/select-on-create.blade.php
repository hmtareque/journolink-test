<div class="header">
    <div class="title"><i class="fa fa-lg fa-book fa-fw"></i> Case Assessment</div>
    <div class="links">
        <a href="#advocacy/cases/create" class="btn btn-success btn-sm"><i class="fa fa-info-circle fa-fw"></i> Case Info</a>
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Activities <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#advocacy/activities"><i class="fa fa-list"></i> All Activities</a></li>
                <li><a href="#advocacy/activities/create"><i class="fa fa-plus"></i> Create New Activity</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-8 col-xs-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Select Assessment</h3>
            </div>
            <div class="panel-body">
                
                
                <ul class="list-unstyled list-group">
                    <li class="list-group-item"><a href="#advocacy/cases/1/assessments/1/create">Personal Circumstance Assessment</a></li>
                    <li class="list-group-item"><a href="#advocacy/cases/1/assessments/1/create">Mental Health Assessment</a></li>
                </ul>
                
             
                
            </div>
        </div>
    </div>
    <div class="col-md-3 col-xs-12">
        @include('app.shared.feed')
    </div>
</div>




