<div class="header">
    <div class="title"><i class="fa fa-lg fa-book fa-fw"></i> Activities</div>
    <div class="links">
        <a href="#advocacy/activities/create" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> Create New Activity</a>
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Cases <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#advocacy/cases"><i class="fa fa-list"></i> All Cases</a></li>
                <li><a href="#advocacy/cases/create"><i class="fa fa-plus"></i> Create New Case</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-md-9 col-xs-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Active Activities</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th colspan="6">Community Advocacy Service</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="info">
                                <th width="10%">Ref. No.</th>
                                <th width="25%">Activity</th>
                                <th width="25%">Topic</th>
                                <th width="10%">Registration</th>
                                <th width="10%">Recurrence</th>
                                <th width="10%">Attendees</th>
                            </tr>
                            <tr>
                                <td> <a href="#advocacy/activities/1">KD0018976</a></td>
                                <td>Women Group</td>
                                <td>Mindfulness</td>
                                <td class="text-center">required</td>
                                <td class="text-center">daily</td>
                                <td class="text-center">8</td>
                            </tr>
                            <tr>
                                <td> <a href="#advocacy/activities/2">BK987650</a></td>
                                <td>Parenting</td>
                                <td>Common Mental Health</td>
                                <td class="text-center">not required</td>
                                <td class="text-center">weekly</td>
                                <td class="text-center">--</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
                
            
                
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th colspan="6">Mental Health Service</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="info">
                                <th width="10%">Ref. No.</th>
                                <th width="25%">Activity</th>
                                <th width="25%">Topic</th>
                                <th width="10%">Registration</th>
                                <th width="10%">Recurrence</th>
                                <th width="10%">Attendees</th>
                            </tr>
                            <tr>
                                <td><a href="#advocacy/activities/3">KD0018976</a></td>
                                <td>Yoga</td>
                                <td>Yoga at Childs Hill</td>
                                <td class="text-center">required</td>
                                <td class="text-center">once</td>
                                <td class="text-center">--</td>
                            </tr>
                        </tbody>
                    </table>
                </div>

            </div>
        </div>
    </div>
    <div class="col-md-3 col-xs-12">
        @include('app.shared.feed')
    </div>
</div>




