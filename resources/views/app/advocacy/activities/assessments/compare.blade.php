<div class="header">
    <div class="title"><i class="fa fa-lg fa-book fa-fw"></i> Compare Assessments</div>
    <div class="links">
        <a href="#advocacy/cases/1" class="btn btn-success btn-sm"><i class="fa fa-info-circle fa-fw"></i> Case Info</a>
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Activities <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#advocacy/activities"><i class="fa fa-list"></i> All Activities</a></li>
                <li><a href="#advocacy/activities/create"><i class="fa fa-plus"></i> Create New Activity</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Compare Assessments</h3>
            </div>
            <div class="panel-body">
                <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th width="70%">Measure</th>
                                <th width="15%">Assessment 1</th>
                                <th width="15%">Assessment 2</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="info">
                                <th width="70%"></th>
                                <th width="15%">07/09/2016</th>
                                <th width="15%">14/09/2016</th>
                            </tr>
                            <tr>
                                <td>Opening Bank Account</td>
                                <td>Beginner </td>
                                <td>Expert</td>
                            </tr>
                            <tr>
                                <td>Reading and understanding bank statement</td>
                                <td>Intermediate</td>
                                <td>Expert</td>
                            </tr>
                            <tr>
                                <td>Knowing how seek help</td>
                                <td>Beginner</td>
                                <td>Beginner</td>
                            </tr>
                            <tr>
                                <td>Applying for a National Insurance Number</td>
                                <td>Intermediate</td>
                                <td>Expert</td>
                            </tr>
                            <tr>
                                <td>Applying for Income Support/Job Seekers Allowance and Housing Benefit</td>
                                <td>Beginner</td>
                                <td>Expert</td>
                            </tr>
                            <tr>
                                <td>Ability to budget</td>
                                <td>Beginner</td>
                                <td>Expert</td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
</div>




