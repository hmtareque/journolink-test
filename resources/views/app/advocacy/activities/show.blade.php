<div class="header">
    <div class="title"><i class="fa fa-lg fa-book fa-fw"></i> Activity</div>
    <div class="links">
        <a href="#advocacy/activities/1/edit" class="btn btn-success btn-sm"><i class="fa fa-edit fa-fw"></i> Update Activity</a>
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Cases <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#advocacy/cases"><i class="fa fa-list"></i> All Cases</a></li>
                <li><a href="#advocacy/cases/create"><i class="fa fa-plus"></i> Create New Case</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-8">
        
        <div class="row">
            <div class="col-xs-12">
                 <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Activity</h3>
                    </div>
                    <div class="panel-body">
                        
                        <div class="row">
                            <div class="col-xs-12 col-md-6">
                                <ul class="list-unstyled">
                                <li><strong>Service</strong> : Community Advocacy Service</li>
                                <li><strong>Category</strong> : -- </li>
                                <li><strong>Activity</strong> : Women Group</li>
                                <li><strong>Topic</strong> : Mindfulness</li>
                            </ul>
                            </div>
                            <div class="col-xs-12 col-md-6">
                                <ul class="list-unstyled">
                                <li><strong>Registration</strong> : <label class="label label-danger">required</label></li>
                                <li><strong>Recurrence</strong> : Daily</li>
                            </ul>
                            </div>
                        </div>
                        
                            
                    </div>
                </div>
                
                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Registered Attendees</h3>
                    </div>
                    <div class="panel-body">
                        <div class="table-responsive">
                            <table class="table data-table table-striped table-bordered table-hover" width="100%">
                                <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Attended</th>
                                        <th>Assessed</th>
                                        <th>Contact</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    <tr>
                                        <td><a href="#advocacy/clients/1">Hasan <strong>Tareque</strong></a></td>
                                        <td>3</td>
                                        <td>2</td>
                                        <td>0789 676543</td>
                                    </tr>

                                    <tr>
                                        <td><a href="#advocacy/clients/1">Fatima <strong>Basit</strong></a></td>
                                        <td>4</td>
                                        <td><label class="label label-danger">never</label></td>
                                        <td>0203 546 2675</td>
                                    </tr>

                                    <tr>
                                        <td><a href="#advocacy/clients/1">Behnaz <strong>Nessar</strong></a></td>
                                        <td>1</td>
                                        <td>1</td>
                                        <td>07951 786543</td>
                                    </tr>
                                </tbody>
                            </table>
                        </div>
                    </div>
                    <div class="panel-footer">
                        <a href="#advocacy/activities/1/attendees/create" class="btn btn-success btn-xs"><i class="fa fa-plus fa-fw"></i>Register New Attendee</a>
                        <a href="#advocacy/activities/1/attendees" class="btn btn-primary btn-xs"><i class="fa fa-wrench fa-fw"></i>Manage Attendees</a>
                    </div>
                </div>
            </div>
        </div>

          <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Actions</h3>
            </div>
            <div class="panel-body">
                
                
                  <div class="table-responsive">
                    <table class="table table-striped table-bordered table-hover" width="100%">
                        <thead>
                            <tr>
                                <th>Action Types</th>
                                <th class="text-center">Taken</th>
                                <th class="text-right">Time Spent (min)</th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>One to One Session</td>
                                <td class="text-center">2</td>
                                <td class="text-right">70min</td>
                            </tr>

                            <tr>
                                 <td>Weekly Football Activity</td>
                                <td class="text-center">5</td>
                                <td class="text-right">30min</td>
                            </tr>
                        </tbody>
                        <tfoot>
                            <tr>
                                <th>Total</th>
                                <th class="text-center">7</th>
                                <th class="text-right">1 hour 40 min</th>
                            </tr>
                        </tfoot>
                    </table>
                </div>
                
                
                
                <h4 class="section-title">Recent Actions</h4>
                
                
                
                <div class="panel panel-success">
                    <div class="panel-heading">Attended mentoring session</div>
                    <div class="panel-body">A prominent Thai human rights lawyer faces a prison term of up to 150 years if convicted of royal defamation under Thailand's royal insult law, according to a legal watchdog.</div>
                    <div class="panel-footer">Created By Mamudu Abudu At 12/09/2017 6:25pm</div>
                </div>

                <div class="panel panel-success">
                    <div class="panel-heading">College support</div>
                    <div class="panel-body">
                        It was not clear what exactly Prawet, 57, wrote on Facebook, but the AFP news agency said in one of the recent posts he encouraged Thais to push the boundaries of the lese majeste law.
                        <ul class="list-unstyled list-inline">
                            <li><i class="fa fa-paperclip fa-fw"></i> <a href="">image</a></li>
                            <li><i class="fa fa-paperclip fa-fw"></i> <a href="">excel</a></li>
                            <li><i class="fa fa-paperclip fa-fw"></i> <a href="">pdf</a></li>
                        </ul>
                    </div>
                    <div class="panel-footer">Created By Hussain Tawhid At 17/03/2017 6:25pm</div>
                </div>

                <div class="panel panel-success">
                    <div class="panel-heading">ESOL support</div>
                    <div class="panel-body">The small bronze monument, which lay in Bangkok's heavily-policed Royal Plaza, marked the 1932 revolution that ended absolute monarchy in Thailand.</div>
                    <div class="panel-footer">Created By Tahmid Ahmed At 12/05/2017 6:25pm</div>
                </div>

                <div class="collapse margin-top-5" id="note-form">
                    <div class="panel panel-primary">
                        <div class="panel-heading">Create New Action</div>
                        <div class="panel-body">
                            <form class="form-horizontal ajax-form" action="{{url('')}}" method="post">
                                <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                
                                <div class="form-group" id="type">
                                    <div class="col-xs-12">
                                        <select name="type" class="form-control">
                                            <option> -- Please Select -- </option>
                                            <option>Action</option>
                                        </select>
                                        <span class="help-block hide"></span>
                                    </div>
                                </div>
                                <div class="form-group" id="note">
                                    <div class="col-xs-12">
                                        <textarea name="note" class="form-control" placeholder="Description"></textarea>
                                        <span class="help-block hide"></span>
                                    </div>
                                </div>
                                <div class="form-group" id="time_spent">
                                    <div class="col-xs-12">
                                        <input type="text" class="form-control" name="time_spent" placeholder="Time Spent"/>
                                        <span class="help-block hide"></span>
                                    </div>
                                </div>
                                <div class="form-group" id="private">
                                    <div class="col-xs-12">
                                        <div class="checkbox">
                                                <label><input type="checkbox" value=""> Private</label>
                                              </div>
                                        <span class="help-block hide"></span>
                                    </div>
                                </div>
                                <div class="form-group" id="files">
                                    <div class="col-xs-12">
                                        <input type="file" class="btn btn-sm btn-default" name="files[]" multiple=""/>
                                        <span class="help-block hide"></span>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <div class="col-xs-12">
                                        <button type="submit" class="btn btn-primary"> 
                                            <i class="fa fa-save fa-fw"></i> Save
                                        </button>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel-footer">
                <a class="btn btn-xs btn-success" role="button" data-toggle="collapse" href="#note-form" aria-expanded="false" aria-controls="collapseExample"><i class="fa fa-plus fa-fw"></i> Create New Action</a>
                <a href="#advocacy/cases/1/actions" class="btn btn-primary btn-xs"><i class="fa fa-wrench fa-fw"></i>Manage Actions</a>
            </div>
        </div>
    </div>
    
    <div class="col-xs-12 col-sm-12 col-md-4">
 
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Facilitator(s)</h3>
            </div>
            <div class="panel-body">
                <ul class="list-unstyled">
                    <li><strong>Person(s)</strong> : Emma Goldie</li>
                    <li><strong>Organisation(s)</strong> : Paiwand</li>
                </ul>
            </div>
        </div>
        
        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Partner(s)</h3>
            </div>
            <div class="panel-body">
                <p>Harrow Council</p>
            </div>
        </div>
        
        <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Location</h3>
                    </div>
                    <div class="panel-body">
                        <ul class="list-unstyled">
                                <li>23 Love Lane</li>
                                <li>Heart Street</li>
                                <li>Mindland</li>
                                <li>LV5 8VM</li>
                            </ul>
                    </div>
                </div>
        
         <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Recent Attendances</h3>
                    </div>
                    <div class="panel-body">
                        <ul class="list-unstyled">
                            <li><i class="fa fa-calendar-check-o fa-fw"></i><a href="#advocacy/activities/45/attendances/1">12/09/2016</a></li>
                            <li><i class="fa fa-calendar-check-o fa-fw"></i><a href="#advocacy/activities/45/attendances/1">05/09/2016</a></li>
                            <li><i class="fa fa-calendar-check-o fa-fw"></i><a href="#advocacy/activities/45/attendances/1">28/08/2016</a></li>
                            <li><i class="fa fa-calendar-check-o fa-fw"></i><a href="#advocacy/activities/45/attendances/1">21/08/2016</a></li>
                        </ul>
                    </div>
                    <div class="panel-footer">
                        <a class="btn btn-xs btn-success" href="#advocacy/activities/45/attendances/create"><i class="fa fa-check-circle-o fa-fw"></i>Record Attendance</a>
                        <a class="btn btn-xs btn-primary" href="#advocacy/activities/45/attendances"><i class="fa fa-list-alt fa-fw"></i>All Attendances</a>
                    </div>
                </div>
        
        
       
               
           

          <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Recent Documents</h3>
                    </div>
                    <div class="panel-body">
                        <ul class="list-unstyled">
                            <li><i class="fa fa-file-o fa-fw"></i><a href="#">Document 1</a></li>
                            <li><i class="fa fa-file-o fa-fw"></i><a href="#">Document 2</a></li>
                            <li><i class="fa fa-file-o fa-fw"></i><a href="#">Document 3</a></li>
                        </ul>
                        
                         <div class="collapse margin-top-5" id="doc-form">
                            <div class="panel panel-primary">
                                <div class="panel-heading">Upload New Doc</div>
                                <div class="panel-body">
                                    <form class="form-horizontal ajax-form" action="{{url('')}}" method="post">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                        <div class="form-group" id="note">
                                            <div class="col-xs-12">
                                                <input type="text" name="name" class="form-control" placeholder="File Name"/>
                                                <span class="help-block hide"></span>
                                            </div>
                                        </div>
                                        <div class="form-group" id="files">
                                            <div class="col-xs-12">
                                                <input type="file" class="btn btn-sm btn-default" name="files[]" multiple=""/>
                                                <span class="help-block hide"></span>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-xs-12">
                                                <button type="submit" class="btn btn-primary"> 
                                                    <i class="fa fa-upload fa-fw"></i> Upload
                                                </button>
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>
                        
                    </div>
                    <div class="panel-footer">
                        <a class="btn btn-xs btn-success" role="button" data-toggle="collapse" href="#doc-form" aria-expanded="false" aria-controls="collapseExample">
                            <i class="fa fa-upload fa-fw"></i> Upload New Doc
                        </a>
                        <a class="btn btn-xs btn-primary" href="#advocacy/cases/1/docs"><i class="fa fa-wrench fa-fw"></i>Manage Documents</a>
                       
                    </div>
                </div>
      
    </div>

</div>







