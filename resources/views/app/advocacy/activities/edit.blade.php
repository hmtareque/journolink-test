<div class="header">
    <div class="title"><i class="fa fa-lg fa-book fa-fw"></i> Edit Activity</div>
    <div class="links">
        <a href="#advocacy/activities" class="btn btn-success btn-sm"><i class="fa fa-list-alt fa-fw"></i> All Activities</a>
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Cases <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#advocacy/cases"><i class="fa fa-list"></i> All Cases</a></li>
                <li><a href="#advocacy/cases/create"><i class="fa fa-plus"></i> Create New Case</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-8">
        <form class="form-horizontal ajax-form" action="{{url('manage/education/students')}}" method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            
            
            <h4 class="section-title">Activity Information</h4>
            
            <div class="form-group" id="service">
                <label class="col-sm-3 control-label">Service</label>
                <div class="col-sm-9">
                    <select class="form-control" name="service">
                        <option value=""> -- Please Select -- </option>
                        <option value="1">Youth</option>
                        <option value="1">Housing</option>
                        <option value="1">Employability</option>
                        <option value="1">Parenting</option>
                    </select>
                </div>
            </div>
            
            <div class="form-group" id="category">
                <label class="col-sm-3 control-label">Category</label>
                <div class="col-sm-9">
                    <select class="form-control" name="category">
                        <option value=""> -- Please Select -- </option>
                        <option value="1">Money</option>
                        <option value="1">Health</option>
                        <option value="1">Education</option>
                        <option value="1">Life</option>
                    </select>
                </div>
            </div>
            
            <div class="form-group" id="type">
                <label class="col-sm-3 control-label">Activity</label>
                <div class="col-sm-9">
                    <select class="form-control" name="type">
                        <option value=""> -- Please Select -- </option>
                    </select>
                </div>
            </div>
            
            <div class="form-group" id="type">
                <label class="col-sm-3 control-label">Topic</label>
                <div class="col-sm-9">
                    <select class="form-control" name="type">
                        <option value=""> -- Please Select -- </option>
                    </select>
                </div>
            </div>
            
            <div class="form-group" id="description">
                <label class="col-sm-3 control-label">Description</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="description"></textarea>
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            <div class="form-group" id="partners">
                <label class="col-sm-3 control-label">Partners</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="partners"></textarea>
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            <div class="form-group" id="recurrence">
                <label class="col-sm-3 control-label">Recurrence</label>
                <div class="col-sm-9">
                    <select class="form-control" name="recurrence">
                        <option value=""> -- Please Select -- </option>
                        <option value="daily">Daily</option>
                        <option value="weekly">Weekly</option>
                        <option value="monthly">Monthly</option>
                        <option value="yearly">Yearly</option>
                        <option value="oneoff">One Off</option>
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            <div class="form-group" id="registration_required">
                <label class="col-sm-3 control-label">Registration Required</label>
                <div class="col-sm-9">
                    <select class="form-control" name="registration_required">
                        <option value="1">Yes</option>
                        <option value="0">No</option>
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            <h4 class="section-title">Facilitator(s)</h4>
            
            <div class="form-group" id="facilitator_name">
                <label class="col-sm-3 control-label">Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="facilitator_name" placeholder="Name">
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            <div class="form-group" id="facilitator_organisation">
                <label class="col-sm-3 control-label">Organisation</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="facilitator_organisation" placeholder="Organisation">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <h4 class="section-title">Location</h4>

            <div class="form-group" id="address_line_1">
                <label class="col-sm-3 control-label">Address Line 1</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="address_line_1" placeholder="Address Line 1">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="address_line_2">
                <label class="col-sm-3 control-label">Address Line 2</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="address_line_2" placeholder="Address Line 2">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="street">
                <label class="col-sm-3 control-label">Street</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="street" placeholder="Street">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="city">
                <label class="col-sm-3 control-label">City</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="city" placeholder="City">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="postcode">
                <label class="col-sm-3 control-label">Postcode</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="postcode" placeholder="Postcode">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="borough">
                <label class="col-sm-3 control-label">Borough</label>
                <div class="col-sm-9">
                    <select class="form-control" name="borough">
                        <option value=""> -- Please Select -- </option>
                        @foreach($boroughes as $borough)
                        <option value="{{trim($borough->name)}}">{{$borough->name}}</option>
                        @endforeach 
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

           
            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-primary"> 
                        <i class="fa fa-refresh fa-fw"></i> Update
                    </button>
                </div>
            </div>
        </form>
    </div>
    <div class="col-xs-12 col-md-4">
        @include('app.shared.feed')
    </div>
</div>

