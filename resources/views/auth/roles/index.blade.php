<div class="header">
    <div class="title">
        <i class="fa fa-user-plus fa-fw"></i> {{trans('label.list_of_items', array('item' => trans('label.roles')))}}
    </div>
    <div class="links">
        <a href="#roles/create" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> {{trans('label.create_new_item', array('item' => trans('label.role')))}}</a>
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{trans('label.user')}} <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#users"><i class="fa fa-list"></i> {{trans('label.list_of_items', array('item' => trans('label.users')))}}</a></li>
                <li><a href="#users/create"><i class="fa fa-user-plus"></i> {{trans('label.create_new_item', array('item' => trans('label.user')))}}</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-8">

        <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->

        <div class="table-responsive">
            <table class="table data-table table-condensed table-striped table-bordered table-hover" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>{{trans('label.role')}}</th>
                        <th>{{trans('label.no_of_item', array('item' => trans('label.user')))}}</th>
                        <th>{{trans('label.action')}}</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($roles as $role)
                    <tr>
                        <td><a href="#roles/{{$role->id}}" class="xclick" title="Details">{{$role->name}}</a></td>
                        <td class="text-center">{{$role->no_of_user}}</td>
                        <td>
                            <a href="#roles/{{$role->id}}/edit" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="{{trans('label.edit_item', array('item' => $role->name))}}"><i class="fa fa-pencil"></i></a>
                            @if($role->no_of_user == 0)
                            <button class="btn btn-danger btn-xs" data-toggle="modal" data-target="#delete-role-{{$role->id}}">
                                <i class="fa fa-trash-o"></i>
                            </button>
                            <!-- Modal -->
                            <div class="modal fade" id="delete-role-{{$role->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
                                <div class="modal-dialog">
                                    <div class="modal-content">
                                        <form class="ajax-form" action="{{url('manage/roles/'.$role->id)}}" method="post">
                                            <input type="hidden" name="_token" value="{{ csrf_token() }}">
                                            <input type="hidden" name="_method" value="delete"/>
                                            <div class="modal-header">
                                                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                                    &times;
                                                </button>
                                                <h4 class="modal-title" id="myModalLabel">{{trans('label.delete_item', array('item' => trans('label.role')))}}</h4>
                                            </div>
                                            <div class="modal-body">
                                                <p>{{trans('alert.sure_to_delete_item', array('item' => $role->name))}}</p>
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
                                                    {{trans('action.cancel')}}
                                                </button>
                                                <button type="submit" class="btn btn-danger btn-sm">
                                                    <i class="fa fa-trash-o"></i>&nbsp;{{trans('action.delete')}}
                                                </button>
                                            </div>
                                        </form> 
                                    </div>
                                </div>
                            </div>
                            @endif
                        </td>
                    </tr>
                    @endforeach 
                </tbody>
                <tfoot>
                    <tr>
                        <th>{{trans('label.role')}}</th>
                        <th>{{trans('label.no_of_item', array('item' => trans('label.user')))}}</th>
                        <th>{{trans('label.action')}}</th>
                    </tr>
                </tfoot>
            </table>
        </div>
    </div>
</div>










