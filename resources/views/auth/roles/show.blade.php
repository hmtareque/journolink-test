<div class="header">
    <div class="title">
        <i class="fa fa-user-plus fa-fw"></i> {{$role->name}} {{trans('label.role')}}
    </div>
    <div class="links">
        <a href="#roles/{{$role->id}}/edit" class="btn btn-primary btn-sm"><i class="fa fa-pencil"></i> {{trans('label.edit_item', array('item' => $role->name))}}</a>
        <a href="#roles/create" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> {{trans('label.create_new_item', array('item' => trans('label.role')))}}</a>
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{trans('label.role')}} <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#roles"><i class="fa fa-list"></i> {{trans('label.list_of_items', array('item' => trans('label.roles')))}}</a></li>
                <li class="separator"></li>
                <li><a href="#users"><i class="fa fa-list"></i> {{trans('label.list_of_items', array('item' => trans('label.users')))}}</a></li>
                <li><a href="#users/create"><i class="fa fa-plus"></i> {{trans('label.create_new_item', array('item' => trans('label.user')))}}</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-7">
        <div class="table-responsive">
            <table class="table table-condensed table-bordered table-hover">
                <thead>
                    <tr>
                        <th colspan="2">{{trans('label.role')}}</th>
                        <th colspan="5" class="text-center">{{$role->name}}</th>
                    </tr>
                </thead>
                <tr>
                    <td colspan="2"class="text-center"><label class="control-label">{{trans('label.module')}}</label></td>
                    <td colspan="5" class="text-center"><label class="control-label">{{trans('label.permission')}}</label></td>
                </tr>

                <tr>
                    <td class="text-center"><label class="control-label">ID</label></td>
                    <td class="text-center"><label class="control-label">NAME</label></td>
                    <td class="text-center"><label class="control-label">CREATE</label></label></td>
                    <td class="text-center"><label class="control-label text-success">READ</label></td>
                    <td class="text-center"><label class="control-label text-primary">UPDATE</label></td>
                    <td class="text-center"><label class="control-label text-danger">DELETE</label></td>
                </tr>

                @foreach($role->permissions as $module => $permission)
                <tr>
                    <td class="text-center">{{$module}}</td>
                    <td class="text-left">{{$permission['name']}}</td>
                    <td class="text-center">@if(in_array(1, $permission)) <strong class="text-center"><i class="fa fa-check fa-fw"></i></strong> @endif </td>
                    <td class="text-center">@if(in_array(2, $permission)) <strong class="text-success text-center"><i class="fa fa-check fa-fw"></i></strong> @endif </td>
                    <td class="text-center">@if(in_array(3, $permission)) <strong class="text-primary text-center"><i class="fa fa-check fa-fw"></i></strong> @endif </td>
                    <td class="text-center">@if(in_array(4, $permission)) <strong class="text-danger text-center"><i class="fa fa-check fa-fw"></i></strong> @endif </td>
                </tr>
                @endforeach 

            </table>
        </div>

    </div>

    <div class="col-xs-12 col-sm-12 col-md-5">
        <div class="table-responsive">
            <table class="table table-bordered table-hover table-condensed table-striped mt-table">
                <thead>
                    <tr>
                        <th colspan="3">{{trans('label.users_of_role', array('role' => $role->name))}}</th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <th>{{trans('label.name')}}</th>
                        <th>{{trans('label.email')}}</th>
                        <th>{{trans('label.action')}}</th>
                    </tr>
                    @if($users->count()>0) 
                    @foreach($users as $user)
                    <tr>
                        <td>{{$user->first_name}} {{$user->last_name}}</td>
                        <td>{{$user->email}}</td>
                        <td>
                            <a href="#users/{{$user->id}}/edit" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="{{trans('label.edit_item', array('item' => $user->first_name.' '.$user->last_name))}}"><i class="fa fa-pencil"></i></a>
                        </td>
                    </tr>
                    @endforeach
                    @else 
                    <tr>
                        <td colspan="3">
                            <p class="alert alert-info"><i class="fa fa-info"></i> {{trans('alert.no_users_found_of_this_role')}}</p>
                        </td>
                    </tr>
                    @endif
                </tbody>
            </table>
        </div>
    </div>
</div>




