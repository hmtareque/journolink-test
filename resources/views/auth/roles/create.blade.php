<div class="header">
    <div class="title">
        <i class="fa fa-user-plus fa-fw"></i> {{trans('label.create_new_item', array('item' => trans('label.role')))}}
    </div>
    <div class="links">
        <a href="#roles" class="btn btn-primary btn-sm"><i class="fa fa-list"></i> {{trans('label.list_of_items', array('item' => trans('label.roles')))}}</a>

        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{trans('label.user')}} <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#users"><i class="fa fa-list"></i> {{trans('label.list_of_items', array('item' => trans('label.user')))}}</a></li>
                <li><a href="#users/create"><i class="fa fa-plus"></i> {{trans('label.create_new_item', array('item' => trans('label.user')))}}</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-8">
        <form class="form-horizontal ajax-form" action="{{url('manage/roles')}}" method="post" data-hash="roles">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <table class="table table-condensed table-bordered table-hover">
                <tr>
                    <td colspan="2"><label class="control-label">{{trans('label.role')}}</label></td>
                    <td colspan="5">
                        <div class="" id="role">
                            <input type="text" name="role" class="form-control" placeholder="{{trans('label.role')}}">
                            <span class="role-help-block help-block hide"></span>
                        </div>
                    </td>
                </tr>
                
                <tr>
                    <td colspan="2"class="text-center"><label class="control-label">{{trans('label.module')}}</label></td>
                    <td colspan="5" class="text-center"><label class="control-label">{{trans('label.permission')}}</label></td>
                </tr>

                <tr>
                    <td class="text-center"><label class="control-label">ID</label></td>
                    <td class="text-center"><label class="control-label">NAME</label></td>
                    <td class="text-center"><label class="control-label">CREATE</label><br/><label><input type="checkbox" class="all-select" value="create"/></label></td>
                    <td class="text-center"><label class="control-label">READ</label><br/><label><input type="checkbox" class="all-select" value="read"/></label></td>
                    <td class="text-center"><label class="control-label">UPDATE</label><br/><label><input type="checkbox" class="all-select" value="update"/></label></td>
                    <td class="text-center"><label class="control-label">DELETE</label><br/><label><input type="checkbox" class="all-select" value="delete"/></label></td>
                    <td class="text-center"><label class="control-label">ALL</label><br/><label><input type="checkbox" class="all-select" value="all"/></label></td>
                </tr>

                @foreach($modules as $module)
                <tr>
                    <td class="text-center">{{$module->id}}</td>
                    <td class="text-left">{{$module->name}}</td>
                    <td class="text-center"><label class="control-label"><input class="create" id="create-{{$module->id}}" data-module="{{$module->id}}" type="checkbox" name="permission[{{$module->id}}][1]" value="1"/></label></td>
                    <td class="text-center"><label class="control-label"><input class="read" id="read-{{$module->id}}" data-module="{{$module->id}}" type="checkbox" name="permission[{{$module->id}}][2]" value="2"/></label></td>
                    <td class="text-center"><label class="control-label"><input class="update" id="update-{{$module->id}}" data-module="{{$module->id}}" type="checkbox" name="permission[{{$module->id}}][3]" value="3"/></label></td>
                    <td class="text-center"><label class="control-label"><input class="delete" id="delete-{{$module->id}}" data-module="{{$module->id}}" type="checkbox" name="permission[{{$module->id}}][4]" value="4"/></label></td>
                    <td class="text-center"><label class="control-label"><input class="all" id="all-{{$module->id}}" data-module="{{$module->id}}" type="checkbox" value="1"></label></td>
                </tr>
                @endforeach 

                <tr>
                    <td colspan="7">
                        <div id="permission"><span class="permission-help-block help-block hide"></span></div>
                        <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-save fa-fw"></i> Save</button>
                    </td>
                </tr>

            </table>

        </form>
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('.linked-row').click(function () {
            window.location = $(this).data('url');
        });

        $(document).on('click', '.all-select', function () {
            if ($(this).prop('checked')) {
                $("." + $(this).val()).prop('checked', true);
                
                if($(this).val() == "create" || $(this).val() == "update" || $(this).val() == "delete"){
                    $(".read").prop('checked', true);
                }

            } else {
                $("." + $(this).val()).prop('checked', false);
                
                if($(this).val() == "read"){
                    $(".create").prop('checked', false);
                    $(".read").prop('checked', false);
                    $(".update").prop('checked', false);
                    $(".delete").prop('checked', false);
                    $(".all").prop('checked', false);
                    $(".all-select").prop('checked', false);
                }
            }

            if ($(this).val() == "all") {
                if ($(this).prop('checked')) {
                    $(".create").prop('checked', true);
                    $(".read").prop('checked', true);
                    $(".update").prop('checked', true);
                    $(".delete").prop('checked', true);
                    $(".all-select").prop('checked', true);
                } else {
                    $(".create").prop('checked', false);
                    $(".read").prop('checked', false);
                    $(".update").prop('checked', false);
                    $(".delete").prop('checked', false);
                    $(".all-select").prop('checked', false);
                }
            }
        });

        $(document).on('click', '.create', function () {
            var module = $(this).data('module');
            if ($(this).prop('checked')) {
                $("#read-" + module).prop('checked', true);
            } else {
                $("#create-" + module).prop('checked', false);
                $("#all-" + module).prop('checked', false);
                $(".all-select").prop('checked', false);
            }
        });

        $(document).on('click', '.read', function () {
            var module = $(this).data('module');
            if ($(this).prop('checked')) {
                $("#read-" + module).prop('checked', true);
            } else {
                $("#create-" + module).prop('checked', false);
                $("#read-" + module).prop('checked', false);
                $("#update-" + module).prop('checked', false);
                $("#delete-" + module).prop('checked', false);
                $("#all-" + module).prop('checked', false);
                $(".all-select").prop('checked', false);
            }
        });


        $(document).on('click', '.update', function () {
            var module = $(this).data('module');
            if ($(this).prop('checked')) {
                $("#read-" + module).prop('checked', true);
            } else {
                $("#update-" + module).prop('checked', false);
                $("#all-" + module).prop('checked', false);
                $(".all-select").prop('checked', false);
            }
        });

        $(document).on('click', '.delete', function () {
            var module = $(this).data('module');
            if ($(this).prop('checked')) {
                $("#read-" + module).prop('checked', true);
            } else {
                $("#delete-" + module).prop('checked', false);
                $("#all-" + module).prop('checked', false);
                $(".all-select").prop('checked', false);
            }
        });


        $(document).on('click', '.all', function () {
            var module = $(this).data('module');
            if ($(this).prop('checked')) {
                $("#create-" + module).prop('checked', true);
                $("#read-" + module).prop('checked', true);
                $("#update-" + module).prop('checked', true);
                $("#delete-" + module).prop('checked', true);
                $("#all-" + module).prop('checked', true);
            } else {
                $("#create-" + module).prop('checked', false);
                $("#read-" + module).prop('checked', false);
                $("#update-" + module).prop('checked', false);
                $("#delete-" + module).prop('checked', false);
                $("#all-" + module).prop('checked', false);
                $(".all-select").prop('checked', false);
            }
        });

    });
</script>