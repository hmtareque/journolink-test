<div class="header">
    <div class="title"><i class="fa fa-user-plus fa-fw"></i> {{trans('label.edit_item', array('item' => trans('label.role')))}}</div>
    <div class="links">
        <a href="#roles/create" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> {{trans('label.create_new_item', array('item' => trans('label.role')))}}</a>
        <a href="#roles" class="btn btn-primary btn-sm"><i class="fa fa-list"></i> {{trans('label.list_of_items', array('item' => trans('label.roles')))}}</a>

        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{trans('label.user')}} <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#users"><i class="fa fa-list"></i> {{trans('label.list_of_items', array('item' => trans('label.users')))}}</a></li>
                <li><a href="#users/create"><i class="fa fa-plus"></i> {{trans('label.create_new_item', array('item' => trans('label.user')))}}</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-7">

        <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->

        <form class="form-horizontal ajax-form" id="role-update-form" action="{{url('manage/roles/'.$role->id)}}" method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <input type="hidden" name="_method" value="put"/>

            <div class="table-responsive">
                <table class="table table-condensed table-bordered table-hover">
                    <tr>
                        <td colspan="2"><label class="control-label">{{trans('label.role')}}</label></td>
                        <td colspan="5">
                            <div class="" id="role">
                                <input type="text" name="role" class="form-control" placeholder="{{trans('label.role')}}" value="{{$role->name}}">
                                <span class="role-help-block help-block hide"></span>
                            </div>
                        </td>
                    </tr>

                    <tr>
                        <td colspan="2"class="text-center"><label class="control-label">{{trans('label.module')}}</label></td>
                        <td colspan="5" class="text-center"><label class="control-label">{{trans('label.permission')}}</label></td>
                    </tr>

                    <tr>
                        <td class="text-center"><label class="control-label">ID</label></td>
                        <td class="text-center"><label class="control-label">NAME</label></td>
                        <td class="text-center"><label class="control-label">CREATE</label><br/><label><input type="checkbox" class="all-select" value="create"/></label></td>
                        <td class="text-center"><label class="control-label text-success">READ</label><br/><label><input type="checkbox" class="all-select" value="read"/></label></td>
                        <td class="text-center"><label class="control-label text-primary">UPDATE</label><br/><label><input type="checkbox" class="all-select" value="update"/></label></td>
                        <td class="text-center"><label class="control-label text-danger">DELETE</label><br/><label><input type="checkbox" class="all-select" value="delete"/></label></td>
                        <td class="text-center"><label class="control-label">ALL</label><br/><label><input type="checkbox" class="all-select" value="all"/></label></td>
                    </tr>

                    @foreach($role->permissions as $module => $permission)
                    <tr>
                        <td class="text-center">{{$module}}</td>
                        <td class="text-left">{{$permission['name']}}</td>
                        <td class="text-center"><label class="control-label"><input class="create" id="create-{{$module}}" data-module="{{$module}}" type="checkbox" name="permission[{{$module}}][1]" value="1" @if(old('permission.'.$module.'.1') == 1) checked="" @elseif($permission[1]) == 1) checked="" @endif/></label></td>
                        <td class="text-center"><label class="control-label"><input class="read" id="read-{{$module}}" data-module="{{$module}}" type="checkbox" name="permission[{{$module}}][2]" value="2" @if(old('permission.'.$module.'.2') == 2) checked="" @elseif($permission[2]) == 2) checked="" @endif></label></td>
                        <td class="text-center"><label class="control-label"><input class="update" id="update-{{$module}}" data-module="{{$module}}" type="checkbox" name="permission[{{$module}}][3]" value="3" @if(old('permission.'.$module.'.3') == 3) checked="" @elseif($permission[3]) == 3) checked="" @endif></label></td>
                        <td class="text-center"><label class="control-label"><input class="delete" id="delete-{{$module}}" data-module="{{$module}}" type="checkbox" name="permission[{{$module}}][4]" value="4" @if(old('permission.'.$module.'.4') == 4) checked="" @elseif($permission[4]) == 4) checked="" @endif></label></td>
                        <td class="text-center"><label class="control-label"><input class="all" id="all-{{$module}}" data-module="{{$module}}" type="checkbox" value="1"></label></td>
                
                    </tr>
                    @endforeach 

                    <tr>
                        <td colspan="7">
                            <div id="permission"><span class="permission-help-block help-block hide"></span></div>
                            <button type="submit" class="btn btn-primary btn-sm"><i class="fa fa-refresh fa-fw"></i> Update</button>
                        </td>
                    </tr>

                </table>
            </div>
        </form>
    </div>
    
      <div class="col-xs-12 col-sm-12 col-md-5">
        <div class="table-responsive">
                        <table class="table table-bordered table-hover table-condensed table-striped mt-table">
                            <thead>
                                <tr>
                                    <th colspan="3">{{trans('label.users_of_role', array('role' => $role->name))}}</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th>{{trans('label.name')}}</th>
                                    <th>{{trans('label.email')}}</th>
                                    <th>{{trans('label.action')}}</th>
                                </tr>
                                @if($users->count()>0) 
                                @foreach($users as $user)
                                
                                <tr>
                                    <td>{{$user->first_name}} {{$user->last_name}}</td>
                                    <td>{{$user->email}}</td>
                                    <td>
                                        <a href="#users/{{$user->id}}/edit" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="{{trans('label.edit_item', array('item' => trans('label.user')))}}"><i class="fa fa-pencil"></i></a>
                                    </td>
                                </tr>
                                @endforeach
                                @else 
                                <tr>
                                    <td colspan="3">
                                       <p class="alert alert-info"><i class="fa fa-info"></i> {{trans('alert.no_users_found_of_this_role')}}</p>
 
                                    </td>
                                </tr>
                                 @endif
                            </tbody>
                        </table>

                        
        </div>
                        
                       
    </div>
</div>

<script type="text/javascript">
    $(document).ready(function () {
        $('.linked-row').click(function () {
            window.location = $(this).data('url');
        });

        $(document).on('click', '.all-select', function () {
            if ($(this).prop('checked')) {
                $("." + $(this).val()).prop('checked', true);
                
                if($(this).val() == "create" || $(this).val() == "update" || $(this).val() == "delete"){
                    $(".read").prop('checked', true);
                }

            } else {
                $("." + $(this).val()).prop('checked', false);
                
                if($(this).val() == "read"){
                    $(".create").prop('checked', false);
                    $(".read").prop('checked', false);
                    $(".update").prop('checked', false);
                    $(".delete").prop('checked', false);
                    $(".all").prop('checked', false);
                    $(".all-select").prop('checked', false);
                }
            }

            if ($(this).val() == "all") {
                if ($(this).prop('checked')) {
                    $(".create").prop('checked', true);
                    $(".read").prop('checked', true);
                    $(".update").prop('checked', true);
                    $(".delete").prop('checked', true);
                    $(".all-select").prop('checked', true);
                } else {
                    $(".create").prop('checked', false);
                    $(".read").prop('checked', false);
                    $(".update").prop('checked', false);
                    $(".delete").prop('checked', false);
                    $(".all-select").prop('checked', false);
                }
            }
        });

        $(document).on('click', '.create', function () {
            var module = $(this).data('module');
            if ($(this).prop('checked')) {
                $("#read-" + module).prop('checked', true);
            } else {
                $("#create-" + module).prop('checked', false);
                $("#all-" + module).prop('checked', false);
                $(".all-select").prop('checked', false);
            }
        });

        $(document).on('click', '.read', function () {
            var module = $(this).data('module');
            if ($(this).prop('checked')) {
                $("#read-" + module).prop('checked', true);
            } else {
                $("#create-" + module).prop('checked', false);
                $("#read-" + module).prop('checked', false);
                $("#update-" + module).prop('checked', false);
                $("#delete-" + module).prop('checked', false);
                $("#all-" + module).prop('checked', false);
                $(".all-select").prop('checked', false);
            }
        });


        $(document).on('click', '.update', function () {
            var module = $(this).data('module');
            if ($(this).prop('checked')) {
                $("#read-" + module).prop('checked', true);
            } else {
                $("#update-" + module).prop('checked', false);
                $("#all-" + module).prop('checked', false);
                $(".all-select").prop('checked', false);
            }
        });

        $(document).on('click', '.delete', function () {
            var module = $(this).data('module');
            if ($(this).prop('checked')) {
                $("#read-" + module).prop('checked', true);
            } else {
                $("#delete-" + module).prop('checked', false);
                $("#all-" + module).prop('checked', false);
                $(".all-select").prop('checked', false);
            }
        });


        $(document).on('click', '.all', function () {
            var module = $(this).data('module');
            if ($(this).prop('checked')) {
                $("#create-" + module).prop('checked', true);
                $("#read-" + module).prop('checked', true);
                $("#update-" + module).prop('checked', true);
                $("#delete-" + module).prop('checked', true);
                $("#all-" + module).prop('checked', true);
            } else {
                $("#create-" + module).prop('checked', false);
                $("#read-" + module).prop('checked', false);
                $("#update-" + module).prop('checked', false);
                $("#delete-" + module).prop('checked', false);
                $("#all-" + module).prop('checked', false);
                $(".all-select").prop('checked', false);
            }
        });

    });
</script>