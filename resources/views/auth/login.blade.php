@extends('auth.layout')
@section('content')
<div class="row">
    <div class="col-xs-12"><h3 class="app-login-msg">{{config('settings.app_login_msg')}}</h3></div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-6 col-md-offset-3">
        <div class="login-box">
            <form class="form-horizontal" role="form" method="POST" action="{{ url('/login') }}">
                {{ csrf_field() }}
                <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                    <div class="col-xs-12">
                        <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="Email Address" autofocus>

                        @if ($errors->has('email'))
                        <span class="help-block">
                            <strong>{{ $errors->first('email') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group{{ $errors->has('password') ? ' has-error' : '' }}">
                    <div class="col-xs-12">
                        <input id="password" type="password" class="form-control" name="password" placeholder="Password">

                        @if ($errors->has('password'))
                        <span class="help-block">
                            <strong>{{ $errors->first('password') }}</strong>
                        </span>
                        @endif
                    </div>
                </div>

                <div class="form-group">
                    <div class="col-xs-12">
                        <button type="submit" class="btn btn-primary btn-block">
                            Login
                        </button>
                    </div>
                </div>
                
                <div class="form-group small-font">
                    <div class="col-xs-6">
                        <label data-toggle="tooltip" data-placement="bottom" title="For your convenience keep this checked but take precaution on shared devices!">
                            <input type="checkbox" name="remember"> <span class="stay-sign-in">Stay sign in</span>
                        </label>
                    </div>
                    <div class="col-xs-6 text-right">
                        <a href="{{ url('/password/reset') }}">
                            Forgot password?
                        </a>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>
@endsection
