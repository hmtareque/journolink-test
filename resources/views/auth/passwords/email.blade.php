@extends('auth.layout')

@section('content')

    <div class="row">
        
        <div class="col-xs-12 col-md-6 col-md-offset-3">
        
            <div class="login-box">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif

                    <form class="form-horizontal" role="form" method="POST" action="{{ route('password.email') }}">
                        {{ csrf_field() }}

                        <div class="form-group{{ $errors->has('email') ? ' has-error' : '' }}">
                           

                            <div class="col-xs-12">
                                <input id="email" type="email" class="form-control" name="email" value="{{ old('email') }}" placeholder="E-Mail Address">

                                @if ($errors->has('email'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('email') }}</strong>
                                    </span>
                                @endif
                                
                                
                                
                            </div>
                        </div>

                        <div class="form-group">
                            <div class="col-xs-12">
                                <button type="submit" class="btn btn-primary btn-block">
                                    Send Password Reset Link
                                </button>
                            </div>
                        </div>
                    </form>
                    
                    <div class="row">
                        <div class="col-xs-12">
                            <div class="alert alert-info">
                                <small>Please provide the email address associated with your account. And it will send a reset link to that email address. 
                                    Or, <a href="{{ url('/login') }}">Recalled password?</a></small>
                            </div>
                        </div>
                    </div>
                                
                </div>
            </div>
        </div>
     
 
@endsection
