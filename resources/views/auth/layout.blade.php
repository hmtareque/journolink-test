<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>{{ config('settings.name') }}</title>

        <link href="{{asset('css/app-requisite.css')}}" rel="stylesheet">
        <link href="{{asset('css/login.css')}}" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        <div class="container app-login-area">
            
            <div class="row">
                <div class="col-xs-12 text-center"><img class="logo" src="{{asset('storage/'.config('settings.logo'))}}" alt="{{config('settings.app_name')}}" title="{{config('settings.app_name')}}"/></div>
            </div>
            
            <div class="row">
                <div class="col-xs-12"><h2 class="app-name">{{config('settings.app_name')}}</h2></div>
            </div>
            
            @yield('content')

            <div class="row">
                <div class="col-xs-12 text-center exetie">
                    <div class="logo"><img src="{{asset('storage/'.config('settings.exetie_logo'))}}"/></div>
                    <div class="moto">{{config('settings.exetie_moto')}}</div>
                </div>
            </div>
        </div>

        <script src="{{asset('js/app-requisite.js')}}"></script>
        <script type="text/javascript">
        $(function () {
            $('[data-toggle="tooltip"]').tooltip()
        });
        </script>
    </body>
</html>
