<div class="header">
    <div class="title">
        <i class="fa fa-users fa-fw"></i> {{trans('label.list_of_items', array('item' => trans('label.users')))}}
    </div>
    <div class="links">
        <a href="#users/create" class="btn btn-sm btn-success"><i class="fa fa-plus"></i> {{trans('label.create_new_item', array('item' => trans('label.user')))}}</a>
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{trans('label.role')}} <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#roles"><i class="fa fa-list"></i> {{trans('label.list_of_items', array('item' => trans('label.roles')))}}</a></li>
                <li><a href="#roles/create"><i class="fa fa-user-plus"></i> {{trans('label.create_new_item', array('item' => trans('label.role')))}}</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12">
        <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->

        <div class="table-responsive">
            <table class="table data-table table-striped table-bordered table-hover" cellspacing="0" width="100%">
                <thead>
                    <tr>
                        <th>{{trans('label.name')}}</th>
                        <th>{{trans('label.designation')}}</th>
                        <th>{{trans('label.contact')}}</th>
                        <th>{{trans('label.last_logged_in_at')}}</th>
                        <th>{{trans('label.status')}}</th>
                        <th>{{trans('label.action')}}</th>
                    </tr>
                </thead>

                <tbody>
                    @foreach($users as $user) 
                    <tr>
                        <td>{{$user->first_name}} {{$user->last_name}}</td>
                        <td>{{$user->designation}}</td>
                        <td>
                            @if($user->email != "") 
                            {{$user->email}}
                            @elseif($user->mobile != "")
                            {{$user->mobile}}
                            @elseif($user->phone != "") 
                            {{$user->phone}}
                            @else 
                            <label class="label label-danger"><i class="fa fa-warning"></i> not found</label>
                            @endif 
                        </td>
                        <td>
                            @if($user->last_logged_in_at)
                            {{date('d M Y H:i A', strtotime($user->last_logged_in_at))}}
                            @else 
                            <label class="label label-danger"><i class="fa fa-warning fa-fw"></i>never</label>
                            @endif 
                        </td>
                        <td>
                            @if($user->status == 'left')
                            <label class="label label-warning"><i class="fa fa-warning"></i> left</label>
                            @elseif($user->active == 1)
                            <label class="label label-success"><i class="fa fa-check"></i> active</label>
                            @elseif($user->active == 0) 
                            
                            <label class="label label-danger"><i class="fa fa-lock"></i> blocked</label>
                            @endif 
                        </td>
                        <td>
                            <a href="#users/{{$user->id}}/edit" class="btn btn-primary btn-xs" data-toggle="tooltip" data-placement="top" title="{{trans('label.edit_item', array('item' => $user->first_name.' '.$user->last_name))}}"><i class="fa fa-pencil"></i></a>
                            <a href="#users/{{$user->id}}/login-details" class="btn btn-success btn-xs" data-toggle="tooltip" data-placement="top" title="Login Account Details"><i class="fa fa-sign-in fa-fw"></i>account</a>

                           
                        </td>
                    </tr>
                    @endforeach 
                </tbody>
            </table>
        </div>

    </div>
</div>







