<div class="header">
    <div class="title"><i class="fa fa-users fa-fw"></i> {{trans('label.users')}}</div>
    <div class="links">
        <a href="#users/create" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> {{trans('label.create_new_item', array('item' => trans('label.user')))}}</a>
        <a href="#users" class="btn btn-primary btn-sm"><i class="fa fa-list"></i> {{trans('label.list_of_items', array('item' => trans('label.users')))}}</a>
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                Access <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#users/{{$user->id}}/access"><i class="fa fa-edit fa-fw"></i> Update Data Access</a></li>
                <li><a href="#users/{{$user->id}}/login-details"><i class="fa fa-edit fa-fw"></i> Update Login Details</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-9">
        
        
         <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->
        
        
        <form class="form-horizontal ajax-form" action="{{url('manage/users/'.$user->id)}}" method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>
            <input type="hidden" name="_method" value="put"/>

            <h4 class="section-title">Personal Details</h4>

            <div class="form-group" id="first_name">
                <label class="col-sm-3 control-label">First Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="first_name" placeholder="First Name" value="{{$user->first_name}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="middle_name">
                <label class="col-sm-3 control-label">Middle Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="middle_name" placeholder="Middle Name" value="{{$user->middle_name}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="last_name">
                <label class="col-sm-3 control-label">Last Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="last_name" placeholder="Last Name" value="{{$user->last_name}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="ni_number">
                <label class="col-sm-3 control-label">NI Number</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="ni_number" placeholder="NI Number" value="{{$user->ni_number}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="date_of_birth">
                <label class="col-sm-3 control-label">Date of Birth</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control jdob" name="date_of_birth" placeholder="Date of Birth" @if($user->date_of_birth != "") value="{{date('d/m/Y', strtotime($user->date_of_birth))}}" @endif>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="gender">
                <label class="col-sm-3 control-label">Gender</label>
                <div class="col-sm-9">
                    <select class="form-control" name="gender">
                        <option value="male" @if($user->gender == 'male') selected="" @endif>Male</option>
                        <option value="female" @if($user->gender == 'female') selected="" @endif>Female</option>
                        <option value="other" @if($user->gender == 'other') selected="" @endif>Other</option>
                    </select>
                    <div id="other_gender">
                        <input type="text" class="form-control @if($user->gender != 'other') hide @endif margin-top-5" name="other_gender" placeholder="Other Gender" value="{{$user->other_gender}}">
                        <span class="help-block hide"></span>
                    </div>
                </div>
            </div>

            <h4 class="section-title">Address</h4>

            <div class="form-group" id="address_line_1">
                <label class="col-sm-3 control-label">Address Line 1</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="address_line_1" placeholder="Address Line 1" value="{{$user->address_line_1}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="address_line_2">
                <label class="col-sm-3 control-label">Address Line 2</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="address_line_2" placeholder="Address Line 2" value="{{$user->address_line_2}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="street">
                <label class="col-sm-3 control-label">Street</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="street" placeholder="Street" value="{{$user->street}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="city">
                <label class="col-sm-3 control-label">City</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="city" placeholder="City" value="{{$user->city}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="postcode">
                <label class="col-sm-3 control-label">Postcode</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="postcode" placeholder="Postcode" value="{{$user->postcode}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <h4 class="section-title">Contacts</h4>

            <div class="form-group" id="phone">
                <label class="col-sm-3 control-label">Phone</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="phone" placeholder="Phone" value="{{$user->phone}}">
                    <span class="help-block hide"></span>
                </div>
            </div>


            <div class="form-group" id="mobile">
                <label class="col-sm-3 control-label">Mobile</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="mobile" placeholder="Mobile" value="{{$user->mobile}}">
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            <h4 class="section-title">Immigration</h4>

            <div class="form-group" id="immigration_status">
                <label class="col-sm-3 control-label">Immigration Status</label>
                <div class="col-sm-9">
                    <select class="form-control" name="immigration_status">
                        <option value=""> -- Please Select -- </option>
                        @foreach($immigration_statuses as $status)
                        <option value="{{$status->name}}" @if($user->immigration_status == $status->name) selected="" @endif>{{$status->name}}</option>
                        @endforeach 
                        <option value="other" @if($user->immigration_status == 'other') selected="" @endif>Other</option>
                    </select>
                    <div id="other_immigration_status">
                        <input type="text" class="form-control @if($user->immigration_status != 'other') hide @endif margin-top-5" name="other_immigration_status" placeholder="Other Immigration Status" value="{{$user->other_immigration_status}}">
                        <span class="help-block hide"></span>
                    </div>
                </div>
            </div>
            
            <div class="form-group" id="passport_no">
                <label class="col-sm-3 control-label">Passport No.</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="passport_no" placeholder="Passport No." value="{{$user->passport_no}}">
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            <div class="form-group" id="next_check_date">
                <label class="col-sm-3 control-label">Visa Expiry Date</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control jdate" name="visa_expiry_date" placeholder="Visa Expiry Date" @if($user->visa_expiry_date != '') value="{{date('d/m/Y', strtotime($user->visa_expiry_date))}}" @endif>
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            
            <h4 class="section-title">Position Details</h4>

            <div class="form-group" id="joined_at">
                <label class="col-sm-3 control-label">Joined At</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control jtoday" name="joined_at" placeholder="Joined At" @if($user->joined_at != "") value="{{date('d/m/Y', strtotime($user->joined_at))}}" @endif>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="designation">
                <label class="col-sm-3 control-label">{{trans('label.designation')}}</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="designation" placeholder="Designation" value="{{$user->designation}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="paid_role">
                <label class="col-sm-3 control-label">Paid</label>
                <div class="col-sm-9">
                    <select class="form-control" id="is-paid" name="has_paid_role">
                        <option value="1" @if($user->has_paid_role == 1) selected='' @endif> Yes </option>
                        <option value="0" @if($user->has_paid_role == 0) selected='' @endif> No </option>
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            <div id="salary-data" class="@if($user->has_paid_role == 0) hide @endif">
            <div class="form-group" id="salary_type">
                <label class="col-sm-3 control-label">Salary Type</label>
                <div class="col-sm-9">
                    <select class="form-control" name="salary_type">
                        <option value=""> -- Please Select -- </option>
                        <option value="daily" @if($user->salary_type == 'daily') selected="" @endif> Per Day</option>
                        <option value="monthly" @if($user->salary_type == 'monthly') selected="" @endif> Per Month </option>
                        <option value="yearly" @if($user->salary_type == 'yearly') selected="" @endif> Per Annum </option>
                        <option value="contract" @if($user->salary_type == 'contract') selected="" @endif> Contract </option>
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="salary_amount">
                <label class="col-sm-3 control-label">Salary Amount</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control text-right" name="salary_amount" placeholder="Amount" value="{{$user->salary_amount}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="salary_description">
                <label class="col-sm-3 control-label">Salary Description</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="salary_description">{{$user->salary_description}}</textarea>
                    <span class="help-block hide"></span>
                </div>
            </div>
            </div>
            
            <h4 class="section-title">DBS Details</h4>
            <div class="form-group" id="dbs_number">
                <label class="col-sm-3 control-label">DBS Number</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="dbs_number" placeholder="DBS Number" value="{{$user->dbs_number}}">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="dbs_issue_date">
                <label class="col-sm-3 control-label">DBS Issue Date</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control jtoday" name="dbs_issue_date" placeholder="DBS Issue Date" @if($user->dbs_issue_date != "") value="{{date('d/m/Y', strtotime($user->dbs_issue_date))}}" @endif/>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-primary"> 
                        <i class="fa fa-save fa-fw"></i> Update
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>




