<div class="header">
    <div class="title"><i class="fa fa-users fa-fw"></i> {{trans('label.users')}}</div>
    <div class="links">
        <a href="#users" class="btn btn-sm btn-success"><i class="fa fa-list"></i> {{trans('label.list_of_items', array('item' => trans('label.users')))}}</a>
        <!-- Single button -->
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{trans('label.role')}} <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#roles"><i class="fa fa-list"></i> {{trans('label.list_of_items', array('item' => trans('label.roles')))}}</a></li>
                <li><a href="#roles/create"><i class="fa fa-plus"></i> {{trans('label.create_new_item', array('item' => trans('label.role')))}}</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-9">
        <form class="form-horizontal ajax-form" action="{{url('manage/users')}}" method="post" data-hash="users">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>

            <h4 class="section-title">Personal Details</h4>

            <div class="form-group" id="first_name">
                <label class="col-sm-3 control-label">First Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="first_name" placeholder="First Name">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="middle_name">
                <label class="col-sm-3 control-label">Middle Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="middle_name" placeholder="Middle Name">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="last_name">
                <label class="col-sm-3 control-label">Last Name</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="last_name" placeholder="Last Name">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="ni_number">
                <label class="col-sm-3 control-label">NI Number</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="ni_number" placeholder="NI Number">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="date_of_birth">
                <label class="col-sm-3 control-label">Date of Birth</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control jdob" name="date_of_birth" placeholder="Date of Birth">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="gender">
                <label class="col-sm-3 control-label">Gender</label>
                <div class="col-sm-9">
                    <select class="form-control" name="gender">
                        <option value="male">Male</option>
                        <option value="female">Female</option>
                        <option value="other">Other</option>
                    </select>
                    <div id="other_gender">
                        <input type="text" class="form-control hide margin-top-5" name="other_gender" placeholder="Other Gender">
                        <span class="help-block hide"></span>
                    </div>
                </div>
            </div>
            
            <h4 class="section-title">Address</h4>

            <div class="form-group" id="address_line_1">
                <label class="col-sm-3 control-label">Address Line 1</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="address_line_1" placeholder="Address Line 1">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="address_line_2">
                <label class="col-sm-3 control-label">Address Line 2</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="address_line_2" placeholder="Address Line 2">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="street">
                <label class="col-sm-3 control-label">Street</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="street" placeholder="Street">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="city">
                <label class="col-sm-3 control-label">City</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="city" placeholder="City">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="postcode">
                <label class="col-sm-3 control-label">Postcode</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="postcode" placeholder="Postcode">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <h4 class="section-title">Contacts</h4>

            <div class="form-group" id="phone">
                <label class="col-sm-3 control-label">Phone</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="phone" placeholder="Phone">
                    <span class="help-block hide"></span>
                </div>
            </div>


            <div class="form-group" id="mobile">
                <label class="col-sm-3 control-label">Mobile</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="mobile" placeholder="Mobile">
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            <h4 class="section-title">Immigration</h4>

            <div class="form-group" id="immigration_status">
                <label class="col-sm-3 control-label">Immigration Status</label>
                <div class="col-sm-9">
                    <select class="form-control" name="immigration_status">
                        <option value=""> -- Please Select -- </option>
                        @foreach($immigration_statuses as $status)
                        <option value="{{$status->name}}">{{$status->name}}</option>
                        @endforeach 
                        <option value="other">Other</option>
                    </select>
                    <div id="other_immigration_status">
                        <input type="text" class="form-control hide margin-top-5" name="other_immigration_status" placeholder="Other Immigration Status">
                        <span class="help-block hide"></span>
                    </div>
                </div>
            </div>
            
            <div class="form-group" id="passport_no">
                <label class="col-sm-3 control-label">Passport No.</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="passport_no" placeholder="Passport No.">
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            <div class="form-group" id="next_check_date">
                <label class="col-sm-3 control-label">Visa Expiry Date</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control jtoday" name="visa_expiry_date" placeholder="Visa Expiry Date">
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            <div class="form-group" id="immigration_note">
                <label class="col-sm-3 control-label">Note</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="immigration_note"></textarea>
                    <span class="help-block hide"></span>
                </div>
            </div>


            <h4 class="section-title">Position Details</h4>

            <div class="form-group" id="joined_at">
                <label class="col-sm-3 control-label">Joined At</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control jtoday" name="joined_at" placeholder="Joined At">
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            <div class="form-group" id="designation">
                <label class="col-sm-3 control-label">{{trans('label.designation')}}</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="designation" placeholder="Designation">
                    <span class="help-block hide"></span>
                </div>
            </div>

            

            <div class="form-group" id="paid_role">
                <label class="col-sm-3 control-label">Paid</label>
                <div class="col-sm-9">
                    <select class="form-control" id="is-paid" name="paid_role">
                        <option value="1"> Yes </option>
                        <option value="0"> No </option>
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            <div id="salary-data" class="">
            <div class="form-group" id="salary_type">
                <label class="col-sm-3 control-label">Salary Type</label>
                <div class="col-sm-9">
                    <select class="form-control" name="salary_type">
                        <option value=""> -- Please Select -- </option>
                        <option value="daily"> Per Day</option>
                        <option value="monthly"> Per Month </option>
                        <option value="yearly"> Per Annum </option>
                        <option value="contract"> Contract </option>
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="salary_amount">
                <label class="col-sm-3 control-label">Salary Amount</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control text-right" name="salary_amount" placeholder="Amount">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="salary_description">
                <label class="col-sm-3 control-label">Salary Description</label>
                <div class="col-sm-9">
                    <textarea class="form-control" name="salary_description"></textarea>
                    <span class="help-block hide"></span>
                </div>
            </div>
            </div>

            <div class="form-group" id="teaching">
                <label class="col-sm-3 control-label">Teaching</label>
                <div class="col-sm-9">
                    <select class="form-control" id="is-teaching" name="teaching">
                        <option value="0"> No </option>
                        <option value="1"> Yes </option>
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>
            
            <div id="teaching-data" class="hide">
            <div class="form-group" id="schools">
                <label class="col-sm-3 control-label">Assign Class(es)</label>
                <div class="col-sm-9">
                    <ul class="list-group margin-top-5">
                            @foreach($schools as $school) 
                            
                                
                                            @foreach($school->classes as $class) 
                                           <li class="list-group-item">
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="schools[{{$school->id}}][]" value="{{$class->id}}" > {{$school->name}} - {{$class->course}} - {{$class->group}}
                                                    </label>
                                                </div>
                                            </li>
                                            @endforeach
                                    

                        
                            @endforeach 
                        </ul>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="dbs_number">
                <label class="col-sm-3 control-label">DBS Number</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="dbs_number" placeholder="DBS Number">
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="dbs_issue_date">
                <label class="col-sm-3 control-label">DBS Issue Date</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control jtoday" name="dbs_issue_date" placeholder="DBS Issue Date">
                    <span class="help-block hide"></span>
                </div>
            </div>
                
                <div class="form-group" id="teacher_docs">
                <label class="col-sm-3 control-label">Teacher Documents</label>
                <div class="col-sm-9">
                    <input type="file" class="btn btn-sm btn-default" name="teacher_docs[]" multiple=""/>
                    <span class="help-block hide"></span>
                </div>
            </div>
                
                
            </div>

            <h4 class="section-title">Data Access</h4>

            <div class="form-group">
                <label class="col-sm-3 control-label">Education Service</label>
                <div class="col-sm-9">
                    <select class="form-control data-access-type" data-access="education-data" name="access[education][area]">
                        <option value="full"> Full </option>
                        <option value="custom"> Custom </option>
                        <option value="none"> None </option>
                    </select>

                    <div class="hide" id="education-data">

                        <select class="form-control margin-top-5" name="access[education][data]">
                            <option value="all"> Can access all users data </option>
                            <option value="user"> Can access only his/her data </option>
                        </select>

                        <ul class="list-group margin-top-5">
                            @foreach($schools as $school) 
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="checkbox" >
                                            <label>
                                                <input type="checkbox" class="service" id="school-{{$school->id}}" name="access[education][services][{{$school->id}}]" value="{{$school->id}}"> {{$school->name}}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <ul class="list-unstyled">
                                            @foreach($school->classes as $class) 
                                            <li>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="access[education][services][{{$school->id}}][categories][]" value="{{$class->id}}" class="service-school-{{$school->id}}-category service-category" data-service="school-{{$school->id}}"> {{$class->course}} - {{$class->group}}
                                                    </label>
                                                </div>
                                            </li>
                                            @endforeach
                                        </ul>

                                    </div>
                                </div>
                            </li>
                            @endforeach 
                        </ul>

                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Family Service</label>
                <div class="col-sm-9">

                    <select class="form-control data-access-type" data-access="family-data" name="access[family][area]">
                        <option value="full"> Full </option>
                        <option value="custom"> Custom </option>
                        <option value="none"> None </option>
                    </select>

                    <div class="hide" id="family-data">

                        <select class="form-control margin-top-5" name="access[family][data]">
                            <option value="all"> Can access all users data </option>
                            <option value="user"> Can access only his/her data </option>
                        </select>

                        <ul class="list-group margin-top-5">
                            @foreach($family_services as $family) 
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" class="service" id="{{$family->id}}" name="access[family][services][{{$family->id}}]" value="{{$family->id}}"> {{$family->name}}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <ul class="list-unstyled">
                                            @foreach($family->categories as $family_category) 
                                            <li>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="access[family][services][{{$family->id}}][categories][]" value="{{$family_category->id}}" class="service-{{$family->id}}-category service-category" data-service="{{$family->id}}"> {{$family_category->name}} @if($family_category->private == 1) <lable class="label label-warning">private</lable>@endif
                                                    </label>
                                                </div>
                                            </li>
                                            @endforeach
                                        </ul>

                                    </div>
                                </div>
                            </li>
                            @endforeach 
                        </ul>

                    </div>
                </div>
            </div>


            <div class="form-group">
                <label class="col-sm-3 control-label">Advocacy Service</label>
                <div class="col-sm-9">

                    <select class="form-control data-access-type" data-access="advocacy-data" name="access[advocacy][area]">
                        <option value="full"> Full </option>
                        <option value="custom"> Custom </option>
                        <option value="none"> None </option>

                    </select>

                    <div class="hide" id="advocacy-data">

                        <select class="form-control margin-top-5" name="access[advocacy][data]">
                            <option value="all"> Can access all users data </option>
                            <option value="user"> Can access only his/her data </option>
                        </select>

                        <ul class="list-group margin-top-5">
                            @foreach($advocacy_services as $advocacy) 
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" class="service" id="{{$advocacy->id}}" name="access[advocacy][services][{{$advocacy->id}}]" value="{{$advocacy->id}}"> {{$advocacy->name}}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <ul class="list-unstyled">
                                            @foreach($advocacy->categories as $advocacy_category) 
                                            <li>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="access[advocacy][services][{{$advocacy->id}}][categories][]" value="{{$advocacy_category->id}}" class="service-{{$advocacy->id}}-category service-category" data-service="{{$advocacy->id}}"> {{$advocacy_category->name}} @if($advocacy_category->private == 1) <lable class="label label-warning">private</lable>@endif
                                                    </label>
                                                </div>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            @endforeach 
                        </ul>
                    </div>
                </div>
            </div>

            <h4 class="section-title">Login Details</h4>
            
            <div class="form-group" id="role">
                <label class="col-sm-3 control-label">{{trans('label.role')}}</label>
                <div class="col-sm-9">
                    <select class="form-control" name="role" id="select-role">
                        <option value="">{{trans('label.please_select')}}</option>
                        @if($roles->count()>0)
                        @foreach($roles as $role)
                        <option value="{{$role->id}}">{{$role->name}}</option>
                        @endforeach 
                        @endif 
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="email">
                <label class="col-sm-3 control-label">{{trans('label.email')}}</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="email" placeholder="{{trans('label.email')}}">
                    <span class="email-help-block help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="password">
                <label class="col-sm-3 control-label">
                    {{trans('label.password')}}
                </label>
                <div class="col-sm-9">
                    <input type="password" class="form-control" name="password" placeholder="{{trans('label.password')}}">
                    <span class="password-help-block help-block hide"></span>
                    <small class="text-primary">Password should be at least 6 character long including one uppercase letter, one lowercase letter, one number and one special character such as & * @ # etc.</small>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-primary"> 
                        <i class="fa fa-save fa-fw"></i> Save
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>




