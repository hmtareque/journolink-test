<div class="header">
    <div class="title"><i class="fa fa-users fa-fw"></i> {{trans('label.users')}}</div>
    <div class="links">
        <a href="#users/create" class="btn btn-success btn-sm"><i class="fa fa-plus"></i> {{trans('label.create_new_item', array('item' => trans('label.user')))}}</a>
            <a href="#users" class="btn btn-primary btn-sm"><i class="fa fa-list"></i> {{trans('label.list_of_items', array('item' => trans('label.users')))}}</a>
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{trans('label.role')}} <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#roles"><i class="fa fa-list"></i> {{trans('label.list_of_items', array('item' => trans('label.roles')))}}</a></li>
                    <li><a href="#roles/create"><i class="fa fa-plus"></i> {{trans('label.create_new_item', array('item' => trans('label.role')))}}</a></li>
                </ul>
        </div>
    </div>
</div>

<div class="col-xs-12 col-md-9">
    
    
    <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->
    
    <form class="form-horizontal ajax-form" action="{{url('manage/users/'.$user->id)}}" method="post">
        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
        <input type="hidden" name="_method" value="put"/>

        <div class="form-group" id="first_name">
            <label class="col-sm-2 control-label">{{trans('label.first_name')}}</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="first_name" placeholder="{{trans('label.first_name')}}" value="{{$user->first_name}}">
                <span class="first_name-help-block help-block hide"></span>
            </div>
        </div>

        <div class="form-group" id="last_name">
            <label class="col-sm-2 control-label">{{trans('label.last_name')}}</label>
            <div class="col-sm-10">
                <input type="text" class="form-control" name="last_name" placeholder="{{trans('label.last_name')}}" value="{{$user->last_name}}">
                <span class="last_name-help-block help-block hide"></span>
            </div>
        </div>

        <div class="form-group" id="role">
            <label class="col-sm-2 control-label">{{trans('label.role')}}</label>
            <div class="col-sm-10">
                <select class="form-control" name="role" id="select-role">
                    <option value="">{{trans('label.please_select')}}</option>
                    @if($roles->count()>0)
                    @foreach($roles as $role)
                    <option value="{{$role->id}}" @if($user->role_id == $role->id) selected="" @endif>{{$role->name}}</option>
                    @endforeach 
                    @endif 
                </select>
                <span class="role-help-block help-block hide"></span>
            </div>
        </div>

        <div class="form-group" id="email">
            <label class="col-sm-2 control-label">{{trans('label.email')}}</label>
            <div class="col-sm-10">
                <input type="email" class="form-control" name="email" placeholder="{{trans('label.email')}}" value="{{$user->email}}">
                <span class="email-help-block help-block hide"></span>
            </div>
        </div>

        <div class="form-group" id="password">
            <label class="col-sm-2 control-label">
                {{trans('label.password')}}
            </label>
            <div class="col-sm-10">
                <input type="password" class="form-control" name="password" placeholder="{{trans('label.password')}}">
                <span class="password-help-block help-block hide"></span>
                <small class="text-primary">Password should be at least 6 character long including one uppercase letter, one lowercase letter, one number and one special character such as & * @ # etc.</small>
            </div>
        </div>

        <div class="form-group">
            <div class="col-sm-offset-2 col-sm-10">
                <button type="submit" class="btn btn-primary"> 
                    <i class="fa fa-save fa-fw"></i> Save
                </button>
            </div>
        </div>
    </form>
</div>













