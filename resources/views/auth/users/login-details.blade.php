<div class="header">
    <div class="title"><i class="fa fa-user fa-fw"></i> User Login Details</div>
    <div class="links">
        <a href="#users" class="btn btn-sm btn-success"><i class="fa fa-list"></i> {{trans('label.list_of_items', array('item' => trans('label.users')))}}</a>

        @if($user->active == 1)

        <button class="btn btn-danger btn-sm" data-toggle="modal" data-target="#block-user-{{$user->id}}">
            <i class="fa fa-lock"></i> Block
        </button>

        <!-- Modal -->
        <div class="modal fade" id="block-user-{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="blockModal" aria-hidden="true" data-backdrop="static">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form class="ajax-form" action="{{url('manage/users/'.$user->id.'/block')}}" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                &times;
                            </button>
                            <h4 class="modal-title" id="myModalLabel">{{trans('label.block_item', array('item' => 'User'))}}</h4>
                        </div>
                        <div class="modal-body">
                            <p>{{trans('alert.sure_to_block_item', array('item' => $user->first_name.' '.$user->last_name))}}</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
                                {{trans('action.cancel')}}
                            </button>
                            <button type="submit" class="btn btn-sm btn-danger">
                                <i class="fa fa-lock"></i>&nbsp; {{trans('action.block')}}
                            </button>
                        </div>
                    </form> 
                </div>
            </div>
        </div>

        @else 

        <button class="btn btn-success btn-sm" data-toggle="modal" data-target="#unblock-user-{{$user->id}}">
            <i class="fa fa-unlock"></i> Unblock
        </button>

        <!-- Modal -->
        <div class="modal fade" id="unblock-user-{{$user->id}}" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" data-backdrop="static">
            <div class="modal-dialog">
                <div class="modal-content">
                    <form class="ajax-form" action="{{url('manage/users/'.$user->id.'/unblock')}}" method="post">
                        <input type="hidden" name="_token" value="{{ csrf_token() }}">
                        <div class="modal-header">
                            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">
                                &times;
                            </button>
                            <h4 class="modal-title" id="myModalLabel">{{trans('label.unblock_item', array('item' => 'User'))}}</h4>
                        </div>
                        <div class="modal-body">
                            <p>{{trans('alert.sure_to_unblock_item', array('item' => $user->first_name.' '.$user->last_name))}}</p>
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-default btn-sm" data-dismiss="modal">
                                {{trans('action.cancel')}}
                            </button>
                            <button type="submit" class="btn btn-sm btn-success">
                                <i class="fa fa-unlock"></i>&nbsp; {{trans('action.unblock')}}
                            </button>
                        </div>
                    </form> 
                </div>
            </div>
        </div>
        @endif 

    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-9">

        <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->

        <form class="form-horizontal ajax-form" action="{{url('manage/users/'.$user->id.'/login-details')}}" method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>

            <h4 class="section-title">Login Details of {{$user->first_name}} {{$user->last_name}}</h4>

            <div class="form-group" id="role">
                <label class="col-sm-3 control-label">{{trans('label.role')}}</label>
                <div class="col-sm-9">
                    <select class="form-control" name="role" id="select-role">
                        <option value="">{{trans('label.please_select')}}</option>
                        @if($roles->count()>0)
                        @foreach($roles as $role)
                        <option value="{{$role->id}}" @if($role->id == $user->role_id) selected='' @endif>{{$role->name}}</option>
                        @endforeach 
                        @endif 
                    </select>
                    <span class="help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="email">
                <label class="col-sm-3 control-label">{{trans('label.email')}}</label>
                <div class="col-sm-9">
                    <input type="text" class="form-control" name="email" placeholder="{{trans('label.email')}}" value="{{$user->email}}">
                    <span class="email-help-block help-block hide"></span>
                </div>
            </div>

            <div class="form-group" id="password">
                <label class="col-sm-3 control-label">
                    {{trans('label.password')}}
                </label>
                <div class="col-sm-9">
                    <input type="password" class="form-control" name="password" placeholder="{{trans('label.password')}}" value="">
                    <span class="password-help-block help-block hide"></span>
                    <small class="text-primary">Password should be at least 6 character long including one uppercase letter, one lowercase letter, one number and one special character such as & * @ # etc.</small>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-primary"> 
                        <i class="fa fa-refresh fa-fw"></i> Update
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>




