<div class="header">
    <div class="title"><i class="fa fa-check fa-fw"></i> {{$user->first_name}} {{$user->last_name}} Data Access</div>
    <div class="links">
        <a href="#users/{{$user->id}}/edit" class="btn btn-sm btn-primary"><i class="fa fa-edit fa-fw"></i> Update {{$user->first_name}} {{$user->last_name}}</a>
        <!-- Single button -->
        <div class="btn-group">
            <button type="button" class="btn btn-sm btn-info dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                {{trans('label.role')}} <span class="caret"></span>
            </button>
            <ul class="dropdown-menu">
                <li><a href="#roles"><i class="fa fa-list"></i> {{trans('label.list_of_items', array('item' => trans('label.roles')))}}</a></li>
                <li><a href="#roles/create"><i class="fa fa-plus"></i> {{trans('label.create_new_item', array('item' => trans('label.role')))}}</a></li>
            </ul>
        </div>
    </div>
</div>

<div class="row">
    <div class="col-xs-12 col-md-9"> 

        <!-- Notify Area -->
        @include('app.shared.flash') 
        <!-- Notfiy Area End -->
        
        <form class="form-horizontal ajax-form" action="{{url('manage/users/'.$user->id.'/access')}}" method="post">
            <input type="hidden" name="_token" value="{{csrf_token()}}"/>

            <div class="form-group">
                <label class="col-sm-3 control-label">Education Service</label>
                <div class="col-sm-9">
                    <select class="form-control data-access-type" data-access="education-data" name="access[education][type]">
                        <option value="full" @if(isset($access['education']['type']) && $access['education']['type'] == 'full') selected='' @endif> All Schools</option>
                        <option value="custom" @if(isset($access['education']['type']) && $access['education']['type'] == 'custom') selected='' @endif> Specified Schools </option>
                        <option value="none" @if(isset($access['education']['type']) && $access['education']['type'] == 'none') selected='' @endif> No School </option>
                    </select>
                    
                    <select class="form-control margin-top-5" name="access[education][data]">
                            <option value="all" @if(isset($access['education']['data']) && $access['education']['data'] == 'all') selected='' @endif> Can access all users data </option>
                            <option value="user" @if(isset($access['education']['data']) && $access['education']['data'] == 'user') selected='' @endif> Can access only his/her data </option>
                        </select>

                    <div class="@if(!isset($access['education']['type']) || $access['education']['type'] != 'custom') hide @endif" id="education-data">

                        

                        <ul class="list-group margin-top-5">
                            @foreach($schools as $school) 
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="checkbox" >
                                            <label>
                                                <input type="checkbox" class="service" id="school-{{$school->id}}" name="access[education][access][{{$school->id}}]" value="{{$school->id}}" @if(isset($access['education']['access']) && is_array($access['education']['access']) && array_key_exists($school->id, $access['education']['access'])) checked @endif> {{$school->name}}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        @if($school->classes)
                                        <ul class="list-unstyled">
                                            @foreach($school->classes as $class) 
                                            <li>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="access[education][access][{{$school->id}}][]" value="{{$class->id}}" class="service-school-{{$school->id}}-category service-category" data-service="school-{{$school->id}}" @if(isset($access['education']['access'][$school->id]) && is_array($access['education']['access'][$school->id]) && in_array($class->id, $access['education']['access'][$school->id])) checked @endif> {{$class->course}} - {{$class->group}}
                                                    </label>
                                                </div>
                                            </li>
                                            @endforeach
                                        </ul>
                                        @endif 

                                    </div>
                                </div>
                            </li>
                            @endforeach 
                        </ul>

                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Family Service</label>
                <div class="col-sm-9">

                    <select class="form-control data-access-type" data-access="family-data" name="access[family][type]">
                        <option value="full" @if(isset($access['family']['type']) && $access['family']['type'] == 'full') selected='' @endif> All Services</option>
                        <option value="custom" @if(isset($access['family']['type']) && $access['family']['type'] == 'custom') selected='' @endif> Specified Services </option>
                        <option value="none" @if(isset($access['family']['type']) && $access['family']['type'] == 'none') selected='' @endif> No Service </option>
                    </select>
                    
                    <select class="form-control margin-top-5" name="access[family][data]">
                            <option value="all" @if(isset($access['family']['data']) && $access['family']['data'] == 'all') selected='' @endif> Can access all users data </option>
                            <option value="user" @if(isset($access['family']['data']) && $access['family']['data'] == 'user') selected='' @endif> Can access only his/her data </option>
                        </select>

                    <div class="@if(!isset($access['family']['type']) || $access['family']['type'] != 'custom') hide @endif" id="family-data">

                        

                        <ul class="list-group margin-top-5">
                            @foreach($family_services as $family) 
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" class="service" id="{{$family->id}}" name="access[family][access][{{$family->id}}]" value="{{$family->id}}" @if(isset($access['family']['access']) && is_array($access['family']['access']) && array_key_exists($family->id, $access['family']['access'])) checked @endif> {{$family->name}}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <ul class="list-unstyled">
                                            @foreach($family->categories as $family_category) 
                                            <li>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="access[family][access][{{$family->id}}][]" value="{{$family_category->id}}" class="service-{{$family->id}}-category service-category" data-service="{{$family->id}}" @if(isset($access['family']['access'][$family->id]) && is_array($access['family']['access'][$family->id]) && in_array($family_category->id, $access['family']['access'][$family->id])) checked @endif> {{$family_category->name}} @if($family_category->private == 1) <lable class="label label-warning">private</lable>@endif
                                                    </label>
                                                </div>
                                            </li>
                                            @endforeach
                                        </ul>

                                    </div>
                                </div>
                            </li>
                            @endforeach 
                        </ul>

                    </div>
                </div>
            </div>

            <div class="form-group">
                <label class="col-sm-3 control-label">Advocacy Service</label>
                <div class="col-sm-9">

                    <select class="form-control data-access-type" data-access="advocacy-data" name="access[advocacy][type]">
                        <option value="full" @if(isset($access['advocacy']['type']) && $access['advocacy']['type'] == 'full') selected='' @endif> All Services</option>
                        <option value="custom" @if(isset($access['advocacy']['type']) && $access['advocacy']['type'] == 'custom') selected='' @endif> Specified Services </option>
                        <option value="none" @if(isset($access['advocacy']['type']) && $access['advocacy']['type'] == 'none') selected='' @endif> No Service </option>
                    </select>
                    
                    <select class="form-control margin-top-5" name="access[advocacy][data]">
                            <option value="all" @if(isset($access['advocacy']['data']) && $access['advocacy']['data'] == 'all') selected='' @endif> Can access all users data </option>
                            <option value="user" @if(isset($access['advocacy']['data']) && $access['advocacy']['data'] == 'user') selected='' @endif> Can access only his/her data </option>
                        </select>

                    <div class="@if(!isset($access['advocacy']['type']) || $access['advocacy']['type'] != 'custom') hide @endif" id="advocacy-data">

                        <ul class="list-group margin-top-5">
                            @foreach($advocacy_services as $advocacy) 
                            <li class="list-group-item">
                                <div class="row">
                                    <div class="col-xs-5">
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox" class="service" id="{{$advocacy->id}}" name="access[advocacy][access][{{$advocacy->id}}]" value="{{$advocacy->id}}" @if(isset($access['advocacy']['access']) && is_array($access['advocacy']['access']) && array_key_exists($advocacy->id, $access['advocacy']['access'])) checked @endif> {{$advocacy->name}}
                                            </label>
                                        </div>
                                    </div>
                                    <div class="col-xs-7">
                                        <ul class="list-unstyled">
                                            @foreach($advocacy->categories as $advocacy_category) 
                                            <li>
                                                <div class="checkbox">
                                                    <label>
                                                        <input type="checkbox" name="access[advocacy][access][{{$advocacy->id}}][]" value="{{$advocacy_category->id}}" class="service-{{$advocacy->id}}-category service-category" data-service="{{$advocacy->id}}" @if(isset($access['advocacy']['access'][$advocacy->id]) && is_array($access['advocacy']['access'][$advocacy->id]) && in_array($advocacy_category->id, $access['advocacy']['access'][$advocacy->id])) checked @endif> {{$advocacy_category->name}} @if($advocacy_category->private == 1) <lable class="label label-warning">private</lable>@endif
                                                    </label>
                                                </div>
                                            </li>
                                            @endforeach
                                        </ul>
                                    </div>
                                </div>
                            </li>
                            @endforeach 
                        </ul>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="col-sm-offset-3 col-sm-9">
                    <button type="submit" class="btn btn-primary"> 
                        <i class="fa fa-refresh fa-fw"></i> Update
                    </button>
                </div>
            </div>
        </form>
    </div>
</div>




