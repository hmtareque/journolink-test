<div class="header">
    <div class="title"><i class="fa fa-lg fa-user-circle-o fa-fw"></i> {{$user->first_name}} {{$user->middle_name}} {{$user->last_name}}</div>
</div>

<div class="row">
    <div class="col-xs-12 col-sm-12 col-md-8">
        <div class="row">
            <div class="col-xs-12">
               

                <div class="panel panel-info">
                    <div class="panel-heading">
                        <h3 class="panel-title">Personal Information</h3>
                    </div>
                    <div class="panel-body">
                        <div class="col-xs-12 col-md-4">
                            <div class="panel panel-success">
                                <div class="panel-body">
                                    <img class="img-responsive img-rounded profile-pic" @if($user->profile_picture) src="{{$user->profile_picture}}" @else src="{{asset('img/avatar.png')}}" @endif/>
                                </div>
                                <div class="panel-footer">
                                    <div id="upload-response"></div>
                                    <form class="form-inline ajax-upload-form"  method="POST" action="{{url('manage/docs/picture')}}" enctype="multipart/form-data">
                                        <input type="hidden" name="_token" value="{{csrf_token()}}"/>
                                        <input type="hidden" name="object_type" value="staff_profile_picture"/>
                                        <input type="hidden" name="object_id" value="{{$user->staff_id}}"/>
                                        <input type="hidden" name="folder" value="staffs/{{$user->staff_id}}/picture"/>
                                        <input type="hidden" name="name" value="Profile Picture"/>
                                        <input type="hidden" name="return" value="manage/profile"/>
                                        <div class="file-upload btn btn-xs btn-success">
                                            <span id="upload-status"><i class="fa fa-refresh fa-fw"></i>Update Picture</span>
                                            <input type="file" name="picture" class="upload upload-picture"/>
                                        </div>
                                    </form>
                                </div>
                            </div>
                        </div>


                        <div class="col-xs-12 col-md-8">
                            <ul class="list-unstyled">
                                <li><strong>Name</strong> : {{$user->first_name}} {{$user->middle_name}}  <strong>{{$user->last_name}}</strong></li>
                                <li><strong>Date of Birth</strong> : {{date('d M Y', strtotime($user->date_of_birth))}}</li>
                                <li>
                                    <strong>Gender</strong> : 
                                    @if($user->gender == 'male') 
                                    <i class="fa fa-male fa-fw text-danger"></i>Male
                                    @elseif($user->gender == 'female') 
                                    <i class="fa fa-female fa-fw text-danger"></i>Female
                                    @elseif($user->gender == 'other') 
                                    <i class="fa fa-info-circle fa-fw text-danger"></i> {{$user->other_gender}}
                                    @endif 

                                </li>
                            </ul>

                            <div class="row">
                                <div class="col-xs-12 col-md-5">
                                    <ul class="list-unstyled">
                                        <li><strong>Address</strong></li>
                                        <li>{{$user->address_line_1}}</li>
                                        @if($user->address_line_2 != '')<li>{{$user->address_line_2}}</li>@endif
                                        <li>{{$user->street}}</li>
                                        <li>{{$user->city}}</li>
                                        <li><strong>{{$user->postcode}}</strong></li>
                                    </ul>
                                </div>
                                <div class="col-xs-12 col-md-7">
                                    <ul class="list-unstyled">
                                        <li><strong>Contact</strong></li>
                                        <li>Phone : @if($user->phone != '') {{$user->phone}} @else <label class="label label-warning">not found</label> @endif</li>
                                        <li>Mobile : @if($user->mobile != '') {{$user->mobile}} @else <label class="label label-warning">not found</label> @endif</li>
                                        <li>Email : @if($user->email != '') <a href="mailto:{{$user->email}}">{{$user->email}}</a> @else <label class="label label-warning">not found</label> @endif</li>
                                    </ul>
                                </div>
                            </div>

                            <ul class="list-unstyled">
                                <li><strong>DBS Number</strong> : @if($user->dbs_number != "") {{$user->dbs_number}} @else <label class="label label-danger">not found</label> @endif</li>
                                <li><strong>DBS Issue Date</strong> : @if($user->dbs_issue_date != "") {{date('d M Y', strtotime($user->dbs_issue_date))}} @else <label class="label label-danger">not found</label> @endif</li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    <div class="col-xs-12 col-sm-12 col-md-4">

        <div class="panel panel-info">
            <div class="panel-heading">
                <h3 class="panel-title">Position</h3>
            </div>
            <div class="panel-body">
                <ul class="list-unstyled">
                    <li><strong>Joined At</strong> : @if($user->joined_at != "") {{date('d M Y', strtotime($user->joined_at))}} @else <label class="label label-danger">not found</label> @endif</li>    
                    <li><strong>Designation</strong> : {{$user->designation}}</li>
                    <li><strong>Paid Role</strong> : @if($user->has_paid_role == 1) <label class="label label-success">yes</label> @else <label class="label label-danger">no</label> @endif</li>
                    <li><strong>Salary Type</strong> : {{$user->salary_type}}</li>
                    <li><strong>Salary</strong> : &pound;{{$user->salary_amount}}</li>
                    @if($user->salary_description != "")<li><small>{{$user->salary_description}}</small></li>@endif
                </ul>
            </div>
        </div>

    </div>

</div>







