<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>Web</title>

        <!-- Bootstrap -->
        <link href="{{asset('css/web-requisite.css')}}" rel="stylesheet">

    
                
                <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
		
	</head>

	<body>

            <header>
                <div class="container-fluid">
                    <div class="container">
                        <nav class="navbar navbar-default">
                            <div class="container-fluid" >
                                <!-- Brand and toggle get grouped for better mobile display -->
                                <div class="navbar-header">
                                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navbar" aria-expanded="false">
                                        <span class="sr-only">Toggle navigation</span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                        <span class="icon-bar"></span>
                                    </button>
                                    <a class="navbar-brand" href="#">AppName</a>
                                </div>

                                <!-- Collect the nav links, forms, and other content for toggling -->
                                <div class="collapse navbar-collapse" id="main-navbar">
                                    <ul class="nav navbar-nav navbar-right">
                                        <li><a href="#">About Us</a></li>
                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Another action</a></li>
                                                <li><a href="#">Something else here</a></li>
                                                <li role="separator" class="divider"></li>
                                                <li><a href="#">Separated link</a></li>
                                            </ul>
                                        </li>

                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Another action</a></li>
                                                <li><a href="#">Something else here</a></li>
                                                <li role="separator" class="divider"></li>
                                                <li><a href="#">Separated link</a></li>
                                            </ul>
                                        </li>

                                        <li class="dropdown">
                                            <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Dropdown <span class="caret"></span></a>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Another action</a></li>
                                                <li><a href="#">Something else here</a></li>
                                                <li role="separator" class="divider"></li>
                                                <li><a href="#">Separated link</a></li>
                                            </ul>
                                        </li>
                                        <li><a href="#">Pricing</a></li>
                                        <li><a href="#">Register</a></li>
                                        <li><a href="{{url('/login')}}">Login</a></li>
                                    </ul>
                                </div>
                            </div>
                        </nav>
                    </div>
                </div>
            </header>
            
            
            
<!-- masterslider -->
<div class="master-slider ms-skin-default" id="masterslider">

    <!-- new slide -->
    <div class="ms-slide slide-1" data-delay="14">
         
        <!-- slide background -->
        <img src="masterslider/blank.gif" data-src="img/slide1/bg.jpg" alt="Slide1 background"/>     
		
		<h3 class="ms-layer hps-title1"
			style="left: 580px; top: 28px;"
			data-type="text"
		>
			The Web's Most Advanced
		</h3>
		
		<h3 class="ms-layer hps-title2"
			style="left: 485px; top: 54px;"
			data-type="text"
		>
			Responsive and Touch Slider
		</h3>
		
		 <img src="masterslider/blank.gif" data-src="img/slide1/ico-4.png" alt="Icon"
		 	  style="left:218px; top:315px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="9200"
		 	  data-duration="2701"
		 	  data-effect="scalefrom(0.5,0.5,449,-39)"
		 	  data-ease="easeOutExpo"
		 />	
		
		 <img src="masterslider/blank.gif" data-src="img/slide1/ico-6.png" alt="Icon"
		 	  style="left:73px; top:196px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="9000"
		 	  data-duration="2701"
		 	  data-effect="scalefrom(0.5,0.5,596,80)"
		 	  data-ease="easeOutExpo"
		 />
		 
		<img src="masterslider/blank.gif" data-src="img/slide1/ico-8.png" alt="Icon"
		 	  style="left:1021px; top:412px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="9800"
		 	  data-duration="2701"
		 	  data-effect="scalefrom(0.5,0.5,-354,-136)"
		 	  data-ease="easeOutExpo"
		 />	
			
		<img src="masterslider/blank.gif" data-src="img/slide1/ico-14.png" alt="Icon"
		 	  style="left:1104px; top:148px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="10000"
		 	  data-duration="2701"
		 	  data-effect="scalefrom(0.5,0.5,-437,128)"
		 	  data-ease="easeOutExpo"
		 />		
		 
		 <img src="masterslider/blank.gif" data-src="img/slide1/ico-15.png" alt="Icon"
		 	  style="left:958px; top:323px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="9900"
		 	  data-duration="2701"
		 	  data-effect="scalefrom(0.5,0.5,-291,-47)"
		 	  data-ease="easeOutExpo"
		 />	
		 
		<img src="masterslider/blank.gif" data-src="img/slide1/ico-16.png" alt="Icon"
		 	  style="left:386px; top:224px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="8800"
		 	  data-duration="2701"
		 	  data-effect="scalefrom(0.5,0.5,456,200)"
		 	  data-ease="easeOutExpo"
		 />	
			
		 <img src="masterslider/blank.gif" data-src="img/slide1/ipad.png" alt="Master slider in ipad"
		 	  style="left:380px; top:64px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-effect="bottom(900)"
		 	  data-duration="4500"
		 	  data-ease="easeOutExpo"
		 />
		 
		 <img src="masterslider/blank.gif" data-src="img/slide1/ipad-screen-bg.jpg" alt="Background of ipad"
		 	  style="left:473px; top:138px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="3400"
		 	  data-effect="scale(0.98,0.98)"
		 />
		 
		 <img src="masterslider/blank.gif" data-src="img/slide1/logo-trans-stroke.png" alt="Transparent stroke"
		 	  style="left:607px; top:214px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="7500"
		 	  data-duration="1000"
		 	  data-effect="scale(0.5,0.5)" 
		 	  data-ease="easeOutBack"
		 />
		 
		  <img src="masterslider/blank.gif" data-src="img/slide1/ipad-reflect.png" alt="iPad reflect"
		 	  style="left:656px; top:103px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-effect="bottom(900)"
		 	  data-duration="4500"
		 	  data-ease="easeOutExpo"
		 />
		
		 
		 
		<img src="masterslider/blank.gif" data-src="img/slide1/ico-1.png" alt="Icon"
		 	  style="left:1260px; top:207px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="9200"
		 	  data-duration="2701"
		 	  data-effect="scalefrom(0.5,0.5,-593,69)"
		 	  data-ease="easeOutExpo"
		 />
		 
		<img src="masterslider/blank.gif" data-src="img/slide1/ico-2.png" alt="Icon"
		 	  style="left:1225px; top:439px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="9000"
		 	  data-duration="2701"
		 	  data-effect="scalefrom(0.5,0.5,-558,-163)"
		 	  data-ease="easeOutExpo"
		 />
		 
		<img src="masterslider/blank.gif" data-src="img/slide1/ico-3.png" alt="Icon"
		 	  style="left:211px; top:76px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="8500"
		 	  data-duration="2701"
		 	  data-effect="scalefrom(0.5,0.5,456,200)"
		 	  data-ease="easeOutExpo"
		 />
		 
		 
		<img src="masterslider/blank.gif" data-src="img/slide1/ico-5.png" alt="Icon"
		 	  style="left:121px; top:346px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="8800"
		 	  data-duration="2701"
		 	  data-effect="scalefrom(0.5,0.5,546,-70)"
		 	  data-ease="easeOutExpo"
		 />
		 
		<img src="masterslider/blank.gif" data-src="img/slide1/ico-7.png" alt="Icon"
		 	  style="left:258px; top:191px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="8600"
		 	  data-duration="2701"
		 	  data-effect="scalefrom(0.5,0.5,409,85)"
		 	  data-ease="easeOutExpo"
		 />	
		
		
		<img src="masterslider/blank.gif" data-src="img/slide1/ico-9.png" alt="Icon"
		 	  style="left:1155px; top:23px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="9400"
		 	  data-duration="2701"
		 	  data-effect="scalefrom(0.5,0.5,-488,253)"
		 	  data-ease="easeOutExpo"
		 />	
		 
		<img src="masterslider/blank.gif" data-src="img/slide1/ico-10.png" alt="Icon"
		 	  style="left:960px; top:165px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="9300"
		 	  data-duration="4001"
		 	  data-effect="scalefrom(0.5,0.5,-293,111)"
		 	  data-ease="easeOutExpo"
		 />	
			
		<img src="masterslider/blank.gif" data-src="img/slide1/ico-11.png" alt="Icon"
		 	  style="left:368px; top:337px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="8700"
		 	  data-duration="2701"
		 	  data-effect="scalefrom(0.5,0.5,299,-61)"
		 	  data-ease="easeOutExpo"
		 />	
				
		<img src="masterslider/blank.gif" data-src="img/slide1/ico-12.png" alt="Icon"
		 	  style="left:247px; top:465px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="8900"
		 	  data-duration="2701"
		 	  data-effect="scalefrom(0.5,0.5,418,-189)"
		 	  data-ease="easeOutExpo"
		 />	
				
		<img src="masterslider/blank.gif" data-src="img/slide1/ico-13.png" alt="Icon"
		 	  style="left:1105px; top:288px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="9100"
		 	  data-duration="2701"
		 	  data-effect="scalefrom(0.5,0.5,-438,-12)"
		 />	
		
		 <img src="masterslider/blank.gif" data-src="img/slide1/logo-white-stroke.png" alt="White stroke"
		 	  style="left:569px; top:175px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="3900"
		 	  data-duration="3000"
		 	  data-effect="scale(0.5,0.5)"
		 	  data-ease="easeOutExpo"
		 />
		 
		 <img src="masterslider/blank.gif" data-src="img/slide1/logo.gif" alt="Master Slider logo in ipad"
		 	  style="left:662px; top:287px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="5100"
		 	  data-duration="1500"
		 	  data-effect="scale(0.8,0.8,c)" 
		 	  data-ease="easeOutBack"
		 />
		 
		
			
		<img src="masterslider/blank.gif" data-src="img/slide1/hand.png" alt="Hand over ipad"
		 	  style="left:687px; top:312px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="5800"
		 	  data-effect="bottom(500,false)"
		 	  data-ease="easeOutExpo"
		 	  data-duration="2400"
		 	  data-hide-time="8210"
		 	  data-hide-ease="easeInExpo"
		 />
		
		

    </div>
    <!-- end of slide -->
   
    <!-- new slide -->
    <div class="ms-slide slide-2" data-delay="10">
         
        <!-- slide background -->
        <img src="masterslider/blank.gif" data-src="img/slide2/bg.png" alt="Slide background"/>     
        
        <h3 class="ms-layer hps-title1"
			style="left: 570px;top: 89px;"
			data-type="text"
			data-delay="1500"
		 	data-duration="2800"
		 	data-ease="easeOutExpo"
		 	data-effect="rotatefront(-40,900,tr)"
		>
			Responsive &amp;
		</h3>
		
		<h3 class="ms-layer hps-title2"
			style="left: 445px;top: 146px;"
			data-type="text"
			data-delay="2200"
		 	data-duration="2000"
		 	data-ease="easeOutExpo"
		 	data-effect="rotate3dright(10,50,0,50)"
		>
			Touch Swipe Navigation
		</h3>
		
        <img src="masterslider/blank.gif" data-src="img/slide2/macbook.png" alt="Master Slider in Macbook"
		 	  style="left:528px; top:265px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="0"
		 	  data-duration="2500"
		 	  data-ease="easeOutExpo"
		 	  data-effect="scalefrom(1.1,1.1,-190,0)"
		 />	
		 
		<img src="masterslider/blank.gif" data-src="img/slide2/imac.png" alt="Master Slider in iMac"
		 	  style="left:814px; top:157px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="600"
		 	  data-duration="2500"
		 	  data-ease="easeOutExpo"
		 	  data-effect="scalefrom(1.1,1.1,190,0)"
		 />	
		 
       <img src="masterslider/blank.gif" data-src="img/slide2/ipad.png" alt="Master Slider in iPad"
		 	  style="left:770px; top:295px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="400"
		 	  data-duration="2500"
		 	  data-ease="easeOutExpo"
		 	  data-effect="scalefrom(1.3,1.3,0,0)"
		/>
		
		<img src="masterslider/blank.gif" data-src="img/slide2/iphone.png" alt="Master Slider in iPhone"
		 	  style="left:146px; top:257px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="3000"
		 	  data-duration="3000"
		 	  data-ease="easeOutExpo"
		 	  data-effect="from(-30,350,false)"
		/>
		
		<img src="masterslider/blank.gif" data-src="img/slide1/hand.png" alt="Hande is touching iPhone"
		 	  style="left:512px; top:326px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="4000"
		 	  data-duration="2500"
		 	  data-ease="easeOutExpo"
		 	  data-effect="from(30,350,false)"
		/>
                 
    </div>
    <!-- end of slide -->
    
    <!-- new slide -->
    <div class="ms-slide slide-3" data-delay="8">
         
        <!-- slide background -->
        <img src="masterslider/blank.gif" data-src="img/slide3/bg.jpg" alt="Slide3 Background"/>     
		
		 <h3 class="ms-layer hps-title1"
			style="left:660px;top:136px;"
			data-type="text"
			data-ease="easeOutExpo"
			data-delay="2000"
		 	data-duration="1400"
		 	data-effect="skewleft(30,80)"
		>
			Hardware
		</h3>
		
		<h3 class="ms-layer hps-amp"
			style="left: 1063px;top: 114px;"
			data-type="text"
			data-delay="2800"
		 	data-duration="1400"
		 	data-effect="scale(2.8,2.8)"
		 	data-ease="easeOutExpo"
		>
			&amp;
		</h3>
		
		 <h3 class="ms-layer hps-title2"
			style="left: 950px;top: 150px;"
			data-type="text"
			data-delay="2500"
		 	data-duration="1400"
		 	data-effect="skewright(30,80)"
		 	data-ease="easeOutExpo"
		>
			Accelerated
		</h3>
		
		
		<h3 class="ms-layer hps-title3"
			style="left: 662px;top: 200px;"
			data-type="text"
			data-delay="2900"
		 	data-duration="2400"
		 	data-effect="rotate3dtop(-100,0,0,40,t)"
		 	data-ease="easeOutExpo"
		>
			Super Smooth
		</h3>
		
		<h3 class="ms-layer hps-title4"
			style="left: 662px;top: 257px;"
			data-type="text"
			data-delay="3100"
		 	data-duration="2800"
		 	data-effect="rotate3dtop(-100,0,0,18,t)"
		 	data-ease="easeOutExpo"
		>
			Transitions
		</h3>
		
		
		<img src="masterslider/blank.gif" data-src="img/slide3/part-1.png" alt="Snow particle 1"
		 	  style="left:426px; top:252px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-ease="easeOutExpo"
		 	  data-duration="8000"
		 	  data-delay="200"
		 	  data-effect="scalefrom(0.4,0.4,180,300,br)"
		/>
		
		<img src="masterslider/blank.gif" data-src="img/slide3/part-2.png" alt="Snow particle 2"
		 	  style="left:424px; top:261px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-ease="easeOutExpo"
		 	  data-delay="150"
		 	  data-duration="8200"
		 	  data-effect="scalefrom(0.3,0.3,180,300,br)"
		/>
		
		<img src="masterslider/blank.gif" data-src="img/slide3/part-3.png" alt="Snow particle 3"
		 	  style="left:401px; top:241px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-ease="easeOutExpo"
		 	  data-duration="7400"
		 	  data-effect="scalefrom(0.3,0.3,180,300,br)"
		 	  
		/>
		
		<img src="masterslider/blank.gif" data-src="img/slide3/cool-guy.png" alt="A guy with snowboard"
		 	  style="left:345px; top:103px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="0"
		 	  data-ease="easeOutExpo"
		 	  data-duration="5000"
		 	  data-effect="scalefrom(0.7,0.7,380,450,br,false)"
		/>
		
		 <div class="ms-layer" style="left:416px; top:135px;"
			data-delay="4010"
			data-type="hotspot"
			data-align="left"
		>
			<div class="product-tt"><h3>LOREM IPSUM DOLOR</h3>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.</div>
		</div>
		
		 <div class="ms-layer" style="left:408px; top:222px;"
			data-delay="4100"
			data-type="hotspot"
			data-align="bottom"
		>
			<div class="product-tt"><h3>LOREM IPSUM DOLOR</h3>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.</div>
		</div>
		
		 <div class="ms-layer" style="left:565px; top:265px;"
			data-delay="4050"
			data-type="hotspot"
			data-align="right"
		>
			<div class="product-tt"><h3>LOREM IPSUM DOLOR</h3>Lorem ipsum dolor sit amet, consectetuer adipiscing elit, sed diam nonummy nibh euismod tincidunt.</div>
		</div>
		
		
    </div>
    <!-- end of slide -->
 
 	<!-- new slide -->
    <div class="ms-slide slide-4" data-delay="11.5">
         
        <!-- slide background -->
        <img src="masterslider/blank.gif" data-src="img/slide4/bg.jpg" alt="Slide3 Background"/>     
		
		 <h3 class="ms-layer hps-title1"
			style="left: 641px;top: 70px;"
			data-type="text"
			data-delay="7500"
			data-duration="2500"
			data-ease="easeOutExpo"
			data-effect="rotate3dleft(90,0,0,100)"
			
		>
			25+ Starter Templates
		</h3>
		
		<img src="masterslider/blank.gif" data-src="img/slide4/two.png" alt="Two of 25+"
		 	  style="left:327px; top:95px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="0"
		 	  data-duration="1800"
		 	  data-effect="bottom(480,false)"
		 	  data-ease="easeOutExpo"
		/>
		
		<img src="masterslider/blank.gif" data-src="img/slide4/five.png" alt="Five of 25+"
		 	  style="left:591px; top:112px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="350"
		 	  data-duration="1800"
		 	  data-effect="bottom(480,false)"
		 	  data-ease="easeOutExpo"
		/>
		
		<img src="masterslider/blank.gif" data-src="img/slide4/plus.png" alt="Plus of 25+"
		 	  style="left:854px; top:178px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="700"
		 	  data-duration="1800"
		 	  data-effect="bottom(480,false)"
		 	  data-ease="easeOutExpo"
		/>
		
		<!-- Two Particles -->
		
		<img src="masterslider/blank.gif" data-src="img/slide4/two-9.png" alt="Particle"
		 	  style="left:461px; top:400px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="0"
		 	  data-duration="1800"
		 	  data-effect="bottom(480,false)"
		 	  data-ease="easeOutExpo"
		 	  data-hide-duration="800"
		 	  data-hide-time="4110"
		 	  data-hide-ease="easeInQuart"
		 	  data-hide-effect="rotatebottom(-150|150,780,t,false)"
		/>
		
		<img src="masterslider/blank.gif" data-src="img/slide4/two-8.png" alt="Particle"
		 	  style="left:392px; top:338px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="0"
		 	  data-duration="1800"
		 	  data-effect="bottom(480,false)"
		 	  data-ease="easeOutExpo"
		 	  data-hide-duration="800"
		 	  data-hide-time="3910"
		 	  data-hide-ease="easeInQuart"
		 	  data-hide-effect="rotatebottom(-150|150,780,t,false)"
		/>
		
		<img src="masterslider/blank.gif" data-src="img/slide4/two-7.png" alt="Particle"
		 	  style="left:485px; top:390px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="0"
		 	  data-duration="1800"
		 	  data-effect="bottom(480,false)"
		 	  data-ease="easeOutExpo"
		 	  data-hide-duration="800"
		 	  data-hide-time="3710"
		 	  data-hide-ease="easeInQuart"
		 	  data-hide-effect="rotatebottom(-150|150,780,t,false)"
		/>
		
		<img src="masterslider/blank.gif" data-src="img/slide4/two-6.png" alt="Particle"
		 	  style="left:332px; top:359px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="0"
		 	  data-duration="1800"
		 	  data-effect="bottom(480,false)"
		 	  data-ease="easeOutExpo"
		 	  data-hide-duration="800"
		 	  data-hide-time="3510"
		 	  data-hide-ease="easeInQuart"
		 	  data-hide-effect="rotatebottom(-150|150,780,t,false)"
		/>
		
		
		<img src="masterslider/blank.gif" data-src="img/slide4/two-5.png" alt="Particle"
		 	  style="left:333px; top:357px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="0"
		 	  data-duration="1800"
		 	  data-effect="bottom(480,false)"
		 	  data-ease="easeOutExpo"
		 	  data-hide-duration="800"
		 	  data-hide-time="3310"
		 	  data-hide-ease="easeInQuart"
		 	  data-hide-effect="rotatebottom(-150|150,780,t,false)"
		/>
		
		<img src="masterslider/blank.gif" data-src="img/slide4/two-4.png" alt="Particle"
		 	  style="left:389px; top:194px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="0"
		 	  data-duration="1800"
		 	  data-effect="bottom(480,false)"
		 	  data-ease="easeOutExpo"
		 	  data-hide-duration="800"
		 	  data-hide-time="3110"
		 	  data-hide-ease="easeInQuart"
		 	  data-hide-effect="rotatebottom(-150|150,780,t,false)"
		/>
		
		<img src="masterslider/blank.gif" data-src="img/slide4/two-3.png" alt="Particle"
		 	  style="left:537px; top:162px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="0"
		 	  data-duration="1800"
		 	  data-effect="bottom(480,false)"
		 	  data-ease="easeOutExpo"
		 	  data-hide-duration="800"
		 	  data-hide-time="2910"
		 	  data-hide-ease="easeInQuart"
		 	  data-hide-effect="rotatebottom(-150|150,780,t,false)"
		/>
		
		<img src="masterslider/blank.gif" data-src="img/slide4/two-2.png" alt="Particle"
		 	  style="left:430px; top:111px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="0"
		 	  data-duration="1800"
		 	  data-effect="bottom(480,false)"
		 	  data-ease="easeOutExpo"
		 	  data-hide-duration="800"
		 	  data-hide-time="2710"
		 	  data-hide-ease="easeInQuart"
		 	  data-hide-effect="rotatebottom(-150|150,780,t,false)"
		/>
		
		<img src="masterslider/blank.gif" data-src="img/slide4/two-1.png" alt="Particle"
		 	  style="left:337px; top:112px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="0"
		 	  data-duration="1800"
		 	  data-effect="bottom(480,false)"
		 	  data-ease="easeOutExpo"
		 	  data-hide-duration="800"
		 	  data-hide-time="2510"
		 	  data-hide-ease="easeInQuart"
		 	  data-hide-effect="rotatebottom(-150|150,780,t,false)"
		 	  
		/>
		
		<!-- Five Particles -->
		
		<img src="masterslider/blank.gif" data-src="img/slide4/five-1.png" alt="Particle"
		 	  style="left:637px; top:114px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="350"
		 	  data-duration="1800"
		 	  data-effect="bottom(480,false)"
		 	  data-ease="easeOutExpo"
		 	  data-hide-duration="800"
		 	  data-hide-time="5310"
		 	  data-hide-ease="easeInQuart"
		 	  data-hide-effect="rotatebottom(-150|150,780,t,false)"
		/>
		
		<img src="masterslider/blank.gif" data-src="img/slide4/five-2.png" alt="Particle"
		 	  style="left:615px; top:130px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="350"
		 	  data-duration="1800"
		 	  data-effect="bottom(480,false)"
		 	  data-ease="easeOutExpo"
		 	  data-hide-duration="800"
		 	  data-hide-time="5110"
		 	  data-hide-ease="easeInQuart"
		 	  data-hide-effect="rotatebottom(-150|150,780,t,false)"
		/>
		
		<img src="masterslider/blank.gif" data-src="img/slide4/five-3.png" alt="Particle"
		 	  style="left:610px; top:226px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="350"
		 	  data-duration="1800"
		 	  data-effect="bottom(480,false)"
		 	  data-ease="easeOutExpo"
		 	  data-hide-duration="800"
		 	  data-hide-time="4910"
		 	  data-hide-ease="easeInQuart"
		 	  data-hide-effect="rotatebottom(-150|150,780,t,false)"
		/>
		
		<img src="masterslider/blank.gif" data-src="img/slide4/five-4.png" alt="Particle"
		 	  style="left:649px; top:206px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="350"
		 	  data-duration="1800"
		 	  data-effect="bottom(480,false)"
		 	  data-ease="easeOutExpo"
		 	  data-hide-duration="800"
		 	  data-hide-time="4710"
		 	  data-hide-ease="easeInQuart"
		 	  data-hide-effect="rotatebottom(-150|150,780,t,false)"
		/>
		
		<img src="masterslider/blank.gif" data-src="img/slide4/five-5.png" alt="Particle"
		 	  style="left:716px; top:288px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="350"
		 	  data-duration="1800"
		 	  data-effect="bottom(480,false)"
		 	  data-ease="easeOutExpo"
		 	  data-hide-duration="800"
		 	  data-hide-time="4510"
		 	  data-hide-ease="easeInQuart"
		 	  data-hide-effect="rotatebottom(-150|150,780,t,false)"
		/>
		
		<img src="masterslider/blank.gif" data-src="img/slide4/five-6.png" alt="Particle"
		 	  style="left:591px; top:316px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="350"
		 	  data-duration="1800"
		 	  data-effect="bottom(480,false)"
		 	  data-ease="easeOutExpo"
		 	  data-hide-duration="800"
		 	  data-hide-time="4310"
		 	  data-hide-ease="easeInQuart"
		 	  data-hide-effect="rotatebottom(-150|150,780,t,false)"
		/>
		
		<!-- Plus Particles -->
		
		<img src="masterslider/blank.gif" data-src="img/slide4/plus-1.png" alt="Particle"
		 	  style="left:864px; top:180px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="700"
		 	  data-duration="1800"
		 	  data-effect="bottom(480,false)"
		 	  data-ease="easeOutExpo"
		 	  data-hide-duration="800"
		 	  data-hide-time="5510"
		 	  data-hide-ease="easeInQuart"
		 	  data-hide-effect="rotatebottom(-150|150,780,t,false)"
		/>
		
		<img src="masterslider/blank.gif" data-src="img/slide4/plus-2.png" alt="Particle"
		 	  style="left:961px; top:180px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="700"
		 	  data-duration="1800"
		 	  data-effect="bottom(480,false)"
		 	  data-ease="easeOutExpo"
		 	  data-hide-duration="800"
		 	  data-hide-time="5710"
		 	  data-hide-ease="easeInQuart"
		 	  data-hide-effect="rotatebottom(-150|150,780,t,false)"
		/>
		
		<img src="masterslider/blank.gif" data-src="img/slide4/plus-3.png" alt="Particle"
		 	  style="left:980px; top:357px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="700"
		 	  data-duration="1800"
		 	  data-effect="bottom(480,false)"
		 	  data-ease="easeOutExpo"
		 	  data-hide-duration="800"
		 	  data-hide-time="5910"
		 	  data-hide-ease="easeInQuart"
		 	  data-hide-effect="rotatebottom(-150|150,780,t,false)"
		/>
		
		<img src="masterslider/blank.gif" data-src="img/slide4/plus-4.png" alt="Particle"
		 	  style="left:864px; top:180px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="700"
		 	  data-duration="1800"
		 	  data-effect="bottom(480,false)"
		 	  data-ease="easeOutExpo"
		 	  data-hide-duration="800"
		 	  data-hide-time="6110"
		 	  data-hide-ease="easeInQuart"
		 	  data-hide-effect="rotatebottom(-150|150,780,t,false)"
		/>

    </div>
    <!-- end of slide -->
 
 	<!-- new slide -->
    <div class="ms-slide slide-5" data-delay="23">
         
        <!-- slide background -->
        <img src="masterslider/blank.gif" data-src="img/slide5/bg.jpg" alt="Slide3 Background"/>     
		
		<img src="masterslider/blank.gif" data-src="img/slide5/plane1.png" alt="Layers plane"
		 	  style="left:141px; top:151px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-effect="left(200)"
		 	  data-ease="easeOutQuart"
		 	  data-duration="2200"
		/>
		
		<img src="masterslider/blank.gif" data-src="img/slide5/plane2.png" alt="Layers plane"
		 	  style="left:87px; top:157px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-effect="left(200)"
		 	  data-ease="easeOutQuart"
		 	  data-duration="2200"
		 	  data-hide-effect="top(53,false)"
		 	  data-hide-time="2301"
		 	  data-hide-duration="4500"
		 	  data-hide-ease="easeInOutQuart"
		 	  
		/>
		
		<img src="masterslider/blank.gif" data-src="img/slide5/plane3.png" alt="Layers plane"
		 	  style="left:88px; top:133px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-effect="left(200)"
		 	  data-ease="easeOutQuart"
		 	  data-duration="2200"
		 	  data-hide-effect="top(105,false)"
		 	  data-hide-time="2301"
		 	  data-hide-duration="4500"
		 	  data-hide-ease="easeInOutQuart"
		/>
		
		<img src="masterslider/blank.gif" data-src="img/slide5/tooltip1.png" alt="Animated Layers"
		 	  style="left:97px; top:97px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-effect="top(400,false)"
		 	  data-duration="2100"
		 	  data-ease="easeOutQuart"
		 	  data-delay="6400"
			  data-hide-time="9000"
			  data-hide-duration="800"
			  data-hide-effect="from(-90,50)"
			  data-hide-ease="easeInQuad"
		/>
		
		<img src="masterslider/blank.gif" data-src="img/slide5/tooltip-shadow.png" alt="Tooltip Shadow"
		 	  style="left:102px; top:262px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-duration="2100"
		 	  data-ease="easeOutQuart"
		 	  data-delay="6400"
			  data-hide-time="9000"
			  data-hide-duration="800"
			  data-hide-effect="from(-90,50)"
			  data-hide-ease="easeInQuad"
		/>
		
		<img src="masterslider/blank.gif" data-src="img/slide5/image-layer.png" alt="Image Layers"
		 	  style="left:97px; top:97px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-effect="top(400,false)"
		 	  data-duration="2100"
		 	  data-ease="easeOutQuart"
		 	  data-delay="9100"
			  data-hide-time="12300"
			  data-hide-duration="800"
			  data-hide-effect="from(-90,50)"
			  data-hide-ease="easeInQuad"
		/>
		
		<img src="masterslider/blank.gif" data-src="img/slide5/tooltip-shadow.png" alt="Tooltip Shadow"
		 	  style="left:102px; top:262px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-duration="2100"
		 	  data-ease="easeOutQuart"
		 	  data-delay="9100"
			  data-hide-time="12300"
			  data-hide-duration="800"
			  data-hide-effect="from(-90,50)"
			  data-hide-ease="easeInQuad"
		/>
		
		<img src="masterslider/blank.gif" data-src="img/slide5/video-layer.png" alt="Video Layers"
		 	  style="left:97px; top:97px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-effect="top(400,false)"
		 	  data-duration="2100"
		 	  data-ease="easeOutQuart"
		 	  data-delay="12800"
		 	  data-hide-time="15600"
			  data-hide-duration="800"
			  data-hide-effect="from(-90,50)"
			  data-hide-ease="easeInQuad"
		/>
		
		<img src="masterslider/blank.gif" data-src="img/slide5/tooltip-shadow.png" alt="Tooltip Shadow"
		 	  style="left:102px; top:262px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-duration="2100"
		 	  data-ease="easeOutQuart"
		 	  data-delay="12800"
		 	  data-hide-time="15600"
			  data-hide-duration="800"
			  data-hide-effect="from(-90,50)"
			  data-hide-ease="easeInQuad"
		/>
		
		<img src="masterslider/blank.gif" data-src="img/slide5/html-layer.png" alt="HTML Layers"
		 	  style="left:97px; top:97px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-effect="top(400,false)"
		 	  data-duration="2100"
		 	  data-ease="easeOutQuart"
		 	  data-delay="15700"
		 	  data-hide-time="18400"
			  data-hide-duration="800"
			  data-hide-effect="from(-90,50)"
			  data-hide-ease="easeInQuad"
		/>
		
		<img src="masterslider/blank.gif" data-src="img/slide5/tooltip-shadow.png" alt="Tooltip Shadow"
		 	  style="left:102px; top:262px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-duration="2100"
		 	  data-ease="easeOutQuart"
		 	  data-delay="15700"
		 	  data-hide-time="18400"
			  data-hide-duration="800"
			  data-hide-effect="from(-90,50)"
			  data-hide-ease="easeInQuad"
		/>
		
		<img src="masterslider/blank.gif" data-src="img/slide5/text-layer.png" alt="Text Layers"
		 	  style="left:97px; top:97px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-effect="top(400,false)"
		 	  data-duration="2100"
		 	  data-ease="easeOutQuart"
		 	  data-delay="18500"
		 	 
		/>
		
		<img src="masterslider/blank.gif" data-src="img/slide5/tooltip-shadow.png" alt="Tooltip Shadow"
		 	  style="left:102px; top:262px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-duration="2100"
		 	  data-ease="easeOutQuart"
		 	  data-delay="18500"
		 	 
		/>
		
		<img src="masterslider/blank.gif" data-src="img/slide5/infinit.png" alt="char"
		 	  style="left:1057px; top:175px;"
		 	  class="ms-layer"
		 	  data-type="image"
		 	  data-delay="7600"
		 	  data-effect="rotatefront(40,900)"
		 	  data-ease="easeOutExpo"
		 	  data-duration="1500"
		/>
		
		<h3 class="ms-layer hps-title1"
			style="left: 881px;top: 193px;"
			data-type="text"
			data-delay="6300"
			data-effect="skewleft(50,340)"
			data-ease="easeOutExpo"
			data-duration="2200"
		>
			Unlimited
		</h3>
		
		<h3 class="ms-layer hps-title2"
			style="left: 883px;top: 233px;"
			data-type="text"
			data-delay="6300"
			data-effect="skewright(50,340)"
			data-ease="easeOutExpo"
			data-duration="2200"
		>
			Transition Effects
		</h3>
		
		<h3 class="ms-layer hps-title3"
			style="left: 883px;top: 287px;"
			data-type="text"
			data-delay="7000"
		 	data-effect="rotate3dbottom(100,0,0,70)"
		 	data-ease="easeOutExpo"
		 	data-duration="2300"
		>
			In &amp; Out Transitions Supported
		</h3>
		
		<p class="ms-layer hps-text1"
			style="left: 885px; top: 350px;"
			data-type="text"
			data-delay="7800"
			data-ease="easeOutExpo"
			data-effect="left(50)"
		 	data-duration="2300"
		>
			You can easily add image, html formatted texts and video layers over each slide and each layer accepts unique animation parameters like animation effect, duration, delay, etc.
		</p>
		
    </div>
    <!-- end of slide -->
    
 	<!-- new slide -->
    <div class="ms-slide slide-6" data-delay="9">
         
        <!-- slide background -->
        <img src="masterslider/blank.gif" data-src="img/slide6/bg.jpg" alt="Slide3 Background"/>     
        
		<h3 class="ms-layer hps-title1"
			style="left: 1032px;top: 197px;"
			data-type="text"
			data-delay="1500"
			data-effect="left(250)"
			data-ease="easeInOutQuart"
			data-duration="2400"
		>
			Video Layer <br /><span class="sub-title">Support</span>
		</h3>
		
		<h3 class="ms-layer hps-title2"
			style="left: 1032px;top: 287px;"
			data-type="text"
			data-delay="1650"
			data-effect="left(250)"
			data-ease="easeInOutQuart"
			data-duration="2400"
			
		>
			with Custom Video Cover
		</h3>
		
		<h3 class="ms-layer hps-title3"
			style="left: 1032px;top: 320px;"
			data-type="text"
			data-delay="1750"
			data-effect="left(250)"
			data-ease="easeInOutQuart"
			data-duration="2400"
			
		>
			YouTube
		</h3>
		
		<h3 class="ms-layer hps-title3 hps-title3-vimeo"
			style="left: 1032px;top: 372px;"
			data-type="text"
			data-delay="1850"
			data-effect="left(250)"
			data-ease="easeInOutQuart"
			data-duration="2400"
		>
			Vimeo
		</h3>
		
		<img src="masterslider/blank.gif" data-src="img/slide6/video-shadow.png" alt="video shadow"
		 	  style="left:298px; top:435px;"
		 	  class="ms-layer"
		 	  data-type="image"
			  data-duration="3000"
			  data-ease="easeOutExpo"
		/>
		
		<div class="ms-layer video-box" style="left:370px; top:105px; width:662px; height:372px"
			  data-type="video"
			  data-effect="top(100)"
			  data-duration="3000"
			  data-ease="easeOutExpo"
		>
			<img src="masterslider/blank.gif" data-src="img/slide6/video-cover.jpg" alt="video shadow"/>
			<iframe src="http://player.vimeo.com/video/50672540" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen> </iframe>
		</div>
		
		
    </div>
    <!-- end of slide -->
    
    	<!-- new slide -->
    <div class="ms-slide slide-7" data-delay="6">
         
        <!-- slide background -->
        <img src="masterslider/blank.gif" data-src="img/slide7/bg.jpg" alt="Slide3 Background"/>     
		<a href="http://player.vimeo.com/video/58226214" data-type="video">REDBULL</a>
		
		<h3 class="ms-layer hps-title1"
			style="left: 830px; top: 220px;"
			data-type="text"
			data-duration="1400"
		 	data-ease="easeOutExpo"
		 	data-effect="rotate3dtop(90,0,0,80)"
		>
			Fullscreen
		</h3>
		
		<h3 class="ms-layer hps-title2"
			style="left: 835px;top: 284px;"
			data-type="text"
			data-duration="1800"
			data-delay="140"
		 	data-ease="easeOutExpo"
		 	data-effect="bottom(40)"
		>
			Video Support
		</h3>
		
		<h3 class="ms-layer hps-title3"
			style="left: 835px;top: 318px;"
			data-type="text"
			data-delay="1000"
			data-ease="easeOutExpo"
			data-duration="1230"
			data-effect="scale(1.5,1.6)"
		>
			with Custom Video Cover
		</h3>
		
    </div>
    <!-- end of slide -->
</div>




<div class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-xs-12" style="background: #7A7A7A; height: 250px;">
                
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-xs-12" style="background: #66afe9; height: 450px;">
                
            </div>
        </div>
    </div>
</div>

<div class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-xs-12" style="background: #008200; height: 180px;">
                <div class="owl-carousel owl-theme">
    <div class="item"><div style="height: 100px; width: 150px; background: red;"><i class="fa fa-close"></i></div></div>
    <div class="item"><h4>2</h4></div>
    <div class="item"><div style="height: 100px; width: 150px; background: red;"><i class="fa fa-close"></i></div></div>
    <div class="item"><h4>4</h4></div>
    <div class="item"><div style="height: 100px; width: 150px; background: red;"></div></div>
    <div class="item"><h4>6</h4></div>
    <div class="item"><div style="height: 100px; width: 150px; background: red;"></div></div>
    <div class="item"><h4>8</h4></div>
    <div class="item"><div style="height: 100px; width: 150px; background: red;"></div></div>
    <div class="item"><h4>10</h4></div>
    <div class="item"><div style="height: 100px; width: 150px; background: red;"></div></div>
    <div class="item"><h4>12</h4></div>
</div>
            </div>
        </div>
    </div>
</div>


<div class="container-fluid">
    <div class="container">
        <div class="row">
            <div class="col-xs-12" style="background: #3f3f3f; height: 300px;">
                
            </div>
        </div>
    </div>
</div>








<script src="{{asset('js/web-requisite.js')}}"></script>
	
	<script type="text/javascript">		
	    var slider = new MasterSlider();

	    // adds Arrows navigation control to the slider.
	    slider.control('arrows');
	    slider.control('timebar' , {insertTo:'#masterslider'});
	    slider.control('bullets');

	     slider.setup('masterslider' , {
	         width:1400,    // slider standard width
	         height:580,   // slider standard height
	         space:1,
	         layout:'fullwidth',
	         loop:true,
	         preload:0,
	         instantStartLayers:true,
	         autoplay:true
	    });
            
            
            $('.owl-carousel').owlCarousel({
    loop:true,
    margin:10,
  //  nav:true,
    autoplay:true,
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:5
        }
    }
});

	</script>
		</body>
</html>
