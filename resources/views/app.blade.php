<!DOCTYPE html>
<html lang="{{ config('app.locale') }}">
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
        <title>App</title>
        
        
        
        <style>
            
            .sidebar {
                margin: 0px !important;
                padding: 0px !important;
                
            }
            
            .sidebar-list {
                list-style: none;
                margin: 0px;
                padding: 0px;
            }
            
            .sidebar-list li {
                margin: 0px;
                padding: 0px;
                display: block;
                overflow: hidden;
            }
            
            .sidebar-list li a {
                float: left;
                padding: 10px 15px;
                width: 100%;
                border-bottom: 1px solid #ddd;
            }
            
            
            
        </style>

        <!-- Bootstrap -->
        <link href="{{asset('css/app.css')}}" rel="stylesheet">

        <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
        <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
        <!--[if lt IE 9]>
          <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
          <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
        <![endif]-->
    </head>
    <body>
        
        <header style="margin-top: 50px;">
        
            <nav class="navbar navbar-default navbar-fixed-top">
            <div class="container-fluid">
              <!-- Brand and toggle get grouped for better mobile display -->
              <div class="navbar-header">
                <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navbar" aria-expanded="false">
                  <span class="sr-only">Toggle navigation</span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                  <span class="icon-bar"></span>
                </button>
                <a class="navbar-brand" href="#" style="color: #fff;">Brand</a>
              </div>

              <!-- Collect the nav links, forms, and other content for toggling -->
              <div class="collapse navbar-collapse" id="main-navbar">
                <ul class="nav navbar-nav">
                  <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i> Menu 1 <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">Action</a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="#">Separated link</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="#">One more separated link</a></li>
                    </ul>
                  </li>

                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bicycle"></i> Menu 2 <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">Action</a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="#">Separated link</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="#">One more separated link</a></li>
                    </ul>
                  </li>

                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-unlock"></i> Menu 3 <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">Action</a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="#">Separated link</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="#">One more separated link</a></li>
                    </ul>
                  </li>

                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cog"></i> Menu 4 <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">Action</a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="#">Separated link</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="#">One more separated link</a></li>
                    </ul>
                  </li>

                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-cogs"></i> Settings <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">Action</a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="#">Separated link</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="#">One more separated link</a></li>
                    </ul>
                  </li>
                </ul>

                <ul class="nav navbar-nav navbar-right">
                    
                    
                    
                    <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-tasks" style="font-size: 1.0em; color: #fff;"></i><sup style="background: green; padding: 2px 5px; color: #fff; font-size: 0.7em; border-radius: 10px; margin-left: -2px; margin-top: -10px;">5</sup></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">Action</a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="#">Separated link</a></li>
                    </ul>
                  </li>

                  <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-envelope" style="font-size: 1.0em; color: #fff;"></i> <sup style="background: orange; padding: 2px 5px; color: #fff; font-size: 0.7em; border-radius: 10px; margin-left: -7px; margin-top: -10px;">5</sup></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">Action</a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="#">Separated link</a></li>
                    </ul>
                  </li>

                  <li class="dropdown">
                      <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-bell" style="font-size: 1.0em; color: #fff;"></i> <sup style="background: orangered; padding: 2px 5px; color: #fff; font-size: 0.7em; border-radius: 10px; margin-left: -8px; margin-top: -10px;">5</sup></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">Action</a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="#">Separated link</a></li>
                    </ul>
                  </li>

                  <li class="dropdown">
                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false"><i class="fa fa-user"></i> Profile <span class="caret"></span></a>
                    <ul class="dropdown-menu">
                      <li><a href="#">Action</a></li>
                      <li><a href="#">Another action</a></li>
                      <li><a href="#">Something else here</a></li>
                      <li role="separator" class="divider"></li>
                      <li><a href="#">Separated link</a></li>
                    </ul>
                  </li>

                  <li><a href="#" style="color: red;"><i class="fa fa-sign-out"></i></a></li>
                </ul>
              </div><!-- /.navbar-collapse -->
            </div><!-- /.container-fluid -->
          </nav>
      
        </header>
        
        
        
        
        <div class="container-fluid" style="border-bottom: 1px solid #ddd;">
            <div class="row">
                <div class="col-xs-2 col-sm-2 sidebar" style="min-height: 600px;">
                    
                    <div id="profile" style="margin: 0px 0px 0px 0px; padding: 10px; clear: both; height: 70px; border-bottom: 1px solid #ddd;">
                        <div class="profile-pic" style="float: left; width: 50px; height: 50px; background: red; border-radius: 25px;"><img class="img-" src=""/></div>
                        <div class="profile-name" style="float: left; margin-left: 5px; vertical-align: middle; font-size: 1.2em; width: 150px; height: 50px;">
                            <div style="color: #000;"><a href="">Hasan Tareque</a></div>
                            <div style="color: #666; font-size: 0.8em; font-style: italic;">Designation</div>
                        </div>
                    </div>
                    
                    
                    
                    <ul class="sidebar-list">
                        <li><a href="#">link 1</a></li>
                        <li class="active"><a href="#">link 1</a></li>
                        <li><a href="#">link 1</a></li>
                        <li><a href="#">link 1</a></li>
                        <li><a href="#">link 1</a></li>
                    </ul>

                    
                </div>
                <div class="col-xs-10 col-sm-10" style="min-height: 600px;">
                    <div class="row">
                        <div class="col-xs-10 col-sm-10" style="min-height: 600px; border-right: 1px solid #ddd; border-left: 1px solid #ddd;">
                            
                            <div class="content" style="padding: 0px;">
                                
                                
                                <div id="" style="margin: 0px; padding: 8px 0px 0px 0px; color: green; font-size: 1.5em;">
                                     <i class="fa fa-spinner fa-pulse fa-fw"></i> Loading ... 
                                </div>
                                
                                
                                
                                <div class="content-header" style="line-height: 50px; border-bottom: 1px solid #ddd; margin-bottom: 10px;">
                                    <div class="content-title" style="float: left; font-size: 1.5em; color: #265a88; line-height: 50px; padding: 0px; margin-right: 10px;"><i class="fa fa-file-movie-o"></i> test</div>
                                    <div class="content-links">
                                        <!-- Single button -->
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-default dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                Action <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu">
                                                <li><a href="#">Action</a></li>
                                                <li><a href="#">Another action</a></li>
                                                <li><a href="#">Something else here</a></li>
                                                <li role="separator" class="divider"></li>
                                                <li><a href="#">Separated link</a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                
                                

                               
                                <p class="alert alert-info"> <i class="fa fa-info-circle fa-fw"></i> Please put the form details here!</p>
                               


                                <form class="form-horizontal">
                                    
                                    
                                    <div class="form-group has-error">
                                        <label class="col-sm-2 control-label" for="inputSuccess1">Input with success</label>
                                        <div class="col-sm-10">
                                            <input type="text" class="form-control">
                                            <span class="help-block" style="font-size: 0.8em;">A block of help text that breaks onto a new line and may extend beyond one line.</span>
                                        </div>
                                    </div>
                                   

                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">First Name</label>
                                        <div class="col-sm-10">
                                            <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Last Name</label>
                                        <div class="col-sm-10">
                                            <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Email</label>
                                        <div class="col-sm-10">
                                            <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
                                        </div>
                                    </div>
                                    
                                  
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Phone</label>
                                        <div class="col-sm-10">
                                            <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Mobile</label>
                                        <div class="col-sm-10">
                                            <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputEmail3" class="col-sm-2 control-label">Role</label>
                                        <div class="col-sm-10">
                                            <input type="email" class="form-control" id="inputEmail3" placeholder="Email">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label for="inputPassword3" class="col-sm-2 control-label">Password</label>
                                        <div class="col-sm-10">
                                            <input type="password" class="form-control" id="inputPassword3" placeholder="Password">
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox"> Remember me
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="submit" class="btn btn-primary"> 
                                            
                                            <i class="fa fa-spinner fa-pulse fa-fw"></i> <span class="hidden-xs">Please wait ...</span>
                                            
                                            </button>
                                        </div>
                                    </div>
                                    
                                    
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="submit" class="btn btn-info"> 
                                            
                                            <i class="fa fa-save fa-fw"></i> <span class="hidden-xs">Save</span>
                                            
                                            </button>
                                        </div>
                                    </div>
                                    
                                    <div class="form-group">
                                        <div class="col-sm-offset-2 col-sm-10">
                                            <button type="submit" class="btn btn-primary"> 
                                            
                                            <i class="fa fa-refresh fa-fw"></i> <span class="hidden-xs">Update</span>
                                            
                                            </button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                            
                            
                        </div>
                        <div class="col-xs-2 col-sm-2" style="min-height: 600px;">sidebar</div>
                    </div>
                </div>
            </div>
        </div>
        
        <div class="container-fluid" style="background: #f5f5f5; height: 100px;">
            <div class="row">
                <div class="col-xs-6"></div>
                <div class="col-xs-6"></div>
            </div>
        </div>
        
        
        
        

        <script src="{{asset('js/app.js')}}"></script>
    </script>
</body>
</html>
