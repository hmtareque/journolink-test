<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alert Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'success' => 'Success',
    'warning' => 'Warning',
    'info' => 'Info',
    'danger' => 'Error',
    'error' => 'Error',
    
    
    'create_success' => ':name successfully created.',
    'create_failed' => 'Failed to create :name! Please try again.',
    'update_success' => ':name successfully updated.',
    'update_failed' => 'Failed to update :name! Please try again.',
    'delete_success' => ':name successfully deleted.',
    'delete_failed' => 'Failed to delete :name! Please try again.',
    'block_success' => ':name successfully blocked.',
    'block_failed' => 'Failed to block :name! Please try again.',
    'unblock_success' => ':name successfully unblocked.',
    'unblock_failed' => 'Failed to unblock :name! Please try again.',
    
    'publish_success' => ':name successfully published.',
    'publish_failed' => 'Failed to publish :name! Please try again.',
    
    'unpublish_success' => ':name successfully unpublished.',
    'unpublish_failed' => 'Failed to unpublish :name! Please try again.',
    
    'hide_success' => ':name successfully made hidden.',
    'hide_failed' => 'Failed to hide :name! Please try again.',
    
    'make_visible_success' => ':name successfully made visible.',
    'make_visible_failed' => 'Failed to make visible :name! Please try again.',
    
    
    'no_item_found' => 'Not :item found.',
    
    'sure_to_block_item' => 'Are you sure to block :item?',
    'sure_to_unblock_item' => 'Are you sure to unblock :item?',
    
    'sure_to_publish_item' => 'Are you sure to publish :item?',
    'sure_to_unpublish_item' => 'Are you sure to unpublish :item?',
    
    'sure_to_hide_item' => 'Are you sure to hide :item?',
    'sure_to_show_item' => 'Are you sure to make visible :item?',
    
    'sure_to_delete_item' => 'Are you sure to delete :item?',
    
    'never_logged_in' => 'Never logged in.',
    
    'no_users_found_of_this_role' => 'No users found of this role.',
    
    
    'are_you_sure_to_delete' => 'Are you sure to delete?'
    
    
    
    
    

];
