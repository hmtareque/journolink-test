<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Label Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'loading' => 'Loading ...',
    'app' => 'App',
    'settings' => 'Settings',
    'app_settings' => 'App Settings',
    'web_settings' => 'Web Settings',
    'business_settings' => 'Business Settings',
    'configurations' => 'Configurations',
    'parameter' => 'Parameter',
    'value' => 'Value',
    'remove' => 'Remove',
    
    'admins' => 'Admins',
    'recent_events' => 'Recent Events',
    
    'no_of_item' => 'No of :item',
    'list_of_items' => 'List of :item',
    'new_item' => 'New :item',
    'create_new_item' => 'Create New :item',
    'edit_item' => 'Edit :item',
    'delete_item' => 'Delete :item',
    'block_item' => 'Block :item',
    'unblock_item' => 'Unblock :item',
    
    
    'name' => 'Name',
    'business' => 'Business',
    'branch' => 'Branch',
    
    'role' => 'Role',
    'roles' => 'Roles',
    'email' => 'Email Address',
    'last_logged_in_at' => 'Last Logged in At',
    'status' => 'Status',
    'active' => 'Active',
    'action' => 'Action',
    
    'auth' => 'Auth',
    'user' => 'User',
    'users' => 'Users',
    'first_name' => 'First Name',
    'last_name' => 'Last Name',
    'email_address' => 'Email Address',
    'designation' => 'Designation',
    'password' => 'Password',
    
    'permission' => 'Permission',
    'permissions_of_role' => 'Permissions of :role Role',
    'users_of_role' => 'Users of :role Role',
    'module' => 'Module',
    'none' => 'None',
    'read_only' => 'Read Only',
    'read_write' => 'Read & Write',
    'read_write_update' => 'Read, Write & Update',
    'all' => 'All',
    
    
    
    'website' => 'Website',
    
    'type' => 'Type',
    'option' => 'Option',
    
    'category' => 'Category',
    'post_category' => 'Post Category',
    'post_categories' => 'Post Categories',
    'no_of_posts' => 'No of Posts',
    'visible' => 'Visible',
    
    'please_select' => 'Please select',
    'post' => 'Post',
    'posts' => 'Posts',
    'title' => 'Title',
    'meta_keywords' => 'Meta Keywords',
    'meta_description' => 'Meta Description',
    'content' => 'Content',
    'published' => 'Published',
    'created_at' => 'Created At',
    
    
    'do_not_publish' => 'Do not publish',
    
    'publish_item' => 'Publish :item',
    'unpublish_item' => 'Unpublish :item',
    
    'nav_type' => 'Navigation Type',
    'nav' => 'Navigation',
    'navs' => 'Navigations',
    
    'component' => 'Component',
    'components' => 'Components',
    
    'component_item' => 'Component Item',
    'component_items' => 'Component Items',
    
    'link' => 'Link',
    'links' => 'Links',
    'no_of_links' => 'No of Links',
    'links_of_nav' => 'Links of :nav',
    'create_link_of_nav' => 'Create link of :nav',
    'internal' => 'Internal',
    'internal_link' => 'Internal Link',
    'external' => 'External',
    'external_link' => 'External Link',
    'nav_link' => 'Navigation Link',
    'open_in_new_window' => 'Open in new window',
    
    
    'custom_component' => 'Custom Component',
    'custom_components' => 'Custom Components',
    'property' => 'Property',
    'items' => 'Item(s)',
    'no_of_items' => 'No of Items',
    'items_of_component' => 'Items of :component',
    'reload_default' => 'Reload Default',
    
    
    'identifier' => 'Identifier',
    
    
    
    
    
    'contact' => 'Contact',
    'contacts' => 'Contacts',
    'business_name' => 'Business Name',
    'address_line_1' => 'Address Line 1',
    'address_line_2' => 'Address Line 2',
    'street' => 'Street',
    'city' => 'City',
    'postcode' => 'Postcode',
    'county' => 'County',
    'country' => 'Country',
    'phone' => 'Phone',
    'fax' => 'Fax',
    'mobile' => 'Mobile',
    'web_contacts' => 'Web Contacts',
    'address' => 'Address',
    
    
    
    //customs
    
    'options_separated_by_comma' => 'Options Separated by Comma (,)',

];
