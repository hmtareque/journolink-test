<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Alert Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */
    
    'yes' => 'Yes',
    'no' => 'No',
    'close' => 'Close',
    'save' => 'Save',
    'edit' => 'Edit',
    'update' => 'Update',
    'delete' => 'Delete',
    'cancel' => 'Cancel',
    'block' => 'Block',
    'unblock' => 'Unblock',
    'publish' => 'Publish',
    'unpublish' => 'Unpublish',
    'delete' => 'Delete',
    'show' => 'Show',
    'hide' => 'Hide',
    
    'remove' => 'Remove',
    'reload' => 'Reload'
];
